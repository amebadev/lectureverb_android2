package com.malmstein.fenster.play;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.malmstein.fenster.R;
import com.malmstein.fenster.controller.FensterPlayerController;
import com.malmstein.fenster.controller.FensterPlayerControllerVisibilityListener;
import com.malmstein.fenster.controller.SimpleMediaFensterPlayerController;
import com.malmstein.fenster.view.FensterLoadingView;
import com.malmstein.fenster.view.FensterVideoView;

public class FensterVideoFragment extends Fragment implements FensterVideoStateListener, View.OnTouchListener
{

    private View contentView;
    public FensterVideoView textureView;
    public FensterPlayerController fensterPlayerController;
    private FensterLoadingView fensterLoadingView;

    public FensterVideoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.fen__fragment_fenster_video, container);
        textureView = (FensterVideoView) contentView.findViewById(R.id.fen__play_video_texture);
        fensterPlayerController = (FensterPlayerController) contentView.findViewById(R.id.fen__play_video_controller);
        fensterLoadingView = (FensterLoadingView) contentView.findViewById(R.id.fen__play_video_loading);
        return contentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initVideo();
    }

    private void initVideo() {
        textureView.setMediaController(fensterPlayerController);
        textureView.setOnPlayStateListener(this);
    }

    public void playVideo(String videoUrl) {
        textureView.setVideo(videoUrl,
                SimpleMediaFensterPlayerController.DEFAULT_VIDEO_START);
        textureView.start();

        textureView.setOnTouchListener(this);
    }

    public void setVisibilityListener(FensterPlayerControllerVisibilityListener visibilityListener) {
        fensterPlayerController.setVisibilityListener(visibilityListener);
    }

    public void showFensterController() {
        fensterLoadingView.hide();
        fensterPlayerController.show();
    }

    private void showLoadingView() {
        fensterLoadingView.show();
        fensterPlayerController.hide();
    }

    @Override
    public void onFirstVideoFrameRendered() {
        fensterPlayerController.show();
    }

    @Override
    public void onPlay() {
        showFensterController();
    }

    @Override
    public void onBuffer() {
        showLoadingView();
    }

    @Override
    public boolean onStopWithExternalError(int position) {
        return false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (fensterPlayerController.isControllerShowing()) {
            fensterPlayerController.hide();
        }
        else
        {
            fensterPlayerController.show();
        }

        return false;
    }
}
