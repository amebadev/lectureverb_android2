package gilm.lecture.verb.UtilsG.BitmapUtils;

import android.animation.LayoutTransition;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import gilm.lecture.verb.R;

/**
 * Created by G-Expo on 19 Apr 2017.
 */
public class SmartImageView extends FrameLayout
{

    public SmartImageView(Context context)
    {
        super(context);

        init(context, null);
    }

    public SmartImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs);
    }

    public SmartImageView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public SmartImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private ImageView imageView;

    private View progressBar;

    public void init(Context context, AttributeSet attrs)
    {
        this.context = context;

        imageView = new ImageView(context);
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER;
        imageView.setLayoutParams(params);

        setLayoutTransition(new LayoutTransition());

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        progressBar = inflater.inflate(R.layout.progress_dialog, null);
        progressBar.setVisibility(INVISIBLE);
        progressBar.setLayoutParams(params);

        addView(imageView);
        addView(progressBar);
    }

    private Context context;

    public ImageView getImageView()
    {
        return imageView;
    }

    public View getProgressBar()
    {
        return progressBar;
    }

    public void loadingImage(boolean loading)
    {
        if (loading) {
            imageView.setVisibility(INVISIBLE);
            progressBar.setVisibility(VISIBLE);
        }
        else {
            progressBar.setVisibility(INVISIBLE);
            imageView.setVisibility(VISIBLE);
        }
    }

}