package gilm.lecture.verb.UtilsG;

import org.greenrobot.eventbus.EventBus;

import gilm.lecture.verb.Features.Help.HelpFragment;
import gilm.lecture.verb.Features.Home.Home.HomeFragment;
import gilm.lecture.verb.Features.Home.HomeTabFragment;
import gilm.lecture.verb.Features.LectureEvent.LectureEventTabFragment;
import gilm.lecture.verb.Features.MyProfile.View.MyProfileFragment;
import gilm.lecture.verb.Features.NaviagtionDrawer.NavigationBus;
import gilm.lecture.verb.Features.NaviagtionDrawer.Title;
import gilm.lecture.verb.Features.Search.SearchLectureFragment;
import gilm.lecture.verb.Features.Settings.MainSettings.SettingsFragment;
import gilm.lecture.verb.Features.Settings.Message.MessagesSettingFragment;
import gilm.lecture.verb.Features.Settings.Notifications.NotificationSettingsFragment;
import gilm.lecture.verb.Features.Settings.Tags.TagSettingsFragment;

/**
 * Created by G-Expo on 07 Jul 2017.
 */

/**
 * Responsible for changing title of navigation screen using event bus.
 */
public class TitleNavigator
{
    private static final TitleNavigator ourInstance = new TitleNavigator();

    public static TitleNavigator getInstance()
    {
        return ourInstance;
    }

    private TitleNavigator()
    {
    }

    public void homeTabScreen(HomeTabFragment homeFragment)
    {
        EventBus.getDefault().post(new NavigationBus(Title.HOME, homeFragment));
    }

    public void homeScreen(HomeFragment homeFragment)
    {
        EventBus.getDefault().post(new NavigationBus(Title.HOME, homeFragment));
    }

    public void searchScreen(SearchLectureFragment searchLectureFragment)
    {
        EventBus.getDefault().post(new NavigationBus(Title.SEARCH, searchLectureFragment));
    }

    public void showMainSettings(SettingsFragment settingsFragment)
    {
        EventBus.getDefault().post(new NavigationBus(Title.SETTINGS, settingsFragment));
    }

    public void LectureEventScreen(LectureEventTabFragment lectureEventTabFragment)
    {
        EventBus.getDefault().post(new NavigationBus(Title.LECTURE_EVENTS, lectureEventTabFragment));
    }

    public void RecentActivity()
    {
        //EventBus.getDefault().post(new NavigationBus(Title.RECENT_ACTIVITIES, null));
        EventBus.getDefault().post(new NavigationBus(Title.MY_PROFILE, null));
    }

    public void Notification()
    {
        EventBus.getDefault().post(new NavigationBus(Title.ALERT_NOTIFICATION, null));
    }

    public void DirectMessage()
    {
        EventBus.getDefault().post(new NavigationBus(Title.DIRECT_MESSAGE, null));
    }

    public void ShowHelpScreen(HelpFragment helpFragment)
    {
        EventBus.getDefault().post(new NavigationBus(Title.HELP, helpFragment));
    }

    public void ShowMessageSettings(MessagesSettingFragment messagesSettingFragment)
    {
        EventBus.getDefault().post(new NavigationBus(Title.MESSAGE_SETTINGS, messagesSettingFragment));

    }

    public void ShowNotificationSettings(NotificationSettingsFragment notificationSettingsFragment)
    {
        EventBus.getDefault().post(new NavigationBus(Title.NOTIFICATION_SETTINGS, notificationSettingsFragment));
    }

    public void ShowTagSettings(TagSettingsFragment tagSettingsFragment)
    {
        EventBus.getDefault().post(new NavigationBus(Title.TAG_SETTINGS, tagSettingsFragment));
    }

    public void ShowMyProfile(MyProfileFragment myProfileFragment)
    {
        EventBus.getDefault().post(new NavigationBus(Title.MY_PROFILE, myProfileFragment));
    }
}
