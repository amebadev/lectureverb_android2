package gilm.lecture.verb.UtilsG;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsApi;
import gilm.lecture.verb.R;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.Web;
import ooo.oxo.library.widget.TouchImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static android.content.Context.NOTIFICATION_SERVICE;
import static gilm.lecture.verb.MyFirebaseMessagingService.resourceToUri;

/**
 * Created by G-Expo on 05 Jul 2017.
 */

public class UtillsG
{
    private static Toast toastG;
    private static ProgressDialog progressDialog;

    /**
     * @param msg    -message to be displayed
     * @param center - true ,if toast is to be displayed in center,otherwise false.
     */
    public static void showToast(String msg, Context context, boolean center)
    {
        if (toastG != null)
        {
            toastG.cancel();
        }
        toastG = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        if (center)
        {
            toastG.setGravity(Gravity.CENTER, 0, 0);
        }
        toastG.show();
    }

    public static void toggleKeyboard(Context con, View view)
    {
        if (view != null)
        {
            InputMethodManager imm = (InputMethodManager) con.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public static String get_time_ago(String stringDate)
    {
        SimpleDateFormat format = new SimpleDateFormat(DateHelper.DateFormat);
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        format.setTimeZone(timeZone);
        Date date = null;
        try
        {
            date = format.parse(stringDate);
            System.out.println(date);
        } catch (ParseException e)
        {
            e.printStackTrace();
        }

        long diff = System.currentTimeMillis() - date.getTime();
        return millisToLongDHMS(
                diff > 0 ? diff : -(diff)).replaceAll(",", "") + " ago.";
    }

    public final static long ONE_SECOND = 1000;
    public final static long SECONDS = 60;

    public final static long ONE_MINUTE = ONE_SECOND * 60;
    public final static long MINUTES = 60;

    public final static long ONE_HOUR = ONE_MINUTE * 60;
    public final static long HOURS = 24;

    public final static long ONE_DAY = ONE_HOUR * 24;
    public final static long MONTHS = ONE_DAY * 30;

    public static String millisToLongDHMS(long duration)
    {
        StringBuffer res = new StringBuffer();
        long temp = 0;
        if (duration >= ONE_SECOND)
        {
            temp = duration / MONTHS;
            if (temp > 0)
            {
                duration -= temp * MONTHS;
                res.append(temp).append(" month").append(temp > 1 ? "s" : "");
                return res.toString();
            }


            temp = duration / ONE_DAY;
            if (temp > 0)
            {
                duration -= temp * ONE_DAY;
                res.append(temp).append(" day").append(temp > 1 ? "s" : "");

                return res.toString();
            }

            temp = duration / ONE_HOUR;
            if (temp > 0)
            {
                duration -= temp * ONE_HOUR;
                res.append(temp).append(" hour").append(temp > 1 ? "s" : "");
                return res.toString();
            }

            temp = duration / ONE_MINUTE;
            if (temp > 0)
            {
                duration -= temp * ONE_MINUTE;
                res.append(temp).append(" minute").append(temp > 1 ? "s" : "");
                return res.toString();
            }

            if (!res.toString().equals("") && duration >= ONE_SECOND)
            {
                res.append(" and ");
            }

            temp = duration / ONE_SECOND;
            if (temp > 0)
            {
                //                res.append(temp).append(" second").append(temp > 1 ? "s" : "");
                res.append("few seconds");
            }
            return res.toString();
        } else
        {
            return "0 second";
        }
    }


    public static void shakeThisView(View target_view)
    {
        final AnimatorSet mAnimatorSet = new AnimatorSet();
        mAnimatorSet.playTogether(ObjectAnimator.ofFloat(target_view, "scaleX", 1, 0.7f, 0.7f, 0.7f, 1, 1), ObjectAnimator.ofFloat(target_view, "scaleY", 1, 0.7f, 0.7f, 0.7f, 1, 1), ObjectAnimator.ofFloat(target_view, "rotation", 0, -3, -3, 3, -3, 3, -3, 3, -3, 3, -3, 0));
        mAnimatorSet.setDuration(1000);
        mAnimatorSet.start();
    }

    // this animation will zoon the view from its -60% to 100 zooming position . means from -60% to its origial size
    public synchronized static void zoomUpaBit(View target_view)
    {
        final AnimatorSet mAnimatorSet = new AnimatorSet();
        mAnimatorSet.playTogether(ObjectAnimator.ofFloat(target_view, "scaleX", 0.6f, 1f), ObjectAnimator.ofFloat(target_view, "scaleY", 0.6f, 1f));
        mAnimatorSet.setDuration(500);
//        mAnimatorSet.setInterpolator(new BounceInterpolator());
        mAnimatorSet.start();
    }

    public static String getNotNullString(String target, String defaultString)
    {
        return target == null || target.trim().isEmpty() ? defaultString
                : target.equals("null") ? defaultString
                : target.equals("") ? defaultString : target;
    }

    /**
     * finish all the activities from stack.(works only in higher versions).
     */
    public static void finishAll(Context context)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
        {
            ((Activity) context).finishAffinity();
        } else
        {

            ((Activity) context).finish();
        }
    }

    /**
     * @param i    -intent to be fired.
     * @param logo --shareable view. (used shared object for transitions ).
     */
    public static void startTransition(Activity activity, Intent i, View logo)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(activity,
                    Pair.create(logo, Constants.Extras.TRANSITION_NAME_1));
            activity.startActivity(i, options.toBundle());
        } else
        {

        }
    }

    /**
     * @param i     -intent to be fired.activity.startActivity(i);
     * @param view1 --1st shareable view. (used shared object for transitions ).
     * @param view2 --2nd shareable view. (used shared object for transitions ).
     */
    public static void startTransition(Activity activity, Intent i, View view1, View view2)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Pair<View, String> p1 = Pair.create(view1, Constants.Extras.TRANSITION_NAME_1);
            Pair<View, String> p2 = Pair.create(view2, Constants.Extras.TRANSITION_NAME_2);

            ActivityOptions options = ActivityOptions.
                    makeSceneTransitionAnimation(activity, p1, p2);
            activity.startActivity(i, options.toBundle());
        } else
        {
            activity.startActivity(i);
        }
    }

    /**
     * @return true, if app is running in foreground.
     */
    public static boolean isAppOnForeground(Context context)
    {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null)
        {
            return false;
        }
        final String packageName = "gilm.lecture.verb";
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses)
        {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * @param view --current focused view
     */
    public static void hideKeyboard(Context context, View view)
    {
        if (view != null)
        {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public static void shareIntent(Context con, String text)
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TITLE, con.getString(R.string.invitation_message));
        sendIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(con.getString(R.string.invitation_deep_link)));
        sendIntent.setType("text/plain");
        con.startActivity(sendIntent);
    }

    public static void sendFireBaseInvite(Context activityG)
    {
        Intent intent = new AppInviteInvitation.IntentBuilder(activityG.getString(R.string.app_name))
                .setEmailSubject(activityG.getString(R.string.invitation_message))
                .setEmailHtmlContent(activityG.getString(R.string.invitation_deep_link))
//                .setCustomImage(Uri.parse(activityG.getString(R.string.invitation_custom_image)))
                .build();
        ((Activity) activityG).startActivityForResult(intent, Constants.RequestCode.INVITE);
    }

    public static void InviteByOtherApp(Context activityG)
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, activityG.getString(R.string.invitation_message));
        sendIntent.putExtra(Intent.EXTRA_TEXT, activityG.getString(R.string.invitation_text));
        sendIntent.setType("text/plain");
        ((Activity) activityG).startActivity(sendIntent);

    }

    /**
     * @param con
     * @param textView  pass any view having text compatibility. Example : Edittext also extends TextView
     * @param percntage give it followed by "f"
     */
    public static void setTextSizeByPercentage(Context con, TextView textView, float percntage)
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) con).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        float textSize = width * percntage;

        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (textSize / 100));
    }

    public static String getWifiStrength(Context mContext)
    {
        final WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        int state = wifi.getWifiState();
        int signalStrangth = 0;
        if (state == WifiManager.WIFI_STATE_ENABLED)
        {
            List<ScanResult> results = wifi.getScanResults();

            for (ScanResult result : results)
            {
                if (result.BSSID.equals(wifi.getConnectionInfo().getBSSID()))
                {
                    int level = WifiManager.calculateSignalLevel(wifi.getConnectionInfo().getRssi(),
                            result.level);
                    int difference = level * 100 / result.level;
                    if (difference >= 100)
                    {
                        signalStrangth = 4;
                    } else if (difference >= 75)
                    {
                        signalStrangth = 3;
                    } else if (difference >= 50)
                    {
                        signalStrangth = 2;
                    } else if (difference >= 25)
                    {
                        signalStrangth = 1;
                    } else
                    {
                        signalStrangth = 0;
                    }
                    Log.e("Values are", "\nDifference :" + difference + " signal state:" + signalStrangth);

                }

            }
        }
        return "" + signalStrangth;
    }

    public static String getMacAddress(Context con)
    {
        WifiManager manager = (WifiManager) con.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        return info.getMacAddress();
    }


    public static void showFullImage(String image, Context con, boolean isUri)
    {
        final Dialog image_dialog = new Dialog(con);
        image_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        image_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#68000000")));
        image_dialog.setContentView(R.layout.dialog_full_image);
        image_dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(image_dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;

        TouchImageView imgv_image = (TouchImageView) image_dialog.findViewById(R.id.imgv_image);
        ImageView imgvClose = (ImageView) image_dialog.findViewById(R.id.imgvClose);

        try
        {
            if (isUri)
            {
                Picasso.with(con).load(new File(image)).error(R.color.greyDark).into(imgv_image);
            } else
            {
                Picasso.with(con).load(image).error(R.color.greyDark).into(imgv_image);
            }
        } catch (Exception e)
        {
            Picasso.with(con).load(image).centerInside().error(R.color.greyLight).into(imgv_image);
        }
        imgvClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                image_dialog.dismiss();
            }
        });

        image_dialog.show();
        image_dialog.getWindow().setAttributes(lp);
    }

    public static void showFullSizeVideo(String URL_URI, Context con)
    {
        final Dialog image_dialog = new Dialog(con);
        image_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        image_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        image_dialog.setContentView(R.layout.dialog_full_screen_videoview);
        image_dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(image_dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;

        VideoView videoView = (VideoView) image_dialog.findViewById(R.id.videoView);
        MediaController mediaController = new MediaController(con);
        mediaController.setAnchorView(videoView);
        mediaController.setMediaPlayer(videoView);
        videoView.setVideoURI(Uri.parse(URL_URI));
        videoView.start();

        (image_dialog.findViewById(R.id.imgv_close)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                image_dialog.dismiss();
            }
        });

        image_dialog.show();
        image_dialog.getWindow().setAttributes(lp);
    }

    public static String getScreenShotOf(View view, Context context) throws ClassCastException
    {
        try
        {
            /**
             * the screenshot functionality does not works properly in marshmallow in case of full webview screenshot . So we have separated the code for vartious versions.
             */
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
            {
                view.setDrawingCacheEnabled(true);
                return storeImage(context, view.getDrawingCache());
            } else
            {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
                view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                view.buildDrawingCache();
                Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

                Canvas canvas = new Canvas(bitmap);
                view.draw(canvas);
                return storeImage(context, bitmap);
            }
        } catch (Exception e)
        {
            UtillsG.showToast("Error while taking screenshot.", context, false);
        }

        return null;
    }

    private static String storeImage(Context context, Bitmap image)
    {
        File pictureFile = getOutputMediaFile(context);
        if (pictureFile == null)
        {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return "";
        }
        try
        {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();

            return pictureFile.getAbsolutePath();
        } catch (FileNotFoundException e)
        {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e)
        {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
        return "";
    }

    private static File getOutputMediaFile(Context context)
    {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + context.getApplicationContext().getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists())
        {
            if (!mediaStorageDir.mkdirs())
            {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public static void setHeightWidthWithRatio_LinearLayout(Activity activity, View imgv, float height, float width)
    {
        Display display = activity.getWindowManager().getDefaultDisplay();
        float screenWidth = Math.round(display.getWidth());
        float imageHeight = height;
        float imageWidth = width;
        float target1 = imageHeight / imageWidth;
        float targetHeight = target1 * screenWidth;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Math.round(targetHeight));
        imgv.setLayoutParams(params);
        imgv.invalidate();

        Log.e("TARGET HEIGHT", targetHeight + "");
    }

    public static void setHeightWidthWithRatio_FrameLayout(Activity activity, View imgv, float height, float width)
    {
        Display display = activity.getWindowManager().getDefaultDisplay();
        float screenWidth = Math.round(display.getWidth());
        float imageHeight = height;
        float imageWidth = width;
        float target1 = imageHeight / imageWidth;
        float targetHeight = target1 * screenWidth;

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Math.round(targetHeight));
        imgv.setLayoutParams(params);
        imgv.invalidate();

        Log.e("TARGET HEIGHT", targetHeight + "");
    }

    public static void setHeightWidtWRAP_LinearLayout(Activity activity, View imgv, Context con)
    {
        Display display = activity.getWindowManager().getDefaultDisplay();

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imgv.setLayoutParams(params);
        imgv.invalidate();
    }

    public static Uri saveBitmapToLocal(Bitmap inImage)
    {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
//        return Uri.parse(path);

        try
        {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            inImage.compress(Bitmap.CompressFormat.JPEG, 40, bytes);

            File f = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "thumbnail.jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());

            fo.close();

            return Uri.fromFile(f);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static void showLoading(String progressMessage, Context context)
    {
        if (progressDialog == null)
        {
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(context.getResources().getString(R.string.app_name));
            progressDialog.setMessage(progressMessage);
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
    }

    public static void hideLoading()
    {
        if (progressDialog != null && progressDialog.isShowing())
        {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public static String secondsToMinutes(int totalSeconds)
    {

        final int MINUTES_IN_AN_HOUR = 60;
        final int SECONDS_IN_A_MINUTE = 60;

        int seconds = totalSeconds % SECONDS_IN_A_MINUTE;
        int totalMinutes = totalSeconds / SECONDS_IN_A_MINUTE;
        int minutes = totalMinutes % MINUTES_IN_AN_HOUR;
        int hours = totalMinutes / MINUTES_IN_AN_HOUR;

        if (hours == 0)
        {
            return (minutes < 10 ? "0" + minutes : minutes)
                    + ":"
                    + (seconds < 10 ? "0" + seconds : seconds);
        } else
        {
            return (hours < 10 ? "0" + hours : hours)
                    + ":"
                    + (minutes < 10 ? "0" + minutes : minutes)
                    + ":"
                    + (seconds < 10 ? "0" + seconds : seconds);
        }
    }

    public static String encodeEmoji(String message)
    {
        try
        {
            return URLEncoder.encode(message,
                    "UTF-8");
        } catch (UnsupportedEncodingException e)
        {
            return message;
        }
    }


    public static String decodeEmoji(String message)
    {
        String myString = null;
        try
        {
            return URLDecoder.decode(
                    message, "UTF-8");
        } catch (UnsupportedEncodingException e)
        {
            return message;
        }
    }

    public static String QuickLectureType(String quickLectureType, String defaultString)
    {
        if (quickLectureType.equals("text"))
        {
            return "Text Post ";
        } else if (quickLectureType.equals("audio"))
        {
            return "Audio Post ";
        } else if (quickLectureType.equals("video"))
        {
            return "Presentation Post ";
        } else if (quickLectureType.equals("discovery_audio"))
        {
            return "Discovery Audio Post ";
        } else if (quickLectureType.equals("discovery_video"))
        {
            return "Discovery Video Post ";
        } else if (quickLectureType.equals("event_video"))
        {
            return "Video Post ";
        } else if (quickLectureType.equals("event_video"))
        {
            return "Audio Post ";
        } else if (quickLectureType.equals("discovery_text"))
        {
            return "Discovery Text Post ";
        } else
        {
            return defaultString;
        }
    }

    public static boolean isAudioVideoMediaPost(Lecture_And_Event_Model model)
    {
        return model.getQuick_lecture_type().equalsIgnoreCase("audio")
                || model.getQuick_lecture_type().equalsIgnoreCase("video")
                || model.getQuick_lecture_type().equalsIgnoreCase("discovery_audio")
                || model.getQuick_lecture_type().equalsIgnoreCase("discovery_video")
                || model.getQuick_lecture_type().equalsIgnoreCase("event_audio")
                || model.getQuick_lecture_type().equalsIgnoreCase("event_video");
    }

    public static boolean isTextPost(Lecture_And_Event_Model model)
    {
        return model.getQuick_lecture_type().equalsIgnoreCase("text")
                || model.getQuick_lecture_type().equalsIgnoreCase("discovery_text");
    }

    public static ArrayList<WebInnerStackModel> jsonToArrayList_InnerStack(String json)
    {
        ArrayList<WebInnerStackModel> alInnerStack = new ArrayList<>();
        try
        {

            WebHistoryModel model = new WebHistoryModel();
            JSONObject inner = new JSONObject(json);
            model.setTitle(inner.optString("title"));
            model.setUrl(inner.optString("url"));
            model.setIcon(inner.optString("icon"));

            JSONArray innerStack = inner.optJSONArray("innerStack");

            for (int j = 0; j < innerStack.length(); j++)
            {
                JSONObject innerStackObj = innerStack.optJSONObject(j);
                WebInnerStackModel innerModel = new WebInnerStackModel();
                innerModel.setTitle(innerStackObj.optString("title"));
                innerModel.setUrl(innerStackObj.optString("url"));
                alInnerStack.add(innerModel);
            }
            model.setInnerstack(alInnerStack);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return alInnerStack;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String arrayListToJsonObject_innerStack(Context context, String icon, String title, String url, ArrayList<WebInnerStackModel> alStack)
    {
        try
        {
            JSONObject innerObj = new JSONObject();
            innerObj.put("url", url);
            innerObj.put("title", title);
            innerObj.put("icon", icon);

            JSONArray innerStackArray = new JSONArray();
            for (int i = 0; i < alStack.size(); i++)
            {
                JSONObject innerStack = new JSONObject();
                innerStack.put("title", alStack.get(i).getTitle());
                innerStack.put("url", alStack.get(i).getUrl());
                innerStackArray.put(innerStack);
            }
            innerObj.put("innerStack", innerStackArray);

            return innerObj.toString(); // returning the currently saved object back to be saved on server as a history stack for the current post
        } catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }
    }

    public static void displayLocationSettingsRequest(final Fragment context)
    {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context.getActivity())
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>()
        {
            @Override
            public void onResult(LocationSettingsResult result)
            {
                final Status status = result.getStatus();
                switch (status.getStatusCode())
                {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try
                        {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(context.getActivity(), Constants.RequestCode.GPS_LOCATION);
                        } catch (IntentSender.SendIntentException e)
                        {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }


    public static boolean isGpsEnabled(Context con)
    {
        boolean gps_enabled = false;
        boolean network_enabled = false;
        LocationManager lm = (LocationManager) con.getSystemService(Context.LOCATION_SERVICE);

        try
        {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }

        if (!gps_enabled)// && !network_enabled)
        {
            return false;
        } else
        {
            return true;
        }
    }

    public static boolean isExternalStorageAvailable()
    {

        String state = Environment.getExternalStorageState();
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;

        if (Environment.MEDIA_MOUNTED.equals(state))
        {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
        {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else
        {
            // Something else is wrong. It may be one of many other states, but
            // all we need
            // to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }

        if (mExternalStorageAvailable == true
                && mExternalStorageWriteable == true)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public static ArrayList<String> getExternalMounts()
    {
        final ArrayList<String> out = new ArrayList<String>();
        String reg = "(?i).*vold.*(vfat|ntfs|exfat|fat32|ext3|ext4).*rw.*";
        String s = "";
        try
        {
            final Process process = new ProcessBuilder().command("mount")
                    .redirectErrorStream(true).start();
            process.waitFor();
            final InputStream is = process.getInputStream();
            final byte[] buffer = new byte[1024];
            while (is.read(buffer) != -1)
            {
                s = s + new String(buffer);
            }
            is.close();
        } catch (final Exception e)
        {
            e.printStackTrace();
        }

        // parse output
        final String[] lines = s.split("\n");
        for (String line : lines)
        {
            if (!line.toLowerCase(Locale.US).contains("asec"))
            {
                if (line.matches(reg))
                {
                    String[] parts = line.split(" ");
                    for (String part : parts)
                    {
                        if (part.startsWith("/"))
                        {
                            if (!part.toLowerCase(Locale.US).contains("vold"))
                            {
                                out.add(part);
                            }
                        }
                    }
                }
            }
        }
        return out;
    }

    public static String getExternalSdCardPath()
    {
        String path = null;

        File sdCardFile = null;
        List<String> sdCardPossiblePath = Arrays.asList("media_rw/A01B-247D", "external_sd", "ext_sd", "external", "extSdCard", "externalSdCard", "media_rw/sdcard1", "40A1-0AE8");

        for (String sdPath : sdCardPossiblePath)
        {
            File file = new File("/mnt/", sdPath);

            if (file.isDirectory() && file.canWrite())
            {
                path = file.getAbsolutePath();

                String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
                File testWritable = new File(path, "test_" + timeStamp);

                if (testWritable.mkdirs())
                {
                    testWritable.delete();
                } else
                {
                    path = null;
                }
            }
        }

        if (path != null)
        {
            sdCardFile = new File(path);
        } else
        {
            sdCardFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        }

        return sdCardFile.getAbsolutePath();
    }

    public static boolean deleteThisFile(String path)
    {
        File fdelete = new File(path);
        if (fdelete.exists())
        {
            return fdelete.delete();
        }
        return false;
    }

    public static void showNotification(Context con, String title, String message, Intent notificationIntent)
    {
        try
        {
            Uri defaultSound = resourceToUri(con, new SharedPrefHelper(con).getSoundId());

            boolean isSoundEnabled = new SharedPrefHelper(con).isNotificationSoundEnabled();


            if (defaultSound == null)
            {
                defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                if (defaultSound == null)
                {
                    defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                }
            }

            SharedPrefHelper spHelper = new SharedPrefHelper(con);

            NotificationCompat.Builder mBuilder;
            if (isSoundEnabled)
            {
                mBuilder = new NotificationCompat.Builder(con, "default")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setSound(defaultSound)
                        .setVibrate(new long[]{spHelper.getVibrationPattern(), spHelper.getVibrationPattern()})
                        .setLights(Color.parseColor(new SharedPrefHelper(con).getNotificationLightColorCode()), 20, 80)
                        .setAutoCancel(true)
                        .setContentText(message);
            } else
            {
                mBuilder = new NotificationCompat.Builder(con, "default")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setVibrate(new long[]{spHelper.getVibrationPattern(), spHelper.getVibrationPattern()})
                        .setLights(Color.parseColor(new SharedPrefHelper(con).getNotificationLightColorCode()), 20, 80)
                        .setAutoCancel(true)
                        .setContentText(message);

            }
            Intent resultIntent = notificationIntent;
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            con,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            mBuilder.setContentIntent(resultPendingIntent);

            int mNotificationId = 001;
            // Gets an instance of the NotificationManager service
            NotificationManager mNotifyMgr = (NotificationManager) con.getSystemService(NOTIFICATION_SERVICE);
            // Builds the notification and issues it.
            mNotifyMgr.notify(mNotificationId, mBuilder.build());
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void ads_counter(Context context, String user_id, String ads_id)
    {
        Call<BasicApiModel> call = LVApplication.getRetrofit().create(HomeApis.class).user_ads_counter(user_id);
        call.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
            {
                Log.e("User_Ads Counter.....", response.body().getMessage());
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t)
            {
                Log.e("User_Ads Counterfailure", t.getMessage());

            }
        });
        Call<BasicApiModel> call1 = LVApplication.getRetrofit().create(HomeApis.class).ads_counter(ads_id);
        call1.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
            {
                Log.e(".Ads Counter.....", response.body().getMessage());

            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t)
            {
                Log.e("Ads Counter Failure....", t.getMessage());

            }
        });
    }

    public static void showInterstitialAds(final Context context, String AddId, String AdUnitId)
    {


        MobileAds.initialize(context, AddId);

        final InterstitialAd mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(AdUnitId);
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener()
        {
            @Override
            public void onAdClosed()
            {

            }

            @Override
            public void onAdLoaded()
            {
                mInterstitialAd.show();

            }

            @Override
            public void onAdFailedToLoad(int errorCode)
            {
                UtillsG.showToast(errorCode + "", context, false);
            }
        });

    }

    public static void showInterstitialAdsWithCallBack(final Context context, String AddId, String AdUnitId, final CallBackG<Boolean> callback)
    {

        UtillsG.showLoading("Please wait..", context);
        MobileAds.initialize(context, AddId);

        final InterstitialAd mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(AdUnitId);
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener()
        {
            @Override
            public void onAdClosed()
            {
                callback.onCallBack(true);
            }

            @Override
            public void onAdLoaded()
            {
                mInterstitialAd.show();
                UtillsG.hideLoading();

            }

            @Override
            public void onAdFailedToLoad(int errorCode)
            {
                UtillsG.showToast(errorCode + "", context, false);
                callback.onCallBack(false);
                UtillsG.hideLoading();
            }
        });

    }

    public static boolean isAlreadyCallBacked = false;

    public static void showRewardedAds(final Context context, String AddId, String AdUnitId, final CallBackG<Boolean> callback)
    {
        UtillsG.showLoading("Please wait..", context);
        MobileAds.initialize(context, AddId);

        final RewardedVideoAd mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context);
        mRewardedVideoAd.loadAd(AdUnitId,
                new AdRequest.Builder().build());
        mRewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener()
        {
            @Override
            public void onRewarded(RewardItem reward)
            {
                isAlreadyCallBacked = true;
                callback.onCallBack(true);
                UtillsG.hideLoading();
                // Reward the user.
            }

            @Override
            public void onRewardedVideoAdLeftApplication()
            {
//                Toast.makeText(context, "onRewardedVideoAdLeftApplication",
//                        Toast.LENGTH_SHORT).show();
//                callback.onCallBack(true);
                UtillsG.hideLoading();
            }

            @Override
            public void onRewardedVideoAdClosed()
            {
                if (!isAlreadyCallBacked)
                {
//                Toast.makeText(context, "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();noti
                    callback.onCallBack(true);
                    UtillsG.hideLoading();
                } else
                {
                    isAlreadyCallBacked = false;
                }
            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int errorCode)
            {
//                Toast.makeText(context, "onRewardedVideoAdFailedToLoad", Toast.LENGTH_SHORT).show();
                callback.onCallBack(false);
                UtillsG.hideLoading();
            }

            @Override
            public void onRewardedVideoAdLoaded()
            {
                mRewardedVideoAd.show();
                UtillsG.hideLoading();

            }

            @Override
            public void onRewardedVideoAdOpened()
            {
//                Toast.makeText(context, "onRewardedVideoAdOpened", Toast.LENGTH_SHORT).show();
                UtillsG.hideLoading();
            }

            @Override
            public void onRewardedVideoStarted()
            {
//                Toast.makeText(context, "onRewardedVideoStarted", Toast.LENGTH_SHORT).show();
                UtillsG.hideLoading();
            }

        });


    }

    public static void showBannerAds(Context context)
    {


        MobileAds.initialize(context, "ca-app-pub-3238690422120916~5130468951");

        final InterstitialAd mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId("ca-app-pub-3238690422120916/1783563967");
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener()
        {
            @Override
            public void onAdClosed()
            {

            }

            @Override
            public void onAdLoaded()
            {
                mInterstitialAd.show();

            }

            @Override
            public void onAdFailedToLoad(int errorCode)
            {

            }
        });

    }

    public static void shareLinkExternally_QuickLecture(final Context context, final Lecture_And_Event_Model mModel)
    {
//        Intent share = new Intent(Intent.ACTION_SEND);
//        share.setType("text/plain");
//        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//        share.putExtra(Intent.EXTRA_SUBJECT, "Shared via Lecture Verb");
//        share.putExtra(Intent.EXTRA_TEXT, Web.Path.EXTERNAL_SHARE_URL_QUICK_LECTURE + mModel.getQuick_lecture_id());
//        context.startActivity(Intent.createChooser(share, "Share link!"));

        customShareDialog(context, Web.Path.EXTERNAL_SHARE_URL_QUICK_LECTURE + mModel.getQuick_lecture_id(), mModel.getQuick_lecture_id(), "lecture");
    }

    public static void sendDirectNotification(Context context, String receiver_id, String flag, String extra_data, final CallBackG<Boolean> callBackG)
    {
        UtillsG.showLoading("Please wait..", context);
        Call<BasicApiModel> call = LVApplication.getRetrofit().create(LectureEventsApi.class).send_direct_notification(receiver_id, flag, extra_data);
        call.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
            {
                UtillsG.hideLoading();
                callBackG.onCallBack(response.body().getStatus());
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t)
            {
                UtillsG.hideLoading();
                callBackG.onCallBack(false);
            }
        });
    }

    public static void customShareDialog(final Context context, final String url, final String postid, final String post_type)
    {
        final Item[] items = {
                new Item("Email", R.drawable.ic_gmail_outline),
                new Item("Facebook", R.drawable.ic_fb_outline),
                new Item("WhatsApp", R.drawable.ic_whatsapp_out),//no icon for this one
                new Item("Twitter", R.drawable.ic_twitter_out),//no icon for this one
        };

        ListAdapter adapter = new ArrayAdapter<Item>(
                context,
                android.R.layout.select_dialog_item,
                android.R.id.text1,
                items)
        {
            public View getView(int position, View convertView, ViewGroup parent)
            {
                //Use super class to create the View
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView) v.findViewById(android.R.id.text1);

                //Put the image on the TextView
                tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);

                //Add margin between image and text (support various screen densities)
                int dp5 = (int) (5 * context.getResources().getDisplayMetrics().density + 0.5f);
                tv.setCompoundDrawablePadding(dp5);

                return v;
            }
        };

        new AlertDialog.Builder(context)
                .setTitle("Share Using")
                .setAdapter(adapter, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int item)
                    {
                        switch (item)
                        {
                            case 0:

                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("message/rfc822");
                                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                                intent.putExtra(Intent.EXTRA_SUBJECT, "Shared via Lecture Verb");
                                intent.putExtra(Intent.EXTRA_TEXT, url + "&share=gp");
                                intent.setPackage("com.google.android.gm");
                                if (intent.resolveActivity(context.getPackageManager()) != null)
                                {
                                    context.startActivity(intent);
                                    saveShareToServer(context,
                                            post_type,
                                            "google",
                                            url
                                            , postid
                                    );
                                } else
                                {
                                    Toast.makeText(context, "Gmail App is not installed", Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 1:

                                Intent share = new Intent(Intent.ACTION_SEND);
                                share.setType("text/plain");
                                share.putExtra(Intent.EXTRA_TEXT, url + "&share=fb");
                                share.setPackage("com.facebook.katana"); //Facebook App package
                                context.startActivity(Intent.createChooser(share, "Share on facebook"));
                                saveShareToServer(context,
                                        post_type,
                                        "facebook",
                                        url
                                        , postid
                                );


                                break;
                            case 2:

                                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                                whatsappIntent.setType("text/plain");
                                whatsappIntent.setPackage("com.whatsapp");
                                whatsappIntent.putExtra(Intent.EXTRA_TEXT, url + "&share=wp");
                                try
                                {
                                    Objects.requireNonNull(context).startActivity(whatsappIntent);
                                    saveShareToServer(context,
                                            post_type,
                                            "whatsapp",
                                            url
                                            , postid
                                    );

                                } catch (android.content.ActivityNotFoundException ex)
                                {
                                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.whatsapp")));
                                }

                                break;
                            case 3:

                                shareTwitter(context, url + "&share=tw");
                                saveShareToServer(context,
                                        post_type,
                                        "twitter",
                                        url
                                        , postid
                                );


                                break;
                        }
                    }
                }).show();
    }

    public static void saveShareToServer(Context context, String share_type, String share_to, String share_url, String lecture_event_id)
    {
        UtillsG.showLoading("Please wait..", context);
        Call<BasicApiModel> call = LVApplication.getRetrofit().create(HomeApis.class).save_social_share(
                new SharedPrefHelper(context).getUserId()
                , share_type
                , share_to
                , share_url
                , lecture_event_id
        );
        call.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
            {
                UtillsG.hideLoading();
                Log.e("SOCIAL SHARE : ", response.body().getStatus() + "");
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t)
            {
                UtillsG.hideLoading();
                Log.e("SOCIAL SHARE ERROR: ", t.toString() + "");
            }
        });
    }

    private static void shareTwitter(Context context, String message)
    {
        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        tweetIntent.putExtra(Intent.EXTRA_TEXT, "This is a Test.");
        tweetIntent.setType("text/plain");

        PackageManager packManager = context.getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        for (ResolveInfo resolveInfo : resolvedInfoList)
        {
            if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android"))
            {
                tweetIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name);
                resolved = true;
                break;
            }
        }
        if (resolved)
        {
            context.startActivity(tweetIntent);
        } else
        {
            Intent i = new Intent();
            i.putExtra(Intent.EXTRA_TEXT, message);
            i.setAction(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://twitter.com/intent/tweet?text=" + urlEncode(message)));
            context.startActivity(i);
            Toast.makeText(context, "Twitter app isn't found", Toast.LENGTH_LONG).show();
        }
    }

    private static String urlEncode(String s)
    {
        try
        {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e)
        {
            Log.wtf(TAG, "UTF-8 should always be supported", e);
            return "";
        }
    }


    public static class Item
    {
        public final String text;
        public final int icon;

        public Item(String text, Integer icon)
        {
            this.text = text;
            this.icon = icon;
        }

        @Override
        public String toString()
        {
            return text;
        }
    }

    public static void shareLinkExternally_LectureEvent(final Context context, final String eventId)
    {
        /*Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, "Shared via Lecture Verb");
        share.putExtra(Intent.EXTRA_TEXT, Web.Path.EXTERNAL_SHARE_URL_EVENT + eventId);
        context.startActivity(Intent.createChooser(share, "Share link!"));*/

        customShareDialog(context, Web.Path.EXTERNAL_SHARE_URL_EVENT + eventId, eventId, "event");
    }


    public static void shareToFacebook(final Activity activity, String url, String title, String pDesc)
    {
        CallbackManager callbackManager = CallbackManager.Factory.create();
        final ShareDialog dailog_share = new ShareDialog(activity);
        dailog_share.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>()
        {
            @Override
            public void onSuccess(Sharer.Result result)
            {
                UtillsG.showToast("Link shared successfully on facebook profile.", activity, true);
            }

            @Override
            public void onCancel()
            {
            }

            @Override
            public void onError(FacebookException error)
            {
            }
        });

        if (ShareDialog.canShow(ShareLinkContent.class))
        {
            ShareLinkContent linkContent = new ShareLinkContent.Builder().setContentTitle(title).setContentDescription(pDesc).setContentUrl(Uri.parse(url)).build();
            dailog_share.show(linkContent);
        }
    }
}