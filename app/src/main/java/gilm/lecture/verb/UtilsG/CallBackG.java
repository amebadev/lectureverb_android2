package gilm.lecture.verb.UtilsG;

/**
 * Created by G-Expo on 15 Mar 2017.
 */
public interface CallBackG<T>
{
    void onCallBack(T output);
}
