package gilm.lecture.verb.UtilsG;

/**
 * Created by G-Expo on 07 Jul 2017.
 */

public class LocationG
{
    private String LocationName;
    private double latitude, longitude;

    public String getLocationName()
    {
        return LocationName;
    }

    public void setLocationName(String locationName)
    {
        LocationName = locationName;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }
}
