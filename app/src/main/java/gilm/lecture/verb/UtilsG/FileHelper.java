package gilm.lecture.verb.UtilsG;

import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

import gilm.lecture.verb.WebServices.Web;

/**
 * Created by G-Expo on 18 Jul 2017.
 */

public class FileHelper {
    private static final FileHelper ourInstance = new FileHelper();

    public static FileHelper getInstance() {

        return ourInstance;
    }

    public FileHelper() {
        initLocalFolders();
    }


    /**
     * intialize folder for audio files
     *
     * @return
     */
    public File initFolderAudio() {
        File folder = new File(Environment.getExternalStorageDirectory() + File.separator + Web.LocalFolders.LectureVerbAudio);
        if (!folder.exists()) {
            folder.mkdir();
        }

        return folder;
    }

    public File createAudioFile(String fileName) {
        File newFile = new File(initFolderAudio(), fileName);

        return newFile;
    }


    /**
     * intialize folder for Images files
     *
     * @return
     */
    private File initFolderImages() {

        File folder = new File(Environment.getExternalStorageDirectory() + File.separator + Web.LocalFolders.LectureVerbImages);
        if (!folder.exists()) {
            folder.mkdir();
        }


        return folder;
    }


    public File createImageFile(String fileName) {
        File newFile = new File(initFolderImages(), fileName);
        try {
            newFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newFile;
    }


    /**
     * intialize folder for video files
     *
     * @return
     */
    private File initFolderVideos() {
        File folder = new File(Environment.getExternalStorageDirectory() + File.separator + Web.LocalFolders.LectureVerbVideos);
        if (!folder.exists()) {
            folder.mkdir();
        }

        return folder;
    }


    public File createVideosFile(String fileName) {
        File newFile = new File(initFolderImages(), fileName);
        return newFile;
    }

    public void initLocalFolders() {
        File folder = new File(Environment.getExternalStorageDirectory() + File.separator + Web.LocalFolders.LectureVerbAudio);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        File folderVideo = new File(Environment.getExternalStorageDirectory() + File.separator + Web.LocalFolders.LectureVerbVideos);
        if (!folderVideo.exists()) {
            folderVideo.mkdirs();
        }
        File folderThumbs = new File(Environment.getExternalStorageDirectory() + File.separator + Web.LocalFolders.LectureVerbThumbs);
        if (!folderThumbs.exists()) {
            folderThumbs.mkdirs();
        }
        File folderImages = new File(Environment.getExternalStorageDirectory() + File.separator + Web.LocalFolders.LectureVerbImages);
        if (!folderImages.exists()) {
            folderImages.mkdirs();
        }
    }

    public String getNewVideoFilePath() {
        File imagesFolder = new File(
                Environment.getExternalStorageDirectory(), Web.LocalFolders.LectureVerbVideos);
        if (!imagesFolder.exists()) {
            imagesFolder.mkdirs();
        }

        File videoFile = new File(imagesFolder, System.currentTimeMillis()
                + ".mp4");
        return videoFile.getAbsolutePath();
    }
}
