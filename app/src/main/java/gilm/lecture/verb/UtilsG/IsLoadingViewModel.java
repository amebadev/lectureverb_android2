package gilm.lecture.verb.UtilsG;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

/**
 * Created by G-Expo on 06 Jul 2017.
 */

public class IsLoadingViewModel extends BaseObservable
{
        private boolean isLoading;

    @Bindable
    public boolean isLoading()
    {
        return isLoading;
    }

    public void setLoading(boolean loading)
    {
        isLoading = loading;
        notifyChange();
    }
}
