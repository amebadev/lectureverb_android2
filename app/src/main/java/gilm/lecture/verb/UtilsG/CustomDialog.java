/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.UtilsG;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import gilm.lecture.verb.R;

/**
 * Created by G-Expo on 24 Jul 2017.
 */

public class CustomDialog
{
    private static final CustomDialog ourInstance = new CustomDialog();

    public static CustomDialog getInstance()
    {
        return ourInstance;
    }

    private CustomDialog()
    {
    }

    public void showAudioHosts(Context context)
    {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_audio_hosts);
        Window                     window = dialog.getWindow();
        WindowManager.LayoutParams wlp    = window.getAttributes();
        wlp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        wlp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        wlp.y = 70; // bottom margin
        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        dialog.show();
    }
}
