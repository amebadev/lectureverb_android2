package gilm.lecture.verb.UtilsG;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import gilm.lecture.verb.Features.Help.HelpApi;
import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Home.ReactedUsers.ReactedUsersActivity;
import gilm.lecture.verb.Features.InterestsSelection.AddInterests.AddInterestCategoriesActivity;
import gilm.lecture.verb.Features.LectureEvent.AttendingLecture;
import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;
import gilm.lecture.verb.Features.QuickLecture.Presentation.SelectPPT.PPTSelector;
import gilm.lecture.verb.Features.WebHistory.WebHistoryInnerAdapter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.Widget.SquareCardView;
import me.nereo.multi_image_selector.MultiImageSelector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 03 May 2017.
 */

public class DialogHelper
{
    private static final DialogHelper ourInstance = new DialogHelper();

    public static DialogHelper getInstance()
    {
        return ourInstance;
    }

    private DialogHelper()
    {
    }

    public void reportAbuse(Context context, String title, final CallBackG<String> callBackG)
    {
        final CharSequence[] reportMessage = {"Spam", "Fake Account", "Inappropriate content", "Other"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setItems(reportMessage, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, final int item)
            {
                callBackG.onCallBack(reportMessage[item].toString());
            }
        });
        builder.show();
    }

    public void showInformation(Context context, String message, final CallBackG<String> callBackG)
    {
        showInformation(context, context.getResources().getString(R.string.app_name), message, callBackG);
    }

    public void showInformation(Context context, String title, String message, final CallBackG<String> callBackG)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                if (callBackG != null)
                {
                    callBackG.onCallBack("");
                }
            }
        });

        AlertDialog dialog = builder.create();
//        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }

    public void showWithAction(Context context, String message, final CallBackG<String> callBackG)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.app_name));
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                callBackG.onCallBack("");
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    public void showWith2Action(Context context, String positiveAction, String negativeActionText, String title, String message, final CallBackG<String> callBackG)
    {
        try
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setCancelable(false);
            builder.setPositiveButton(positiveAction, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialogInterface, int i)
                {
                    callBackG.onCallBack("");
                }
            });
            builder.setNegativeButton(negativeActionText, null);
            builder.show();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void showDeleteMenu(Context context, final View anchor, String option, final CallBackG<Boolean> callBackG)
    {
        final PopupMenu popup = new PopupMenu(context, anchor);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.menu_delete, popup.getMenu());

        popup.getMenu().findItem(R.id.delete).setTitle(option);
        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            public boolean onMenuItemClick(MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.cancel:
                        popup.dismiss();
                        break;

                    case R.id.delete:
                        callBackG.onCallBack(true);
                        break;

                }
                return true;
            }
        });

        popup.show(); //showing popup menu
    }

    public void showLectureMoreOptions(Context context, final View anchor, Boolean isReposted, Boolean showDeleteOption, final CallBackG<Integer> callBackG)
    {
        final PopupMenu popup = new PopupMenu(context, anchor);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.menu_lecture_more_options, popup.getMenu());


        if (isReposted)
        {
            popup.getMenu().getItem(4).setTitle("Un Repost");
        } else
        {
            popup.getMenu().getItem(4).setTitle("Repost");
        }
        popup.getMenu().getItem(7).setVisible(showDeleteOption);


        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            public boolean onMenuItemClick(MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.itemPlay:
                        callBackG.onCallBack(1);
                        break;
                    case R.id.itemFavorite:
                        callBackG.onCallBack(2);
                        break;
                    case R.id.itemConnect:
                        callBackG.onCallBack(3);
                        break;
                    case R.id.itemComment:
                        callBackG.onCallBack(4);
                        break;
                    case R.id.itemRepost:
                        callBackG.onCallBack(5);
                        break;
                    case R.id.browserWebLink:
                        callBackG.onCallBack(6);
                        break;
                    case R.id.shareLecture:
                        callBackG.onCallBack(7);
                        break;
                    case R.id.deleteLecture:
                        callBackG.onCallBack(8);
                        break;


                }
                return true;
            }
        });

        popup.show(); //showing popup menu
    }

    public void showAttendentMenu(Context context, final View anchor, final CallBackG<String> callBackG)
    {
        final PopupMenu popup = new PopupMenu(context, anchor);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.menu_attendent, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            public boolean onMenuItemClick(MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.digital:
                        callBackG.onCallBack(AttendingLecture.GOING_DIGITAL);
                        break;

                    case R.id.attendening:
                        callBackG.onCallBack(AttendingLecture.ATTENDING);
                        break;

                }
                return true;
            }
        });

        popup.show(); //showing popup menu
    }

    public void showInterestedMenu(Context context, final View anchor, final CallBackG<String> callBackG)
    {
        final PopupMenu popup = new PopupMenu(context, anchor);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.menu_interested, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            public boolean onMenuItemClick(MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.isInterested:
                        callBackG.onCallBack(AttendingLecture.IS_INTERESTED);
                        break;

                    case R.id.isIgnored:
                        callBackG.onCallBack(AttendingLecture.IS_IGNORED);
                        break;

                }
                return true;
            }
        });

        popup.show(); //showing popup menu
    }


    public void showHomeAdapterMenu(Context context, final View anchor, final CallBackG<Integer> callBackG)
    {
        final PopupMenu popup = new PopupMenu(context, anchor);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.menu_home_adapter_more, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            public boolean onMenuItemClick(MenuItem item)
            {
                callBackG.onCallBack(item.getItemId());

                return true;
            }
        });

        popup.show(); //showing popup menu
    }

    public void shareEventDialog(final LectureEventViewModel viewModel, final Context context, final String data, final CallBackG<Boolean> callBackG)
    {
        final CharSequence[] items = {"Share Internally", "Share using different application", "Share on your profile",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Share");
        builder.setItems(items, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int item)
            {
                switch (item)
                {
                    case 0:
                        callBackG.onCallBack(false);
                        break;
                    case 1:
                        UtillsG.shareLinkExternally_LectureEvent(context, viewModel.getmLectureEvent().getEvent_id());

                        Call<BasicApiModel> shareExternally = LVApplication.getRetrofit().create(HomeApis.class).save_external_share("1",
                                viewModel.getmLectureEvent().getEvent_id(),
                                new SharedPrefHelper(context).getUserId(),
                                viewModel.getmLectureEvent().getUser_id(),
                                viewModel.getmLectureEvent().getUser_id().equals(new SharedPrefHelper(context).getUserId())
                                        ? " You " : new SharedPrefHelper(context).getUserName(),
                                viewModel.getTitle());
                        shareExternally.enqueue(new Callback<BasicApiModel>() {
                            @Override
                            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {

                            }

                            @Override
                            public void onFailure(Call<BasicApiModel> call, Throwable t) {

                            }
                        });

//                        UtillsG.shareIntent(context, data);
                        break;
                    case 2:
                        callBackG.onCallBack(true);
                        break;
                    case 3:
                        dialog.dismiss();
                        break;

                }
            }
        });
        builder.show();
    }

    public static void inviteConnectOptions(final Context context)
    {
        final CharSequence[] items = {"Email or Contacts", "Other apps",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Invite Options");
        builder.setItems(items, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int item)
            {
                switch (item)
                {
                    case 0:
                        UtillsG.sendFireBaseInvite(context);
                        break;
                    case 1:
                        UtillsG.InviteByOtherApp(context);
                        break;
                    case 2:
                        dialog.dismiss();
                        break;

                }
            }
        });
        builder.show();
    }


    public void logOutDialog(Context context, CallBackG callBackG)
    {
        DialogHelper.getInstance().showWith2Action(context, "Log out", "Cancel", "Are you sure ?", "You want to logout from Lecture Verb.", callBackG);
    }

    public void deleteAccountDialog(Context context, CallBackG callBackG)
    {
        DialogHelper.getInstance().showWith2Action(context, "Confirm Deletion", "Cancel", "Are you sure ?", "By deleting your account, the associated data will also be lost. Are you sure that you want to delete your Lecture Verb account?", callBackG);
    }

    public void exitDialog(Context context, CallBackG callBackG)
    {
        DialogHelper.getInstance().showWith2Action(context, "Exit", "Cancel", "Are you sure ?", "You want to exit from Lecture Verb.", callBackG);
    }

    public void showWelcomeDialog(final Context con, CallBackSingleInterface callBackSingleInterface)
    {
        final Dialog dialog = new Dialog(con);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_welcome_notice);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView textView = (TextView) dialog.findViewById(R.id.textView);
        TextView txtvTitle = (TextView) dialog.findViewById(R.id.txtvTitle);
        UtillsG.setTextSizeByPercentage(con, textView, 6f);
        UtillsG.setTextSizeByPercentage(con, txtvTitle, 7f);

        textView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener()
        {
            @Override
            public void onDismiss(DialogInterface dialog)
            {
                if (!new SharedPrefHelper(con).isInterestAlreadySet(con))
                {
//                    InterestsSelectionActivity.start(con, "What's Your Interest's");
                    AddInterestCategoriesActivity.start(con, false, "", "");
                }
            }
        });
        callBackSingleInterface.onCompletion();
    }

    public void showReportDialog(final Context con)
    {
        final Dialog dialog = new Dialog(con);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_report_help);

        final EditText edMessage = dialog.findViewById(R.id.edMessage);
        final TextView txtvSubmit = dialog.findViewById(R.id.txtvSubmit);

        txtvSubmit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(final View view)
            {
                if (edMessage.getText().toString().isEmpty() || edMessage.getText().toString().length() < 10)
                {
                    UtillsG.showToast("Please enter a detailed message.", con, true);
                } else
                {
                    UtillsG.showLoading("Please wait...", con);
                    LVApplication.getRetrofit().create(HelpApi.class).send_report_email(new SharedPrefHelper(con).getUserId(), edMessage.getText().toString()).enqueue(new Callback<BasicApiModel>()
                    {
                        @Override
                        public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
                        {
                            UtillsG.hideKeyboard(con, view);
                            UtillsG.showToast(response.body().getMessage(), con, true);
                            UtillsG.hideLoading();
                            if (response.body().getStatus())
                            {
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(Call<BasicApiModel> call, Throwable t)
                        {
                            UtillsG.showToast("An error occured : " + t.getMessage(), con, true);
                            UtillsG.hideLoading();
                        }
                    });
                }
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener()
        {
            @Override
            public void onDismiss(DialogInterface dialog)
            {
                UtillsG.hideKeyboard(con, txtvSubmit);
            }
        });


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void playAudio(Activity con, String filePath)
    {
        // initializing custom dialog
        final Dialog dialog = new Dialog(con);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_play_audio);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        dialog.show();
        final SeekBar seekBAr = (SeekBar) dialog.findViewById(R.id.seekBAr);

        final MediaPlayer mPlayer = new MediaPlayer();
        try
        {
            // Playing audio using filePath
            Uri myUri1 = Uri.parse(filePath);
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mPlayer.setDataSource(con, myUri1);
            mPlayer.prepare();
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
                @Override
                public void onCompletion(MediaPlayer mp)
                {
                    dialog.dismiss();
                }
            });
            mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
            {
                @Override
                public void onPrepared(MediaPlayer mp)
                {
                    mPlayer.start();
                    seekBAr.setMax(mp.getDuration() / 200);
                    seekBAr.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
                    {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
                        {

                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar)
                        {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar)
                        {
                            mPlayer.seekTo(seekBar.getProgress() * 200);
                        }
                    });
                }
            });

        } catch (IOException e)
        {
            e.printStackTrace();
        }

        // ondismiss listener is being used to stop audio
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener()
        {
            @Override
            public void onDismiss(DialogInterface dialog)
            {
                if (mPlayer != null && mPlayer.isPlaying())
                {
                    mPlayer.stop();
                }
            }
        });

        // Handler is being used to show progress of audio in seekbar
        final Handler mHandler = new Handler();
        //necessary to update Seekbar on UI thread only
        con.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                if (mPlayer != null)
                {
                    int mCurrentPosition = mPlayer.getCurrentPosition() / 200;
                    seekBAr.setProgress(mCurrentPosition);
                }
                mHandler.postDelayed(this, 200);
            }
        });

        seekBAr.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
                if (mPlayer != null && fromUser)
                {
                    mPlayer.seekTo(progress * 1000);
                }
            }
        });

    }


    public void showPresentationOptions(final Context con)
    {
        final Dialog dialog = new Dialog(con);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_build_upload_presentation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(con.getResources().getColor(R.color.dialogBackgroundColor)));

        SquareCardView layoutRecordAudio = (SquareCardView) dialog.findViewById(R.id.layoutRecordAudio);
        SquareCardView layoutRecordPPT = (SquareCardView) dialog.findViewById(R.id.layoutRecordPPT);

        layoutRecordPPT.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    PPTSelector.start(con);
                    dialog.dismiss();
                } else
                {
                    UtillsG.showToast("Your device does not support this feature.", con, true);
                }
            }
        });
        layoutRecordAudio.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                MultiImageSelector.create(con)
                        .count(10)
                        .showCamera(false)
                        .start(((Activity) con), Constants.RequestCode.MULTIPLE_IMAGE_PICKER);

//                UtillsG.showToast("Functionality under progress.", con, true);
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    public void showLectureOptionsOnPlayer(boolean isvideo, Context context, final View anchor, boolean isFavourite,
                                           boolean isLecturePlaying, Lecture_And_Event_Model mLectureEventModel, final CallBackG<Integer> callBackG)
    {
        final PopupMenu popup = new PopupMenu(context, anchor);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.menu_quick_lecture_details_options, popup.getMenu());

        MenuItem itemPlayPause = popup.getMenu().findItem(R.id.menu_start_lecture);
        itemPlayPause.setTitle(isLecturePlaying ? "Stop Lecture" : "Start Lecture");
        itemPlayPause.setVisible(isvideo ? false : true);

        MenuItem itemFavourite = popup.getMenu().findItem(R.id.menu_add_to_playlist);
        itemFavourite.setTitle(isFavourite ? "Remove From Favorite" : "Add To Favorite");

        if (mLectureEventModel.getIs_reposted())
        {
            popup.getMenu().findItem(R.id.menu_unpost).setTitle("Un Repost");
        } else
        {
            popup.getMenu().findItem(R.id.menu_unpost).setTitle("Repost");
        }


        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            public boolean onMenuItemClick(MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.menu_start_lecture:
                        callBackG.onCallBack(0);
                        break;
                    case R.id.menu_add_to_playlist:
                        callBackG.onCallBack(1);
                        break;
                    case R.id.menu_share:
                        callBackG.onCallBack(2);
                        break;
                    case R.id.menu_unpost:
                        callBackG.onCallBack(3);
                        break;
                    case R.id.menu_comment:
                        callBackG.onCallBack(4);
                        break;
                    case R.id.menu_details:
                        callBackG.onCallBack(5);
                        break;

                }
                return true;
            }
        });

        popup.show(); //showing popup menu
    }


    //TODO : show lecture detailils
    public void showLectureDetails(final Context con, final Lecture_And_Event_Model mLectureModel, final View viewToPerformClick)
    {
        final Dialog dialog = new Dialog(con);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_lecture_info);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(con.getResources().getColor(R.color.dialogBackgroundColor)));

        ((TextView) dialog.findViewById(R.id.txtv_lectureTitle)).setText(mLectureModel.getQuick_lecture_text());
        ((TextView) dialog.findViewById(R.id.txtv_lectureViews)).setText(mLectureModel.getLecture_view());
        ((TextView) dialog.findViewById(R.id.txtv_lectureFav)).setText(mLectureModel.getFavourites_count());
        ((TextView) dialog.findViewById(R.id.txtv_lectureRepost)).setText(mLectureModel.getReposted_count());
        ((TextView) dialog.findViewById(R.id.txtv_lectureComments)).setText(
                mLectureModel.getComments_count() + " comments on this lecture");

        ((TextView) dialog.findViewById(R.id.txtv_lectureViews)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ReactedUsersActivity.start(con, mLectureModel.getQuick_lecture_id(), 0);
            }
        });
        ((TextView) dialog.findViewById(R.id.txtv_lectureFav)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ReactedUsersActivity.start(con, mLectureModel.getQuick_lecture_id(), 1);
            }
        });
        (dialog.findViewById(R.id.llComments)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ReactedUsersActivity.start(con, mLectureModel.getQuick_lecture_id(), 2);
            }
        });
        ((TextView) dialog.findViewById(R.id.txtv_lectureRepost)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ReactedUsersActivity.start(con, mLectureModel.getQuick_lecture_id(), 3);
            }
        });

        //Setting time according to the conditions
        if (mLectureModel.getIs_reposted()
                && mLectureModel.getUserdata().getUser_id().equals(new SharedPrefHelper(con).getUserId()))
        {
            ((TextView) dialog.findViewById(R.id.txtv_lectureTime)).setText("Posted " + UtillsG.get_time_ago(mLectureModel.getDate_time()));
        } else if ((mLectureModel.getOwnerUserdata() != null
                && mLectureModel.getUserdata().getUser_id().
                equals(mLectureModel.getOwnerUserdata().getUser_id()))
                || mLectureModel.getOwnerUserdata() == null
                )
        {
            ((TextView) dialog.findViewById(R.id.txtv_lectureTime)).setText("Posted " + UtillsG.get_time_ago(mLectureModel.getDate_time()));
        } else
        {
            ((TextView) dialog.findViewById(R.id.txtv_lectureTime)).setText("Posted " + UtillsG.get_time_ago(mLectureModel.getReposted_date_time()));
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    public interface CallBackSingleInterface
    {
        public void onCompletion();
    }

    public static void showInnerStackDialog(Context context, ArrayList<WebInnerStackModel> data)
    {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_inner_history);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.dialogBackgroundColor)));

        RecyclerView recyHistory = (RecyclerView) dialog.findViewById(R.id.recyHistory);
        ImageView imgvClose = (ImageView) dialog.findViewById(R.id.imgvClose);

        WebHistoryInnerAdapter adapter = new WebHistoryInnerAdapter(data, context);

        adapter.setShouldLoadMore(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyHistory.setLayoutManager(linearLayoutManager);
        recyHistory.setAdapter(adapter);

        imgvClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    public void executeDeleteLecture(final Lecture_And_Event_Model alData, final Context context, final CallBackG<Boolean> callback)
    {
        if (alData.getUser_id().equalsIgnoreCase(new SharedPrefHelper(context).getUserId()))
        {
            DialogHelper.getInstance().showWith2Action(context, "Delete", "Cancel", context.getResources().getString(R.string.app_name), "Are you sure that you want to delete this Post?", new CallBackG<String>()
            {
                @Override
                public void onCallBack(String output)
                {
                    UtillsG.showLoading("Please wait..", context);
                    Call<BasicApiModel> shareExternally = LVApplication.getRetrofit().create(HomeApis.class).
                            delete_lecture(alData.getQuick_lecture_id());
                    shareExternally.enqueue(new Callback<BasicApiModel>()
                    {
                        @Override
                        public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
                        {
                            UtillsG.hideLoading();
                            Log.e("delete lecture", "---------------------------------");
                            callback.onCallBack(response.body() != null && response.body().getStatus());

                            UtillsG.showToast(response.body().getMessage(), context, true);
                        }

                        @Override
                        public void onFailure(Call<BasicApiModel> call, Throwable t)
                        {
                            UtillsG.hideLoading();
                            Log.e("delete lecture failed", "---------------------------------");
                            UtillsG.showToast(t.toString() + "", context, true);
                        }
                    });
                }
            });
        }
    }


}
