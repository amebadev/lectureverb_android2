package gilm.lecture.verb.Features.Search;

import gilm.lecture.verb.WebServices.Web;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * created by PARAMBIR SINGH on 28/9/17.
 */

public interface SearchApi
{
    /**
     * This api is developed without pagination because we need 3 arrays in the response and only need a limited data . Means max 50 lectures will be returned in response. Retrofit can handle this much data easily.
     *
     * @param user_id
     * @return
     */
    @GET(Web.Path.get_suggested_lectures)
    Call<SuggestedLectureModel> get_suggested_lectures(@Query(Web.Keys.USER_ID) String user_id);
}
