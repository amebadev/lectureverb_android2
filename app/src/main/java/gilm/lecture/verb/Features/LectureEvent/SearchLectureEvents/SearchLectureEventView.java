package gilm.lecture.verb.Features.LectureEvent.SearchLectureEvents;

import android.view.View;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;
import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;

/**
 * Created by harpreet on 9/26/17.
 */

public interface SearchLectureEventView extends Viewable<SearchLectureEventPresenter> {

    void showData(ArrayList<LectureEventViewModel> list, int pageNo);
}