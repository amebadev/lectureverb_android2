package gilm.lecture.verb.Features.PaypalPayment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import java.math.BigDecimal;

import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;

public class PaymentActivity extends AppCompatActivity
{

    private String TAG = "PAYMENT ACTIVITY";
    String amount = "";
    String paymentTitle = "";

    public static void start(Context context,String amount,String paymentTitle) {
        Intent starter = new Intent(context, PaymentActivity.class);
        starter.putExtra("amount",amount);
        starter.putExtra("paymentTitle",paymentTitle);
        ((Activity) context).startActivityForResult(starter, Constants.RequestCode.PAYPAL_PAYMENT);
    }

    String SANDBOX_CLIENT_ID = "AZcmkEuxKfbSsaY338eCxEi-A6lI-5-LnHO74kuGJS1GVg8xN9NBg7oV8WjthFZIxXQ1p4FkzHzA1A6h";

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_NO_NETWORK)
            .clientId("AZcmkEuxKfbSsaY338eCxEi-A6lI-5-LnHO74kuGJS1GVg8xN9NBg7oV8WjthFZIxXQ1p4FkzHzA1A6h")
            .merchantName("Lecture Verb");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        amount = getIntent().getStringExtra("amount");
        paymentTitle = getIntent().getStringExtra("paymentTitle");
        PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(amount), "USD", paymentTitle,
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(PaymentActivity.this, com.paypal.android.sdk.payments.PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intent, Constants.RequestCode.PAYPAL_PAYMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.RequestCode.PAYPAL_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Intent inn = new Intent();
                        inn.putExtra("isSuccess", true);
                        setResult(RESULT_OK, inn);
                        finish();
                    }
                    catch (Exception e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED) {
                Log.e(TAG, "The user canceled.");

                Intent inn = new Intent();
                inn.putExtra("isSuccess", false);
                setResult(RESULT_OK, inn);
                finish();
            }
            else if (resultCode == com.paypal.android.sdk.payments.PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.e(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");

                Intent inn = new Intent();
                inn.putExtra("isSuccess", false);
                setResult(RESULT_OK, inn);
                finish();
            }
        }
    }

}
