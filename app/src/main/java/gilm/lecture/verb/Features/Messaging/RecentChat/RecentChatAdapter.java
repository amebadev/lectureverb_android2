package gilm.lecture.verb.Features.Messaging.RecentChat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.Messaging.Chat.ChatActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.Web;
import gilm.lecture.verb.Widget.TextViewWithImages;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

public class RecentChatAdapter extends RecyclerView.Adapter<RecentChatAdapter.RecentChatsViewHolder>
{
    private LayoutInflater inflater;

    private List<ListOfRecentChatsModel.RecentChatsModel> alLectureModels;
    LectureEventClicks mClicks = new LectureEventClicks();
    Context context;

    public RecentChatAdapter(List<ListOfRecentChatsModel.RecentChatsModel> dList, Context context) {
        this.alLectureModels = dList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecentChatsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecentChatsViewHolder(inflater.inflate(R.layout.inflator_recent_chat, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecentChatsViewHolder holder, final int position) {
        ImageLoader.setImageRoundSmall(holder.imgvUserImage, UtillsG.getNotNullString(alLectureModels.get(position).getUserData().getProfile_pic(), ""));

        holder.txtvUserName.setText(UtillsG.getNotNullString(alLectureModels.get(position).getUserData().getFull_name(), ""));
        holder.txtvDateTime.setText(UtillsG.get_time_ago(alLectureModels.get(position).getTime()));
        holder.txtvLastMessage.setText(alLectureModels.get(position).getMessage().trim().isEmpty()
                ? alLectureModels.get(position).getMessage_type()
                : UtillsG.decodeEmoji(alLectureModels.get(position).getMessage()));

        if(Integer.parseInt(alLectureModels.get(position).getUnread_count()) == 0) {
            holder.txtUnreadCount.setVisibility(View.GONE);
        }
        else{
            holder.txtUnreadCount.setVisibility(View.VISIBLE);
        }

        holder.txtUnreadCount.setText(alLectureModels.get(position).getUnread_count());

        // handling blocking and unblocking checks
        if (alLectureModels.get(position).getUserData().getIs_blocked() != null) {

            if (alLectureModels.get(position).getUserData().getIs_blocked().equals(Web.BlockUserStates.BlockByMe)) {
                holder.ll_recent.setBackgroundColor(context.getResources().getColor(R.color.offwhite));
                holder.txtv_blockUserMessage.setText("You have  [img src=ic_block_user/]  blocked this user to send message");
                holder.txtvLastMessage.setVisibility(View.GONE);
                holder.txtv_blockUserMessage.setVisibility(View.VISIBLE);
            }
            else if (alLectureModels.get(position).getUserData().getIs_blocked().equals(Web.BlockUserStates.BlockByOther)) {
                holder.ll_recent.setBackgroundColor(context.getResources().getColor(R.color.offwhite));
                holder.txtv_blockUserMessage.setText("This user [img src=ic_block_user/]  blocks you to send message");
                holder.txtvLastMessage.setVisibility(View.GONE);
                holder.txtv_blockUserMessage.setVisibility(View.VISIBLE);
            }
            else if (alLectureModels.get(position).getUserData().getIs_blocked().equals(Web.BlockUserStates.Noblock)) {
                holder.ll_recent.setBackgroundColor(context.getResources().getColor(R.color.white));
                holder.txtv_blockUserMessage.setVisibility(View.GONE);
                holder.txtvLastMessage.setVisibility(View.VISIBLE);
            }
        }
        else {
            holder.txtv_blockUserMessage.setVisibility(View.GONE);
            holder.txtvLastMessage.setVisibility(View.VISIBLE);
            holder.ll_recent.setBackgroundColor(context.getResources().getColor(R.color.white));
        }


        holder.view.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                ChatActivity.start(context, holder.imgvUserImage, alLectureModels.get(position).getUserData());
            }
        });
    }


    @Override
    public int getItemCount() {
        return alLectureModels.size();
    }

    class RecentChatsViewHolder extends RecyclerView.ViewHolder
    {
        View view;
        ImageView imgvUserImage;
        TextView txtvUserName, txtvDateTime, txtUnreadCount;
        EmojiconTextView txtvLastMessage;
        TextViewWithImages txtv_blockUserMessage;
        LinearLayout ll_recent;


        public RecentChatsViewHolder(View row) {
            super(row);

            view = row;
            imgvUserImage = (ImageView) view.findViewById(R.id.imgvUserImage);
            txtUnreadCount = (TextView) view.findViewById(R.id.txtUnreadCount);
            txtvUserName = (TextView) view.findViewById(R.id.txtvUserName);
            txtvDateTime = (TextView) view.findViewById(R.id.txtvDateTime);
            txtvLastMessage = (EmojiconTextView) view.findViewById(R.id.txtvLastMessage);
            txtv_blockUserMessage = (TextViewWithImages) view.findViewById(R.id.txtv_blockUserMessage);
            ll_recent = (LinearLayout) view.findViewById(R.id.ll_recent);
        }
    }
}