package gilm.lecture.verb.Features.LectureEvent.RecordAudioVideoEvent.MergerModule.AudioRecorder;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.text.Html;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.Internal.Base.EmptyPresenter;
import gilm.lecture.verb.Features.Internal.Base.EmptyView;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.Features.LectureEvent.RecordAudioVideoEvent.AudioRecorderTool;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureApi;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.FileHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import gilm.lecture.verb.databinding.ActivityAudioRecorderBinding;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AudioRecorderActivity extends BaseActivity<ActivityAudioRecorderBinding, EmptyPresenter> implements EmptyView, ProgressRequestBody.UploadCallbacks {

    LectureDetailsModel eventModel;
    private DatabaseReference hostReference;
    private DatabaseReference lecturerRef;
    private boolean isHostReady;

    private String audioFilePath = "";
    AudioRecorderTool audioRecorderTool;

    boolean isRecording;
    private PowerManager.WakeLock wl;

    /*THIS ACTIVITY IS COMMUNICATING WITH VIDEO RECORDER ACTIVITY IN THE SAME PACKGE - USING FIREBASE DATABASE*/
    public static void start(Context context, LectureDetailsModel eventModel) {
        Intent starter = new Intent(context, AudioRecorderActivity.class);
        starter.putExtra(Constants.Extras.DATA, eventModel);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_audio_recorder;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new EmptyPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        wl.acquire();

        eventModel = (LectureDetailsModel) getIntent().getSerializableExtra(Constants.Extras.DATA);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        audioFilePath = FileHelper.getInstance().createAudioFile("prm" + System.currentTimeMillis() + ".m4a").getAbsolutePath();
        audioRecorderTool = new AudioRecorderTool(audioFilePath);

        implementFirebase();

//        getDataBinder().imgvMyStatus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                stopRecording();
//            }
//        });
    }

    @Override
    public Context getActivityG() {
        return AudioRecorderActivity.this;
    }

    private void implementFirebase() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        lecturerRef = database.getReference().child("event").child(eventModel.getEvent_id()).child("lecturer");

        setMeReady();

        // Read from the database

        database.getReference().child("event").child(eventModel.getEvent_id()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild("host")) {
                    hostReference = database.getReference().child("event").child(eventModel.getEvent_id()).child("host");
                    hostReference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String state = (String) dataSnapshot.getValue();

                            if (state.equalsIgnoreCase("ready")) {
                                getDataBinder().imgvHostStatus.setImageResource(R.drawable.ic_dot_greem);
                                getDataBinder().txtvHostStatus.setText(Html.fromHtml("<b>Facilitator : </b>" + "Ready"));
                                isHostReady = true;
                            } else if (state.equalsIgnoreCase("notReady")) {
                                getDataBinder().imgvHostStatus.setImageResource(R.drawable.ic_dot_grey);
                                getDataBinder().txtvHostStatus.setText(Html.fromHtml("<b>Facilitator : </b>" + "Not Ready"));
                                isHostReady = false;
                            } else if (state.equalsIgnoreCase("recording")) {
                                getDataBinder().imgvHostStatus.setImageResource(R.drawable.ic_dot_greem);
                                getDataBinder().txtvHostStatus.setText(Html.fromHtml("<b>Facilitator : </b>" + "Recording Video"));
                                isHostReady = true;

                                EventBus.getDefault().post(state);

                            } else if (state.equalsIgnoreCase("recordingStopped")) {
                                getDataBinder().imgvHostStatus.setImageResource(R.drawable.ic_dot_greem);
                                getDataBinder().txtvHostStatus.setText(Html.fromHtml("<b>Facilitator : </b>" + "Recording Stopped"));
                                isHostReady = true;

                                EventBus.getDefault().post(state);
                            } else if (state.equalsIgnoreCase("cancelled")) {
                                getDataBinder().imgvHostStatus.setImageResource(R.drawable.ic_dot_greem);
                                getDataBinder().txtvHostStatus.setText(Html.fromHtml("<b>Facilitator : </b>" + "Recording Cancelled"));
                                isHostReady = true;

                                EventBus.getDefault().post(state);
                            }

                            Log.e("DATA CHANGED : " + state + ":", dataSnapshot.getValue() + "");
                        }

                        @Override
                        public void onCancelled(DatabaseError error) {
                            // Failed to read value
                            Log.e("FIREBASE DB", "Failed to read value.", error.toException());
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void startRecording() {
        try {
            audioRecorderTool.start();
            getDataBinder().txtvMyStatus.setText("Status : Recording Now");

            UtillsG.showToast("Recording Started.", getActivityG(), true);
            isRecording = true;

            setMeRecording();
        } catch (IOException e) {
            UtillsG.showToast("Error while starting recording.", getActivityG(), true);
        }
    }

    private void stopRecording() {
        try {
            audioRecorderTool.stop();
            getDataBinder().txtvMyStatus.setText("Status : Recording Stopped");
            UtillsG.showToast("Recording Stopped.", getActivityG(), true);
            isRecording = false;
        } catch (Exception e) {
            UtillsG.showToast("Error while stop recording.", getActivityG(), true);
        }
    }

    private void setMeReady() {
        lecturerRef.setValue("ready");
    }

    private void setMeRecording() {
        lecturerRef.setValue("recording");
    }

    private void setMeNotReady() {
        lecturerRef.setValue("notReady");
    }

    private void setMeUploading() {
        lecturerRef.setValue("uploading");
    }

    private void setMeUploaded() {
        lecturerRef.setValue("uploaded");
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }

        setMeNotReady();
        stopRecording();
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void RefreshData(String data) {
        if (data.equalsIgnoreCase("recording")) {
            startRecording();
        } else if (data.equalsIgnoreCase("recordingStopped")) {
            stopRecording();
            if (!isUploading) {
                uploadVideoLecture();
            }
        } else if (data.equalsIgnoreCase("cancelled")) {
            UtillsG.showToast("Recording Cancelled.", getActivityG(), true);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (isRecording) {
            DialogHelper.getInstance().showInformation(getActivityG(), "Your device is currently recording audio.", new CallBackG<String>() {
                @Override
                public void onCallBack(String output) {

                }
            });
        } else {
            super.onBackPressed();
        }
    }

    boolean isUploading;

    private void uploadVideoLecture() {
        isUploading = true;
        setMeUploading();
        showLoading("Please wait while uploading your recording ...");
        Call<QuickLectureModel> quickLectureApiModelCall =
                LVApplication.getRetrofit().create(QuickLectureApi.class).uploadPreRecordedLecture(
                        getFilePart(new File(audioFilePath), "audio"),
                        getOtherParams("audio"),
                        getOtherParams(eventModel.getUser_id()),
                        getOtherParams(eventModel.getLecture_title()),
                        getOtherParams(""),
                        getOtherParams("Public")
                        , getOtherParams("")
                        , getOtherParams(eventModel.getUserdata().getFull_name())
                        , getOtherParams("")
                        , getOtherParams(eventModel.getLecture_poster())
                        , getOtherParams("0")
                        , getOtherParams(eventModel.getEvent_id())
                );
        quickLectureApiModelCall.enqueue(new Callback<QuickLectureModel>() {
            @Override
            public void onResponse(Call<QuickLectureModel> call, final Response<QuickLectureModel> response) {

                if (response.body() != null) {

                    if (response.body().getStatus()) {
                        setMeUploaded();
                        UtillsG.showToast("Uploaded successfully.", getActivityG(), true);
                        finish();

                        /*Call<ThumbnailResponseModel> lectureThumbnailApiModelCall
                                = LVApplication.getRetrofit().create(QuickLectureApi.class).uploadLectureThumbnail(
                                getFilePart(asdf, "thumbnail"),
                                getOtherParams(response.body().getmQuickLectureData().getQuick_lecture_id()),
                                getUploadTypePart());
                        lectureThumbnailApiModelCall.enqueue(new Callback<ThumbnailResponseModel>() {
                            @Override
                            public void onResponse(Call<ThumbnailResponseModel> thumbnailApiCall, Response<ThumbnailResponseModel> thumbnailApiResponse) {
                                hideLoading();

                                if (thumbnailApiResponse.body() != null) {

                                    if (thumbnailApiResponse.body().getStatus()) {
                                        updateLectureEventInterest(response.body().getmQuickLectureData().getQuick_lecture_id());

                                    } else {
                                        UtillsG.showToast("Error in creating quick lecture", context, true);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ThumbnailResponseModel> thumbnailApiCall, Throwable t) {
                                hideLoading();
                                Log.e("Error", t.toString());
                            }
                        });*/
                    } else {
                        hideLoading();
                        UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                    }
                } else {
                    hideLoading();
                    UtillsG.showToast("An error occured while uploading.", getActivityG(), true);
                }
            }

            @Override
            public void onFailure(Call<QuickLectureModel> call, Throwable t) {
                Log.e("Error", t.toString());
                UtillsG.showToast("Error in creating quick lecture = " + t.toString(), getActivityG(), true);
                hideLoading();
            }
        });
    }


    private MultipartBody.Part getFilePart(final File file, String fileType) {
        ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(fileType, file.getName(), fileBody);
        return filePart;
    }

    private RequestBody getUploadTypePart() {
        return RequestBody.create(MediaType.parse("text/plain"), "upload");
    }


    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
