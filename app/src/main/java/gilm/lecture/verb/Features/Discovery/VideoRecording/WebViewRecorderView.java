package gilm.lecture.verb.Features.Discovery.VideoRecording;

import android.content.Intent;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * created by PARAMBIR SINGH on 19/8/17.
 */

public interface WebViewRecorderView extends Viewable<WebViewRecorderPresenter>
{

    MediaRecorder getRecorder();

    MediaProjection getMediaProjection();

    void setMediaProjectionNull();

    MediaProjection.Callback getMediaProjectionCallBack();

    void initMediaProjectionCallBack(int resultCode, Intent data);

    void shareScreen();

    void stopScreenSharing();

    void hideRecordingButton();

    void startTimer();

    void hideNotificationBar();

    void setVideoPath(String videoPath);

    void finishActivity();
}
