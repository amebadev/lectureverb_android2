package gilm.lecture.verb.Features.LectureEvent.SendInvitationViaApp;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.CoHostListModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.InfiniteAdapter_WithoutBuinding;
import gilm.lecture.verb.UtilsG.LoadingViewHolder;

/**
 * Created by harpreet on 9/7/17.
 */

public class UserDetailsAdapter extends InfiniteAdapter_WithoutBuinding<UserDetailsAdapter.MyViewHolderG> {
    private LayoutInflater inflater;

    Activity mActivity;
    ArrayList<CoHostListModel.SingleCoHostData> mCoHostList;
    ValueFilter valueFilter;

    public UserDetailsAdapter(Activity mActivity, ArrayList<CoHostListModel.SingleCoHostData> mCoHostList) {
        this.mActivity = mActivity;
        inflater = LayoutInflater.from(mActivity);
        this.mCoHostList=mCoHostList;
    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        return new LoadingViewHolder(inflater.inflate(R.layout.layout_progress, parent, false));
    }

    @Override
    public int getCount() {
        return mCoHostList.size();
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }


    @Override
    public UserDetailsAdapter.MyViewHolderG onCreateView(ViewGroup parent, int viewType) {
        return new UserDetailsAdapter.MyViewHolderG(inflater.inflate(R.layout.inflater_user_details, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        try {
            if (!(holder instanceof LoadingViewHolder)) {
                MyViewHolderG mHolder=((MyViewHolderG) holder);
                final CoHostListModel.SingleCoHostData msingleCoHostData=mCoHostList.get(position);

                mHolder.txtv_userName.setText(msingleCoHostData.getFull_name());
                ImageLoader.setImageBig(mHolder.imgv_coHostPic, msingleCoHostData.getProfile_pic());


                if(mActivity instanceof  SendInvitationActivity){
                    if (((SendInvitationActivity)mActivity).invitedUserIds.contains(msingleCoHostData.getUser_id()))
                    {
                        ((MyViewHolderG) holder).check_selectUser.setChecked(true);
                    }
                    else{
                        ((MyViewHolderG) holder).check_selectUser.setChecked(false);
                    }
                }


                ((MyViewHolderG) holder).view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });

                ((MyViewHolderG) holder).check_selectUser.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if(mActivity instanceof  SendInvitationActivity){
                           if (b)
                           {
                               ((SendInvitationActivity)mActivity).invitedUserIds.add(msingleCoHostData.getUser_id());
                           }
                           else{
                               ((SendInvitationActivity)mActivity).invitedUserIds.remove(msingleCoHostData.getUser_id());
                           }
                        }
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    class MyViewHolderG extends RecyclerView.ViewHolder {
        View view;
        TextView txtv_userName,txtv_Email;
        ImageView imgv_coHostPic;
        CheckBox check_selectUser;



        public MyViewHolderG(View row) {
            super(row);

            view = row;
            txtv_userName=(TextView)view.findViewById(R.id.txtv_userName);
            txtv_Email=(TextView)view.findViewById(R.id.txtv_Email);
            imgv_coHostPic=(ImageView)view.findViewById(R.id.imgv_coHostPic);
            check_selectUser=(CheckBox)view.findViewById(R.id.check_selectUser);

        }
    }


    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<CoHostListModel.SingleCoHostData> filterList = new ArrayList();
                for (int i = 0; i < ((SendInvitationActivity)mActivity).getcoHostList().size(); i++) {
                    if ((((SendInvitationActivity)mActivity).getcoHostList().get(i).getFull_name().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(((SendInvitationActivity)mActivity).getcoHostList().get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = ((SendInvitationActivity)mActivity).getcoHostList().size();
                results.values = ((SendInvitationActivity)mActivity).getcoHostList();
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            mCoHostList = (ArrayList<CoHostListModel.SingleCoHostData>) results.values;
            notifyDataSetChanged();
        }

    }


}

