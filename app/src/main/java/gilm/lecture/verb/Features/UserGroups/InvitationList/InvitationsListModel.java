package gilm.lecture.verb.Features.UserGroups.InvitationList;

import java.util.List;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * created by PARAMBIR SINGH on 23/4/18.
 */

public class InvitationsListModel extends BasicApiModel
{
    private List<InvitationModel> data;

    public List<InvitationModel> getData() {
        return data;
    }

    public void setData(List<InvitationModel> data) {
        this.data = data;
    }

}
