package gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.ExisitngCoHost;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.CoHostListModel;
import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.Features.Internal.Base.BaseActivityWithoutPresenter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.WebServices.ProgressRequestBody;

/**
 * Created by harpreet on 9/7/17.
 */

public class SingleCoHostDetails extends BaseActivityWithoutPresenter implements View.OnClickListener, ProgressRequestBody.UploadCallbacks
{


    ImageView imgv_coverPic, imgv_profilePic;
    TextView txtv_userName, txtv_deviceName, txtv_macAddress;
    RatingBar ratingBar_val;
    ImageView imgv_wifi, img_microphone;
    CoHostListModel.SingleCoHostData mCoHostData;

    Button btnProfile, btnConnect;

    public static void start(Context context, CoHostListModel.SingleCoHostData mCoHostData) {
        Intent starter = new Intent(context, SingleCoHostDetails.class);
        starter.putExtra("Data", mCoHostData);
        ((Activity) context).startActivityForResult(starter, Constants.RequestCode.CoHostRequestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_single_cohost_details);
        setupToolbar("Connect Recording Host");
        initViews();
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (viewById != null) {
            viewById.setText(title);
        }
        else {
            getSupportActionBar().setTitle(title);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void initViews() {

        btnProfile = (Button) findViewById(R.id.btnProfile);
        btnConnect = (Button) findViewById(R.id.btnConnect);
        btnProfile.setOnClickListener(this);
        btnConnect.setOnClickListener(this);

        imgv_coverPic = (ImageView) findViewById(R.id.imgv_coverPic);
        imgv_profilePic = (ImageView) findViewById(R.id.imgv_profilePic);
        txtv_userName = (TextView) findViewById(R.id.txtv_userName);
        txtv_deviceName = (TextView) findViewById(R.id.txtv_deviceName);
        txtv_macAddress = (TextView) findViewById(R.id.txtv_macAddress);
        ratingBar_val = (RatingBar) findViewById(R.id.ratingBar_val);
        imgv_wifi = (ImageView) findViewById(R.id.imgv_wifi);
        img_microphone = (ImageView) findViewById(R.id.img_microphone);

        mCoHostData = (CoHostListModel.SingleCoHostData) getIntent().getSerializableExtra("Data");

        ImageLoader.setImageBig(imgv_coverPic, mCoHostData.getCover_pic());
        ImageLoader.setImageRoundSmall(imgv_profilePic, mCoHostData.getProfile_pic());
        txtv_userName.setText(mCoHostData.getFull_name());
        txtv_deviceName.setText(mCoHostData.getDevice_name());
        txtv_macAddress.setText(mCoHostData.getMac_address());
        ratingBar_val.setRating(5);


    }

    @Override
    public Context getActivityG() {
        return SingleCoHostDetails.this;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnConnect:
                Intent inn = new Intent();
                inn.putExtra(Constants.Extras.user_id, mCoHostData.getUser_id());
                inn.putExtra(Constants.Extras.name, mCoHostData.getFull_name());
                setResult(RESULT_OK, inn);
                finish();
                break;
            case R.id.btnProfile:
                ActivityOtherUserDetails.start(SingleCoHostDetails.this, mCoHostData.getUser_id(), "User Profile");
                break;
        }
    }


    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }


}
