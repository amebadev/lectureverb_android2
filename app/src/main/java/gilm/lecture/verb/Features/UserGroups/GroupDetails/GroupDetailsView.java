/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.UserGroups.GroupDetails;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 21 Jul 2017.
 */

public interface GroupDetailsView extends Viewable<GroupDetailsPresenter>
{
}
