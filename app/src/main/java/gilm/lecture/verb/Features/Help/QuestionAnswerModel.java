package gilm.lecture.verb.Features.Help;

import java.io.Serializable;

/**
 * created by PARAMBIR SINGH on 14/12/17.
 */

public class QuestionAnswerModel implements Serializable
{
    String question = "";
    String answer = "";

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
