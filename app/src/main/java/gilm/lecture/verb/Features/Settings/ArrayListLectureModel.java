package gilm.lecture.verb.Features.Settings;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.WebServices.BasicApiModel;

public class ArrayListLectureModel extends BasicApiModel
{
    @SerializedName("data")
    List<Lecture_And_Event_Model> listOfTagsModel;

    public List<Lecture_And_Event_Model> getListOfTagsModel() {
        return listOfTagsModel;
    }

    public void setListOfTagsModel(List<Lecture_And_Event_Model> listOfTagsModel) {
        this.listOfTagsModel = listOfTagsModel;
    }
}