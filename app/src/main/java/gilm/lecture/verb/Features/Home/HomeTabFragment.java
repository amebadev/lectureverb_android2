package gilm.lecture.verb.Features.Home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.piterwilson.audio.MP3RadioStreamDelegate;
import com.piterwilson.audio.MP3RadioStreamPlayer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import gilm.lecture.verb.Features.Home.Home.HomeFragment_NoBinding;
import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Home.Notification.NotificationFragment;
import gilm.lecture.verb.Features.Messaging.RecentChat.RecentChatFragment;
import gilm.lecture.verb.Features.Messaging.UsersList.UsersFollowingList;
import gilm.lecture.verb.Features.MyProfile.View.MyProfileFragment;
import gilm.lecture.verb.Features.Player.PlayerActivity;
import gilm.lecture.verb.Features.Player.PublicAudioPlayer;
import gilm.lecture.verb.Features.QuickLecture.ChoosePostOptionActivity;
import gilm.lecture.verb.Features.TabAdapter.FragmentTabModel;
import gilm.lecture.verb.Features.TabAdapter.SectionsPagerAdapter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.TitleNavigator;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeTabFragment extends Fragment {

    private View view;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ImageView fab;
    LinearLayout llMinimizedPlayer;
    ImageView imgvPlayPause;
    private Timer timer;
    private boolean playeEnd;
    TextView txtvDuration;
    CardView cardMinimizedPlayer;
    private int tabNumber;
    SharedPrefHelper mSharedPrefsHelper;

    public static HomeTabFragment newInstance(int tabNumber) {
        return new HomeTabFragment(tabNumber);
    }

    @SuppressLint("ValidFragment")
    public HomeTabFragment(int tabNumber) {
        // Required empty public constructor
        this.tabNumber = tabNumber;
    }

    public HomeTabFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);

        mSharedPrefsHelper = new SharedPrefHelper(getActivity());
        cardMinimizedPlayer = ((CardView) view.findViewById(R.id.cardMinimizedPlayer));
        txtvDuration = ((TextView) view.findViewById(R.id.txtvDuration));
        imgvPlayPause = (ImageView) view.findViewById(R.id.imgvPlayPause);
        llMinimizedPlayer = (LinearLayout) view.findViewById(R.id.llMinimizedPlayer);
        llMinimizedPlayer.setVisibility(View.GONE);
        viewPager = (ViewPager) view.findViewById(R.id.container);

        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        //Creating our pager adapter

        List<FragmentTabModel> list = new ArrayList<>();
        list.add(new FragmentTabModel(new HomeFragment_NoBinding(), ""));
       /* list.add(new FragmentTabModel(RecentActivitiesFragment.newInstance(), ""));*/
        list.add(new FragmentTabModel(MyProfileFragment.newInstanceForMyProfile(null, false), ""));
        list.add(new FragmentTabModel(NotificationFragment.newInstance(), ""));
        list.add(new FragmentTabModel(RecentChatFragment.newInstance(), ""));


        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getChildFragmentManager(), list);
        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        fab = (ImageView) view.findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewPager.getCurrentItem() == 3) {

                    UsersFollowingList.start(getActivity());
                } else {
                    ChoosePostOptionActivity.start(getActivity());
                }
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position) {
                    case 0:
                        TitleNavigator.getInstance().homeScreen(null);
                      /*  fab.show();*/
                        fab.setVisibility(View.VISIBLE);
                        fab.setImageResource(R.drawable.img_mic_home);
                        break;
                    case 1:
                        TitleNavigator.getInstance().RecentActivity();
                        fab.setVisibility(View.VISIBLE);
                        fab.setImageResource(R.drawable.img_mic_home);
                        break;
                    case 2:
                        tabLayout.getTabAt(2).setIcon(R.mipmap.ic_notification_bell_tab);
                        mSharedPrefsHelper.setIsNewNotification(false);
                        TitleNavigator.getInstance().Notification();
                        fab.setVisibility(View.VISIBLE);
                        fab.setImageResource(R.drawable.img_mic_home);
                        break;
                    case 3:
                        tabLayout.getTabAt(3).setIcon(R.mipmap.ic_envelope_tab);
                        mSharedPrefsHelper.setIsNewMessage(false);
                        TitleNavigator.getInstance().DirectMessage();
                        fab.setVisibility(View.VISIBLE);
                        fab.setImageResource(R.drawable.ic_msg_fab_1);
                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setCurrentItem(tabNumber);
        return view;
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.mipmap.ic_home_tab);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_user_white);

        if (mSharedPrefsHelper.isNewNotification()) {
            tabLayout.getTabAt(2).setIcon(R.mipmap.ic_notification_bell_tab_yellow);
        } else {
            tabLayout.getTabAt(2).setIcon(R.mipmap.ic_notification_bell_tab);
        }

        if (mSharedPrefsHelper.isNewMessage()) {
            tabLayout.getTabAt(3).setIcon(R.mipmap.ic_envelope_tab_yellow);
        } else {
            tabLayout.getTabAt(3).setIcon(R.mipmap.ic_envelope_tab);
        }
    }


    long maxDuration;
    Lecture_And_Event_Model lecutureModel;

    public void onActivityResultAudioPlayer(int requestCode, int resultCode, Intent data) {
        maxDuration = (int) PublicAudioPlayer.getInstance().getAudioPlayer().getDuration();

        llMinimizedPlayer.setVisibility(View.VISIBLE);
        lecutureModel = (Lecture_And_Event_Model) data.getSerializableExtra(Constants.Extras.DATA);
        ((TextView) view.findViewById(R.id.txtvLectureTitle)).setText(lecutureModel.getQuick_lecture_text());

        String name = lecutureModel.getUserdata().getFull_name();

        TextView txtvOwnerName = (view.findViewById(R.id.txtvOwnerName));
        txtvOwnerName.setText(name.trim());

        (view.findViewById(R.id.txtvLectureTitle)).setSelected(true);
        (view.findViewById(R.id.txtvUserName)).setSelected(true);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                (view.findViewById(R.id.viewToHide)).setVisibility(View.VISIBLE);
            }
        }, 300);

        if (PublicAudioPlayer.getInstance().getAudioPlayer().isPause()) {
            imgvPlayPause.setImageResource(android.R.drawable.ic_media_play);
        } else {
            imgvPlayPause.setImageResource(android.R.drawable.ic_media_pause);
        }
        imgvPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PublicAudioPlayer.getInstance().getAudioPlayer().isPause()) {
                    PublicAudioPlayer.getInstance().getAudioPlayer().setPause(false);
                    imgvPlayPause.setImageResource(android.R.drawable.ic_media_pause);
                } else {
                    PublicAudioPlayer.getInstance().getAudioPlayer().setPause(true);
                    imgvPlayPause.setImageResource(android.R.drawable.ic_media_play);
                }
            }
        });

        PublicAudioPlayer.getInstance().getAudioPlayer().setDelegate(new MP3RadioStreamDelegate() {
            @Override
            public void onRadioPlayerPlaybackStarted(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
                playeEnd = false;
            }

            @Override
            public void onRadioPlayerStopped(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
                playeEnd = true;
                try {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideMusicPlayer();
                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                PublicAudioPlayer.setReleaseAudioPlayer();
            }

            @Override
            public void onRadioPlayerError(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
                playeEnd = false;
            }

            @Override
            public void onRadioPlayerBuffering(MP3RadioStreamPlayer mp3RadioStreamPlayer) {

            }
        });

        cardMinimizedPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayerActivity.startAndResume(getActivity(), lecutureModel, false);

                hideMusicPlayer();
            }
        });

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (playeEnd || PublicAudioPlayer.getInstance().getAudioPlayer() == null) {
                    return;
                }
                long position = PublicAudioPlayer.getInstance().getAudioPlayer().getCurPosition();
                if (position > 0) {
                    // timer code here . par haasse wali gal eh a k eh chalna nahi kdi v.
                }
            }
        }, 1000, 1000);
    }

    private void hideMusicPlayer() {
        llMinimizedPlayer.setVisibility(View.GONE);
        (view.findViewById(R.id.viewToHide)).setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        PublicAudioPlayer.setReleaseAudioPlayer();
    }

    /**
     * this methos is being called from @{@link gilm.lecture.verb.Features.Home.Home.HomeAdapter_NoBinding} for closing the minimized view of the player when opened another lecture.
     * EventBus has been used to call this broadcast.
     *
     * @param hidePlayer
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String hidePlayer) {
        hideMusicPlayer();

        PublicAudioPlayer.getInstance().setReleaseAudioPlayer();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNewNotification(NewNotificationTypeBus newNotificationTypeBus) {
        if (newNotificationTypeBus.getOtherNotification()) {
            tabLayout.getTabAt(2).setIcon(R.mipmap.ic_notification_bell_tab_yellow);

        } else if (newNotificationTypeBus.getNewMessage()) {
            tabLayout.getTabAt(3).setIcon(R.mipmap.ic_envelope_tab_yellow);
        }
    }

}
