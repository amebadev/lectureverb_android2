package gilm.lecture.verb.Features.Internal.Base;


import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by param on 21/2/18.
 */

public interface EmptyView extends Viewable<EmptyPresenter> {
}
