package gilm.lecture.verb.Features.Subscription;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.Internal.Base.EmptyPresenter;
import gilm.lecture.verb.Features.Internal.Base.EmptyView;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.PaypalPayment.PriceModelRetrofit;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.ActivitySubscriptionBaseBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscriptionBaseActivity extends BaseActivity<ActivitySubscriptionBaseBinding, EmptyPresenter> implements EmptyView {


    public static void start(Context context) {
        Intent starter = new Intent(context, SubscriptionBaseActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_subscription_base;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new EmptyPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        showLoading("Please wait..");
        Call<PriceModelRetrofit> call = LVApplication.getRetrofit().create(ProfileApi.class).get_subscription_prices();
        call.enqueue(new Callback<PriceModelRetrofit>() {
            @Override
            public void onResponse(Call<PriceModelRetrofit> call, Response<PriceModelRetrofit> response) {
                hideLoading();
                if (response.body().getStatus()) {
                    if (response.body().getData() != null && response.body().getData().size() >= 1) {
                        final String subscriptionPrice = response.body().getData().get(1).getAmount();// getting second item

                        getDataBinder().txtvSubscriptionRate.setText("Lecture Verb Pro $" + subscriptionPrice + "/Month.");
                        getDataBinder().txtvSubscribeNow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                SubscriptionActivity.start(getActivityG());
                            }
                        });
                        getDataBinder().btnStartTrial.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                SubscriptionActivity.start(getActivityG());
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<PriceModelRetrofit> call, Throwable t) {
                hideLoading();
                UtillsG.showToast("Unable to fetch price list.", getActivityG(), true);
                finish();
            }
        });
    }

    @Override
    public Context getActivityG() {
        return SubscriptionBaseActivity.this;
    }
}
