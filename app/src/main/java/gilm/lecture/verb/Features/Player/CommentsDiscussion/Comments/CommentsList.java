package gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments;

import java.util.ArrayList;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * Created by harpreet on 10/18/17.
 */

public class CommentsList extends BasicApiModel {

    ArrayList<CommentsModel> data;

    public ArrayList<CommentsModel> getData() {
        return data;
    }

    public void setData(ArrayList<CommentsModel> data) {
        this.data = data;
    }
}
