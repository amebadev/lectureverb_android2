/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Settings.MainSettings;

import android.view.View;

import gilm.lecture.verb.Features.Settings.SettingsBaseBinder;

/**
 * Created by G-Expo on 20 Jul 2017.
 */

public interface MainSettingsBinder extends SettingsBaseBinder
{
    void openProfileSettings(View view);

    void openMessageSettings(View view);

    void openNotificationSettings(View view);

    void openTagSettings(View view);

    void openHelp(View view);

    void logOut(View view);

    void openAccountSettings(View view);

    void openContactsSettings(View view);

    void openImprintActivity(View view);

    void openDataStorageSettings(View view);

}
