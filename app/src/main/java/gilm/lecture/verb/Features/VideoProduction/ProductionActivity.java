/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.VideoProduction;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import gilm.lecture.verb.R;

public class ProductionActivity extends AppCompatActivity
{

    public static void start(Context context)
    {
        Intent starter = new Intent(context, ProductionActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_production);
    }

    public void bAck(View view)
    {
        finish();
    }
}
