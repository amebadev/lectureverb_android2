package gilm.lecture.verb.Features.LoginRegisteration.Login;

import android.app.Activity;
import android.content.Intent;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.LoginRegisteration.LoginRegisterApi;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import io.reactivex.Observable;
import retrofit2.Call;

/**
 * Created by G-Expo on 05 Jul 2017.
 */

public class LoginPresenter extends BasePresenter<LoginView> implements LoginPresenterBinder, GoogleApiClient.OnConnectionFailedListener, FacebookCallback<LoginResult>, CallBackG<UserDataModel> {

    private static final String TAG = "LoginPresenter";
    public ObservableField<String> userName =
            new ObservableField<>();
    public ObservableField<String> password =
            new ObservableField<>();
    private GoogleApiClient mGoogleApiClient;
    public CallbackManager callbackManager;

    public LoginPresenter() {
        userName.set("");
        password.set("");
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    /**
     * build google api client,for sign in.
     */
    public void intiSocialMedia() {
        //google plus initialization startForResult
        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            mGoogleApiClient = new GoogleApiClient.Builder(getView().getActivityG())
                    .enableAutoManage((FragmentActivity) getView().getActivityG() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();


        }
        //google plus initialization end


        //Facebook initialization startForResult
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, this);
        //Facebook initialization end

        mGoogleApiClient.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {
                Log.e("GPLUS CONNECTED", "-------------------------");
            }

            @Override
            public void onConnectionSuspended(int i) {
                Log.e("GPLUS SUSPENDED", "-------------------------");
            }
        });

    }

    @Override
    public void LoginClicked(View view) {
        if (userName.get().isEmpty()) {
            getView().displayError("Please enter user name");
        } else if (password.get().isEmpty()) {
            getView().displayError("Please enter password");
        } else {

            createApiRequest(getRetrofitInstance().manualLogin(userName.get(), password.get()), this);
        }

    }

    @Override
    public <V> void createApiRequest(Observable<V> observables, CallBackG<V> callBack) {
        getView().showLoading("Please wait");
        super.createApiRequest(observables, callBack);
    }

    protected LoginRegisterApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(LoginRegisterApi.class);
    }

    @Override
    public void GplusLoginClicked(View view) {

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            Log.e("getOut", status.toString());
                            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                            ((Activity) getView().getActivityG()).startActivityForResult(signInIntent, Constants.RequestCode.GP_SIGN_IN);
                        }
                    });
        } else {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            ((Activity) getView().getActivityG()).startActivityForResult(signInIntent, Constants.RequestCode.GP_SIGN_IN);
        }

    }

    @Override
    public void FacebookLoginClicked(View view) {
        LoginManager.getInstance().logInWithReadPermissions(((Activity) getView().getActivityG()), Arrays.asList("public_profile", "email"));
    }

    @Override
    public void TwitterLoginClicked(View view) {

        getView().getDataBinder().twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls
                TwitterSession data = result.data;
                Call<User> user = TwitterCore.getInstance().getApiClient().getAccountService().verifyCredentials(false, false, true);
                user.enqueue(new Callback<User>() {
                    @Override
                    public void success(Result<User> userResult) {
                        String name = userResult.data.name;
                        String email = userResult.data.email;

                        // _normal (48x48px) | _bigger (73x73px) | _mini (24x24px)
                        String photoUrlNormalSize = userResult.data.profileImageUrl;
//                        String photoUrlBiggerSize   = userResult.data.profileImageUrl.replace("_normal", "_bigger");
//                        String photoUrlMiniSize     = userResult.data.profileImageUrl.replace("_normal", "_mini");
//                        String photoUrlOriginalSize = userResult.data.profileImageUrl.replace("_normal", "");
                        getView().getLocalData().setProfilePic(photoUrlNormalSize);
                        socialLogin(name, email, "Twitter", userResult.data.getId() + "");
                    }

                    @Override
                    public void failure(TwitterException exc) {
                        Log.d("TwitterKit", "Verify Credentials Failure", exc);
                    }
                });

            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
                getView().twitterFailure(exception.getMessage());
            }
        });
        getView().getDataBinder().twitterLoginButton.performClick();
    }

    @Override
    public ObservableField<String> getPassword() {
        return password;
    }

    @Override
    public ObservableField<String> getUserName() {
        return userName;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("conection result", "" + connectionResult);
        getView().googlePlusSignInFail();
    }

    /**
     * activity result after {@link LoginPresenter#GplusLoginClicked(View)} ()}
     *
     * @param data-from google plus
     */
    public void onActivityResultGP(Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        handleSignInResultGP(result);
    }

    private void handleSignInResultGP(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResultGP:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();

            //            getView().getLocalData().setProfilePic("https://plus.google.com/s2/photos/profile/" + acct.getId() + "?sz=500");
            getView().getLocalData().setProfilePic(acct.getPhotoUrl() + "");
            socialLogin(acct.getDisplayName(), acct.getEmail(), "GooglePlus", acct.getId());
            //acct.getDisplayName()
        } else {
            getView().googlePlusSignInFail();
        }
    }

    //facebook override methods
    @Override
    public void onSuccess(LoginResult loginResult) {

        GraphRequestAsyncTask request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject user, GraphResponse graphResponse) {
                getView().getLocalData().setProfilePic("http://graph.facebook.com" + File.separator
                        + String.valueOf(user.optString("id")) + File.separator + "picture?width=500&width=500");

                socialLogin(user.optString("name"), user.optString("email"), "Facebook", user.optString("id"));
            }
        }).executeAsync();

    }

    @Override
    public void onCancel() {
        getView().displayError("Facebook Login cancelled.");
    }

    @Override
    public void onError(FacebookException error) {
        getView().displayError(error.getMessage());
    }

    //end facebook override methods

    private void socialLogin(String name, String email, String registerVia, String socialId) {
        createApiRequest(getRetrofitInstance().socialLogin(name, email, registerVia, socialId, "android", getView().getLocalData().getUserType()), this);
    }

    /**
     * data returned from the API call.
     *
     * @param output-data from API
     */
    @Override
    public void onCallBack(UserDataModel output) {
        getView().hideLoading();

        if (output.getStatus()) {
            if (UtillsG.getNotNullString(getView().getLocalData().getUserType(), "").equals(UtillsG.getNotNullString(output.getData().getRole_id(), ""))) {
                getView().getLocalData().setIsRegistered(true);
                getView().openNavigationActivity(output);
            } else {
                DialogHelper.getInstance().showInformation(getView().getActivityG(), "This Email is registered with a different type of User Preference. Please go back and select another option.", new CallBackG<String>() {
                    @Override
                    public void onCallBack(String output) {

                    }
                });
            }
        } else {
            getView().displayError(output.getMessage());

            if (output.getData() != null) {
                getView().getLocalData().setUserData(output.getData());
                getView().openEmailVerification();
            }
        }
    }
}

