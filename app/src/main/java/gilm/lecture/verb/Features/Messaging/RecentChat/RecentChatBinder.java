/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Messaging.RecentChat;

import android.view.View;

/**
 * Created by G-Expo on 25 Jul 2017.
 */

public interface RecentChatBinder
{
    /**
     * @param view
     * @param recentChatViewModel Intent to open chatting screen,with transition.
     */
    void openChat(View view, RecentChatViewModel recentChatViewModel);

}
