package gilm.lecture.verb.Features.Connect.ConnectNearBy.UsersList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.Features.UserGroups.GroupModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.InfiniteAdapter_WithoutBuinding;
import gilm.lecture.verb.UtilsG.LoadingViewHolder;

/**
 * Created by G-Expo on 06 Jul 2017.
 */

public class UsersListAdapter extends InfiniteAdapter_WithoutBuinding<UsersListAdapter.MyViewHolderG> {
    private LayoutInflater inflater;

    private Context con;
    private CallBackG<UserDataModel.UserData> callBack;

    private List<UserDataModel.UserData> dataList;


    public UsersListAdapter(Context context, List<UserDataModel.UserData> dList, CallBackG<UserDataModel.UserData> callBack) {
        inflater = LayoutInflater.from(context);
        this.dataList = dList;
        con = context;
        this.callBack = callBack;
    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        return new LoadingViewHolder(inflater.inflate(R.layout.layout_progress, parent, false));
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }


    @Override
    public UsersListAdapter.MyViewHolderG onCreateView(ViewGroup parent, int viewType) {
        return new UsersListAdapter.MyViewHolderG(inflater.inflate(R.layout.inflater_nearby_users, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        try {
            if (!(holder instanceof LoadingViewHolder)) {
                ImageLoader.setImageBig(((MyViewHolderG) holder).img_userImage, dataList.get(position).getProfile_pic());
                ((MyViewHolderG) holder).txtvUserName.setText(dataList.get(position).getFull_name());
                ((MyViewHolderG) holder).txtvEmailid.setText(
                        dataList.get(position).getEmail().equalsIgnoreCase("no@email.com") ? ""
                                : dataList.get(position).getEmail());

                ((MyViewHolderG) holder).view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (callBack == null) {
                            ActivityOtherUserDetails.start(con
                                    , dataList.get(position)
                                    , dataList.get(position).getIs_following().equalsIgnoreCase("true"));
                        } else {
                            callBack.onCallBack(dataList.get(position));
                        }
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    class MyViewHolderG extends RecyclerView.ViewHolder {
        View view;
        ImageView img_userImage;
        TextView txtvUserName, txtvEmailid;


        public MyViewHolderG(View row) {
            super(row);

            view = row;
            img_userImage = view.findViewById(R.id.img_userImage);
            txtvUserName = view.findViewById(R.id.txtvUserName);
            txtvEmailid = view.findViewById(R.id.txtvEmailid);
        }
    }

}
