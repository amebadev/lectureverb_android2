/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.LoginRegisteration.EmailVerification;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 10 Aug 2017.
 */

public interface EmailVerificationView extends Viewable<EmailVerificationPresenter>
{
    void gotoHome();
}
