package gilm.lecture.verb.Features.Home.Details;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import gilm.lecture.verb.R;

public class LectureDetailsActivity<ActivityLectureDetailsBinding extends ViewDataBinding> extends AppCompatActivity
{

    ActivityLectureDetailsBinding binder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_lecture_details);
        binder = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_lecture_event_details, null, false);



    }
}
