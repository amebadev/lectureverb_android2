package gilm.lecture.verb.Features.Search;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;

/**
 * created by PARAMBIR SINGH on 4/10/17.
 */

public class InnerSuggestedListsModel implements Serializable
{
    @SerializedName("suggested")
    List<Lecture_And_Event_Model> suggestedLecList = new ArrayList<>();

    @SerializedName("hot")
    List<Lecture_And_Event_Model> hotLecturesList = new ArrayList<>();

    @SerializedName("top")
    List<Lecture_And_Event_Model> topLecturesList = new ArrayList<>();

    public List<Lecture_And_Event_Model> getSuggestedLecList() {
        return suggestedLecList;
    }

    public void setSuggestedLecList(List<Lecture_And_Event_Model> suggestedLecList) {
        this.suggestedLecList = suggestedLecList;
    }

    public List<Lecture_And_Event_Model> getHotLecturesList() {
        return hotLecturesList;
    }

    public void setHotLecturesList(List<Lecture_And_Event_Model> hotLecturesList) {
        this.hotLecturesList = hotLecturesList;
    }

    public List<Lecture_And_Event_Model> getTopLecturesList() {
        return topLecturesList;
    }

    public void setTopLecturesList(List<Lecture_And_Event_Model> topLecturesList) {
        this.topLecturesList = topLecturesList;
    }
}
