package gilm.lecture.verb.Features.QuickLecture;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import gilm.lecture.verb.Features.Internal.Base.BaseActivityWithoutPresenter;
import gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence.UserType;
import gilm.lecture.verb.Features.QuickLecture.Audio.RecordAudio.RecordingActivity;
import gilm.lecture.verb.Features.QuickLecture.Presentation.PresentationOptions.PresentationOptionsActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChoosePostOptionActivity extends BaseActivityWithoutPresenter
{
    EditText edTitle;
    String group_id = "";

    public static void start(Context context) {
        Intent starter = new Intent(context, ChoosePostOptionActivity.class);
        context.startActivity(starter);
    }

    public static void start(Context context, String group_id) {
        Intent starter = new Intent(context, ChoosePostOptionActivity.class);
        starter.putExtra("group_id", group_id);
        context.startActivity(starter);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_post_option);

    group_id = UtillsG.getNotNullString(getIntent().getStringExtra("group_id"),"0");

        edTitle = (EditText) findViewById(R.id.edTitle);
        setupToolbar("Choose Post Option");


        ((CardView) findViewById(R.id.layoutRecordAudio)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                RecordingActivity.start(ChoosePostOptionActivity.this, edTitle.getText().toString(),group_id);
            }
        });


        ((CardView) findViewById(R.id.layoutRecordPPT)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
              /*  if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    UtillsG.showToast("Sorry your device does not support this feature.", getActivityG(), true);
                }
                else {*/
                PresentationOptionsActivity.start(ChoosePostOptionActivity.this, edTitle.getText().toString().trim(),group_id);
//                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.menu_save, menu);
        menu.findItem(R.id.save).setTitle("Post");


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save) {
            upload();
        }
        else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (viewById != null) {
            viewById.setText(title);
        }
        else {
            getSupportActionBar().setTitle(title);
        }
    }

    public void upload() {
        if (edTitle.getText().toString().trim().isEmpty()) {
            displayError("Please enter lecture title");
        }
        else {
            showLoading("Saving text type lecture...");
            Call<QuickLectureModel> quickLectureApiModelCall = getRetrofitInstance().textTypeQuickLecture(
                    getOtherParams("text"),
                    getOtherParams(new SharedPrefHelper(getActivityG()).getUserId()),
                    getOtherParams(edTitle.getText().toString())
                    , getOtherParams(new SharedPrefHelper(getActivityG()).getUserName())
                    , getOtherParams("{This is text lecture with no stack}"));
            quickLectureApiModelCall.enqueue(new Callback<QuickLectureModel>()
            {
                @Override
                public void onResponse(Call<QuickLectureModel> call, final Response<QuickLectureModel> response) {

                    hideLoading();
                    if (response.body() != null) {

                        if (response.body().getStatus()) {

                            UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                            UtillsG.hideKeyboard(getActivityG(), edTitle);
                            if (new SharedPrefHelper(getActivityG()).getUserType().equalsIgnoreCase(UserType.LECTURER)) {
                                UtillsG.showInterstitialAds(getActivityG(),getResources().getString(R.string.userAppId),getResources().getString(R.string.userAdUnitQuickPresent));
                            }
                            else {
                                UtillsG.showInterstitialAds(getActivityG(),getResources().getString(R.string.userAppId),getResources().getString(R.string.nonStudentAdUnitTextPostAd));

                            }
                          finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<QuickLectureModel> call, Throwable t) {
                    Log.e("Error", t.toString());
                }
            });
        }
    }


    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }


    private QuickLectureApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(QuickLectureApi.class);
    }

    @Override
    public Context getActivityG() {
        return ChoosePostOptionActivity.this;
    }
}
