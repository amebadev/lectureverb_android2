package gilm.lecture.verb.Features.Subscription;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.Internal.Base.EmptyPresenter;
import gilm.lecture.verb.Features.Internal.Base.EmptyView;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.PaypalPayment.PaymentActivity;
import gilm.lecture.verb.Features.PaypalPayment.PriceModelRetrofit;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DateHelper;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.ActivitySubscriptionBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscriptionActivity extends BaseActivity<ActivitySubscriptionBinding, EmptyPresenter> implements EmptyView
{
    String subscriptionPrice = "";

    public static void start(Context context)
    {
        Intent starter = new Intent(context, SubscriptionActivity.class);

        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId()
    {
        return R.layout.activity_subscription;
    }

    @Override
    protected void onCreateActivityG()
    {
        injectPresenter(new EmptyPresenter());
        getPresenter().attachView(this);

    }

    @Override
    public void initViews()
    {
        setupToolbar("Subscription");

        showLoading("Fetching prices, please wait..");
        Call<PriceModelRetrofit> call = LVApplication.getRetrofit().create(ProfileApi.class).get_subscription_prices();
        call.enqueue(new Callback<PriceModelRetrofit>()
        {
            @Override
            public void onResponse(Call<PriceModelRetrofit> call, Response<PriceModelRetrofit> response)
            {
                hideLoading();
                if (response.body().getStatus())
                {
                    if (response.body().getData() != null && response.body().getData().size() >= 2)
                    {
                        subscriptionPrice = response.body().getData().get(1).getAmount();// getting second item

                        getDataBinder().btnPayment.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View view)
                            {
                                PaymentActivity.start(getActivityG(), subscriptionPrice, "Subscription");
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<PriceModelRetrofit> call, Throwable t)
            {
                hideLoading();
                UtillsG.showToast("Unable to fetch price list.", getActivityG(), true);
                finish();
            }
        });


    }

    @Override
    public Context getActivityG()
    {
        return SubscriptionActivity.this;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == Constants.RequestCode.PAYPAL_PAYMENT)
            {
                if (data.getBooleanExtra("isSuccess", false))
                {
                    showLoading("Please wait..");
                    Call<BasicApiModel> call = LVApplication.getRetrofit().create(ProfileApi.class).save_subscription(getLocalData().getUserId(), DateHelper.getInstance().getOneMonthLaterDate());
                    call.enqueue(new Callback<BasicApiModel>()
                    {
                        @Override
                        public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
                        {
                            hideLoading();
                            if (response.body().getStatus())
                            {
                                DialogHelper.getInstance().showInformation(getActivityG(), "Congratulations", "You are now a premium user of Lecture Verb.", new CallBackG<String>()
                                {
                                    @Override
                                    public void onCallBack(String output)
                                    {
                                        finish();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(Call<BasicApiModel> call, Throwable t)
                        {
                            hideLoading();
                            UtillsG.showToast("an error pccured.", getActivityG(), true);
                        }
                    });
                }
            }
        }
    }
}
