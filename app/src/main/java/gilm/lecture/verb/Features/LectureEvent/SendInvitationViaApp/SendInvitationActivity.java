package gilm.lecture.verb.Features.LectureEvent.SendInvitationViaApp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.CoHostListModel;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EventLectureApi;
import gilm.lecture.verb.Features.Internal.Base.BaseActivityWithoutPresenter;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsApi;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.EndlessRecyclerOnScrollListener;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harpreet on 9/19/17.
 */

public class SendInvitationActivity extends BaseActivityWithoutPresenter implements View.OnClickListener, ProgressRequestBody.UploadCallbacks {

    RecyclerView recycle_list;
    int page = 1;
    Double mLatitude = 0.0, mLongitude = 0.0;
    public ArrayList<CoHostListModel.SingleCoHostData> coHostList = new ArrayList<>();
    UserDetailsAdapter coHostListAdapter;
    public ArrayList<String> invitedUserIds = new ArrayList<>();
    TextView txtv_sendInvitation;
    LectureEventClicks commonApisClicks = new LectureEventClicks();
    private EditText edSearch;
    private ImageView imgvSearch;


    public static void start(Context context, String lectureEventId) {
        Intent starter = new Intent(context, SendInvitationActivity.class);
        starter.putExtra("lectureEventId", lectureEventId);
        ((Activity) context).startActivityForResult(starter, 102);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_invite_members);
        setupToolbar("Invite Members");
        initViews();
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (viewById != null) {
            viewById.setText(title);
        } else {
            getSupportActionBar().setTitle(title);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void initViews() {
        edSearch = (EditText) findViewById(R.id.edSearch);
        imgvSearch = (ImageView) findViewById(R.id.imgvSearch);

        recycle_list = (RecyclerView) findViewById(R.id.recycle_list);
        LinearLayoutManager mLinearManager = new LinearLayoutManager(getActivityG());
        recycle_list.setLayoutManager(mLinearManager);

        txtv_sendInvitation = (TextView) findViewById(R.id.txtv_sendInvitation);
        txtv_sendInvitation.setOnClickListener(this);
        imgvSearch.setOnClickListener(this);

        EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(mLinearManager) {
            @Override
            public void onLoadMore(int current_page) {
                page++;
                getCoHostLists();
            }

            @Override
            public void countChange(int currentVisibleCount) {

            }
        };
        recycle_list.setOnScrollListener(endlessRecyclerOnScrollListener);

        coHostListAdapter = new UserDetailsAdapter((Activity) getActivityG(), coHostList);
        recycle_list.setAdapter(coHostListAdapter);

        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                coHostListAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        getCoHostLists();

    }

    @Override
    public Context getActivityG() {
        return SendInvitationActivity.this;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtv_sendInvitation:
                if (invitedUserIds.size() > 0) {
                    commonApisClicks.SetInvitationViaApp((Activity) getActivityG(), LVApplication.getRetrofit().create(LectureEventsApi.class),
                            getIntent().getStringExtra("lectureEventId"),
                            invitedUserIds.toString().replace("{", "").replace("}", "").replace("[", "").replace("]", "").replace(" ", ""), new CallBackG<Boolean>() {
                                @Override
                                public void onCallBack(Boolean output) {
                                    if (output) {
                                        finish();
                                    }
                                }
                            }
                    );
                } else {
                    UtillsG.showToast("Select users to send invite", getActivityG(), true);
                }

                break;
            case R.id.imgvSearch:
                getSearchedCoHosts(edSearch.getText().toString());
                break;
        }
    }


    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    public void getSearchedCoHosts(String searchedKeyword) {

        Call<CoHostListModel> basicApiModelCall = getRetrofitInstance().search_user(
                getOtherParams(new SharedPrefHelper(getActivityG()).getUserId())
                , getOtherParams("" + 1)
                , getOtherParams(searchedKeyword));
        basicApiModelCall.enqueue(new Callback<CoHostListModel>() {
            @Override
            public void onResponse(Call<CoHostListModel> call, Response<CoHostListModel> response) {
                hideLoading();
                if (response.body() != null) {
                    if (response.body().getStatus()) {

                        coHostList.clear();
                        coHostList.addAll(response.body().getmQuickLectureData());
                        coHostListAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<CoHostListModel> call, Throwable t) {
                hideLoading();
                Log.e("Error occured", t.getMessage().toString());
            }
        });
    }

    public void getCoHostLists() {

        if (page == 1) {
            showLoading("Getting nearby users");
        }

        Call<CoHostListModel> basicApiModelCall = getRetrofitInstance().get_nearby_co_hosts(
                getOtherParams(new SharedPrefHelper(getActivityG()).getUserId())
                , getOtherParams("" + mLatitude)
                , getOtherParams("" + mLongitude)
                , getOtherParams("" + page)
                , getOtherParams("500")
                , getOtherParams("false")
        );
        basicApiModelCall.enqueue(new Callback<CoHostListModel>() {
            @Override
            public void onResponse(Call<CoHostListModel> call, Response<CoHostListModel> response) {
                hideLoading();
                if (response.body() != null) {
                    if (response.body().getStatus()) {
                        if (page == 1) {
                            coHostList.clear();
                        }

                        coHostList.addAll(response.body().getmQuickLectureData());
                        coHostListAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<CoHostListModel> call, Throwable t) {
                hideLoading();
                Log.e("Error occured", t.getMessage().toString());
            }
        });
    }

    public ArrayList<CoHostListModel.SingleCoHostData> getcoHostList() {
        return coHostList;
    }

    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    protected EventLectureApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(EventLectureApi.class);
    }


}
