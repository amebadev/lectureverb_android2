package gilm.lecture.verb.Features.MonitorLecture;

import android.app.FragmentManager;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.io.File;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * created by PARAMBIR SINGH on 26/9/17.
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class LectureMonitorPresenter extends BasePresenter<LectureMonitorView> implements ProgressRequestBody.UploadCallbacks {

//    CameraView cameraView;

    File videoFile = null;
    private String audioFilePath;
    private boolean isfrontCamera;
    private FragmentManager fragmentManager;
    Camera2VideoFragment camera2VideoFragment;

    public void setCameraListener() {
/*
        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onVideoTaken(File video) {
                Log.e("VIDEO PATH ==== ", video.getAbsolutePath() + "");

                getView().videoRecordingStopped(video.getAbsolutePath());
            }
        });
*/
    }

    public void setCameraView(FragmentManager fragmentManager_) {
//        this.cameraView = cameraView;
        this.fragmentManager = fragmentManager_;

        camera2VideoFragment = Camera2VideoFragment.newInstance(isfrontCamera);

        fragmentManager.beginTransaction()
                .replace(R.id.container, camera2VideoFragment)
                .commit();
    }

    public void startVideoRecording() {
       /* File imagesFolder = new File(
                Environment.getExternalStorageDirectory(), Web.LocalFolders.LectureVerbVideos);
        if (!imagesFolder.exists()) {
            imagesFolder.mkdirs();
        }


        videoFile = new File(imagesFolder, System.currentTimeMillis()
                + ".mp4");

        cameraView.startCapturingVideo(videoFile);*/
    }

    public void stopVideoRecording() {
     /*   if (cameraView.isCapturingVideo()) {
            cameraView.stopCapturingVideo();
        }*/
    }


    public void toggleCamera() {

        isfrontCamera = !isfrontCamera;
        setCameraView(fragmentManager);

        /*        if (cameraView.isCapturingVideo()) return;
        switch (cameraView.toggleFacing()) {
            case BACK:
                UtillsG.showToast("Switched to back camera", getView().getActivityG(), true);
                break;

            case FRONT:
                UtillsG.showToast("Switched to front camera", getView().getActivityG(), true);
                break;
        }*/
    }

    public void uploadRecordedMedia() {
        if (videoFile != null || !UtillsG.getNotNullString(audioFilePath, "").isEmpty()) {

            UtillsG.showLoading("Please wait", getView().getActivityG());

            File audioFile = new File(audioFilePath != null ? audioFilePath : "");

            Call<BasicApiModel> upload_recorded_event = LVApplication.getRetrofit().create(LectureMonitorApi.class)
                    .upload_recorded_event(
                            (audioFile != null && audioFile.exists()) ? getFilePart(audioFile, "event_audio") : null
                            , (videoFile != null && videoFile.exists()) ? getFilePart(videoFile, "event_video") : null
                            , getOtherParams("upload_recorded_event")
                            , getOtherParams(getView().getLocalData().getUserId())
                            , getOtherParams("false")
                            , getOtherParams(getView().getEventModel().getEvent_id()));
            upload_recorded_event.enqueue(new Callback<BasicApiModel>() {
                @Override
                public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                    UtillsG.hideLoading();
                    if (response.body().getStatus()) {
                        getView().fileUploadingDone();
                    } else {
                        UtillsG.showToast(response.body().getMessage(), getView().getActivityG(), true);
                    }
                }

                @Override
                public void onFailure(Call<BasicApiModel> call, Throwable t) {
                    UtillsG.hideLoading();
                    UtillsG.showToast(t.getMessage(), getView().getActivityG(), true);
                }
            });
        } else {
            UtillsG.showToast("No media recorded.", getView().getActivityG(), true);
        }
    }

    private MultipartBody.Part getFilePart(final File file, String fileType) {
        ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(fileType, file.getName(), fileBody);
        return filePart;
    }

    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    public void setAudioFilePath(String filePath) {
        this.audioFilePath = filePath;
    }

    public Camera2VideoFragment getVideoRecorderFragment() {
        return camera2VideoFragment;
    }

    public void setVideoFilePath(String videoFilePath_) {
        videoFile = new File(videoFilePath_);
    }
}
