package gilm.lecture.verb.Features.Search;

import android.content.Context;
import android.util.Log;
import android.view.View;

import java.util.List;

import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.Features.LectureEvent.LectureEventPresenterBinder;
import gilm.lecture.verb.Features.Player.PlayerActivity;
import gilm.lecture.verb.Features.Player.PublicAudioPlayer;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.InflatorWithImageBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * created by G-Expo on 31/7/17.
 */

public class LectureOnlyImagesAdapter extends InfiniteAdapterG<InflatorWithImageBinding>
{
    private final Context context;
    private LectureEventPresenterBinder presenter;
    List<Lecture_And_Event_Model> alData;

    public LectureOnlyImagesAdapter(Context context, List<Lecture_And_Event_Model> alData) {
        this.context = context;
        this.alData = alData;
    }

    @Override
    protected void bindData(final int position, final MyViewHolderG myViewHolderG) {
        myViewHolderG.binding.setImage(alData.get(position).getThumbnail());
        myViewHolderG.binding.setTitle(alData.get(position).getQuick_lecture_text());

        UtillsG.setTextSizeByPercentage(context, myViewHolderG.binding.txtvLectureTitle, 6f);

        if (alData.get(position).getThumbnail().isEmpty()) {
            myViewHolderG.binding.txtvLectureTitle.setVisibility(View.VISIBLE);
        }
        else {
            myViewHolderG.binding.txtvLectureTitle.setVisibility(View.GONE);
        }

        ImageLoader.setImageSmallCentreCrop(myViewHolderG.binding.imgv, alData.get(position).getThumbnail());
        myViewHolderG.binding.imgv.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                if (UtillsG.isAudioVideoMediaPost(alData.get(position))) {
//                    NotificationDetailsActivity.start(context, alData.get(position));
                    setLectureAsPlayed(position);
                    PublicAudioPlayer.setReleaseAudioPlayer();
                    PlayerActivity.startForResult(context, alData.get(position), false);
                }
                else if (UtillsG.isTextPost(alData.get(position))) {
//                    UtillsG.showToast("It is a text Lecture.", context, true);
//                    NotificationDetailsActivity.start(context, alData.get(position));
                    DialogHelper.getInstance().showInformation(context, alData.get(position).getUserdata().getFull_name(), alData.get(position).getQuick_lecture_text(), new CallBackG<String>()
                    {
                        @Override
                        public void onCallBack(String output) {

                        }
                    });

                }
            }
        });
        myViewHolderG.binding.txtvLectureTitle.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                myViewHolderG.binding.imgv.performClick();
            }
        });

        myViewHolderG.binding.executePendingBindings();
    }

    @Override
    public int getCount() {
        return alData.size();
    }

    @Override
    public int getInflateLayout() {
        return R.layout.inflator_with_image;
    }


    private void setLectureAsPlayed(final int position) {
        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance().add_lecture_to_recent_played_list(new SharedPrefHelper(context).getUserId(), alData.get(position).getQuick_lecture_id());
        basicApiModelCall.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body() != null && response.body().getStatus()) {
                    Log.e("RESPONSE", response.body().getStatus() + "");

                    if (response.body().getStatus()) {
                        int count = Integer.parseInt(alData.get(position).getLecture_view());
                        count++;
                        alData.get(position).setLecture_view(String.valueOf(count));
                        notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("ERROR get_posted_le", t.getMessage().toString());
            }
        });
    }

    protected HomeApis getRetrofitInstance() {
        return LVApplication.getRetrofit().create(HomeApis.class);
    }


}
