package gilm.lecture.verb.Features.Messaging.UsersList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.Messaging.Chat.ChatActivity;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.WebServices.LVApplication;

/**
 * Created by harpreet on 11/2/17.
 */

public class UserFollowingAdapter extends RecyclerView.Adapter<UserFollowingAdapter.MyViewHolderG>
{


    private final LayoutInflater inflater;
    private List<UserDataModel.UserData> alData;
    private boolean isFollowing,isMyProfile;
    private Context context;
    UserFollowingAdapter.ValueFilter valueFilter;
    UsersFollowingList mActivity;

    public UserFollowingAdapter(Context context, List<UserDataModel.UserData> alData, UsersFollowingList mActivity) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.alData = alData;
        this.isFollowing = isFollowing;
        this.isMyProfile=isMyProfile;
        this.mActivity=mActivity;
    }

    @Override
    public UserFollowingAdapter.MyViewHolderG onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserFollowingAdapter.MyViewHolderG(inflater.inflate(R.layout.inflater_followers_followings, parent, false));
    }

    @Override
    public void onBindViewHolder(UserFollowingAdapter.MyViewHolderG holder, final int position) {
        ImageLoader.setImageRoundSmall(holder.imgvUserImage, alData.get(position).getProfile_pic());
        holder.txtvUserName.setText(alData.get(position).getFull_name());
        holder.txtvFollowUnFollow.setText("Chat");
        holder.txtvFollowUnFollow.setVisibility(
                alData.get(position).getUser_id().equalsIgnoreCase(new SharedPrefHelper(context).getUserId())
                        ? View.GONE : View.VISIBLE);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityOtherUserDetails.start(context,alData.get(position),alData.get(position).getIs_following().equalsIgnoreCase("1"));
            }
        });


        holder.txtvFollowUnFollow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                alData.get(position).setIs_blocked("0");
                ChatActivity.start(mActivity,alData.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return alData.size();
    }

    class MyViewHolderG extends RecyclerView.ViewHolder
    {
        View view;
        ImageView imgvUserImage;
        TextView txtvUserName, txtvFollowUnFollow;

        public MyViewHolderG(View row) {
            super(row);

            view = row;
            imgvUserImage = (ImageView) view.findViewById(R.id.imgvUserImage);
            txtvUserName = (TextView) view.findViewById(R.id.txtvUserName);
            txtvFollowUnFollow = (TextView) view.findViewById(R.id.txtvFollowUnFollow);
        }
    }

    private ProfileApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(ProfileApi.class);
    }


    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new UserFollowingAdapter.ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<UserDataModel.UserData> filterList = new ArrayList();
                for (int i = 0; i < mActivity.getUsersList().size(); i++) {
                    if ((mActivity.getUsersList().get(i).getFull_name().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(mActivity.getUsersList().get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count =mActivity.getUsersList().size();
                results.values = mActivity.getUsersList();
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            alData = (ArrayList<UserDataModel.UserData>) results.values;
            notifyDataSetChanged();
        }

    }
}
