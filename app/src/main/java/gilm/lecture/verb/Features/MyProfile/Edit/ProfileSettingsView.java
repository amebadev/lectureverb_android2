/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.MyProfile.Edit;

import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;

/**
 * Created by G-Expo on 21 Jul 2017.
 */

public interface ProfileSettingsView extends Viewable<ProfileSettingsPresenter>
{
    /**
     * Close profile setting by calling {@link #getActivityG()}.finish()}
     */
    void close();

    void profileUpdated(UserDataModel output);

    void showProfileUpdateProgress();

    void openPlacePicker(View view);

    View getViewToHideKeyBoard();
}
