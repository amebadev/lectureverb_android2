package gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.CoHostListModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.UtilsG.BitmapUtils.BitmapDecoderG;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DateHelper;
import gilm.lecture.verb.UtilsG.LocationG;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 07 Jul 2017.
 */

public class CreateLectureEventPresenter extends BasePresenter<CreateLectureEventView> implements CreateEditLectureEventPresenterBinder, ProgressRequestBody.UploadCallbacks {
    public CreateEditLectureEventViewModel createEditLectureEventViewModel;
    LocationG locationDetails = null;
    LectureEventClicks mClicks = new LectureEventClicks();
    String commaSeparatedIds = "";

    public CreateLectureEventPresenter() {
        createEditLectureEventViewModel = new CreateEditLectureEventViewModel();
        createEditLectureEventViewModel.setDetail("");
        createEditLectureEventViewModel.setTitle("");
        createEditLectureEventViewModel.setIsLive(false);
        createEditLectureEventViewModel.setSelectedImage(null);
        createEditLectureEventViewModel.setLocationG(null);
        createEditLectureEventViewModel.setIsGuestInvited(false);

        createEditLectureEventViewModel.setStartDate("");
        createEditLectureEventViewModel.setEndDate("");
        createEditLectureEventViewModel.setStartTime("");
        createEditLectureEventViewModel.setEndTime("");
        createEditLectureEventViewModel.setSelectedCoHost(null);
        createEditLectureEventViewModel.setLectureEventIsPublic(true);
        createEditLectureEventViewModel.setLectureTags(null);
        createEditLectureEventViewModel.setAlCategories(null);
        commaSeparatedIds = "";
    }

    public void createEvent() {

        if (createEditLectureEventViewModel.getTitle().trim().isEmpty()) {
            getView().displayError("Please enter lecture title");
        } else if (createEditLectureEventViewModel.getSelectedImage() == null) {
            getView().displayError("Please select lecture image");
        } else if (createEditLectureEventViewModel.getStartDate().trim().isEmpty()) {
            getView().displayError("Please select start date");
        } else if (createEditLectureEventViewModel.getEndDate().trim().isEmpty()) {
            getView().displayError("Please select end date");
        } else if (createEditLectureEventViewModel.getStartTime().trim().isEmpty()) {
            getView().displayError("Please select start time");
        } else if (createEditLectureEventViewModel.getEndTime().trim().isEmpty()) {
            getView().displayError("Please select end time");
        } /*else if (getView().getSelectedCoHostId().trim().isEmpty()) {
            getView().displayError("Please select facilitator");
        } */else if (createEditLectureEventViewModel.getDetail().trim().isEmpty()) {
            getView().displayError("Please enter details");
        } else if (createEditLectureEventViewModel.getAlCategories() == null) {
            getView().displayError("Please select lecture categories");
        } else {

            getView().showLoading("Saving lecture details");
            retrofit2.Call<EventLectureCreatedResponse> basicApiModelCall = getRetrofitInstance().create_lecture_event(
                    getFilePart(new File(createEditLectureEventViewModel.getSelectedImage().getPath()), "lecture_poster")
                    , getOtherParams(new SharedPrefHelper(getView().getActivityG()).getUserId())
                    , getOtherParams(createEditLectureEventViewModel.getTitle())
                    , getOtherParams(createEditLectureEventViewModel.getIsLive() ? "live_recorded"
                            : "pre_recorded")
                    , getOtherParams(UtillsG.getNotNullString(createEditLectureEventViewModel.getLocationG(), ""))
                    , getOtherParams("" + ((locationDetails != null) ? locationDetails.getLatitude()
                            : "0.0"))
                    , getOtherParams("" + ((locationDetails != null) ? locationDetails.getLongitude()
                            : "0.0"))
                    , getOtherParams("" + createEditLectureEventViewModel.getIsGuestInvited())
                    , getOtherParams(
                            getView().getSelectedCoHostId())
                    , getOtherParams(createEditLectureEventViewModel.getLectureTags() == null ? ""
                            : createEditLectureEventViewModel.getLectureTags().toString().replace("[", "").replace("]", ""))
                    , getOtherParams(
                            createEditLectureEventViewModel.getLectureEventIsPublic() ? "true"
                                    : "false")
                    , getOtherParams(DateHelper.convertDateToRequiredFormat(createEditLectureEventViewModel.getStartDate() + " " + createEditLectureEventViewModel.getStartTime(), DateHelper.DateFormat))
                    , getOtherParams(DateHelper.convertDateToRequiredFormat(createEditLectureEventViewModel.getEndDate() + " " + createEditLectureEventViewModel.getEndTime(), DateHelper.DateFormat))
                    , getOtherParams(createEditLectureEventViewModel.getDetail())
                    , getOtherParams(new SharedPrefHelper(getView().getActivityG()).getUserName())
                    , getOtherParams(getView().getSelectedLecturerId())
                    , getOtherParams(getView().getSelectedGroupId())
            );
            basicApiModelCall.enqueue(new Callback<EventLectureCreatedResponse>() {
                @Override
                public void onResponse(retrofit2.Call<EventLectureCreatedResponse> call, Response<EventLectureCreatedResponse> response) {

                    if (response.body() != null) {

                        if (response.body().getStatus()) {

                            UtillsG.deleteThisFile(createEditLectureEventViewModel.getSelectedImage().getPath());

                            mClicks.setLectureInterests((Activity) getView().getActivityG(), getRetrofitInstance(), response.body().getmQuickLectureData().getEvent_id(),
                                    commaSeparatedIds, "event", new CallBackG<Boolean>() {
                                        @Override
                                        public void onCallBack(Boolean output) {
                                            if (getView() != null) {
                                                getView().hideLoading();
                                            }
                                            if (output) {
                                                UtillsG.showToast("Lecture event created successfully", getView().getActivityG(), true);
                                                ((Activity) getView().getActivityG()).finish();
                                            }

                                        }
                                    });
                        } else {
                            if (getView() != null) {
                                getView().hideLoading();
                            }
                            UtillsG.showToast("Error while saving event lecture", getView().getActivityG(), true);
                        }
                    } else {
                        if (getView() != null) {
                            getView().hideLoading();
                        }
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<EventLectureCreatedResponse> call, Throwable t) {
                    if (getView() != null) {
                        getView().hideLoading();
                    }
                    Log.e("Error occured", t.getMessage().toString());
                }
            });
        }
    }

    private MultipartBody.Part getFilePart(final File file, String fileType) {
        ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(fileType, file.getName(), fileBody);
        return filePart;
    }

    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }


    protected EventLectureApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(EventLectureApi.class);
    }

    public CreateEditLectureEventViewModel getDataViewModel() {
        return createEditLectureEventViewModel;
    }

    @Override
    public void liveRecorder(boolean isLive) {
        createEditLectureEventViewModel.setIsLive(isLive);
    }

    @Override
    public void selectImage(View view) {
        BitmapDecoderG.selectImage(getView().getActivityG(), null);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Uri selectedFile = BitmapDecoderG.onActivityResult(getView().getActivityG(), requestCode, resultCode, data);
        createEditLectureEventViewModel.setSelectedImage(selectedFile);
    }

    Calendar c = Calendar.getInstance();

    @Override
    public void selectStartDate(final View view) {

        showStartDatePicker((TextView) view);
    }

    protected void showStartDatePicker(final TextView view) {
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getView().getActivityG(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                c.set(Calendar.YEAR, i);
                c.set(Calendar.MONTH, i1);
                c.set(Calendar.DAY_OF_MONTH, i2);
                updateLabelDate(view, true);
            }
        }, year, month, day);
        dialog.setTitle("Select Date");
        dialog.show();
    }

    protected void showEndDatePicker(final TextView view) {
        if (createEditLectureEventViewModel.getStartDate().trim().isEmpty()) {
            getView().displayError("Select start date");

        } else if (createEditLectureEventViewModel.getStartTime().trim().isEmpty()) {
            getView().displayError("Select start time");
        } else {
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getView().getActivityG(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    c.set(Calendar.YEAR, i);
                    c.set(Calendar.MONTH, i1);
                    c.set(Calendar.DAY_OF_MONTH, i2);
                    updateLabelDate(view, false);
                }
            }, year, month, day);
            dialog.setTitle("Select Date");
            dialog.show();
        }
    }

    private void updateLabelDate(TextView textView, boolean isStartDate) {

        String myFormat = "dd MMM yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

        try {

            if (isStartDate) {
                createEditLectureEventViewModel.setEndDate("");
                createEditLectureEventViewModel.setEndTime("");
                createEditLectureEventViewModel.setStartTime("");


                if (sdf.parse(sdf.format(c.getTime())).before(getCurrentDate())) {
                    getView().displayError("Invalid date");
                    textView.setText("");
                    createEditLectureEventViewModel.setStartDate("");
                } else {
                    textView.setText(sdf.format(c.getTime()));
                    createEditLectureEventViewModel.setStartDate(sdf.format(c.getTime()));
                }

            } else {
                createEditLectureEventViewModel.setEndTime("");
                if (sdf.parse(sdf.format(c.getTime())).before(sdf.parse(createEditLectureEventViewModel.getStartDate()))) {
                    getView().displayError("Invalid date");
                } else {
                    textView.setText(sdf.format(c.getTime()));
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void updateLabelTime(TextView textView, boolean isEndTime) {
        try {
            String dateFormat = "dd MMM yyyy";

            String timeFormat = "hh:mm a";
            SimpleDateFormat sdf = new SimpleDateFormat(timeFormat);

            SimpleDateFormat dateSDF = new SimpleDateFormat(dateFormat);
            if (isEndTime) {

                if (((dateSDF.parse(createEditLectureEventViewModel.getStartDate())).equals(dateSDF.parse(createEditLectureEventViewModel.getEndDate())))
                        &&
                        (sdf.parse(sdf.format(c.getTime())).before(sdf.parse(createEditLectureEventViewModel.getStartTime()))
                                ||
                                sdf.parse(sdf.format(c.getTime())).equals(sdf.parse(createEditLectureEventViewModel.getStartTime()))
                        )) {
                    getView().displayError("Select time greater than start time");
                } else {
                    textView.setText(sdf.format(c.getTime()));
                }

            } else {
                createEditLectureEventViewModel.setEndDate("");
                createEditLectureEventViewModel.setEndTime("");
           /* if(sdf.parse(sdf.format(c.getTime())).before(getCurrentTime())
                    ||
                    sdf.parse(sdf.format(c.getTime())).equals(getCurrentTime())
                    ) {
                getView().displayError("Select time greater than current time");
            }
            else{*/
                textView.setText(sdf.format(c.getTime()));
           /* }*/
            }
        } catch (Exception ex) {
            Log.e("Exception is", ex.toString());
        }
    }

    @Override
    public void selectStartTime(final View view) {
        showStartTimePicker((TextView) view);
    }

    protected void showStartTimePicker(final TextView view) {
        if (createEditLectureEventViewModel.getStartDate().trim().isEmpty()) {
            getView().displayError("Select start date.");
        } else {
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getView().getActivityG(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    c.set(Calendar.HOUR, selectedHour);
                    c.set(Calendar.MINUTE, selectedMinute);
//                ((TextView) view).setText(selectedHour + ":" + selectedMinute);
                    updateLabelTime(view, false);
                }
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        }
    }


    protected void showEndTimePicker(final TextView view) {
        if (createEditLectureEventViewModel.getEndDate().trim().isEmpty()) {
            getView().displayError("Select end date.");
        } else {
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getView().getActivityG(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    c.set(Calendar.HOUR, selectedHour);
                    c.set(Calendar.MINUTE, selectedMinute);
//                ((TextView) view).setText(selectedHour + ":" + selectedMinute);
                    updateLabelTime(view, true);
                }
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        }
    }


    @Override
    public void selectEndDate(View view) {
        showEndDatePicker((TextView) view);
    }

    @Override
    public void selectEndTime(View view) {
        showEndTimePicker((TextView) view);
    }

    @Override
    public void selectLocation(View view) {
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            ((Activity) getView().getActivityG()).startActivityForResult(builder.build((Activity) getView().getActivityG()), 101);
        } catch (Exception | Error e) {
            e.printStackTrace();
        }
    }

    public void locationPickerResult(Intent data) {
        Place place = PlacePicker.getPlace(getView().getActivityG(), data);

        locationDetails = new LocationG();
        locationDetails.setLocationName(place.getName().toString());
        locationDetails.setLongitude(place.getLatLng().longitude);
        locationDetails.setLatitude(place.getLatLng().longitude);
        createEditLectureEventViewModel.setLocationG(locationDetails.getLocationName());
    }


    public void setSelectedCoHost(CoHostListModel.SingleCoHostData selectedCoHost) {
        createEditLectureEventViewModel.setSelectedCoHost(selectedCoHost);
    }

    public void setSelectedLectureCategories(ArrayList<ChildCategoryModel> alCategories, String Ids) {
        createEditLectureEventViewModel.setAlCategories(alCategories);
        commaSeparatedIds = Ids;
    }

    public Date getCurrentDate() {
        SimpleDateFormat serverDate = new SimpleDateFormat("dd MMM yyyy");
        try {
            return serverDate.parse(serverDate.format(new Date(System.currentTimeMillis())));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Date getCurrentTime() {
        SimpleDateFormat serverDate = new SimpleDateFormat("hh:mm a");
        try {
            return serverDate.parse(serverDate.format(new Date(System.currentTimeMillis())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
