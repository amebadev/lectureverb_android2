package gilm.lecture.verb.Features.Messaging.Chat;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * Created by harpreet on 11/3/17.
 */

public class ChatModel extends BasicApiModel {

    @SerializedName("data")
    ChatDetailsModel mChatData=new ChatDetailsModel();

    public ChatDetailsModel getmChatList() {
        return mChatData;
    }

    public void setmChatList(ChatDetailsModel mChatData) {
        this.mChatData = mChatData;
    }
}
