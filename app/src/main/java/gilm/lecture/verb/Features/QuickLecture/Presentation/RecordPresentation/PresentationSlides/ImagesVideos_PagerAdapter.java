package gilm.lecture.verb.Features.QuickLecture.Presentation.RecordPresentation.PresentationSlides;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.QuickLecture.Presentation.PresentationOptions.SingleImageModel;
import gilm.lecture.verb.R;

public class ImagesVideos_PagerAdapter extends PagerAdapter
{
    private Context con;
    LayoutInflater inflater;

    private List<SingleImageModel> al_images = new ArrayList<>();

    public ImagesVideos_PagerAdapter(Context con, List<SingleImageModel> al_images) {
        this.con = con;
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.al_images = al_images;
    }

    @Override
    public int getCount() {
        return al_images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup row, final int position) {
        View v = inflater.inflate(R.layout.inflater_dialog_pager, null);

        ImageView rimgv_image = (ImageView) v.findViewById(R.id.rimgv_image);

        try {
            if (al_images.get(position).getDownload_file().toLowerCase().contains("http")) {
                Picasso.with(con)
                        .load(al_images.get(position).getDownload_file())
                        .into(rimgv_image);
            }
            else {
                Picasso.with(con)
                        .load(new File(al_images.get(position).getDownload_file()))
                        .into(rimgv_image);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        row.addView(v);

        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}