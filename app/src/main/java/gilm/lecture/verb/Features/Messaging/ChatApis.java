package gilm.lecture.verb.Features.Messaging;

import gilm.lecture.verb.Features.Messaging.Chat.ChatModel;
import gilm.lecture.verb.Features.Messaging.Chat.ChatsListModel;
import gilm.lecture.verb.Features.Messaging.RecentChat.ListOfRecentChatsModel;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.Web;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by harpreet on 11/2/17.
 */

public interface ChatApis
{

    @FormUrlEncoded
    @POST(Web.Path.get_chat_messages)
    Call<ChatsListModel> get_chat_messages(@Field("user_id") String user_id, @Field("other_user_id") String other_user_id, @Field("page") String page);

    @FormUrlEncoded
    @POST(Web.Path.get_recent_chats)
    Call<ListOfRecentChatsModel> get_recent_chats(@Field("user_id") String user_id, @Field("page") String page);

    @Multipart
    @POST(Web.Path.chat)
    Call<ChatModel> saveChat(
            @Part MultipartBody.Part chatmedia,
            @Part("sender_id") RequestBody sender_id
            , @Part("receiver_id") RequestBody receiver_id
            , @Part("message") RequestBody message
            , @Part("message_type") RequestBody message_type // comment_type - f-file,t-text
            , @Part("extension") RequestBody file_extension
            , @Part("sender_name") RequestBody sender_name
    );





    @FormUrlEncoded
    @POST(Web.Path.delete_chat_message)
    Call<BasicApiModel> delete_chat_message(@Field("user_id") String user_id, @Field("chat_id") String chat_id);


    @FormUrlEncoded
    @POST(Web.Path.block_unblock_user)
    Call<BasicApiModel> block_unblock_user(@Field("blocked_user_id") String blocked_user_id,@Field("user_id") String user_id, @Field("action") String action);


}
