package gilm.lecture.verb.Features.Player;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.czt.mp3recorder.MP3Recorder;
import com.shuyu.waveview.AudioPlayer;
import com.shuyu.waveview.FileUtils;

import java.io.File;
import java.io.IOException;

import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments.CommentsList;
import gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments.CommentsModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.FileHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import gilm.lecture.verb.WebServices.Web;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harpreet on 10/16/17.
 */

public class CommentsReplyDailog implements ProgressRequestBody.UploadCallbacks
{

    Activity mActivity;
    TextView txtvTimer;
    ImageView imgv_Play;
    private boolean mIsRecord;
    AudioPlayer audioPlayer;
    private boolean mIsPlay;
    private MP3Recorder mRecorder;
    private String audioFilePath = "";


    public CommentsReplyDailog(Activity mActivity) {
        this.mActivity = mActivity;
    }

    public void initRecorder() {
        audioPlayer = new AudioPlayer(mActivity, new Handler()
        {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case AudioPlayer.HANDLER_CUR_TIME:
                        break;
                    case AudioPlayer.HANDLER_COMPLETE:
                        mIsPlay = false;
                        imgv_Play.setImageResource(android.R.drawable.ic_media_play);
                        txtvTimer.setText("Finished");
                        break;
                    case AudioPlayer.HANDLER_PREPARED:
                        break;
                    case AudioPlayer.HANDLER_ERROR:
                        mIsPlay = false;
                        txtvTimer.setText("Playback Error");
                        break;
                }

            }
        });
    }

    private void resetEverything() {
        try {
            stopAndResetUI();
            audioFilePath = "";

            if (audioPlayer != null) {
                audioPlayer.stop();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void pausePlayer() {
        if (audioPlayer != null) {
            audioPlayer.pause();
        }
    }

    public void pauseRecording() {
        if (mRecorder.isRecording()) {
            mIsRecord = false;
            mRecorder.setPause(true);
            isRecordingBoolean = false;
        }
        else {
            startRecording();
        }
    }


    private void startRecording() {
        if (mIsPlay) {
            mIsPlay = false;
        }

        if (audioFilePath.isEmpty()) {
            audioFilePath = FileHelper.getInstance().createAudioFile("PK" + System.currentTimeMillis() + ".m4a").getAbsolutePath();
        }
        if (mRecorder == null) {
            mRecorder = new MP3Recorder(new File(audioFilePath));
            mRecorder.stop();
        }
        else {
            if (mRecorder.isPause()) {
                mIsRecord = true;
                mRecorder.setPause(false);

                return;
            }
        }
        mRecorder.setErrorHandler(new Handler()
        {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == MP3Recorder.ERROR_TYPE) {
                    resolveErrorDelete();
                    audioFilePath = "";
                    mRecorder = null;
                    return;
                }
            }
        });

        try {
            mRecorder.start();
            mIsRecord = true;
            isRecordingBoolean = true;
        }
        catch (IOException e) {
            e.printStackTrace();
            resolveErrorDelete();
        }
    }

    int hours, minutes, seconds;
    public boolean isRecordingBoolean;

    public void startTimer() {
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run() {
                //checking if recording is starting from start
                if (txtvTimer.getText().toString().trim().equalsIgnoreCase("record")) {
                    seconds = 0;
                    minutes = 0;
                    hours = 0;
                }
                seconds++;
                if (seconds == 60) {
                    seconds = 0;
                    minutes++;
                    if (minutes == 60) {
                        minutes = 0;
                        hours++;
                    }
                }
                if (minutes == 0) {
                    txtvTimer.setText("00:" + (seconds < 10 ? ("0" + seconds)
                            : seconds));
                }
                else if (hours == 0 && minutes > 0) {
                    txtvTimer.setText((minutes < 10 ? ("0" + minutes)
                            : minutes) + ":" + (
                            seconds < 10 ? ("0" + seconds) : seconds));
                }
                else if (hours > 0) {
                    txtvTimer.setText(hours + ":" + (minutes < 10 ? ("0" + minutes)
                            : minutes));
                }
                try {
                    if (isRecordingBoolean) {
                        startTimer();
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1000);

    }

    private void resolvePlayRecord() {
        if (!mIsRecord) {
            if (mIsPlay) {
                mIsPlay = false;
                pausePlayer();
                imgv_Play.setImageResource(android.R.drawable.ic_media_play);
                txtvTimer.setText("Stopped");
            }
            else {
                mIsPlay = true;
                if (audioPlayer != null) {
                    audioPlayer.playUrl(audioFilePath);
                    imgv_Play.setImageResource(R.drawable.ic_stop_music);
                    txtvTimer.setText("Playing");
                }
            }
        }
        else {
            UtillsG.showToast("Stop recording first..!", mActivity, false);
        }
    }

    protected void stopAndResetUI() {
        if (mIsRecord) {
            StopRecord();
        }
        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder = null;
        }
        isRecordingBoolean = true;
        if (mIsPlay) {
            mIsPlay = false;
        }
    }

    private void StopRecord() {
        if (mRecorder != null && mRecorder.isRecording()) {
            mRecorder.setPause(false);
            mRecorder.stop();
        }
        mIsRecord = false;
    }

    private void resolveErrorDelete() {
        stopAndResetUI();
        mRecorder = null;
        FileUtils.deleteFile(audioFilePath);
        audioFilePath = "";
        hours = 0;
        minutes = 0;
        seconds = 0;
    }


    public void showPostDialog(final Lecture_And_Event_Model mLectureDetails, final Boolean isCommentReply, final CallBackG<CommentsModel> commentAdded) {

        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.daliog_add_lecture_comments);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#7F000000")));

        initRecorder();

        final LinearLayout layRecording = (LinearLayout) dialog.findViewById(R.id.layRecording);
        final CardView cardView = (CardView) dialog.findViewById(R.id.cardView);
        final TextView txtv_lectureTitle = (TextView) dialog.findViewById(R.id.txtv_lectureTitle);
        final EditText edLectureTitle = (EditText) dialog.findViewById(R.id.edLectureTitle);

        imgv_Play = (ImageView) dialog.findViewById(R.id.imgv_Play);
        final ImageView imgvRecord = (ImageView) dialog.findViewById(R.id.imgvRecord);
        TextView txtvRecord = (TextView) dialog.findViewById(R.id.txtvRecord);
        TextView txtvCancel = (TextView) dialog.findViewById(R.id.txtvCancel);
        TextView txtvPost = (TextView) dialog.findViewById(R.id.txtvPost);
        txtvTimer = (TextView) dialog.findViewById(R.id.txtvTimer);

        txtv_lectureTitle.setText(mLectureDetails.getQuick_lecture_text());

        imgvRecord.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if (mIsRecord) {
                    pauseRecording();

                    imgvRecord.setVisibility(View.GONE);
                    imgv_Play.setVisibility(View.VISIBLE);

                }
                else {
                    new Handler().post(new Runnable()
                    {
                        @Override
                        public void run() {
                            startRecording();
                            startTimer();

                            imgvRecord.setImageResource(
                                    isRecordingBoolean ? R.drawable.ic_red_dot
                                            : R.drawable.ic_green_dot);


                        }
                    });
                }

//                imgvRecord.setVisibility(isRecordingBoolean ? View.VISIBLE : View.GONE);
            }
        });
        txtvRecord.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                layRecording.setVisibility(View.VISIBLE);
            }
        });
        imgv_Play.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                resolvePlayRecord();
            }
        });
        txtvCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                UtillsG.hideKeyboard(mActivity, v);
                if (!mIsRecord) {
                    dialog.dismiss();
                }
                else {
                    UtillsG.showToast("Stop recording first..!", mActivity, false);
                }
            }
        });
        txtvPost.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                String title = edLectureTitle.getText().toString().trim();

                if (title.isEmpty() && audioFilePath.trim().isEmpty()) {
                    UtillsG.showToast("Please enter atleast a title or audio recording", mActivity, true);
                }
                else {

                    if (!mIsRecord) {
                        UtillsG.hideKeyboard(mActivity, v);
                        if (isCommentReply) {
                            if (audioFilePath.trim().isEmpty()) {
                                PostLectureCommentReply("", mLectureDetails.getQuick_lecture_id(), title, "t", "", dialog);
                            }
                            else {
                                PostLectureCommentReply(audioFilePath, mLectureDetails.getQuick_lecture_id(), title, "f", "", dialog);
                            }
                        }
                        else {
                            if (audioFilePath.trim().isEmpty()) {
                                PostLectureComment(mActivity, "", mLectureDetails.getQuick_lecture_id(), title, "t", "", "", dialog, commentAdded);
                            }
                            else {
                                PostLectureComment(mActivity, audioFilePath, mLectureDetails.getQuick_lecture_id(), title, "f", Web.File_Options.AUDIO, "", dialog, commentAdded);
                            }
                        }
                    }
                    else {
                        UtillsG.showToast("Stop recording first..!", mActivity, false);
                    }
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.horizontalMargin = 10;
        dialog.getWindow().setAttributes(lp);
        dialog.show();


        dialog.setOnDismissListener(new DialogInterface.OnDismissListener()
        {
            @Override
            public void onDismiss(DialogInterface dialog) {
                resetEverything();
            }
        });
    }


    public void PostLectureComment(final Activity mActivity, String audioPath, String lectureId, String comment, String commentType,
                                   final String fileType, String comment_thumb, final Dialog mdialog, final CallBackG<CommentsModel> commentAdded) {

        if (mActivity != null) {
            UtillsG.showLoading("Please wait", mActivity);
        }

        Call<CommentsList> quickLectureApiModelCall = LVApplication.getRetrofit().create(CommentsApis.class).lecture_write_comment(
                UtillsG.getNotNullString(audioPath, "").isEmpty()
                        ? null
                        : getFilePart(new File(audioPath), "comment_file"),
                UtillsG.getNotNullString(comment_thumb, "").isEmpty()
                        ? null
                        : getFilePart(new File(comment_thumb), "comment_thumb"),
                getOtherParams(lectureId),
                getOtherParams(new SharedPrefHelper(mActivity).getUserId()),
                getOtherParams(comment)
                , getOtherParams(commentType)
                , getOtherParams(fileType));
        quickLectureApiModelCall.enqueue(new Callback<CommentsList>()
        {
            @Override
            public void onResponse(Call<CommentsList> call, final Response<CommentsList> response) {
                UtillsG.hideLoading();
                if (response.body() != null) {

                    if (response.body().getStatus()) {

                        resolveErrorDelete();
                        if (mActivity != null) {
                            UtillsG.showToast("Comment posted successfully", mActivity, true);
                        }
                        if (mdialog != null) {
                            mdialog.dismiss();
                        }
                        commentAdded.onCallBack(response.body().getData().get(0));
                    }
                    else {
                        if (mActivity != null) {
                            UtillsG.showToast(response.message(), mActivity, true);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CommentsList> call, Throwable t) {
                UtillsG.hideLoading();
                Log.e("Error", t.toString());
                resolveErrorDelete();
                UtillsG.showToast("Error in creating comment on quick lecture", mActivity, true);
            }
        });
    }


    public void PostLectureCommentReply(String audioPath, String lectureId, String comment, String commentType, String commentId, final Dialog mdialog) {


        UtillsG.showLoading("Please wait", mActivity);

        Call<CommentsList> quickLectureApiModelCall = LVApplication.getRetrofit().create(CommentsApis.class).write_comment_reply(
                UtillsG.getNotNullString(audioPath, "").isEmpty()
                        ? null
                        : getFilePart(new File(audioPath), "comment_file"),
                getOtherParams(lectureId),
                getOtherParams(new SharedPrefHelper(mActivity).getUserId()),
                getOtherParams(commentId),
                getOtherParams(comment)
                , getOtherParams(commentType));
        quickLectureApiModelCall.enqueue(new Callback<CommentsList>()
        {
            @Override
            public void onResponse(Call<CommentsList> call, final Response<CommentsList> response) {
                UtillsG.hideLoading();
                if (response.body() != null) {
                    if (response.body().getStatus()) {
                        resolveErrorDelete();
                        UtillsG.showToast("Comment posted successfully", mActivity, true);
                        if (mdialog != null) {
                            mdialog.dismiss();
                        }
                    }
                    else {
                        UtillsG.showToast(response.message(), mActivity, true);
                    }
                }
            }

            @Override
            public void onFailure(Call<CommentsList> call, Throwable t) {
                UtillsG.hideLoading();
                Log.e("Error", t.toString());
                resolveErrorDelete();
                UtillsG.showToast("Error in creating comment on quick lecture", mActivity, true);
            }
        });
    }


    private MultipartBody.Part getFilePart(final File file, String fileType) {
        ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(fileType, file.getName(), fileBody);
        return filePart;
    }

    public void showRecordingDialog(final Lecture_And_Event_Model mLectureDetails, final String textComment, final CallBackG<CommentsModel> commentAdded) {

        // Stopping lecture audio file so that user can record an audio comment
        audioFilePath = "";
        if (PublicAudioPlayer.getAudioPlayer() != null && !PublicAudioPlayer.getAudioPlayer().isPause()) {
            PublicAudioPlayer.getAudioPlayer().setPause(true);
        }

        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.daliog_record_audio);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#7F000000")));

        initRecorder();

        ImageView imgvClose = (ImageView) dialog.findViewById(R.id.imgvClose);
        imgv_Play = (ImageView) dialog.findViewById(R.id.imgv_Play);
        final ImageView imgvRecord = (ImageView) dialog.findViewById(R.id.imgvRecord);
        txtvTimer = (TextView) dialog.findViewById(R.id.txtvTimer);
        TextView txtvPost = (TextView) dialog.findViewById(R.id.txtvPost);

        imgvClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {

                if (mIsRecord) {
                    UtillsG.showToast("Stop recording first..!", mActivity, true);
                    return;
                }
                seconds = 0;
                minutes = 0;
                hours = 0;
                isRecordingBoolean = false;

                resetEverything();
                dialog.dismiss();
            }
        });

        imgvRecord.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if (mIsRecord) {
                    pauseRecording();

                    imgvRecord.setVisibility(View.GONE);
                    imgv_Play.setVisibility(View.VISIBLE);

                    imgvRecord.clearAnimation();
                }
                else {
                    new Handler().post(new Runnable()
                    {
                        @Override
                        public void run() {
                            startRecording();
                            startTimer();

                            imgvRecord.setBackgroundResource(
                                    isRecordingBoolean ? R.drawable.ic_red_dot
                                            : R.drawable.ic_green_dot);

                            Animation pulse = AnimationUtils.loadAnimation(mActivity, R.anim.heart_beat_anim);
                            imgvRecord.startAnimation(pulse);

                        }
                    });
                }
            }
        });
        imgv_Play.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                resolvePlayRecord();
            }
        });
        txtvPost.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if (!mIsRecord) {
                    if (audioFilePath.trim().isEmpty() && textComment.trim().isEmpty()) {
                        UtillsG.showToast("Please record audio or enter text to add a comment.", mActivity, false);
                    }
                    else if (audioFilePath.trim().isEmpty()) {
                        PostLectureComment(mActivity, "", mLectureDetails.getQuick_lecture_id(), textComment, "t", "", "", dialog, commentAdded);
                    }
                    else {
                        PostLectureComment(mActivity, audioFilePath, mLectureDetails.getQuick_lecture_id(), textComment, "f", Web.File_Options.AUDIO, "", dialog, commentAdded);
                    }
                }
                else {
                    UtillsG.showToast("Stop recording first..!", mActivity, false);
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.horizontalMargin = 10;
        dialog.getWindow().setAttributes(lp);
        dialog.show();


        dialog.setOnDismissListener(new DialogInterface.OnDismissListener()
        {
            @Override
            public void onDismiss(DialogInterface dialog) {
                resetEverything();

                //playing lecture audio after recording audio message.
                /*if (PublicAudioPlayer.getAudioPlayer() != null && PublicAudioPlayer.getAudioPlayer().isPause()) {
                    PublicAudioPlayer.getAudioPlayer().setPause(false);
                }*/
            }
        });
    }


    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
