package gilm.lecture.verb.Features.QuickLecture.Presentation.RecordPresentation;

import android.app.Activity;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.MediaRecorder;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Surface;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.FileHelper;
import gilm.lecture.verb.UtilsG.UtillsG;

import static com.facebook.GraphRequest.TAG;

/**
 * created by PARAMBIR SINGH on 19/8/17.
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class RecordPresentationPresenter extends BasePresenter<RecordPresentationView> {
    public boolean isRecording = false;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    private static final int DISPLAY_WIDTH = 320;//720;
    private static final int DISPLAY_HEIGHT = 480;//1280;
    public int screenDensity;

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }


    public RecordPresentationPresenter() {
    }

    public void destroyMediaProjection() {
        if (getView().getMediaProjection() != null) {
            getView().getMediaProjection().unregisterCallback(getView().getMediaProjectionCallBack());
            getView().getMediaProjection().stop();
            getView().setMediaProjectionNull();
        }
    }

    @Override
    public void detachView() {
        destroyMediaProjection();
        super.detachView();
    }

    public void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        if (requestCode != Constants.RequestCode.SCREEN_RECORDER) {
            return;
        }
        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(getView().getActivityG(),
                    "Screen Cast Permission Denied", Toast.LENGTH_SHORT).show();
            isRecording = true;
            getView().permissionDenied();
            return;
        }

        DialogHelper.getInstance().showInformation(getView().getActivityG(), "Please Note : \nYou can stop recording by pressing back button.", new CallBackG<String>() {
            @Override
            public void onCallBack(String output) {
                getView().hideRecordingButton(); // hiding recording button because all screen will be recorded.
                getView().startTimer(); // timer on the PPT recording screen .. where the slides will be shown up
                getView().hideNotificationBar();

                getView().initMediaProjectionCallBack(resultCode, data);
            }
        });


    }

    public void initRecorder() {
        try {
//            String videoPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/presentationVideo.mp4";
            String videoPath = FileHelper.getInstance().getNewVideoFilePath();
            new FileOutputStream(videoPath, false).close();

            getView().setVideoPath(videoPath);
            getView().getRecorder().setAudioSource(MediaRecorder.AudioSource.MIC);
            getView().getRecorder().setVideoSource(MediaRecorder.VideoSource.SURFACE);
            getView().getRecorder().setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            getView().getRecorder().setOutputFile(videoPath);
            getView().getRecorder().setVideoSize(DISPLAY_WIDTH, DISPLAY_HEIGHT);
            getView().getRecorder().setVideoEncoder(MediaRecorder.VideoEncoder.H264);
            getView().getRecorder().setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            getView().getRecorder().setVideoEncodingBitRate(512 * 1000);
            getView().getRecorder().setVideoFrameRate(30);
            int rotation = ((Activity) getView().getActivityG()).getWindowManager().getDefaultDisplay().getRotation();
            int orientation = ORIENTATIONS.get(rotation + 90);
            getView().getRecorder().setOrientationHint(orientation);
            getView().getRecorder().prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public VirtualDisplay createVirtualDisplay() {
        return getView().getMediaProjection().createVirtualDisplay("MainActivity",
                DISPLAY_WIDTH, DISPLAY_HEIGHT, screenDensity,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
                getView().getRecorder().getSurface(), null /*Callbacks*/, null
                /*Handler*/);

    }

    public void onToggleScreenShare() {
        if (!isRecording) {
            initRecorder();
            getView().shareScreen();
            isRecording = true;
        } else {
            stopRecording();
            isRecording = false;
            UtillsG.showToast("Recording Completed.", getView().getActivityG(), true);
            detachView();
        }
    }

    public void stopRecording() {
        try {
            getView().getRecorder().stop();
        }
        catch (Exception e){}
        try {
            getView().getRecorder().reset();
        }
        catch (Exception e){}
        Log.v(TAG, "Stopping Recording");
        getView().stopScreenSharing();
    }


}
