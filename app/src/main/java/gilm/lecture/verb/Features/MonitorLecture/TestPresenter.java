package gilm.lecture.verb.Features.MonitorLecture;

import android.databinding.ObservableField;
import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;

/**
 * created by PARAMBIR SINGH on 12/4/18.
 */

public class TestPresenter extends BasePresenter<TestView> implements TestBinder
{
    public ObservableField<String> First=new ObservableField<>();
    public ObservableField<String> Second=new ObservableField<>();


public  TestPresenter()
{First.set("");
Second.set("");
}
    @Override
    public void click(View view) {

            getView().showLoading("Please l");


        if(First.get().isEmpty()) {
getView().displayError("Please Enter First Value");
        }
        else if(Second.get().isEmpty())
        {
            getView().displayError("Please Enter Second Value");

        }
        else
        {
            getView().hideLoading();
            getView().displayError("Good Try");
            getView().finiss();
        }


    }

    @Override
    public ObservableField<String> getFirst() {
        return First;
    }

    @Override
    public ObservableField<String> getSecond() {
        return Second;
    }
}
