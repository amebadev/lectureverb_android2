/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Settings;

import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;
import gilm.lecture.verb.Features.Settings.MainSettings.SettingsFragment;
import gilm.lecture.verb.UtilsG.TitleNavigator;

/**
 * Created by G-Expo on 20 Jul 2017.
 */

public class SettingBasePresenter<T extends Viewable> extends BasePresenter<T> implements SettingsBaseBinder
{
    public ObservableField<String> profileImage = new ObservableField<>();
    public ObservableField<String> coverProfileImage = new ObservableField<>();
    public ObservableField<String> name = new ObservableField<>();

    public SettingBasePresenter() {
        name.set("");
        profileImage.set("");
        coverProfileImage.set("");
    }

    public void initLocalData() {
        name.set(getView().getLocalData().getUserName());
        profileImage.set(getView().getLocalData().getProfilePic());
        coverProfileImage.set(getView().getLocalData().getCoverProfilePic());
    }

    public void refreshUserData() {
        //refresh data from local storage here.
        initLocalData();
    }

    @Override
    public void attachView(@NonNull T viewable) {
        super.attachView(viewable);
        initLocalData();
    }

    @Override
    public void openMainSettings(View view) {
        TitleNavigator.getInstance().showMainSettings(SettingsFragment.newInstance());
    }

    @Override
    public ObservableField<String> profileImage() {
        return profileImage;
    }

    @Override
    public ObservableField<String> coverProfileImage() {
        return coverProfileImage;
    }

    @Override
    public ObservableField<String> name() {
        return name;
    }
}
