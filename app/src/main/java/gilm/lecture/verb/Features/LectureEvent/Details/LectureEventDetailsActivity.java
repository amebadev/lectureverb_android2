package gilm.lecture.verb.Features.LectureEvent.Details;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.LectureEvent.AttendingLecture;
import gilm.lecture.verb.Features.LectureEvent.Details.About.AboutFragment;
import gilm.lecture.verb.Features.LectureEvent.Details.Discussion.DiscussionFragment;
import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsApi;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureEventModel;
import gilm.lecture.verb.Features.TabAdapter.FragmentTabModel;
import gilm.lecture.verb.Features.TabAdapter.SectionsPagerAdapter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DateHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.ActivityLectureEventDetailsBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LectureEventDetailsActivity extends BaseActivity<ActivityLectureEventDetailsBinding, LectureEventDetailsPresenter> implements LectureEventDetailsView
{
    LectureEventViewModel lectureEventViewModel;
    TextView createdBy;
    private boolean iAmCoHost;
    private boolean iAmOwner;
    private boolean iAmLecturer;

    public static <T> void start(Context context, T data)
    {
        Intent starter = new Intent(context, LectureEventDetailsActivity.class);
        if (data != null)
        {
            starter.putExtra(Constants.Extras.DATA, (Parcelable) (data));
        }
        context.startActivity(starter);
    }

    /**
     * THIS SCREEN WILL BE ONLY OPENED IF THE is_completed PARAMETER IS = 0 MEANS IF THE EVENT IS NOT YET COMPLETED .
     * A COMPLETED EVENT MEANS THAT IS RECORDED AND UPLOADED AS QUICK LECTURE
     *
     * @param context
     * @param data
     * @param view
     * @param mLectureDetails
     * @param <T>
     */
    public static <T> void start(Context context, T data, View view, LectureDetailsModel mLectureDetails)
    {
        // Only open this screen if the event is not yet completed.
        if (mLectureDetails.getIs_completed() == 0)
        {
            Intent starter = new Intent(context, LectureEventDetailsActivity.class);
            if (data != null)
            {
                starter.putExtra(Constants.Extras.DATA, (Parcelable) (data));
            }
            starter.putExtra("mLectureDetails", mLectureDetails);

            UtillsG.startTransition((Activity) context, starter, view);
        } else
        {
            UtillsG.showToast("This event has been completed.", context, true);
        }

//        context.startActivity(starter);
    }

    public static void startActivityWithApiCall(Context context, String lectureId, View view)
    {
        Intent starter = new Intent(context, LectureEventDetailsActivity.class);
        starter.putExtra("lectureId", lectureId);
        UtillsG.startTransition((Activity) context, starter, view);
    }

    @Override
    protected int setLayoutId()
    {
        return R.layout.activity_lecture_event_details;
    }

    @Override
    protected void onCreateActivityG()
    {
        injectPresenter(new LectureEventDetailsPresenter());
        getPresenter().attachView(this);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == 191)
            {
                EventBus.getDefault().post(new HomeFragmentUpdateBus(true));
                LectureEventViewModel mViewModel = getLectureDetails((LectureDetailsModel) data.getSerializableExtra("mLectureDetails"));
                getDataBinder().setData(mViewModel);
                getDataBinder().setBinder(getPresenter());
                getPresenter().setLectureModel(mViewModel.getmLectureEvent());

            }
        }
    }

    public LectureEventViewModel getLectureDetails(LectureDetailsModel lecDetails)
    {

        LectureDetailsModel mLectureDetails = lecDetails;
        LectureEventViewModel lectureEventViewModel = new LectureEventViewModel();
        lectureEventViewModel.setmLectureEvent(mLectureDetails);
        lectureEventViewModel.setLecture_event_id(mLectureDetails.getEvent_id());
        lectureEventViewModel.setTitle(mLectureDetails.getLecture_title());
        lectureEventViewModel.setStartTime(DateHelper.convertFromServerDateToRequiredDate(mLectureDetails.getLecture_start_date_time(), "dd MMM"));
        lectureEventViewModel.setImageUrl(mLectureDetails.getLecture_poster());
        lectureEventViewModel.setMessage(mLectureDetails.getDetails());
        lectureEventViewModel.setLocation(mLectureDetails.getAddress());
        lectureEventViewModel.setTimeRange(
                "Start Date Time : " + DateHelper.convertFromServerDateToRequiredDate(mLectureDetails.getLecture_start_date_time(), "dd-MM-yyyy HH:mm a")
                        + "\n" +
                        "End Date Time : " + DateHelper.convertFromServerDateToRequiredDate(mLectureDetails.getLecture_end_date_time(), "dd-MM-yyyy HH:mm a"));
        lectureEventViewModel.setTimeRange(mLectureDetails.getLecture_created_date_time() + "- " + mLectureDetails.getLecture_end_date_time());


        if (lecDetails.getUser_id().equals(new SharedPrefHelper(getActivityG()).getUserId()))
        {
            lectureEventViewModel.setUpcoming(false);
        } else
        {
            lectureEventViewModel.setUpcoming(true);
        }
        lectureEventViewModel.setCanInviteGuest(
                mLectureDetails.getCan_invite_friends().equals("true") ? true : false);

        if (mLectureDetails.getmIsInterested() != null)
        {

            if (mLectureDetails.getmIsInterested().getAttend_type().equals("digital"))
            {
                lectureEventViewModel.setAttendingLecture(AttendingLecture.GOING_DIGITAL);
            } else if (mLectureDetails.getmIsInterested().getAttend_type().equals("attending"))
            {
                lectureEventViewModel.setAttendingLecture(AttendingLecture.ATTENDING);
            } else
            {
                lectureEventViewModel.setAttendingLecture(AttendingLecture.NO_OUTPUT);
            }


            if (mLectureDetails.getmIsInterested().getIs_interested().equals("true"))
            {
                lectureEventViewModel.setIsInterested(AttendingLecture.IS_INTERESTED);
            } else if (mLectureDetails.getmIsInterested().getIs_interested().equals("false"))
            {
                lectureEventViewModel.setIsInterested(AttendingLecture.IS_IGNORED);
            } else
            {
                lectureEventViewModel.setIsInterested(AttendingLecture.NO_OUTPUT);
            }


            lectureEventViewModel.setIgnored(false);
        } else
        {
            lectureEventViewModel.setAttendingLecture(AttendingLecture.NO_OUTPUT);
            lectureEventViewModel.setIsInterested(AttendingLecture.NO_OUTPUT);
            lectureEventViewModel.setIgnored(false);
        }

        return lectureEventViewModel;

    }

    @Override
    public void closeActivity()
    {
        finish();
    }

    @Override
    public void initViews()
    {
        setupToolbar("");
        createdBy = (TextView) findViewById(R.id.createdBy);
        if (getIntent().getStringExtra("lectureId") != null)
        {
            getLectureEvent(getIntent().getStringExtra("lectureId"));
        } else
        {
            lectureEventViewModel = getIntent().getParcelableExtra(Constants.Extras.DATA);
            getPresenter().setLectureModel((LectureDetailsModel) getIntent().getSerializableExtra("mLectureDetails"));
            lectureEventViewModel.setmLectureEvent((LectureDetailsModel) getIntent().getSerializableExtra("mLectureDetails"));
            setupTabBar();
            getDataBinder().setData(lectureEventViewModel);
            getDataBinder().setBinder(getPresenter());

            createdBy.setText("Created By: " + getPresenter().getLectureModel().getUserdata().getFull_name());

            if (getPresenter().mLectureViewModel != null && getPresenter().mLectureViewModel.getLecture_start_date_time() != null && getPresenter().mLectureViewModel.getLecture_end_date_time() != null)
            {
                getDataBinder().txtvEventTime.setText(
                        "Start Date Time : " + DateHelper.convertFromServerDateToRequiredDate(getPresenter().mLectureViewModel.getLecture_start_date_time(), "dd-MM-yyyy HH:mm a")
                                + "\n" +
                                "End Date Time : " + DateHelper.convertFromServerDateToRequiredDate(getPresenter().mLectureViewModel.getLecture_end_date_time(), "dd-MM-yyyy HH:mm a"));
            }

            iAmOwner = getPresenter().getLectureModel().getUserdata().getUser_id().equals(getLocalData().getUserId());
            iAmLecturer = getPresenter().getLectureModel().getLecturer_data() != null
                    && getPresenter().getLectureModel().getLecturer_data().getUser_id() != null
                    && getPresenter().getLectureModel().getLecturer_data().getUser_id().equalsIgnoreCase(getLocalData().getUserId());
            iAmCoHost = getPresenter().getLectureModel().getLecturer_co_hosts() != null
                    && !getPresenter().getLectureModel().getLecturer_co_hosts().isEmpty()
                    && getPresenter().getLectureModel().getLecturer_co_hosts().get(0).getUser_id() != null
                    && getPresenter().getLectureModel().getLecturer_co_hosts().get(0).getUser_id().equals(getLocalData().getUserId());

            getDataBinder().txtvEditEvent.setVisibility(iAmOwner ? View.VISIBLE : View.GONE);
            getDataBinder().viewEditEvent.setVisibility(
                    (iAmOwner && !iAmLecturer && !iAmCoHost) ? View.VISIBLE : View.GONE);

            getDataBinder().txtvMyRole.setVisibility(iAmOwner ? View.GONE : View.VISIBLE);

            getDataBinder().txtvGoingDigital.setVisibility(iAmOwner ? View.GONE : View.VISIBLE);
            getDataBinder().txtvInterested.setVisibility(iAmOwner ? View.GONE : View.VISIBLE);
            getDataBinder().vGoingDigital.setVisibility(iAmOwner ? View.GONE : View.VISIBLE);
            getDataBinder().vInterested.setVisibility(iAmOwner ? View.GONE : View.VISIBLE);

            String myRole = (getPresenter().getLectureModel().getLecturer_data() != null
                    && getPresenter().getLectureModel().getLecturer_data().getUser_id() != null
                    && iAmOwner
                    ? "Lecturer"
                    : (getPresenter().getLectureModel().getLecturer_co_hosts() != null
                    && getPresenter().getLectureModel().getLecturer_co_hosts().get(0).getUser_id() != null
                    && iAmOwner)
                    ? "Facilitator" : "Lecture Owner");
            getDataBinder().txtvMyRole.setText("You are appointed as " + myRole);
        }


    }

    public void updateViewModel(LectureEventViewModel lectureEventViewModel)
    {
        getDataBinder().setData(lectureEventViewModel);
        getPresenter().setLectureModel(lectureEventViewModel.getmLectureEvent());
    }


    @Override
    public Context getActivityG()
    {
        return LectureEventDetailsActivity.this;
    }


    private void setupTabBar()
    {
        ViewPager viewPager = (ViewPager) findViewById(R.id.container);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        //Creating our pager adapter

        List<FragmentTabModel> list = new ArrayList<>();
        list.add(new FragmentTabModel(AboutFragment.newInstance(getPresenter().getLectureModel()), "ABOUT"));
        list.add(new FragmentTabModel(DiscussionFragment.newInstance(), "DISCUSSION"));


        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager(), list);
        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void getLectureEvent(String event_id)
    {

        showLoading("Loading lecture details");
        Call<LectureEventModel> getLectureApiCall = LVApplication.getRetrofit().create(LectureEventsApi.class).get_single_lecture_event(event_id
                , new SharedPrefHelper(getActivityG()).getUserId());
        getLectureApiCall.enqueue(new Callback<LectureEventModel>()
        {
            @Override
            public void onResponse(Call<LectureEventModel> call, Response<LectureEventModel> response)
            {

                hideLoading();

                if (response != null && response.body() != null)
                {
                    LectureDetailsModel mLectureDetails = response.body().getmQuickLectureData().get(0);
                    LectureEventViewModel lectureEventViewModel1 = new LectureEventViewModel();
                    lectureEventViewModel1.setmLectureEvent(response.body().getmQuickLectureData().get(0));
                    lectureEventViewModel1.setLecture_event_id(mLectureDetails.getEvent_id());
                    lectureEventViewModel1.setTitle(mLectureDetails.getLecture_title());
                    lectureEventViewModel1.setStartTime(DateHelper.convertFromServerDateToRequiredDate(mLectureDetails.getLecture_start_date_time(), "dd MMM"));
                    lectureEventViewModel1.setImageUrl(mLectureDetails.getLecture_poster());
                    lectureEventViewModel1.setMessage(mLectureDetails.getDetails());
                    lectureEventViewModel1.setLocation(mLectureDetails.getAddress());
                    lectureEventViewModel1.setTimeRange(mLectureDetails.getLecture_created_date_time() + "- " + mLectureDetails.getLecture_end_date_time());
                    if (response.body().getmQuickLectureData().get(0).getUser_id().equals(new SharedPrefHelper(getActivityG()).getUserId()))
                    {
                        lectureEventViewModel1.setUpcoming(false);
                    } else
                    {
                        lectureEventViewModel1.setUpcoming(true);
                    }
                    lectureEventViewModel1.setCanInviteGuest(
                            mLectureDetails.getCan_invite_friends().equals("true") ? true : false);

                    if (mLectureDetails.getmIsInterested() != null)
                    {

                        if (mLectureDetails.getmIsInterested().getAttend_type().equals("digital"))
                        {
                            lectureEventViewModel1.setAttendingLecture(AttendingLecture.GOING_DIGITAL);
                        } else if (mLectureDetails.getmIsInterested().getAttend_type().equals("attending"))
                        {
                            lectureEventViewModel1.setAttendingLecture(AttendingLecture.ATTENDING);
                        } else
                        {
                            lectureEventViewModel1.setAttendingLecture(AttendingLecture.NO_OUTPUT);
                        }


                        if (mLectureDetails.getmIsInterested().getIs_interested().equals("true"))
                        {
                            lectureEventViewModel1.setIsInterested(AttendingLecture.IS_INTERESTED);
                        } else if (mLectureDetails.getmIsInterested().getIs_interested().equals("false"))
                        {
                            lectureEventViewModel1.setIsInterested(AttendingLecture.IS_IGNORED);
                        } else
                        {
                            lectureEventViewModel1.setIsInterested(AttendingLecture.NO_OUTPUT);
                        }


                        lectureEventViewModel1.setIgnored(false);
                    } else
                    {
                        lectureEventViewModel1.setAttendingLecture(AttendingLecture.NO_OUTPUT);
                        lectureEventViewModel1.setIsInterested(AttendingLecture.NO_OUTPUT);
                        lectureEventViewModel1.setIgnored(false);
                    }

                    lectureEventViewModel = lectureEventViewModel1;

                    getPresenter().setLectureModel(lectureEventViewModel1.getmLectureEvent());
                    setupTabBar();
                    getDataBinder().setData(lectureEventViewModel);
                    getDataBinder().setBinder(getPresenter());
                    createdBy.setText("Created By:" + getPresenter().getLectureModel().getUserdata().getFull_name());
                } else
                {
                    displayError("Json conversion error");
                }
            }

            @Override
            public void onFailure(Call<LectureEventModel> call, Throwable t)
            {

                hideLoading();

                displayError(t.getMessage());
            }
        });
    }

}
