package gilm.lecture.verb.Features.QuickLecture.Presentation.RecordPresentation;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.display.VirtualDisplay;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.BaseActivityNoBinding;
import gilm.lecture.verb.Features.QuickLecture.Presentation.PostPresentation.PostPresentationActivity;
import gilm.lecture.verb.Features.QuickLecture.Presentation.PresentationOptions.SingleImageModel;
import gilm.lecture.verb.Features.QuickLecture.Presentation.RecordPresentation.PresentationSlides.ImagesVideos_PagerAdapter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.UtillsG;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class RecordPresentationActivity extends BaseActivityNoBinding<RecordPresentationPresenter> implements RecordPresentationView, ViewPager.OnPageChangeListener
{
    int hours, minutes, seconds;

    TextView txtvTime;
    List<SingleImageModel> mModelList;
    public ViewPager auto_viewpager;
    RecyclerView recPPTslides;

    String recordedVideoPath = "";
    String group_id = "";

    public static void start(Context context, List<SingleImageModel> mModelList, String discoveryUrl, String artWorkImage, String lectureCategory, String group_id)
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        {
            UtillsG.showToast("Not able to work on lower versions", context, true);
        } else
        {
            Intent starter = new Intent(context, RecordPresentationActivity.class);
            starter.putExtra(Constants.Extras.DATA, (ArrayList) mModelList);
            starter.putExtra("discoveryUrl", discoveryUrl);
            starter.putExtra("artWorkImage", artWorkImage);
            starter.putExtra("lectureCategory", lectureCategory);
            starter.putExtra("group_id", group_id);
            context.startActivity(starter);
            ((Activity) context).finish();
        }
    }

    private static final String TAG = "MainActivity";
    private MediaProjectionManager mProjectionManager;
    private MediaProjection mMediaProjection;
    private VirtualDisplay mVirtualDisplay;
    private MediaProjectionCallback mMediaProjectionCallback;
    private FloatingActionButton mToggleButton;
    private MediaRecorder mMediaRecorder;


    @Override
    public MediaRecorder getRecorder()
    {
        return mMediaRecorder;
    }

    @Override
    protected int setLayoutId()
    {
        return R.layout.activity_record_presentation;
    }

    @Override
    protected void onCreateActivityG()
    {
        injectPresenter(new RecordPresentationPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews()
    {
        mModelList = (List<SingleImageModel>) getIntent().getSerializableExtra(Constants.Extras.DATA);
        group_id = UtillsG.getNotNullString(getIntent().getStringExtra("group_id"), "0");

        HorizontalImagesAdapter adapter = new HorizontalImagesAdapter(mModelList, getActivityG());
        adapter.setShouldLoadMore(false);

        recPPTslides = (RecyclerView) findViewById(R.id.recPPTslides);
        recPPTslides.setLayoutManager(new LinearLayoutManager(getActivityG(), LinearLayoutManager.HORIZONTAL, false));
        recPPTslides.setAdapter(adapter);

        auto_viewpager = (ViewPager) findViewById(R.id.auto_viewpager);
        ImagesVideos_PagerAdapter mDapater = new ImagesVideos_PagerAdapter(getActivityG(), mModelList);
        auto_viewpager.setAdapter(mDapater);
        auto_viewpager.addOnPageChangeListener(this);
//        auto_viewpager.setOffscreenPageLimit(mModelList.size());

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        getPresenter().screenDensity = metrics.densityDpi;

        mMediaRecorder = new MediaRecorder();

        mProjectionManager = (MediaProjectionManager) getSystemService
                (Context.MEDIA_PROJECTION_SERVICE);

        txtvTime = (TextView) findViewById(R.id.txtvTime);
        mToggleButton = (FloatingActionButton) findViewById(R.id.toggle);
        mToggleButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getPresenter().onToggleScreenShare();
            }
        });
    }

    @Override
    public void hideRecordingButton()
    {
        mToggleButton.setVisibility(View.GONE);
        (findViewById(R.id.txtvTapToRecord)).setVisibility(View.GONE);
    }

    @Override
    public void startTimer()
    {
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                seconds++;
                if (seconds == 60)
                {
                    seconds = 0;
                    minutes++;
                    if (minutes == 60)
                    {
                        minutes = 0;
                        hours++;
                    }
                }
                if (minutes == 0)
                {
                    txtvTime.setText("00:" + (seconds < 10 ? ("0" + seconds) : seconds));
                } else if (hours == 0 && minutes > 0)
                {
                    txtvTime.setText((minutes < 10 ? ("0" + minutes) : minutes) + ":" + (
                            seconds < 10 ? ("0" + seconds) : seconds));
                } else if (hours > 0)
                {
                    txtvTime.setText(hours + ":" + (minutes < 10 ? ("0" + minutes) : minutes));
                }

                if (minutes < 10)
                {
                    startTimer();
                } else
                {
                    if (getPresenter().isRecording)
                    {
                        stopRecording(true);
                        DialogHelper.getInstance().showInformation(getActivityG(), "You are a non-premium user so your 10 minute recording has been finished.", new CallBackG<String>()
                        {
                            @Override
                            public void onCallBack(String output)
                            {

                            }
                        });
                    }
                }
            }
        }, 1000);
    }

    @Override
    public Context getActivityG()
    {
        return RecordPresentationActivity.this;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void shareScreen()
    {
        if (mMediaProjection == null)
        {
            startActivityForResult(mProjectionManager.createScreenCaptureIntent(), Constants.RequestCode.SCREEN_RECORDER);
            return;
        }
        mVirtualDisplay = getPresenter().createVirtualDisplay();
        mMediaRecorder.start();

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
    {

    }

    @Override
    public void onPageSelected(int position)
    {
        recPPTslides.scrollToPosition(position);
    }

    @Override
    public void onPageScrollStateChanged(int state)
    {

    }

    public class MediaProjectionCallback extends MediaProjection.Callback
    {
        @Override
        public void onStop()
        {
            if (getPresenter().isRecording)
            {
                getPresenter().isRecording = false;
                mMediaRecorder.stop();
                mMediaRecorder.reset();
                Log.v(TAG, "Recording Stopped");
            }
            mMediaProjection = null;
            stopScreenSharing();
        }
    }


    @Override
    public MediaProjection getMediaProjection()
    {
        return mMediaProjection;
    }

    @Override
    public void setMediaProjectionNull()
    {
        mMediaProjection = null;
    }

    @Override
    public MediaProjection.Callback getMediaProjectionCallBack()
    {
        return mMediaProjectionCallback;
    }

    @Override
    public void initMediaProjectionCallBack(int resultCode, Intent data)
    {
        mMediaProjectionCallback = new RecordPresentationActivity.MediaProjectionCallback();
        mMediaProjection = mProjectionManager.getMediaProjection(resultCode, data);
        mMediaProjection.registerCallback(mMediaProjectionCallback, null);
        mVirtualDisplay = getPresenter().createVirtualDisplay();
        mMediaRecorder.start();
    }

    @Override
    public void stopScreenSharing()
    {
        if (mVirtualDisplay == null)
        {
            return;
        }
        mVirtualDisplay.release();
        //mMediaRecorder.release(); //If used: mMediaRecorder object cannot
        // be reused again
        getPresenter().destroyMediaProjection();
    }

    @Override
    public void hideNotificationBar()
    {
        enableFullScreenMode();
    }


    @Override
    public void onBackPressed()
    {
        if (getPresenter().isRecording)
        {
            stopRecording(true);
        } else
        {
            super.onBackPressed();
        }
    }

    private void stopRecording(boolean needConfimation)
    {
        getPresenter().stopRecording();
        if (needConfimation)
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivityG());
            dialog.setMessage("Do you want to Save this recording?");
            dialog.setPositiveButton("Save", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    PostPresentationActivity.start(getActivityG(), recordedVideoPath, getIntent().getStringExtra("discoveryUrl"),
                            getIntent().getStringExtra("artWorkImage")
                            , getIntent().getStringExtra("lectureCategory"), txtvTime.getText().toString(), group_id);
                    finish();
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    finish();
                }
            });
            dialog.show();
        } else
        {
            getPresenter().stopRecording();
            finish();
        }
    }

    @Override
    public void setVideoPath(String videoPath)
    {
        recordedVideoPath = videoPath;
    }

    @Override
    public void permissionDenied()
    {
        finish();
    }

    @Override
    public String getTimeDuration()
    {
        return txtvTime.getText().toString();
    }
}
