package gilm.lecture.verb.Features.Splash;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ViewFlipper;

import gilm.lecture.verb.Features.Internal.Base.BaseActivityNoBinding;
import gilm.lecture.verb.Features.LoginRegisteration.EmailVerification.EmailVerificationActivity;
import gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence.UserSelectionActivity;
import gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.FileHelper;
import gilm.lecture.verb.UtilsG.UtillsG;

public class SplashActivity extends BaseActivityNoBinding<SplashPresenter> implements SplashView {

    ViewFlipper viewFlipper;

    public static void start(Context context) {
        Intent starter = new Intent(context, SplashActivity.class);
        context.startActivity(starter);
        UtillsG.finishAll(context);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new SplashPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public Context getActivityG() {
        return SplashActivity.this;
    }

    @Override
    public void initViews() {
        viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);
    }

    @Override
    public long splashTimer() {
        return 2000;
    }

    @Override
    public void openHome() {
        NavigationActivity.start(getActivityG());
    }

    @Override
    public void openVerificationScreen() {
        EmailVerificationActivity.start(getActivityG());
        finish();
    }

    @Override
    public void permissionCheckDone() {
        getPresenter().splashCheck();
        new FileHelper().initLocalFolders();
    }

    @Override
    public void openUserSelection() {
        UserSelectionActivity.start(getActivityG());
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionCheckDone();
    }

    @Override
    public void showStartUpOptions() {
        if (viewFlipper.getCurrentView().getId() == R.id.space) {
            viewFlipper.showNext();
        }
    }

    public void onTour(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(Constants.YOUTUBE_TUTORIAL));
        startActivity(intent);
    }

    public void getStarted(View view) {
        openUserSelection();
    }
}
