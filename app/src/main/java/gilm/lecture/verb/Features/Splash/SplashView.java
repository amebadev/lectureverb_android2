package gilm.lecture.verb.Features.Splash;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 05 Jul 2017.
 */

public interface SplashView extends Viewable<SplashPresenter>
{
    long splashTimer();

    void openHome();

    void openVerificationScreen();

    void openUserSelection();

    void permissionCheckDone();

    void showStartUpOptions();
}
