package gilm.lecture.verb.Features.Search;

import com.google.gson.annotations.SerializedName;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * created by PARAMBIR SINGH on 4/10/17.
 */

public class SuggestedLectureModel extends BasicApiModel
{
    @SerializedName("data")
    InnerSuggestedListsModel lectures;

    public InnerSuggestedListsModel getLectures() {
        return lectures;
    }

    public void setLectures(InnerSuggestedListsModel lectures) {
        this.lectures = lectures;
    }
}
