package gilm.lecture.verb.Features.Player;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.malmstein.fenster.play.FensterVideoFragment;
import com.piterwilson.audio.MP3RadioStreamDelegate;
import com.piterwilson.audio.MP3RadioStreamPlayer;
import com.shuyu.waveview.AudioWaveView;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments.CommentsFragment;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureApi;
import gilm.lecture.verb.Features.QuickLecture.Single_Lecture_Event_Model;
import gilm.lecture.verb.Features.TabAdapter.FragmentTabModel;
import gilm.lecture.verb.Features.TabAdapter.SectionsPagerAdapter;
import gilm.lecture.verb.Features.WebHistory.WebHistoryActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.Web;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayerActivity extends AppCompatActivity implements MP3RadioStreamDelegate, View.OnClickListener {
    public Lecture_And_Event_Model lectureModel;
    SeekBar seekBar;
    AudioWaveView audioWave, audioWave2;
    public boolean playeEnd, seekBarTouch, isAudioMode, isMinimized, IS_PLAYER_RESUME, isLayoutExpand = false, openCommentsDirectly;
    public ImageView playBtn;
    ImageView imgvMinimize;
    ImageView imgvShareAudio;
    ImageView imgvLikeAudio;
    ImageView imgvCommentAudio;
    ImageView imgvLectureThumbnail, imgvLectureThumbNail2;
    ImageView imgvRewindAudio;
    ImageView imgvForwardAudio;
    ImageView imgv_add_To_Favourite;
    ImageView imgvShowComments;
    private Timer timer;
    LinearLayout llCommentOnVideo, llLikeAudio, layCommentsFrameLayout, llAudioplayerIcon, llVideoPlayerIcon, llAudioPlayer, ll_root, linear_favourite,
            linear_share, llHistoryStack, llBothPlayers, llCommentsTab, llDiscussionsTab;
    TextView txtvLectureTitle, txtvLikesCountAudio, txtvDuration, txtvMaxDuration, txtvDuration2, txtvMaxDuration2;
    Context con;
    private String LECTURE_TYPE_AUDIO = "audio", LECTURE_TYPE_VIDEO = "video", mediaPath;
    private FrameLayout flVideoPlayer, llMain;
    PublicAudioPlayer publicAudioPlayer;
    int maxSeconds = 0;
    CommentsReplyDailog mCommentsReply;
    ViewPager id_viewpager;
    View view1, view2;
    private CommentsFragment commentsFragment;
    LectureEventClicks mClicks = new LectureEventClicks();

    boolean showAds = false;

    public static void startForResult(final Context context, final Lecture_And_Event_Model lectureModel, final boolean openComments) {
        UtillsG.showRewardedAds(context, context.getResources().getString(R.string.nonStudenSearchLectureAppId), context.getResources().getString(R.string.nonStudentSearchLectureScreen), new CallBackG<Boolean>() {
            @Override
            public void onCallBack(Boolean output) {
                Intent starter = new Intent(context, PlayerActivity.class);
                starter.putExtra(Constants.Extras.DATA, lectureModel);
                starter.putExtra(Constants.Extras.openComments, openComments);
                ((Activity) context).startActivityForResult(starter, Constants.RequestCode.AUDIO_PLAYER);
            }
        });
    }

    public static void startWithAds(final Context context, final Lecture_And_Event_Model lectureModel, final boolean openComments) {
        UtillsG.showRewardedAds(context, context.getResources().getString(R.string.nonStudenSearchLectureAppId), context.getResources().getString(R.string.nonStudentSearchLectureScreen), new CallBackG<Boolean>() {
            @Override
            public void onCallBack(Boolean output) {
                Intent starter = new Intent(context, PlayerActivity.class);
                starter.putExtra(Constants.Extras.DATA, lectureModel);
                starter.putExtra(Constants.Extras.openComments, openComments);
                starter.putExtra(Constants.Extras.showAds, true);
                ((Activity) context).startActivityForResult(starter, Constants.RequestCode.AUDIO_PLAYER);
            }
        });
    }

    public static void startWithId(final Context context, final String quick_lecture_id, final boolean openComments) {
        UtillsG.showRewardedAds(context, context.getResources().getString(R.string.nonStudenSearchLectureAppId), context.getResources().getString(R.string.nonStudentSearchLectureScreen), new CallBackG<Boolean>() {
            @Override
            public void onCallBack(Boolean output) {
                getLectureAndPlay(context, quick_lecture_id, openComments);
            }
        });
    }

    private static void getLectureAndPlay(final Context context, String quick_lecture_id, final boolean openComments) {
        Call<Single_Lecture_Event_Model> call1 = LVApplication.getRetrofit().create(QuickLectureApi.class).get_single_posted_lectures(
                new SharedPrefHelper(context).getUserId(),
                quick_lecture_id);
        call1.enqueue(new Callback<Single_Lecture_Event_Model>() {
            @Override
            public void onResponse(Call<Single_Lecture_Event_Model> call, Response<Single_Lecture_Event_Model> response) {
                if (response.body().getStatus()) {
                    Lecture_And_Event_Model quickLectureModel = response.body().getQuickLectureModels();
                    if (UtillsG.isAudioVideoMediaPost(quickLectureModel)) {
                        PublicAudioPlayer.setReleaseAudioPlayer();

                        Intent starter = new Intent(context, PlayerActivity.class);
                        starter.putExtra(Constants.Extras.DATA, quickLectureModel);
                        starter.putExtra(Constants.Extras.openComments, openComments);
                        ((Activity) context).startActivityForResult(starter, Constants.RequestCode.AUDIO_PLAYER);

                    } else if (UtillsG.isTextPost(quickLectureModel)) {

                    }
                } else {
                    UtillsG.showToast(response.body().getMessage(), context, true);
                }
            }

            @Override
            public void onFailure(Call<Single_Lecture_Event_Model> call, Throwable t) {
                UtillsG.showToast(t.getMessage(), context, true);
            }
        });
    }

    public static void startAndResume(final Context context, final Lecture_And_Event_Model lectureModel, final boolean openComments) {
        UtillsG.showRewardedAds(context, context.getResources().getString(R.string.nonStudenSearchLectureAppId), context.getResources().getString(R.string.nonStudentSearchLectureScreen), new CallBackG<Boolean>() {
            @Override
            public void onCallBack(Boolean output) {
                Intent starter = new Intent(context, PlayerActivity.class);
                starter.putExtra(Constants.Extras.DATA, lectureModel);
                starter.putExtra(Constants.Extras.IS_PLAYER_RESUME, true);
                starter.putExtra(Constants.Extras.openComments, openComments);
                starter.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(starter);
            }
        });
    }

    public static void start(final Context context, final Lecture_And_Event_Model lectureModel, final boolean openComments) {
        UtillsG.showRewardedAds(context, context.getResources().getString(R.string.nonStudenSearchLectureAppId), context.getResources().getString(R.string.nonStudentSearchLectureScreen), new CallBackG<Boolean>() {
            @Override
            public void onCallBack(Boolean output) {
                Intent starter = new Intent(context, PlayerActivity.class);
                starter.putExtra(Constants.Extras.DATA, lectureModel);
                starter.putExtra(Constants.Extras.openComments, openComments);
                context.startActivity(starter);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.music_player);

        showAds = getIntent().getBooleanExtra("showAds", false);

        con = this;
        mCommentsReply = new CommentsReplyDailog(PlayerActivity.this);
        mClicks = new LectureEventClicks();

        lectureModel = (Lecture_And_Event_Model) getIntent().getSerializableExtra(Constants.Extras.DATA);
        IS_PLAYER_RESUME = getIntent().getBooleanExtra(Constants.Extras.IS_PLAYER_RESUME, false);
        openCommentsDirectly = getIntent().getBooleanExtra(Constants.Extras.openComments, false);
        mediaPath = Web.Path.BASE_URL + lectureModel.getFile_path();

        initViews();
    }

    public void initViews() {
//        UtillsG.showInterstitialAds(con, getResources().getString(R.string.nonStudenSearchLectureAppId), getResources().getString(R.string.nonStudentSearchLectureScreen));

        llDiscussionsTab = ((LinearLayout) findViewById(R.id.llDiscussionsTab));
        llCommentsTab = ((LinearLayout) findViewById(R.id.llCommentsTab));
        view1 = ((View) findViewById(R.id.view1));
        view2 = ((View) findViewById(R.id.view2));

        llBothPlayers = ((LinearLayout) findViewById(R.id.llBothPlayers));
        txtvMaxDuration = ((TextView) findViewById(R.id.txtvMaxDuration));
        txtvDuration = ((TextView) findViewById(R.id.txtvDuration));
        txtvDuration2 = (TextView) findViewById(R.id.txtvDuration2);
        txtvMaxDuration2 = ((TextView) findViewById(R.id.txtvMaxDuration2));
        imgvLectureThumbnail = ((ImageView) findViewById(R.id.imgvLectureThumbnail));
        imgvLectureThumbNail2 = ((ImageView) findViewById(R.id.imgvLectureThumbNail2));
        imgvShowComments = ((ImageView) findViewById(R.id.imgvShowComments));
        imgvLikeAudio = ((ImageView) findViewById(R.id.imgvLikeAudio));
        imgvShareAudio = ((ImageView) findViewById(R.id.imgvShareAudio));
        imgvCommentAudio = ((ImageView) findViewById(R.id.imgvCommentAudio));
        imgvMinimize = ((ImageView) findViewById(R.id.imgvMinimize));
        imgv_add_To_Favourite = ((ImageView) findViewById(R.id.imgv_add_To_Favourite));
        llMain = ((FrameLayout) findViewById(R.id.llMain));
        llCommentOnVideo = ((LinearLayout) findViewById(R.id.llCommentOnVideo));
        llLikeAudio = ((LinearLayout) findViewById(R.id.llLikeAudio));
        linear_share = ((LinearLayout) findViewById(R.id.linear_share));
        llHistoryStack = ((LinearLayout) findViewById(R.id.llHistoryStack));
        linear_favourite = ((LinearLayout) findViewById(R.id.linear_favourite));
        llVideoPlayerIcon = ((LinearLayout) findViewById(R.id.llVideoPlayerIcon));
        llAudioplayerIcon = ((LinearLayout) findViewById(R.id.llAudioplayerIcon));
        llAudioPlayer = ((LinearLayout) findViewById(R.id.llAudioPlayer));
        ll_root = (LinearLayout) findViewById(R.id.ll_root);
        txtvLikesCountAudio = ((TextView) findViewById(R.id.txtvLikesCountAudio));
        flVideoPlayer = ((FrameLayout) findViewById(R.id.flVideoPlayer));

        layCommentsFrameLayout = (LinearLayout) findViewById(R.id.layCommentsFrameLayout);
        layCommentsFrameLayout.setOnClickListener(this);
        imgvShowComments.setOnClickListener(this);

        llHistoryStack.setOnClickListener(this);
        llCommentOnVideo.setOnClickListener(this);
        imgvMinimize.setOnClickListener(this);

        if (openCommentsDirectly) { // this thing will open comments section directly, this will be called when user will click to open comments from home adapter or anywhere else.
            imgvShowComments.setRotation(0);
            llBothPlayers.setVisibility(View.GONE);
            layCommentsFrameLayout.setVisibility(View.VISIBLE);
        }
        if (getLectureType().equalsIgnoreCase(LECTURE_TYPE_AUDIO)
                || getLectureType().equalsIgnoreCase("discovery_audio")
                || getLectureType().equalsIgnoreCase("event_audio")
                ) {
            enableAudioMode();
        } else if (getLectureType().equalsIgnoreCase(LECTURE_TYPE_VIDEO)
                || getLectureType().equalsIgnoreCase("discovery_video")
                || getLectureType().equalsIgnoreCase("event_video")) {
            enableVideoMode();
            if (getLectureType().equalsIgnoreCase("event_video")) {
                (findViewById(R.id.webHistoryIndicator)).setVisibility(View.INVISIBLE);
                (findViewById(R.id.audioplayerIndicator)).setVisibility(View.INVISIBLE);
                (findViewById(R.id.pptIndicator)).setVisibility(View.INVISIBLE);
                (findViewById(R.id.videoPlayerIndicator)).setVisibility(View.VISIBLE);
            }
        }

        id_viewpager = (ViewPager) findViewById(R.id.id_viewpager);

        commentsFragment = CommentsFragment.newInstance(lectureModel);
        List<FragmentTabModel> list = new ArrayList<>();
        list.add(new FragmentTabModel(commentsFragment, "Comments"));
//        list.add(new FragmentTabModel(BlankFragment.newInstance(), "Discussion"));
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager(), list);
        id_viewpager.setAdapter(adapter);
        id_viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    view1.setVisibility(View.VISIBLE);
                    view2.setVisibility(View.INVISIBLE);
                } else {
                    view2.setVisibility(View.VISIBLE);
                    view1.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        llCommentsTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                id_viewpager.setCurrentItem(0);
                view1.setVisibility(View.VISIBLE);
                view2.setVisibility(View.INVISIBLE);
            }
        });
        llDiscussionsTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                id_viewpager.setCurrentItem(1);
                view2.setVisibility(View.VISIBLE);
                view1.setVisibility(View.INVISIBLE);
            }
        });
    }

    private String getLectureType() {
        return lectureModel.getQuick_lecture_type();
    }

    private void enableVideoMode() {
        (findViewById(R.id.llAudioWave2)).setVisibility(View.GONE);

        isAudioMode = false;

        flVideoPlayer.setVisibility(View.VISIBLE);
        llAudioPlayer.setVisibility(View.GONE);
        imgvMinimize.setVisibility(View.GONE);
        (findViewById(R.id.webHistoryIndicator)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.audioplayerIndicator)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.pptIndicator)).setVisibility(View.VISIBLE);
        (findViewById(R.id.videoPlayerIndicator)).setVisibility(View.INVISIBLE);
        llMain.setBackgroundResource(R.color.blueVideoPlayer);

        imgv_add_To_Favourite.setImageResource(
                lectureModel.getIs_favourite().equalsIgnoreCase("0") ? R.mipmap.ic_fav_white_border
                        : R.mipmap.ic_fav);
        imgv_add_To_Favourite.setColorFilter(
                lectureModel.getIs_favourite().equalsIgnoreCase("0")
                        ? getResources().getColor(R.color.white)
                        : getResources().getColor(R.color.redTheme));

        getVideoFragment().playVideo(mediaPath);

        linear_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likeUnlikeLecture(imgv_add_To_Favourite);
            }
        });
        linear_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<BasicApiModel> shareExternally = LVApplication.getRetrofit().create(HomeApis.class).
                        save_external_share("0",
                                lectureModel.getQuick_lecture_id(),
                                new SharedPrefHelper(con).getUserId(),
                                lectureModel.getUser_id(),
                                lectureModel.getUser_id().equals(new SharedPrefHelper(con).getUserId())
                                        ? " You " : new SharedPrefHelper(con).getUserName(),
                                lectureModel.getQuick_lecture_text());
                shareExternally.enqueue(new Callback<BasicApiModel>() {
                    @Override
                    public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {

                    }

                    @Override
                    public void onFailure(Call<BasicApiModel> call, Throwable t) {

                    }
                });
               /* Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, mediaPath);
                sendIntent.setType("text/plain");
                con.startActivity(sendIntent);*/

                UtillsG.shareLinkExternally_QuickLecture(con, lectureModel);

            }
        });
    }

    public FensterVideoFragment getVideoFragment() {
        return (FensterVideoFragment) getFragmentManager().findFragmentById(R.id.play_demo_fragment);
    }

    private void enableAudioMode() {
        publicAudioPlayer = PublicAudioPlayer.getInstance();

        isAudioMode = true;

        if (!UtillsG.getNotNullString(lectureModel.getThumbnail(), "").isEmpty()) {
            ImageLoader.setImageBig_FitCenter(imgvLectureThumbnail, lectureModel.getThumbnail());
            ImageLoader.setImageBig_FitCenter(imgvLectureThumbNail2, lectureModel.getThumbnail());
        }

        (findViewById(R.id.audioplayerIndicator)).setVisibility(View.VISIBLE);
        (findViewById(R.id.videoPlayerIndicator)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.webHistoryIndicator)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.pptIndicator)).setVisibility(View.INVISIBLE);
        llAudioPlayer.setVisibility(View.VISIBLE);
        flVideoPlayer.setVisibility(View.GONE);
        llMain.setBackgroundResource(R.color.colorAccent);

        txtvLectureTitle = ((TextView) findViewById(R.id.txtvLectureTitle));
        txtvLectureTitle.setText(lectureModel.getQuick_lecture_text());
        txtvLectureTitle.setSelected(true);


        txtvLikesCountAudio.setText(lectureModel.getFavourites_count());

        imgvLikeAudio.setImageResource(
                lectureModel.getIs_favourite().equalsIgnoreCase("0") ? R.mipmap.ic_fav_white_border
                        : R.mipmap.ic_fav);
        imgvLikeAudio.setColorFilter(
                lectureModel.getIs_favourite().equalsIgnoreCase("0")
                        ? getResources().getColor(R.color.white)
                        : getResources().getColor(R.color.redTheme));

        audioWave = (AudioWaveView) findViewById(R.id.audioWave);
        audioWave2 = (AudioWaveView) findViewById(R.id.audioWave2);
        audioWave.setChangeColor(Color.WHITE, Color.WHITE, Color.WHITE);
        audioWave2.setChangeColor(Color.BLACK, Color.BLACK, Color.BLACK);

        seekBar = (SeekBar) findViewById(R.id.seekBAr);
        playBtn = (ImageView) findViewById(R.id.imgPlay);
        imgvRewindAudio = (ImageView) findViewById(R.id.imgvRewindAudio);
        imgvForwardAudio = (ImageView) findViewById(R.id.imgvForwardAudio);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                play();
            }
        }, IS_PLAYER_RESUME ? 100 : 1000);

        // if the audio is in resume mode then the on started method will not be called and the playbtn will never be called if we dont implement below condition here
        if (!IS_PLAYER_RESUME) {
            playBtn.setEnabled(false);
//            UtillsG.showToast("Loading...", this, true);
        }

        imgvRewindAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekAudioByPercentage(-20, v);
            }
        });
        imgvForwardAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekAudioByPercentage(10, v);
            }
        });

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playeEnd) {
                    stop();
                    playBtn.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause));
                    play();
                    return;
                }

                if (PublicAudioPlayer.getInstance().getAudioPlayer().isPause()) {
                    playBtn.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause));
                    PublicAudioPlayer.getInstance().getAudioPlayer().setPause(false);
                } else {
                    playBtn.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
                    PublicAudioPlayer.getInstance().getAudioPlayer().setPause(true);
                }

            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                /**
                 * this check is implemented because if user will close the player before one second .
                 * player and txtvMaxDuration will become null and app will be crashed
                 */
                if (txtvMaxDuration != null && PublicAudioPlayer.getInstance().getAudioPlayer() != null) {
                    // Calculating the max duration and changing timer value according to seekbar position.
                    if (maxSeconds != 0) {
                        int progressPerSecond = Math.round(PublicAudioPlayer.getInstance().getAudioPlayer().getDuration() / maxSeconds);
                        int currentltySeconds = Math.round(progress / progressPerSecond);

                        txtvDuration.setText(UtillsG.secondsToMinutes(currentltySeconds));
                        txtvDuration2.setText(UtillsG.secondsToMinutes(currentltySeconds));
                    } else {
                        final int previousPosition = (int) PublicAudioPlayer.getInstance().getAudioPlayer().getCurPosition();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    int difference = (int) PublicAudioPlayer.getInstance().getAudioPlayer().getCurPosition() - previousPosition;
                                    if (difference > 0) {
                                        maxSeconds = Math.round(PublicAudioPlayer.getInstance().getAudioPlayer().getDuration() / difference);
                                        txtvMaxDuration.setText(" / " + UtillsG.secondsToMinutes(maxSeconds));
                                        txtvMaxDuration2.setText(" / " + UtillsG.secondsToMinutes(maxSeconds));
                                    }
                                } catch (Exception ex) {
                                    //TODO : handle null pointer player exception
                                    Log.e("Exception is", ex.toString());
                                }
                            }
                        }, 1000);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBarTouch = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBarTouch = false;
                if (!playeEnd) {
                    PublicAudioPlayer.getInstance().getAudioPlayer().seekTo(seekBar.getProgress());
                }
            }
        });

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (playeEnd || PublicAudioPlayer.getInstance().getAudioPlayer() == null || !seekBar.isEnabled()) {
                    return;
                }
                final long position = PublicAudioPlayer.getInstance().getAudioPlayer().getCurPosition();
                if (position > 0 && !seekBarTouch) {
                    seekBar.setProgress((int) position);
                }
            }
        }, 200, 200);


        llLikeAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likeUnlikeLecture(imgvLikeAudio);
            }
        });
        imgvShareAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<BasicApiModel> shareExternally = LVApplication.getRetrofit().create(HomeApis.class).
                        save_external_share("0",
                                lectureModel.getQuick_lecture_id(),
                                new SharedPrefHelper(con).getUserId(),
                                lectureModel.getUser_id(),
                                lectureModel.getUser_id().equals(new SharedPrefHelper(con).getUserId())
                                        ? "You " : new SharedPrefHelper(con).getUserName(),
                                lectureModel.getQuick_lecture_text());
                shareExternally.enqueue(new Callback<BasicApiModel>() {
                    @Override
                    public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {

                    }

                    @Override
                    public void onFailure(Call<BasicApiModel> call, Throwable t) {

                    }
                });
                /*Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, mediaPath);
                sendIntent.setType("text/plain");
                con.startActivity(sendIntent);*/

                UtillsG.shareLinkExternally_QuickLecture(con, lectureModel);
            }
        });
    }

    @Override
    public void onRadioPlayerPlaybackStarted(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imgvCommentAudio.setOnClickListener(PlayerActivity.this);

                playeEnd = false;
                playBtn.setEnabled(true);
                seekBar.setMax((int) PublicAudioPlayer.getInstance().getAudioPlayer().getDuration());
                playBtn.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause));
            }
        });
    }

    @Override
    public void onRadioPlayerStopped(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
        this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                playeEnd = true;
                playBtn.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
                playBtn.setEnabled(true);
                seekBar.setProgress(100);
            }
        });
    }

    @Override
    public void onRadioPlayerError(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                playeEnd = false;
                playBtn.setEnabled(true);
            }
        });
    }

    @Override
    public void onRadioPlayerBuffering(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
        this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                playBtn.setEnabled(false);
            }
        });
    }

    public void play() {
        if (PublicAudioPlayer.getInstance().getAudioPlayer() != null && !IS_PLAYER_RESUME) {
            PublicAudioPlayer.setReleaseAudioPlayer();
        }
        if (!IS_PLAYER_RESUME) {
            publicAudioPlayer.initPlayer();
            PublicAudioPlayer.getInstance().getAudioPlayer().setUrlString(this, true, mediaPath);
            PublicAudioPlayer.getInstance().getAudioPlayer().setDelegate(this);
            if (llBothPlayers.getVisibility() == View.VISIBLE) {
                audioWave.setBaseRecorder(PublicAudioPlayer.getInstance().getAudioPlayer());
                audioWave.startView();
            } else {
                audioWave2.setBaseRecorder(PublicAudioPlayer.getInstance().getAudioPlayer());
                audioWave2.startView();
                audioWave2.setVisibility(View.VISIBLE);
            }

            try {
                PublicAudioPlayer.getInstance().getAudioPlayer().play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            if (PublicAudioPlayer.getInstance().getAudioPlayer() != null) {
                PublicAudioPlayer.getInstance().getAudioPlayer().setDelegate(this);
                audioWave.setBaseRecorder(PublicAudioPlayer.getInstance().getAudioPlayer());
                audioWave.startView();

                PublicAudioPlayer.getInstance().getAudioPlayer().setPause(false);

                seekBar.setMax((int) PublicAudioPlayer.getInstance().getAudioPlayer().getDuration());
            }
        }

        int size = getScreenWidth(this) / dip2px(this, 1);//控件默认的间隔是1
        if (llBothPlayers.getVisibility() == View.VISIBLE) {
            PublicAudioPlayer.getInstance().getAudioPlayer().setDataList(audioWave.getRecList(), size);
        } else {
            PublicAudioPlayer.getInstance().getAudioPlayer().setDataList(audioWave2.getRecList(), size);
        }
    }

    // get width in pixels
    public static int getScreenWidth(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.widthPixels;
    }

    public static int dip2px(Context context, float dipValue) {
        float fontScale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * fontScale + 0.5f);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // show ads on destroy
        // if (showAds) {
        //  if (new SharedPrefHelper(con).getUserType().equalsIgnoreCase(UserType.NON_STUDENT)) {
        //      UtillsG.showInterstitialAds(con, getResources().getString(R.string.nonStudenSearchLectureAppId), getResources().getString(R.string.nonStudentSearchLectureScreen));

        //  }
        // show ads on destroy

        if (audioWave != null) {
            audioWave.stopView();
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
            if (!isMinimized) {
                stop();
            }
        }
        if (audioWave2 != null) {
            audioWave2.stopView();
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
            if (!isMinimized) {
                stop();
            }
        }
    }

    public void stop() {
        if (PublicAudioPlayer.getInstance().getAudioPlayer() != null) {
            PublicAudioPlayer.getInstance().getAudioPlayer().stop();
            IS_PLAYER_RESUME = false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgvCommentAudio:
                showOptions(false);
                break;
            case R.id.imgvShowComments:

                showHideComments();

                break;
            case R.id.llHistoryStack:
                if (PublicAudioPlayer.getAudioPlayer() != null && !PublicAudioPlayer.getAudioPlayer().isPause() && !playeEnd && playBtn != null) {
                    playBtn.performClick();
                }
                if (UtillsG.getNotNullString(lectureModel.getHistory_stack(), "").isEmpty()) {
                    WebHistoryActivity.start(con);
                } else {
                    DialogHelper.showInnerStackDialog(con, UtillsG.jsonToArrayList_InnerStack(lectureModel.getHistory_stack()));
                }
                break;
            case R.id.llCommentOnVideo:
                showOptions(true);
                break;
            case R.id.imgvMinimize:
                if (isAudioMode) {
                    if (playeEnd) {
                        stop();
                        playBtn.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause));
                        play();
                    }
                    isMinimized = true;

                    /*Intent intent = new Intent();
                    intent.putExtra(Constants.Extras.DATA, lectureModel);
                    setResult(RESULT_OK, intent);*/

                    LVApplication app = (LVApplication) getApplication();
                    app.showMinimizedPlayer(lectureModel);

                    finish();
                    overridePendingTransition(R.anim.slide_down, R.anim.slide_down);
                }
                break;
        }
    }

    private void showHideComments() {
        UtillsG.hideKeyboard(con, imgvShowComments);
        if (llBothPlayers.getVisibility() == View.VISIBLE) {
            imgvShowComments.setRotation(0);
            llBothPlayers.setVisibility(View.GONE);
            layCommentsFrameLayout.setVisibility(View.VISIBLE);

            if (isAudioMode) {
                audioWave.stopView();
                audioWave2.setVisibility(View.VISIBLE);
                audioWave2.setBaseRecorder(PublicAudioPlayer.getInstance().getAudioPlayer());
                audioWave2.startView();

                int size = getScreenWidth(this) / dip2px(this, 1);
                PublicAudioPlayer.getInstance().getAudioPlayer().setDataList(audioWave2.getRecList(), size);

                (findViewById(R.id.llAudioWave2)).setVisibility(View.VISIBLE);
            } else {
                (findViewById(R.id.llAudioWave2)).setVisibility(View.GONE);
            }
        } else {
            imgvShowComments.setRotation(180);
            llBothPlayers.setVisibility(View.VISIBLE);
            layCommentsFrameLayout.setVisibility(View.GONE);

            if (isAudioMode) {
                audioWave2.stopView();
                audioWave2.setVisibility(View.GONE);
                audioWave.setBaseRecorder(PublicAudioPlayer.getInstance().getAudioPlayer());
                audioWave.startView();
                int size = getScreenWidth(this) / dip2px(this, 1);
                PublicAudioPlayer.getInstance().getAudioPlayer().setDataList(audioWave.getRecList(), size);
            }
        }
    }

    private void showOptions(final boolean isVideo) {
        DialogHelper.getInstance().showLectureOptionsOnPlayer(isVideo, con,
                isVideo ? llCommentOnVideo
                        : imgvCommentAudio, lectureModel.getIs_favourite().equals("1"),
                isVideo ? getVideoFragment().textureView.isPlaying()
                        : !publicAudioPlayer.getAudioPlayer().isPause(), lectureModel, new CallBackG<Integer>() {
                    @Override
                    public void onCallBack(Integer output) {
                        if (isVideo) {
                            switch (output) {
                                case 0:
                                    if (getVideoFragment().textureView.isPlaying()) {
                                        getVideoFragment().textureView.pause();
                                    } else {
                                        getVideoFragment().textureView.resume();
                                    }
                                    break;
                                case 1:
                                    linear_favourite.performClick();
                                    break;
                                case 2:
                                    linear_share.performClick();
                                    break;
                                case 3:
                                    rePost_and_unRepost();
                                    break;
                                case 4:
                                   /* mCommentsReply.showPostDialog(lectureModel, false, new CallBackG<CommentsModel>()
                                    {
                                        @Override
                                        public void onCallBack(CommentsModel output) {

                                        }
                                    });*/
                                    imgvShowComments.performClick();
                                    break;
                                case 5:
                                    DialogHelper.getInstance().showLectureDetails(con, lectureModel, imgvShowComments);
                                    break;
                            }

                        } else {
                            switch (output) {
                                case 0:
                                    playBtn.performClick();
                                    break;
                                case 1:
                                    llLikeAudio.performClick();
                                    break;
                                case 2:
                                    imgvShareAudio.performClick();
                                    break;
                                case 3:
                                    rePost_and_unRepost();
                                    break;
                                case 4:
                                    //DialogHelper.getInstance().showCommentsDailog(con, lectureModel);
                                  /*  mCommentsReply.showPostDialog(lectureModel, false, new CallBackG<CommentsModel>()
                                    {
                                        @Override
                                        public void onCallBack(CommentsModel output) {

                                        }
                                    });*/
                                    imgvShowComments.performClick();
                                    break;
                                case 5:
                                    DialogHelper.getInstance().showLectureDetails(con, lectureModel, imgvShowComments);
                                    break;
                            }

                        }
                    }
                });
    }


    public void rePost_and_unRepost() {
        if (!lectureModel.getIs_reposted()
                && (lectureModel.getUser_id().equals(new SharedPrefHelper(con).getUserId())
                || (lectureModel.getOwnerUserdata() != null
                && lectureModel.getOwnerUserdata().getUser_id().equals(new SharedPrefHelper(con).getUserId())))) {
            UtillsG.showToast("You are the creator so you cannot repost it.", con, true);
        } else if (!lectureModel.getIs_reposted()) {
            mClicks.RepostQuickLecture((Activity) con, LVApplication.getRetrofit().create(HomeApis.class),
                    lectureModel.getQuick_lecture_id(), new CallBackG<Boolean>() {
                        @Override
                        public void onCallBack(Boolean output) {
                            if (output) {
                                int repostCount = Integer.parseInt(lectureModel.getReposted_count());
                                repostCount++;
                                lectureModel.setReposted_count(String.valueOf(repostCount));
                                lectureModel.setIs_reposted(true);
                                EventBus.getDefault().post(new HomeFragmentUpdateBus(true));

                            }
                        }
                    });
        } else if (lectureModel.getIs_reposted()) {
            mClicks.unrepost_lecture((Activity) con,
                    lectureModel.getQuick_lecture_id() != null ?
                            lectureModel.getQuick_lecture_id()
                            : ""
                    , new CallBackG<Boolean>() {
                        @Override
                        public void onCallBack(Boolean output) {
                            if (output) {
                                int repostCount = Integer.parseInt(lectureModel.getReposted_count());
                                repostCount--;
                                lectureModel.setReposted_count(String.valueOf(repostCount));
                                lectureModel.setIs_reposted(false);
                                EventBus.getDefault().post(new HomeFragmentUpdateBus(true));
                            }
                        }
                    });
        } else {
            UtillsG.showToast("Already reposted this lecture", con, true);
        }
    }

    private void likeUnlikeLecture(final ImageView targetImgv) {
        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance().add_remove_to_favourite_list(new SharedPrefHelper(con).getUserId(), lectureModel.getQuick_lecture_id());
        basicApiModelCall.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body() != null && response.body().getStatus()) {
                    Log.e("RESPONSE", response.body().getStatus() + "");

                    if (response.body().getStatus()) {
                        int count = Integer.parseInt(lectureModel.getFavourites_count());
                        UtillsG.showToast(String.valueOf(response.body().getMessage()), con, true);
                        if (lectureModel.getIs_favourite().equalsIgnoreCase("0")) {
                            lectureModel.setIs_favourite("1");
                            count++;
                            targetImgv.setImageResource(R.mipmap.ic_fav);
                            targetImgv.setColorFilter(con.getResources().getColor(R.color.redTheme));
                        } else {
                            lectureModel.setIs_favourite("0");
                            count--;
                            targetImgv.setImageResource(R.mipmap.ic_fav_white_border);
                            targetImgv.setColorFilter(con.getResources().getColor(R.color.white));
                        }
                        lectureModel.setFavourites_count(String.valueOf(count));
                        txtvLikesCountAudio.setText(String.valueOf(count));
                        EventBus.getDefault().post(new HomeFragmentUpdateBus(true));
                    }
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("ERROR get_posted_le", t.getMessage().toString());
                UtillsG.showToast(String.valueOf(t.getMessage().toString()), con, true);
            }
        });

    }

    protected HomeApis getRetrofitInstance() {
        return LVApplication.getRetrofit().create(HomeApis.class);
    }

    private void seekAudioByPercentage(final int percentage, final View view) {
        view.setEnabled(false);
        seekBarTouch = true;

        int currentProgress = seekBar.getProgress();
        seekBar.setProgress(currentProgress);
        int maxProgress = seekBar.getMax();

        float currentPercentage = (currentProgress * 100) / maxProgress;

        float newPercentage = currentPercentage + percentage;

        if (newPercentage >= 0 && newPercentage < 100) {
            float newProgress = maxProgress * (newPercentage / 100);
            publicAudioPlayer.getAudioPlayer().seekTo(Math.round(newProgress));
            Log.e("----------------", "Seeked from " + currentProgress + " to " + newProgress + "--------------------");
        } else {
            publicAudioPlayer.getAudioPlayer().seekTo(0);
        }

        seekBarTouch = false;
        view.setEnabled(true);

        if (publicAudioPlayer.getAudioPlayer().isPause()) {
            playBtn.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
        } else {
            playBtn.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause));
        }
    }
}

