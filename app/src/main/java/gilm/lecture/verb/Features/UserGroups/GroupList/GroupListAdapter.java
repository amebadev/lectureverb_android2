package gilm.lecture.verb.Features.UserGroups.GroupList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import gilm.lecture.verb.Features.Favorites.FavouritesLecturesList.FavouriteLecturesListActivity;
import gilm.lecture.verb.Features.Home.Home.HomeFragment_NoBinding;
import gilm.lecture.verb.Features.UserGroups.GroupDetails.GroupDetailsActivity;
import gilm.lecture.verb.Features.UserGroups.GroupModel;
import gilm.lecture.verb.Features.UserGroups.JoinGroup.AllGroupsListActivity;
import gilm.lecture.verb.Features.UserGroups.JoinGroup.JoinAndSignUpActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.Web;

/**
 * Created by ParamBir Singh on 06 Jul 2017.
 *
 * @see GroupListAdapter is used in 2 classes one is {@link HomeFragment_NoBinding} and another one is {@link FavouriteLecturesListActivity}
 */

public class GroupListAdapter extends RecyclerView.Adapter<GroupListAdapter.GroupViewHolder> {
    private LayoutInflater inflater;
    private CallBackG<GroupModel> selectionCallBack;

    private List<GroupModel> alData;
    Context context;


    public GroupListAdapter(List<GroupModel> alData, Context context, CallBackG<GroupModel> selectionCallBack) {
        this.alData = alData;
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.selectionCallBack = selectionCallBack;
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new GroupViewHolder(inflater.inflate(R.layout.inflater_group_list, parent, false));
    }

    @Override
    public void onBindViewHolder(final GroupViewHolder holder, final int position) {
        ImageLoader.setImageSmallCentreCrop(holder.imgvGroupIcon, UtillsG.getNotNullString(alData.get(position).getGroup_image(), "null"));
        holder.txtvGroupName.setText(alData.get(position).getGroup_title());
        String groupTypeId = alData.get(position).getGroup_type();
        String groupType = groupTypeId.equals(Web.GroupType.Institute) ? "Institute"
                : groupTypeId.equals(Web.GroupType.PrivateTraining) ? "Private Training"
                : "Public Group";
        holder.txtvGroupType.setText(groupType);
        if (alData.get(position).getMembersList() != null) {
            holder.txtvMembers.setText(alData.get(position).getMembersList().size() + " Members");
            holder.txtvMembers.setVisibility(View.VISIBLE);
        } else {
            holder.txtvMembers.setVisibility(View.GONE);
        }

        if (alData.get(position).getData().getUser_id().equals(new SharedPrefHelper(context).getUserId())) {
            holder.txtvAdmin.setVisibility(View.VISIBLE);
        } else {
            holder.txtvAdmin.setVisibility(View.GONE);
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectionCallBack != null) {
                    selectionCallBack.onCallBack(alData.get(position));
                } else {
                    if (context instanceof AllGroupsListActivity) {
                        if (((AllGroupsListActivity) context).isForResult) {
                            DialogHelper.getInstance().showWithAction(context, "Do you want to join \"" + alData.get(position).getGroup_title() + "\" ? Click Ok to confirm.", new CallBackG<String>() {
                                @Override
                                public void onCallBack(String output) {
                                    Intent inn = new Intent();
                                    inn.putExtra(Constants.Extras.DATA, alData.get(position));
                                    ((AllGroupsListActivity) context).setResult(Activity.RESULT_OK, inn);
                                    ((AllGroupsListActivity) context).finish();
                                }
                            });
                        } else {
                            JoinAndSignUpActivity.start(context, alData.get(position));
                        }
                    } else {
                        GroupDetailsActivity.start(context, alData.get(position));
                    }
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return alData.size();
    }

    class GroupViewHolder extends RecyclerView.ViewHolder {
        View view;
        ImageView imgvGroupIcon;
        TextView txtvGroupName, txtvGroupType, txtvAdmin, txtvMembers;

        public GroupViewHolder(View row) {
            super(row);

            view = row;
            imgvGroupIcon = view.findViewById(R.id.imgvGroupIcon);
            txtvGroupName = view.findViewById(R.id.txtvGroupName);
            txtvGroupType = view.findViewById(R.id.txtvGroupType);
            txtvAdmin = view.findViewById(R.id.txtvAdmin);
            txtvMembers = view.findViewById(R.id.txtvMembers);
        }
    }
}