/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Search;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Player.PlayerActivity;
import gilm.lecture.verb.Features.Player.PublicAudioPlayer;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImagePagerAdapter_notInUse extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    ArrayList<Lecture_And_Event_Model> alData;

    public ImagePagerAdapter_notInUse(Context context, ArrayList<Lecture_And_Event_Model> alData) {
        this.context = context;
        this.alData = alData;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return alData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.image_pager, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.image);
        TextView txtvLectureTitle = (TextView) itemView.findViewById(R.id.txtvLectureTitle);

        txtvLectureTitle.setText(alData.get(position).getQuick_lecture_text());
        UtillsG.setTextSizeByPercentage(context, txtvLectureTitle, 9f);

        if (alData.get(position).getThumbnail() != null &&
                !alData.get(position).getThumbnail().isEmpty()) {
            ImageLoader.setImageBig(imageView, alData.get(position).getThumbnail());
            txtvLectureTitle.setVisibility(View.GONE);
        } else {
            txtvLectureTitle.setVisibility(View.VISIBLE);
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UtillsG.isAudioVideoMediaPost(alData.get(position))) {
                    setLectureAsPlayed(position);
                    PublicAudioPlayer.setReleaseAudioPlayer();
                    PlayerActivity.startForResult(context, alData.get(position), false);
                } else if (UtillsG.isTextPost(alData.get(position))) {
//                    NotificationDetailsActivity.start(context, alData.get(position));
                    DialogHelper.getInstance().showInformation(context, alData.get(position).getUserdata().getFull_name(), alData.get(position).getQuick_lecture_text(), new CallBackG<String>() {
                        @Override
                        public void onCallBack(String output) {

                        }
                    });
                }
            }
        });

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }


    private void setLectureAsPlayed(final int position) {
        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance().add_lecture_to_recent_played_list(new SharedPrefHelper(context).getUserId(), alData.get(position).getQuick_lecture_id());
        basicApiModelCall.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body() != null && response.body().getStatus()) {
                    Log.e("RESPONSE", response.body().getStatus() + "");

                    if (response.body().getStatus()) {
                        int count = Integer.parseInt(alData.get(position).getLecture_view());
                        count++;
                        alData.get(position).setLecture_view(String.valueOf(count));
                        notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("ERROR get_posted_le", t.getMessage().toString());
            }
        });
    }

    protected HomeApis getRetrofitInstance() {
        return LVApplication.getRetrofit().create(HomeApis.class);
    }


}