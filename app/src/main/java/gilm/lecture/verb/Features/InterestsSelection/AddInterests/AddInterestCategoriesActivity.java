package gilm.lecture.verb.Features.InterestsSelection.AddInterests;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Home.Notification.Details.UserProfileModel;
import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.Features.InterestsSelection.InterestSelectionApi;
import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.Search.SearchApi;
import gilm.lecture.verb.Features.Search.SuggestedLectureModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.Widget.CategoriesEdittext;
import gilm.lecture.verb.databinding.ActivityAddInterestBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddInterestCategoriesActivity extends BaseActivity<ActivityAddInterestBinding, AddInterestPresenter> implements AddInterestView {

    ArrayList<ChildCategoryModel> alData = new ArrayList<>();
    public boolean isLectureEventCat;

    public static void start(Context context, boolean isLectureEventCat, String categoryIds, String categoryNames) {
        ArrayList<ChildCategoryModel> alData = new ArrayList<>();
        getPreviousCategoryList(categoryIds, categoryNames, alData);

        Intent starter = new Intent(context, AddInterestCategoriesActivity.class);
        starter.putExtra("isLectureEventCat", isLectureEventCat);
        starter.putExtra(Constants.Extras.DATA, alData);
        ((Activity) context).startActivityForResult(starter, Constants.RequestCode.INTEREST_SELECTION);
    }

    public static void startForFragment(Fragment fragment, boolean isLectureEventCat, String categoryIds, String categoryNames) {
        ArrayList<ChildCategoryModel> alData = new ArrayList<>();
        getPreviousCategoryList(categoryIds, categoryNames, alData);

        Intent starter = new Intent(fragment.getActivity(), AddInterestCategoriesActivity.class);
        starter.putExtra("isLectureEventCat", isLectureEventCat);
        starter.putExtra(Constants.Extras.DATA, alData);
        fragment.startActivityForResult(starter, Constants.RequestCode.INTEREST_SELECTION);
    }

    private static void getPreviousCategoryList(String categoryIds, String categoryNames, ArrayList<ChildCategoryModel> alData) {
        if (!categoryIds.trim().isEmpty() && !categoryNames.trim().isEmpty()) {
            final List<String> ids = Arrays.asList(categoryIds.split("\\s*,\\s*"));
            final List<String> names = Arrays.asList(categoryNames.split("\\s*,\\s*"));
            for (int i = 0; i < ids.size(); i++) {
                ChildCategoryModel model = new ChildCategoryModel();
                model.setId(ids.get(i));
                model.setCategory(names.get(i));
                alData.add(model);
            }
        }
    }

    @Override
    protected int setLayoutId() {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.setFinishOnTouchOutside(true);
        return R.layout.activity_add_interest;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new AddInterestPresenter());
        getPresenter().attachView(this);
//        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void initViews() {
        isLectureEventCat = getIntent().getBooleanExtra("isLectureEventCat", false);
        ArrayList<ChildCategoryModel> alDataTemp = (ArrayList<ChildCategoryModel>) getIntent().getSerializableExtra(Constants.Extras.DATA);

        if (alDataTemp != null) {
            alData.addAll(alDataTemp);
        }
        showOrHideNoDataMessage();

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDataBinder().recyCategories.setLayoutManager(new GridLayoutManager(getActivityG(), 3, GridLayoutManager.VERTICAL, false));
        MyInterestsListAdapter myInterestsListAdapter = new MyInterestsListAdapter(alData, getActivityG());
        getDataBinder().recyCategories.setAdapter(myInterestsListAdapter);
        myInterestsListAdapter.setShouldLoadMore(false);
        getDataBinder().txtvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = getDataBinder().autoTextInterest.getText().toString().trim();
                if (text.isEmpty()) {
                    UtillsG.showToast("Please enter something.", getActivityG(), true);
                } else {
                    executeSaveInterestApi(text);
                    getDataBinder().autoTextInterest.setText("");
                    UtillsG.hideKeyboard(getActivityG(), getDataBinder().autoTextInterest);
                }
            }
        });

        if (!isLectureEventCat) {
            getMyInterests();
        }

        getDataBinder().btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isLectureEventCat) {
                    saveMyInterestsInLocal();
                } else {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.Extras.DATA, alData);
                    setResult(RESULT_OK, intent);

                    finish();
                }

            }
        });
        getDataBinder().txtvAdd.setVisibility(View.GONE);
        getDataBinder().autoTextInterest.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getDataBinder().txtvAdd.setVisibility(
                        charSequence.toString().trim().isEmpty() ? View.GONE : View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getDataBinder().autoTextInterest.startSuggestions(getActivityG(), new CategoriesEdittext.SuggestionsCallBack() {
            @Override
            public void onTagInserted(ChildCategoryModel tagsModel) {
                getDataBinder().txtvAdd.performClick();
            }
        });
    }

    private void getMyInterests() {
        showLoading("Please wait..");
        Call<UserProfileModel> call = LVApplication.getRetrofit().create(HomeApis.class).get_user_profile(getLocalData().getUserId(), getLocalData().getUserId());
        call.enqueue(new Callback<UserProfileModel>() {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response) {
                hideLoading();
                if (response.body().getStatus()) {
                    List<UserDataModel.UserInterests> userInterests = response.body().getData().getUserdata().getUser_interest();
                    for (int i = 0; i < userInterests.size(); i++) {
                        ChildCategoryModel childCategoryModel = new ChildCategoryModel();
                        childCategoryModel.setCategory(userInterests.get(i).getCategory_name());
                        childCategoryModel.setId(userInterests.get(i).getCategory_id());

                        alData.add(0, childCategoryModel);
                    }
                    getDataBinder().recyCategories.getAdapter().notifyDataSetChanged();

                    showOrHideNoDataMessage();
                }
            }

            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t) {
                hideLoading();
                UtillsG.showToast("ERROR: " + t.getMessage(), getActivityG(), true);
            }
        });
    }

    private void saveMyInterestsInLocal() {
        String commaSeparatedCategories = "";
        for (int i = 0; i < alData.size(); i++) {
            commaSeparatedCategories = commaSeparatedCategories.isEmpty()
                    ? alData.get(i).getCategory()
                    : commaSeparatedCategories + ", " + alData.get(i).getCategory();
        }
        getLocalData().setMyInterests(commaSeparatedCategories);

        Intent inn = new Intent();
        inn.putExtra("isSuccess", true);
        inn.putExtra(Constants.Extras.DATA, commaSeparatedCategories);
        setResult(RESULT_OK, inn);
        finish();

        /*showLoading("Please wait..");
        Call<UserProfileModel> call = LVApplication.getRetrofit().create(HomeApis.class).get_user_profile(getLocalData().getUserId(), getLocalData().getUserId());
        call.enqueue(new Callback<UserProfileModel>()
        {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response) {
                hideLoading();
                if (response.body().getStatus()) {
                    List<UserDataModel.UserInterests> userInterests = response.body().getData().getUserdata().getUser_interest();
                    String commaSeparatedCategories = "";
                    for (int i = 0; i < userInterests.size(); i++) {
                        commaSeparatedCategories = commaSeparatedCategories.isEmpty()
                                ? userInterests.get(i).getCategory_name()
                                : commaSeparatedCategories + ", " + userInterests.get(i).getCategory_name();
                    }
                    getLocalData().setMyInterests(commaSeparatedCategories);

                    Intent inn = new Intent();
                    inn.putExtra("isSuccess", true);
                    inn.putExtra(Constants.Extras.DATA, commaSeparatedCategories);
                    setResult(RESULT_OK, inn);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t) {
                hideLoading();
                UtillsG.showToast("ERROR: " + t.getMessage(), getActivityG(), true);
            }
        });*/
    }

    @Override
    public Context getActivityG() {
        return AddInterestCategoriesActivity.this;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void executeSaveInterestApi(final String categoryName) {
        Call<ListOfChildInterestsModel> call = LVApplication.getRetrofit().create(InterestSelectionApi.class)
                .save_users_interest_v2(categoryName, getLocalData().getUserId());
        call.enqueue(new Callback<ListOfChildInterestsModel>() {
            @Override
            public void onResponse(Call<ListOfChildInterestsModel> call, Response<ListOfChildInterestsModel> response) {
                if (response.body().getStatus()) {
                    if (!response.body().getCategories().isEmpty()) {
                        for (int i = 0; i < response.body().getCategories().size(); i++) {
                            boolean isFound = false;
                            if (!response.body().getCategories().get(i).isNew()) {
                                for (int j = 0; j < alData.size(); j++) {
                                    if (alData.get(j).getCategory().equalsIgnoreCase(categoryName)) {
                                        isFound = true;
                                        break;
                                    }
                                }
                            }
                            if (!isFound) {
                                UtillsG.showToast("Added successfully.", getActivityG(), true);
                                ChildCategoryModel model = new ChildCategoryModel();
                                model.setNew(response.body().getCategories().get(i).isNew());
                                model.setCategory(response.body().getCategories().get(i).getCategory());
                                model.setId(response.body().getCategories().get(i).getId());
                                alData.add(0, model);
                            } else {
                                UtillsG.showToast("Already exists in the list.", getActivityG(), true);
                            }
                        }
                    }

                    getDataBinder().recyCategories.getAdapter().notifyDataSetChanged();

                    showOrHideNoDataMessage();
                } else {
                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                }
            }

            @Override
            public void onFailure(Call<ListOfChildInterestsModel> call, Throwable t) {
                UtillsG.showToast("ERROR: " + t.getMessage(), getActivityG(), true);
            }
        });
    }

    private void showOrHideNoDataMessage() {
        if (alData.isEmpty()) {
            getDataBinder().txtvNoInterests.setVisibility(View.VISIBLE);
        } else {
            getDataBinder().txtvNoInterests.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        if (!isLectureEventCat) {
            saveMyInterestsInLocal();
        }
        super.onDestroy();
    }
}
