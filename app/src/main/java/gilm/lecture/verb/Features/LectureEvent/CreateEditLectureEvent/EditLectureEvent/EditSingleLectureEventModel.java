package gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EditLectureEvent;


import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * Created by harpreet on 9/12/17.
 */

public class EditSingleLectureEventModel extends BasicApiModel {

    private LectureDetailsModel data;

    public LectureDetailsModel getmQuickLectureData() {
        return data;
    }

    public void setmQuickLectureData(LectureDetailsModel
                                             data) {
        this.data = data;
    }


}
