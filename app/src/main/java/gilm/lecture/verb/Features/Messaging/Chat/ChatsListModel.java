package gilm.lecture.verb.Features.Messaging.Chat;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * Created by harpreet on 11/2/17.
 */

public class ChatsListModel extends BasicApiModel {

    @SerializedName("chat")
    ArrayList<ChatDetailsModel> mChatList=new ArrayList<>();

    public ArrayList<ChatDetailsModel> getmChatList() {
        return mChatList;
    }

    public void setmChatList(ArrayList<ChatDetailsModel> mChatList) {
        this.mChatList = mChatList;
    }
}
