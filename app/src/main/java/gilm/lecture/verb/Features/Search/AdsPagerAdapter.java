/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Search;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import gilm.lecture.verb.Features.Discovery.VideoRecording.WebViewRecorderActivity;
import gilm.lecture.verb.Features.LectureEvent.Details.LectureEventDetailsActivity;
import gilm.lecture.verb.Features.Player.PlayerActivity;
import gilm.lecture.verb.Features.Subscription.SubscriptionActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.AdsModel;

public class AdsPagerAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    private List<AdsModel> adsModels;

    public AdsPagerAdapter(Context context, List<AdsModel> adsModels) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.adsModels = adsModels;
    }

    @Override
    public int getCount() {
        return adsModels.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.image_ads_pager, container, false);

        final View frameMain = itemView.findViewById(R.id.frameMain);
        final ImageView imageView = (ImageView) itemView.findViewById(R.id.image);
        final TextView txtvTextLec = (TextView) itemView.findViewById(R.id.txtvTextLec);

        ImageLoader.setImageBig(imageView, adsModels.get(position).getImage_path());
        txtvTextLec.setText(adsModels.get(position).getTitle());

        if (!UtillsG.getNotNullString(adsModels.get(position).getImage_path(), "").isEmpty()) {
            imageView.setVisibility(View.VISIBLE);
            txtvTextLec.setVisibility(View.GONE);
        } else {
            imageView.setVisibility(View.GONE);
            txtvTextLec.setVisibility(View.VISIBLE);
        }

        frameMain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                UtillsG.ads_counter(context, new SharedPrefHelper(context).getUserId(), adsModels.get(position).getId() + "");
                switch (adsModels.get(position).getAd_type()) {
                    // 1= subscription, 2= event, 3 = quick_lecture, 4 = custom link
                    case "1":
                        SubscriptionActivity.start(context);
                        break;
                    case "2":
                        LectureEventDetailsActivity.startActivityWithApiCall(context, adsModels.get(position).getData(), imageView);
                        break;
                    case "3":
                        PlayerActivity.startWithId(context, adsModels.get(position).getData(), false);
                        break;
                    case "4":
                        if (adsModels.get(position).getData().toLowerCase().contains("facebook") || adsModels.get(position).getData().toLowerCase().contains("fb")) {
                            try {
                                String url = adsModels.get(position).getData();
                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(url));
                                context.startActivity(i);
                            } catch (Exception e) {
                                WebViewRecorderActivity.startForBrowsingOnly(context, adsModels.get(position).getData());
                            }
                        } else {
                            WebViewRecorderActivity.startForBrowsingOnly(context, adsModels.get(position).getData());
                        }
                        break;
                }
            }
        });

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }

}