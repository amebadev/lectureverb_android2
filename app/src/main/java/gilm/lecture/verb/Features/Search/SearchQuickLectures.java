package gilm.lecture.verb.Features.Search;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Home.Home.HomeAdapter_NoBinding;
import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.Internal.Base.EmptyPresenter;
import gilm.lecture.verb.Features.Internal.Base.EmptyView;
import gilm.lecture.verb.Features.Settings.ArrayListLectureModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.EndlessRecyclerOnScrollListener;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.ActivitySearchQuickLecturesBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchQuickLectures extends BaseActivity<ActivitySearchQuickLecturesBinding, EmptyPresenter> implements EmptyView {

    String keyword = "";
    private int pagenumber = 1;
    List<Lecture_And_Event_Model> alModel = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private HomeAdapter_NoBinding homeAdapter;

    public static void start(Context context, String keyword) {
        Intent starter = new Intent(context, SearchQuickLectures.class);
        starter.putExtra("keyword", keyword);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_search_quick_lectures;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new EmptyPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        setupToolbar("Search Results");

        keyword = getIntent().getStringExtra("keyword");

        linearLayoutManager = new LinearLayoutManager(getActivityG(), LinearLayoutManager.VERTICAL, false);
        getDataBinder().recyclerView.setLayoutManager(linearLayoutManager);
        EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                pagenumber++;
                loadHomeData();
            }

            @Override
            public void countChange(int currentVisibleCount) {

            }
        };
        getDataBinder().recyclerView.setOnScrollListener(endlessRecyclerOnScrollListener);

        homeAdapter = new HomeAdapter_NoBinding(null, alModel, getActivityG());
        getDataBinder().recyclerView.setAdapter(homeAdapter);


        loadHomeData();
    }

    private void loadHomeData() {
        showLoading("Searching..");
        Call<ArrayListLectureModel> call = LVApplication.getRetrofit().create(HomeApis.class).search_quick_lecture(getLocalData().getUserId(), keyword, String.valueOf(pagenumber));
        call.enqueue(new Callback<ArrayListLectureModel>() {
            @Override
            public void onResponse(Call<ArrayListLectureModel> call, Response<ArrayListLectureModel> response) {
                hideLoading();

                if (response.body() != null) {
                    if (response.body().getStatus()) {
                        Log.e("get_lectures : OUTPUT", String.valueOf(response.body().getListOfTagsModel()));
                        if (pagenumber == 1) {
                            alModel.clear();
                        }
                        if (response.body().getListOfTagsModel() != null)
                        {
                            alModel.addAll(response.body().getListOfTagsModel());
                        }
                        getDataBinder().recyclerView.getAdapter().notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayListLectureModel> call, Throwable t) {
                UtillsG.showToast(t.getMessage() + "", getActivityG(), true);
            }
        });
    }

    @Override
    public Context getActivityG() {
        return SearchQuickLectures.this;
    }
}
