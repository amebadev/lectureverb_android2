package gilm.lecture.verb.Features.Help;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;

public class SupportActivity extends AppCompatActivity
{

    public static void start(Context context) {
        Intent starter = new Intent(context, SupportActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        setupToolbar("Support");
        findViewById(R.id.cardCall).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + ((TextView) findViewById(R.id.txtvNumber)).getText().toString()));
                startActivity(intent);
            }
        });
        findViewById(R.id.cardEmail).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{((TextView) findViewById(R.id.txtvEmail)).getText().toString()});
                intent.putExtra(Intent.EXTRA_SUBJECT, new SharedPrefHelper(SupportActivity.this).getUserName()
                        + " (" +
                        getResources().getString(R.string.app_name)
                        + ")");

                startActivity(Intent.createChooser(intent, "Email via..."));
            }
        });
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.title);
        if (viewById != null) {
            viewById.setText(title);
            getSupportActionBar().setTitle("");
        }
        else {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
