package gilm.lecture.verb.Features.InterestsSelection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.Settings.Tags.TagSettingsFragment;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.ActivityInterestSelectionBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * created by PARAMBIR SINGH on 31/8/17.
 */

public class InterestsSelectionActivity extends BaseActivity<ActivityInterestSelectionBinding, InterestSelectionPresenter> implements InterestSelectionView, View.OnClickListener
{

    boolean isForResult;
    ArrayList<ChildCategoryModel> alChildInterests = new ArrayList<ChildCategoryModel>();

    public static void start(Context context, String toolbarTitle) {
        Intent starter = new Intent(context, InterestsSelectionActivity.class);
        starter.putExtra("toolbarTitle", toolbarTitle);
        context.startActivity(starter);
    }

    /**
     * ArrayList<ChildCategoryModel> will be returned in onactivityresult in parameter "DATA" in @{@link gilm.lecture.verb.UtilsG.Constants.Extras}  method of the activity from where it has been called.
     *
     * @param context
     */
    public static void startfOResult(TagSettingsFragment context, String toolbarTitle, String commaSeparatedCategories) {
        Intent starter = new Intent(context.getActivity(), InterestsSelectionActivity.class);
        starter.putExtra("isForResult", true);
        starter.putExtra("toolbarTitle", toolbarTitle);
        starter.putExtra("commaSeparatedCategories", commaSeparatedCategories);
         context.startActivityForResult(starter, Constants.RequestCode.INTEREST_SELECTION);
    }

    public static void startfOResult(Context context, String toolbarTitle, String commaSeparatedCategories) {
        Intent starter = new Intent(context, InterestsSelectionActivity.class);
        starter.putExtra("isForResult", true);
        starter.putExtra("toolbarTitle", toolbarTitle);
        starter.putExtra("commaSeparatedCategories", commaSeparatedCategories);
        ((Activity) context).startActivityForResult(starter, Constants.RequestCode.INTEREST_SELECTION);
    }

    @Override
    protected void onCreateActivityG() {

        injectPresenter(new InterestSelectionPresenter());
        getPresenter().attachView(this);

    }

    @Override
    public void initViews() {
        isForResult = getIntent().getBooleanExtra("isForResult", false);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivityG(), 3, GridLayoutManager.VERTICAL, false);

        getDataBinder().recyCategories.setLayoutManager(gridLayoutManager);

        getDataBinder().btnSave.setOnClickListener(this);
        UtillsG.setTextSizeByPercentage(this, getDataBinder().btnToolbar, 6f);

        getDataBinder().btnToolbar.setText(getIntent().getStringExtra("toolbarTitle"));

        getCategories();
    }

    private void getCategories() {
        final List<String> items = Arrays.asList(UtillsG.getNotNullString(getIntent().getStringExtra("commaSeparatedCategories"), "").split("\\s*,\\s*"));
        getDataBinder().progressBar.setVisibility(View.VISIBLE);
        Call<ListOfInterestsModel> basicApiModelCall = getRetrofitInstance().get_categories();
        basicApiModelCall.enqueue(new Callback<ListOfInterestsModel>()
        {
            @Override
            public void onResponse(Call<ListOfInterestsModel> call, Response<ListOfInterestsModel> response) {
                getDataBinder().progressBar.setVisibility(View.GONE);
                if (response.body() != null && response.body().getStatus()) {
                    Log.e("RESPONSE", response.body().getStatus() + "  size = " + response.body().getCategories().size());

                    alChildInterests.clear();
                    for (int i = 0; i < response.body().getCategories().size(); i++) {
                        for (int j = 0; j < response.body().getCategories().get(i).getChild().size(); j++) {
                            ChildCategoryModel model = new ChildCategoryModel();
                            model.setCategory(response.body().getCategories().get(i).getChild().get(j).getCategory());
                            model.setId(response.body().getCategories().get(i).getChild().get(j).getId());
                            model.setParent_id(response.body().getCategories().get(i).getChild().get(j).getParent_id());
                            model.setParent_name(response.body().getCategories().get(i).getCategory());
                            for (int k = 0; k < items.size(); k++) {
                                if (response.body().getCategories().get(i).getChild().get(j).getCategory().trim().equalsIgnoreCase(items.get(k).trim())) {
                                    model.setChecked(true);
                                    break;
                                }
                                else {
                                    model.setChecked(false);
                                }
                            }
                            alChildInterests.add(model);
                        }
                    }

                    InterestsListAdapter interestsListAdapter = new InterestsListAdapter(alChildInterests, getActivityG());
                    interestsListAdapter.setShouldLoadMore(false);
                    getDataBinder().recyCategories.setAdapter(interestsListAdapter);
                }
            }

            @Override
            public void onFailure(Call<ListOfInterestsModel> call, Throwable t) {
                getDataBinder().progressBar.setVisibility(View.GONE);
                Log.e("ERROR get_posted_le", t.getMessage().toString());
                UtillsG.showToast(String.valueOf(t.getMessage().toString()), getActivityG(), true);
            }
        });
    }

    private InterestSelectionApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(InterestSelectionApi.class);
    }

    @Override
    public Context getActivityG() {
        return InterestsSelectionActivity.this;
    }

    @Override
    protected int setLayoutId() {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setFinishOnTouchOutside(true);
        return R.layout.activity_interest_selection;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSave:

                String commaSeparatedIds = "", commaSeparatedNames = "";
                ArrayList<ChildCategoryModel> alSelectedcategories = new ArrayList<>();
                for (int i = 0; i < alChildInterests.size(); i++) {
                    if (alChildInterests.get(i).isChecked()) {
                        alSelectedcategories.add(alChildInterests.get(i));
                        commaSeparatedIds = commaSeparatedIds + (commaSeparatedIds.isEmpty() ? ""
                                : ",") + alChildInterests.get(i).getId();
                        commaSeparatedNames = commaSeparatedNames + (commaSeparatedNames.isEmpty()
                                ? ""
                                : ", ") + alChildInterests.get(i).getCategory();
                    }
                }

                if (isForResult) {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.Extras.DATA, alSelectedcategories);
                    setResult(RESULT_OK, intent);

                    finish();
                }
                else {
                    if (!commaSeparatedIds.isEmpty()) {
                        showLoading("Please wait...");
                        Call<BasicApiModel> saveMyInterests = getRetrofitInstance().save_users_interest(commaSeparatedIds, new SharedPrefHelper(getActivityG()).getUserId());
                        final String finalCommaSeparatedNames = commaSeparatedNames;
                        saveMyInterests.enqueue(new Callback<BasicApiModel>()
                        {
                            @Override
                            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                                hideLoading();
                                UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                                if (response.body().getStatus()) {
                                    new SharedPrefHelper(getActivityG()).setInterestAlreadySet(getActivityG());
                                    new SharedPrefHelper(getActivityG()).setMyInterests(finalCommaSeparatedNames);
                                    finish();
                                }
                            }

                            @Override
                            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                                hideLoading();
                                UtillsG.showToast(t.getMessage(), getActivityG(), true);
                            }
                        });
                    }
                    else {
                        UtillsG.showToast("Please select at least one option.", getActivityG(), true);
                    }
                }

                break;
        }
    }
}
