package gilm.lecture.verb.Features.MonitorLecture;

import android.databinding.ObservableField;
import android.view.View;

/**
 * created by PARAMBIR SINGH on 12/4/18.
 */

public interface TestBinder
{
     void click(View view);

    ObservableField<String> getFirst();
    ObservableField<String> getSecond();

}
