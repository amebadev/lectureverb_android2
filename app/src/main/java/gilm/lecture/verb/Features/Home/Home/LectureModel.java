package gilm.lecture.verb.Features.Home.Home;

import java.io.Serializable;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;

/**
 * created by PARAMBIR SINGH on 4/9/17.
 */

public class LectureModel implements Serializable
{
    private String quick_lecture_id;
    private String user_id;
    private String quick_lecture_type;
    private String file_path;
    private String quick_lecture_text;
    private String thumbnail;
    private String discovery_url;
    private String privacy;
    private String date_time;
    private String comments_count;
    private String repost_lecture_count;
    private String likes_count;
    private String favourites_count;
    private String lecture_view;
    private String is_liked;
    private String is_favourite;
    private UserDataModel.UserData Userdata;
    private String quick_lecture_time_duration;
    private String history_stack;
    private Boolean is_reposted=false;

    public String getRepost_lecture_count() {
        return repost_lecture_count;
    }

    public void setRepost_lecture_count(String repost_lecture_count) {
        this.repost_lecture_count = repost_lecture_count;
    }

    public String getHistory_stack() {
        return history_stack;
    }

    public void setHistory_stack(String history_stack) {
        this.history_stack = history_stack;
    }

    public Boolean getIs_reposted() {
        return is_reposted;
    }

    public void setIs_reposted(Boolean is_reposted) {
        this.is_reposted = is_reposted;
    }

    public String getQuick_lecture_time_duration() {
        return quick_lecture_time_duration;
    }

    public void setQuick_lecture_time_duration(String quick_lecture_time_duration) {
        this.quick_lecture_time_duration = quick_lecture_time_duration;
    }

    public String getIs_favourite() {
        return is_favourite;
    }

    public void setIs_favourite(String is_favourite) {
        this.is_favourite = is_favourite;
    }

    public String getLecture_view() {
        return lecture_view;
    }

    public void setLecture_view(String lecture_view) {
        this.lecture_view = lecture_view;
    }

    public String getIs_liked() {
        return is_liked;
    }

    public void setIs_liked(String is_liked) {
        this.is_liked = is_liked;
    }

    public String getComments_count() {
        return comments_count;
    }

    public void setComments_count(String comments_count) {
        this.comments_count = comments_count;
    }

    public String getReposted_count() {
        return repost_lecture_count;
    }

    public void setReposted_count(String repost_lecture_count) {
        this.repost_lecture_count = repost_lecture_count;
    }

    public String getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(String likes_count) {
        this.likes_count = likes_count;
    }

    public String getFavourites_count() {
        return favourites_count;
    }

    public void setFavourites_count(String favourites_count) {
        this.favourites_count = favourites_count;
    }

    public UserDataModel.UserData getUserdata() {
        return Userdata;
    }

    public void setUserdata(UserDataModel.UserData userdata) {
        Userdata = userdata;
    }

    public String getQuick_lecture_id() {
        return quick_lecture_id;
    }

    public void setQuick_lecture_id(String quick_lecture_id) {
        this.quick_lecture_id = quick_lecture_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getQuick_lecture_type() {
        return quick_lecture_type;
    }

    public void setQuick_lecture_type(String quick_lecture_type) {
        this.quick_lecture_type = quick_lecture_type;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getQuick_lecture_text() {
        return quick_lecture_text;
    }

    public void setQuick_lecture_text(String quick_lecture_text) {
        this.quick_lecture_text = quick_lecture_text;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDiscovery_url() {
        return discovery_url;
    }

    public void setDiscovery_url(String discovery_url) {
        this.discovery_url = discovery_url;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }
}
