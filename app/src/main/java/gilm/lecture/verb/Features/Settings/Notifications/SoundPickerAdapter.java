package gilm.lecture.verb.Features.Settings.Notifications;

import android.content.Context;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.R;

/**
 * created by PARAMBIR SINGH on 15/11/17.
 */

public class SoundPickerAdapter extends BaseAdapter
{
    private Context context;
    private List<String> alSoundNames;
    private int[] alSoundIds;
    private NotificationSettingsFragment fragment;
    private List<Boolean> alIsChecked = new ArrayList<>();

    public SoundPickerAdapter(Context context, List<String> alSoundNames, int[] alSoundIds,List<Boolean> alIsChecked,NotificationSettingsFragment fragment) {
        this.context = context;
        this.alSoundNames = alSoundNames;
        this.alSoundIds = alSoundIds;
        this.alIsChecked = alIsChecked;
        this.fragment = fragment;
    }

    @Override
    public int getCount() {
        return alSoundIds.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.inflater_sound_selector, null);

        LinearLayout llMain = (LinearLayout) convertView.findViewById(R.id.llMain);
        TextView txtvSoundName = (TextView) convertView.findViewById(R.id.txtvSoundName);

        txtvSoundName.setText(alSoundNames.get(i));

        ImageView imgvCheckUncheck = (ImageView) convertView.findViewById(R.id.imgvCheckUncheck);
        if (alIsChecked.get(i)) {
            imgvCheckUncheck.setVisibility(View.VISIBLE);
        }
        else {
            imgvCheckUncheck.setVisibility(View.INVISIBLE);
        }

        llMain.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                MediaPlayer.create(context, alSoundIds[i]).start();

                fragment.selectSoundIndex = i;

                for (int j = 0; j < alSoundIds.length; j++) {
                    if (j != i) {
                        alIsChecked.set(j, false);
                    }
                    else {
                        alIsChecked.set(j, true);
                    }
                }
                fragment.listOfSounds.invalidateViews();
            }
        });


        return convertView;
    }
}
