package gilm.lecture.verb.Features.Home.ListingByCategory;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * created by PARAMBIR SINGH on 26/9/17.
 */

public interface ListingByCategoriesView extends Viewable<ListingByCategoriesPresenter>
{

    View getProgressBar();

    RecyclerView getRecyclerView();

    TextView getNoDataTextview();

    int getIncreasedPageNumber();
}
