package gilm.lecture.verb.Features.LectureEvent;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by G-Expo on 07 Jul 2017.
 */
// LV


@StringDef({AttendingLecture.GOING_DIGITAL, AttendingLecture.ATTENDING, AttendingLecture.IS_INTERESTED, AttendingLecture.IS_IGNORED})
@Retention(RetentionPolicy.SOURCE)
public @interface AttendingLecture
{
    String NO_OUTPUT     = null;
    String GOING_DIGITAL = "GOING DIGITAL";
    String ATTENDING     = "ATTENDING";
    String IS_INTERESTED = "INTERESTED";
    String IS_IGNORED     = "IGNORED";
}
