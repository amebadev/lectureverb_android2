package gilm.lecture.verb.Features.Home.Details;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import gilm.lecture.verb.R;

public class LectureDetails_NoBinding extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecture_details__no_binding);
    }
}
