package gilm.lecture.verb.Features.LectureEvent;

import android.app.Activity;
import android.util.Log;

import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EventLectureApi;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harpreet on 9/19/17.
 */

public class LectureEventClicks {


    public void setAttendingStatus(final Activity mActivity, LectureEventsApi getRetrofitInstance, String eventId, String attend_type,  final CallBackG<Boolean> values){
        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance.event_attend_and_interest(
                getOtherParams(new SharedPrefHelper(mActivity).getUserId())
                , getOtherParams(eventId)
                , getOtherParams(attend_type)
                , getOtherParams("attend")
                , getOtherParams("true")
                );
        basicApiModelCall.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body() != null) {

                    UtillsG.showToast(response.body().getMessage(),mActivity,true);

                    if (response.body().getStatus()) {
                        values.onCallBack(true);
                    }
                    else{
                        values.onCallBack(false);
                    }
                }
                else{
                    values.onCallBack(false);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("Error occured", t.getMessage().toString());
                values.onCallBack(false);
            }
        });
    }

    public void setInterestedStatus(final Activity mActivity, LectureEventsApi getRetrofitInstance, String eventId, String is_interested, final CallBackG<Boolean> values){
        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance.event_attend_and_interest(
                getOtherParams(new SharedPrefHelper(mActivity).getUserId())
                , getOtherParams(eventId)
                , getOtherParams("pending")
                , getOtherParams("interest")
                , getOtherParams(is_interested)
        );
        basicApiModelCall.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body() != null) {

                    UtillsG.showToast(response.body().getMessage(),mActivity,true);

                    if (response.body().getStatus()) {
                        values.onCallBack(true);
                    }
                    else{
                        values.onCallBack(false);
                    }
                }
                else{
                    values.onCallBack(false);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("Error occured", t.getMessage().toString());
                values.onCallBack(false);
            }
        });
    }


    public void SetInvitationViaApp(final Activity mActivity, LectureEventsApi getRetrofitInstance, String eventId, String recieverIds, final CallBackG<Boolean> values){
        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance.send_event_invitation(
                getOtherParams(new SharedPrefHelper(mActivity).getUserId())
                , getOtherParams(recieverIds)
                , getOtherParams(eventId)
        );
        basicApiModelCall.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body() != null) {

                    UtillsG.showToast(response.body().getMessage(),mActivity,true);

                    if (response.body().getStatus()) {
                        values.onCallBack(true);
                    }
                    else{
                        values.onCallBack(false);
                    }
                }
                else{
                    values.onCallBack(false);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("Error occured", t.getMessage().toString());
                values.onCallBack(false);
            }
        });
    }

    public void ShareOnUserProfileFeed(final Activity mActivity, LectureEventsApi getRetrofitInstance, String shared_id, String share_type,final CallBackG<Boolean> values){
        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance.share(
                new SharedPrefHelper(mActivity).getUserId()
                , shared_id
                ,share_type
        );
        basicApiModelCall.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body() != null) {

                    UtillsG.showToast(response.body().getMessage(),mActivity,true);

                    if (response.body().getStatus()) {
                        values.onCallBack(true);
                    }
                    else{
                        values.onCallBack(false);
                    }
                }
                else{
                    values.onCallBack(false);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("Error occured", t.getMessage().toString());
                values.onCallBack(false);
            }
        });
    }


    public void setLectureInterests(final Activity mActivity, EventLectureApi getRetrofitInstance, String eventId, String interestedIds, String interestType,final CallBackG<Boolean> values){
        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance.save_lecture_or_event_interest(
                new SharedPrefHelper(mActivity).getUserId()
                , eventId
                , interestedIds
                , interestType
        );
        basicApiModelCall.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body() != null) {

                    UtillsG.showToast(response.body().getMessage(),mActivity,true);

                    if (response.body().getStatus()) {
                        values.onCallBack(true);
                    }
                    else{
                        values.onCallBack(false);
                    }
                }
                else{
                    values.onCallBack(false);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("Error occured", t.getMessage().toString());
                values.onCallBack(false);
            }
        });
    }


    public void RepostQuickLecture(final Activity mActivity, HomeApis getRetrofitInstance, String quickLectureId, final CallBackG<Boolean> values){
        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance.repost_lecture(
                new SharedPrefHelper(mActivity).getUserId(),quickLectureId
        );
        basicApiModelCall.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body() != null) {

                    UtillsG.showToast(response.body().getMessage(),mActivity,true);

                    if (response.body().getStatus()) {
                        values.onCallBack(true);
                    }
                    else{
                        values.onCallBack(false);
                    }
                }
                else{
                    values.onCallBack(false);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("Error occured", t.getMessage().toString());
                values.onCallBack(false);
            }
        });
    }

    public void unrepost_lecture(final Activity mActivity, String quickLectureId, final CallBackG<Boolean> values){
        UtillsG.showLoading("Please wait",mActivity);
        Call<BasicApiModel> basicApiModelCall = LVApplication.getRetrofit().create(ProfileApi.class).unrepost_lecture(
                quickLectureId,new SharedPrefHelper(mActivity).getUserId()
        );
        basicApiModelCall.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                UtillsG.hideLoading();
                if (response.body() != null) {

                    UtillsG.showToast(response.body().getMessage(),mActivity,true);

                    if (response.body().getStatus()) {
                        values.onCallBack(true);
                    }
                    else{
                        values.onCallBack(false);
                    }
                }
                else{
                    values.onCallBack(false);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                UtillsG.hideLoading();
                Log.e("Error occured", t.getMessage().toString());
                values.onCallBack(false);
            }
        });
    }



    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }
}
