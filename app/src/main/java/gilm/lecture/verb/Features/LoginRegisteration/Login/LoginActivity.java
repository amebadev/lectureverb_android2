package gilm.lecture.verb.Features.LoginRegisteration.Login;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterSession;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.LoginRegisteration.EmailVerification.EmailVerificationActivity;
import gilm.lecture.verb.Features.LoginRegisteration.ForgotPassword.ForgotPasswordActivity;
import gilm.lecture.verb.Features.LoginRegisteration.Registration.RegistrationActivity;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.databinding.ActivityLoginBinding;

public class LoginActivity extends BaseActivity<ActivityLoginBinding, LoginPresenter> implements LoginView, View.OnClickListener
{
//    boolean isLecturer;

    public static void start(Context context) {
        Intent starter = new Intent(context, LoginActivity.class);
        context.startActivity(starter);
    }

    public static void startForLecturer(Context context) {
        Intent starter = new Intent(context, LoginActivity.class);
//        starter.putExtra("isLecturer", true);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new LoginPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
//        isLecturer = getIntent().getBooleanExtra("isLecturer", false);

        setProgressTitle("Verifying details");
        setupToolbar("");
        getDataBinder().setData(getPresenter());
        getPresenter().intiSocialMedia();

        getDataBinder().layoutForgetPassword.setOnClickListener(this);
        getDataBinder().layoutSignUp.setOnClickListener(this);

/*
        if (isLecturer) {
            getDataBinder().layoutSignUp.setVisibility(View.GONE);
            getDataBinder().viewSocialLogin.setVisibility(View.GONE);
            getDataBinder().txtvOrSignUp.setVisibility(View.GONE);
            getDataBinder().llSocialMedia.setVisibility(View.GONE);
        }
*/
    }

    @Override
    public Context getActivityG() {
        return LoginActivity.this;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layoutForgetPassword:
                ForgotPasswordActivity.start(getActivityG());
                break;
            case R.id.layoutSignUp:
                RegistrationActivity.start(getActivityG());
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getDataBinder().twitterLoginButton.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == Constants.RequestCode.GP_SIGN_IN) {
            getPresenter().onActivityResultGP(data);
        }

        getPresenter().callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void googlePlusSignInFail() {
        displayError("Error while fetching users data.");
    }

    @Override
    public void twitterFailure(String message) {
        displayError(message);
    }

    @Override
    public void twitterLoggedInSuccessfully(Result<TwitterSession> result) {
        getLocalData().setIsRegistered(true);
        NavigationActivity.start(getActivityG());
    }

    @Override
    public void openNavigationActivity(UserDataModel userDataModel) {
        getLocalData().setIsVerified(true);

        if (userDataModel != null) {
            getLocalData().setUserData(userDataModel.getData());
        }
        NavigationActivity.start(getActivityG());
    }

    @Override
    public void openEmailVerification() {
        EmailVerificationActivity.start(getActivityG());
    }
}
