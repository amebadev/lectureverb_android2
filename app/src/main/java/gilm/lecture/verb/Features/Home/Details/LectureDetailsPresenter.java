/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Home.Details;

import android.databinding.Observable;
import android.databinding.ObservableField;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.Messaging.Chat.ChatBinder;
import gilm.lecture.verb.Features.Messaging.Chat.ChatView;
import gilm.lecture.verb.Features.Messaging.Chat.MsgDataModel;
import gilm.lecture.verb.UtilsG.DateHelper;

/**
 * Created by G-Expo on 08 Mar 2017.
 */

public class LectureDetailsPresenter extends BasePresenter<ChatView> implements ChatBinder
{
    private List<MsgDataModel> listData;

    public ObservableField<String> textMessage =
            new ObservableField<>();
    public ObservableField<String> imageUrl    =
            new ObservableField<>();
    public ObservableField<String> name        =
            new ObservableField<>();

    public LectureDetailsPresenter()
    {
        listData = new ArrayList<>();
        textMessage.set("");
        name.set("Name");
        imageUrl.set("https://students.ucsd.edu/_images/student-life/involvement/getinvolved/csi-staff-contacts/Casey.jpg");


        textMessage.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback()
        {
            @Override
            public void onPropertyChanged(Observable observable, int i)
            {
                getView().flipButtons(textMessage.get().isEmpty());
            }
        });
    }

    /**
     * @return Messages list.
     */
    public List<MsgDataModel> getChatList()
    {
        return listData;
    }

    @Override
    public void sendMessage(View view)
    {
        if (textMessage.get().isEmpty()) {
            return;
        }
        //sending messages to server
        String id               = "0";
        String sender_userid    = "1";//static user id for now.
        String recipient_userid = "2";
        String message          = textMessage.get();
        String created_at       = DateHelper.getInstance().getCurrentDateForChat();

        MsgDataModel msgDataModel = new MsgDataModel();

        msgDataModel.setChat_id(id);
        msgDataModel.setSender_id(sender_userid);
        msgDataModel.setReceiver_id(recipient_userid);
        msgDataModel.setMessage(message);
        msgDataModel.setDate(created_at);

        listData.add(msgDataModel);
        getView().updateMyMessage(listData);

        textMessage.set("");
    }

    @Override
    public ObservableField<String> textMessage()
    {
        return textMessage;
    }

    @Override
    public ObservableField<String> imageUrl()
    {
        return imageUrl;
    }

    @Override
    public ObservableField<String> name()
    {
        return name;
    }

}
