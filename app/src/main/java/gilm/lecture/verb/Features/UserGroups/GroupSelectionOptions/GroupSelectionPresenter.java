package gilm.lecture.verb.Features.UserGroups.GroupSelectionOptions;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.UserGroups.CreateNewGroupAndSignUp.CreateGroupAndSignUpActivity;

/**
 * created by PARAMBIR SINGH on 18/12/17.
 */

public class GroupSelectionPresenter extends BasePresenter
{

    public void createGroup() {
        CreateGroupAndSignUpActivity.start(getView().getActivityG());
    }
}
