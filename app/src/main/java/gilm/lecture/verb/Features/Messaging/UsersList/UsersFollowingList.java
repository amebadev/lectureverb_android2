package gilm.lecture.verb.Features.Messaging.UsersList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EventLectureApi;
import gilm.lecture.verb.Features.Internal.Base.BaseActivityWithoutPresenter;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.Followers.ListOfUserModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.EndlessRecyclerOnScrollListener;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harpreet on 11/2/17.
 */

public class UsersFollowingList extends BaseActivityWithoutPresenter implements View.OnClickListener, ProgressRequestBody.UploadCallbacks {


    EditText edsearch;
    RecyclerView recycle_list;
    int page = 1;
    Double mLatitude = 0.0, mLongitude = 0.0;
    public ArrayList<UserDataModel.UserData> mUsersList = new ArrayList<>();
    UserFollowingAdapter usersListAdapter;
    Activity mActivity;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;

    public static void start(Context context) {
        Intent starter = new Intent(context, UsersFollowingList.class);
        ((Activity) context).startActivityForResult(starter, 102);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_lecture_co_host);
        setupToolbar("Your following users");
        mActivity = this;
        initViews();
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (viewById != null) {
            viewById.setText(title);
        } else {
            getSupportActionBar().setTitle(title);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void initViews() {

        ((ImageView) findViewById(R.id.imgv_header_image)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.txtv_headerName)).setVisibility(View.GONE);

        edsearch = (EditText) findViewById(R.id.edSearch);
        recycle_list = (RecyclerView) findViewById(R.id.recycle_liststatic);
        LinearLayoutManager mLinearManager = new LinearLayoutManager(getActivityG(), LinearLayoutManager.VERTICAL, false);
        recycle_list.setLayoutManager(mLinearManager);


        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(mLinearManager) {
            @Override
            public void onLoadMore(int current_page) {
                page++;
             /*   getCoHostLists();*/
            }

            @Override
            public void countChange(int currentVisibleCount) {

            }
        };
        recycle_list.setOnScrollListener(endlessRecyclerOnScrollListener);

        usersListAdapter = new UserFollowingAdapter((Activity) getActivityG(), mUsersList, this);
        recycle_list.setAdapter(usersListAdapter);

        edsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().trim().isEmpty()) {
//                    usersListAdapter.getFilter().filter(s);
                } else {
                    getCoHostLists();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ((ImageView) findViewById(R.id.imgvGo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSearchedCoHosts(edsearch.getText().toString().trim());
            }
        });
        getCoHostLists();
    }

    @Override
    public Context getActivityG() {
        return UsersFollowingList.this;
    }

    @Override
    public void onClick(View view) {

    }


    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    public void getSearchedCoHosts(String searchedKeyword) {
        UtillsG.showLoading("Searching...", mActivity);
        Call<ListOfUserModel> getFollowersList = LVApplication.getRetrofit().create(ProfileApi.class).search_user(new SharedPrefHelper(mActivity).getUserId(), "1", searchedKeyword);
        getFollowersList.enqueue(new Callback<ListOfUserModel>() {
            @Override
            public void onResponse(Call<ListOfUserModel> call, Response<ListOfUserModel> response) {
                UtillsG.hideLoading();
                if (response.body().getStatus()) {
                    if (!(response.body().getUsersList()).isEmpty()) {
                        mUsersList.clear();
                        mUsersList.addAll(response.body().getUsersList());
                        recycle_list.getAdapter().notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<ListOfUserModel> call, Throwable t) {
                UtillsG.hideLoading();
                UtillsG.showToast(t.getMessage(), mActivity, true);
            }
        });
    }

    public void getCoHostLists() {
        UtillsG.showLoading("Getting your followers list", mActivity);
        Call<ListOfUserModel> getFollowersList = LVApplication.getRetrofit().create(ProfileApi.class).get_my_followings(new SharedPrefHelper(mActivity).getUserId());
        getFollowersList.enqueue(new Callback<ListOfUserModel>() {
            @Override
            public void onResponse(Call<ListOfUserModel> call, Response<ListOfUserModel> response) {
                UtillsG.hideLoading();
                if (response.body().getStatus()) {
                    if (!(response.body().getUsersList()).isEmpty()) {
                        endlessRecyclerOnScrollListener.reset();
                        mUsersList.clear();
                        mUsersList.addAll(response.body().getUsersList());
                        recycle_list.getAdapter().notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<ListOfUserModel> call, Throwable t) {
                UtillsG.hideLoading();
                UtillsG.showToast(t.getMessage(), mActivity, true);
            }
        });
    }

    public ArrayList<UserDataModel.UserData> getUsersList() {
        return mUsersList;
    }

    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    protected EventLectureApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(EventLectureApi.class);
    }


}
