package gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent;

import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.CoHostListModel;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EditLectureEvent.EditSingleLectureEventModel;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureModel;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.Web;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by harpreet on 9/11/17.
 */

public interface EventLectureApi
{

    /*
     URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
        API NAme => create_lecture_event
        Method = > POST
        Description => fiile name = lecture image , recording_type = pre_recorded / live_recorded , can_invite_friends = true / false , lecturer_co_hosts = users ids comaseparated
         , isPublic = true / false
        Parameaters = > event_id ,user_id,	lecture_title,	lecture_poster,	recording_type,	address,	latitude,	longitude,	details,	can_invite_friends,
        	lecturer_co_hosts,	lecture_subject_tags,	isPublic,	lecture_start_date_time,	lecture_end_date_time,	lecture_created_date_time
        Code ==> 200 for success 400 for error

        */

    @Multipart
    @POST(Web.Path.create_lecture_event)
    Call<EventLectureCreatedResponse> create_lecture_event(
            @Part MultipartBody.Part filePart,
            @Part("user_id") RequestBody user_id
            , @Part("lecture_title") RequestBody lecture_title
            , @Part("recording_type") RequestBody recording_type //----------- pre_recorded / live_recorded
            , @Part("address") RequestBody address
            , @Part("latitude") RequestBody latitude
            , @Part("longitude") RequestBody longitude
            , @Part("can_invite_friends") RequestBody can_invite_friends
            , @Part("lecturer_co_hosts") RequestBody lecturer_co_hosts
            , @Part("lecture_subject_tags") RequestBody lecture_subject_tags
            , @Part("isPublic") RequestBody isPublic
            , @Part("lecture_start_date_time") RequestBody lecture_start_date_time // yyyy-MM-dd hh:mm:ss
            , @Part("lecture_end_date_time") RequestBody lecture_end_date_time
            , @Part("details") RequestBody details
            , @Part("user_name") RequestBody user_name
            , @Part("lecturer_id") RequestBody lecturer_id
            , @Part("group_id") RequestBody group_id
    );


    @Multipart
    @POST(Web.Path.update_lecture_event)
    Call<EditSingleLectureEventModel> update_lecture_event(
            @Part MultipartBody.Part filePart,
            @Part("event_id") RequestBody event_id,
            @Part("user_id") RequestBody user_id
            , @Part("lecture_title") RequestBody lecture_title
            , @Part("recording_type") RequestBody recording_type //----------- pre_recorded / live_recorded
            , @Part("address") RequestBody address
            , @Part("latitude") RequestBody latitude
            , @Part("longitude") RequestBody longitude
            , @Part("can_invite_friends") RequestBody can_invite_friends
            , @Part("lecturer_co_hosts") RequestBody lecturer_co_hosts
            , @Part("lecture_subject_tags") RequestBody lecture_subject_tags
            , @Part("isPublic") RequestBody isPublic
            , @Part("lecture_start_date_time") RequestBody lecture_start_date_time // yyyy-MM-dd hh:mm:ss
            , @Part("lecture_end_date_time") RequestBody lecture_end_date_time
            , @Part("details") RequestBody details
            , @Part("user_name") RequestBody user_name
            , @Part("lecturer_id") RequestBody lecturer_id
            , @Part("group_id") RequestBody group_id
    );



  /*  URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => search_user
    Method = > GET / POST
    Description => Get user By name or email
    Parameaters = > user_id , page, keyword
    Code ==> 200 for success 400 for error*/

    @Multipart
    @POST(Web.Path.search_user)
    Call<CoHostListModel> search_user(
            @Part("user_id") RequestBody user_id
            , @Part("page") RequestBody page
            , @Part("keyword") RequestBody keyword
    );


   /* URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => get_single_user
    Method = > GET / POST
    Description => Get single user
    Parameaters = > user_id
    Code ==> 200 for success 400 for error*/

    @Multipart
    @POST(Web.Path.get_single_user)
    Call<QuickLectureModel> get_single_user(
            @Part("user_id") RequestBody user_id
    );



  /*  URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => get_nearby_co_hosts
    Method = > GET / POST
    Description => Get nearby co hosts
    Parameaters = > user_id , latitude , longitude , page , distance
    Code ==> 200 for success 400 for error*/

    @Multipart
    @POST(Web.Path.get_nearby_co_hosts)
    Call<CoHostListModel> get_nearby_co_hosts(
            @Part("user_id") RequestBody user_id
            , @Part("latitude") RequestBody latitude
            , @Part("longitude") RequestBody longitude
            , @Part("page") RequestBody page
            , @Part("distance") RequestBody distance
            , @Part("lecturer_only") RequestBody lecturer_only
    );


/*    lectureverb:save_lecture_or_event_interest
    category_id:6,7,8,9
    user_id:18
    interest_id:29
    interest_type:event*/

    @FormUrlEncoded
    @POST(Web.Path.save_lecture_or_event_interest)
    Call<BasicApiModel> save_lecture_or_event_interest(
            @Field("user_id") String category_id
            , @Field("interest_id") String user_id
            , @Field("category_id") String interest_id
            , @Field("interest_type") String interest_type
    );


}
