package gilm.lecture.verb.Features.LectureEvent.Details.About;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.R;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class AboutFragment extends Fragment {

    View mRootView;
    TextView txtv_details;
    LectureDetailsModel mLectureDetails = null;
    LinearLayout createrGroup;

    public static AboutFragment newInstance(LectureDetailsModel mLectureDetails) {
        return new AboutFragment(mLectureDetails);
    }

    public AboutFragment(LectureDetailsModel mLectureDetails) {
        // Required empty public constructor
        this.mLectureDetails = mLectureDetails;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_only_text, container, false);

        initViews();

        setData();

        return mRootView;
    }

    public void initViews() {
        txtv_details = (TextView) mRootView.findViewById(R.id.txtv_details);
        createrGroup = (LinearLayout) mRootView.findViewById(R.id.createrGroup);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int screenWidth = Math.round(display.getWidth());
        int screenHeight = Math.round(display.getHeight());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(screenWidth, screenHeight);
        ((LinearLayout) mRootView.findViewById(R.id.llMain)).setLayoutParams(params);

        createrGroup.setVisibility(View.GONE);
        (mRootView.findViewById(R.id.txtvTotalMembers)).setVisibility(View.GONE);
        (mRootView.findViewById(R.id.txtvLecturersCount)).setVisibility(View.GONE);
        (mRootView.findViewById(R.id.txtvAdmin)).setVisibility(View.GONE);
    }

    public void setData() {
        if (mLectureDetails != null) {
            txtv_details.setText(mLectureDetails.getDetails());
        }
    }

}
