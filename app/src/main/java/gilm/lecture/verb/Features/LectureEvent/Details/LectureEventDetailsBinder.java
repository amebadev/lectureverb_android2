/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.LectureEvent.Details;

import android.view.View;

import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;

/**
 * Created by G-Expo on 24 Jul 2017.
 */

public interface LectureEventDetailsBinder
{
    void gotoProduction(View view);

    void gotoEdit(View view);

    void gotoPreview(View view);

    /**
     * mark an event as attendant.
     *
     * @param viewModel --data on clicked view.
     */
    void markAttendant(View view, LectureEventViewModel viewModel);

    /**
     * mark an event as interested.
     *
     * @param viewModel --data on clicked view.
     */
    void markInterested(View view, LectureEventViewModel viewModel);

    /**
     * share this event
     *
     * @param viewModel --data on clicked view.
     */
    void share(LectureEventViewModel viewModel);
}
