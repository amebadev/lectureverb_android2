package gilm.lecture.verb.Features.Home.Home;

import android.view.View;

/**
 * Created by G-Expo on 12 Jul 2017.
 */

public interface HomePresenterBinder
{
    void showDetailsWithAnim(View view);

    void showOptions(View view, HomeViewModel data);

    int getLayoutHeight();

    void play(View view, HomeViewModel data);

}
