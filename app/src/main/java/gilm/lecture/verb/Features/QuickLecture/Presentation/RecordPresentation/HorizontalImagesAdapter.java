package gilm.lecture.verb.Features.QuickLecture.Presentation.RecordPresentation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import gilm.lecture.verb.Features.QuickLecture.Presentation.PresentationOptions.SingleImageModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.InfiniteAdapter_WithoutBuinding;
import gilm.lecture.verb.UtilsG.LoadingViewHolder;
import gilm.lecture.verb.Widget.SquareImageView;

public class HorizontalImagesAdapter extends InfiniteAdapter_WithoutBuinding<MyViewHolderG>
{
    private LayoutInflater inflater;

    private List<SingleImageModel> alLectureModels;
    Context context;

    public HorizontalImagesAdapter(List<SingleImageModel> dList, Context context) {
        this.alLectureModels = dList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        return new LoadingViewHolder(inflater.inflate(R.layout.layout_progress, parent, false));
    }

    @Override
    public int getCount() {
        return alLectureModels.size();
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public MyViewHolderG onCreateView(ViewGroup parent, int viewType) {
        return new MyViewHolderG(inflater.inflate(R.layout.inflater_ppt_slider, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        try {
            if (!(holder instanceof LoadingViewHolder)) {
                if (alLectureModels.get(position).getDownload_file().toLowerCase().contains("http")) {
                    Picasso.with(context)
                            .load(alLectureModels.get(position).getDownload_file())
                            .placeholder(R.drawable.ic_loading_landscape)
                            .resize(100, 100)
                            .into(((MyViewHolderG) holder).rimgv_image);
                }
                else {
                    Picasso.with(context)
                            .load(new File(alLectureModels.get(position).getDownload_file()))
                            .placeholder(R.drawable.ic_loading_landscape)
                            .into(((MyViewHolderG) holder).rimgv_image);
                }
                ((MyViewHolderG) holder).rimgv_image.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        ((RecordPresentationActivity) context).auto_viewpager.setCurrentItem(position);
                    }
                });
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class MyViewHolderG extends RecyclerView.ViewHolder
{
    View view;
    SquareImageView rimgv_image;
    FrameLayout framLayout;

    public MyViewHolderG(View row) {
        super(row);

        view = row;
        rimgv_image = (SquareImageView) view.findViewById(R.id.rimgv_image);
        framLayout = (FrameLayout) view.findViewById(R.id.framLayout);
    }
}
