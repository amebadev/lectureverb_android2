/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Discovery;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.czt.mp3recorder.MP3Recorder;
import com.shuyu.waveview.AudioPlayer;
import com.shuyu.waveview.FileUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import gilm.lecture.verb.Features.Discovery.VideoRecording.WebViewRecorderActivity;
import gilm.lecture.verb.Features.InterestsSelection.AddInterests.AddInterestCategoriesActivity;
import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EventLectureApi;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence.UserType;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureApi;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureModel;
import gilm.lecture.verb.Features.QuickLecture.ThumbnailResponseModel;
import gilm.lecture.verb.Features.WebHistory.WebHistoryActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.FileHelper;
import gilm.lecture.verb.UtilsG.PKromeProgressBar;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.UtilsG.WebInnerStackModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 24 Jul 2017.
 */

public class DiscoveryTabFragment extends Fragment implements View.OnClickListener, ProgressRequestBody.UploadCallbacks
{
    private View view;
    ImageView imgvDiscovery, imgvCapture, imgvRecordScreen, imgvBarCode, imgvOptions;
    WebView webView;
    private Uri croppedImage;
    private ProgressDialog progressDialog;
    PKromeProgressBar pkProgressBar;
    FrameLayout frameLayout;
    ImageView imgvRecord;

    ArrayList<WebInnerStackModel> alWebHistoryStack = new ArrayList<>();

    String startingUrl = "";

    String commaseparatedIds = "", commaSeparatedNames = "";

    TextView txtvLectureCategory;// we are taking this global in this class because when user will comeback after selecting categories, we need this instance where we have to set comma separated selected ids

    LectureEventClicks mClicks = new LectureEventClicks();

    public static DiscoveryTabFragment newInstance()
    {
        return new DiscoveryTabFragment();
    }

    public static DiscoveryTabFragment newInstance(String startingUrl)
    {
        return new DiscoveryTabFragment(startingUrl);
    }

    public DiscoveryTabFragment()
    {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public DiscoveryTabFragment(String startingUrl)
    {
        this.startingUrl = startingUrl;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_discovery, container, false);

        getActivity().getWindow().setFeatureInt(Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);

        frameLayout = (FrameLayout) view.findViewById(R.id.frameLayout);
        pkProgressBar = (PKromeProgressBar) view.findViewById(R.id.pkProgressBar);

        imgvDiscovery = (ImageView) view.findViewById(R.id.imgvDiscovery);
        imgvCapture = (ImageView) view.findViewById(R.id.imgvCapture);
        imgvRecordScreen = (ImageView) view.findViewById(R.id.imgvRecordScreen);
        imgvBarCode = (ImageView) view.findViewById(R.id.imgvBarCode);
        imgvOptions = (ImageView) view.findViewById(R.id.imgvOptions);

        imgvDiscovery.setOnClickListener(this);
        imgvCapture.setOnClickListener(this);
        imgvRecordScreen.setOnClickListener(this);
        imgvBarCode.setOnClickListener(this);
        imgvOptions.setOnClickListener(this);

        initWebView();

        return view;
    }

    private void initWebView()
    {
        webView = (WebView) view.findViewById(R.id.webView);
        webView.loadUrl(startingUrl.isEmpty() ? "https://www.google.co.in/" : startingUrl);
        webView.setWebViewClient(new WebViewClient()
        {
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return false;
            }
        });

        webView.setWebChromeClient(new WebChromeClient()
        {
            public void onProgressChanged(final WebView view, int progress)
            {
                pkProgressBar.setProgress(progress);
                if (progress == 100)
                {
                    WebInnerStackModel model = new WebInnerStackModel();
                    model.setTitle(webView.getTitle());
                    model.setUrl(webView.getUrl());
                    alWebHistoryStack.add(model);
                }
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUserAgentString("Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3");

    }

    private void selectDiscoveryOption()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            imgvDiscovery.setBackgroundResource(R.drawable.white_transparent_circle);
            imgvCapture.setBackgroundResource(R.drawable.default_btn_selector);
            imgvRecordScreen.setBackgroundResource(R.drawable.default_btn_selector);
            imgvBarCode.setBackgroundResource(R.drawable.default_btn_selector);
            imgvOptions.setBackgroundResource(R.drawable.default_btn_selector);
        } else
        {
            UtillsG.showToast("Your device does not support this feature.", getActivity(), true);
        }
    }

    private void selectCaptureOption()
    {
        imgvCapture.setBackgroundResource(R.drawable.white_transparent_circle);
        imgvDiscovery.setBackgroundResource(R.drawable.default_btn_selector);
        imgvRecordScreen.setBackgroundResource(R.drawable.default_btn_selector);
        imgvBarCode.setBackgroundResource(R.drawable.default_btn_selector);
        imgvOptions.setBackgroundResource(R.drawable.default_btn_selector);
    }

    private void selectShareOption()
    {
        imgvRecordScreen.setBackgroundResource(R.drawable.white_transparent_circle);
        imgvCapture.setBackgroundResource(R.drawable.default_btn_selector);
        imgvDiscovery.setBackgroundResource(R.drawable.default_btn_selector);
        imgvBarCode.setBackgroundResource(R.drawable.default_btn_selector);
        imgvOptions.setBackgroundResource(R.drawable.default_btn_selector);
    }

    private void selectTrimOption()
    {
        imgvBarCode.setBackgroundResource(R.drawable.white_transparent_circle);
        imgvCapture.setBackgroundResource(R.drawable.default_btn_selector);
        imgvRecordScreen.setBackgroundResource(R.drawable.default_btn_selector);
        imgvDiscovery.setBackgroundResource(R.drawable.default_btn_selector);
        imgvOptions.setBackgroundResource(R.drawable.default_btn_selector);
    }

    private void selectMoreOption()
    {
        imgvOptions.setBackgroundResource(R.drawable.white_transparent_circle);
        /*imgvCapture.setBackgroundResource(R.drawable.default_btn_selector);
        imgvRecordScreen.setBackgroundResource(R.drawable.default_btn_selector);
        imgvBarCode.setBackgroundResource(R.drawable.default_btn_selector);
        imgvDiscovery.setBackgroundResource(R.drawable.default_btn_selector);*/
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.imgvDiscovery:
                selectDiscoveryOption();
                alWebHistoryStack.clear();
                webView.loadUrl("https://www.google.co.in/");
                break;
            case R.id.imgvCapture:
                UtillsG.showToast("Please wait...", getActivity(), true);

                selectCaptureOption();
                File imageFile = new File(UtillsG.getScreenShotOf(frameLayout, getActivity()));
                CropImage.activity(Uri.fromFile(imageFile))
                        .setAspectRatio(16, 9)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(getActivity());

                break;
            case R.id.imgvRecordScreen:
                selectShareOption();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    WebViewRecorderActivity.startForResult(getActivity(), Constants.RequestCode.WEBVIEW_RECORDING, webView.getUrl());
                } else
                {
                    UtillsG.showToast("This device does not support this Functionality.", getActivity(), true);
                }

                break;
            case R.id.imgvBarCode:
                selectTrimOption();

                //UtillsG.showToast("Functionality not discussed yet.", getActivity(), true);
                break;
            case R.id.imgvOptions:
                selectMoreOption();
                showPopUpMenu();
                break;
        }
    }

    private void showPopUpMenu()
    {
        PopupMenu popup = new PopupMenu(getActivity(), imgvOptions);
        popup.getMenuInflater()
                .inflate(R.menu.menu_discovery_options, popup.getMenu());

        MenuItem scanMode = popup.getMenu().findItem(R.id.ResearchScanMode);
        scanMode.setVisible(false);

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            public boolean onMenuItemClick(MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.NewDiscovery:
                        imgvDiscovery.performClick();
                        break;
                    case R.id.VoiceNoteSnapShot:
                        imgvCapture.performClick();
                        break;
                    case R.id.ScreenRecord:
                        imgvRecordScreen.performClick();
/*
                        DialogHelper.getInstance().showInformation(getActivity(), "You are a non-premium user so your 5 minutes audio recording has been finished.", new CallBackG<String>()
                        {
                            @Override
                            public void onCallBack(String output)
                            {

                            }
                        });
*/
                        break;
                    case R.id.ResearchScanMode:
                        imgvBarCode.performClick();
                        break;
                    case R.id.BackLinksForREC:
                        WebHistoryActivity.start(getActivity());
                        break;
                    case R.id.Help:
                        DialogHelper.getInstance().showInformation(getActivity(), "Lecture Verb - Discovery", "Discovery help content will be shown here. This text we are showing just for testing the functionality. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.", new CallBackG<String>()
                        {
                            @Override
                            public void onCallBack(String output)
                            {
                                // No implementation needed here for now.
                            }
                        });
                        break;
                }

                return true;
            }
        });

        popup.show();
    }


    public void onbackPressed()
    {
        if (webView.canGoBack())
        {
            webView.goBack();
        } else
        {
            if (getActivity() != null)
            {
                getActivity().finish();
            }
        }
    }

    TextView txtvTimer;
    ImageView imgv_Play;

    private void showPostDialog(Uri imageUri)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dailog_discovery_feature);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#7F000000")));

        initRecorder();

        final LinearLayout layRecording = (LinearLayout) dialog.findViewById(R.id.layRecording);
        final ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar);
        final CardView cardView = (CardView) dialog.findViewById(R.id.cardView);
        final FrameLayout frameLayout = (FrameLayout) dialog.findViewById(R.id.frameLayout);
        final EditText edLectureTitle = (EditText) dialog.findViewById(R.id.edLectureTitle);
        final ImageView imgvCapturedImage = (ImageView) dialog.findViewById(R.id.imgvCapturedImage);
        imgv_Play = (ImageView) dialog.findViewById(R.id.imgv_Play);
        imgvRecord = (ImageView) dialog.findViewById(R.id.imgvRecord);
        TextView txtvRecord = (TextView) dialog.findViewById(R.id.txtvRecord);
        TextView txtvCancel = (TextView) dialog.findViewById(R.id.txtvCancel);
        TextView txtvPost = (TextView) dialog.findViewById(R.id.txtvPost);

        txtvLectureCategory = (TextView) dialog.findViewById(R.id.txtvLectureCategory);

        txtvTimer = (TextView) dialog.findViewById(R.id.txtvTimer);

        edLectureTitle.setText(webView.getTitle());

        imgvCapturedImage.setImageURI(imageUri);
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                (dialog.findViewById(R.id.viewToHide)).setVisibility(View.GONE);
                UtillsG.hideKeyboard(getActivity(), (dialog.findViewById(R.id.viewToHide)));
            }
        }, 500);

        imgvRecord.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (mIsRecord)
                {
                    pauseRecording();

                    imgvRecord.setVisibility(View.GONE);
                    imgv_Play.setVisibility(View.VISIBLE);

                } else
                {
                    new Handler().post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            startRecording();
                            startTimer();

                            imgvRecord.setImageResource(
                                    isRecordingBoolean ? R.drawable.ic_red_dot
                                            : R.drawable.ic_green_dot);

                        }
                    });
                }
            }
        });

        txtvLectureCategory.setVisibility(View.VISIBLE);
        txtvLectureCategory.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                AddInterestCategoriesActivity.startForFragment(DiscoveryTabFragment.this, true, commaseparatedIds, commaSeparatedNames);
            }
        });
        txtvRecord.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                layRecording.setVisibility(View.VISIBLE);
            }
        });
        imgv_Play.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                resolvePlayRecord();
            }
        });
        txtvCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!mIsRecord)
                {
                    DialogHelper.getInstance().showWith2Action(getActivity(), "Exit", "Cancel", getActivity().getResources().getString(R.string.app_name), "The content is not yet saved, do you want to exit and discard the content?", new CallBackG<String>()
                    {
                        @Override
                        public void onCallBack(String output)
                        {
                            dialog.dismiss();
                        }
                    });
                } else
                {
                    UtillsG.showToast("Stop recording first..!", getActivity(), false);
                }
            }
        });
        txtvPost.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String title = edLectureTitle.getText().toString().trim();
                String url = webView.getUrl();
                if (title.isEmpty())
                {
                    UtillsG.showToast("Please Enter a title.", getActivity(), true);
                } else if (commaseparatedIds.trim().isEmpty())
                {
                    UtillsG.showToast("Please select your post's categories", getActivity(), true);
                } else
                {
                    if (!mIsRecord)
                    {
                        postasQuickLecture(title, url, audioFilePath, croppedImage, dialog, txtvTimer.getText().toString());
                    } else
                    {
                        UtillsG.showToast("Stop recording first..!", getActivity(), false);
                    }
                }
            }
        });

        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener()
        {
            @Override
            public void onDismiss(DialogInterface dialog)
            {
                resetEverything();
            }
        });
    }

    private void showVideoPostDialog(Bitmap imageBitmap, final String videoPath, final String currentUrl, final String currentTitle, final ArrayList<WebInnerStackModel> alWebStack, final String time)
    {
        final Uri thumbUri = UtillsG.saveBitmapToLocal(imageBitmap);


        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dailog_discovery_feature);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#7F000000")));

        initRecorder();

        final LinearLayout layRecording = (LinearLayout) dialog.findViewById(R.id.layRecording);
        final EditText edLectureTitle = (EditText) dialog.findViewById(R.id.edLectureTitle);
        final ImageView imgvCapturedImage = (ImageView) dialog.findViewById(R.id.imgvCapturedImage);
        imgv_Play = (ImageView) dialog.findViewById(R.id.imgv_Play);
        imgvRecord = (ImageView) dialog.findViewById(R.id.imgvRecord);
        TextView txtvRecord = (TextView) dialog.findViewById(R.id.txtvRecord);
        TextView txtvCancel = (TextView) dialog.findViewById(R.id.txtvCancel);
        TextView txtvPost = (TextView) dialog.findViewById(R.id.txtvPost);
        txtvLectureCategory = (TextView) dialog.findViewById(R.id.txtvLectureCategory);
        ImageView imgvPlay = (ImageView) dialog.findViewById(R.id.imgvPlay);
        imgvPlay.setVisibility(View.VISIBLE);
        txtvRecord.setVisibility(View.INVISIBLE);
        txtvTimer = (TextView) dialog.findViewById(R.id.txtvTimer);

        edLectureTitle.setText(webView.getUrl());

        imgvCapturedImage.setImageBitmap(imageBitmap);
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                (dialog.findViewById(R.id.viewToHide)).setVisibility(View.GONE);
                UtillsG.hideKeyboard(getActivity(), (dialog.findViewById(R.id.viewToHide)));
            }
        }, 500);

        imgvPlay.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                UtillsG.showFullSizeVideo(videoPath, getActivity());
            }
        });
        txtvLectureCategory.setVisibility(View.VISIBLE);
        txtvLectureCategory.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                AddInterestCategoriesActivity.startForFragment(DiscoveryTabFragment.this, true, commaseparatedIds, commaSeparatedNames);
            }
        });

        txtvCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DialogHelper.getInstance().showWith2Action(getActivity(), "Yes", "No", getResources().getString(R.string.app_name), "Are you sure you want to quit this?", new CallBackG<String>()
                {
                    @Override
                    public void onCallBack(String output)
                    {
                        dialog.dismiss();
                    }
                });
            }
        });
        txtvPost.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String title = edLectureTitle.getText().toString().trim();
                if (title.isEmpty())
                {
                    UtillsG.showToast("Please Enter a title.", getActivity(), true);
                } else if (commaseparatedIds.trim().isEmpty())
                {
                    UtillsG.showToast("Please select your post's categories", getActivity(), true);
                } else
                {
                    if (!mIsRecord)
                    {
                        postAsVideoLecture(currentTitle, currentUrl, videoPath, thumbUri, dialog, alWebStack, time);
                    } else
                    {
                        UtillsG.showToast("Stop recording first..!", getActivity(), false);
                    }
                }
            }
        });

        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener()
        {
            @Override
            public void onDismiss(DialogInterface dialog)
            {
                resetEverything();
            }
        });
    }

    private void resetEverything()
    {
        try
        {
            stopAndResetUI();
            audioFilePath = "";

            if (audioPlayer != null)
            {
                audioPlayer.stop();
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void postasQuickLecture(final String title, final String webUrl, String audioPath, final Uri croppedImage, final Dialog dialog, String time)
    {


        String history_stack = UtillsG.arrayListToJsonObject_innerStack(getActivity(), ""/*thumbnailApiResponse.body().getData().getThumbnail()*/, webView.getTitle(), webUrl, alWebHistoryStack);

        showLoading(UtillsG.getNotNullString(audioPath, "").isEmpty()
                ? "Uploading as Text Lecture..."
                : "Uploading as Audio Lecture...");
        Call<QuickLectureModel> quickLectureApiModelCall = getRetrofitInstance().uploadAudioLecture(
                UtillsG.getNotNullString(audioPath, "").isEmpty()
                        ? null
                        : getFilePart(new File(audioPath), "discovery_audio"),
                getOtherParams(UtillsG.getNotNullString(audioPath, "").isEmpty()
                        ? "discovery_text"
                        : "discovery_audio"),
                getOtherParams(new SharedPrefHelper(getActivity()).getUserId()),
                getOtherParams(title),
                getOtherParams(webUrl),
                getOtherParams("Public")
                , getOtherParams(time)
                , getOtherParams(new SharedPrefHelper(getActivity()).getUserName())
                , getOtherParams(history_stack)
                , getOtherParams("")
                , getOtherParams("0")
        );
        quickLectureApiModelCall.enqueue(new Callback<QuickLectureModel>()
        {
            @Override
            public void onResponse(Call<QuickLectureModel> call, final Response<QuickLectureModel> response)
            {
                if (response.body() != null)
                {
                    if (response.body().getStatus())
                    {
                        Call<ThumbnailResponseModel> lectureThumbnailApiModelCall = getRetrofitInstance().uploadLectureThumbnail(
                                getFilePart(new File(croppedImage.getPath()), "thumbnail"),
                                getOtherParams(response.body().getmQuickLectureData().getQuick_lecture_id()),
                                getUploadTypePart());
                        lectureThumbnailApiModelCall.enqueue(new Callback<ThumbnailResponseModel>()
                        {
                            @Override
                            public void onResponse(Call<ThumbnailResponseModel> thumbnailApiCall, Response<ThumbnailResponseModel> thumbnailApiResponse)
                            {

                                if (thumbnailApiResponse.body() != null)
                                {

                                    if (thumbnailApiResponse.body().getStatus())
                                    {
                                        new SharedPrefHelper(getActivity()).addUrlInHistoryWithStack(getActivity(), thumbnailApiResponse.body().getData().getThumbnail(), webView.getTitle(), webUrl, alWebHistoryStack);
                                        updateLectureEventInterest(response.body().getmQuickLectureData().getQuick_lecture_id(), dialog);

                                    } else
                                    {
                                        UtillsG.showToast(thumbnailApiResponse.body().getMessage(), getActivity(), true);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ThumbnailResponseModel> thumbnailApiCall, Throwable t)
                            {
                                hideLoading();
                                Log.e("Error", t.toString());
                                UtillsG.showToast("Error in creating quick lecture", getActivity(), true);
                            }
                        });
                    } else
                    {
                        hideLoading();
                    }
                }
            }

            @Override
            public void onFailure(Call<QuickLectureModel> call, Throwable t)
            {
                Log.e("Error", t.toString());
                hideLoading();
                UtillsG.showToast("Error in creating quick lecture", getActivity(), true);
            }
        });
    }


    public void updateLectureEventInterest(final String quickLectureId, final Dialog dialog)
    {
        mClicks.setLectureInterests((Activity) getActivity(), LVApplication.getRetrofit().create(EventLectureApi.class), quickLectureId,
                commaseparatedIds, "lecture", new CallBackG<Boolean>()
                {
                    @TargetApi(Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onCallBack(Boolean output)
                    {
                        commaseparatedIds = "";
                        commaSeparatedNames = "";

                        hideLoading();
//                        UtillsG.showToast(response.body().getMessage(), getActivity(), true);
                        UtillsG.showToast("Uploaded Successfully.", getActivity(), true);
                        if (new SharedPrefHelper(getActivity()).getUserType().equalsIgnoreCase(UserType.LECTURER))
                        {
                            UtillsG.showInterstitialAds(getActivity(), getResources().getString(R.string.userAppId), getResources().getString(R.string.nonStudentAdUnitDiscoveryupload));
                        } else
                        {
                            UtillsG.showInterstitialAds(getActivity(), getResources().getString(R.string.userAppId), getResources().getString(R.string.nonStudentAdUnitDiscoveryupload));

                        }
                        dialog.dismiss();

                    }
                });
    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void postAsVideoLecture(final String title, final String webUrl, String videoPath, final Uri croppedImage, final Dialog dialog,
                                    final ArrayList<WebInnerStackModel> alWebStack, String time)
    {

        String history_stack = UtillsG.arrayListToJsonObject_innerStack(getActivity(), "", title, webUrl, alWebStack);

        showLoading("Uploading as Discovery Lecture...");
        Call<QuickLectureModel> quickLectureApiModelCall = getRetrofitInstance().uploadAudioLecture(
                getFilePart(new File(videoPath), "discovery_video"),
                getOtherParams("discovery_video"),
                getOtherParams(new SharedPrefHelper(getActivity()).getUserId()),
                getOtherParams(title),
                getOtherParams(webUrl),
                getOtherParams("Public")
                , getOtherParams(time)
                , getOtherParams(new SharedPrefHelper(getActivity()).getUserName())
                , getOtherParams(history_stack)
                , getOtherParams("")
                , getOtherParams("0")
        );
        quickLectureApiModelCall.enqueue(new Callback<QuickLectureModel>()
        {
            @Override
            public void onResponse(Call<QuickLectureModel> call, final Response<QuickLectureModel> response)
            {


                if (response.body() != null)
                {

                    if (response.body().getStatus())
                    {

                        Call<ThumbnailResponseModel> lectureThumbnailApiModelCall = getRetrofitInstance().uploadLectureThumbnail(
                                getFilePart(new File(croppedImage.getPath()), "thumbnail"),
                                getOtherParams(response.body().getmQuickLectureData().getQuick_lecture_id()),
                                getUploadTypePart());
                        lectureThumbnailApiModelCall.enqueue(new Callback<ThumbnailResponseModel>()
                        {
                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                            @Override
                            public void onResponse(Call<ThumbnailResponseModel> thumbnailApiCall, Response<ThumbnailResponseModel> thumbnailApiResponse)
                            {
                                hideLoading();

                                if (thumbnailApiResponse.body() != null)
                                {

                                    if (thumbnailApiResponse.body().getStatus())
                                    {
                                        /*UtillsG.showToast(response.body().getMessage(), getActivity(), true);
                                        dialog.dismiss();*/

                                        new SharedPrefHelper(getActivity()).addUrlInHistoryWithStack(getActivity(), thumbnailApiResponse.body().getData().getThumbnail(), title, webUrl, alWebStack);
                                        updateLectureEventInterest(response.body().getmQuickLectureData().getQuick_lecture_id(), dialog);

                                    } else
                                    {
                                        UtillsG.showToast("Error in creating quick lecture", getActivity(), true);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ThumbnailResponseModel> thumbnailApiCall, Throwable t)
                            {
                                hideLoading();
                                Log.e("Error", t.toString());
                                UtillsG.showToast("Error in uploading thumbnail = " + t.toString(), getActivity(), true);
                            }
                        });
                    } else
                    {
                        hideLoading();
                    }
                } else
                {
                    hideLoading();
                }
            }

            @Override
            public void onFailure(Call<QuickLectureModel> call, Throwable t)
            {
                Log.e("Error", t.toString());
                UtillsG.showToast("Error in creating quick lecture = " + t.toString(), getActivity(), true);
                hideLoading();
            }
        });
    }

    private MultipartBody.Part getFilePart(final File file, String fileType)
    {
        ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(fileType, file.getName(), fileBody);
        return filePart;
    }

    private RequestBody getUploadTypePart()
    {
        return RequestBody.create(MediaType.parse("text/plain"), "upload");
    }


    private RequestBody getOtherParams(String value)
    {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }


    private QuickLectureApi getRetrofitInstance()
    {
        return LVApplication.getRetrofit().create(QuickLectureApi.class);
    }


    private String audioFilePath = "";
    private boolean mIsPlay;
    private boolean mIsRecord;
    private MP3Recorder mRecorder;
    AudioPlayer audioPlayer;

    private void startRecording()
    {
        if (mIsPlay)
        {
            mIsPlay = false;
        }

        if (audioFilePath.isEmpty())
        {
            audioFilePath = FileHelper.getInstance().createAudioFile("PK" + System.currentTimeMillis() + ".m4a").getAbsolutePath();
        }
        if (mRecorder == null)
        {
            mRecorder = new MP3Recorder(new File(audioFilePath));
            mRecorder.stop();
        } else
        {
            if (mRecorder.isPause())
            {
                mIsRecord = true;
                mRecorder.setPause(false);

                return;
            }
        }
        mRecorder.setErrorHandler(new Handler()
        {
            @Override
            public void handleMessage(Message msg)
            {
                super.handleMessage(msg);
                if (msg.what == MP3Recorder.ERROR_TYPE)
                {
                    resolveErrorDelete();
                    audioFilePath = "";
                    mRecorder = null;
                    return;
                }
            }
        });

        try
        {
            mRecorder.start();
            mIsRecord = true;
            isRecordingBoolean = true;
        } catch (IOException e)
        {
            e.printStackTrace();
            resolveErrorDelete();
        }
    }

    int hours, minutes, seconds;
    public boolean isRecordingBoolean;

    public void startTimer()
    {
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                //checking if recording is starting from start
                if (txtvTimer.getText().toString().trim().equalsIgnoreCase("record"))
                {
                    seconds = 0;
                    minutes = 0;
                    hours = 0;
                }
                seconds++;
                if (seconds == 60)
                {
                    seconds = 0;
                    minutes++;
                    if (minutes == 60)
                    {
                        minutes = 0;
                        hours++;
                    }
                }
                if (minutes == 0)
                {
                    txtvTimer.setText("00:" + (seconds < 10 ? ("0" + seconds)
                            : seconds));
                } else if (hours == 0 && minutes > 0)
                {
                    txtvTimer.setText((minutes < 10 ? ("0" + minutes)
                            : minutes) + ":" + (
                            seconds < 10 ? ("0" + seconds) : seconds));
                } else if (hours > 0)
                {
                    txtvTimer.setText(hours + ":" + (minutes < 10 ? ("0" + minutes)
                            : minutes));
                }
                try
                {
                    if (isRecordingBoolean)
                    {
                        if (minutes < 5)
                        {
                            startTimer();
                        } else
                        {
                            imgvRecord.performClick();
                        }
                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }, 1000);

    }

    public void pauseRecording()
    {
        if (mRecorder.isRecording())
        {
            mIsRecord = false;
            mRecorder.setPause(true);
            isRecordingBoolean = false;
        } else
        {
            startRecording();
        }
    }

    private void resolveErrorDelete()
    {
        stopAndResetUI();
        mRecorder = null;
        FileUtils.deleteFile(audioFilePath);
        audioFilePath = "";
    }

    protected void stopAndResetUI()
    {
        if (mIsRecord)
        {
            StopRecord();
        }
        if (mRecorder != null)
        {
            mRecorder.stop();
        }
        isRecordingBoolean = true;
        if (mIsPlay)
        {
            mIsPlay = false;
        }
    }

    private void StopRecord()
    {
        if (mRecorder != null && mRecorder.isRecording())
        {
            mRecorder.setPause(false);
            mRecorder.stop();
        }
        mIsRecord = false;
    }

    private void resolvePlayRecord()
    {
        if (!mIsRecord)
        {
            if (mIsPlay)
            {
                mIsPlay = false;
                pausePlayer();
                imgv_Play.setImageResource(android.R.drawable.ic_media_play);
                txtvTimer.setText("Stopped");
            } else
            {
                mIsPlay = true;
                if (audioPlayer != null)
                {
                    audioPlayer.playUrl(audioFilePath);
                    imgv_Play.setImageResource(R.drawable.ic_stop_music);
                    txtvTimer.setText("Playing");
                }
            }
        } else
        {
            UtillsG.showToast("Stop recording first..!", getActivity(), false);
        }
    }

    public void initRecorder()
    {
        audioPlayer = new AudioPlayer(getActivity(), new Handler()
        {
            @Override
            public void handleMessage(Message msg)
            {
                super.handleMessage(msg);
                switch (msg.what)
                {
                    case AudioPlayer.HANDLER_CUR_TIME:
                        break;
                    case AudioPlayer.HANDLER_COMPLETE:
                        mIsPlay = false;
                        imgv_Play.setImageResource(android.R.drawable.ic_media_play);
                        txtvTimer.setText("Finished");
                        break;
                    case AudioPlayer.HANDLER_PREPARED:
                        break;
                    case AudioPlayer.HANDLER_ERROR:
                        mIsPlay = false;
                        txtvTimer.setText("Playback Error");
                        break;
                }

            }
        });
    }

    private void pausePlayer()
    {
        if (audioPlayer != null)
        {
            audioPlayer.pause();
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        resetEverything();
    }

    @Override
    public void onProgressUpdate(int percentage)
    {

    }

    @Override
    public void onError()
    {

    }

    @Override
    public void onFinish()
    {

    }

    public void showLoading(String progressMessage)
    {
        if (progressDialog == null)
        {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle(getResources().getString(R.string.app_name));
            progressDialog.setMessage(progressMessage);
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
    }

    public void hideLoading()
    {
        if (progressDialog != null && progressDialog.isShowing())
        {
            progressDialog.dismiss();
        }
    }

    public void onActivityResultImageCropper(Uri imageUri)
    {
        croppedImage = imageUri;
        showPostDialog(imageUri);
    }

    public void onActivityResultWebViewRecorder(final String filePath, final String currentUrl, final String TITLE, ArrayList<WebInnerStackModel> alWebStack, String time)
    {
        final Bitmap bitmap2 = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Video.Thumbnails.MINI_KIND);
        showVideoPostDialog(bitmap2, filePath, currentUrl, TITLE, alWebStack, time);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == Constants.RequestCode.INTEREST_SELECTION)
            {
                commaseparatedIds = "";
                commaSeparatedNames = "";
                ArrayList<ChildCategoryModel> alCategories = (ArrayList<ChildCategoryModel>) data.getSerializableExtra(Constants.Extras.DATA);
                for (int i = 0; i < alCategories.size(); i++)
                {
                    commaseparatedIds = commaseparatedIds + (commaseparatedIds.isEmpty() ? ""
                            : ",") + alCategories.get(i).getId();
                    commaSeparatedNames = commaSeparatedNames + (
                            commaSeparatedNames.isEmpty() ? ""
                                    : ", ") + alCategories.get(i).getCategory();
                }

                if (txtvLectureCategory != null)
                {
                    txtvLectureCategory.setText(commaSeparatedNames);
                }
            }
        }
    }
}