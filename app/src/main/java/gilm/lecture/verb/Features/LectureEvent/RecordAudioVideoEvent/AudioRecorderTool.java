package gilm.lecture.verb.Features.LectureEvent.RecordAudioVideoEvent;

import android.media.MediaRecorder;
import android.util.Log;

import java.io.IOException;

public class AudioRecorderTool {

    MediaRecorder recorder = new MediaRecorder();
    public final String path;

    public AudioRecorderTool(String path) {
        this.path = path;
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(path);
        try {
            recorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() throws IOException {
        recorder.start();
    }

    public void stop() {
        try {
            Log.e("RECORDING STOP CALLED:", "--------------------------------");
            if (recorder != null) {
                recorder.stop();
                recorder.release();

                recorder = null;
            }
        } catch (Exception | Error e) {
            Log.e("ERRORRRR :::", e.toString());
        } finally {
            Log.e("ERRORRRR :::", "--------------");
        }
    }
}