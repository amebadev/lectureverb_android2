/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Connect.Contact;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 28 Jul 2017.
 */

public interface ContactListView extends Viewable<ContactListPresenter>
{
}
