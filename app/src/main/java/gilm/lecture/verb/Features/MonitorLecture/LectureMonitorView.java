package gilm.lecture.verb.Features.MonitorLecture;

import android.app.FragmentManager;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import okhttp3.RequestBody;

/**
 * created by PARAMBIR SINGH on 26/9/17.
 */

public interface LectureMonitorView extends Viewable<LectureMonitorPresenter>
{

    void videoRecordingStopped(String absolutePath);

    LectureDetailsModel getEventModel();

    void fileUploadingDone();

    FragmentManager getFragmentManager();
}
