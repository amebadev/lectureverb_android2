/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.MyProfile.Edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.theartofdev.edmodo.cropper.CropImage;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Home.Notification.Details.UserProfileModel;
import gilm.lecture.verb.Features.InterestsSelection.InterestSelectionApi;
import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.NaviagtionDrawer.ProfileUpdateBus;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.BitmapDecoderG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.ActivityProfileSettingBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileSettingActivity extends BaseActivity<ActivityProfileSettingBinding, ProfileSettingsPresenter> implements ProfileSettingsView
{
    String commaSeparatedIds = "", commaSeparatedNames = "";

    public static void start(Context context) {
        Intent starter = new Intent(context, ProfileSettingActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMyInterests();
    }

    @Override
    protected int setLayoutId() {
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        return R.layout.activity_profile_setting;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new ProfileSettingsPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        setupToolbar("Profile");
        getDataBinder().setBinder(getPresenter());

        UtillsG.setHeightWidthWithRatio_LinearLayout(((Activity) getActivityG()), getDataBinder().frameProfileImage, 4f, 6f);
    }

    private void getMyInterests() {
        showLoading("Please wait..");
        Call<UserProfileModel> call = LVApplication.getRetrofit().create(HomeApis.class).get_user_profile(getLocalData().getUserId(), getLocalData().getUserId());
        call.enqueue(new Callback<UserProfileModel>()
        {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response) {
                hideLoading();
                if (response.body().getStatus()) {
                    List<UserDataModel.UserInterests> userInterests = response.body().getData().getUserdata().getUser_interest();
                    String commaSeparatedCategories = "";
                    for (int i = 0; i < userInterests.size(); i++) {
                        commaSeparatedCategories = commaSeparatedCategories.isEmpty()
                                ? userInterests.get(i).getCategory_name()
                                : commaSeparatedCategories + ", " + userInterests.get(i).getCategory_name();
                    }
                    getLocalData().setMyInterests(commaSeparatedCategories);

                    getDataBinder().txtvInterests.setText(
                            getLocalData().getMyInterests().trim().isEmpty()
                                    ? "No interests selected yet."
                                    : getLocalData().getMyInterests());

                }
            }

            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t) {
                hideLoading();
                UtillsG.showToast("ERROR: " + t.getMessage(), getActivityG(), true);
            }
        });
    }

    @Override
    public View getViewToHideKeyBoard() {
        return (findViewById(R.id.title));
    }

    @Override
    public Context getActivityG() {
        return ProfileSettingActivity.this;
    }

    @Override
    public void close() {
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.menu_save, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save) {


            getPresenter().updateProfile();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void profileUpdated(UserDataModel output) {
        getLocalData().setUserData(output.getData());
        EventBus.getDefault().post(new ProfileUpdateBus(true));
        UtillsG.showToast(output.getMessage(), getActivityG(), true);
        UtillsG.hideKeyboard(getActivityG(), getCurrentFocus());
//        finish();
        if (!commaSeparatedIds.isEmpty()) {
            updateInterests();
        }
        else {
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    @Override
    public void showProfileUpdateProgress() {
        setProgressTitle("Updating profile");
        showLoading("please wait...");
    }

    @Override
    public void displayError(String message) {
        hideLoading();
        super.displayError(message);
    }

    @Override
    public void openPlacePicker(View view) {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(this), Constants.RequestCode.PLACE_PICKER);
        }
        catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        }
        catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == Constants.RequestCode.PLACE_PICKER) {
            if (resultCode != RESULT_OK) {
                return;
            }

            Place place = PlacePicker.getPlace(data, this);
            getPresenter().setSelectedLocation(place);
        }
        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            setProgressTitle("Updating Image");
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                getPresenter().setSelectedProfileImageUri(resultUri);
            }

        }
        else if (requestCode == Constants.RequestCode.INTEREST_SELECTION) {
            if (resultCode == RESULT_OK) {
                /*commaSeparatedIds = "";
                commaSeparatedNames = "";
                ArrayList<ChildCategoryModel> alCategories = (ArrayList<ChildCategoryModel>) data.getSerializableExtra(Constants.Extras.DATA);
                for (int i = 0; i < alCategories.size(); i++) {
                    commaSeparatedIds = commaSeparatedIds + (commaSeparatedIds.isEmpty() ? ""
                            : ",") + alCategories.get(i).getId();
                    commaSeparatedNames = commaSeparatedNames + (
                            commaSeparatedNames.isEmpty() ? ""
                                    : ", ") + alCategories.get(i).getCategory();
                }
                getPresenter().interests.set(commaSeparatedNames);*/

                getPresenter().interests.set(getLocalData().getMyInterests());

            }
        }
        else {
            if (resultCode != RESULT_OK) {
                return;
            }

            getPresenter().cropMeImage(BitmapDecoderG.onActivityResult(getActivityG(), requestCode, resultCode, data));

        }

    }

    public void updateInterests() {
        Call<BasicApiModel> saveMyInterests = getRetrofitInstance().save_users_interest(commaSeparatedIds, new SharedPrefHelper(getActivityG()).getUserId());
        saveMyInterests.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body().getStatus()) {
                    new SharedPrefHelper(getActivityG()).setInterestAlreadySet(getActivityG());
                    new SharedPrefHelper(getActivityG()).setMyInterests(commaSeparatedNames);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                UtillsG.showToast(t.getMessage(), getActivityG(), true);
            }
        });
    }

    private InterestSelectionApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(InterestSelectionApi.class);
    }


}
