package gilm.lecture.verb.Features.InterestsSelection;

import java.io.Serializable;
import java.util.List;

/**
 * created by PARAMBIR SINGH on 28/9/17.
 */

public class InterestsModel implements Serializable
{
    private String category = "";
    private String id = "";
    private String parent_id = "";

    List<ChildCategoryModel> child;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ChildCategoryModel> getChild() {
        return child;
    }

    public void setChild(List<ChildCategoryModel> child) {
        this.child = child;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }
}
