package gilm.lecture.verb.Features.LoginRegisteration.Registration;

import android.databinding.ObservableField;
import android.view.View;

/**
 * Created by G-Expo on 12 Jul 2017.
 */

public interface RegistrationPresenterBinder
{
    /**
     * register user on server with all enter details.
     * Run validation on all the fields,before sending request to server.
     */
    void registerClicked(View view);

    /**
     * Skip registeration and open Home screen as a guest user.
     *
     * @param view
     */
    void skipClicked(View view);

    /**
     * bind with password EditText
     *
     * @return entered password
     */
    ObservableField<String> getPassword();

    /**
     * bind with user name EditText
     *
     * @return entered user name
     */
    ObservableField<String> getUserName();

    /**
     * bind with email EditText
     *
     * @return entered email
     */
    ObservableField<String> getEmail();

    /**
     * bind with confirm password EditText
     *
     * @return entered confirm password
     */
    ObservableField<String> getConfirmPassword();
}
