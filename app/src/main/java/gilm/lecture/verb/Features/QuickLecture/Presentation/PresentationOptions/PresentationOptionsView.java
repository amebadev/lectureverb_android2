/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.QuickLecture.Presentation.PresentationOptions;

import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 20 Jul 2017.
 */

public interface PresentationOptionsView extends Viewable<PresentationOptionsPresenter>
{

    void PPT_into_Images_Done(List<SingleImageModel> imagesList);


}
