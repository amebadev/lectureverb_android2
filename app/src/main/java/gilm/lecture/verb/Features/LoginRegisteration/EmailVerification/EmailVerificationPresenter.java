/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.LoginRegisteration.EmailVerification;

import android.databinding.ObservableField;
import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.LoginRegisteration.ForgotPassword.ForgotPasswordBinder;
import gilm.lecture.verb.Features.LoginRegisteration.LoginRegisterApi;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;

/**
 * Created by G-Expo on 10 Aug 2017.
 */

public class EmailVerificationPresenter extends BasePresenter<EmailVerificationView> implements ForgotPasswordBinder, CallBackG<BasicApiModel>
{
    public final ObservableField<String> code =
            new ObservableField<>();

    public EmailVerificationPresenter()
    {
        code.set("");
    }

    @Override
    public void submitEmail(View view)
    {
        if (code.get().length() > 3) {
            getView().showLoading("Please wait...");
            createApiRequest(getRetrofitInstance().emailVerification(getView().getLocalData().getUserId(), getView().getLocalData().getEmail(), code.get()), this);
        }
        else {
            getView().displayError("Verification code not valid");
        }
    }

    protected LoginRegisterApi getRetrofitInstance()
    {
        return LVApplication.getRetrofit().create(LoginRegisterApi.class);
    }

    /**
     * reusing forgot password binder ,that's why name is get email.
     * but it contains verification code.
     *
     * @return - verification code.
     */
    @Override
    public ObservableField<String> getEmail()
    {
        return code;
    }

    /**
     * call back from API.
     *
     * @param output
     */
    @Override
    public void onCallBack(BasicApiModel output)
    {
        getView().hideLoading();

        if (output.getStatus()) {
            getView().gotoHome();
        }
        else {
            getView().displayError(output.getMessage());
        }
    }
}
