/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.QuickLecture.Presentation.PostPresentation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.VideoView;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EventLectureApi;
import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.Internal.Base.BaseActivityWithoutPresenter;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence.UserType;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureApi;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureModel;
import gilm.lecture.verb.Features.QuickLecture.ThumbnailResponseModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import gilm.lecture.verb.WebServices.Web;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostPresentationActivity extends BaseActivityWithoutPresenter implements View.OnClickListener, ProgressRequestBody.UploadCallbacks {
    String videoPath = "";
    EditText edTitle;
    RadioGroup mRadioGroup;
    FloatingActionButton mFabButton;
    VideoView videoView;
    ImageView imgvThumbnail, imgvFullScreen;
    ImageButton imageButton;
    File mArtWorkUrl;
    LectureEventClicks mClicks = new LectureEventClicks();
String group_id="";
    int currentPosition;

    public static void start(Context context, String videoPath, String discoveryUrl, String artWorkImage, String lectureCategory, String time,String group_id) {
        Intent starter = new Intent(context, PostPresentationActivity.class);
        starter.putExtra(Constants.Extras.DATA, videoPath);
        starter.putExtra("discoveryUrl", discoveryUrl);
        starter.putExtra("artWorkImage", artWorkImage);
        starter.putExtra("lectureCategory", lectureCategory);
        starter.putExtra("time", time);
        starter.putExtra("group_id", group_id);
        context.startActivity(starter);
        ((Activity) context).finish();
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_presentation);
        group_id = UtillsG.getNotNullString(getIntent().getStringExtra("group_id"),"0");

        videoPath = getIntent().getStringExtra(Constants.Extras.DATA);
        edTitle = (EditText) findViewById(R.id.edt_title);
        setupToolbar("Post Presentation");

        imageButton = (ImageButton) findViewById(R.id.imageButton);
        imgvThumbnail = (ImageView) findViewById(R.id.imgvThumbnail);
        imgvFullScreen = (ImageView) findViewById(R.id.imgvFullScreen);

        videoView = (VideoView) findViewById(R.id.videoView);
        mRadioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        mFabButton = (FloatingActionButton) findViewById(R.id.fab_postLec);
        mFabButton.setOnClickListener(this);
        imageButton.setOnClickListener(this);
        imgvFullScreen.setOnClickListener(this);

        Uri uri = Uri.parse(videoPath);
        if (uri != null) {
            videoView.setVideoURI(uri);
/*
            MediaController mediaController = new MediaController(this, false);
            videoView.setMediaController(mediaController);
            mediaController.setAnchorView(videoView);*/

            videoView.start();
            videoView.pause();

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    imageButton.setImageResource(R.mipmap.ic_play);
                }
            });

            Bitmap bitmap2 = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MICRO_KIND);
            imgvThumbnail.setImageBitmap(bitmap2);

            mArtWorkUrl = new File(Environment.getExternalStorageDirectory().toString() + File.separator + Web.LocalFolders.LectureVerbThumbs + System.currentTimeMillis() + ".png");

            FileOutputStream out = null;
            try {
                out = new FileOutputStream(mArtWorkUrl);
                bitmap2.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else {
            UtillsG.showToast("An error occured in generated video file.", getActivityG(), true);
        }
    }

    @Override
    public Context getActivityG() {
        return PostPresentationActivity.this;
    }


    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (viewById != null) {
            viewById.setText(title);
        } else {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void upload() {
        if (edTitle.getText().toString().trim().isEmpty()) {
            displayError("Please enter lecture title");
        } else {
            showLoading("Uploading ppt Lecture...");
            Call<QuickLectureModel> quickLectureApiModelCall = getRetrofitInstance().uploadAudioLecture(
                    getFilePart(new File(getIntent().getStringExtra(Constants.Extras.DATA)), "video"),
                    getOtherParams("video"),
                    getOtherParams(new SharedPrefHelper(getActivityG()).getUserId()),
                    getOtherParams(edTitle.getText().toString()),
                    getOtherParams(getIntent().getStringExtra("discoveryUrl")),
                    getOtherParams(
                            mRadioGroup.getCheckedRadioButtonId() == R.id.radioPublic
                                    ? "Public" : "Private")
                    , getOtherParams(getIntent().getStringExtra("time"))
                    , getOtherParams(new SharedPrefHelper(getActivityG()).getUserName())
                    , getOtherParams("")
                    , getOtherParams("")
                    , getOtherParams(group_id)
            );
            quickLectureApiModelCall.enqueue(new Callback<QuickLectureModel>() {
                @Override
                public void onResponse(Call<QuickLectureModel> call, final Response<QuickLectureModel> response) {


                    if (response.body() != null) {

                        if (response.body().getStatus()) {

                            Call<ThumbnailResponseModel> lectureThumbnailApiModelCall = getRetrofitInstance().uploadLectureThumbnail(
                                    getFilePart(new File(getIntent().getStringExtra("artWorkImage")), "thumbnail"),
                                    getOtherParams(response.body().getmQuickLectureData().getQuick_lecture_id()),
                                    getUploadTypePart());
                            lectureThumbnailApiModelCall.enqueue(new Callback<ThumbnailResponseModel>() {
                                @Override
                                public void onResponse(Call<ThumbnailResponseModel> thumbnailApiCall, Response<ThumbnailResponseModel> thumbnailApiResponse) {
                                    hideLoading();

                                    if (thumbnailApiResponse.body() != null) {

                                        if (thumbnailApiResponse.body().getStatus()) {
                                            UtillsG.deleteThisFile(getIntent().getStringExtra("artWorkImage"));
                                            UtillsG.deleteThisFile(getIntent().getStringExtra(Constants.Extras.DATA));
                                            UtillsG.deleteThisFile(mArtWorkUrl.getAbsolutePath());

                                            updateLectureEventInterest(response.body().getmQuickLectureData().getQuick_lecture_id());

                                        } else {
                                            displayError("Error in creating quick lecture");
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<ThumbnailResponseModel> thumbnailApiCall, Throwable t) {
                                    hideLoading();
                                    Log.e("Error", t.toString());
                                }
                            });
                        } else {
                            hideLoading();
                        }
                    } else {
                        hideLoading();
                    }
                }

                @Override
                public void onFailure(Call<QuickLectureModel> call, Throwable t) {
                    Log.e("Error", t.toString());
                    UtillsG.showToast("Error in creating quick lecture = " + t.toString(), getActivityG(), true);
                    hideLoading();
                }
            });
        }
    }

    public void updateLectureEventInterest(final String quickLectureId) {
        mClicks.setLectureInterests((Activity) getActivityG(), LVApplication.getRetrofit().create(EventLectureApi.class), quickLectureId,
                getIntent().getStringExtra("lectureCategory"), "lecture", new CallBackG<Boolean>() {
                    @Override
                    public void onCallBack(Boolean output) {
                        hideLoading();

                        UtillsG.showToast("Quick lecture created successfully", getActivityG(), true);
                        EventBus.getDefault().post(new HomeFragmentUpdateBus(true));
                        if (new SharedPrefHelper(getActivityG()).getUserType().equalsIgnoreCase(UserType.LECTURER)) {
                            UtillsG.showInterstitialAds(getActivityG(),getActivityG().getResources().getString(R.string.userAppId),getActivityG().getResources().getString(R.string.userAdUnitQuickPresent));
                        }
                        else {
                            UtillsG.showInterstitialAds(getActivityG(),getActivityG().getResources().getString(R.string.userAppId),getActivityG().getResources().getString(R.string.nonStudentAdUnitDiscoveryupload));

                        }

                        finish();

                    }
                });
    }


    private MultipartBody.Part getFilePart(final File file, String fileType) {
        ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(fileType, file.getName(), fileBody);
        return filePart;
    }

    private RequestBody getUploadTypePart() {
        return RequestBody.create(MediaType.parse("text/plain"), "upload");
    }


    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }


    private QuickLectureApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(QuickLectureApi.class);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_postLec:
                upload();
                break;
            case R.id.imageButton:
                if (videoView.isPlaying()) {
                    currentPosition = videoView.getCurrentPosition();
                    videoView.pause();
                    imageButton.setImageResource(R.mipmap.ic_play);
                } else {
                    videoView.start();
                    videoView.seekTo(currentPosition);
                    imageButton.setImageResource(R.drawable.ic_pause);
                }
                break;
            case R.id.imgvFullScreen:
                UtillsG.showFullSizeVideo(videoPath, getActivityG());
                /* Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoPath));
                intent.setDataAndType(Uri.parse(videoPath), "video/mp4");
                startActivity(intent);*/
                break;
        }
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
