/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.MyProfile.Edit;

import android.support.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.NaviagtionDrawer.ProfileUpdateBus;
import gilm.lecture.verb.WebServices.LVApplication;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 17 Aug 2017.
 */

public abstract class ImageUploader
{

    public ImageUploader()
    {
    }

    private ProfileApi getRetrofitInstance()
    {
        return LVApplication.getRetrofit().create(ProfileApi.class);
    }

    public void uploadProfileImage(File file)
    {
        Call<UserDataModel> basicApiModelCall = getRetrofitInstance().uploadProfileImage(getFilePart(file), getUserIdPart(), getUploadTypePart());
        basicApiModelCall.enqueue(new Callback<UserDataModel>()
        {
            @Override
            public void onResponse(Call<UserDataModel> call, Response<UserDataModel> response)
            {
                //change profile image url here..
                if (response.body() != null) {
                    if (response.body().getStatus()) {
                        onResponseG(response.body());
                    }
                    else {
                        onErrorG(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserDataModel> call, Throwable t)
            {
                onErrorG(t.getMessage());
            }
        });
    }
    public void uploadCoverProfileImage(File file)
    {
        Call<UserDataModel> basicApiModelCall = getRetrofitInstance().uploadCoverProfileImage(getFilePartCoverImage(file), getUserIdPart(), getUploadTypePart());
        basicApiModelCall.enqueue(new Callback<UserDataModel>()
        {
            @Override
            public void onResponse(Call<UserDataModel> call, Response<UserDataModel> response)
            {
                //change profile image url here..
                if (response.body() != null) {
                    if (response.body().getStatus()) {
                        onResponseG(response.body());
                    }
                    else {
                        onErrorG(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserDataModel> call, Throwable t)
            {
                onErrorG(t.getMessage());
            }
        });
    }

    private RequestBody getUserIdPart()
    {
        return RequestBody.create(MediaType.parse("text/plain"), getUserId());
    }

    private RequestBody getUploadTypePart()
    {
        return RequestBody.create(MediaType.parse("text/plain"), "upload");
    }

    @NonNull
    private MultipartBody.Part getFilePart(File file)
    {
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData("profile_image", file.getName(), requestBody);

    }
    @NonNull
    private MultipartBody.Part getFilePartCoverImage(File file)
    {
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData("cover_image", file.getName(), requestBody);

    }

    public void updateProfileImage(String imageUrl)
    {
        Call<UserDataModel> basicApiModelCall = getRetrofitInstance().updateProfileImage(imageUrl, getUserId());
        basicApiModelCall.enqueue(new Callback<UserDataModel>()
        {
            @Override
            public void onResponse(Call<UserDataModel> call, Response<UserDataModel> response)
            {
                //change profile image url here..
                if (response.body() != null) {
                    if (response.body().getStatus()) {
                        onResponseG(response.body());
                        EventBus.getDefault().post(new ProfileUpdateBus(true));
                    }
                    else {
                        onErrorG(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserDataModel> call, Throwable t)
            {
                onErrorG(t.getMessage());
            }
        });
    }

    protected abstract void onResponseG(UserDataModel userDataModel);

    protected abstract void onErrorG(String error);

    protected abstract String getUserId();
}
