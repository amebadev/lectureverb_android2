package gilm.lecture.verb.Features.Home.ListingByCategory;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import gilm.lecture.verb.Features.Home.Home.HomeAdapter_NoBinding;
import gilm.lecture.verb.Features.Home.RefreshQuickLectureBus;
import gilm.lecture.verb.Features.Internal.Base.BaseActivityNoBinding;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.EndlessRecyclerOnScrollListener;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;

public class ListingByCategoriesActivity extends BaseActivityNoBinding<ListingByCategoriesPresenter> implements ListingByCategoriesView
{
    protected ListingByCategoriesPresenter presenter;

    RecyclerView recyFavouriteList;
    private LinearLayoutManager linearLayoutManager;
    public int pageNumber = 1;
    private HomeAdapter_NoBinding homeAdapter;

    @Override
    public int getIncreasedPageNumber() {
        pageNumber++;
        return pageNumber;
    }

    ProgressBar progressBar;
    private TextView textvNoData;

    public static void start(Context context, String category_id, String category_name) {
        Intent starter = new Intent(context, ListingByCategoriesActivity.class);
        starter.putExtra("category_id", category_id);
        starter.putExtra("category_name", category_name);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_favourite_lectures_list;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new ListingByCategoriesPresenter());
        getPresenter().attachView(this);
    }


    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.toolbar_title);

        if (viewById != null) {
            viewById.setText(title);
        }
        else {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initViews() {
        setupToolbar(getIntent().getStringExtra("category_name"));
        textvNoData = (TextView) findViewById(R.id.textvNoData);
        recyFavouriteList = (RecyclerView) findViewById(R.id.recyFavouriteList);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        textvNoData = (TextView) findViewById(R.id.textvNoData);

        linearLayoutManager = new LinearLayoutManager(ListingByCategoriesActivity.this, LinearLayoutManager.VERTICAL, false);
        recyFavouriteList.setLayoutManager(linearLayoutManager);
        EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager)
        {
            @Override
            public void onLoadMore(int current_page) {
                getPresenter().getMoreRecords(new SharedPrefHelper(ListingByCategoriesActivity.this).getUserId(), getIntent().getStringExtra("category_id"), getIncreasedPageNumber());
            }

            @Override
            public void countChange(int currentVisibleCount) {

            }
        };
        recyFavouriteList.setOnScrollListener(endlessRecyclerOnScrollListener);

        homeAdapter = new HomeAdapter_NoBinding(null, getPresenter().alModel, ListingByCategoriesActivity.this);
        recyFavouriteList.setAdapter(homeAdapter);

        getPresenter().getMoreRecords(new SharedPrefHelper(ListingByCategoriesActivity.this).getUserId(), getIntent().getStringExtra("category_id"), pageNumber);
    }

    @Override
    public Context getActivityG() {
        return ListingByCategoriesActivity.this;
    }

    @Override
    public View getProgressBar() {
        return progressBar;
    }

    @Override
    public RecyclerView getRecyclerView() {
        return recyFavouriteList;
    }

    @Override
    public TextView getNoDataTextview() {
        return textvNoData;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void RefeshParticularQuickLecture(RefreshQuickLectureBus data) {
        if (getActivityG() != null && getPresenter().alModel != null && getPresenter().alModel.size() > 0) {

            for (int i = 0; i < getPresenter().alModel.size(); i++) {
                if (getPresenter().alModel.get(i).getQuick_lecture_id() != null && getPresenter().alModel.get(i).getQuick_lecture_id().equals(data.getQuickLectureId())) {

                    if (data.getForLike()) {
                        int previousCount = Integer.parseInt(getPresenter().alModel.get(i).getFavourites_count());

                        if (data.getIs_actionTrue()) {
                            previousCount++;
                            getPresenter().alModel.get(i).setFavourites_count("" + previousCount);
                        }
                        else {
                            previousCount--;
                            getPresenter().alModel.get(i).setFavourites_count("" + previousCount);
                        }
                        recyFavouriteList.getAdapter().notifyDataSetChanged();
                    }
                    else {
                        int previousCount = Integer.parseInt(getPresenter().alModel.get(i).getReposted_count());

                        if (data.getIs_actionTrue()) {
                            previousCount++;
                            getPresenter().alModel.get(i).setReposted_count("" + previousCount);
                        }
                        else {
                            previousCount--;
                            getPresenter().alModel.get(i).setReposted_count("" + previousCount);
                        }
                        recyFavouriteList.getAdapter().notifyDataSetChanged();
                    }
                    return;
                }


            }

        }
    }

}
