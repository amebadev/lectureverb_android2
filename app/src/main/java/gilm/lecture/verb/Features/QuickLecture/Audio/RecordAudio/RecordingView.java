package gilm.lecture.verb.Features.QuickLecture.Audio.RecordAudio;

import android.widget.TextView;

import com.shuyu.waveview.AudioWaveView;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;

/**
 * Created by G-Expo on 18 Jul 2017.
 */

public interface RecordingView extends Viewable<RecordingPresenter>
{
    /**
     * @return file name for new audio to record.
     */
    String getAudioFileName();

    /**
     * check if recording is in progress.
     */
    void recordingInProgress();

    /**
     * recording paused.
     */
    void recordingPaused();

    /**
     * recording stopped. reset everything.
     */
    void recordingStopped();

    /**
     * @return {@link AudioWaveView} using Data Binder.
     */
    AudioWaveView getAudioWave();

    void setToolbartitle(String title);

    void startTimer();

    String getLectureTitle();

    TextView getTimerView();

    LectureDetailsModel getEventModel();

    String getGroupId();

    int getMinutesRecorded();
}
