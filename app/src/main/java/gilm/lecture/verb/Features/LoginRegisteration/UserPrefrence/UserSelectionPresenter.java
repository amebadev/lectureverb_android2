package gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.UserGroups.GroupSelectionOptions.GroupSelectionOptionsActivity;
import gilm.lecture.verb.R;

/**
 * Created by G-Expo on 05 Jul 2017.
 */

public class UserSelectionPresenter extends BasePresenter<UserSelectionView>
{
    public String selectedUser = UserType.NON_STUDENT;

    isStudent isStudent;

    public UserSelectionPresenter() {
        isStudent = new isStudent();
        isStudent.setStudent(false);
    }

    public void onCheckChanged(final int i) {
        switch (i) {
            case R.id.radiobtn_student:
                selectedUser = UserType.STUDENT;
                isStudent.setStudent(true);
                getView().displayError("In Progress");

                getView().getLocalData().setUserType(selectedUser);
                break;
            case R.id.radiobtn_nonstudent:
                selectedUser = UserType.NON_STUDENT;
                isStudent.setStudent(false);
                getView().openLogin();

                getView().getLocalData().setUserType(selectedUser);
                break;
            case R.id.radiobtn_lecture:
                selectedUser = UserType.LECTURER;
                isStudent.setStudent(false);

                getView().getLocalData().setUserType(selectedUser);

                GroupSelectionOptionsActivity.start(getView().getActivityG());
                break;
        }
    }

    /**
     * skip button working goes here..
     */
    public void skipClicked() {
        getView().openLogin();
    }

    public class isStudent extends BaseObservable
    {
        boolean isStudent;
        String btnText;

        @Bindable
        public String getBtnText() {
            return btnText;
        }

        public void setBtnText(String btnText) {
            this.btnText = btnText;
        }

        @Bindable
        public boolean isStudent() {
            return isStudent;
        }

        public void setStudent(boolean student) {
            isStudent = student;
            setBtnText(student ? "next" : "skip");
            notifyChange();
        }
    }
}
