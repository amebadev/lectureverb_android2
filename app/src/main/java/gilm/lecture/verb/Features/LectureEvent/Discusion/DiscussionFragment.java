package gilm.lecture.verb.Features.LectureEvent.Discusion;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsAdapter;
import gilm.lecture.verb.Features.LectureEvent.Upcoming.UpcomingFragment;
import gilm.lecture.verb.Features.LectureEvent.Upcoming.UpcomingPresenter;
import gilm.lecture.verb.Features.LectureEvent.Upcoming.UpcomingView;
import gilm.lecture.verb.R;
import gilm.lecture.verb.databinding.FragmentRecyclerlistBinding;

/**
 * Created by harpreet on 9/15/17.
 */

public class DiscussionFragment extends BaseFragment<FragmentRecyclerlistBinding, UpcomingPresenter> implements UpcomingView, InfiniteAdapterG.OnLoadMoreListener
{

    public static UpcomingFragment newInstance()
    {
        return new UpcomingFragment();
    }

    public DiscussionFragment()
    {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutId()
    {
        return R.layout.fragment_recyclerlist;
    }

    @Override
    protected void onCreateFragmentG()
    {
        injectPresenter(new UpcomingPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews()
    {
        getDataBinder().setIsLoading(getPresenter().getIsLoadingViewModel());
        getPresenter().loadData(1);
    }

    @Override
    public void onLoadMore()
    {

    }

    @Override
    public void showData(ArrayList<LectureEventViewModel> list,int pageNo)
    {
        LectureEventsAdapter upcomingAdapter = new LectureEventsAdapter(list,getPresenter(),getActivityG());
        upcomingAdapter.setOnLoadMoreListener(this);
        getDataBinder().reviewsList.setAdapter(upcomingAdapter);
        upcomingAdapter.setShouldLoadMore(false);
    }
}
