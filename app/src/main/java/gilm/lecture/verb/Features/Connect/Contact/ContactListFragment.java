/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Connect.Contact;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.SearchView;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Discovery.VideoRecording.WebViewRecorderActivity;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.Features.LectureEvent.Details.LectureEventDetailsActivity;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.Followers.FollowersFollowingAdapter;
import gilm.lecture.verb.Features.MyProfile.Followers.ListOfUserModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.Player.PlayerActivity;
import gilm.lecture.verb.Features.Subscription.SubscriptionActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.AdsModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.FragmentAddContactBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactListFragment extends BaseFragment<FragmentAddContactBinding, ContactListPresenter> implements ContactListView, InfiniteAdapterG.OnLoadMoreListener {

    SearchView searchViewConnect;
    FollowersFollowingAdapter mFollowingFollwersAdapter;
    public ArrayList<UserDataModel.UserData> mfollowingFollowersList = new ArrayList<>();

    public static ContactListFragment newInstance() {
        return new ContactListFragment();
    }

    public ContactListFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_add_contact;
    }

    @Override
    protected void onCreateFragmentG() {
        injectPresenter(new ContactListPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {

        getConnectAds();


        getDataBinder().setIsLoading(getPresenter().getIsLoadingViewModel());
        getPresenter().loadContactUsers();
        getDataBinder().tvInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogHelper.inviteConnectOptions(getActivityG());
            }
        });

        getDataBinder().edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString() != null && s.toString().trim().isEmpty()) {
                    getPeopleYouMayKnow();
                } else {
                    mFollowingFollwersAdapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        getDataBinder().imgvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtillsG.hideKeyboard(getActivityG(), getDataBinder().edSearch);
                getSearchedConnectUsers(getDataBinder().edSearch.getText().toString().trim());
            }
        });

        mFollowingFollwersAdapter = new FollowersFollowingAdapter(getActivity(), mfollowingFollowersList, false, true, ContactListFragment.this);
        getDataBinder().reviewsList.setAdapter(mFollowingFollwersAdapter);

        getDataBinder().getIsLoading().setLoading(true);
        UtillsG.showLoading("Loading..", getActivity());
        getPeopleYouMayKnow();
    }

    private void getConnectAds() {
        Call<AdsModel> call = LVApplication.getRetrofit().create(HomeApis.class).get_ads("connect_screen_ads");
        call.enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(Call<AdsModel> call, final Response<AdsModel> response) {
                if (response.body().getStatus()) {
                    getDataBinder().llAd.setVisibility(View.VISIBLE);
                    UtillsG.setHeightWidthWithRatio_FrameLayout((getActivity()), getDataBinder().imgvConnectAd, 5f, 16f);
                    if (!response.body().getList().isEmpty()) {
                        ImageLoader.setImageBig(getDataBinder().imgvConnectAd, response.body().getList().get(0).getImage_path());

                        getDataBinder().txtvAdTitle.setText(response.body().getList().get(0).getTitle());

                        if (!UtillsG.getNotNullString(response.body().getList().get(0).getImage_path(), "").isEmpty()) {
                            getDataBinder().imgvConnectAd.setVisibility(View.VISIBLE);
                            getDataBinder().txtvAdTitle.setVisibility(View.GONE);
                        } else {
                            getDataBinder().imgvConnectAd.setVisibility(View.INVISIBLE);
                            getDataBinder().txtvAdTitle.setVisibility(View.VISIBLE);
                        }

                        getDataBinder().llAd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                UtillsG.ads_counter(getActivityG(), getLocalData().getUserId(), response.body().getList().get(0).getId() + "");

                                switch (response.body().getList().get(0).getAd_type()) {
                                    // 1= subscription, 2= event, 3 = quick_lecture, 4 = custom link
                                    case "1":
                                        SubscriptionActivity.start(getActivityG());

                                        break;
                                    case "2":
                                        LectureEventDetailsActivity.startActivityWithApiCall(getActivityG(), response.body().getList().get(0).getData(), getDataBinder().imgvConnectAd);
                                        break;
                                    case "3":
                                        PlayerActivity.startWithId(getActivityG(), response.body().getList().get(0).getData(), false);
                                        break;
                                    case "4":
                                        if (response.body().getList().get(0).getData().toLowerCase().contains("facebook") || response.body().getList().get(0).getData().toLowerCase().contains("fb")) {
                                            try {
                                                String url = response.body().getList().get(0).getData();
                                                Intent i = new Intent(Intent.ACTION_VIEW);
                                                i.setData(Uri.parse(url));
                                                startActivity(i);
                                            } catch (Exception e) {
                                                WebViewRecorderActivity.startForBrowsingOnly(getActivityG(), response.body().getList().get(0).getData());
                                            }
                                        } else {
                                            WebViewRecorderActivity.startForBrowsingOnly(getActivityG(), response.body().getList().get(0).getData());
                                        }
                                        break;
                                }
                            }
                        });
                    }
                } else {
                    getDataBinder().llAd.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<AdsModel> call, Throwable t) {
                UtillsG.showToast(t.getMessage(), getActivityG(), true);
            }
        });
    }


    public void getPeopleYouMayKnow() {

        Call<ListOfUserModel> getFollowersList = getRetrofitInstance().people_you_may_know(new SharedPrefHelper(getActivity()).getUserId());
        getFollowersList.enqueue(new Callback<ListOfUserModel>() {
            @Override
            public void onResponse(Call<ListOfUserModel> call, Response<ListOfUserModel> response) {
                getDataBinder().getIsLoading().setLoading(false);
                UtillsG.hideLoading();
                if (response.body().getStatus()) {
                    if (!(response.body().getUsersList()).isEmpty()) {
                        mfollowingFollowersList.clear();
                        mfollowingFollowersList.addAll(response.body().getUsersList());
                        mFollowingFollwersAdapter.notifyDataSetChanged();
                        getDataBinder().reviewsList.setVisibility(View.VISIBLE);
                        getDataBinder().txtvNoData.setVisibility(View.GONE);
                    } else {
                        getDataBinder().reviewsList.setVisibility(View.GONE);
                        getDataBinder().txtvNoData.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ListOfUserModel> call, Throwable t) {
                getDataBinder().getIsLoading().setLoading(false);
                UtillsG.hideLoading();
                UtillsG.showToast(t.getMessage(), getActivity(), true);
            }
        });
    }

    public void getSearchedConnectUsers(String searchedKeyword) {
        UtillsG.showLoading("Searching...", getActivityG());
        Call<ListOfUserModel> getFollowersList = getRetrofitInstance().search_user(new SharedPrefHelper(getActivity()).getUserId(), "1", searchedKeyword);
        getFollowersList.enqueue(new Callback<ListOfUserModel>() {
            @Override
            public void onResponse(Call<ListOfUserModel> call, Response<ListOfUserModel> response) {
                UtillsG.hideLoading();
                getDataBinder().getIsLoading().setLoading(false);
                UtillsG.hideLoading();
                if (response.body().getStatus()) {
                    if (!(response.body().getUsersList()).isEmpty()) {
                        mfollowingFollowersList.clear();
                        mfollowingFollowersList.addAll(response.body().getUsersList());
                        mFollowingFollwersAdapter.notifyDataSetChanged();
                        getDataBinder().reviewsList.setVisibility(View.VISIBLE);
                        getDataBinder().txtvNoData.setVisibility(View.GONE);
                    } else {
                        getDataBinder().reviewsList.setVisibility(View.GONE);
                        getDataBinder().txtvNoData.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ListOfUserModel> call, Throwable t) {
                UtillsG.hideLoading();
                getDataBinder().getIsLoading().setLoading(false);
                UtillsG.hideLoading();
                UtillsG.showToast(t.getMessage(), getActivity(), true);
            }
        });


    }

    public ArrayList<UserDataModel.UserData> getConnectsList() {
        return mfollowingFollowersList;
    }

    @Override
    public void onLoadMore() {

    }

    private ProfileApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(ProfileApi.class);
    }

}
