package gilm.lecture.verb.Features.QuickLecture.Audio.PostAudio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.theartofdev.edmodo.cropper.CropImage;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import gilm.lecture.verb.Features.Discovery.DiscoveryActivity;
import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.InterestsSelection.AddInterests.AddInterestCategoriesActivity;
import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.Features.Internal.Base.BaseActivityNoBinding;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.BitmapDecoderG;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.UtillsG;

public class PostAudioActivity extends BaseActivityNoBinding<PostAudioPresenter> implements PostAudioView, View.OnClickListener, CallBackG<String> {
    TextView txtvAddDiscoveryUrl, txtvArtWork, txtvLectureCategory;
    String audioFilePath = "";
    ProgressBar progressBar;
    String commaSeparatedIds = "", commaSeparatedNames = "";
    LectureDetailsModel lectureDetailsModel;
    LinearLayout llThumbnail;

String group_id="";
    public static void start(Context context, String filePath, String title, String time,String group_id) {
        Intent starter = new Intent(context, PostAudioActivity.class);
        starter.putExtra(Constants.Extras.DATA, filePath);
        starter.putExtra("title", title);
        starter.putExtra("group_id", group_id);

        starter.putExtra("time", time);
        context.startActivity(starter);
        ((Activity) context).finish();
    }

    public static void startForEvent(Context context, String filePath, String title, String time, LectureDetailsModel lectureDetailsModel,String group_id) {
        Intent starter = new Intent(context, PostAudioActivity.class);
        starter.putExtra(Constants.Extras.DATA, filePath);
        starter.putExtra("title", title);
        starter.putExtra("time", time);
        starter.putExtra("group_id", group_id);
        starter.putExtra("lectureDetailsModel", lectureDetailsModel);
        context.startActivity(starter);
        ((Activity) context).finish();
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_post_audio;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new PostAudioPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        setupToolbar("Post");

        lectureDetailsModel = (LectureDetailsModel) getIntent().getSerializableExtra("lectureDetailsModel");
        group_id = UtillsG.getNotNullString(getIntent().getStringExtra("group_id"),"0");

        llThumbnail = (LinearLayout) findViewById(R.id.llThumbnail);
        txtvLectureCategory = (TextView) findViewById(R.id.txtvLectureCategory);
        audioFilePath = getIntent().getStringExtra(Constants.Extras.DATA);
        txtvAddDiscoveryUrl = (TextView) findViewById(R.id.txtvAddDiscoveryUrl);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        txtvArtWork = (TextView) findViewById(R.id.txtvArtWork);
        txtvAddDiscoveryUrl.setOnClickListener(this);
        txtvArtWork.setOnClickListener(this);

        txtvLectureCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddInterestCategoriesActivity.start(getActivityG(), true, commaSeparatedIds, commaSeparatedNames);
            }
        });

        if (lectureDetailsModel != null) {
            ((EditText) findViewById(R.id.edTitle)).setText(lectureDetailsModel.getDetails());

            commaSeparatedIds = "";
            commaSeparatedNames = "";
            ArrayList<ChildCategoryModel> alCategories = (ArrayList<ChildCategoryModel>) lectureDetailsModel.getEvent_interest();
            for (int i = 0; i < alCategories.size(); i++) {
                commaSeparatedIds = commaSeparatedIds + (commaSeparatedIds.isEmpty() ? ""
                        : ",") + alCategories.get(i).getCategory_id();
                commaSeparatedNames = commaSeparatedNames + (
                        commaSeparatedNames.isEmpty() ? ""
                                : ", ") + alCategories.get(i).getCategory_name();
            }
            getPresenter().setSelectedLectureCategories(commaSeparatedIds);
            txtvLectureCategory.setText(commaSeparatedNames);

            llThumbnail.setVisibility(View.GONE);
        } else {
            ((EditText) findViewById(R.id.edTitle)).setText(getIntent().getStringExtra("title"));
            llThumbnail.setVisibility(View.VISIBLE);
        }


        progressBar.setVisibility(View.GONE);
    }

    @Override
    public LectureDetailsModel getEventModel() {
        return lectureDetailsModel;
    }

    @Override
    public String get_Group_id() {
        return group_id;
    }

    @Override
    public String getLectureTitle() {
        return ((EditText) findViewById(R.id.edTitle)).getText().toString().trim();
    }

    @Override
    public RadioGroup getRadioGroup() {
        return (RadioGroup) findViewById(R.id.radioGroup);
    }

    @Override
    public Context getActivityG() {
        return PostAudioActivity.this;
    }

    public void upLoad(View view) {


        getPresenter().upload();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtvAddDiscoveryUrl:
                DiscoveryActivity.start(this);
                break;
            case R.id.txtvArtWork:
                BitmapDecoderG.selectImage(getActivityG(), null);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.RequestCode.DISCOVERY) {
                txtvAddDiscoveryUrl.setText(data.getStringExtra(Constants.Extras.DATA));
                getPresenter().setDiscoveryUrl(data.getStringExtra(Constants.Extras.DATA));
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    File file = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "audioLectureThumbnail.png");
                    FileOutputStream out = null;
                    try {
                        if (bitmap != null) {
                            out = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (out != null) {
                                out.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    txtvArtWork.setText(file.getAbsolutePath());
                    getPresenter().setArtWorkImage(Uri.fromFile(file));
                }
            } else if (requestCode == Constants.RequestCode.INTEREST_SELECTION) {

                commaSeparatedIds = "";
                commaSeparatedNames = "";
                ArrayList<ChildCategoryModel> alCategories = (ArrayList<ChildCategoryModel>) data.getSerializableExtra(Constants.Extras.DATA);
                for (int i = 0; i < alCategories.size(); i++) {
                    commaSeparatedIds = commaSeparatedIds + (commaSeparatedIds.isEmpty() ? ""
                            : ",") + alCategories.get(i).getId();
                    commaSeparatedNames = commaSeparatedNames + (
                            commaSeparatedNames.isEmpty() ? ""
                                    : ", ") + alCategories.get(i).getCategory();
                }
                getPresenter().setSelectedLectureCategories(commaSeparatedIds);
                txtvLectureCategory.setText(commaSeparatedNames);
            } else {
                getPresenter().cropMeImage(BitmapDecoderG.onActivityResult(getActivityG(), requestCode, resultCode, data));
            }
        }
    }

    public void onClickDiscoveryHelp(View view) {
        DialogHelper.getInstance().showInformation(this, "Select a Discovery Url for your quick lecture.", this);
    }

    public void onClickArtWorkHelp(View view) {
        DialogHelper.getInstance().showInformation(this, "Select an ArtWork for your quick lecture. Art work will be cropped in Ratio 16:9.", this);
    }

    @Override
    public void onCallBack(String output) {
        // FIXME: 30/8/17 informative dialog call back
    }

    @Override
    public File getAudioFile() {
//        // TODO: 30/8/17 audio file here from previous activity.
        return new File(audioFilePath);
    }

    @Override
    public void setprogress(int percentage) {

    }

    @Override
    public void uploadDone() {
        EventBus.getDefault().post(new HomeFragmentUpdateBus(true));
        finish();
    }

    @Override
    public String getTimeDuration() {
        return getIntent().getStringExtra("time") != null ? getIntent().getStringExtra("time") : "";
    }
}
