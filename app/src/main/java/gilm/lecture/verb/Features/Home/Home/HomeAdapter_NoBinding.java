package gilm.lecture.verb.Features.Home.Home;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import gilm.lecture.verb.Features.Discovery.VideoRecording.WebViewRecorderActivity;
import gilm.lecture.verb.Features.Favorites.FavouritesLecturesList.FavouriteLecturesListActivity;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Home.ListingByCategory.ListingByCategoriesActivity;
import gilm.lecture.verb.Features.LectureEvent.AttendingLecture;
import gilm.lecture.verb.Features.LectureEvent.Details.LectureEventDetailsActivity;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments.TextLectureCommentsActivity;
import gilm.lecture.verb.Features.Player.CommentsReplyDailog;
import gilm.lecture.verb.Features.Player.PlayerActivity;
import gilm.lecture.verb.Features.Player.PublicAudioPlayer;
import gilm.lecture.verb.Features.Search.SearchLectureFragment;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DateHelper;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.Web;
import gilm.lecture.verb.Widget.TextViewWithImages;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ParamBir Singh on 06 Jul 2017.
 *
 * @see HomeAdapter_NoBinding is used in 2 classes one is {@link HomeFragment_NoBinding} and another one is {@link FavouriteLecturesListActivity}
 */

public class HomeAdapter_NoBinding extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private LayoutInflater inflater;
    CommentsReplyDailog mCommentsReply;

    private List<Lecture_And_Event_Model> alLectureModels;
    LectureEventClicks mClicks = new LectureEventClicks();
    Context context;
    Fragment mRootFragment = null;

    boolean blockReposts;

    public HomeAdapter_NoBinding(Fragment mRootFragment, List<Lecture_And_Event_Model> dList, Context context)
    {
        this.alLectureModels = dList;
        this.context = context;
        inflater = LayoutInflater.from(context);
        mCommentsReply = new CommentsReplyDailog((Activity) context);
        this.mRootFragment = mRootFragment;
    }

    public HomeAdapter_NoBinding(Fragment mRootFragment, List<Lecture_And_Event_Model> dList, Context context, boolean blockReposts)
    {
        this.alLectureModels = dList;
        this.context = context;
        this.blockReposts = blockReposts;
        inflater = LayoutInflater.from(context);
        mCommentsReply = new CommentsReplyDailog((Activity) context);
        this.mRootFragment = mRootFragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        if (viewType == 2)
        {
            return new EventViewHolder(inflater.inflate(R.layout.inflater_lecture_event_no_binding, parent, false));
        } else
        {
            return new LectureViewHolder(inflater.inflate(R.layout.inflator_home_two_no_binding, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position)
    {
        try
        {
            if (holder instanceof LectureViewHolder)
            {
                if (blockReposts)
                {
                    ((LectureViewHolder) holder).llRepost.setVisibility(View.GONE);
                } else
                {
                    ((LectureViewHolder) holder).llRepost.setVisibility(View.VISIBLE);
                }
                if (alLectureModels.get(position).getEvent_interest() != null && !alLectureModels.get(position).getEvent_interest().isEmpty())
                {
                    CategoriesInterestsAdapter adapter = new CategoriesInterestsAdapter(alLectureModels.get(position).getEvent_interest(), context, context instanceof ListingByCategoriesActivity == false);
                    ((LectureViewHolder) holder).recyLectureInterests.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                    ((LectureViewHolder) holder).recyLectureInterests.setAdapter(adapter);
                    ((LectureViewHolder) holder).recyLectureInterests.setVisibility(View.VISIBLE);
                    ((LectureViewHolder) holder).view3.setVisibility(View.VISIBLE);
                    adapter.setShouldLoadMore(false);
                } else
                {
                    ((LectureViewHolder) holder).recyLectureInterests.setVisibility(View.GONE);
                    ((LectureViewHolder) holder).view3.setVisibility(View.GONE);
                }

                if (!UtillsG.getNotNullString(alLectureModels.get(position).getThumbnail(), "").isEmpty())
                {
                    ImageLoader.setImageBig(((LectureViewHolder) holder).imgvThumbnail, alLectureModels.get(position).getThumbnail());
                } else
                {
                    ((LectureViewHolder) holder).imgvThumbnail.setImageResource(R.drawable.white_square);
                }
                if (alLectureModels.get(position).getOwnerUserdata() != null)
                {
                    if (!UtillsG.getNotNullString(alLectureModels.get(position).getOwnerUserdata().getProfile_pic(), "").isEmpty())
                    {
                        ImageLoader.setImageBig(((LectureViewHolder) holder).imgvUserImage, alLectureModels.get(position).getOwnerUserdata().getProfile_pic());
                    } else
                    {
                        ImageLoader.setImageBig(((LectureViewHolder) holder).imgvUserImage, "https://images.designtrends.com/wp-content/uploads/2015/11/30162730/White-and-Grey-Plain-Background.jpg");
                    }
                } else
                {
                    ImageLoader.setImageBig(((LectureViewHolder) holder).imgvUserImage, alLectureModels.get(position).getUserdata().getProfile_pic());
                }

                ((LectureViewHolder) holder).txtvFavouriteCount.setText(alLectureModels.get(position).getFavourites_count());
                ((LectureViewHolder) holder).txtvCommentsCount.setText(alLectureModels.get(position).getComments_count());
                ((LectureViewHolder) holder).txtvRepostedCount.setText(alLectureModels.get(position).getReposted_count());
                ((LectureViewHolder) holder).txtvViewsCount.setText(alLectureModels.get(position).getLecture_view());

                ((LectureViewHolder) holder).layPlay.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        EventBus.getDefault().post("");
                        if (UtillsG.isAudioVideoMediaPost(alLectureModels.get(position)))
                        {
                            setLectureAsPlayed(position);

                            PublicAudioPlayer.setReleaseAudioPlayer();

                            PlayerActivity.startForResult(context, alLectureModels.get(position), false);
                        } else if (UtillsG.isTextPost(alLectureModels.get(position)))
                        {
//                            UtillsG.showToast("It is a text Lecture.", context, true);
                        }
                    }
                });
                ((LectureViewHolder) holder).view.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        ((LectureViewHolder) holder).layPlay.performClick();
                    }
                });

                ((LectureViewHolder) holder).imgvUserImage.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {

                        if (alLectureModels.get(position).getOwnerUserdata() != null)
                        {
                            if (!UtillsG.getNotNullString(alLectureModels.get(position).getOwnerUserdata().getProfile_pic(), "").isEmpty())
                            {
                                UtillsG.showFullImage(Web.Path.BASE_URL + alLectureModels.get(position).getOwnerUserdata().getProfile_pic(), context, false);

                            } else
                            {
                                UtillsG.showFullImage("https://images.designtrends.com/wp-content/uploads/2015/11/30162730/White-and-Grey-Plain-Background.jpg", context, false);
                            }
                        } else
                        {

                            UtillsG.showFullImage(Web.Path.BASE_URL + alLectureModels.get(position).getUserdata().getProfile_pic(), context, false);

                        }


                    }
                });
                ((LectureViewHolder) holder).ll_repostDetails.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                       /* if (alLectureModels.get(position).getIs_reposted()) {
                            UtillsG.showFullImage(Web.Path.BASE_URL + new SharedPrefHelper(context).getProfilePic(), context, false);
                        } else {
                            UtillsG.showFullImage(Web.Path.BASE_URL + alLectureModels.get(position).getUserdata().getProfile_pic(), context, false);
                        }*/
                        ActivityOtherUserDetails.start(context, alLectureModels.get(position).getUserdata().getUser_id(), "User Profile");
                    }
                });

                if (alLectureModels.get(position).getIs_favourite().equalsIgnoreCase("1"))
                {
                    ((LectureViewHolder) holder).imgvLike.setImageResource(R.drawable.ic_liked);
                    ((LectureViewHolder) holder).imgvLike.setColorFilter(ContextCompat.getColor(context, R.color.redTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
                } else
                {
                    ((LectureViewHolder) holder).imgvLike.setImageResource(R.mipmap.ic_fav_white_border);
                    ((LectureViewHolder) holder).imgvLike.setColorFilter(ContextCompat.getColor(context, R.color.dialogBackgroundColor), android.graphics.PorterDuff.Mode.MULTIPLY);
                }

                if (alLectureModels.get(position).getIs_reposted())
                {
                    ((LectureViewHolder) holder).imgv_repost.setColorFilter(ContextCompat.getColor(context, R.color.greenTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
                } else
                {
                    ((LectureViewHolder) holder).imgv_repost.setColorFilter(ContextCompat.getColor(context, R.color.dialogBackgroundColor), android.graphics.PorterDuff.Mode.MULTIPLY);
                }

                ((LectureViewHolder) holder).layLike.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        likeUnlikeLecture(position, ((LectureViewHolder) holder));
                    }
                });

                ((LectureViewHolder) holder).imgvOptions.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        showMoreOptions(v, alLectureModels.get(position), ((LectureViewHolder) holder), position);
                    }
                });

                ((LectureViewHolder) holder).ll_comments.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        EventBus.getDefault().post("");
                        if (UtillsG.isAudioVideoMediaPost(alLectureModels.get(position)))
                        {
                            setLectureAsPlayed(position);
                            PlayerActivity.startForResult(context, alLectureModels.get(position), true);
                        } else if (UtillsG.isTextPost(alLectureModels.get(position)))
                        {

                            TextLectureCommentsActivity.start(context, alLectureModels.get(position));
                        }
                    }
                });


                ((LectureViewHolder) holder).ll_connect.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        ActivityOtherUserDetails.start(context, alLectureModels.get(position).getUserdata().getUser_id(), "User Profile");
                    }
                });


                ((LectureViewHolder) holder).llRepost.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        repostLecture(position, (LectureViewHolder) holder);
                    }
                });

                if (alLectureModels.get(position).getQuick_lecture_type().equalsIgnoreCase("text")
                        || alLectureModels.get(position).getQuick_lecture_type().equalsIgnoreCase("discovery_text"))
                {
                    ((LectureViewHolder) holder).layPlay.setVisibility(View.GONE);
//                    ((LectureViewHolder) holder).viewTop.setVisibility(View.VISIBLE);
                    ((LectureViewHolder) holder).actxtvLectureTitle.setMaxLines(2);
                    ((LectureViewHolder) holder).actxtvLectureTitle.setVisibility(View.GONE);
                    ((LectureViewHolder) holder).txtvDuration.setVisibility(View.GONE);
                    ((LectureViewHolder) holder).txtvTitleBig.setVisibility(View.VISIBLE);
                    ((LectureViewHolder) holder).txtvTitleBig.setText(alLectureModels.get(position).getQuick_lecture_text());
                    ((LectureViewHolder) holder).actxtvLectureTitle.setText(alLectureModels.get(position).getQuick_lecture_text());
                    UtillsG.setHeightWidtWRAP_LinearLayout(((Activity) context), ((LectureViewHolder) holder).frameLayout, context);
                    ((LectureViewHolder) holder).txtvDuration.setText("");
                } else
                {
//                    ((LectureViewHolder) holder).viewTop.setVisibility(View.INVISIBLE);
                    ((LectureViewHolder) holder).txtvDuration.setText(UtillsG.getNotNullString(alLectureModels.get(position).getQuick_lecture_time_duration(), ""));
                    ((LectureViewHolder) holder).layPlay.setVisibility(View.VISIBLE);
                    ((LectureViewHolder) holder).actxtvLectureTitle.setMaxLines(2);
                    ((LectureViewHolder) holder).actxtvLectureTitle.setVisibility(View.VISIBLE);
                    ((LectureViewHolder) holder).txtvDuration.setVisibility(View.VISIBLE);
                    ((LectureViewHolder) holder).txtvTitleBig.setVisibility(View.GONE);

                    ((LectureViewHolder) holder).txtvTitleBig.setText(alLectureModels.get(position).getQuick_lecture_text());
                    ((LectureViewHolder) holder).actxtvLectureTitle.setText(alLectureModels.get(position).getQuick_lecture_text());

                    UtillsG.setHeightWidthWithRatio_LinearLayout(((Activity) context), ((LectureViewHolder) holder).frameLayout, 7f, 16f);
                }

                UtillsG.setTextSizeByPercentage(context, ((LectureViewHolder) holder).actxtvLectureTitle, 4);
                UtillsG.setTextSizeByPercentage(context, ((LectureViewHolder) holder).txtvTitleBig, 6);


                String timeAgo = "";
                String groupName = "";
                if (alLectureModels.get(position).getUserGroupData() != null && !UtillsG.getNotNullString(alLectureModels.get(position).getUserGroupData().getGroup_title(), "").isEmpty())
                {
                    groupName = " in group " + alLectureModels.get(position).getUserGroupData().getGroup_title() + ".";
                } else
                {
                    groupName = ".";
                }

                if (mRootFragment != null && mRootFragment instanceof HomeFragment_NoBinding &&
                        alLectureModels.get(position).getIs_reposted()
                        && alLectureModels.get(position).getUserdata().getUser_id().equals(new SharedPrefHelper(context).getUserId()))
                {
                    ImageLoader.setImageSmall(((LectureViewHolder) holder).imgvRepostUserImage, alLectureModels.get(position).getOwnerUserdata().getProfile_pic());
                    timeAgo = UtillsG.get_time_ago(alLectureModels.get(position).getDate_time());
                    ((LectureViewHolder) holder).txtvTimeAgo.setText(timeAgo);
                    ((LectureViewHolder) holder).txtvUserName.setText(whoPosted(alLectureModels.get(position).getOwnerUserdata().getFull_name())
                            + " Posted a New " + UtillsG.QuickLectureType(alLectureModels.get(position).getQuick_lecture_type(), "post ") + groupName);
                } else if ((alLectureModels.get(position).getOwnerUserdata() != null
                        && alLectureModels.get(position).getUserdata().getUser_id().
                        equals(alLectureModels.get(position).getOwnerUserdata().getUser_id()))
                        || alLectureModels.get(position).getOwnerUserdata() == null
                        )
                {
                    ImageLoader.setImageSmall(((LectureViewHolder) holder).imgvRepostUserImage, alLectureModels.get(position).getUserdata().getProfile_pic());
                    timeAgo = UtillsG.get_time_ago(alLectureModels.get(position).getDate_time());
                    ((LectureViewHolder) holder).txtvTimeAgo.setText(timeAgo);
                    ((LectureViewHolder) holder).txtvUserName.setText(whoPosted(alLectureModels.get(position).getUserdata().getFull_name())
                            + " Posted a New " + UtillsG.QuickLectureType(alLectureModels.get(position).getQuick_lecture_type(), "post ") + groupName);
                } else
                {
                    timeAgo = UtillsG.get_time_ago(alLectureModels.get(position).getReposted_date_time());
                    ((LectureViewHolder) holder).txtvUserName.setText(whoPosted(alLectureModels.get(position).getUserdata().getFull_name())
                            + " [img src=ic_refresh_green/]  Re-Posted a " + UtillsG.QuickLectureType(alLectureModels.get(position).getQuick_lecture_type(), "post ") + groupName);

                    ImageLoader.setImageSmall(((LectureViewHolder) holder).imgvRepostUserImage, alLectureModels.get(position).getUserdata().getProfile_pic());
                    ((LectureViewHolder) holder).txtvTimeAgo.setText(timeAgo);
                }

                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((Activity) context).getWindowManager()
                        .getDefaultDisplay()
                        .getMetrics(displayMetrics);
                int width = displayMetrics.widthPixels;
                if (mRootFragment instanceof SearchLectureFragment)
                {
                    ((LectureViewHolder) holder).txtvUserName.setSingleLine(true);
                } else
                {
                    Gson gson = new Gson();
                    final LectureDetailsModel mModel;

                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse((gson.toJson(alLectureModels.get(position))));
                    Gson gson2 = new Gson();
                    mModel = gson2.fromJson(mJson, LectureDetailsModel.class);

                    ((EventViewHolder) holder).textUserCreatedBy.setText("Created By : " + whoPosted(alLectureModels.get(position).getUserdata().getFull_name()));

                    final LectureEventViewModel lectureEventViewModel = new LectureEventViewModel();
                    lectureEventViewModel.setmLectureEvent(mModel);
                    lectureEventViewModel.setLecture_event_id(mModel.getEvent_id());
                    lectureEventViewModel.setTitle(mModel.getLecture_title());
                    lectureEventViewModel.setStartTime(DateHelper.convertFromServerDateToRequiredDate(mModel.getLecture_start_date_time(), "dd MMM"));
                    lectureEventViewModel.setImageUrl(mModel.getLecture_poster());
                    lectureEventViewModel.setMessage(mModel.getDetails());
                    lectureEventViewModel.setLocation(mModel.getAddress());
                    lectureEventViewModel.setTimeRange(mModel.getLecture_start_date_time() + " to " + mModel.getLecture_end_date_time());
                    lectureEventViewModel.setCanInviteGuest(
                            mModel.getCan_invite_friends().equals("true") ? true : false);
                    lectureEventViewModel.setmLectureEvent(mModel);

                    if (alLectureModels.get(position).getUser_id().equals(new SharedPrefHelper(context).getUserId()))
                    {
                        lectureEventViewModel.setUpcoming(false);
                    } else
                    {
                        lectureEventViewModel.setUpcoming(true);
                    }
                    if (mModel.getmIsInterested() != null)
                    {
                        if (mModel.getmIsInterested().getAttend_type().equals("digital"))
                        {
                            lectureEventViewModel.setAttendingLecture(AttendingLecture.GOING_DIGITAL);
                        } else if (mModel.getmIsInterested().getAttend_type().equals("attending"))
                        {
                            lectureEventViewModel.setAttendingLecture(AttendingLecture.ATTENDING);
                        } else
                        {
                            lectureEventViewModel.setAttendingLecture(AttendingLecture.NO_OUTPUT);
                        }


                        if (mModel.getmIsInterested().getIs_interested().equals("true"))
                        {
                            lectureEventViewModel.setIsInterested(AttendingLecture.IS_INTERESTED);
                        } else if (mModel.getmIsInterested().getIs_interested().equals("false"))
                        {
                            lectureEventViewModel.setIsInterested(AttendingLecture.IS_IGNORED);
                        } else
                        {
                            lectureEventViewModel.setIsInterested(AttendingLecture.NO_OUTPUT);
                        }


                        lectureEventViewModel.setIgnored(false);
                    } else
                    {
                        lectureEventViewModel.setAttendingLecture(AttendingLecture.NO_OUTPUT);
                        lectureEventViewModel.setIsInterested(AttendingLecture.NO_OUTPUT);
                        lectureEventViewModel.setIgnored(false);
                    }


                    ((EventViewHolder) holder).frameProfileImage.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            LectureEventDetailsActivity.start(context, lectureEventViewModel, v, mModel);
                        }
                    });
                    ImageLoader.setImageSmall(((EventViewHolder) holder).imgvUserImage, alLectureModels.get(position).getLecture_poster());

                    ((EventViewHolder) holder).txtvTime.setText(DateHelper.convertFromServerDateToRequiredDate(alLectureModels.get(position).getLecture_start_date_time(), "dd MMM"));
                    ((EventViewHolder) holder).txtvEventTitle.setText(alLectureModels.get(position).getLecture_title());

                    String dateFrom = DateHelper.convertFromServerDateToRequiredDate(alLectureModels.get(position).getLecture_start_date_time(), "dd/MMM hh:mm a");
                    String dateTo = DateHelper.convertFromServerDateToRequiredDate(alLectureModels.get(position).getLecture_end_date_time(), "dd/MMM hh:mm a");
                    ((EventViewHolder) holder).txtvTimeRange.setText(Html.fromHtml(dateFrom + " <b>To</b> " + dateTo));

                    if (!UtillsG.getNotNullString(alLectureModels.get(position).getAddress(), "").isEmpty())
                    {
                        ((EventViewHolder) holder).txtvLocation.setText(alLectureModels.get(position).getAddress());
                        ((EventViewHolder) holder).txtvLocation.setVisibility(View.VISIBLE);
                    } else
                    {
                        ((EventViewHolder) holder).txtvLocation.setVisibility(View.GONE);
                    }

                    // Setting background according to isInterested
                    ((EventViewHolder) holder).txtvIsInterested.setBackgroundResource(
                            alLectureModels.get(position).getIsinterested() == null
                                    ? R.drawable.default_btn_selector
                                    : R.drawable.offwhite_btn_selector);

                    // set is interested
                    if (alLectureModels.get(position).getIsinterested().getIs_interested().equals("true"))
                    {
                        ((EventViewHolder) holder).txtvIsInterested.setText(AttendingLecture.IS_INTERESTED);
                    } else if (alLectureModels.get(position).getIsinterested().getIs_interested().equals("false"))
                    {
                        ((EventViewHolder) holder).txtvIsInterested.setText(AttendingLecture.IS_IGNORED);
                    } else
                    {
                        ((EventViewHolder) holder).txtvIsInterested.setText(AttendingLecture.NO_OUTPUT);
                    }

                    // Setting background according to isInterested
                    ((EventViewHolder) holder).txtvAttending.setBackgroundResource(
                            alLectureModels.get(position).getIsinterested() == null
                                    ? R.drawable.default_btn_selector
                                    : R.drawable.offwhite_btn_selector);


                    // Setting background according to isInterested
                    ((EventViewHolder) holder).txtvAttending.setBackgroundResource(
                            alLectureModels.get(position).getIsinterested().getAttend_type() == null
                                    ? R.drawable.default_btn_selector
                                    : R.drawable.offwhite_btn_selector);
                    // set is attending
                    if (alLectureModels.get(position).getIsinterested().getAttend_type().equals("digital"))
                    {
                        ((EventViewHolder) holder).txtvAttending.setText(AttendingLecture.GOING_DIGITAL);
                    } else if (alLectureModels.get(position).getIsinterested().getAttend_type().equals("attending"))
                    {
                        ((EventViewHolder) holder).txtvAttending.setText(AttendingLecture.ATTENDING);
                    } else
                    {
                        ((EventViewHolder) holder).txtvAttending.setText(AttendingLecture.NO_OUTPUT);
                    }

                    ((EventViewHolder) holder).txtvAttending.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {

                        }
                    });
                    ((EventViewHolder) holder).view.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            LectureEventDetailsActivity.start(context, lectureEventViewModel, v, mModel);
                        }
                    });
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void repostLecture(final int position, final LectureViewHolder holder)
    {
        if (alLectureModels.get(position).getUser_id().equals(new SharedPrefHelper(context).getUserId())
                || (alLectureModels.get(position).getOwnerUserdata() != null
                && alLectureModels.get(position).getOwnerUserdata().getUser_id().equals(new SharedPrefHelper(context).getUserId()))
                )
        {
            UtillsG.showToast("You are the creator so you cannot repost it.", context, true);
        } else if (!alLectureModels.get(position).getIs_reposted())
        {

            MediaPlayer mp = MediaPlayer.create(context, R.raw.tone_like);
            mp.start();
            UtillsG.showLoading("Please wait..", context);
            mClicks.RepostQuickLecture((Activity) context, LVApplication.getRetrofit().create(HomeApis.class),
                    alLectureModels.get(position).getQuick_lecture_id(), new CallBackG<Boolean>()
                    {
                        @Override
                        public void onCallBack(Boolean output)
                        {
                            UtillsG.hideLoading();
                            if (output)
                            {
                                int repostCount = Integer.parseInt(alLectureModels.get(position).getReposted_count());
                                repostCount++;
                                holder.txtvRepostedCount.setText("" + repostCount);
                                alLectureModels.get(position).setReposted_count(String.valueOf(repostCount));
                                alLectureModels.get(position).setIs_reposted(true);
                                notifyDataSetChanged();
                                EventBus.getDefault().post(new HomeFragmentUpdateBus(true));

                            }
                        }
                    });
        } else if (alLectureModels.get(position).getIs_reposted())
        {

            MediaPlayer mp = MediaPlayer.create(context, R.raw.tone_dislike);
            mp.start();
            UtillsG.showLoading("Please wait..", context);
            mClicks.unrepost_lecture((Activity) context,
                    alLectureModels.get(position).getQuick_lecture_id() != null ?
                            alLectureModels.get(position).getQuick_lecture_id()
                            : ""
                    , new CallBackG<Boolean>()
                    {
                        @Override
                        public void onCallBack(Boolean output)
                        {
                            UtillsG.hideLoading();
                            if (output)
                            {
                                int repostCount = Integer.parseInt(alLectureModels.get(position).getReposted_count());
                                repostCount--;
                                holder.txtvRepostedCount.setText("" + repostCount);
                                alLectureModels.get(position).setReposted_count(String.valueOf(repostCount));
                                alLectureModels.get(position).setIs_reposted(false);
                                notifyDataSetChanged();
                                EventBus.getDefault().post(new HomeFragmentUpdateBus(true));
                            }
                        }
                    });
        } else
        {
            UtillsG.showToast("Already reposted this lecture", context, true);
        }
    }


    @Override
    public int getItemCount()
    {
        return alLectureModels.size();
    }

    @Override
    public int getItemViewType(int position)
    {
        if (alLectureModels.get(position).getType().equals("lecture"))
        {
            return 1;
        } else
        {
            return 2;
        }
    }

    class LectureViewHolder extends RecyclerView.ViewHolder
    {
        View view, view3, viewTop, viewBottom;
        ImageView imgvThumbnail, imgvUserImage, imgvOptions, imgvLike, imgv_repost, imgvRepostUserImage;
        TextView actxtvLectureTitle, txtvDuration;
        TextView txtvFavouriteCount, txtvCommentsCount, txtvRepostedCount, txtvTitleBig, txtvViewsCount, txtvTimeAgo;
        TextViewWithImages txtvUserName;
        LinearLayout layPlay, linearMain, layLike, llRepost, ll_comments, ll_connect, ll_repostDetails;
        FrameLayout frameLayout;
        RecyclerView recyLectureInterests;
        CardView cardView;

        public LectureViewHolder(View row)
        {
            super(row);

            view = row;
            view3 = view.findViewById(R.id.view3);
            viewTop = view.findViewById(R.id.viewTop);
            viewBottom = view.findViewById(R.id.viewBottom);
            recyLectureInterests = (RecyclerView) view.findViewById(R.id.recyLectureInterests);
            frameLayout = (FrameLayout) view.findViewById(R.id.frameLayout);
            llRepost = (LinearLayout) view.findViewById(R.id.llRepost);
            layLike = (LinearLayout) view.findViewById(R.id.layLike);
            linearMain = (LinearLayout) view.findViewById(R.id.linearMain);
            layPlay = (LinearLayout) view.findViewById(R.id.layPlay);
            imgvLike = (ImageView) view.findViewById(R.id.imgvLike);
            imgvOptions = (ImageView) view.findViewById(R.id.imgvOptions);
            imgvUserImage = (ImageView) view.findViewById(R.id.imgvUserImage);
            imgvThumbnail = (ImageView) view.findViewById(R.id.imgvThumbnail);
            actxtvLectureTitle = (TextView) view.findViewById(R.id.actxtvLectureTitle);
            txtvFavouriteCount = (TextView) view.findViewById(R.id.txtvFavouriteCount);
            txtvCommentsCount = (TextView) view.findViewById(R.id.txtvCommentsCount);
            txtvRepostedCount = (TextView) view.findViewById(R.id.txtvRepostedCount);
            txtvDuration = (TextView) view.findViewById(R.id.txtvDuration);

            ll_repostDetails = (LinearLayout) view.findViewById(R.id.ll_repostDetails);

            txtvTitleBig = (TextView) view.findViewById(R.id.txtvTitleBig);
            txtvViewsCount = (TextView) view.findViewById(R.id.txtvViewsCount);
            imgv_repost = (ImageView) view.findViewById(R.id.imgv_repost);
            ll_comments = (LinearLayout) view.findViewById(R.id.ll_comments);
            cardView = (CardView) view.findViewById(R.id.cardView);

            txtvUserName = (TextViewWithImages) view.findViewById(R.id.txtvUserName);
            txtvTimeAgo = (TextView) view.findViewById(R.id.txtvTimeAgo);
            ll_connect = (LinearLayout) view.findViewById(R.id.ll_connect);
            imgvRepostUserImage = (ImageView) view.findViewById(R.id.imgvRepostUserImage);

        }
    }

    class EventViewHolder extends RecyclerView.ViewHolder
    {
        View view;
        LinearLayout frameProfileImage;
        ImageView imgvUserImage;
        TextView txtvTime, txtvEventTitle, txtvTimeRange, txtvLocation, txtvIsInterested, txtvAttending, textUserCreatedBy;
        LinearLayout layoutBottomButtons;


        public EventViewHolder(View row)
        {
            super(row);

            view = row;
            frameProfileImage = (LinearLayout) view.findViewById(R.id.frameProfileImage);
            imgvUserImage = (ImageView) view.findViewById(R.id.imgvUserImage);
            txtvTime = (TextView) view.findViewById(R.id.txtvTime);
            txtvTimeRange = (TextView) view.findViewById(R.id.txtvTimeRange);
            txtvEventTitle = (TextView) view.findViewById(R.id.txtvEventTitle);
            txtvLocation = (TextView) view.findViewById(R.id.txtvLocation);
            txtvIsInterested = (TextView) view.findViewById(R.id.txtvIsInterested);
            txtvAttending = (TextView) view.findViewById(R.id.txtvAttending);
            layoutBottomButtons = (LinearLayout) view.findViewById(R.id.layoutBottomButtons);
            textUserCreatedBy = (TextView) view.findViewById(R.id.textUserCreatedBy);

        }
    }

    public void showMoreOptions(View view, final Lecture_And_Event_Model mModel, final LectureViewHolder holder, final int position)
    {
        DialogHelper.getInstance().showLectureMoreOptions(
                context,
                view,
                mModel.getIs_reposted(), alLectureModels.get(position).getUser_id().equalsIgnoreCase(new SharedPrefHelper(context).getUserId()),
                new CallBackG<Integer>()
                {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onCallBack(final Integer attendType)
                    {

                        if (attendType == 1)
                        {
                            ((LectureViewHolder) holder).layPlay.performClick();
                        } else if (attendType == 2)
                        {
                            ((LectureViewHolder) holder).layLike.performClick();
                        } else if (attendType == 3)
                        {
                            ((LectureViewHolder) holder).ll_connect.performClick();
                        } else if (attendType == 4)
                        {

                        } else if (attendType == 5)
                        {
                            ((LectureViewHolder) holder).llRepost.performClick();
                        } else if (attendType == 6)
                        {
                            if (mModel.getDiscovery_url() != null && !mModel.getDiscovery_url().trim().isEmpty())
                            {
                                WebViewRecorderActivity.startForBrowsingOnly(context, mModel.getDiscovery_url());
                            } else
                            {
                                UtillsG.showToast("Url is empty", context, true);
                            }
                        } else if (attendType == 7)
                        {
                            executeShareExternally(mModel);
                        } else if (attendType == 8)
                        {
                            DialogHelper.getInstance().executeDeleteLecture(alLectureModels.get(position), context, new CallBackG<Boolean>()
                            {
                                @Override
                                public void onCallBack(Boolean output)
                                {
                                    if (output)
                                    {
                                        alLectureModels.remove(position);
                                        notifyDataSetChanged();
                                    }
                                }
                            });
                        }
                    }
                });
    }

    private void executeShareExternally(final Lecture_And_Event_Model mModel)
    {
        Call<BasicApiModel> shareExternally = LVApplication.getRetrofit().create(HomeApis.class).
                save_external_share("0",
                        mModel.getQuick_lecture_id(),
                        new SharedPrefHelper(context).getUserId(),
                        mModel.getUser_id(),
                        mModel.getUser_id().equals(new SharedPrefHelper(context).getUserId())
                                ? " You "
                                : new SharedPrefHelper(context).getUserName(),
                        mModel.getQuick_lecture_text());
        shareExternally.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
            {
                Log.e("external shr success", "---------------------------------");
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t)
            {
                Log.e("external shr failed", t.getMessage() + "---------------------------------");
            }
        });
       /* Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, Web.Path.BASE_URL + mModel.getFile_path());
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);*/

        UtillsG.shareLinkExternally_QuickLecture(context, mModel);
    }


    private void likeUnlikeLecture(final int position, final LectureViewHolder mHolder)
    {

        MediaPlayer mp = MediaPlayer.create(context,
                alLectureModels.get(position).getIs_favourite().equalsIgnoreCase("0")
                        ? R.raw.tone_like : R.raw.tone_dislike);
        mp.start();

        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance().add_remove_to_favourite_list(new SharedPrefHelper(context).getUserId(), alLectureModels.get(position).getQuick_lecture_id());
        basicApiModelCall.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
            {
                if (response.body() != null && response.body().getStatus())
                {
                    Log.e("RESPONSE", response.body().getStatus() + "");

                    if (response.body().getStatus())
                    {
                        int count = Integer.parseInt(alLectureModels.get(position).getFavourites_count());
                        UtillsG.showToast(String.valueOf(response.body().getMessage()), context, true);
                        if (alLectureModels.get(position).getIs_favourite().equalsIgnoreCase("0"))
                        {
                            alLectureModels.get(position).setIs_favourite("1");
                            count++;
                            alLectureModels.get(position).setFavourites_count("" + count);
                            mHolder.imgvLike.setImageResource(R.drawable.ic_liked);
                            mHolder.imgvLike.setColorFilter(ContextCompat.getColor(context, R.color.redTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
                        } else
                        {
                            alLectureModels.get(position).setIs_favourite("0");
                            count--;
                            alLectureModels.get(position).setFavourites_count("" + count);
                            mHolder.imgvLike.setImageResource(R.mipmap.ic_fav_white_border);
                            mHolder.imgvLike.setColorFilter(ContextCompat.getColor(context, R.color.greyDark), android.graphics.PorterDuff.Mode.MULTIPLY);
                        }
                        alLectureModels.get(position).setFavourites_count(String.valueOf(count));
                        /*
                         * If user is in Favourite screen then it should be removed from list after Unliking
                         */
                        if (alLectureModels.get(position).getIs_favourite().equalsIgnoreCase("0"))
                        {
                            if (context instanceof FavouriteLecturesListActivity)
                            {
                                alLectureModels.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, alLectureModels.size());
                            } else
                            {
                                notifyDataSetChanged();
                            }
                        } else
                        {
                            notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t)
            {
                Log.e("ERROR get_posted_le", t.getMessage().toString());
                UtillsG.showToast(String.valueOf(t.getMessage().toString()), context, true);
            }
        });
    }

    private void setLectureAsPlayed(final int position)
    {
        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance().add_lecture_to_recent_played_list(new SharedPrefHelper(context).getUserId(), alLectureModels.get(position).getQuick_lecture_id());
        basicApiModelCall.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
            {
                if (response.body() != null && response.body().getStatus())
                {
                    Log.e("RESPONSE", response.body().getStatus() + "");

                    if (response.body().getStatus())
                    {
                        int count = Integer.parseInt(alLectureModels.get(position).getLecture_view());
                        count++;
                        alLectureModels.get(position).setLecture_view(String.valueOf(count));
                        notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t)
            {
                Log.e("ERROR get_posted_le", t.getMessage().toString());
            }
        });
    }

    private RequestBody getOtherParams(String value)
    {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    protected HomeApis getRetrofitInstance()
    {
        return LVApplication.getRetrofit().create(HomeApis.class);
    }

    private String whoPosted(String userName)
    {
        return new SharedPrefHelper(context).getUserName().equalsIgnoreCase(userName) ? "You" : userName;
    }

}