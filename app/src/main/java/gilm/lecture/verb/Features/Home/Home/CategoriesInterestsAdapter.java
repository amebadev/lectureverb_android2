package gilm.lecture.verb.Features.Home.Home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import gilm.lecture.verb.Features.Home.ListingByCategory.ListingByCategoriesActivity;
import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.InfiniteAdapter_WithoutBuinding;
import gilm.lecture.verb.UtilsG.LoadingViewHolder;
import gilm.lecture.verb.UtilsG.UtillsG;

/**
 * created by PARAMBIR SINGH on 31/8/17.
 */

public class CategoriesInterestsAdapter extends InfiniteAdapter_WithoutBuinding<CategoriesInterestsAdapter.MyViewHolderG>
{

    private LayoutInflater inflater;
    List<ChildCategoryModel> data;
    Context context;
    boolean enableClick;

    public CategoriesInterestsAdapter(List<ChildCategoryModel> data, Context context, boolean enableClick) {
        this.data = data;
        this.context = context;
        this.enableClick = enableClick;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        return new LoadingViewHolder(inflater.inflate(R.layout.layout_progress, parent, false));
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public MyViewHolderG onCreateView(ViewGroup parent, int viewType) {
        return new MyViewHolderG(inflater.inflate(R.layout.inflater_interests_home, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        try {
            if (!(holder instanceof LoadingViewHolder)) {
                ((MyViewHolderG) holder).txtvTitle.setText(
                        UtillsG.getNotNullString(
                                data.get(position).getCategory().isEmpty()
                                        ? data.get(position).getCategory_name()
                                        : data.get(position).getCategory(), "Category not found"));
                UtillsG.setTextSizeByPercentage(context, ((MyViewHolderG) holder).txtvTitle, 3f);
                ((MyViewHolderG) holder).llInterest.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        if (enableClick) {
                            ListingByCategoriesActivity.start(context, UtillsG.getNotNullString(
                                    data.get(position).getCategory_id().isEmpty()
                                            ? data.get(position).getId()
                                            : data.get(position).getCategory_id(), "0"), UtillsG.getNotNullString(
                                    data.get(position).getCategory().isEmpty()
                                            ? data.get(position).getCategory_name()
                                            : data.get(position).getCategory(), "Filter by category"));
                        }
                    }
                });
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    class MyViewHolderG extends RecyclerView.ViewHolder
    {
        View view;
        TextView txtvTitle, txtvParent;
        LinearLayout llInterest;

        public MyViewHolderG(View row) {
            super(row);

            view = row;
            txtvParent = (TextView) view.findViewById(R.id.txtvParent);
            txtvTitle = (TextView) view.findViewById(R.id.txtvTitle);
            llInterest = (LinearLayout) view.findViewById(R.id.llInterest);
        }
    }

}
