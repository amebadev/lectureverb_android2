package gilm.lecture.verb.Features.Favorites;

import android.view.View;
import android.widget.ImageView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import gilm.lecture.verb.Features.Favorites.FavouritesLecturesList.FavouriteLecturesListActivity;
import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.Home.Home.HomeFragment_NoBinding;
import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsListActivity;
import gilm.lecture.verb.Features.Messaging.Chat.ChatActivity;
import gilm.lecture.verb.Features.QuickLecture.ChoosePostOptionActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Navigator;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.databinding.FragmentFavoritesBinding;

/**
 * Created by G-Expo on 17 Jul 2017.
 */

public class FavoritesFragment extends BaseFragment<FragmentFavoritesBinding, FavoritesPresenter> implements FavoritesView
{

    private ImageView fab;

    public static FavoritesFragment newInstance()
    {
        return new FavoritesFragment();
    }

    @Override
    protected int getLayoutId()
    {
        return R.layout.fragment_favorites;
    }

    @Override
    protected void onCreateFragmentG()
    {
        injectPresenter(new FavoritesPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews()
    {
        getDataBinder().setPresenter(getPresenter());
        new Navigator().replaceFragment(getActivity(), R.id.container, new HomeFragment_NoBinding());

        fab = (ImageView) view.findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {

                ChoosePostOptionActivity.start(getActivity());

            }
        });


    }

    @Override
    public void showLectureEvents()
    {
        LectureEventsListActivity.start(getActivityG());
    }

    @Override
    public void FavouriteLecturesClicked() {
        FavouriteLecturesListActivity.start(getActivityG());
    }


}
