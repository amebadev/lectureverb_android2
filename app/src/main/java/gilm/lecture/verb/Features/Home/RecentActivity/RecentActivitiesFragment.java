package gilm.lecture.verb.Features.Home.RecentActivity;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import gilm.lecture.verb.Features.Home.Notification.NotificationViewModel;
import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.R;
import gilm.lecture.verb.databinding.FragmentRecyclerlistBinding;

/**
 * Created by G-Expo on 17 Jul 2017.
 */

public class RecentActivitiesFragment extends BaseFragment<FragmentRecyclerlistBinding, RecentActivitiesPresenter> implements RecentActivitiesView, InfiniteAdapterG.OnLoadMoreListener
{

    public static RecentActivitiesFragment newInstance() {
        return new RecentActivitiesFragment();
    }

    public RecentActivitiesFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_recyclerlist;
    }

    @Override
    protected void onCreateFragmentG() {
        injectPresenter(new RecentActivitiesPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        getDataBinder().setIsLoading(getPresenter().getIsLoadingViewModel());
        getPresenter().loadNotificationData();
    }

    @Override
    public void showData(List<NotificationViewModel> list) {
        RecentActivitiesAdapter notificationAdapter = new RecentActivitiesAdapter(getActivity(), list);
        notificationAdapter.setOnLoadMoreListener(this);
        getDataBinder().reviewsList.setAdapter(notificationAdapter);
        notificationAdapter.setShouldLoadMore(false);
    }

    @Override
    public void onLoadMore() {

    }
}