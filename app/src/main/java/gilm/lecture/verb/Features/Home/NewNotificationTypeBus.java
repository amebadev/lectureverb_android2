package gilm.lecture.verb.Features.Home;

/**
 * Created by harpreet on 11/14/17.
 */

public class NewNotificationTypeBus {

    Boolean isNewMessage,isOtherNotification;

    public NewNotificationTypeBus(Boolean isNewMessage,Boolean isOtherNotification){
        this.isNewMessage=isNewMessage;
        this.isOtherNotification=isOtherNotification;
    }

    public Boolean getNewMessage() {
        return isNewMessage;
    }

    public void setNewMessage(Boolean newMessage) {
        isNewMessage = newMessage;
    }

    public Boolean getOtherNotification() {
        return isOtherNotification;
    }

    public void setOtherNotification(Boolean otherNotification) {
        isOtherNotification = otherNotification;
    }
}
