package gilm.lecture.verb.Features.Favorites;

import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;

/**
 * Created by G-Expo on 17 Jul 2017.
 */

public class FavoritesPresenter extends BasePresenter<FavoritesView>
{

    public void LectureEventClicked(View view) {
        getView().showLectureEvents();
    }

    public void FavouriteLecturesClicked(View view) {
        getView().FavouriteLecturesClicked();
    }
}
