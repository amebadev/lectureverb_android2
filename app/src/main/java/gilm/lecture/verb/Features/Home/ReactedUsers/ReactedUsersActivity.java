package gilm.lecture.verb.Features.Home.ReactedUsers;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.Internal.Base.EmptyPresenter;
import gilm.lecture.verb.Features.Internal.Base.EmptyView;
import gilm.lecture.verb.Features.TabAdapter.FragmentTabModel;
import gilm.lecture.verb.Features.TabAdapter.SectionsPagerAdapter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.ActivityReactedUsersBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Lists of users who liked commented and reposted the posts
 */
public class ReactedUsersActivity extends BaseActivity<ActivityReactedUsersBinding, EmptyPresenter> implements EmptyView, SwipeRefreshLayout.OnRefreshListener {

    String quick_lecture_id = "";
    int tabNumber = 0;

    public static void start(Context context, String quick_lecture_id, int tabNumber) {
        Intent starter = new Intent(context, ReactedUsersActivity.class);
        starter.putExtra("quick_lecture_id", quick_lecture_id);
        starter.putExtra("tabNumber", tabNumber);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_reacted_users;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new EmptyPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        setupToolbar("Details");
        getDataBinder().swipeRefresh.setOnRefreshListener(this);

        quick_lecture_id = getIntent().getStringExtra("quick_lecture_id");
        tabNumber = getIntent().getIntExtra("tabNumber", 0);

        refreshData();
    }

    private void refreshData() {
        getDataBinder().swipeRefresh.setRefreshing(true);
        Call<ReacteddataModel> call = LVApplication.getRetrofit().create(HomeApis.class).get_lecture_likes_comments_reposts_list(getLocalData().getUserId(), quick_lecture_id);
        call.enqueue(new Callback<ReacteddataModel>() {
            @Override
            public void onResponse(Call<ReacteddataModel> call, Response<ReacteddataModel> response) {
                getDataBinder().swipeRefresh.setRefreshing(false);
                if (response.body() != null && response.body().getStatus()) {

                    List<FragmentTabModel> list = new ArrayList<>();
                    list.add(new FragmentTabModel(new ReactedUsersListFragment(response.body().getLists().getViewedBy()), "Viewed By"));
                    list.add(new FragmentTabModel(new ReactedUsersListFragment(response.body().getLists().getLikes()), "Liked By"));
                    list.add(new FragmentTabModel(new ReactedUsersListFragment(response.body().getLists().getComments()), "Commented By"));
                    list.add(new FragmentTabModel(new ReactedUsersListFragment(response.body().getLists().getReposts()), "Reposted By"));

                    SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager(), list);
                    getDataBinder().viewPager.setAdapter(adapter);
                    getDataBinder().tab.setupWithViewPager(getDataBinder().viewPager);
                    getDataBinder().tab.setTabMode(TabLayout.MODE_SCROLLABLE);

                    getDataBinder().viewPager.setCurrentItem(tabNumber);
                }
            }

            @Override
            public void onFailure(Call<ReacteddataModel> call, Throwable t) {
                getDataBinder().swipeRefresh.setRefreshing(false);
                UtillsG.showToast(t.getMessage(), getActivityG(), true);
            }
        });
    }

    @Override
    public Context getActivityG() {
        return ReactedUsersActivity.this;
    }

    @Override
    public void onRefresh() {
        refreshData();
    }
}
