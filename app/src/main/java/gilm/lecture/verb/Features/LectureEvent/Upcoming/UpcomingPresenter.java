package gilm.lecture.verb.Features.LectureEvent.Upcoming;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.LectureEvent.AttendingLecture;
import gilm.lecture.verb.Features.LectureEvent.Details.LectureEventDetailsActivity;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.LectureEvent.LectureEventPresenterBinder;
import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsApi;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureEventModel;
import gilm.lecture.verb.Features.LectureEvent.SendInvitationViaApp.SendInvitationActivity;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DateHelper;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.IsLoadingViewModel;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 07 Jul 2017.
 */

public class UpcomingPresenter extends BasePresenter<UpcomingView> implements LectureEventPresenterBinder {

    public IsLoadingViewModel isLoadingViewModel;
    LectureEventClicks mClicks = new LectureEventClicks();

    public IsLoadingViewModel getIsLoadingViewModel() {
        return isLoadingViewModel;
    }

    public UpcomingPresenter() {
        isLoadingViewModel = new IsLoadingViewModel();
        isLoadingViewModel.setLoading(false);
    }


    public void loadData(final int page) {

        isLoadingViewModel.setLoading(true);
        Call<LectureEventModel> basicApiModelCall = getRetrofitInstance().get_lecture_event(
                getOtherParams(new SharedPrefHelper(getView().getActivityG()).getUserId())
                , getOtherParams("upcoming")
                , getOtherParams("" + page));
        basicApiModelCall.enqueue(new Callback<LectureEventModel>() {
            @Override
            public void onResponse(Call<LectureEventModel> call, Response<LectureEventModel> response) {
                isLoadingViewModel.setLoading(false);
                if (response.body() != null) {
                    if (response.body().getStatus()) {

                        if (response.body().getmQuickLectureData() != null && response.body().getmQuickLectureData().size() > 0) {
                            ArrayList<LectureEventViewModel> list = new ArrayList<>();
                            for (int i = 0; i < response.body().getmQuickLectureData().size(); i++) {

                                LectureDetailsModel mLectureDetails = response.body().getmQuickLectureData().get(i);
                                LectureEventViewModel lectureEventViewModel = new LectureEventViewModel();
                                lectureEventViewModel.setmLectureEvent(response.body().getmQuickLectureData().get(i));
                                lectureEventViewModel.setLecture_event_id(mLectureDetails.getEvent_id());
                                lectureEventViewModel.setTitle(mLectureDetails.getLecture_title());
                                lectureEventViewModel.setStartTime(DateHelper.convertFromServerDateToRequiredDate(mLectureDetails.getLecture_start_date_time(), "dd MMM"));
                                lectureEventViewModel.setImageUrl(mLectureDetails.getLecture_poster());
                                lectureEventViewModel.setMessage(mLectureDetails.getDetails());
                                lectureEventViewModel.setLocation(mLectureDetails.getAddress());
                                lectureEventViewModel.setCanInviteGuest(
                                        mLectureDetails.getCan_invite_friends().equals("true")
                                                ? true : false);

                                if (UtillsG.getNotNullString(response.body().getmQuickLectureData().get(0).getUser_id(), "").equals(new SharedPrefHelper(getView().getActivityG()).getUserId())) {
                                    lectureEventViewModel.setUpcoming(false);
                                } else {
                                    lectureEventViewModel.setUpcoming(true);
                                }

                                if (mLectureDetails.getmIsInterested() != null) {

                                    if (mLectureDetails.getmIsInterested().getAttend_type().equals("digital")) {
                                        lectureEventViewModel.setAttendingLecture(AttendingLecture.GOING_DIGITAL);
                                    } else if (mLectureDetails.getmIsInterested().getAttend_type().equals("attending")) {
                                        lectureEventViewModel.setAttendingLecture(AttendingLecture.ATTENDING);
                                    } else {
                                        lectureEventViewModel.setAttendingLecture(AttendingLecture.NO_OUTPUT);
                                    }


                                    if (mLectureDetails.getmIsInterested().getIs_interested().equals("true")) {
                                        lectureEventViewModel.setIsInterested(AttendingLecture.IS_INTERESTED);
                                    } else if (mLectureDetails.getmIsInterested().getIs_interested().equals("false")) {
                                        lectureEventViewModel.setIsInterested(AttendingLecture.IS_IGNORED);
                                    } else {
                                        lectureEventViewModel.setIsInterested(AttendingLecture.NO_OUTPUT);
                                    }


                                    lectureEventViewModel.setIgnored(false);
                                } else {
                                    lectureEventViewModel.setAttendingLecture(AttendingLecture.NO_OUTPUT);
                                    lectureEventViewModel.setIsInterested(AttendingLecture.NO_OUTPUT);
                                    lectureEventViewModel.setIgnored(false);
                                }

/*
                                String timeRange = DateHelper.convertFromServerDateToRequiredDate(mLectureDetails.getLecture_created_date_time(), "dd/MM/yyyy HH:mm a")
                                        + "- " +
                                        DateHelper.convertFromServerDateToRequiredDate(mLectureDetails.getLecture_end_date_time(), "dd/MM/yyyy HH:mm a");
*/

                                String timeRange = "Start - "+DateHelper.convertFromServerDateToRequiredDate(mLectureDetails.getLecture_created_date_time(), "dd-MM-yyyy HH:mm a")
                                        + "\n" +
                                        DateHelper.convertFromServerDateToRequiredDate(mLectureDetails.getLecture_end_date_time(), "dd-MM-yyyy HH:mm a")+" - End";

//                                lectureEventViewModel.setTimeRange(mLectureDetails.getLecture_created_date_time() + "- " + mLectureDetails.getLecture_end_date_time());
                                lectureEventViewModel.setTimeRange(timeRange);

                                list.add(lectureEventViewModel);
                            }
                            if (getView() != null) {
                                getView().showData(list, page);
                            }
                        } else {
                            UtillsG.showToast("No lecture events yet", getView().getActivityG(), true);
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<LectureEventModel> call, Throwable t) {
                if (getView() != null) {
                    isLoadingViewModel.setLoading(true);
                    Log.e("Error occured", t.getMessage().toString());
                }
            }
        });
    }

    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    protected LectureEventsApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(LectureEventsApi.class);
    }

    @Override
    public void markAttendant(View view, final LectureEventViewModel viewModel) {
        DialogHelper.getInstance().showAttendentMenu(getView().getActivityG(), view, new CallBackG<String>() {
            @Override
            public void onCallBack(final String attendType) {
                mClicks.setAttendingStatus((Activity) getView().getActivityG(), getRetrofitInstance(), viewModel.getLecture_event_id(),
                        attendType.equals(AttendingLecture.GOING_DIGITAL) ? "digital"
                                : "attending", new CallBackG<Boolean>() {
                            @Override
                            public void onCallBack(Boolean output) {
                                if (output) {
//                                    loadData(1);
                                    viewModel.setIgnored(false);
                                    viewModel.setAttendingLecture(attendType);
                                }
                            }
                        });
            }
        });
    }

    @Override
    public void markInterested(View view, final LectureEventViewModel viewModel) {
        DialogHelper.getInstance().showInterestedMenu(getView().getActivityG(), view, new CallBackG<String>() {
            @Override
            public void onCallBack(final String interestedValue) {
                mClicks.setInterestedStatus((Activity) getView().getActivityG(), getRetrofitInstance(), viewModel.getLecture_event_id(),
                        interestedValue.equals(AttendingLecture.IS_INTERESTED) ? "true"
                                : "false", new CallBackG<Boolean>() {
                            @Override
                            public void onCallBack(Boolean output) {
                                if (output) {
//                                    loadData(1);
                                    viewModel.setIgnored(false);
                                    viewModel.setIsInterested(interestedValue);
                                }
                            }
                        });
            }
        });
    }

    @Override
    public void share(final LectureEventViewModel viewModel) {
        DialogHelper.getInstance().shareEventDialog(viewModel, getView().getActivityG(), "Share event", new CallBackG<Boolean>() {
            @Override
            public void onCallBack(Boolean isShareOnProfile) {
                if (isShareOnProfile) {
                    mClicks.ShareOnUserProfileFeed((Activity) getView().getActivityG(), getRetrofitInstance(), viewModel.getLecture_event_id(), "event",
                            new CallBackG<Boolean>() {
                                @Override
                                public void onCallBack(Boolean output) {

                                }
                            });
                } else {
                    SendInvitationActivity.start(getView().getActivityG(), viewModel.getLecture_event_id());
                }
            }
        });
    }

    @Override
    public void showDetails(View view, LectureEventViewModel viewModel) {
        LectureEventDetailsActivity.start(getView().getActivityG(), viewModel, view, viewModel.getmLectureEvent());
    }
}

