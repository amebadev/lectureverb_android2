/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Search;

import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.Features.QuickLecture.ChoosePostOptionActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.AdsModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.FargmentSearchLectureBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 24 Jul 2017.
 */

public class SearchLectureFragment extends BaseFragment<FargmentSearchLectureBinding, SearchLecturePresenter> implements SearchLectureView, InfiniteAdapterG.OnLoadMoreListener {
    private ImageView fab;

    public static SearchLectureFragment newInstance() {
        return new SearchLectureFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fargment_search_lecture;
    }

    @Override
    protected void onCreateFragmentG() {
        injectPresenter(new SearchLecturePresenter());
        getPresenter().attachView(this);

        showLoading("Please wait...");
        Call<SuggestedLectureModel> get_suggested_lectures = getRetrofitInstance().get_suggested_lectures(new SharedPrefHelper(getActivityG()).getUserId());
        get_suggested_lectures.enqueue(new Callback<SuggestedLectureModel>() {
            @Override
            public void onResponse(Call<SuggestedLectureModel> call, Response<SuggestedLectureModel> response) {

                hideLoading();
                if (response.body() != null && response.body().getStatus()) {

                    // checking if the lectures found or not
                    boolean noSuggestions = response.body().getLectures().getSuggestedLecList() == null || response.body().getLectures().getSuggestedLecList().isEmpty();
                    boolean noHotLectures = (response.body().getLectures().getHotLecturesList() == null) || response.body().getLectures().getHotLecturesList().isEmpty();
                    boolean noTopLectures = (response.body().getLectures().getTopLecturesList() == null) || response.body().getLectures().getTopLecturesList().isEmpty();


                    // FIXME: 29/1/18  REMOVING TEXT POSTS FROM THE RESULT BECAUSE THE INFLATER'S DESIGN WILL BE HOTPOTCH BECAUSE OF THAT
                    final ArrayList<Lecture_And_Event_Model> alData = new ArrayList<>();
                    if (!noSuggestions) {
                        for (int i = 0; i < response.body().getLectures().getSuggestedLecList().size(); i++) {
                            if (!response.body().getLectures().getSuggestedLecList().get(i).getQuick_lecture_type().equalsIgnoreCase("text")
                                    && !response.body().getLectures().getSuggestedLecList().get(i).getQuick_lecture_type().equalsIgnoreCase("discovery_text")) {
                                alData.add(response.body().getLectures().getSuggestedLecList().get(i));
                            }
                        }
                        noSuggestions = alData.isEmpty();
                    }


                    // Validation for suggested lectures

                    getDataBinder().txtvMoreSuggestions.setVisibility(noSuggestions ? View.GONE : View.VISIBLE);
                    if (noSuggestions) {
                        getDataBinder().txtvNoSuggestions.setVisibility(View.VISIBLE);
                    } else {
                        if (getActivityG() != null) {
                            getDataBinder().txtvNoSuggestions.setVisibility(View.GONE);
                            getDataBinder().viewPager.setAdapter(new SuggestionPagerAdapter(SearchLectureFragment.this, getActivityG(), alData));
                            getDataBinder().pagerIndicator.setViewPager(getDataBinder().viewPager);

                            getDataBinder().txtvScrollLeftSuggested.setText("<<");
                            getDataBinder().txtvScrollLeftSuggested.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (getDataBinder().viewPager.getCurrentItem() != 0) {
                                        int newPosition = getDataBinder().viewPager.getCurrentItem() - 1;
                                        getDataBinder().viewPager.setCurrentItem(newPosition);
                                    }
                                }
                            });
                            getDataBinder().txtvMoreSuggestions.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (getDataBinder().viewPager.getCurrentItem() != (alData.size() - 1)) {
                                        int newPosition = getDataBinder().viewPager.getCurrentItem() + 1;
                                        getDataBinder().viewPager.setCurrentItem(newPosition);
                                    }
                                }
                            });

                        }
                    }


                    // FIXME: 29/1/18  REMOVING TEXT POSTS FROM THE RESULT BECAUSE THE INFLATER'S DESIGN WILL BE HOTPOTCH BECAUSE OF THAT
                    ArrayList<Lecture_And_Event_Model> alDataHotLectures = new ArrayList<>();
                    if (!noHotLectures) {
                        for (int i = 0; i < response.body().getLectures().getHotLecturesList().size(); i++) {
                            if (!response.body().getLectures().getHotLecturesList().get(i).getQuick_lecture_type().equalsIgnoreCase("text")
                                    && !response.body().getLectures().getHotLecturesList().get(i).getQuick_lecture_type().equalsIgnoreCase("discovery_text")) {
                                alDataHotLectures.add(response.body().getLectures().getHotLecturesList().get(i));
                            }
                        }
                        noHotLectures = alData.isEmpty();
                    }


                    // validations for hot lectures
                    if (noHotLectures) {
                        getDataBinder().txtvNoHotLectures.setVisibility(View.VISIBLE);
                    } else {
                        getDataBinder().txtvNoHotLectures.setVisibility(View.GONE);
                        showHotLecture(alDataHotLectures);
                    }


                    // FIXME: 29/1/18  REMOVING TEXT POSTS FROM THE RESULT BECAUSE THE INFLATER'S DESIGN WILL BE HOTPOTCH BECAUSE OF THAT
                    ArrayList<Lecture_And_Event_Model> alDataTopLectures = new ArrayList<>();
                    if (!noTopLectures) {
                        for (int i = 0; i < response.body().getLectures().getTopLecturesList().size(); i++) {
                            if (!response.body().getLectures().getTopLecturesList().get(i).getQuick_lecture_type().equalsIgnoreCase("text")
                                    && !response.body().getLectures().getTopLecturesList().get(i).getQuick_lecture_type().equalsIgnoreCase("discovery_text")) {
                                alDataTopLectures.add(response.body().getLectures().getTopLecturesList().get(i));
                            }
                        }
                        noTopLectures = alData.isEmpty();
                    }


                    // validations for top lectures
                    if (noTopLectures) {
                        getDataBinder().txtvNoTopLectures.setVisibility(View.VISIBLE);
                    } else {
                        getDataBinder().txtvNoTopLectures.setVisibility(View.GONE);
                        getDataBinder().txtvTopLecturesCount.setText("Top " + response.body().getLectures().getTopLecturesList().size() + " Lectures");
                        showTopLecture(alDataTopLectures);
                    }
                }
            }

            @Override
            public void onFailure(Call<SuggestedLectureModel> call, Throwable t) {
                hideLoading();
                if (getActivityG() != null) {
                    UtillsG.showToast(t.getMessage(), getActivityG(), true);
                }
            }
        });
    }

    @Override
    public void initViews() {
        getAllAds();

        getPresenter().loadHotLectures();
        getPresenter().loadTopLectures();

        fab = (ImageView) view.findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChoosePostOptionActivity.start(getActivity());
            }
        });

        getDataBinder().imgvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getDataBinder().edSearch.getText().toString().trim().isEmpty()) {
                    UtillsG.shakeThisView(getDataBinder().llSearchView);
                } else {
                    SearchQuickLectures.start(getActivityG(), getDataBinder().edSearch.getText().toString().trim());
                }
            }
        });
        getDataBinder().edSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getDataBinder().imgvSearch.performClick();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void showHotLecture(final List<Lecture_And_Event_Model> data) {
        if (getActivityG() != null) {
//            HomeAdapter_NoBinding homeAdapter = new HomeAdapter_NoBinding(SearchLectureFragment.this, data, getActivityG());
//            getDataBinder().recyclerViewHot.setAdapter(homeAdapter);
//            getDataBinder().recyclerViewHot.setScrollX(10);

            getDataBinder().viewPagerViewHot.setAdapter(new SuggestionPagerAdapter(SearchLectureFragment.this, getActivityG(), (ArrayList) data));
            getDataBinder().viewPagerViewHot.startAutoScroll(2000);
            getDataBinder().viewPagerViewHot.setStopScrollWhenTouch(true);
            getDataBinder().viewPagerViewHot.setAutoScrollDurationFactor(17);

            getDataBinder().txtvMoreHot.setVisibility(data.isEmpty() ? View.GONE : View.VISIBLE);
            getDataBinder().txtvScrollLeftHot.setText("<<");
            getDataBinder().txtvScrollLeftHot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getDataBinder().viewPagerViewHot.getCurrentItem() != 0) {
                        int newPosition = getDataBinder().viewPagerViewHot.getCurrentItem() - 1;
                        getDataBinder().viewPagerViewHot.setCurrentItem(newPosition);
                    }
                }
            });
            getDataBinder().txtvMoreHot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getDataBinder().viewPagerViewHot.getCurrentItem() != (data.size() - 1)) {
                        int newPosition = getDataBinder().viewPagerViewHot.getCurrentItem() + 1;
                        getDataBinder().viewPagerViewHot.setCurrentItem(newPosition);
                    }
                }
            });

        }
    }

    @Override
    public void showTopLecture(final List<Lecture_And_Event_Model> data) {
        if (getActivityG() != null) {
//            HomeAdapter_NoBinding homeAdapter = new HomeAdapter_NoBinding(SearchLectureFragment.this, data, getActivityG());
//            getDataBinder().recyclerViewTop.setAdapter(homeAdapter);

            getDataBinder().viewPagerViewTop.setAdapter(new SuggestionPagerAdapter(SearchLectureFragment.this, getActivityG(), (ArrayList) data));
            getDataBinder().viewPagerViewTop.startAutoScroll(2000);
            getDataBinder().viewPagerViewTop.setStopScrollWhenTouch(true);
            getDataBinder().viewPagerViewTop.setAutoScrollDurationFactor(17);

            getDataBinder().txtvMoreTop.setVisibility(data.isEmpty() ? View.GONE : View.VISIBLE);
            getDataBinder().txtvScrollLeftTop.setText("<<");
            getDataBinder().txtvScrollLeftTop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getDataBinder().viewPagerViewTop.getCurrentItem() != 0) {
                        int newPosition = getDataBinder().viewPagerViewTop.getCurrentItem() - 1;
                        getDataBinder().viewPagerViewTop.setCurrentItem(newPosition);
                    }
                }
            });
            getDataBinder().txtvMoreTop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getDataBinder().viewPagerViewTop.getCurrentItem() != (data.size() - 1)) {
                        int newPosition = getDataBinder().viewPagerViewTop.getCurrentItem() + 1;
                        getDataBinder().viewPagerViewTop.setCurrentItem(newPosition);
                    }
                }
            });

        }
    }

    @Override
    public void onLoadMore() {

    }

    private SearchApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(SearchApi.class);
    }

    private void getAllAds() {
        Call<AdsModel> call = LVApplication.getRetrofit().create(HomeApis.class).get_ads("lecture_ads");
        call.enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(Call<AdsModel> call, final Response<AdsModel> response) {
                if (response.body().getStatus()) {
                    UtillsG.setHeightWidthWithRatio_FrameLayout((getActivity()), getDataBinder().viewPagerAd, 7f, 16f);
                    if (!response.body().getList().isEmpty()) {
                        getDataBinder().viewPagerAd.setVisibility(View.VISIBLE);
                        List<AdsModel> adsModels = new ArrayList<>();
                        adsModels = response.body().getList();
                        getDataBinder().viewPagerAd.setAdapter(new AdsPagerAdapter(getActivityG(), adsModels));
                        getDataBinder().viewPagerAd.startAutoScroll(2000);
                        getDataBinder().viewPagerAd.setStopScrollWhenTouch(true);
                        getDataBinder().viewPagerAd.setAutoScrollDurationFactor(17);
                        getDataBinder().viewPagerIndicator.setupWithViewPager(getDataBinder().viewPagerAd);


                    }
                } else {
                    getDataBinder().viewPagerAd.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<AdsModel> call, Throwable t) {
                UtillsG.showToast(t.getMessage(), getActivityG(), true);
            }
        });
    }


}
