package gilm.lecture.verb.Features.NaviagtionDrawer;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.theartofdev.edmodo.cropper.CropImage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Connect.ConnectTabFragment;
import gilm.lecture.verb.Features.Discovery.DiscoveryTabFragment;
import gilm.lecture.verb.Features.Favorites.FavoritesFragment;
import gilm.lecture.verb.Features.Help.HelpFragment;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Home.HomeTabFragment;
import gilm.lecture.verb.Features.LectureEvent.LectureEventTabFragment;
import gilm.lecture.verb.Features.LectureEvent.SearchLectureEvents.SearchLectureEventsActivity;
import gilm.lecture.verb.Features.LoginRegisteration.LoginRegisterApi;
import gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence.UserType;
import gilm.lecture.verb.Features.Player.PublicAudioPlayer;
import gilm.lecture.verb.Features.QuickLecture.ChoosePostOptionActivity;
import gilm.lecture.verb.Features.Search.AdsPagerAdapter;
import gilm.lecture.verb.Features.Search.SearchLectureFragment;
import gilm.lecture.verb.Features.Settings.MainSettings.SettingsFragment;
import gilm.lecture.verb.Features.Splash.SplashActivity;
import gilm.lecture.verb.Features.Subscription.SubscriptionBaseActivity;
import gilm.lecture.verb.Features.UserGroups.GroupList.GroupListFragment;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.GeocodingNotifier;
import gilm.lecture.verb.UtilsG.GetCurrentLocation;
import gilm.lecture.verb.UtilsG.Navigator;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.TitleNavigator;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.UtilsG.WebInnerStackModel;
import gilm.lecture.verb.WebServices.AdsModel;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.Widget.AutoScrollViewPager;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * <b>Home Screen</b> with navigation drawer.
 */
public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static Navigator navigator;
    private Toolbar toolbar;
    AutoScrollViewPager viewPager;
    boolean showSearchIcon = true,
            showHomeIcon = false;
    private Menu menu;
    private TextView toolbar_title;
    private boolean secondOptions;
    private NavigationView navigationView;
    public FrameLayout layoutContainer;
    Fragment selectedFragment;
    MenuItem searchMenuItem, homeMenuItem;

    public static void start(Context context) {
        Intent starter = new Intent(context, NavigationActivity.class);
        context.startActivity(starter);
        UtillsG.finishAll(context);
    }

    SharedPrefHelper sharedPrefHelper;

    private SharedPrefHelper getLocalData() {
        if (sharedPrefHelper == null) {
            sharedPrefHelper = new SharedPrefHelper(getActivityG());
        }
        return sharedPrefHelper;
    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateMyLocation();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(NavigationActivity.this)) {
                DialogHelper.getInstance().showInformation(this, "A permission is needed to access all the features.", new CallBackG<String>() {
                    @Override
                    public void onCallBack(String output) {

                        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                        intent.setData(Uri.parse("package:" + getPackageName()));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
                return;
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                DialogHelper.getInstance().showInformation(this, "A permission is needed to access audio player's all features.", new CallBackG<String>() {
                    @Override
                    public void onCallBack(String output) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, Constants.RequestCode.MINIMIZED_PLAYER_PERMISSION);
                    }
                });
            }
        }
    }

    private void updateMyLocation() {
        GetCurrentLocation location = new GetCurrentLocation(NavigationActivity.this, new GeocodingNotifier<Location>() {
            @Override
            public void GeocodingDetails(Location geocodeLatLng) {
                if (geocodeLatLng != null) {
                    if (getActivityG() != null && new SharedPrefHelper(getActivityG()).getUserId() != null && !new SharedPrefHelper(getActivityG()).getUserId().trim().isEmpty()) {
                        updateDeviceDetails(geocodeLatLng);
                    }

                    if (getActivityG() != null) {
                        new SharedPrefHelper(getActivityG()).setMyLatitude(String.valueOf(geocodeLatLng.getLatitude()));
                        new SharedPrefHelper(getActivityG()).setMyLongitude(String.valueOf(geocodeLatLng.getLongitude()));
                    }
                } else {
                    // unable to fetch location details
                }
            }
        });
        if (location.mGoogleApiClient != null) {
            location.mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigtation);
        setupToolBar();
        viewPager = (AutoScrollViewPager) findViewById(R.id.viewPagerAd);
        EventBus.getDefault().register(this);

        layoutContainer = (FrameLayout) findViewById(R.id.layoutContainer);

        init();

        if (getLocalData().isRegistered()) {
            showHomeIcon = false;
            showSearchIcon = true;
            selectedFragment = HomeTabFragment.newInstance(getIntent().getIntExtra(Constants.Extras.tabNumber, 0));
            TitleNavigator.getInstance().homeTabScreen((HomeTabFragment) selectedFragment);
        } else {
            selectedFragment = SearchLectureFragment.newInstance();
            TitleNavigator.getInstance().searchScreen((SearchLectureFragment) selectedFragment);
        }

        if (!new SharedPrefHelper(getActivityG()).isWelcomeMessageSeen(getActivityG())) {
            DialogHelper.getInstance().showWelcomeDialog(getActivityG(), new DialogHelper.CallBackSingleInterface() {
                @Override
                public void onCompletion() {
                    disableNavigationViewScrollbars(navigationView);
                }
            });
            new SharedPrefHelper(getActivityG()).setWelcomeMessageSeen(getActivityG());
        }
    }

    public void updateDeviceDetails(Location geocodeLatLng) {

        try {
            Call<BasicApiModel> basicApiModelCall = getRetrofitInstance().update_device_details(
                    getOtherParams(new SharedPrefHelper(getActivityG()).getUserId())
                    , getOtherParams(Build.DEVICE)
                    , getOtherParams(UtillsG.getMacAddress(getActivityG()))
                    , getOtherParams(UtillsG.getWifiStrength(getActivityG()))
                    , getOtherParams("" + geocodeLatLng.getLatitude())
                    , getOtherParams("" + geocodeLatLng.getLongitude())
                    , getOtherParams("true"));
            basicApiModelCall.enqueue(new Callback<BasicApiModel>() {
                @Override
                public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {

                    if (response.body() != null) {
                        if (response.body().getStatus()) {
                        }
                    }
                }

                @Override
                public void onFailure(Call<BasicApiModel> call, Throwable t) {
                    Log.e("Error occured", t.getMessage().toString());
                }
            });
        } catch (Exception ex) {
            Log.e("Exception is", ex.toString());
        }
    }

    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    protected HomeApis getRetrofitInstance() {
        return LVApplication.getRetrofit().create(HomeApis.class);
    }

    private void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    private void setupToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
    }

    private void setTitle(String title) {
        if (toolbar_title != null) {
            toolbar_title.setText(title);
        }
    }

    private void init() {
        navigator = new Navigator();

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        this.menu = navigationView.getMenu();


        if (getLocalData().isRegistered()) {
            /**
             * Setting up menu according to user type
             */
            setMenuAccordingToUserType();
            /***/

            navigationView.getHeaderView(0).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    navigationView.getMenu().clear();
                    //                TitleNavigator.getInstance().ShowMyProfile(new MyProfileFragment());
                    //                drawer.closeDrawer(GravityCompat.START);
                    if (secondOptions) {
                        secondOptions = false;
                        setMenuAccordingToUserType();

                        view.findViewById(R.id.imgArrow).setRotation(0);
                    } else {
                        secondOptions = true;
                        navigationView.inflateMenu(R.menu.menu_home_2);
                        view.findViewById(R.id.imgArrow).setRotation(180);
                    }

                    menu = navigationView.getMenu();
                }
            });
        } else {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.activity_nav_drawer_unregister);
            showSearchIcon = false;
            supportInvalidateOptionsMenu();

        }
        setHeaderData();
        getAllAds();

    }

    private void setMenuAccordingToUserType() {
        navigationView.getMenu().clear();
        if (getLocalData().getUserType().equals(UserType.GUEST)) {
            navigationView.inflateMenu(R.menu.activity_navigtation_drawer);
        } else {
            navigationView.inflateMenu(R.menu.activity_navigtation_drawer_lecturer);
        }
    }

    private void setHeaderData() {
        View headerView = navigationView.getHeaderView(0);
        ImageLoader.setImageRoundSmall((ImageView) headerView.findViewById(R.id.imgProfile), getLocalData().getProfilePic());
        ImageLoader.setImageBig((ImageView) headerView.findViewById(R.id.imgProfileback), getLocalData().getCoverProfilePic());

        ((TextView) headerView.findViewById(R.id.tvName)).setText(getLocalData().getUserName());
        ((TextView) headerView.findViewById(R.id.tvEmail)).setText(getLocalData().getEmail());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (selectedFragment instanceof DiscoveryTabFragment) {
            ((DiscoveryTabFragment) selectedFragment).onbackPressed();
        } else {
            showEditDialog();
        }
    }

    public void showEditDialog() {
        DialogHelper.getInstance().exitDialog(getActivityG(), new CallBackG() {
            @Override
            public void onCallBack(Object output) {
                finish();
            }
        });
    }

    private Context getActivityG() {
        return NavigationActivity.this;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        selectedMenu = item;
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_lec_groups:
                // Handle the camera action


                setTitle(Title.LECTURE_GROUP_NETWORK);
                selectedFragment = GroupListFragment.newInstance();
                navigator.replaceFragment(this, R.id.layoutContainer, selectedFragment);

                break;
            case R.id.nav_fav:
                // Handle the camera action


                setTitle(Title.FAVORITES);
                selectedFragment = FavoritesFragment.newInstance();
                navigator.replaceFragment(this, R.id.layoutContainer, selectedFragment);

                break;
            case R.id.nav_my_profile:

               /* setTitle(Title.MY_PROFILE);
                selectedFragment = MyProfileFragment.newInstance(null,false);
                navigator.replaceFragment(this, R.id.layoutContainer, selectedFragment);*/
                openHomeTabsFragment(1);
                break;
            case R.id.nav_discovery:


                setTitle(Title.DISCOVERY);
                selectedFragment = DiscoveryTabFragment.newInstance();
                navigator.replaceFragment(this, R.id.layoutContainer, selectedFragment);
                PublicAudioPlayer.setReleaseAudioPlayer();

                break;
            case R.id.nav_connect:


                setTitle(Title.CONNECT);
                selectedFragment = ConnectTabFragment.newInstance();
                navigator.replaceFragment(this, R.id.layoutContainer, selectedFragment);

                break;
            case R.id.nav_lec_events:


                setTitle(Title.LECTURE_EVENTS);
                selectedFragment = LectureEventTabFragment.newInstance();
                navigator.replaceFragment(this, R.id.layoutContainer, selectedFragment);

                break;
            case R.id.nav_record:
//                setTitle(Title.RECORD);

                ChoosePostOptionActivity.start(NavigationActivity.this);
                PublicAudioPlayer.setReleaseAudioPlayer();

                break;
            case R.id.nav_setting:

                setTitle(Title.SETTINGS);
                selectedFragment = SettingsFragment.newInstance();
                navigator.replaceFragment(this, R.id.layoutContainer, selectedFragment);

                break;
            case R.id.nav_help:

                setTitle(Title.HELP);
                selectedFragment = HelpFragment.newInstance();
                navigator.replaceFragment(this, R.id.layoutContainer, selectedFragment);

                break;
            case R.id.nav_logout:

                DialogHelper.getInstance().logOutDialog(NavigationActivity.this, new CallBackG() {
                    @Override
                    public void onCallBack(Object output) {
                        UtillsG.showLoading("Please wait..", getActivityG());
                        Call<BasicApiModel> callApi = LVApplication.getRetrofit().create(LoginRegisterApi.class).logout(getLocalData().getUserId());
                        callApi.enqueue(new Callback<BasicApiModel>() {
                            @Override
                            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                                UtillsG.hideLoading();

                                if (response.body() != null && response.body().getStatus()) {
                                    getLocalData().logOut();
                                    SplashActivity.start(NavigationActivity.this);
                                } else {
                                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                                }
                            }

                            @Override
                            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                                UtillsG.hideLoading();
                            }
                        });
                    }
                });

                break;

            default:

                SubscriptionBaseActivity.start(getActivityG());
                navigationView.getHeaderView(0).performClick();
                break;
        }
        if (id == R.id.nav_my_profile) {
            showHomeIcon = false;
            showSearchIcon = true;

        } else {
            showHomeIcon = true;
            /*        if (getLocalData().isRegistered()) {*/
            if (id == R.id.nav_lec_events) {
                showSearchIcon = true;
                supportInvalidateOptionsMenu();
            } else {
                showSearchIcon = false;
                //always show search button.
                supportInvalidateOptionsMenu();
            }
        }
/*
        }
*/


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_home, menu);

        searchMenuItem = menu.findItem(R.id.search);
        homeMenuItem = menu.findItem(R.id.home);

        if (showSearchIcon) {
            searchMenuItem.setVisible(true);
        } else {
            searchMenuItem.setVisible(false);
        }

        if (showHomeIcon) {
            homeMenuItem.setVisible(true);
        } else {
            homeMenuItem.setVisible(false);
        }


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.home) {
            if (selectedFragment instanceof SearchLectureFragment) {
                UtillsG.showInterstitialAds(getActivityG(), getActivityG().getResources().getString(R.string.userAppId), getActivityG().getResources().getString(R.string.nonStudentAdUnitDiscoveryupload));


            }
            openHomeTabsFragment(0);

            return true;
        } else if (item.getItemId() == R.id.search) {
            if (selectedMenu != null) {
                selectedMenu.setChecked(false);
            }

            if (selectedMenu != null && selectedMenu.getItemId() == R.id.nav_lec_events) {
                showSearchIcon = true;
                showHomeIcon = true;

                SearchLectureEventsActivity.startActivity((Activity) getActivityG(), "");
            } else {
                selectedFragment = SearchLectureFragment.newInstance();
                TitleNavigator.getInstance().searchScreen((SearchLectureFragment) selectedFragment);
                showSearchIcon = false;
                showHomeIcon = true;
                supportInvalidateOptionsMenu();
            }

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void openHomeTabsFragment(int tabNumber) {
        if (selectedMenu != null) {
            selectedMenu.setChecked(false);
        }
        selectedMenu = null;
        selectedFragment = HomeTabFragment.newInstance(tabNumber);
        TitleNavigator.getInstance().homeTabScreen((HomeTabFragment) selectedFragment);
        showSearchIcon = true;
        showHomeIcon = false;
        supportInvalidateOptionsMenu();
    }

    MenuItem selectedMenu;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateTitle(NavigationBus navigationBus) {
        setTitle(navigationBus.getTitle());

        if (navigationBus.getFragment() != null) {
            navigator.replaceFragment(this, R.id.layoutContainer, navigationBus.getFragment());
        }


        switch (navigationBus.getTitle()) {
            case Title.HELP:
                selectedMenu = (menu.findItem(R.id.nav_help)).setChecked(true);
                break;
        /*    case Title.MY_PROFILE:
                selectedMenu = (menu.findItem(R.id.nav_my_profile)).setChecked(true);
                break;*/
            case Title.HOME:
                if (selectedMenu != null) {
                    selectedMenu.setChecked(false);
                }
                break;
            case Title.ALERT_NOTIFICATION:
                break;
            case Title.CONNECT:
                selectedMenu = menu.findItem(R.id.nav_connect).setChecked(true);
                break;
            case Title.DISCOVERY:
                selectedMenu = menu.findItem(R.id.nav_discovery).setChecked(true);
                break;
            case Title.FAVORITES:
                selectedMenu = menu.findItem(R.id.nav_fav).setChecked(true);
                break;
            case Title.LECTURE_EVENTS:
                selectedMenu = menu.findItem(R.id.nav_lec_events).setChecked(true);
                break;
            case Title.SETTINGS:
                selectedMenu = menu.findItem(R.id.nav_setting).setChecked(true);
                break;

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void profileUpdated(ProfileUpdateBus data) {
        setHeaderData();
    }


    /**
     * This method will be called if the internal fragments will call an activityforresult , then the result will come here.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                if (selectedFragment instanceof DiscoveryTabFragment) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    Uri resultUri = result.getUri();
                    ((DiscoveryTabFragment) selectedFragment).onActivityResultImageCropper(resultUri);
                }
            } else if (requestCode == Constants.RequestCode.WEBVIEW_RECORDING) {
                if (selectedFragment instanceof DiscoveryTabFragment) {
                    String filePath = data.getStringExtra(Constants.Extras.DATA);
                    String currentUrl = data.getStringExtra(Constants.Extras.currentUrl);
                    String TITLE = data.getStringExtra(Constants.Extras.TITLE);
                    ArrayList<WebInnerStackModel> alStack = (ArrayList<WebInnerStackModel>) data.getSerializableExtra(Constants.Extras.WEB_STACK);
                    ((DiscoveryTabFragment) selectedFragment).onActivityResultWebViewRecorder(filePath, currentUrl, TITLE, alStack, data.getStringExtra("time"));
                }
            } else if (requestCode == Constants.RequestCode.AUDIO_PLAYER) { // if the result is from audio player while playing the audio file
                if (selectedFragment instanceof HomeTabFragment) {
                    ((HomeTabFragment) selectedFragment).onActivityResultAudioPlayer(requestCode, resultCode, data);
                }
            }
        }
    }


    void searchLectureEventOption() {
        final Dialog dialog = new Dialog(getActivityG());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.inflater_lecture_event_search);

        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.TOP);


        dialog.show();

    }

    public void showLocationOffDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivityG());
        dialog.setTitle("GPS");
        dialog.setCancelable(false);
        dialog.setMessage("Your location service is still enabled. Would you like to disable it ?");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dialog.show();
    }

    private void getAllAds() {
        Call<AdsModel> call = LVApplication.getRetrofit().create(HomeApis.class).get_ads("navigation_ads");
        call.enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(Call<AdsModel> call, final Response<AdsModel> response) {
                if (response.body().getStatus()) {

                    if (!response.body().getList().isEmpty()) {
                        float adHeight = Float.parseFloat(UtillsG.getNotNullString(response.body().getList().get(0).getAd_height(), "5"));
                        float adWidth = Float.parseFloat(UtillsG.getNotNullString(response.body().getList().get(0).getAd_width(), "16"));
//                        UtillsG.setHeightWidthWithRatio_FrameLayout((NavigationActivity.this), viewPager, adHeight, adWidth);

                        viewPager.setVisibility(View.VISIBLE);
                        List<AdsModel> adsModels = new ArrayList<>();
                        adsModels = response.body().getList();
                        viewPager.setAdapter(new AdsPagerAdapter(getActivityG(), adsModels));
                        viewPager.startAutoScroll(2000);
                        viewPager.setStopScrollWhenTouch(true);
                        viewPager.setAutoScrollDurationFactor(17);


                    }
                } else {
                    viewPager.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<AdsModel> call, Throwable t) {
                UtillsG.showToast(t.getMessage(), getActivityG(), true);
            }
        });
    }

}
