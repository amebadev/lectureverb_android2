package gilm.lecture.verb.Features.Home.Notification;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Home.Notification.Details.NotificationDetailsActivity;
import gilm.lecture.verb.Features.LectureEvent.Details.LectureEventDetailsActivity;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments.TextLectureCommentsActivity;
import gilm.lecture.verb.Features.UserGroups.GroupDetails.GroupDetailsActivity;
import gilm.lecture.verb.Features.UserGroups.InvitationList.GroupInvitationListActivity;
import gilm.lecture.verb.Features.UserGroups.SingleGroupModel;
import gilm.lecture.verb.Features.UserGroups.UserGroupApis;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.DateHelper;
import gilm.lecture.verb.UtilsG.InfiniteAdapter_WithoutBuinding;
import gilm.lecture.verb.UtilsG.LoadingViewHolder;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.Web;
import gilm.lecture.verb.Widget.TextViewWithImages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 06 Jul 2017.
 */

public class NotificationAdapter extends InfiniteAdapter_WithoutBuinding<NotificationAdapter.MyViewHolderG> {
    private LayoutInflater inflater;

    private Context con;

    private List<NotificationViewModel> dataList;


    public NotificationAdapter(Context context, List<NotificationViewModel> dList) {
        inflater = LayoutInflater.from(context);
        this.dataList = dList;
        con = context;
    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        return new LoadingViewHolder(inflater.inflate(R.layout.layout_progress, parent, false));
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }


    @Override
    public NotificationAdapter.MyViewHolderG onCreateView(ViewGroup parent, int viewType) {
        return new NotificationAdapter.MyViewHolderG(inflater.inflate(R.layout.inflate_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        try {
            if (!(holder instanceof LoadingViewHolder)) {

                String title = "";
                switch (dataList.get(position).getFlag()) {
                    case "favourite":
                        title = "Liked";
                        break;
                    case "follow":
                        title = "Follow request";
                        break;
                    case "follow_direct":
                        title = "Followed";
                        break;
                    case "follow_approved":
                        title = "Request accepted";
                        break;
                    case "unfollow":
                        title = "Unfollowed";
                        break;
                    case "external_share":
                        title = "Shared";
                        break;
                    case "group_invitation":
                        title = "Invitation";
                        break;
                    case "added_to_group":
                        title = "Added to Group";
                        break;
                }


                NotificationAdapter.MyViewHolderG mHolder = ((NotificationAdapter.MyViewHolderG) holder);
                final NotificationViewModel mNotificationModel = dataList.get(position);

                mHolder.textImages_msg.setText(mNotificationModel.getMessage());
                mHolder.txtv_time.setText(mNotificationModel.getTime());
                mHolder.txtv_date_time.setText(DateHelper.convertFromServerDateToRequiredDate(mNotificationModel.getTime(), "dd-MMM HH:mm a"));


                if (!UtillsG.getNotNullString(dataList.get(position).getImageUrl(), "").isEmpty()) {
                    ImageLoader.setImageRoundSmall(mHolder.img_userImage, dataList.get(position).getImageUrl());
                } else {
                    mHolder.img_userImage.setImageResource(R.mipmap.ic_launcher);
                }

                if (dataList.get(position).getmNotificationDetails().getIs_viewed().equals("N")) {
                    mHolder.llMain.setBackgroundColor(con.getResources().getColor(R.color.cGreen));
                } else {
                    mHolder.llMain.setBackground(con.getResources().getDrawable(R.drawable.default_btn_selector));
                }

                if (dataList.get(position).getFlag().equalsIgnoreCase("follow")) {
                    ((MyViewHolderG) holder).llFollowButtons.setVisibility(View.VISIBLE);
                    ((MyViewHolderG) holder).txtvAccept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            acceptRejectRequest(true, dataList.get(position).getmNotificationDetails().getUserData().getUser_id(), position);
                        }
                    });
                    ((MyViewHolderG) holder).txtvReject.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            acceptRejectRequest(true, dataList.get(position).getmNotificationDetails().getUserData().getUser_id(), position);
                        }
                    });
                } else if (dataList.get(position).getFlag().equalsIgnoreCase("follow_direct")) {
                    ((MyViewHolderG) holder).llFollowButtons.setVisibility(View.GONE);
                } else {
                    ((MyViewHolderG) holder).llFollowButtons.setVisibility(View.GONE);
                }

                final String finalTitle = title;
                mHolder.ll_notificationClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Call<BasicApiModel> markNotificationRead = LVApplication.getRetrofit().create(HomeApis.class).mark_notification_read
                                (dataList.get(position).getmNotificationDetails().getId());
                        markNotificationRead.enqueue(new Callback<BasicApiModel>() {
                            @Override
                            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                                dataList.get(position).getmNotificationDetails().setIs_viewed("Y");
                                notifyDataSetChanged();
                            }

                            @Override
                            public void onFailure(Call<BasicApiModel> call, Throwable t) {

                            }
                        });

                        if (dataList.get(position).getmNotificationDetails().getNotify_id_table().equals("lv_quick_lecture")) {
                            if (mNotificationModel.getFlag().equals(Web.NotificationList_Type.comment)) {
                                TextLectureCommentsActivity.start(con, dataList.get(position).getmNotificationDetails().getQuick_lecture());
                            } else {
                                NotificationDetailsActivity.start(con, dataList.get(position).getmNotificationDetails());
                            }
                        } else if (dataList.get(position).getmNotificationDetails().getNotify_id_table().equals("lv_events")) {
                            LectureEventDetailsActivity.startActivityWithApiCall(con, dataList.get(position).getmNotificationDetails().getNotify_id(), v);
                        } else if (dataList.get(position).getmNotificationDetails().getNotify_id_table().equals("lv_users")) {
                            if (dataList.get(position).getFlag().equalsIgnoreCase("follow")) {
                                ActivityOtherUserDetails.startForFollow(
                                        con,
                                        dataList.get(position).getmNotificationDetails().getUserData().getUser_id(),
                                        finalTitle + " by " + dataList.get(position).getmNotificationDetails().getUserData().getFull_name(),
                                        dataList.get(position).getmNotificationDetails().getId());
                            } else {
                                ActivityOtherUserDetails.start(
                                        con,
                                        dataList.get(position).getmNotificationDetails().getUserData().getUser_id(),
                                        finalTitle + " by " + dataList.get(position).getmNotificationDetails().getUserData().getFull_name());
                            }
                        } else if (dataList.get(position).getmNotificationDetails().getType().equals("added_to_group")) {
                            Call<SingleGroupModel> callGroup = LVApplication.getRetrofit().create(UserGroupApis.class).get_single_group(dataList.get(position).getmNotificationDetails().getNotify_id());
                            callGroup.enqueue(new Callback<SingleGroupModel>() {
                                @Override
                                public void onResponse(Call<SingleGroupModel> call, Response<SingleGroupModel> response) {
                                    if (response.body().getStatus()) {
                                        GroupDetailsActivity.start(con, response.body().getData());
                                    } else {
                                        UtillsG.showToast(response.body().getMessage() + "", con, true);
                                    }
                                }

                                @Override
                                public void onFailure(Call<SingleGroupModel> call, Throwable t) {
                                    UtillsG.showToast(t.getMessage() + "", con, true);
                                }
                            });
                        } else if (dataList.get(position).getmNotificationDetails().getType().equals("group_invitation")) {
                            GroupInvitationListActivity.start(con);

                        }
                    }
                });

                if (mNotificationModel.isFavorite()) {
                    mHolder.imgBtn_fav.setColorFilter(ContextCompat.getColor(con, R.color.redTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
                } else {
                    mHolder.imgBtn_fav.setColorFilter(ContextCompat.getColor(con, R.color.dialogBackgroundColor), android.graphics.PorterDuff.Mode.MULTIPLY);
                }

                if (mNotificationModel.isReposted()) {
                    mHolder.imgBtn_refresh.setColorFilter(ContextCompat.getColor(con, R.color.redTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
                } else {
                    mHolder.imgBtn_refresh.setColorFilter(ContextCompat.getColor(con, R.color.dialogBackgroundColor), android.graphics.PorterDuff.Mode.MULTIPLY);
                }

                if (mNotificationModel.isCommented()) {
                    mHolder.imgBtn_.setColorFilter(ContextCompat.getColor(con, R.color.redTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
                } else {
                    mHolder.imgBtn_.setColorFilter(ContextCompat.getColor(con, R.color.dialogBackgroundColor), android.graphics.PorterDuff.Mode.MULTIPLY);
                }


            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static Drawable changeDrawableColor(Context context, int icon, int newColor) {
        Drawable mDrawable = ContextCompat.getDrawable(context, icon).mutate();
        mDrawable.setColorFilter(new PorterDuffColorFilter(newColor, PorterDuff.Mode.SRC_IN));
        return mDrawable;
    }

    private void acceptRejectRequest(final boolean isAccepted, String userId, final int position) {
        UtillsG.showLoading("Please wait..", con);
        Call<BasicApiModel> getFollowersList = LVApplication.getRetrofit().create(ProfileApi.class).approve_reject_follow_request(new SharedPrefHelper(con).getUserId(), userId, String.valueOf(isAccepted));
        getFollowersList.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                UtillsG.hideLoading();
                if (response.body().getStatus()) {
                    executeDeleteNotification(dataList.get(position).getmNotificationDetails().getId());
                    UtillsG.showToast(isAccepted ? "Request accepted." : "Request rejected.", con, true);
                    dataList.remove(position);
                    notifyDataSetChanged();
                } else {
                    UtillsG.showToast(response.body().getMessage(), con, true);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                UtillsG.showToast(t.getMessage(), con, true);
                UtillsG.hideLoading();
            }
        });
    }


    class MyViewHolderG extends RecyclerView.ViewHolder {
        View view;
        TextView txtv_time, txtv_date_time, txtvAccept, txtvReject;
        ImageView imgBtn_fav, imgBtn_refresh, img_userImage, imgBtn_;
        LinearLayout ll_notificationClick, llFollowButtons, llMain;
        TextViewWithImages textImages_msg;


        public MyViewHolderG(View row) {
            super(row);

            view = row;
            llMain = (LinearLayout) view.findViewById(R.id.llMain);
            llFollowButtons = (LinearLayout) view.findViewById(R.id.llFollowButtons);
            ll_notificationClick = (LinearLayout) view.findViewById(R.id.ll_notificationClick);
            imgBtn_fav = (ImageView) view.findViewById(R.id.imgBtn_fav);
            textImages_msg = (TextViewWithImages) view.findViewById(R.id.textImages_msg);
            txtv_time = (TextView) view.findViewById(R.id.txtv_time);
            txtvReject = (TextView) view.findViewById(R.id.txtvReject);
            txtvAccept = (TextView) view.findViewById(R.id.txtvAccept);
            txtv_date_time = (TextView) view.findViewById(R.id.txtv_date_time);
            img_userImage = (ImageView) view.findViewById(R.id.img_userImage);
            imgBtn_refresh = (ImageView) view.findViewById(R.id.imgBtn_refresh);
            imgBtn_ = (ImageView) view.findViewById(R.id.imgBtn_);

        }
    }

    private void executeDeleteNotification(String notificationId) {
        UtillsG.showLoading("Please wait..", con);
        Call<BasicApiModel> deleteNotification = LVApplication.getRetrofit().create(ProfileApi.class).delete_notification(notificationId);
        deleteNotification.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                UtillsG.hideLoading();
                Log.e("DELETE NOTIFICATION :", response.body().toString());
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                UtillsG.hideLoading();
            }
        });
    }


}
