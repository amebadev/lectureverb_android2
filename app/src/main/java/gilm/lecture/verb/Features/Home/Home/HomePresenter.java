package gilm.lecture.verb.Features.Home.Home;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.Settings.ArrayListLectureModel;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.IsLoadingViewModel;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 06 Jul 2017.
 */

public class HomePresenter extends BasePresenter<HomeView> implements HomePresenterBinder, CallBackG<Object>
{

    public IsLoadingViewModel isLoadingViewModel;

    public IsLoadingViewModel getIsLoadingViewModel() {
        return isLoadingViewModel;
    }

    public HomePresenter() {
        isLoadingViewModel = new IsLoadingViewModel();
        isLoadingViewModel.setLoading(false);
    }


    public void loadHomeData(String user_id, int pageNumber) {

        isLoadingViewModel.setLoading(true);
        Call<ArrayListLectureModel> basicApiModelCall = getRetrofitInstance().get_posted_lectures(user_id, String.valueOf(pageNumber));
        basicApiModelCall.enqueue(new Callback<ArrayListLectureModel>()
        {
            @Override
            public void onResponse(Call<ArrayListLectureModel> call, Response<ArrayListLectureModel> response) {

                if( getView()!=null) {

                    Log.e("get_posted_lectures : ", String.valueOf(response.body().getListOfTagsModel()));

                    List<Lecture_And_Event_Model> alModel = response.body().getListOfTagsModel();
                    ArrayList<HomeViewModel> list = new ArrayList<HomeViewModel>();

                    for (int i = 0; i < alModel.size(); i++) {
                        HomeViewModel innerModel = new HomeViewModel();

                        innerModel.setTime("1");
                        innerModel.setBackImageUrl(alModel.get(i).getThumbnail());
                        innerModel.setTitle(alModel.get(i).getQuick_lecture_text());
                        innerModel.setImageUrl("");
                        list.add(innerModel);
                    }

                    isLoadingViewModel.setLoading(false);
                    getView().showData(list);

                }
            }

            @Override
            public void onFailure(Call<ArrayListLectureModel> call, Throwable t) {

                if( getView()!=null) {
                    Log.e("ERROR get_posted_le", t.getMessage().toString());
                    isLoadingViewModel.setLoading(false);
                    UtillsG.showToast("An error occured.", getView().getActivityG(), false);
                }

            }
        });
    }

    protected HomeApis getRetrofitInstance() {
        return LVApplication.getRetrofit().create(HomeApis.class);
    }

    @Override
    public void onResume() {
        loadHomeData("26"/*new SharedPrefHelper(getActivityG()).getUserId()*/, 1);
        super.onResume();
    }

    @Override
    public void showDetailsWithAnim(View view) {
        ObjectAnimator rotateAnimator;
        ObjectAnimator animation;
        final View childAt = ((FrameLayout) view).getChildAt(0);
        final boolean visibleC = childAt.getVisibility() == View.VISIBLE;
        if (visibleC) {
            animation = ObjectAnimator.ofFloat(childAt, "rotationY", 360f, 180f);
            rotateAnimator = ObjectAnimator.ofFloat(childAt, View.ALPHA, 1, 0);
        }
        else {
            animation = ObjectAnimator.ofFloat(childAt, "rotationY", 180f, 360f);
            rotateAnimator = ObjectAnimator.ofFloat(childAt, View.ALPHA, 0, 1);
        }
        childAt.setVisibility(View.VISIBLE);

        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animation, rotateAnimator);
        animatorSet.setDuration(1000);
        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener()
        {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (visibleC) {
                    childAt.setVisibility(View.INVISIBLE);
                }
                else {
                    childAt.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

    }

    @Override
    public int getLayoutHeight() {
        return setHeightWidthWithRatio_FrameLayout(((Activity) getView().getActivityG()));
    }

    @Override
    public void play(View view, HomeViewModel data) {
//        PlayerTabActivity.start(getView().getActivityG(),new LectureModel());
    }

    public int setHeightWidthWithRatio_FrameLayout(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        float screenWidth = Math.round(display.getWidth());
        float imageHeight = 2f;
        float imageWidth = 5f;
        float target1 = imageHeight / imageWidth;
        float targetHeight = target1 * screenWidth;

        return Math.round(targetHeight);
    }

    @Override
    public void showOptions(View view, HomeViewModel data) {
        getView().showOptionsOnAdapterItem(view);
    }

    /**
     * @param itemId-id of clicked menu item.
     */
    public void adapterOptionClicked(Integer itemId) {

    }

    @Override
    public void onCallBack(Object output) {

    }
}

