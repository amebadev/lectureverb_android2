/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Player.CommentsApis;
import gilm.lecture.verb.Features.Player.CommentsReplyDailog;
import gilm.lecture.verb.Features.Player.PlayerActivity;
import gilm.lecture.verb.Features.Player.PublicAudioPlayer;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.BitmapDecoderG;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.EndlessRecyclerOnScrollListener;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.Web;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


/**
 * Created by G-Expo on 10 Aug 2017.
 */

public class CommentsFragment extends Fragment implements View.OnClickListener {

    private CommentAdapter commentAdapter;
    View rootView;
    RecyclerView recyclerView;
    EmojiconEditText edt_sendMessage;
    ImageView img_attachfile;
    ImageButton btn_sendMesg, btn_sendAudio;
    LinearLayoutManager linearLayoutManager;
    ArrayList<CommentsModel> mCommentsList = new ArrayList<>();
    int pageNumber = 1;
    CommentsReplyDailog commentsReplyDailog;
    public static Lecture_And_Event_Model quick_lecture_id;
    Activity mActivity;
    Uri selectedFilePath = null;
    String Extension_file = "";
    String videoThumbnailPath = "";
    Uri videoThumbNailUri = null;
    Boolean isMoreDataToLoad = true;
    ImageView emoji_btn;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    EmojIconActions emojIcon;
    ProgressBar progress_bar;

    public static CommentsFragment newInstance(Lecture_And_Event_Model lecture_id) {
//        Bundle args = new Bundle();

        quick_lecture_id = lecture_id;
        CommentsFragment fragment = new CommentsFragment();
//        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_comment, container, false);
        commentsReplyDailog = new CommentsReplyDailog(getActivity());
        mActivity = getActivity();
        initViews();

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        commentAdapter.releaseAudioPlayer();
    }

    public void initViews() {
        progress_bar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        edt_sendMessage = (EmojiconEditText) rootView.findViewById(R.id.edt_sendMessage);
        emoji_btn = (ImageView) rootView.findViewById(R.id.emoji_btn);
        (btn_sendMesg = (ImageButton) rootView.findViewById(R.id.btn_sendMesg)).setOnClickListener(this);
        (btn_sendAudio = (ImageButton) rootView.findViewById(R.id.btn_sendAudio)).setOnClickListener(this);
        (img_attachfile = (ImageView) rootView.findViewById(R.id.img_attachfile)).setOnClickListener(this);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        emojIcon = new EmojIconActions(getActivity(), rootView, edt_sendMessage, emoji_btn);
        emojIcon.ShowEmojIcon();
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e("Keyboard", "open");
            }

            @Override
            public void onKeyboardClose() {
                Log.e("Keyboard", "close");
            }
        });


        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true);
        recyclerView.setLayoutManager(linearLayoutManager);
        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (isMoreDataToLoad) {
                    progress_bar.setVisibility(View.VISIBLE);
                    pageNumber++;
                    getCommentsList(pageNumber);
                    Log.e("PAGE NUMBER = ", pageNumber + "");
                }
            }

            @Override
            public void countChange(int currentVisibleCount) {

            }
        };
        recyclerView.setOnScrollListener(endlessRecyclerOnScrollListener);
        commentAdapter = new CommentAdapter(mCommentsList, getActivity(), this);
        recyclerView.setAdapter(commentAdapter);
        getCommentsList(pageNumber);

    }


    public void getCommentsList(final int pageNo) {
        Log.e("PAGE NUMBER = ", pageNumber + "");

        Call<CommentsList> quickLectureApiModelCall = LVApplication.getRetrofit().create(CommentsApis.class).comments_list(
                new SharedPrefHelper(getActivity()).getUserId()
                , quick_lecture_id.getQuick_lecture_id()
                , "" + pageNo
        );
        quickLectureApiModelCall.enqueue(new Callback<CommentsList>() {
            @Override
            public void onResponse(Call<CommentsList> call, final Response<CommentsList> response) {

                progress_bar.setVisibility(View.GONE);

                if (response.body() != null) {

                    if (response.body().getStatus()) {
                        isMoreDataToLoad = true;
                        if (pageNo == 1) {
                            mCommentsList.clear();
                        }

                        if (response.body().getData() != null) {
                            mCommentsList.addAll(0, response.body().getData());
                            recyclerView.getAdapter().notifyDataSetChanged();

//                            recyclerView.smoothScrollToPosition(mCommentsList.size() - 1);

                            edt_sendMessage.setText("");
                            Extension_file = "";
                            selectedFilePath = null;
                            edt_sendMessage.setText("");
                            img_attachfile.setImageResource(R.mipmap.ic_clipboard);
                        }
                    } else {
                        isMoreDataToLoad = false;

                    }
                }
            }

            @Override
            public void onFailure(Call<CommentsList> call, Throwable t) {
                progress_bar.setVisibility(View.GONE);
                Log.e("Error", t.toString());
                UtillsG.showToast("Error in creating comment on quick lecture", getActivity(), true);
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sendMesg:
                if (selectedFilePath == null && edt_sendMessage.getText().toString().trim().isEmpty()) {
                    UtillsG.showToast("Select attached or enter comment to send", mActivity, true);
                } else {


                    commentsReplyDailog.PostLectureComment(getActivity(),
                            selectedFilePath != null ? selectedFilePath.getPath() : "",
                            quick_lecture_id.getQuick_lecture_id(), UtillsG.encodeEmoji(edt_sendMessage.getText().toString()),
                            selectedFilePath != null ? "f" : "t", Extension_file,
                            videoThumbNailUri != null ? videoThumbNailUri.getPath() : "", null,
                            new CallBackG<CommentsModel>() {
                                @Override
                                public void onCallBack(CommentsModel output) {
                                    pageNumber = 1;
                                    endlessRecyclerOnScrollListener.reset();
                                    getCommentsList(pageNumber);
                                    EventBus.getDefault().post(new HomeFragmentUpdateBus(true));

                                    if (getActivity() instanceof PlayerActivity) {
                                        int newCount = Integer.parseInt(quick_lecture_id.getComments_count()) + 1;
                                        quick_lecture_id.setComments_count("" + newCount);
                                        ((PlayerActivity) getActivity()).lectureModel = quick_lecture_id;
                                    }
                                }
                            });
                }
                break;
            case R.id.btn_sendAudio:

                if (getActivity() instanceof PlayerActivity && ((PlayerActivity) getActivity()).isAudioMode) {
                    if (!((PlayerActivity) getActivity()).playeEnd) {
                        if (!PublicAudioPlayer.getInstance().getAudioPlayer().isPause()) {
                            ((PlayerActivity) getActivity()).playBtn.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
                            PublicAudioPlayer.getInstance().getAudioPlayer().setPause(true);
                        }
                    }
                } else if (getActivity() instanceof PlayerActivity && !((PlayerActivity) getActivity()).isAudioMode) {
                    if (((PlayerActivity) getActivity()).getVideoFragment().textureView.isPlaying()) {
                        ((PlayerActivity) getActivity()).getVideoFragment().onPause();
                    }
                }

                if (commentAdapter.audioPlayer != null && !commentAdapter.audioPlayer.isPause() && !commentAdapter.playeEnd) {
                    commentAdapter.audioPlayer.stop();
                    if (commentAdapter.publicImgvPlayAudio != null && commentAdapter.publicSeekbarAudio != null) {
                        commentAdapter.publicImgvPlayAudio.setImageDrawable(getResources().getDrawable(R.mipmap.ic_play));
                        commentAdapter.publicSeekbarAudio.setProgress(0);
                    }
                }

                commentsReplyDailog.showRecordingDialog(quick_lecture_id, edt_sendMessage.getText().toString().trim().isEmpty() ? "" :
                        UtillsG.encodeEmoji(edt_sendMessage.getText().toString()), new CallBackG<CommentsModel>()

                {
                    @Override
                    public void onCallBack(CommentsModel output) {
                        pageNumber = 1;
                        endlessRecyclerOnScrollListener.reset();
                        getCommentsList(pageNumber);
                        EventBus.getDefault().post(new HomeFragmentUpdateBus(true));

                        if (getActivity() instanceof PlayerActivity) {
                            int newCount = Integer.parseInt(quick_lecture_id.getComments_count()) + 1;
                            quick_lecture_id.setComments_count("" + newCount);
                            ((PlayerActivity) getActivity()).lectureModel = quick_lecture_id;
                        }
                    }
                });
                break;
            case R.id.img_attachfile:
                BitmapDecoderG.selectImageForCommentsAndChat(mActivity, CommentsFragment.this);

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);

                selectedFilePath = result.getUri();
                img_attachfile.setImageResource(R.mipmap.ic_clipborad_red);
                Extension_file = Web.File_Options.IMAGE;

            } else if (requestCode == 1011) {

                selectedFilePath = Uri.fromFile(new File(data.getExtras().getString("file_path")));
                Extension_file = data.getExtras().getString("Document_type");
                img_attachfile.setImageResource(R.mipmap.ic_clipborad_red);


                if (Extension_file.equals(Web.File_Options.VIDEO)) {
                    Bitmap bmp;
                    bmp = ThumbnailUtils.createVideoThumbnail(Uri.fromFile(new File(data.getExtras().getString("file_path"))).getPath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                    videoThumbNailUri = Uri.fromFile(getFileonSDCard("thumbnail.png"));
                    videoThumbnailPath = getFileonSDCard("thumbnail.png").getPath();

                    FileOutputStream out = null;
                    try {
                        out = new FileOutputStream(getFileonSDCard("thumbnail.png"));
                        bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (out != null) {
                                out.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                if (resultCode != RESULT_OK) {
                    return;
                }

                cropMeImage(BitmapDecoderG.onActivityResult(mActivity, requestCode, resultCode, data));

            }


        }

    }


    public static File getFileonSDCard(String nameWithXtension) {
        File file = null;

        try {
            //create file
            file = new File(Environment.getExternalStorageDirectory() + File.separator, nameWithXtension);
            file.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;

    }


    public String getRealPathFromURIVideo(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    public void cropMeImage(Uri uri) {
        CropImage.activity(uri)
                .setAspectRatio(2, 2)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(mActivity, this);
    }

}
