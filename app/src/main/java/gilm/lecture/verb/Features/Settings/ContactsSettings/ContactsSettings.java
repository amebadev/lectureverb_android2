package gilm.lecture.verb.Features.Settings.ContactsSettings;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import com.google.android.gms.appinvite.AppInviteInvitation;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.databinding.ActivityContactsSettingsBinding;

public class ContactsSettings extends BaseActivity<ActivityContactsSettingsBinding, ContactsSettingsPresenter> implements ContactsSettingsView
{

    public static void start(Context context) {
        Intent starter = new Intent(context, ContactsSettings.class);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_contacts_settings;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new ContactsSettingsPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        setupToolbar("Contacts");

        getDataBinder().txtvInvite.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.app_name))
                        .setMessage("Hi, I am inviting you to install and use Lecture Verb App.")
                        .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))
//                        .setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
                        .setCallToActionText(getString(R.string.invitation_cta))
                        .build();
                startActivityForResult(intent, Constants.RequestCode.FIREBASE_INVITE);
            }
        });
    }

    @Override
    public Context getActivityG() {
        return ContactsSettings.this;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.RequestCode.FIREBASE_INVITE) {
            if (resultCode == RESULT_OK) {
                // Get the invitation IDs of all sent messages
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                for (String id : ids) {
                    Log.e("FIREBASE INVITE RESULT", "onActivityResult: sent invitation " + id);
                }
            }
            else {
                // Sending failed or it was canceled, show failure message to the user
                // ...
            }
        }
    }
}
