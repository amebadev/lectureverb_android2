package gilm.lecture.verb.Features.UserGroups.GroupDetails.GroupInfo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.WebServices.LVApplication;

/**
 * created by PARAMBIR SINGH on 10/10/17.
 */

public class GroupInfoAdapter extends RecyclerView.Adapter<GroupInfoAdapter.MyViewHolderG>
{


    private final LayoutInflater inflater;
    private List<UserDataModel.UserData> alData = new ArrayList<>();
    private boolean isFollowing, isMyProfile;
    private Context context;
    GroupInfoFragment groupInfoFragment;

    public GroupInfoAdapter(Context context, List<UserDataModel.UserData> alData, boolean isFollowing, boolean isMyProfile, GroupInfoFragment groupInfoFragment) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.alData = alData;
        this.isFollowing = isFollowing;
        this.isMyProfile = isMyProfile;
        this.groupInfoFragment = groupInfoFragment;
    }

    @Override
    public MyViewHolderG onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolderG(inflater.inflate(R.layout.inflater_followers_followings, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolderG holder, final int position) {
        ImageLoader.setImageRoundSmall(holder.imgvUserImage, alData.get(position).getProfile_pic());
        holder.txtvUserName.setText(alData.get(position).getFull_name());

        holder.txtvFollowUnFollow.setVisibility(View.GONE);


        holder.imgvDelete.setVisibility(View.GONE);


        holder.view.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                ActivityOtherUserDetails.start(context, alData.get(position).getUser_id(), "User Profile");
            }
        });


    }

    @Override
    public int getItemCount() {
        return alData.size();
    }

    class MyViewHolderG extends RecyclerView.ViewHolder
    {
        View view;
        ImageView imgvUserImage, imgvDelete;
        TextView txtvUserName, txtvFollowUnFollow;

        public MyViewHolderG(View row) {
            super(row);

            view = row;
            imgvDelete = (ImageView) view.findViewById(R.id.imgvDelete);
            imgvUserImage = (ImageView) view.findViewById(R.id.imgvUserImage);
            txtvUserName = (TextView) view.findViewById(R.id.txtvUserName);
            txtvFollowUnFollow = (TextView) view.findViewById(R.id.txtvFollowUnFollow);
        }
    }

    private ProfileApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(ProfileApi.class);
    }
}
