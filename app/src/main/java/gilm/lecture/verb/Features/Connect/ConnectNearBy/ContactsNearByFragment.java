/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Connect.ConnectNearBy;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

import gilm.lecture.verb.BuildConfig;
import gilm.lecture.verb.Features.Connect.ConnectNearBy.UsersList.UsersListActivity;
import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.Followers.ListOfUserModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.GeocodingNotifier;
import gilm.lecture.verb.UtilsG.GetCurrentLocation;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.Web;
import gilm.lecture.verb.databinding.FragmentContactsNearbyBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.request.RequestOptions.circleCropTransform;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsNearByFragment extends BaseFragment<FragmentContactsNearbyBinding, ContactNearByPresenter> implements ContactNearByView, InfiniteAdapterG.OnLoadMoreListener, OnMapReadyCallback
{

    public ArrayList<UserDataModel.UserData> alData = new ArrayList<>();
    private SupportMapFragment fragment;
    GoogleMap googleMap;

    HashMap<Marker, UserDataModel.UserData> modelDAta = new HashMap<>();

    boolean isMyProfileSet = false;

    int maxRangeNow = 10;

    String latitude = "", longitude = "";

    public static ContactsNearByFragment newInstance() {
        return new ContactsNearByFragment();
    }

    public ContactsNearByFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        if (googleMap != null) {
            loadMApDAta(false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (UtillsG.isGpsEnabled(getActivityG())) {
            if (getActivity() instanceof NavigationActivity) {
                ((NavigationActivity) getActivity()).showLocationOffDialog();
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_contacts_nearby;
    }

    @Override
    protected void onCreateFragmentG() {
        injectPresenter(new ContactNearByPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        if (getLocalData().getMaxKmRange() == 0) {
            getLocalData().setMaxKmRange(20);
        }

        ImageLoader.setImageRoundSmall(getDataBinder().imgvMyImage, getLocalData().getProfilePic());
        initializeGoogleMaps();

        getDataBinder().txtvCurrentMeters.setText(getLocalData().getMaxKmRange() + " Meters");
        getDataBinder().seekBAr.setProgress(getLocalData().getMaxKmRange() - 10);
        getDataBinder().seekBAr.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                int progress = i + 10;

                maxRangeNow = progress;
                getDataBinder().txtvCurrentMeters.setText(maxRangeNow + " Meters");

                getLocalData().setMaxKmRange(maxRangeNow);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
/*
                if (maxRangeNow < 10) {
                    maxRangeNow = 10;
                    getDataBinder().seekBAr.setProgress(maxRangeNow);
                    getDataBinder().txtvCurrentMeters.setText(maxRangeNow + " Meters");
                }
*/
                if (googleMap != null) {
                    loadMApDAta(true);
                }
            }
        });
    }

    @Override
    public void onLoadMore() {

    }

    private void initializeGoogleMaps() {
        try {
            fragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.googleMap);
            fragment.getMapAsync(this);

            getDataBinder().frameMap.setVisibility(View.INVISIBLE);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMapInstance) {
        googleMap = googleMapInstance;

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);

        googleMap.getUiSettings().setScrollGesturesEnabled(false);
//        googleMap.getUiSettings().setZoomGesturesEnabled(false);

        loadMApDAta(true);

        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener()
        {
            @Override
            public void onInfoWindowClick(Marker marker) {
                UserDataModel.UserData model = modelDAta.get(marker);
                if (model != null) {
                    ActivityOtherUserDetails.start(getActivity(), model, model.getIs_following().equals("true"));
                }
            }
        });
    }

    private void loadMApDAta(boolean showLoading) {

        if (!UtillsG.getNotNullString(latitude, "").isEmpty()) {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)), 20));
        }

        getDataBinder().txtvListView.setOnClickListener(null);

        if (showLoading) {
            getDataBinder().rippleBackGround.setVisibility(View.VISIBLE);
        }
        GetCurrentLocation location = new GetCurrentLocation(getActivity(), new GeocodingNotifier<Location>()
        {
            @Override
            public void GeocodingDetails(Location geocodeLatLng) {
                Log.e("Latitude", geocodeLatLng.getLatitude() + "");
                Log.e("Longitude", geocodeLatLng.getLongitude() + "");


                latitude = geocodeLatLng.getLatitude() + "";
                longitude = geocodeLatLng.getLongitude() + "";

                getLocalData().setMyLatitude(latitude);
                getLocalData().setMyLongitude(longitude);

                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(geocodeLatLng.getLatitude(), geocodeLatLng.getLongitude()), 20));

                getDataBinder().frameMap.setVisibility(View.VISIBLE);

                float range = getLocalData().getMaxKmRange() / 1000f;
                Call<ListOfUserModel> apiCall = getRetrofitInstance().getNearByUsers(getLocalData().getUserId(), getLocalData().getMyLatitude(), getLocalData().getMyLongitude(), String.valueOf(range));
                apiCall.enqueue(new Callback<ListOfUserModel>()
                {
                    @Override
                    public void onResponse(Call<ListOfUserModel> call, Response<ListOfUserModel> response) {

                        modelDAta.clear();

                        if (response.body().getStatus() && response.body().getUsersList() != null && !response.body().getUsersList().isEmpty()) {

                            Log.e("--- NEAR BY DATA SIZE", response.body().getUsersList().size() + "-------------------");

                            alData.clear();
                            alData.addAll(response.body().getUsersList());

                            // preparing button to show data in a listview
                            getDataBinder().txtvListView.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View view) {
                                    UsersListActivity.start(getActivity(), alData);
                                }
                            });


                            googleMap.clear();

                            for (int i = 0; i < alData.size(); i++) {
                                final UserDataModel.UserData model = alData.get(i);

                                new AsyncTask<Void, Void, Void>()
                                {
                                    @Override
                                    protected Void doInBackground(Void... voids) {
                                        addMarker(model);

                                        return null;
                                    }

                                    @Override
                                    protected void onPostExecute(Void aVoid) {
                                        super.onPostExecute(aVoid);
                                        getDataBinder().rippleBackGround.setVisibility(View.GONE);
                                    }
                                }.execute();
                            }
                        }
                        else {
                            Log.e("--- NEAR BY DATA SIZE", "---------  NO DATA ----------");

                        }
                    }

                    @Override
                    public void onFailure(Call<ListOfUserModel> call, Throwable t) {
                        Log.e("--- NEAR BY --- ERROR", t.getMessage());
                    }
                });
            }
        });
        if (location.mGoogleApiClient != null) {
            location.mGoogleApiClient.connect();
        }


    }

    synchronized private void addMarker(final UserDataModel.UserData userModel) {
        try {
            final String title, snippet;
            final String url;

            title = userModel.getFull_name();
            snippet = userModel.getEmail().equalsIgnoreCase("no@email.com") ? ""
                    : userModel.getEmail();

            if (!userModel.getProfile_pic().isEmpty()) {
                url = Web.Path.BASE_URL + userModel.getProfile_pic();
            }
            else {
                url = "";
            }

            new Handler(Looper.getMainLooper()).post(new Runnable()
            {
                @Override
                public void run() {
                    try {
                        if (!url.trim().isEmpty()) {
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.placeholder(R.drawable.loading_image_large_round);
                            requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
                            requestOptions.error(R.drawable.no_mediaround);

                            Glide.with(getActivity())
                                    .setDefaultRequestOptions(requestOptions)
                                    .asBitmap()
                                    .load(url)
                                    .apply(circleCropTransform().override(70, 70))
                                    .into(new SimpleTarget<Bitmap>()
                                    {
                                        @Override
                                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                            if (!userModel.getUser_id().equalsIgnoreCase(getLocalData().getUserId())) {
                                                BitmapDescriptor bd = BitmapDescriptorFactory.fromBitmap(getMarkerDrawable(resource));
                                                Marker marker = googleMap.addMarker(new MarkerOptions().title(title).snippet(snippet).icon(bd).position(new LatLng(Double.parseDouble(userModel.getLat()), Double.parseDouble(userModel.getLang()))));

                                                modelDAta.put(marker, userModel);
                                            }
                                            else {
                                                BitmapDescriptor bd = BitmapDescriptorFactory.fromBitmap(getMarkerDrawable(resource));
                                                Marker marker = googleMap.addMarker(new MarkerOptions().icon(bd).position(new LatLng(Double.parseDouble(userModel.getLat()), Double.parseDouble(userModel.getLang()))));

                                                modelDAta.put(marker, userModel);
                                            }
                                        }
                                    });
                        }
                        else {
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.placeholder(R.drawable.loading_image_large_round);
                            requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
                            requestOptions.error(R.drawable.no_mediaround);

                            Glide.with(getActivity())
                                    .setDefaultRequestOptions(requestOptions)
                                    .asBitmap()
                                    .load(R.mipmap.ic_launcher)
                                    .apply(circleCropTransform().override(70, 70))
                                    .into(new SimpleTarget<Bitmap>()
                                    {
                                        @Override
                                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                            if (!userModel.getUser_id().equalsIgnoreCase(getLocalData().getUserId())) {
                                                BitmapDescriptor bd = BitmapDescriptorFactory.fromBitmap(getMarkerDrawable(resource));
                                                Marker marker = googleMap.addMarker(new MarkerOptions().title(title).snippet(snippet).icon(bd).position(new LatLng(Double.parseDouble(userModel.getLat()), Double.parseDouble(userModel.getLang()))));

                                                modelDAta.put(marker, userModel);
                                            }
                                            else {
                                                BitmapDescriptor bd = BitmapDescriptorFactory.fromBitmap(getMarkerDrawable(resource));
                                                Marker marker = googleMap.addMarker(new MarkerOptions().icon(bd).position(new LatLng(Double.parseDouble(userModel.getLat()), Double.parseDouble(userModel.getLang()))));

                                                modelDAta.put(marker, userModel);
                                            }
                                        }
                                    });
                        }

                    }
                    catch (Exception ex) {
                        if (BuildConfig.DEBUG) {
                            Log.d("EXCEPTION :: ", ex.toString());
                        }
                    }
                }
            });
        }
        catch (Exception ex) {
            if (BuildConfig.DEBUG) {
                Log.d("EXCEPTION :: ", ex.toString());
            }
        }
    }


    private ProfileApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(ProfileApi.class);
    }

    private Bitmap getMarkerDrawable(Bitmap bmp) {
        Bitmap bm;
        try {
            View markerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.inflater_map_iamge, null);
            ImageView imgMarker = (ImageView) markerView.findViewById(R.id.imgv_ItemImage);
            imgMarker.setImageBitmap(bmp);
            markerView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            markerView.layout(0, 0, markerView.getMeasuredWidth(), markerView.getMeasuredHeight());
            markerView.setDrawingCacheEnabled(true);
            markerView.buildDrawingCache();
            bm = markerView.getDrawingCache();
        }
        catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.d("EXCEPTION :: ", e.toString());
            }
            return null;
        }
        return bm;
    }


}
