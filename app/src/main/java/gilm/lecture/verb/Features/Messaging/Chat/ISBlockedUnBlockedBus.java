package gilm.lecture.verb.Features.Messaging.Chat;

/**
 * Created by harpreet on 10/6/17.
 */

public class ISBlockedUnBlockedBus {
    private boolean isBlocked;
    private String userId;
    private boolean isChatMessage;

    public ISBlockedUnBlockedBus(boolean isBlocked,String userId,Boolean isChatMessage)
    {
        this.isBlocked = isBlocked;
        this.userId=userId;
        this.isChatMessage=isChatMessage;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isChatMessage() {
        return isChatMessage;
    }

    public void setChatMessage(boolean chatMessage) {
        isChatMessage = chatMessage;
    }
}
