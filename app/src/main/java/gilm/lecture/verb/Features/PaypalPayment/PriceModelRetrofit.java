package gilm.lecture.verb.Features.PaypalPayment;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * Created by Parambir Singh on 08-Jul-18.
 */

public class PriceModelRetrofit extends BasicApiModel
{
    List<PaymentPriceModel> data = new ArrayList<>();

    public List<PaymentPriceModel> getData()
    {
        return data;
    }

    public void setData(List<PaymentPriceModel> data)
    {
        this.data = data;
    }
}
