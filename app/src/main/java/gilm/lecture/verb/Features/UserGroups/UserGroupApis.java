/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.UserGroups;

import gilm.lecture.verb.Features.UserGroups.InvitationList.InvitationsListModel;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.Web;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by G-Expo on 17 Aug 2017.
 */

public interface UserGroupApis
{
    @FormUrlEncoded
    @POST(Web.Path.get_my_user_groups)
    Call<GroupListModel> get_my_user_groups(@Field(Web.Keys.USER_ID) String user_id, @Field(Web.Keys.page) String page);

    /**
     * @param user_id
     * @param page    starts with 1
     * @return
     */
    @FormUrlEncoded
    @POST(Web.Path.get_all_user_groups)
    Call<GroupListModel> get_all_user_groups(@Field(Web.Keys.USER_ID) String user_id, @Field(Web.Keys.page) String page);

    @FormUrlEncoded
    @POST(Web.Path.get_single_group)
    Call<SingleGroupModel> get_single_group(@Field(Web.Keys.group_id) String group_id);

    @FormUrlEncoded
    @POST(Web.Path.join_group)
    Call<BasicApiModel> join_group(@Field(Web.Keys.USER_ID) String user_id, @Field(Web.Keys.group_id) String group_id);

    @FormUrlEncoded
    @POST(Web.Path.leave_group)
    Call<BasicApiModel> leave_group(@Field(Web.Keys.USER_ID) String user_id, @Field(Web.Keys.group_id) String group_id);

    @FormUrlEncoded
    @POST(Web.Path.delete_group)
    Call<BasicApiModel> delete_group(@Field(Web.Keys.group_id) String group_id);

    @FormUrlEncoded
    @POST(Web.Path.search_all_user_groups)
    Call<GroupListModel> search_all_user_groups(@Field(Web.Keys.USER_ID) String user_id, @Field(Web.Keys.keyword) String keyword, @Field(Web.Keys.page) String page);

    @FormUrlEncoded
    @POST(Web.Path.save_group_payments)
    Call<BasicApiModel> save_group_payments(
            @Field(Web.Keys.USER_ID) String user_id,
            @Field(Web.Keys.group_id) String group_id,
            @Field(Web.Keys.amount) String amount,
            @Field(Web.Keys.payment_via) String payment_via);

    @FormUrlEncoded
    @POST(Web.Path.invite_user)
    Call<BasicApiModel> invite_user(
            @Field(Web.Keys.group_id) String group_id,
            @Field(Web.Keys.sender_id) String sender_id,
            @Field(Web.Keys.sender_is_admin) String sender_is_admin,
            @Field(Web.Keys.group_creater_id) String group_creater_id,
            @Field(Web.Keys.target_user_id) String target_user_id,
            @Field(Web.Keys.target_user_name) String target_user_name,
            @Field(Web.Keys.group_name) String group_name
    );

    @FormUrlEncoded
    @POST(Web.Path.get_pending_invitations)
    Call<InvitationsListModel> get_pending_invitations(
            @Field(Web.Keys.user_id) String user_id);

    @FormUrlEncoded
    @POST(Web.Path.approve_invite)
    Call<BasicApiModel> approve_invite(
            @Field(Web.Keys.user_id) String user_id,
            @Field(Web.Keys.group_id) String group_id,
    @Field(Web.Keys.sender_user_id) String sender_user_id,
    @Field(Web.Keys.group_name) String group_name);


}
