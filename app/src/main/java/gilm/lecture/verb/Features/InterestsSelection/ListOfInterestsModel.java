package gilm.lecture.verb.Features.InterestsSelection;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * created by PARAMBIR SINGH on 28/9/17.
 */

public class ListOfInterestsModel extends BasicApiModel
{
    @SerializedName("data")
    List<InterestsModel> categories;

    public List<InterestsModel> getCategories() {
        return categories;
    }

    public void setCategories(List<InterestsModel> categories) {
        this.categories = categories;
    }
}
