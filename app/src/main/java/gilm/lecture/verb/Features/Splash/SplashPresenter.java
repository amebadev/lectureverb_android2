package gilm.lecture.verb.Features.Splash;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;

/**
 * Created by G-Expo on 05 Jul 2017.
 */

public class SplashPresenter extends BasePresenter<SplashView> implements Runnable
{
    public void splashCheck() {
        new Handler().postDelayed(this, getView().splashTimer());
    }

    @Override
    public void onResume() {
        super.onResume();
        startUp();
    }

    @Override
    public void run() {
        //all login or other checks goes here..

        try {
            if (getView().getLocalData().getUserId().isEmpty()) {
                getView().showStartUpOptions();
            }
            else {
                if (getView().getLocalData().isVerified()) {
                    getView().openHome();
                }
                else {
                    getView().openVerificationScreen();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startUp() {
        if (ContextCompat.checkSelfPermission(getView().getActivityG(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getView().getActivityG(),
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getView().getActivityG(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED|| ContextCompat.checkSelfPermission(getView().getActivityG(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

// Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getView().getActivityG(),
                    Manifest.permission.READ_CONTACTS)
                    ||
                    ActivityCompat.shouldShowRequestPermissionRationale((Activity) getView().getActivityG(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    ||
                    ActivityCompat.shouldShowRequestPermissionRationale((Activity) getView().getActivityG(),
                            Manifest.permission.ACCESS_COARSE_LOCATION)
                    ||
                    ActivityCompat.shouldShowRequestPermissionRationale((Activity) getView().getActivityG(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                     ||
                    ActivityCompat.shouldShowRequestPermissionRationale((Activity) getView().getActivityG(),
                            Manifest.permission.CAMERA)
                    )

            {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getView().getActivityG());
                builder.setMessage("Allows Lecture Verb to access your contact and location information.")
                        .setCancelable(false)
                        .setPositiveButton("Allow", new DialogInterface.OnClickListener()
                        {
                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                ActivityCompat.requestPermissions((Activity) getView().getActivityG(),
                                        new String[]{Manifest.permission.READ_CONTACTS,
                                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                Manifest.permission.ACCESS_NETWORK_STATE,
                                                Manifest.permission.RECORD_AUDIO,
                                                Manifest.permission.ACCESS_FINE_LOCATION,
                                                Manifest.permission.CAMERA},
                                        11);

                            }
                        })
                        .setNegativeButton("Exit", new DialogInterface.OnClickListener()
                        {
                            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alert = builder.create();
                alert.show();

            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions((Activity) getView().getActivityG(),
                        new String[]{Manifest.permission.READ_CONTACTS,
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_NETWORK_STATE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.RECORD_AUDIO,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.CAMERA},
                        11);

            }

        }
        else {

            getView().permissionCheckDone();

        }
    }

}
