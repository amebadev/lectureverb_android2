/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Home.Notification.Details;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import gilm.lecture.verb.Features.Discovery.VideoRecording.WebViewRecorderActivity;
import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Home.Notification.NotificationSingleModel;
import gilm.lecture.verb.Features.Internal.Base.BaseActivityWithoutPresenter;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments.TextLectureCommentsActivity;
import gilm.lecture.verb.Features.Player.CommentsReplyDailog;
import gilm.lecture.verb.Features.Player.PlayerActivity;
import gilm.lecture.verb.Features.Player.PlayerTabActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DateHelper;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.Web;
import gilm.lecture.verb.Widget.TextViewWithImages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationDetailsActivity extends BaseActivityWithoutPresenter
{


    NotificationSingleModel singleNotificationDetails = null;
    ImageView imgvThumbnail, imgvUserImage, imgvOptions, imgvLike, imgv_userImage, imgv_repost, imgvRepostUserImage, img_notify_icon;
    TextView actxtvLectureTitle, txtvDuration;
    TextView txtvFavouriteCount, txtvCommentsCount, txtvRepostedCount, txtvTitleBig, txtvViewsCount, txtv_userName,
            txtv_following, txtv_followers, txtv_lectureDetails, txtv_followEvent, textv_userDetails, txtvTimeAgo;
    LinearLayout layPlay, linearMain, layLike, llRepost, ll_comments, ll_connect;
    FrameLayout frameLayout;
    LectureEventClicks mClicks = new LectureEventClicks();
    CommentsReplyDailog mCommentsReply;
    private UserDataModel.UserData userProfileModel;
    TextViewWithImages txtvUserName;


    public static void start(Context context, NotificationSingleModel mNotificationModel)
    {
        Intent starter = new Intent(context, NotificationDetailsActivity.class);
        starter.putExtra("singleNotificationDetails", mNotificationModel);
        context.startActivity(starter);
    }

    public static void start(Context context, Lecture_And_Event_Model mModel)
    {
        Intent starter = new Intent(context, NotificationDetailsActivity.class);

        // To show the details of quick lecture
        NotificationSingleModel mSingleNotification = new NotificationSingleModel();
        mSingleNotification.setType("QuickLectureDetails");
        mSingleNotification.setUserData(mModel.getUserdata());
        mSingleNotification.setQuick_lecture(mModel);

        starter.putExtra("singleNotificationDetails", mSingleNotification);
        context.startActivity(starter);
    }

    public void setupToolbar(String title)
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_details);
        mCommentsReply = new CommentsReplyDailog(NotificationDetailsActivity.this);

        singleNotificationDetails = (NotificationSingleModel) getIntent().getSerializableExtra("singleNotificationDetails");
        if (singleNotificationDetails != null && singleNotificationDetails.getType().equals(Web.NotificationList_Type.favourite))
        {
            setupToolbar("Liked by " + singleNotificationDetails.getUserData().getFull_name());
        } else if (singleNotificationDetails != null && singleNotificationDetails.getType().equals(Web.NotificationList_Type.repost))
        {
            setupToolbar("Reposted by " + singleNotificationDetails.getUserData().getFull_name());
        } else if (singleNotificationDetails != null && singleNotificationDetails.getType().equals(Web.NotificationList_Type.comment))
        {
            setupToolbar("Commented by " + singleNotificationDetails.getUserData().getFull_name());
        } else if (singleNotificationDetails != null && singleNotificationDetails.getType().equals(Web.NotificationList_Type.external_share))
        {
            setupToolbar("Shared by " + singleNotificationDetails.getUserData().getFull_name());
        } else if (singleNotificationDetails != null && singleNotificationDetails.getType().equals("QuickLectureDetails"))
        {
            setupToolbar("Quick Lecture Details");
        } else
        {
            setupToolbar("Notification Details");
        }

        initViews();
        setData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void initViews()
    {
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        layLike = (LinearLayout) findViewById(R.id.layLike);
        linearMain = (LinearLayout) findViewById(R.id.linearMain);
        layPlay = (LinearLayout) findViewById(R.id.layPlay);
        imgvLike = (ImageView) findViewById(R.id.imgvLike);
        imgvOptions = (ImageView) findViewById(R.id.imgvOptions);
        imgvUserImage = (ImageView) findViewById(R.id.imgvUserImage);
        imgvThumbnail = (ImageView) findViewById(R.id.imgvThumbnail);
        actxtvLectureTitle = (TextView) findViewById(R.id.actxtvLectureTitle);
        txtvFavouriteCount = (TextView) findViewById(R.id.txtvFavouriteCount);
        txtvCommentsCount = (TextView) findViewById(R.id.txtvCommentsCount);
        txtvRepostedCount = (TextView) findViewById(R.id.txtvRepostedCount);
        txtvTitleBig = (TextView) findViewById(R.id.txtvTitleBig);
        txtvViewsCount = (TextView) findViewById(R.id.txtvViewsCount);
        llRepost = (LinearLayout) findViewById(R.id.llRepost);

        imgv_userImage = (ImageView) findViewById(R.id.imgv_userImage);
        imgv_repost = (ImageView) findViewById(R.id.imgv_repost);

        txtv_userName = (TextView) findViewById(R.id.txtv_userName);
        txtv_following = (TextView) findViewById(R.id.txtv_following);
        txtv_followers = (TextView) findViewById(R.id.txtv_followers);
        txtv_lectureDetails = (TextView) findViewById(R.id.txtv_lectureDetails);
        txtv_followEvent = (TextView) findViewById(R.id.txtv_followEvent);
        textv_userDetails = (TextView) findViewById(R.id.textv_userDetails);
        txtvDuration = (TextView) findViewById(R.id.txtvDuration);
        ll_comments = (LinearLayout) findViewById(R.id.ll_comments);
        ll_connect = (LinearLayout) findViewById(R.id.ll_connect);

        imgvRepostUserImage = (ImageView) findViewById(R.id.imgvRepostUserImage);
        txtvTimeAgo = (TextView) findViewById(R.id.txtvTimeAgo);
        txtvUserName = (TextViewWithImages) findViewById(R.id.txtvUserName);

        img_notify_icon = (ImageView) findViewById(R.id.img_notify_icon);


    }


    public void setData()
    {
        try
        {
            getFollowingFollowersData();

            txtv_followEvent.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    followUnfollow();
                }
            });

            txtv_userName.setText(singleNotificationDetails.getUserData().getFull_name());

            txtv_lectureDetails.setText(
                    singleNotificationDetails.getUserData().getBio().trim().isEmpty()
                            ? "No bio details avaiable yet."
                            : singleNotificationDetails.getUserData().getBio());


            txtvDuration.setText(
                    singleNotificationDetails.getQuick_lecture().getQuick_lecture_time_duration() != null
                            ?
                            singleNotificationDetails.getQuick_lecture().getQuick_lecture_time_duration()
                            : "");

            if (!UtillsG.getNotNullString(singleNotificationDetails.getQuick_lecture().getThumbnail(), "").isEmpty())
            {
                ImageLoader.setImageBig(imgvThumbnail, singleNotificationDetails.getQuick_lecture().getThumbnail());
            } else
            {
                imgvThumbnail.setImageResource(R.drawable.white_square);
            }

            ImageLoader.setImageBig(imgvUserImage, new SharedPrefHelper(getActivityG()).getProfilePic());


            if (!UtillsG.getNotNullString(singleNotificationDetails.getUserData().getProfile_pic(), "").isEmpty())
            {
                ImageLoader.setImageBig(imgv_userImage, singleNotificationDetails.getUserData().getProfile_pic());
            } else
            {
                ImageLoader.setImageBig(imgv_userImage, "https://images.designtrends.com/wp-content/uploads/2015/11/30162730/White-and-Grey-Plain-Background.jpg");
            }


            txtvUserName.setText(singleNotificationDetails.getMessage());

           /* if (singleNotificationDetails.getType().equals(Web.NotificationList_Type.repost)) {
                ImageLoader.setImageSmall(imgvRepostUserImage, singleNotificationDetails.getQuick_lecture().getUserdata().getProfile_pic());
            }
            else {*/
            ImageLoader.setImageSmall(imgvRepostUserImage, singleNotificationDetails.getUserData().getProfile_pic());
  /*          }*/


            txtvTimeAgo.setText(DateHelper.convertDateToRequiredFormat(singleNotificationDetails.getDate_time(), "dd MM yyyy, hh:mm a"));


            txtvFavouriteCount.setText(singleNotificationDetails.getQuick_lecture().getFavourites_count());
            txtvCommentsCount.setText(singleNotificationDetails.getQuick_lecture().getComments_count());
            txtvRepostedCount.setText(singleNotificationDetails.getQuick_lecture().getReposted_count());
            txtvViewsCount.setText(singleNotificationDetails.getQuick_lecture().getLecture_view());


            if (singleNotificationDetails.getQuick_lecture().getIs_reposted())
            {
                imgv_repost.setColorFilter(ContextCompat.getColor(NotificationDetailsActivity.this, R.color.greenTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
            } else
            {
                imgv_repost.setColorFilter(ContextCompat.getColor(NotificationDetailsActivity.this, R.color.dialogBackgroundColor), android.graphics.PorterDuff.Mode.MULTIPLY);
            }


            layPlay.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (UtillsG.isAudioVideoMediaPost(singleNotificationDetails.getQuick_lecture()))
                    {
                        setLectureAsPlayed();
                        PlayerTabActivity.start(getActivityG(), singleNotificationDetails.getQuick_lecture());

                    } else if (UtillsG.isTextPost(singleNotificationDetails.getQuick_lecture()))
                    {
                        UtillsG.showToast("It is a text Lecture.", getActivityG(), true);
                    }
                }
            });

            imgvUserImage.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    UtillsG.showFullImage(Web.Path.BASE_URL + singleNotificationDetails.getQuick_lecture().getUserdata().getProfile_pic(), getActivityG(), false);
                }
            });

            if (singleNotificationDetails.getQuick_lecture().getIs_favourite().equalsIgnoreCase("1"))
            {
                imgvLike.setImageResource(R.drawable.ic_liked);
                imgvLike.setColorFilter(ContextCompat.getColor(getActivityG(), R.color.redTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
            } else
            {
                imgvLike.setImageResource(R.mipmap.ic_fav_white_border);
                imgvLike.setColorFilter(ContextCompat.getColor(getActivityG(), R.color.dialogBackgroundColor), android.graphics.PorterDuff.Mode.MULTIPLY);
            }

            layLike.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    likeUnlikeLecture(imgvLike);
                }
            });

            imgvOptions.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    showMoreOptions(v, singleNotificationDetails.getQuick_lecture());
                }
            });

            llRepost.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {


                    if (singleNotificationDetails.getQuick_lecture().getUser_id().equals(new SharedPrefHelper(getActivityG()).getUserId())
                            ||
                            (singleNotificationDetails.getQuick_lecture().getOwnerUserdata() != null &&
                                    singleNotificationDetails.getQuick_lecture().getOwnerUserdata().getUser_id().equals(new SharedPrefHelper(getActivityG()).getUserId())))
                    {
                        UtillsG.showToast("You are the creator so you cannot repost it.", getActivityG(), true);
                    } else if (!singleNotificationDetails.getQuick_lecture().getIs_reposted())
                    {

                        MediaPlayer mp = MediaPlayer.create(getActivityG(), R.raw.tone_like);
                        mp.start();

                        mClicks.RepostQuickLecture((Activity) getActivityG(), LVApplication.getRetrofit().create(HomeApis.class),
                                singleNotificationDetails.getQuick_lecture().getQuick_lecture_id(), new CallBackG<Boolean>()
                                {
                                    @Override
                                    public void onCallBack(Boolean output)
                                    {
                                        if (output)
                                        {
                                            int repostCount = Integer.parseInt(singleNotificationDetails.getQuick_lecture().getReposted_count());
                                            repostCount++;
                                            txtvRepostedCount.setText("" + repostCount);
                                            imgv_repost.setColorFilter(ContextCompat.getColor(NotificationDetailsActivity.this, R.color.greenTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
                                            singleNotificationDetails.getQuick_lecture().setIs_reposted(true);
                                            EventBus.getDefault().post(new HomeFragmentUpdateBus(true));
                                        }
                                    }
                                });
                    } else if (singleNotificationDetails.getQuick_lecture().getIs_reposted())
                    {

                        MediaPlayer mp = MediaPlayer.create(getActivityG(), R.raw.tone_dislike);
                        mp.start();

                        mClicks.unrepost_lecture((Activity) getActivityG(),
                                singleNotificationDetails.getQuick_lecture().getQuick_lecture_id() != null
                                        ?
                                        singleNotificationDetails.getQuick_lecture().getQuick_lecture_id()
                                        : ""
                                , new CallBackG<Boolean>()
                                {
                                    @Override
                                    public void onCallBack(Boolean output)
                                    {
                                        if (output)
                                        {
                                            int repostCount = Integer.parseInt(singleNotificationDetails.getQuick_lecture().getReposted_count());
                                            repostCount--;
                                            txtvRepostedCount.setText("" + repostCount);
                                            imgv_repost.setColorFilter(ContextCompat.getColor(NotificationDetailsActivity.this, R.color.greyDark), android.graphics.PorterDuff.Mode.MULTIPLY);
                                            EventBus.getDefault().post(new HomeFragmentUpdateBus(true));
                                            singleNotificationDetails.getQuick_lecture().setIs_reposted(false);

                                        }
                                    }
                                });
                    } else
                    {
                        UtillsG.showToast("Already reposted this lecture", NotificationDetailsActivity.this, true);
                    }


                }
            });

            ll_comments.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    EventBus.getDefault().post("");
                    if (UtillsG.isAudioVideoMediaPost(singleNotificationDetails.getQuick_lecture()))
                    {
                        setLectureAsPlayed();
                        PlayerActivity.startForResult(NotificationDetailsActivity.this, singleNotificationDetails.getQuick_lecture(), true);
                    } else if (UtillsG.isTextPost(singleNotificationDetails.getQuick_lecture()))
                    {
                        //Directly redirecting to comment screen . Uncomment above and comment below code if want to comment on NotificationDetailsActivity itself
                        Lecture_And_Event_Model lecture_and_event_model = singleNotificationDetails.getQuick_lecture();
                        TextLectureCommentsActivity.start(NotificationDetailsActivity.this, lecture_and_event_model);
                    }
                }
            });

            ll_connect.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    ActivityOtherUserDetails.start(NotificationDetailsActivity.this, singleNotificationDetails.getUserData(), true);
                }
            });


            imgvOptions.setColorFilter(getActivityG().getResources().getColor(R.color.dialogBackgroundColor));

            if (singleNotificationDetails.getQuick_lecture().getQuick_lecture_type().equalsIgnoreCase("text"))
            {
                layPlay.setVisibility(View.GONE);
                actxtvLectureTitle.setMaxLines(2);
                actxtvLectureTitle.setVisibility(View.GONE);
                txtvTitleBig.setVisibility(View.VISIBLE);
                txtvTitleBig.setText(singleNotificationDetails.getQuick_lecture().getQuick_lecture_text());
                actxtvLectureTitle.setText(singleNotificationDetails.getQuick_lecture().getQuick_lecture_text());

                UtillsG.setHeightWidtWRAP_LinearLayout(((Activity) getActivityG()), frameLayout, getActivityG());
            } else
            {
                layPlay.setVisibility(View.VISIBLE);
                actxtvLectureTitle.setMaxLines(2);
                actxtvLectureTitle.setVisibility(View.VISIBLE);
                txtvTitleBig.setVisibility(View.GONE);

                txtvTitleBig.setText(singleNotificationDetails.getQuick_lecture().getQuick_lecture_text());
                actxtvLectureTitle.setText(singleNotificationDetails.getQuick_lecture().getQuick_lecture_text());

                UtillsG.setHeightWidthWithRatio_LinearLayout(((Activity) getActivityG()), frameLayout, 7f, 16f);
            }

            UtillsG.setTextSizeByPercentage(getActivityG(), actxtvLectureTitle, 4);
            UtillsG.setTextSizeByPercentage(getActivityG(), txtvTitleBig, 6);

            textv_userDetails.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    ActivityOtherUserDetails.start(NotificationDetailsActivity.this, singleNotificationDetails.getUserData(), true);
                }
            });


            if (singleNotificationDetails.getType().equals(Web.NotificationList_Type.favourite))
            {
                img_notify_icon.setImageResource(R.mipmap.ic_favorite_red);
            } else if (singleNotificationDetails.getType().equals(Web.NotificationList_Type.repost))
            {
                img_notify_icon.setImageResource(R.mipmap.ic_refresh_green);
            } else if (singleNotificationDetails.getType().equals(Web.NotificationList_Type.share))
            {
                img_notify_icon.setImageResource(R.mipmap.ic_share_green);
            } else if (singleNotificationDetails.getType().equals(Web.NotificationList_Type.comment))
            {
                img_notify_icon.setImageResource(R.mipmap.ic_message_grey);
            } else
            {
                img_notify_icon.setVisibility(View.GONE);
            }


        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setLectureAsPlayed()
    {
        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance().add_lecture_to_recent_played_list(new SharedPrefHelper(getActivityG()).getUserId(), singleNotificationDetails.getQuick_lecture().getQuick_lecture_id());
        basicApiModelCall.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
            {
                if (response.body() != null && response.body().getStatus())
                {
                    Log.e("RESPONSE", response.body().getStatus() + "");

                    if (response.body().getStatus())
                    {
                        int count = Integer.parseInt(singleNotificationDetails.getQuick_lecture().getLecture_view());
                        count++;
                        singleNotificationDetails.getQuick_lecture().setLecture_view(String.valueOf(count));

                        txtvViewsCount.setText(String.valueOf(count));
                    }
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t)
            {
                Log.e("ERROR get_posted_le", t.getMessage().toString());
            }
        });
    }

    protected HomeApis getRetrofitInstance()
    {
        return LVApplication.getRetrofit().create(HomeApis.class);
    }


    private void followUnfollow()
    {
        UtillsG.showLoading("Please wait..", getActivityG());
        Call<BasicApiModel> getFollowersList = LVApplication.getRetrofit().create(ProfileApi.class).follow_user(new SharedPrefHelper(getActivityG()).getUserId(), singleNotificationDetails.getUserData().getUser_id());
        getFollowersList.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
            {
                UtillsG.hideLoading();
                if (response.body().getStatus())
                {
                    userProfileModel.setIs_following(!userProfileModel.is_following());

                    txtv_followEvent.setText(
                            userProfileModel.is_following() ? "Un follow" : "Follow");

                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                    getFollowingFollowersData();
                } else
                {
                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t)
            {
                UtillsG.hideLoading();
                UtillsG.showToast(t.getMessage(), getActivityG(), true);
            }
        });
    }

    private void getFollowingFollowersData()
    {
        Call<UserProfileModel> userProfileApi = LVApplication.getRetrofit().create(HomeApis.class).get_user_profile(new SharedPrefHelper(getActivityG()).getUserId(), singleNotificationDetails.getUserData().getUser_id());
        userProfileApi.enqueue(new Callback<UserProfileModel>()
        {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response)
            {
                if (response.body().getStatus())
                {
                    userProfileModel = response.body().getData().getUserdata();

                    txtv_following.setText(UtillsG.getNotNullString(userProfileModel.getFollowings_count(), "0"));
                    txtv_followers.setText(UtillsG.getNotNullString(userProfileModel.getFollowers_count(), "0"));
                    txtv_followEvent.setText(
                            userProfileModel.is_following() ? "Un follow" : "Follow");
                } else
                {
                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                }
            }

            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t)
            {
                UtillsG.showToast(t.getMessage(), getActivityG(), true);
            }
        });
    }

    public void showMoreOptions(View view, final Lecture_And_Event_Model mModel)
    {
        DialogHelper.getInstance().showLectureMoreOptions(getActivityG(), view,
                mModel.getIs_reposted(), mModel.getUser_id().equalsIgnoreCase(new SharedPrefHelper(getActivityG()).getUserId()), new CallBackG<Integer>()
                {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onCallBack(final Integer attendType)
                    {

                        if (attendType == 1)
                        {
                            layPlay.performClick();
                        } else if (attendType == 2)
                        {
                            layLike.performClick();
                        } else if (attendType == 3)
                        {
                            ll_connect.performClick();
                        } else if (attendType == 4)
                        {

                        } else if (attendType == 5)
                        {
                            llRepost.performClick();
                        } else if (attendType == 6)
                        {
                            if (mModel.getDiscovery_url() != null && !mModel.getDiscovery_url().trim().isEmpty())
                            {
                                WebViewRecorderActivity.startForBrowsingOnly(getActivityG(), mModel.getDiscovery_url());
                            } else
                            {
                                UtillsG.showToast("Url is empty", getActivityG(), true);
                            }
                        } else if (attendType == 7)
                        {
                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra(Intent.EXTRA_TEXT, mModel.getFile_path());
                            sendIntent.setType("text/plain");
                            getActivityG().startActivity(sendIntent);
                        } else if (attendType == 8)
                        {
                            DialogHelper.getInstance().executeDeleteLecture(singleNotificationDetails.getQuick_lecture(), getActivityG(), new CallBackG<Boolean>()
                            {
                                @Override
                                public void onCallBack(Boolean output)
                                {
                                    if (output)
                                    {
                                        finish();
                                    }
                                }
                            });
                        }


                    }
                });
    }


    private void likeUnlikeLecture(final ImageView imgvLike)
    {
        Call<BasicApiModel> basicApiModelCall = LVApplication.getRetrofit().create(HomeApis.class).add_remove_to_favourite_list(new SharedPrefHelper(getActivityG()).getUserId(),
                singleNotificationDetails.getQuick_lecture().getQuick_lecture_id());
        basicApiModelCall.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
            {
                if (response.body() != null && response.body().getStatus())
                {
                    Log.e("RESPONSE", response.body().getStatus() + "");

                    if (response.body().getStatus())
                    {
                        int count = Integer.parseInt(singleNotificationDetails.getQuick_lecture().getFavourites_count());
                        UtillsG.showToast(String.valueOf(response.body().getMessage()), getActivityG(), true);
                        if (singleNotificationDetails.getQuick_lecture().getIs_favourite().equalsIgnoreCase("0"))
                        {
                            singleNotificationDetails.getQuick_lecture().setIs_favourite("1");
                            count++;
                            imgvLike.setImageResource(R.drawable.ic_liked);
                            imgvLike.setColorFilter(ContextCompat.getColor(getActivityG(), R.color.redTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
                        } else
                        {
                            singleNotificationDetails.getQuick_lecture().setIs_favourite("0");
                            count--;
                            imgvLike.setImageResource(R.mipmap.ic_fav_white_border);
                            imgvLike.setColorFilter(ContextCompat.getColor(getActivityG(), R.color.dialogBackgroundColor), android.graphics.PorterDuff.Mode.MULTIPLY);
                        }
                        singleNotificationDetails.getQuick_lecture().setFavourites_count(String.valueOf(count));
                    }
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t)
            {
                Log.e("ERROR get_posted_le", t.getMessage().toString());
                UtillsG.showToast(String.valueOf(t.getMessage().toString()), getActivityG(), true);
            }
        });
    }


    @Override
    public Context getActivityG()
    {
        return NotificationDetailsActivity.this;
    }
}
