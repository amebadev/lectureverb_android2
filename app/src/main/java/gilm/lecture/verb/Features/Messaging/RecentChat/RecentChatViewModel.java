/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Messaging.RecentChat;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

/**
 * Created by G-Expo on 17 Jul 2017.
 */

public class RecentChatViewModel extends BaseObservable
{
    private String imageUrl;
    private String time;
    private String message, title;
    private boolean isRead;

    @Bindable
    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
        notifyChange();
    }

    @Bindable
    public String getImageUrl()
    {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
        notifyChange();
    }

    @Bindable
    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
        notifyChange();
    }

    @Bindable
    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
        notifyChange();
    }

    @Bindable
    public boolean isRead()
    {
        return isRead;
    }

    public void setRead(boolean read)
    {
        isRead = read;
        notifyChange();
//        notifyPropertyChanged(BR.read);
    }

}
