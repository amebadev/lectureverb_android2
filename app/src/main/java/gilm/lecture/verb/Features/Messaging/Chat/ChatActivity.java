/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Messaging.Chat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.czt.mp3recorder.MP3Recorder;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.BaseActivityNoBinding;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.Messaging.ChatApis;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.BitmapDecoderG;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.EndlessRecyclerOnScrollListener;
import gilm.lecture.verb.UtilsG.FileHelper;
import gilm.lecture.verb.UtilsG.Select_Document;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import gilm.lecture.verb.WebServices.Web;
import gilm.lecture.verb.Widget.TextViewWithImages;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends BaseActivityNoBinding<ChatPresenter> implements ChatView, View.OnClickListener, ProgressRequestBody.UploadCallbacks {
    private String recordedAudioPath = "";
    private MP3Recorder mAudioRecorder;
    MediaPlayer recordedAudioPlayer;
    boolean isRecorderButtonFocused;
    private ImageButton imgbAudio, imgbPlayAudio, imgbCancel;
    LinearLayout llAudioRecorderTimer, llRecordedAudio;
    SeekBar seekBAr;
    RecyclerView recyclerView;
    ViewFlipper viewFlipper;
    TextView txtvTimer;
    private CardView cardAttachment;
    Activity mActivity;
    Uri videoThumbNailUri = null;
    Uri selectedFilePath = null;
    String Extension_file = "";
    String videoThumbnailPath = "";
    ImageView imgv_audio, imgv_gallery, imgv_video, imgv_document, imgv_pdf, imgv_text, imgv_sendMsg, imgvMoreOptions;
    UserDataModel.UserData mOtherUserModel;
    ChatAdapter2 mChatAdapter = null;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    CardView blockUserCard;
    TextViewWithImages txtv_blockMsg;
    TextView txtv_BlockUnBlock;
    ImageView emoji_btn;
    EmojiconEditText edMessage;
    int pageNumber = 1;
    public ProgressBar progress_bar;
    EmojIconActions emojIcon;
    View rootView;
    ProgressBar progressBarSendMessage;


    public static void start(Context context, View view, UserDataModel.UserData mOtherUserModel) {
        Intent starter = new Intent(context, ChatActivity.class);
        starter.putExtra(Constants.Extras.DATA, mOtherUserModel);
        starter.putExtra("mOtherUserModel", mOtherUserModel);
        UtillsG.startTransition((Activity) context, starter, view);
    }

    public static void start(Context context, UserDataModel.UserData mOtherUserModel) {
        Intent starter = new Intent(context, ChatActivity.class);
        starter.putExtra("mOtherUserModel", mOtherUserModel);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_chat;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new ChatPresenter());
        getPresenter().attachView(this);
    }

    @Override
    protected void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        implementEnterIsSendFunctionality();

        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void RefreshData(ISBlockedUnBlockedBus data) {

        if (!data.isChatMessage() && data.getUserId().equals(mOtherUserModel.getUser_id())) {
            mOtherUserModel.setIs_blocked(data.isBlocked() ? "2" : "0");
            showHideBlockStatus(data.isBlocked() ? "2" : "0");
        } else if (data.getUserId().equals(mOtherUserModel.getUser_id())) {
            pageNumber = 1;
            getPresenter().getInitialChatList(pageNumber);


        }
    }


    @Override
    public void initViews() {

        mActivity = this;

        mOtherUserModel = (UserDataModel.UserData) getIntent().getSerializableExtra("mOtherUserModel");
        TextView title = (TextView) findViewById(R.id.title);
        ImageView imgvBack = (ImageView) findViewById(R.id.imgvBack);
        ImageView imgvUserImage = (ImageView) findViewById(R.id.imgvUserImage);
        title.setText(UtillsG.getNotNullString(mOtherUserModel.getFull_name(), "Chat"));
        ImageLoader.setImageRoundSmall(imgvUserImage, UtillsG.getNotNullString(mOtherUserModel.getProfile_pic(), ""));
        imgvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtillsG.hideKeyboard(getActivityG(), edMessage);
                finish();
            }
        });


        rootView = findViewById(R.id.root_view);
        progressBarSendMessage = (ProgressBar) findViewById(R.id.progressBarSendMessage);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        txtvTimer = (TextView) findViewById(R.id.txtvTimer);
        viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);
        seekBAr = (SeekBar) findViewById(R.id.seekBAr);
        edMessage = (EmojiconEditText) findViewById(R.id.edMessage);
        imgbCancel = (ImageButton) findViewById(R.id.imgbCancel);
        imgbAudio = (ImageButton) findViewById(R.id.imgbAudio);
        imgbPlayAudio = (ImageButton) findViewById(R.id.imgbPlayAudio);
        llAudioRecorderTimer = (LinearLayout) findViewById(R.id.llAudioRecorderTimer);
        llRecordedAudio = (LinearLayout) findViewById(R.id.llRecordedAudio);
        emoji_btn = (ImageView) findViewById(R.id.emoji_btn);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(getActivityG(), LinearLayoutManager.VERTICAL, true);
        recyclerView.setLayoutManager(linearLayoutManager);
        mChatAdapter = new ChatAdapter2(getPresenter().alMessages, getActivityG());
        recyclerView.setAdapter(mChatAdapter);
        blockUserCard = (CardView) findViewById(R.id.blockUserCard);
        blockUserCard.setVisibility(View.INVISIBLE);
        (txtv_BlockUnBlock = (TextView) findViewById(R.id.txtv_BlockUnBlock)).setOnClickListener(this);
        txtv_blockMsg = (TextViewWithImages) findViewById(R.id.txtv_blockMsg);

        emojIcon = new EmojIconActions(this, rootView, edMessage, emoji_btn);
        emojIcon.ShowEmojIcon();
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e("Keyboard", "open");
            }

            @Override
            public void onKeyboardClose() {
                Log.e("Keyboard", "close");
            }
        });

        cardAttachment = (CardView) findViewById(R.id.cardAttachment);
        cardAttachment.setVisibility(View.INVISIBLE);
        (imgv_audio = (ImageView) findViewById(R.id.imgv_audio)).setOnClickListener(this);
        (imgv_gallery = (ImageView) findViewById(R.id.imgv_gallery)).setOnClickListener(this);
        (imgv_video = (ImageView) findViewById(R.id.imgv_video)).setOnClickListener(this);
        (imgv_document = (ImageView) findViewById(R.id.imgv_document)).setOnClickListener(this);
        (imgv_pdf = (ImageView) findViewById(R.id.imgv_pdf)).setOnClickListener(this);
        (imgv_text = (ImageView) findViewById(R.id.imgv_text)).setOnClickListener(this);
        (imgv_sendMsg = (ImageView) findViewById(R.id.imgv_sendMsg)).setOnClickListener(this);
        (imgvMoreOptions = (ImageView) findViewById(R.id.imgvMoreOptions)).setOnClickListener(this);


        imgbAudio.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        isRecorderButtonFocused = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (isRecorderButtonFocused) {
                                    startRecording();
                                }
                            }
                        }, 1000);
                        break;
                    case MotionEvent.ACTION_UP:
                        isRecorderButtonFocused = false;
                        resumeTimer = false;
                        if (mAudioRecorder != null) {
                            mAudioRecorder.stop();
                            mAudioRecorder = null;

                            edMessage.setSingleLine(false);
                            llAudioRecorderTimer.setVisibility(View.GONE);

                            setupRecordedAudioPlayer();
                        }
                        break;
                }
                return false;
            }
        });

        (findViewById(R.id.imgvAttachment)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCloseAttachmentOptions();
            }
        });

        progress_bar.setVisibility(View.VISIBLE);
        getPresenter().getInitialChatList(pageNumber);


        showHideBlockStatus(mOtherUserModel.getIs_blocked());


        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                pageNumber++;
                getPresenter().loadMoreData(pageNumber);
            }

            @Override
            public void countChange(int currentVisibleCount) {

            }
        };

        recyclerView.setOnScrollListener(endlessRecyclerOnScrollListener);

        edMessage.setTextSize((float) new SharedPrefHelper(getActivityG()).getFontSize());
    }


    public void showHideBlockStatus(String blockedId) {
        if (blockedId.equals(Web.BlockUserStates.BlockByMe)) {
            blockUserCard.setVisibility(View.VISIBLE);
            txtv_blockMsg.setText("You have  [img src=ic_block_user/]  blocked this user to send message");
            txtv_BlockUnBlock.setVisibility(View.VISIBLE);
            txtv_BlockUnBlock.setText("Un Block");
            imgv_sendMsg.setEnabled(false);
        } else if (blockedId.equals(Web.BlockUserStates.Noblock)) {
            blockUserCard.setVisibility(View.INVISIBLE);
            imgv_sendMsg.setEnabled(true);
        } else if (blockedId.equals(Web.BlockUserStates.BlockByOther)) {
            blockUserCard.setVisibility(View.VISIBLE);
            txtv_blockMsg.setText("This user [img src=ic_block_user/]  blocks you to send message");
            txtv_BlockUnBlock.setVisibility(View.GONE);
            imgv_sendMsg.setEnabled(false);
        } else {
            blockUserCard.setVisibility(View.INVISIBLE);
            imgv_sendMsg.setEnabled(true);
        }
    }

    @Override
    public String getTargetUserId() {
        return mOtherUserModel.getUser_id();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mChatAdapter.releaseAudioPlayer();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgv_audio:
                Select_Document.startActivity(mActivity, Web.File_Options.AUDIO);
                openCloseAttachmentOptions();
                break;
            case R.id.imgv_gallery:
                BitmapDecoderG.selectImage(mActivity, null);
                openCloseAttachmentOptions();
                break;
            case R.id.imgv_video:
                Select_Document.startActivity(mActivity, Web.File_Options.VIDEO);
                openCloseAttachmentOptions();
                break;
            case R.id.imgv_document:
                Select_Document.startActivity(mActivity, Web.File_Options.DOC);
                openCloseAttachmentOptions();
                break;
            case R.id.imgv_pdf:
                Select_Document.startActivity(mActivity, Web.File_Options.PDF);
                openCloseAttachmentOptions();
                break;
            case R.id.imgv_text:
                Select_Document.startActivity(mActivity, Web.File_Options.TEXT);
                openCloseAttachmentOptions();
                break;
            case R.id.imgvMoreOptions:
                showMoreOptions(mActivity, view, mOtherUserModel);
                break;
            case R.id.txtv_BlockUnBlock:
                blockUserCheck(mOtherUserModel);
                break;
            case R.id.imgv_sendMsg:
                edMessage.setHint("Type a message");
                if (recordedAudioPath.trim().isEmpty() && selectedFilePath == null && edMessage.getText().toString().trim().isEmpty()) {
                    UtillsG.showToast("Select attached or enter message to send", mActivity, true);
                } else {

                    if (selectedFilePath == null && !recordedAudioPath.trim().isEmpty()) {
                        selectedFilePath = Uri.fromFile(new File(recordedAudioPath));
                        Extension_file = Web.File_Options.AUDIO;
                    }
                    progressBarSendMessage.setVisibility(View.VISIBLE);
                    imgv_sendMsg.setVisibility(View.INVISIBLE);

                    imgv_sendMsg.setEnabled(false);
                    Call<ChatModel> quickLectureApiModelCall = LVApplication.getRetrofit().create(ChatApis.class).saveChat(
                            selectedFilePath != null
                                    ? getFilePart(new File(selectedFilePath.getPath()), "chatmedia")
                                    : null,
                            getOtherParams(new SharedPrefHelper(mActivity).getUserId()),
                            getOtherParams(mOtherUserModel.getUser_id()),
                            getOtherParams(UtillsG.encodeEmoji(edMessage.getText().toString()))
                            , getOtherParams(selectedFilePath != null ? "file" : "text")
                            , getOtherParams(Extension_file), getOtherParams(new SharedPrefHelper(getActivityG()).getUserName()));
                    quickLectureApiModelCall.enqueue(new Callback<ChatModel>() {
                        @Override
                        public void onResponse(Call<ChatModel> call, final Response<ChatModel> response) {
                            progressBarSendMessage.setVisibility(View.GONE);
                            imgv_sendMsg.setVisibility(View.VISIBLE);
                            imgv_sendMsg.setEnabled(true);

                            if (response.body() != null) {

                                if (response.body().getStatus()) {
                                    if (mActivity != null && selectedFilePath != null) {
                                        UtillsG.showToast("Message sent successfully.", mActivity, true);
                                    }

                                    edMessage.setText("");
                                    Extension_file = "";
                                    selectedFilePath = null;
                                    recordedAudioPath = "";
                                    llRecordedAudio.setVisibility(View.GONE);
                                    if (recordedAudioPlayer != null) {
                                        recordedAudioPlayer.stop();
                                        recordedAudioPlayer = null;
                                        imgbPlayAudio.setImageResource(android.R.drawable.ic_media_play);
                                        seekBAr.setProgress(0);
                                    }

                                    getPresenter().alMessages.add(0, response.body().getmChatList());
                                    notifyDataUpdated();

                                    //TODO : handle chat adapter
                                    //recyclerView.getAdapter().notifyDataSetChanged();


                                } else {
                                    if (mActivity != null) {
                                        UtillsG.showToast(response.message(), mActivity, true);
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ChatModel> call, Throwable t) {
                            progressBarSendMessage.setVisibility(View.GONE);
                            imgv_sendMsg.setVisibility(View.VISIBLE);
                            imgv_sendMsg.setEnabled(true);
                            Log.e("Error", t.toString());

                            UtillsG.showToast("Error while sending chat message", mActivity, true);
                        }
                    });
                }

                break;

        }
    }

    private MultipartBody.Part getFilePart(final File file, String fileType) {
        ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(fileType, file.getName(), fileBody);
        return filePart;
    }


    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                UtillsG.showToast("Image ready to send.", getActivityG(), true);
                edMessage.setHint("Image Attached");
                CropImage.ActivityResult result = CropImage.getActivityResult(data);

                selectedFilePath = result.getUri();
                Extension_file = Web.File_Options.IMAGE;

            } else if (requestCode == 1011) {
                UtillsG.showToast("File ready to send.", getActivityG(), true);
                edMessage.setHint("File Attached");

                selectedFilePath = Uri.fromFile(new File(data.getExtras().getString("file_path")));
                Extension_file = data.getExtras().getString("Document_type");
                if (Extension_file.equals(Web.File_Options.VIDEO)) {
                    Bitmap bmp;
                    bmp = ThumbnailUtils.createVideoThumbnail(Uri.fromFile(new File(data.getExtras().getString("file_path"))).getPath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                    videoThumbNailUri = Uri.fromFile(getFileonSDCard("thumbnail.png"));
                    videoThumbnailPath = getFileonSDCard("thumbnail.png").getPath();

                    FileOutputStream out = null;
                    try {
                        out = new FileOutputStream(getFileonSDCard("thumbnail.png"));
                        bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (out != null) {
                                out.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    videoThumbNailUri = null;
                    videoThumbnailPath = "";
                }
            } else {
                if (resultCode != RESULT_OK) {
                    return;
                }
                cropMeImage(BitmapDecoderG.onActivityResult(mActivity, requestCode, resultCode, data));
            }
        }
    }


    public void cropMeImage(Uri uri) {
        CropImage.activity(uri)
                .setAspectRatio(2, 2)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(mActivity);
    }

    public static File getFileonSDCard(String nameWithXtension) {
        File file = null;

        try {
            //create file
            file = new File(Environment.getExternalStorageDirectory() + File.separator, nameWithXtension);
            file.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;

    }

    private void startRecording() {
        edMessage.setSingleLine(true);
        llAudioRecorderTimer.setVisibility(View.VISIBLE);
        MediaPlayer.create(ChatActivity.this, R.raw.beep).start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (recordedAudioPath.isEmpty()) {
                    recordedAudioPath = FileHelper.getInstance().createAudioFile("pkChatAudio" + System.currentTimeMillis() + ".m4a").getAbsolutePath();
                }
                try {
                    startTimer();
                    mAudioRecorder = new MP3Recorder(new File(recordedAudioPath));
                    mAudioRecorder.start();
                    resumeTimer = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 300);
    }

    private void setupRecordedAudioPlayer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                llRecordedAudio.setVisibility(View.VISIBLE);
                recordedAudioPlayer = MediaPlayer.create(ChatActivity.this, Uri.fromFile(new File(recordedAudioPath)));
                if (recordedAudioPlayer != null) {
                    seekBAr.setMax(recordedAudioPlayer.getDuration());
                }
                recordedAudioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        imgbPlayAudio.setImageResource(android.R.drawable.ic_media_play);
                        seekBAr.setProgress(0);
                    }
                });
                imgbPlayAudio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        seekbarHandler = new Handler();

                        if (!recordedAudioPlayer.isPlaying()) {
                            recordedAudioPlayer.start();
                            imgbPlayAudio.setImageResource(android.R.drawable.ic_media_pause);
                            updateSeekBar();
                        } else {
                            recordedAudioPlayer.pause();
                            imgbPlayAudio.setImageResource(android.R.drawable.ic_media_play);
                        }
                    }
                });

                imgbCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        recordedAudioPath = "";
                        llRecordedAudio.setVisibility(View.GONE);
                        recordedAudioPlayer.stop();
                        recordedAudioPlayer = null;
                        imgbPlayAudio.setImageResource(android.R.drawable.ic_media_play);
                        seekBAr.setProgress(0);
                    }
                });
            }
        }, 800);
    }

    private void updateSeekBar() {
        if (recordedAudioPlayer != null) {
            seekBAr.setProgress(recordedAudioPlayer.getCurrentPosition());
            seekbarHandler.postDelayed(runnable, 50);
        }
    }

    Handler seekbarHandler;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            updateSeekBar();
        }
    };


    public void showMoreOptions(Context context, final View anchor, final UserDataModel.UserData mOtherUserData) {
        final PopupMenu popup = new PopupMenu(context, anchor);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.menu_chat, popup.getMenu());


        if (UtillsG.getNotNullString(mOtherUserData.getIs_blocked(), "").equals(Web.BlockUserStates.BlockByMe)) {
            popup.getMenu().getItem(0).setTitle("Un Block");
        } else if (UtillsG.getNotNullString(mOtherUserData.getIs_blocked(), "").equals(Web.BlockUserStates.Noblock)) {
            popup.getMenu().getItem(0).setTitle("Block");
        } else if (UtillsG.getNotNullString(mOtherUserData.getIs_blocked(), "").equals(Web.BlockUserStates.BlockByOther)) {
            popup.getMenu().getItem(0).setTitle("Block by " + mOtherUserData.getFull_name());
        }


        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.blockUser:

                        blockUserCheck(mOtherUserData);
                        break;
                }
                return true;
            }
        });

        popup.show(); //showing popup menu
    }


    @Override
    public Context getActivityG() {
        return ChatActivity.this;
    }

    @Override
    public void notifyDataUpdated() {
        progress_bar.setVisibility(View.GONE);
        recyclerView.getAdapter().notifyDataSetChanged();
    }


    @Override
    public void updateMyMessage(List<MsgDataModel> listData) {

    }

    @Override
    public int getPageNumber() {
        return 0;
    }

    @Override
    public void removeAdapterPosition(int i) {

    }

    @Override
    public void flipButtons(boolean showCameraButton) {
        if (showCameraButton) {
            if (viewFlipper.getDisplayedChild() != 0) {
                viewFlipper.setDisplayedChild(0);
            }
        } else {
            if (viewFlipper.getDisplayedChild() != 1) {
                viewFlipper.setDisplayedChild(1);
            }
        }

    }


    int hours, minutes, seconds;
    boolean resumeTimer;

    public void startTimer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //checking if recording is starting from start
                if (txtvTimer.getText().toString().trim().equalsIgnoreCase("record")) {
                    seconds = 0;
                    minutes = 0;
                    hours = 0;
                }
                seconds++;
                if (seconds == 60) {
                    seconds = 0;
                    minutes++;
                    if (minutes == 60) {
                        minutes = 0;
                        hours++;
                    }
                }
                if (minutes == 0) {
                    txtvTimer.setText("00:" + (seconds < 10 ? ("0" + seconds)
                            : seconds));
                } else if (hours == 0 && minutes > 0) {
                    txtvTimer.setText((minutes < 10 ? ("0" + minutes)
                            : minutes) + ":" + (
                            seconds < 10 ? ("0" + seconds) : seconds));
                } else if (hours > 0) {
                    txtvTimer.setText(hours + ":" + (minutes < 10 ? ("0" + minutes)
                            : minutes));
                }
                if (resumeTimer) {
                    startTimer();
                } else {
                    seconds = 0;
                    hours = 0;
                    minutes = 0;
                    txtvTimer.setText("00:00");
                }
            }
        }, 1000);

    }

    private void openCloseAttachmentOptions() {
        if (Build.VERSION.SDK_INT >= 21) {
            int cx = (cardAttachment.getLeft() + cardAttachment.getRight());
            int cy = (cardAttachment.getTop());

            int startradius = 0;
            int endradius = Math.max(cardAttachment.getWidth(), cardAttachment.getHeight());

            Animator animator = ViewAnimationUtils.createCircularReveal(cardAttachment, Math.round((findViewById(R.id.imgvAttachment)).getX()), Math.round((findViewById(R.id.imgvAttachment)).getY()), startradius, endradius);
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setDuration(400);
            int reverse_startradius = Math.max(cardAttachment.getWidth(), cardAttachment.getHeight());

            int reverse_endradius = 0;

            Animator animate = ViewAnimationUtils.createCircularReveal(cardAttachment, Math.round((findViewById(R.id.imgvAttachment)).getX()), Math.round((findViewById(R.id.imgvAttachment)).getY()), reverse_startradius, reverse_endradius);
            if (cardAttachment.getVisibility() == View.GONE || cardAttachment.getVisibility() == View.INVISIBLE) {
                cardAttachment.setVisibility(View.VISIBLE);
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                });
                UtillsG.zoomUpaBit(imgv_audio);
                UtillsG.zoomUpaBit(imgv_gallery);
                UtillsG.zoomUpaBit(imgv_video);
                UtillsG.zoomUpaBit(imgv_document);
                UtillsG.zoomUpaBit(imgv_pdf);
                UtillsG.zoomUpaBit(imgv_text);

                animator.start();
            } else {
                cardAttachment.setVisibility(View.VISIBLE);

                animate.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        cardAttachment.setVisibility(View.INVISIBLE);
                    }
                });
                animate.start();
            }
        } else {
            if (cardAttachment.getVisibility() == View.GONE || cardAttachment.getVisibility() == View.INVISIBLE) {
                cardAttachment.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_down_incoming));
                UtillsG.zoomUpaBit(imgv_audio);
                UtillsG.zoomUpaBit(imgv_gallery);
                UtillsG.zoomUpaBit(imgv_video);
                UtillsG.zoomUpaBit(imgv_document);
                UtillsG.zoomUpaBit(imgv_pdf);
                UtillsG.zoomUpaBit(imgv_text);
                cardAttachment.setVisibility(View.VISIBLE);
            } else {
                cardAttachment.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_up_outgoing));
                cardAttachment.setVisibility(View.INVISIBLE);
            }
        }
    }

    public void blockUserCheck(UserDataModel.UserData mOtherUserData) {
        if (UtillsG.getNotNullString(mOtherUserData.getIs_blocked(), "").equals(Web.BlockUserStates.BlockByMe)) {
            blockUser(false);
        } else if (UtillsG.getNotNullString(mOtherUserData.getIs_blocked(), "").equals(Web.BlockUserStates.Noblock)) {
            blockUser(true);
        } else if (UtillsG.getNotNullString(mOtherUserData.getIs_blocked(), "").equals(Web.BlockUserStates.BlockByOther)) {

        }
    }

    public void blockUser(final Boolean isBlockUser) {
        UtillsG.showLoading(isBlockUser ? "Blocking user, Please wait"
                : "Unblocking user, Please wait", mActivity);
        Call<BasicApiModel> quickLectureApiModelCall = LVApplication.getRetrofit().create(ChatApis.class).block_unblock_user
                (mOtherUserModel.getUser_id(), new SharedPrefHelper(mActivity).getUserId(),
                        isBlockUser ? "1" : "0"
                );
        quickLectureApiModelCall.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, final Response<BasicApiModel> response) {
                UtillsG.hideLoading();
                if (response.body() != null) {

                    if (response.body().getStatus()) {

                        if (isBlockUser) {
                            mOtherUserModel.setIs_blocked("1");
                            blockUserCard.setVisibility(View.VISIBLE);
                            imgv_sendMsg.setEnabled(false);
                        } else {
                            mOtherUserModel.setIs_blocked("0");
                            blockUserCard.setVisibility(View.INVISIBLE);
                            imgv_sendMsg.setEnabled(true);
                        }
                        edMessage.setText("");
                        Extension_file = "";
                        selectedFilePath = null;
                        recordedAudioPath = "";
                        llRecordedAudio.setVisibility(View.GONE);
                        if (recordedAudioPlayer != null) {
                            recordedAudioPlayer.stop();
                            recordedAudioPlayer = null;
                            imgbPlayAudio.setImageResource(android.R.drawable.ic_media_play);
                            seekBAr.setProgress(0);
                        }
                        if (mActivity != null) {
                            UtillsG.showToast(isBlockUser ? "User blocaked successfully"
                                    : " User unblock successfully", mActivity, true);
                        }
                    } else {
                        if (mActivity != null) {
                            UtillsG.showToast(response.message(), mActivity, true);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                UtillsG.hideLoading();
                Log.e("Error", t.toString());
                UtillsG.showToast("Error in blocking / unblocking user", mActivity, true);
            }
        });
    }


    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    private void implementEnterIsSendFunctionality() {
        if (new SharedPrefHelper(getActivityG()).isEnterIsSend()) {
            edMessage.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if (-1 != edMessage.getText().toString().indexOf("\n")) {
                        if (edMessage.getText().toString().length() > 1) {
                            String messageText = edMessage.getText().toString().substring(0, edMessage.getText().toString().length() - 3);
                            edMessage.setText(messageText);
                        }
                        imgv_sendMsg.performClick();
                        edMessage.setText("");

                    }
                }
            });
        } else {
            edMessage.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }
    }

}
