package gilm.lecture.verb.Features.Help;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;

import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.DialogHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class HelpFragment extends Fragment implements TextWatcher
{
    EditText edFindAnswer;
    RecyclerView recyQuestionsAnswers;
    View txtvReport, txtvSupport;
    ArrayList<QuestionAnswerModel> alQuestionsAnswers = new ArrayList<>();
    ArrayList<QuestionAnswerModel> alQuestionsAnswers_Search = new ArrayList<>();

    public String searchString = "";

    public static HelpFragment newInstance() {
        return new HelpFragment();
    }

    public HelpFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help, container, false);

        txtvReport = view.findViewById(R.id.txtvReport);
        txtvSupport = view.findViewById(R.id.txtvSupport);
        recyQuestionsAnswers = view.findViewById(R.id.recyQuestionsAnswers);
        edFindAnswer = view.findViewById(R.id.edFindAnswer);
        edFindAnswer.addTextChangedListener(this);

        showQuestionAnswers();

        txtvReport.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                DialogHelper.getInstance().showReportDialog(getActivity());
            }
        });
        txtvSupport.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                SupportActivity.start(getActivity());
            }
        });

        return view;
    }

    private void showQuestionAnswers() {
        String[] questionArray = getResources().getStringArray(R.array.questions_array);
        String[] answersArray = getResources().getStringArray(R.array.answers_array);

        for (int i = 0; i < questionArray.length; i++) {
            QuestionAnswerModel model = new QuestionAnswerModel();
            model.setQuestion(questionArray[i]);
            model.setAnswer(answersArray[i]);
            alQuestionsAnswers.add(model);
        }
        recyQuestionsAnswers.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        QuestionAnswerAdapter adapter = new QuestionAnswerAdapter(alQuestionsAnswers, this);
        adapter.setShouldLoadMore(false);
        recyQuestionsAnswers.setAdapter(adapter);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        searchString = charSequence.toString().toLowerCase();
        alQuestionsAnswers_Search.clear();
        for (int j = 0; j < alQuestionsAnswers.size(); j++) {
            if (alQuestionsAnswers.get(j).getQuestion().toLowerCase().contains(searchString) || alQuestionsAnswers.get(j).getAnswer().toLowerCase().contains(searchString)) {
                alQuestionsAnswers_Search.add(alQuestionsAnswers.get(j));
            }
        }
        QuestionAnswerAdapter adapter = new QuestionAnswerAdapter(alQuestionsAnswers_Search, this);
        adapter.setShouldLoadMore(false);
        recyQuestionsAnswers.setAdapter(adapter);
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }
}
