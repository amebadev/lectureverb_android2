/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.QuickLecture.Presentation.PresentationOptions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Discovery.DiscoveryActivity;
import gilm.lecture.verb.Features.InterestsSelection.AddInterests.AddInterestCategoriesActivity;
import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.Features.Internal.Base.BaseActivityNoBinding;
import gilm.lecture.verb.Features.QuickLecture.Presentation.RecordPresentation.RecordPresentationActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.BitmapDecoderG;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.Web;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

public class PresentationOptionsActivity extends BaseActivityNoBinding<PresentationOptionsPresenter> implements PresentationOptionsView, View.OnClickListener {
String group_id="";
    TextView txtvAddPresentationDiscoveryUrl, tvUploadPresentation, txtvContinue, tvCoverArt, txtvLectureCategory;
    String lectureTitle = "";
    List<SingleImageModel> imagesList;
    List<SingleImageModel> alImages2 = new ArrayList<>();
    String commaSeparatedIds = "", commaSeparatedNames = "";

    public static void start(Context context, String lectureTitle) {
        Intent starter = new Intent(context, PresentationOptionsActivity.class);
        starter.putExtra("lectureTitle", lectureTitle);
        context.startActivity(starter);
        ((Activity) context).finish();
    }   public static void start(Context context, String lectureTitle,String group_id) {
        Intent starter = new Intent(context, PresentationOptionsActivity.class);
        starter.putExtra("lectureTitle", lectureTitle);
        starter.putExtra("group_id",group_id);

        context.startActivity(starter);
        ((Activity) context).finish();
    }
    @Override
    protected int setLayoutId() {
        return R.layout.activity_presentation_options;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new PresentationOptionsPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        setupToolbar("Presentation Options");

        lectureTitle = getIntent().getStringExtra("lectureTitle");
        group_id = UtillsG.getNotNullString(getIntent().getStringExtra("group_id"),"0");

        tvCoverArt = (TextView) findViewById(R.id.tvCoverArt);
        txtvLectureCategory = (TextView) findViewById(R.id.txtvLectureCategory);
        txtvAddPresentationDiscoveryUrl = (TextView) findViewById(R.id.txtvAddPresentationDiscoveryUrl);
        txtvAddPresentationDiscoveryUrl.setOnClickListener(this);

        txtvContinue = (TextView) findViewById(R.id.txtvContinue);
        tvUploadPresentation = (TextView) findViewById(R.id.tvUploadPresentation);
        tvUploadPresentation.setOnClickListener(this);
        txtvContinue.setVisibility(View.GONE);
        txtvContinue.setOnClickListener(this);
        tvCoverArt.setOnClickListener(this);
        findViewById(R.id.imgvDiscoveryUrl).setOnClickListener(this);
        findViewById(R.id.imgvUploadPPT).setOnClickListener(this);
        findViewById(R.id.imgvCoverArt).setOnClickListener(this);

        txtvLectureCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                InterestsSelectionActivity.startfOResult(getActivityG(),"Select Lecture Category",commaSeparatedNames);
                AddInterestCategoriesActivity.start(getActivityG(), true, commaSeparatedIds, commaSeparatedNames);
            }
        });

    }

    @Override
    public Context getActivityG() {
        return PresentationOptionsActivity.this;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgvDiscoveryUrl:
                DialogHelper.getInstance().showInformation(getActivityG(), "You can surf internet and choose a suitable url for showing your discovery about the post.", new CallBackG<String>() {
                    @Override
                    public void onCallBack(String output) {

                    }
                });

                break;
            case R.id.imgvUploadPPT:
                DialogHelper.getInstance().showInformation(getActivityG(), "You can choose some images or upload a PPT file to start recording presentation lecture.", new CallBackG<String>() {
                    @Override
                    public void onCallBack(String output) {

                    }
                });

                break;
            case R.id.imgvCoverArt:
                DialogHelper.getInstance().showInformation(getActivityG(), "You can choose an image from Gallery or Capture it using camera option to upload it as a cover art pic for your post.", new CallBackG<String>() {
                    @Override
                    public void onCallBack(String output) {

                    }
                });

                break;
            case R.id.tvUploadPresentation:
                DialogHelper.getInstance().showPresentationOptions(getActivityG());

                break;
            case R.id.txtvAddPresentationDiscoveryUrl:
                DiscoveryActivity.start(this);
                break;
            case R.id.txtvContinue:
                if (UtillsG.getNotNullString(getPresenter().artWorkImage.trim(), "").isEmpty()) {
                    displayError("Please select Cover Art.");
                } else if (commaSeparatedIds.trim().isEmpty()) {
                    displayError("Please select lecture category");
                } else {
                    RecordPresentationActivity.start(getActivityG(), alImages2, getPresenter().discoveryUrl, getPresenter().artWorkImage, commaSeparatedIds,group_id);
                }
                break;
            case R.id.tvCoverArt:
                BitmapDecoderG.selectImage(getActivityG(), null);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.RequestCode.DISCOVERY) {
                txtvAddPresentationDiscoveryUrl.setText(data.getStringExtra(Constants.Extras.DATA));
                getPresenter().setDiscoveryUrl(data.getStringExtra(Constants.Extras.DATA));
            } else if (requestCode == Constants.RequestCode.PPT_SELECTOR) {
                tvUploadPresentation.setText(Html.fromHtml("<b>Selected :<b> " + data.getStringExtra("name")));
                getPresenter().setPPTPath(data.getStringExtra("path"));

                getPresenter().uploadPptToServer();

            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    File file = new File(Environment.getExternalStorageDirectory().toString() + File.separator + Web.LocalFolders.LectureVerbThumbs + "lectureThumbnail.png");
                    FileOutputStream out = null;
                    try {
                        if (bitmap != null) {
                            out = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (out != null) {
                                out.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    tvCoverArt.setText(file.getAbsolutePath());
                    getPresenter().setArtWorkImage(Uri.fromFile(file).getPath());
                }
            } else if (requestCode == Constants.RequestCode.MULTIPLE_IMAGE_PICKER) {
                List<String> imagesPaths = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                alImages2.clear();
                for (int i = 0; i < imagesPaths.size(); i++) {
                    SingleImageModel model = new SingleImageModel();
                    model.setDownload_file(imagesPaths.get(i));
                    alImages2.add(model);
                }
                tvUploadPresentation.setText(alImages2.size() + " Image(s) Selected.");
                enableDisableContinueButton();
            } else if (requestCode == Constants.RequestCode.INTEREST_SELECTION) {

                commaSeparatedIds = "";
                commaSeparatedNames = "";
                ArrayList<ChildCategoryModel> alCategories = (ArrayList<ChildCategoryModel>) data.getSerializableExtra(Constants.Extras.DATA);
                for (int i = 0; i < alCategories.size(); i++) {
                    commaSeparatedIds = commaSeparatedIds + (commaSeparatedIds.isEmpty() ? ""
                            : ",") + alCategories.get(i).getId();
                    commaSeparatedNames = commaSeparatedNames + (
                            commaSeparatedNames.isEmpty() ? ""
                                    : ", ") + alCategories.get(i).getCategory();
                }
                getPresenter().setSelectedLectureCategories(commaSeparatedIds);
                txtvLectureCategory.setText(commaSeparatedNames);
            } else {
                getPresenter().cropMeImage(BitmapDecoderG.onActivityResult(getActivityG(), requestCode, resultCode, data));
            }

        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void PPT_into_Images_Done(List<SingleImageModel> imagesList) {
        this.imagesList = imagesList;
        alImages2.clear();
        for (int i = 0; i < imagesList.size(); i++) {
            if (!imagesList.get(i).getDownload_file().toLowerCase().endsWith("zip")) {
                imagesList.get(i).setDownload_file(Web.Path.BASE_URL + imagesList.get(i).getDownload_file());
                alImages2.add(imagesList.get(i));
            }
        }


        enableDisableContinueButton();

    }

    private void enableDisableContinueButton() {
        if (this.alImages2.size() > 0) {
            txtvContinue.setVisibility(View.VISIBLE);
        } else {
            txtvContinue.setVisibility(View.GONE);
        }
    }
}
