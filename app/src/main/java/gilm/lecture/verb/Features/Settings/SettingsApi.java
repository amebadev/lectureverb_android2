/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Settings;

import gilm.lecture.verb.Features.Home.Home.ArrayListTagsModel;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.Web;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by G-Expo on 17 Aug 2017.
 */

public interface SettingsApi {
    @FormUrlEncoded
    @POST(Web.Path.get_ash_tags)
    Call<ArrayListTagsModel> get_ash_tags(@Field(Web.Keys.USER_ID) String user_id);

    @FormUrlEncoded
    @POST(Web.Path.set_profile_privacy)
    Call<BasicApiModel> set_profile_privacy(@Field(Web.Keys.USER_ID) String user_id, @Field(Web.Keys.privacy) String privacy);

    @FormUrlEncoded
    @POST(Web.Path.get_profile_privacy)
    Call<BasicApiModel> get_profile_privacy(@Field(Web.Keys.USER_ID) String user_id);

    @FormUrlEncoded
    @POST(Web.Path.add_user_ash_tags)
    Call<BasicApiModel> add_user_ash_tags(@Field(Web.Keys.USER_ID) String user_id, @Field(Web.Keys.tag_name) String tag_name);


}
