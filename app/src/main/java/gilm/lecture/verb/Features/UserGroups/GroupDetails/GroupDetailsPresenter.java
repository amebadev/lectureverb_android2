/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.UserGroups.GroupDetails;

import android.support.annotation.NonNull;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.UserGroups.GroupModel;

/**
 * Created by G-Expo on 21 Jul 2017.
 */

public class GroupDetailsPresenter extends BasePresenter<GroupDetailsView>
{

    private final GroupModel groupModel;
    public String bioDefaultText = "No bio available ..!";


    public GroupDetailsPresenter(GroupModel groupModel) {
        this.groupModel = groupModel;
    }

    public void initLocalData() {

    }

    @Override
    public void attachView(@NonNull GroupDetailsView viewable) {
        super.attachView(viewable);
        initLocalData();
    }

    public void refreshUserData() {
        //refresh data from local storage here.
        initLocalData();
    }

}
