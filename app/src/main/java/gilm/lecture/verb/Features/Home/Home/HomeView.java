package gilm.lecture.verb.Features.Home.Home;

import android.view.View;

import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 06 Jul 2017.
 */
public interface HomeView extends Viewable<HomePresenter>
{
    void showData(List<HomeViewModel> list);

    void showOptionsOnAdapterItem(View view);
}

