package gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 07 Jul 2017.
 */

public interface CreateLectureEventView extends Viewable<CreateLectureEventPresenter>
{
    String getSelectedCoHostId();

    String getSelectedLecturerId();

    String getSelectedGroupId();
}
