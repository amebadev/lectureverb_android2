/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Settings.MainSettings;

import android.view.View;

import gilm.lecture.verb.Features.Help.HelpFragment;
import gilm.lecture.verb.Features.MyProfile.Edit.ProfileSettingActivity;
import gilm.lecture.verb.Features.Settings.AccountSettings.AccountSettings;
import gilm.lecture.verb.Features.Settings.ContactsSettings.ContactsSettings;
import gilm.lecture.verb.Features.Settings.DataAndStorage.DataStorageActivity;
import gilm.lecture.verb.Features.Settings.Imprint.ImprintActivity;
import gilm.lecture.verb.Features.Settings.Message.MessagesSettingFragment;
import gilm.lecture.verb.Features.Settings.Notifications.NotificationSettingsFragment;
import gilm.lecture.verb.Features.Settings.SettingBasePresenter;
import gilm.lecture.verb.Features.Settings.Tags.TagSettingsFragment;
import gilm.lecture.verb.Features.Splash.SplashActivity;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.TitleNavigator;

/**
 * Created by G-Expo on 20 Jul 2017.
 */

public class SettingsFragmentPresenter extends SettingBasePresenter<SettingsFragmentView> implements MainSettingsBinder
{

    @Override
    public void openProfileSettings(View view) {
//        TitleNavigator.getInstance().ShowMyProfile(MyProfileFragment.newInstance());
        ProfileSettingActivity.start(getView().getActivityG());
    }

    @Override
    public void openMessageSettings(View view) {
        TitleNavigator.getInstance().ShowMessageSettings(MessagesSettingFragment.newInstance());
    }

    @Override
    public void openNotificationSettings(View view) {
        TitleNavigator.getInstance().ShowNotificationSettings(NotificationSettingsFragment.newInstance());
    }

    @Override
    public void openTagSettings(View view) {
        TitleNavigator.getInstance().ShowTagSettings(TagSettingsFragment.newInstance());
    }

    @Override
    public void openHelp(View view) {
        TitleNavigator.getInstance().ShowHelpScreen(HelpFragment.newInstance());
    }

    @Override
    public void logOut(View view) {
        DialogHelper.getInstance().logOutDialog(getView().getActivityG(), new CallBackG()
        {
            @Override
            public void onCallBack(Object output) {
                getView().getLocalData().logOut();
                SplashActivity.start(getView().getActivityG());
            }
        });
    }

    @Override
    public void openAccountSettings(View view) {
        AccountSettings.start(getView().getActivityG());
    }

    @Override
    public void openContactsSettings(View view) {
        ContactsSettings.start(getView().getActivityG());
    }

    @Override
    public void openImprintActivity(View view) {
        ImprintActivity.start(getView().getActivityG());
    }

    @Override
    public void openDataStorageSettings(View view) {
        DataStorageActivity.start(getView().getActivityG());
    }


}
