package gilm.lecture.verb.Features.Home.Home;

import java.io.Serializable;
import java.util.ArrayList;

import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.IsInterestedModel;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.UserGroups.GroupModel;

/**
 * created by PARAMBIR SINGH on 17/10/17.
 */

public class Lecture_And_Event_Model implements Serializable
{
    /**
     * Below are the parameters for an lectures.
     */
    private String quick_lecture_id,
            type = "lecture",
            user_id,
            quick_lecture_type,
            file_path,
            quick_lecture_text,
            thumbnail,
            discovery_url,
            privacy,
            date_time,
            comments_count,
            repost_lecture_count,
            likes_count,
            favourites_count,
            lecture_view,
            is_liked,
            is_favourite, quick_lecture_time_duration, reposted_lecture_id, history_stack, reposted_date_time;


    private Boolean is_reposted = false, isUpcoming = true;
    private UserDataModel.UserData Userdata;



    public GroupModel getGroupmodel() {
        return Groupmodel;
    }

    public void setGroupmodel(GroupModel groupModel) {
        this.Groupmodel = groupModel;
    }
    private GroupModel Groupmodel;

    private UserDataModel.UserData OwnerUserdata;
    private GroupModel UserGroupData;


    /**
     * Below are the parameters for an event.
     */
    private String event_id;
    private String lecture_title;
    private String lecture_poster;
    private String recording_type;
    private String address;
    private String latitude;
    private String longitude;
    private String details;
    private String can_invite_friends;
    private ArrayList<UserDataModel.UserData> lecturer_co_hosts;
    private String lecture_subject_tags;
    private String isPublic;
    private String lecture_start_date_time;
    private String lecture_end_date_time;
    private String lecture_created_date_time;

    private IsInterestedModel Isinterested = null;
    private ArrayList<ChildCategoryModel> event_interest;


    public String getReposted_date_time() {
        return reposted_date_time;
    }

    public void setReposted_date_time(String reposted_date_time) {
        this.reposted_date_time = reposted_date_time;
    }

    public String getHistory_stack() {
        return history_stack;
    }

    public void setHistory_stack(String history_stack) {
        this.history_stack = history_stack;
    }

    public UserDataModel.UserData getUserdata() {
        return Userdata;
    }

    public void setUserdata(UserDataModel.UserData userdata) {
        Userdata = userdata;
    }

    public String getRepost_lecture_count() {
        return repost_lecture_count;
    }

    public void setRepost_lecture_count(String repost_lecture_count) {
        this.repost_lecture_count = repost_lecture_count;
    }

    public Boolean getUpcoming() {
        return isUpcoming;
    }

    public void setUpcoming(Boolean upcoming) {
        isUpcoming = upcoming;
    }

    public UserDataModel.UserData getOwnerUserdata() {
        return OwnerUserdata;
    }

    public void setOwnerUserdata(UserDataModel.UserData ownerUserdata) {
        OwnerUserdata = ownerUserdata;
    }

    public IsInterestedModel getIsinterested() {
        return Isinterested;
    }

    public void setIsinterested(IsInterestedModel isinterested) {
        Isinterested = isinterested;
    }

    public ArrayList<ChildCategoryModel> getEvent_interest() {
        return event_interest;
    }

    public void setEvent_interest(ArrayList<ChildCategoryModel> event_interest) {
        this.event_interest = event_interest;
    }

    public Boolean getIs_reposted() {
        return is_reposted;
    }

    public void setIs_reposted(Boolean is_reposted) {
        this.is_reposted = is_reposted;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getLecture_title() {
        return lecture_title;
    }

    public void setLecture_title(String lecture_title) {
        this.lecture_title = lecture_title;
    }

    public String getLecture_poster() {
        return lecture_poster;
    }

    public void setLecture_poster(String lecture_poster) {
        this.lecture_poster = lecture_poster;
    }

    public String getRecording_type() {
        return recording_type;
    }

    public void setRecording_type(String recording_type) {
        this.recording_type = recording_type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getCan_invite_friends() {
        return can_invite_friends;
    }

    public void setCan_invite_friends(String can_invite_friends) {
        this.can_invite_friends = can_invite_friends;
    }

    public ArrayList<UserDataModel.UserData> getLecturer_co_hosts() {
        return lecturer_co_hosts;
    }

    public void setLecturer_co_hosts(ArrayList<UserDataModel.UserData> lecturer_co_hosts) {
        this.lecturer_co_hosts = lecturer_co_hosts;
    }

    public String getLecture_subject_tags() {
        return lecture_subject_tags;
    }

    public void setLecture_subject_tags(String lecture_subject_tags) {
        this.lecture_subject_tags = lecture_subject_tags;
    }

    public String getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(String isPublic) {
        this.isPublic = isPublic;
    }

    public String getLecture_start_date_time() {
        return lecture_start_date_time;
    }

    public void setLecture_start_date_time(String lecture_start_date_time) {
        this.lecture_start_date_time = lecture_start_date_time;
    }

    public String getLecture_end_date_time() {
        return lecture_end_date_time;
    }

    public void setLecture_end_date_time(String lecture_end_date_time) {
        this.lecture_end_date_time = lecture_end_date_time;
    }

    public String getLecture_created_date_time() {
        return lecture_created_date_time;
    }

    public void setLecture_created_date_time(String lecture_created_date_time) {
        this.lecture_created_date_time = lecture_created_date_time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuick_lecture_id() {
        return quick_lecture_id;
    }

    public void setQuick_lecture_id(String quick_lecture_id) {
        this.quick_lecture_id = quick_lecture_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getQuick_lecture_type() {
        return quick_lecture_type;
    }

    public void setQuick_lecture_type(String quick_lecture_type) {
        this.quick_lecture_type = quick_lecture_type;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getQuick_lecture_text() {
        return quick_lecture_text;
    }

    public void setQuick_lecture_text(String quick_lecture_text) {
        this.quick_lecture_text = quick_lecture_text;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDiscovery_url() {
        return discovery_url;
    }

    public void setDiscovery_url(String discovery_url) {
        this.discovery_url = discovery_url;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getComments_count() {
        return comments_count;
    }

    public void setComments_count(String comments_count) {
        this.comments_count = comments_count;
    }

    public String getReposted_count() {
        return repost_lecture_count;
    }

    public void setReposted_count(String repost_lecture_count) {
        this.repost_lecture_count = repost_lecture_count;
    }

    public String getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(String likes_count) {
        this.likes_count = likes_count;
    }

    public String getFavourites_count() {
        return favourites_count;
    }

    public void setFavourites_count(String favourites_count) {
        this.favourites_count = favourites_count;
    }

    public String getLecture_view() {
        return lecture_view;
    }

    public void setLecture_view(String lecture_view) {
        this.lecture_view = lecture_view;
    }

    public String getIs_liked() {
        return is_liked;
    }

    public void setIs_liked(String is_liked) {
        this.is_liked = is_liked;
    }

    public String getIs_favourite() {
        return is_favourite;
    }

    public void setIs_favourite(String is_favourite) {
        this.is_favourite = is_favourite;
    }

    public String getQuick_lecture_time_duration() {
        return quick_lecture_time_duration;
    }

    public void setQuick_lecture_time_duration(String quick_lecture_time_duration) {
        this.quick_lecture_time_duration = quick_lecture_time_duration;
    }

    public String getReposted_lecture_id() {
        return reposted_lecture_id;
    }

    public void setReposted_lecture_id(String reposted_lecture_id) {
        this.reposted_lecture_id = reposted_lecture_id;
    }

    public GroupModel getUserGroupData()
    {
        return UserGroupData;
    }

    public void setUserGroupData(GroupModel userGroupData)
    {
        UserGroupData = userGroupData;
    }
}
