/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Help;

import gilm.lecture.verb.Features.Settings.ArrayListLectureModel;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.Web;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HelpApi
{
    @GET(Web.Path.send_report_email)
    Call<BasicApiModel> send_report_email(@Query(Web.Keys.USER_ID) String user_id, @Query(Web.Keys.message) String message);

    @GET(Web.Path.get_general_answers)
    Call<ArrayListLectureModel> get_general_answers(@Query(Web.Keys.USER_ID) String user_id);
}
