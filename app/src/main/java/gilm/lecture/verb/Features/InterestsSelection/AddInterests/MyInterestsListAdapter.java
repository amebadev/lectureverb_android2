package gilm.lecture.verb.Features.InterestsSelection.AddInterests;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.Features.InterestsSelection.InterestSelectionApi;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.InfiniteAdapter_WithoutBuinding;
import gilm.lecture.verb.UtilsG.LoadingViewHolder;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * created by PARAMBIR SINGH on 31/8/17.
 */

class MyInterestsListAdapter extends InfiniteAdapter_WithoutBuinding<MyInterestsListAdapter.MyViewHolderG>
{

    private LayoutInflater inflater;
    List<ChildCategoryModel> data;
    Context context;

    public MyInterestsListAdapter(List<ChildCategoryModel> data, Context context) {
        this.data = data;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        return new LoadingViewHolder(inflater.inflate(R.layout.layout_progress, parent, false));
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public MyViewHolderG onCreateView(ViewGroup parent, int viewType) {
        return new MyViewHolderG(inflater.inflate(R.layout.inflater_interests, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        try {
            if (!(holder instanceof LoadingViewHolder)) {
                ((MyViewHolderG) holder).txtvTitle.setText(data.get(position).getCategory());
                UtillsG.setTextSizeByPercentage(context, ((MyViewHolderG) holder).txtvTitle, 3f);
                ((MyViewHolderG) holder).txtvParent.setText("New");
                ((MyViewHolderG) holder).llInterest.setBackgroundResource(R.drawable.btn_light_blue_selected);
                ((MyViewHolderG) holder).txtvParent.setVisibility(
                        data.get(position).isNew() ? View.VISIBLE
                                : View.INVISIBLE);
                ((MyViewHolderG) holder).txtvParent.setTextColor(ContextCompat.getColor(context, R.color.yellow));
                ((MyViewHolderG) holder).txtvParent.setTypeface(Typeface.DEFAULT_BOLD);

                ((MyViewHolderG) holder).llInterest.setOnLongClickListener(new View.OnLongClickListener()
                {
                    @Override
                    public boolean onLongClick(View view) {
                        if (context instanceof AddInterestCategoriesActivity && !((AddInterestCategoriesActivity) context).isLectureEventCat) {
                            openConfirmationDialog(position);
                        }
                        else if (context instanceof AddInterestCategoriesActivity && ((AddInterestCategoriesActivity) context).isLectureEventCat) {
                            openConfirmationDialogForLectureEvents(position);

                        }
                        return false;
                    }
                });
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openConfirmationDialogForLectureEvents(final int position) {
        DialogHelper.getInstance().showWith2Action(context,
                "Remove",
                "Cancel",
                context.getResources().getString(R.string.app_name),
                "Do you want to remove \"" + data.get(position).getCategory() + "\" from List?",
                new CallBackG<String>()
                {
                    @Override
                    public void onCallBack(String output) {
                        data.remove(position);
                        notifyDataSetChanged();
                    }
                });
    }

    private void openConfirmationDialog(final int position) {
        DialogHelper.getInstance().showWith2Action(context,
                "Remove",
                "Cancel",
                context.getResources().getString(R.string.app_name),
                "Do you want to remove \"" + data.get(position).getCategory() + "\" from your Interest List?",
                new CallBackG<String>()
                {
                    @Override
                    public void onCallBack(String output) {
                        Call<BasicApiModel> call = LVApplication.getRetrofit().create(InterestSelectionApi.class)
                                .remove_user_category(data.get(position).getId(), ((AddInterestCategoriesActivity) context).getLocalData().getUserId());
                        call.enqueue(new Callback<BasicApiModel>()
                        {
                            @Override
                            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                                UtillsG.showToast(response.body().getMessage(), context, true);
                                if (response.body().getStatus()) {
                                    data.remove(position);
                                    notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                                UtillsG.showToast(t.getMessage(), context, true);
                            }
                        });
                    }
                });
    }

    class MyViewHolderG extends RecyclerView.ViewHolder
    {
        View view;
        TextView txtvTitle, txtvParent;
        LinearLayout llInterest;

        public MyViewHolderG(View row) {
            super(row);

            view = row;
            txtvParent = (TextView) view.findViewById(R.id.txtvParent);
            txtvTitle = (TextView) view.findViewById(R.id.txtvTitle);
            llInterest = (LinearLayout) view.findViewById(R.id.llInterest);
        }
    }

}
