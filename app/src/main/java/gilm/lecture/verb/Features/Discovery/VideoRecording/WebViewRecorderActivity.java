package gilm.lecture.verb.Features.Discovery.VideoRecording;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.display.VirtualDisplay;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Internal.Base.BaseActivityNoBinding;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.PKromeProgressBar;
import gilm.lecture.verb.UtilsG.WebInnerStackModel;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class WebViewRecorderActivity extends BaseActivityNoBinding<WebViewRecorderPresenter> implements WebViewRecorderView, View.OnClickListener
{

    int hours, minutes, seconds;

    TextView txtvTime, txtvTitle;
    String recordedVideoPath = "";

    private static final String TAG = "MainActivity";
    private MediaProjectionManager mProjectionManager;
    private MediaProjection mMediaProjection;
    private VirtualDisplay mVirtualDisplay;
    public MediaProjectionCallback mMediaProjectionCallback;
    private MediaRecorder mMediaRecorder;

    String startingUrl;
    private WebView webView;
    ImageView imgvBack, imgvHome;
    PKromeProgressBar pkProgressBar;
    boolean isOnlyBrowsing;
    ImageView imgvPlayPause;
    Boolean isPause = false;

    ArrayList<WebInnerStackModel> alWebHistoryStack = new ArrayList<>();

    public static void startForResult(Context context, int requestCode, String startingUrl)
    {
        if (!startingUrl.contains("http"))
        {
            startingUrl = "https://" + startingUrl;
        }
        Intent starter = new Intent(context, WebViewRecorderActivity.class);
        starter.putExtra(Constants.Extras.startingUrl, startingUrl);
        ((Activity) context).startActivityForResult(starter, requestCode);
    }

    public static void startForBrowsingOnly(Context context, String startingUrl)
    {
        if (!startingUrl.contains("http"))
        {
            startingUrl = "https://" + startingUrl;
        }
        Intent starter = new Intent(context, WebViewRecorderActivity.class);
        starter.putExtra(Constants.Extras.startingUrl, startingUrl);
        starter.putExtra("isOnlyBrowsing", true);
        context.startActivity(starter);
    }

    @Override
    public MediaRecorder getRecorder()
    {
        return mMediaRecorder;
    }


    @Override
    protected int setLayoutId()
    {
        return R.layout.activity_web_view_recorder;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreateActivityG()
    {
        injectPresenter(new WebViewRecorderPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public Context getActivityG()
    {
        return WebViewRecorderActivity.this;
    }


    @Override
    public void initViews()
    {
        isOnlyBrowsing = getIntent().getBooleanExtra("isOnlyBrowsing", false);
        startingUrl = getIntent().getStringExtra(Constants.Extras.startingUrl);


        pkProgressBar = (PKromeProgressBar) findViewById(R.id.pkProgressBar);
        txtvTitle = (TextView) findViewById(R.id.txtvTitle);
        txtvTime = (TextView) findViewById(R.id.txtvTime);
        imgvBack = (ImageView) findViewById(R.id.imgvBack);
        imgvHome = (ImageView) findViewById(R.id.imgvHome);
        imgvPlayPause = (ImageView) findViewById(R.id.imgvPlayPause);
        imgvPlayPause.setVisibility(View.GONE);
        imgvPlayPause.setOnClickListener(this);

        imgvHome.setOnClickListener(this);
        imgvBack.setOnClickListener(this);

        if (!isOnlyBrowsing)
        {

            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            getPresenter().screenDensity = metrics.densityDpi;

            mMediaRecorder = new MediaRecorder();

            mProjectionManager = (MediaProjectionManager) getSystemService
                    (Context.MEDIA_PROJECTION_SERVICE);


            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    getPresenter().onToggleScreenShare();
                }
            }, 200);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                imgvPlayPause.setVisibility(View.VISIBLE);
            }

        } else
        {
            txtvTime.setVisibility(View.GONE);
        }

        initWebView();
    }

    private void initWebView()
    {

        webView = (WebView) findViewById(R.id.webView);
        webView.loadUrl(startingUrl);
        webView.setWebViewClient(new WebViewClient()
        {
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return false;
            }
        });

        webView.setWebChromeClient(new WebChromeClient()
        {
            public void onProgressChanged(final WebView view, int progress)
            {

                pkProgressBar.setProgress(progress);

                txtvTitle.setText("Loading " + progress + "%");
                if (progress == 100)
                {
                    txtvTitle.setText(webView.getTitle());

                    WebInnerStackModel model = new WebInnerStackModel();
                    model.setTitle(view.getTitle());
                    model.setUrl(view.getUrl());
                    alWebHistoryStack.add(model);

                }
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUserAgentString("Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3");
    }


    @Override
    public void startTimer()
    {
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                if (!isPause)
                {
                    seconds++;
                    if (seconds == 60)
                    {
                        seconds = 0;
                        minutes++;
                        if (minutes == 60)
                        {
                            minutes = 0;
                            hours++;
                        }
                    }
                    if (minutes == 0)
                    {
                        txtvTime.setText("00:" + (seconds < 10 ? ("0" + seconds) : seconds));
                    } else if (hours == 0 && minutes > 0)
                    {
                        txtvTime.setText((minutes < 10 ? ("0" + minutes) : minutes) + ":" + (
                                seconds < 10 ? ("0" + seconds) : seconds));
                    } else if (hours > 0)
                    {
                        txtvTime.setText(hours + ":" + (minutes < 10 ? ("0" + minutes) : minutes));
                    }

                    if (minutes < 5)
                    {
                        startTimer();
                    } else
                    {
                        stopRecordingOrFinish();
                    }
                }
            }
        }, 1000);
    }


    @Override
    public void hideRecordingButton()
    {
//        mToggleButton.setVisibility(View.GONE);
//        (findViewById(R.id.txtvTapToRecord)).setVisibility(View.GONE);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void shareScreen()
    {
        if (mMediaProjection == null)
        {
            startActivityForResult(mProjectionManager.createScreenCaptureIntent(), Constants.RequestCode.SCREEN_RECORDER);
            return;
        }
        mVirtualDisplay = getPresenter().createVirtualDisplay();
        mMediaRecorder.start();

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.imgvHome:
                webView.loadUrl("https://www.google.co.in/");
                break;
            case R.id.imgvBack:
                if (!isOnlyBrowsing && webView.canGoBack())
                {
                    webView.goBack();
                } else
                {
                    if (isOnlyBrowsing)
                    {
                        finish();
                    }
                }
                break;
            case R.id.imgvPlayPause:
                if (mMediaRecorder != null)
                {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    {
                        if (isPause)
                        {
                            startTimer();
                            isPause = false;
                            mMediaRecorder.resume();
                            imgvPlayPause.setImageResource(R.drawable.ic_pause);
                        } else
                        {

                            isPause = true;
                            mMediaRecorder.pause();
                            imgvPlayPause.setImageResource(R.drawable.ic_play);
                        }
                    }
                }

                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public class MediaProjectionCallback extends MediaProjection.Callback
    {
        @Override
        public void onStop()
        {
            if (getPresenter().isRecording)
            {
                getPresenter().isRecording = false;
                mMediaRecorder.stop();
                mMediaRecorder.reset();
                Log.v(TAG, "Recording Stopped");
            }
            mMediaProjection = null;
            stopScreenSharing();
        }
    }


    @Override
    public MediaProjection getMediaProjection()
    {
        return mMediaProjection;
    }

    @Override
    public void setMediaProjectionNull()
    {
        mMediaProjection = null;
    }

    @Override
    public MediaProjection.Callback getMediaProjectionCallBack()
    {
        return mMediaProjectionCallback;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void initMediaProjectionCallBack(int resultCode, Intent data)
    {
        mMediaProjectionCallback = new WebViewRecorderActivity.MediaProjectionCallback();
        mMediaProjection = mProjectionManager.getMediaProjection(resultCode, data);
        mMediaProjection.registerCallback(mMediaProjectionCallback, null);
        mVirtualDisplay = getPresenter().createVirtualDisplay();
        mMediaRecorder.start();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void stopScreenSharing()
    {
        if (mVirtualDisplay == null)
        {
            return;
        }
        mVirtualDisplay.release();
        //mMediaRecorder.release(); //If used: mMediaRecorder object cannot
        // be reused again
        getPresenter().destroyMediaProjection();
    }

    @Override
    public void hideNotificationBar()
    {
        enableFullScreenMode();
    }


    @Override
    public void onBackPressed()
    {
        stopRecordingOrFinish();
    }

    private void stopRecordingOrFinish()
    {
        if (!isOnlyBrowsing)
        {
            if (getPresenter().isRecording)
            {
                stopRecording();
            } else
            {
                super.onBackPressed();
            }
        } else
        {
            if (webView.canGoBack())
            {
                webView.goBack();
            } else
            {
                super.onBackPressed();
            }
        }
    }

    private void stopRecording()
    {
        final boolean isNougat = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N);

        if (isNougat)
        {
            getPresenter().pauseRecording();
        } else
        {
            getPresenter().stopRecording();
        }

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivityG());
        dialog.setMessage(isNougat ? "Recording is paused, please Select the action."
                : "Do you want to Save this recording?");
        dialog.setPositiveButton("Save", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                if (isNougat)
                {
                    getPresenter().stopRecording();
                }

                Intent inn = new Intent();
                inn.putExtra(Constants.Extras.DATA, recordedVideoPath);
                inn.putExtra(Constants.Extras.currentUrl, webView.getUrl());
                inn.putExtra(Constants.Extras.TITLE, webView.getTitle());
                inn.putExtra(Constants.Extras.WEB_STACK, alWebHistoryStack);
                inn.putExtra("time", txtvTime.getText().toString());
                setResult(RESULT_OK, inn);
                finish();
            }
        });
        dialog.setNegativeButton(
                isNougat ? "Exit" : "Cancel", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        finish();
                    }
                });

        if (isNougat)
        {
            dialog.setNeutralButton("Resume", new DialogInterface.OnClickListener()
            {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    getPresenter().resumeRecording();
                }
            });
        }
        dialog.show();
    }

    @Override
    public void setVideoPath(String videoPath)
    {
        recordedVideoPath = videoPath;
    }

    @Override
    public void finishActivity()
    {
        finish();
    }
}
