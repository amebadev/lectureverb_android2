package gilm.lecture.verb.Features.QuickLecture;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * created by PARAMBIR SINGH on 26/9/17.
 */

public class ThumbnailResponseModel extends BasicApiModel
{

    private data data;

    public ThumbnailResponseModel.data getData() {
        return data;
    }

    public void setData(ThumbnailResponseModel.data data) {
        this.data = data;
    }

    public class data
    {
        private String thumbnail = "";

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }
    }
}
