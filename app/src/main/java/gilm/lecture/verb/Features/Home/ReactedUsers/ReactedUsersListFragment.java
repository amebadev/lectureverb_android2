package gilm.lecture.verb.Features.Home.ReactedUsers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.Followers.FollowRequestListAdapter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;

/**
 * created by PARAMBIR SINGH on 10/10/17.
 */

public class ReactedUsersListFragment extends Fragment {
    View rootView;
    TextView txtvNoData;
    RecyclerView recyclerView;
    UserDataModel.UserData mUserData = null;
    String userId = "";
    FollowRequestListAdapter followingFollwerAdapter = null;
    List<UserDataModel.UserData> mUsersList = new ArrayList<>();

    public ReactedUsersListFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ReactedUsersListFragment(List<UserDataModel.UserData> mUsersList) {
        this.mUsersList = mUsersList;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = getActivity().getLayoutInflater().inflate(R.layout.fragment_following_followers, null);

        txtvNoData = (TextView) rootView.findViewById(R.id.txtvNoData);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        txtvNoData.setText("No data");
        if (!mUsersList.isEmpty()) {
            txtvNoData.setVisibility(View.GONE);
        } else {
            txtvNoData.setVisibility(View.VISIBLE);
        }

        if (mUserData != null && !mUserData.getUser_id().equals(new SharedPrefHelper(getActivity()).getUserId())) {
            userId = mUserData.getUser_id();
        } else {
            userId = new SharedPrefHelper(getActivity()).getUserId();
        }

        followingFollwerAdapter = new FollowRequestListAdapter(getActivity(), mUsersList, null, false);
        recyclerView.setAdapter(followingFollwerAdapter);

        return rootView;
    }
}
