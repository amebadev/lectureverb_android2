package gilm.lecture.verb.Features.QuickLecture.Presentation.PresentationOptions;

import java.io.Serializable;

import gilm.lecture.verb.WebServices.Web;

/**
 * Created by harpreet on 9/6/17.
 */

public class SingleImageModel implements Serializable {

    String download_file;

    public String getDownload_file() {
        return download_file;
    }

    public void setDownload_file(String download_file) {
        this.download_file = download_file;
    }
}
