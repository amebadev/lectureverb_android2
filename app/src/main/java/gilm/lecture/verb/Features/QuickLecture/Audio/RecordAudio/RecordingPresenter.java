package gilm.lecture.verb.Features.QuickLecture.Audio.RecordAudio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.Observable;
import android.databinding.ObservableField;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.czt.mp3recorder.MP3Recorder;
import com.shuyu.waveview.AudioPlayer;
import com.shuyu.waveview.FileUtils;

import java.io.File;
import java.io.IOException;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.QuickLecture.Audio.PostAudio.PostAudioActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.FileHelper;
import param.app.audiotrimmer2.RingdroidEditActivity;

/**
 * Created by G-Expo on 18 Jul 2017.
 */

public class RecordingPresenter extends BasePresenter<RecordingView> implements RecordingBinder {
    public MP3Recorder mRecorder;
    private AudioPlayer audioPlayer;

    public boolean isRecordingBoolean;
    public ObservableField<Boolean> isRecording = new ObservableField<>();
    public ObservableField<Boolean> isRecorded = new ObservableField<>();
    public ObservableField<String> recordingMessage = new ObservableField<>();
    private String filePath = "";
    private boolean mIsPlay;
    private boolean mIsRecord;

    public RecordingPresenter() {
        filePath = "";
        mIsRecord = false;
        isRecording.set(true);
        isRecordingBoolean = true;
        isRecorded.set(false);
        recordingMessage.set(recordingMessagetext());
        isRecording.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                recordingMessage.set(recordingMessagetext());
            }
        });
    }

    public void initRecorder() {
        isRecorded.set(false);

        audioPlayer = new AudioPlayer(getView().getActivityG(), new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case AudioPlayer.HANDLER_CUR_TIME:
//                        curPosition = (int) msg.obj;
//                        playText.setText(toTime(curPosition) + " / " + toTime(duration));
                        break;
                    case AudioPlayer.HANDLER_COMPLETE:
//                        playText.setText(" ");
                        mIsPlay = false;
                        break;
                    case AudioPlayer.HANDLER_PREPARED:
//                        duration = (int) msg.obj;
//                        playText.setText(toTime(curPosition) + " / " + toTime(duration));
                        break;
                    case AudioPlayer.HANDLER_ERROR:
//                        resolveResetPlay();
                        mIsPlay = false;
                        break;
                }

            }
        });
    }

//    @Override
//    public void onPause()
//    {
//        super.onPause();
//        if (mIsRecord) {
//            StopRecord();
//        }
//        if (mIsPlay) {
//            audioPlayer.pause();
//            audioPlayer.stop();
//        }
//
//    }

    @Override
    public void detachView() {
        try {
            stopAndResetUI();
            filePath = "";

            if (audioPlayer != null) {
                audioPlayer.stop();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.detachView();
    }

    public void startRecording() {
        startRecord();
    }

    public void pauseRecording() {
        if (mRecorder.isRecording()) {
            mIsRecord = false;
            mRecorder.setPause(true);
            isRecording.set(false);
            isRecordingBoolean = false;
            getView().recordingPaused();
        } else {
            startRecord();
        }

    }

    public String recordingMessagetext() {
        return recordingInProgress() ? "Tap the record button to stop recording"
                : "Tap the record button to start recording";
    }

    public boolean recordingInProgress() {
        return mIsRecord;
    }

    @Override
    public void recordClicked(View view) {
        if (mIsRecord) {
            pauseRecording();
            recordingMessage.set("Tap the record button to resume recording");
            getView().setToolbartitle("Paused");
        } else {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    if (getView().getMinutesRecorded() < 10) {
                        startRecording();
                        recordingMessage.set("Tap the record button to pause recording");
                        getView().setToolbartitle("Recording");
                        getView().startTimer();
                    } else {
                        DialogHelper.getInstance().showInformation(getView().getActivityG(), "You need to be a premium user to record more than 10 minutes.", new CallBackG<String>() {
                            @Override
                            public void onCallBack(String output) {

                            }
                        });
                    }
                }
            });
        }
    }

    @Override
    public ObservableField<Boolean> isRecording() {
        return isRecording;
    }

    @Override
    public ObservableField<Boolean> isRecorded() {
        return isRecorded;
    }

    @Override
    public ObservableField<String> recordingMessage() {
        return recordingMessage;
    }

    @Override
    public void playRecording(View view) {
        resolvePlayRecord();
    }

    @Override
    public void trimRecording(View view) {

        stopAndResetUI();

        if (!filePath.isEmpty()) {
            try {
                RingdroidEditActivity.start(getView().getActivityG(), filePath);
            } catch (Exception | Error e) {
                e.printStackTrace();
            }
        }
    }

    protected void stopAndResetUI() {
        if (mIsRecord) {
            StopRecord();
            getView().recordingPaused();
        }
        if (mRecorder != null) {
            mRecorder.stop();
        }
        isRecording.set(true);
        isRecordingBoolean = true;
        isRecorded.set(false);
        getView().getAudioWave().stopView();
        if (mIsPlay) {
            mIsPlay = false;
            audioPlayer.pause();
        }
    }

    @Override
    public void deleteRecording(View view) {
        DialogHelper.getInstance().showWith2Action(getView().getActivityG(), "Yes", "No", getView().getActivityG().getResources().getString(R.string.app_name), "Do you want to delete this recording?", new CallBackG<String>() {
            @Override
            public void onCallBack(String output) {
                resolveErrorDelete();

                getView().recordingStopped();
                getView().displayError("Recording deleted !");

                getView().setToolbartitle("Record");
            }
        });
    }

    @Override
    public void next(View view) {
        stopAndResetUI();
        if (!filePath.isEmpty()) {
            if (getView().getEventModel() == null) {
                PostAudioActivity.start(getView().getActivityG(), filePath, getView().getLectureTitle(), getView().getTimerView().getText().toString(), getView().getGroupId());
            } else {
                PostAudioActivity.startForEvent(getView().getActivityG(), filePath, getView().getLectureTitle(), getView().getTimerView().getText().toString(), getView().getEventModel(), getView().getGroupId());
            }
        }
    }

    public void onActivityResultAudioTrim(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            String audioPath = data.getStringExtra(Constants.Extras.DATA);
            Log.e("Trim path--------------", audioPath);

            if (getView().getEventModel() == null) {
                PostAudioActivity.start(getView().getActivityG(), audioPath, getView().getLectureTitle(), getView().getTimerView().getText().toString(), getView().getGroupId());
            } else {
                PostAudioActivity.startForEvent(getView().getActivityG(), audioPath, getView().getLectureTitle(), getView().getTimerView().getText().toString(), getView().getEventModel(), getView().getGroupId());
            }

        }
    }

    private void startRecord() {
        if (mIsPlay) {
            mIsPlay = false;
            pausePlayer();
        }

        if (filePath.isEmpty()) {
            filePath = FileHelper.getInstance().createAudioFile(getView().getAudioFileName()).getAbsolutePath();
        }
        if (mRecorder == null) {
            mRecorder = new MP3Recorder(new File(filePath));
            mRecorder.stop();
        } else {
            if (mRecorder.isPause()) {
                mIsRecord = true;
                mRecorder.setPause(false);
                getView().recordingInProgress();
                isRecording.set(true);
                isRecordingBoolean = true;
                isRecorded.set(true);
                getView().getAudioWave().setBaseRecorder(mRecorder);
                getView().getAudioWave().startView();

                return;
            }
        }

        int size = getScreenWidth(getView().getActivityG()) / dip2px(getView().getActivityG(), 1);
        mRecorder.setDataList(getView().getAudioWave().getRecList(), size);

        mRecorder.setErrorHandler(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == MP3Recorder.ERROR_TYPE) {
                    resolveErrorDelete();
                    filePath = "";
                    mRecorder = null;
                    return;
                }
            }
        });

        //audioWave.setBaseRecorder(mRecorder);

        try {
            mRecorder.start();
            mIsRecord = true;

            getView().recordingInProgress();
            isRecording.set(true);
            isRecordingBoolean = true;
            isRecorded.set(true);
            getView().getAudioWave().setBaseRecorder(mRecorder);
            getView().getAudioWave().startView();
        } catch (IOException e) {
            e.printStackTrace();
            resolveErrorDelete();
        }
    }

    private void StopRecord() {
        if (mRecorder != null && mRecorder.isRecording()) {
            mRecorder.setPause(false);
            mRecorder.stop();
            getView().getAudioWave().stopView();
        }
        mIsRecord = false;
    }

    private void resolveErrorDelete() {
        stopAndResetUI();
        mRecorder = null;
        FileUtils.deleteFile(filePath);
        filePath = "";
    }

    private void resolvePlayRecord() {
        if (!mIsRecord) {
            if (mIsPlay) {
                mIsPlay = false;
                pausePlayer();
            } else {
                mIsPlay = true;
                if (audioPlayer != null) {
                    audioPlayer.playUrl(filePath);
                }
            }
        } else {
            getView().displayError("Stop recording first..!");
        }
    }

    private void pausePlayer() {
        if (audioPlayer != null) {
            audioPlayer.pause();
        }
    }

    public static int getScreenWidth(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.widthPixels;
    }

    public static int getScreenHeight(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.heightPixels;
    }

    public static int dip2px(Context context, float dipValue) {
        float fontScale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * fontScale + 0.5f);
    }

}
