/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Messaging.RecentChat;

import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.EndlessRecyclerOnScrollListener;
import gilm.lecture.verb.databinding.FragmentRecyclerlistBinding;

/**
 * Created by G-Expo on 17 Jul 2017.
 */

public class RecentChatFragment extends BaseFragment<FragmentRecyclerlistBinding, RecentChatPresenter> implements RecentChatView
{

    EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    private LinearLayoutManager linearLayoutManager;

    public static RecentChatFragment newInstance() {
        return new RecentChatFragment();
    }

    public RecentChatFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_recyclerlist;
    }

    @Override
    protected void onCreateFragmentG() {
        injectPresenter(new RecentChatPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        endlessRecyclerOnScrollListener.reset();
        getPresenter().loadRecentChats();

        endlessRecyclerOnScrollListener.reset();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventBusCalled(String userId) {
        Log.e("EVENT BUS", "-------------------------------------------------------------------------------------------------------------");
        endlessRecyclerOnScrollListener.reset();
        getPresenter().loadRecentChats();
    }


    @Override
    public void initViews() {
        getDataBinder().setIsLoading(getPresenter().getIsLoadingViewModel());
        getPresenter().loadRecentChats();
        linearLayoutManager = new LinearLayoutManager(getActivityG(), LinearLayoutManager.VERTICAL, false);
        getDataBinder().txtvNoData.setText("No recent chats found");
        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager)
        {
            @Override
            public void onLoadMore(int current_page) {
                getPresenter().loadMoreData(current_page);
            }

            @Override
            public void countChange(int currentVisibleCount) {

            }
        };
    }

    @Override
    public void showData(List<ListOfRecentChatsModel.RecentChatsModel> list) {
        if (!list.isEmpty()) {
            getDataBinder().reviewsList.setVisibility(View.VISIBLE);
            RecentChatAdapter adapter = new RecentChatAdapter(list, getActivityG());

            getDataBinder().reviewsList.setLayoutManager(linearLayoutManager);
            getDataBinder().reviewsList.setAdapter(adapter);
            getDataBinder().txtvNoData.setVisibility(View.GONE);

        }
        else {
            getDataBinder().txtvNoData.setVisibility(View.VISIBLE);
            getDataBinder().reviewsList.setVisibility(View.GONE);
        }
    }

    @Override
    public void moreDateLoaded(List<ListOfRecentChatsModel.RecentChatsModel> list) {


        if (!list.isEmpty()) {
            getDataBinder().reviewsList.setVisibility(View.VISIBLE);
            getDataBinder().reviewsList.getAdapter().notifyDataSetChanged();
            getDataBinder().txtvNoData.setVisibility(View.GONE);
        }
        else {
            getDataBinder().txtvNoData.setVisibility(View.VISIBLE);
            getDataBinder().reviewsList.setVisibility(View.GONE);
        }
    }
}