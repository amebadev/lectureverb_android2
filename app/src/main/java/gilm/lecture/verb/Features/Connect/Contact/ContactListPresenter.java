/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Connect.Contact;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Home.Notification.NotificationViewModel;
import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.UtilsG.IsLoadingViewModel;

/**
 * Created by G-Expo on 28 Jul 2017.
 */

public class ContactListPresenter extends BasePresenter<ContactListView>
{

    public IsLoadingViewModel isLoadingViewModel;

    public IsLoadingViewModel getIsLoadingViewModel()
    {
        return isLoadingViewModel;
    }

    public ContactListPresenter()
    {
        isLoadingViewModel = new IsLoadingViewModel();
        isLoadingViewModel.setLoading(false);
    }

    private List<NotificationViewModel> list = new ArrayList<>();

    public void loadContactUsers()
    {
        for (int i = 0; i < 7; i++) {

            NotificationViewModel notificationData = new NotificationViewModel();
            notificationData.setMessage("User " + i);
            notificationData.setRead(false);
            notificationData.setTime("");
            notificationData.setImageUrl("https://students.ucsd.edu/_images/student-life/involvement/getinvolved/csi-staff-contacts/Casey.jpg");

            list.add(notificationData);
        }

        isLoadingViewModel.setLoading(true);

    }

}
