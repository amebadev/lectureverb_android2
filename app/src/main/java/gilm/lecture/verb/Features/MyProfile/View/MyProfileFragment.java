/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.MyProfile.View;

import android.annotation.SuppressLint;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.Edit.ProfileSettingActivity;
import gilm.lecture.verb.Features.MyProfile.Followers.FollowRequestsFragment;
import gilm.lecture.verb.Features.MyProfile.Followers.FollowersFragment;
import gilm.lecture.verb.Features.MyProfile.View.RepostedPosts.RepostedPostsFragment;
import gilm.lecture.verb.Features.NaviagtionDrawer.ProfileUpdateBus;
import gilm.lecture.verb.Features.QuickLecture.ChoosePostOptionActivity;
import gilm.lecture.verb.Features.TabAdapter.FragmentTabModel;
import gilm.lecture.verb.Features.TabAdapter.SectionsPagerAdapter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.databinding.FragmentMyProfileBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends BaseFragment<FragmentMyProfileBinding, MyProfilePresenter> implements MyProfileView {
    private ImageView fab;
    UserDataModel.UserData mUserData = null;
    Button edt_editProfile;
    LinearLayout llLockPosts;

    @Override
    public boolean isFollowing() {
        return isOtherUserConnected;
    }

    private Boolean isOtherUserConnected = true;
    private Boolean is_following;
    private Boolean is_approved;

    public static MyProfileFragment newInstanceForMyProfile(UserDataModel.UserData mUserData, Boolean isOtherUserConnected) {
        return new MyProfileFragment(mUserData, isOtherUserConnected, isOtherUserConnected, isOtherUserConnected);
    }

    public MyProfileFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public MyProfileFragment(UserDataModel.UserData mUserData, Boolean isOtherUserConnected, Boolean is_following, Boolean Is_approved) {
        // Required empty public constructor
        this.mUserData = mUserData;
        this.isOtherUserConnected = isOtherUserConnected;
        this.is_following = is_following;
        is_approved = Is_approved;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_my_profile;
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
    protected void onCreateFragmentG() {
        injectPresenter(new MyProfilePresenter(mUserData));
        getPresenter().attachView(this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void initViews() {

        edt_editProfile = (Button) view.findViewById(R.id.edt_editProfile);
        llLockPosts = (LinearLayout) view.findViewById(R.id.llLockPosts);

        getDataBinder().setPresenter(getPresenter());
        fab = (ImageView) view.findViewById(R.id.fab);
        if (mUserData != null && !mUserData.getUser_id().equals(new SharedPrefHelper(getActivity()).getUserId())) {
            edt_editProfile.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);

            if (is_following && is_approved) {
                getDataBinder().idViewpager.setVisibility(View.VISIBLE);
                getDataBinder().tab.setVisibility(View.VISIBLE);

                edt_editProfile.setText("Un Follow");
                edt_editProfile.setVisibility(View.VISIBLE); // onclick is implemented in presenter
                edt_editProfile.setAllCaps(false);
            } else {
                if (is_following && !is_approved) {
                    edt_editProfile.setText("Follow Request Sent");
                    edt_editProfile.setVisibility(View.VISIBLE); // onclick is implemented in presenter
                    llLockPosts.setVisibility(View.VISIBLE);
                    edt_editProfile.setAllCaps(false);

                    getDataBinder().idViewpager.setVisibility(View.INVISIBLE);
                    getDataBinder().tab.setVisibility(View.INVISIBLE);
                } else if (!is_following && !is_approved) {
                    edt_editProfile.setText("Follow");
                    edt_editProfile.setVisibility(View.VISIBLE); // onclick is implemented in presenter
                    llLockPosts.setVisibility(View.VISIBLE);
                    edt_editProfile.setAllCaps(false);

                    getDataBinder().idViewpager.setVisibility(View.INVISIBLE);
                    getDataBinder().tab.setVisibility(View.INVISIBLE);
                }
            }
        } else {
            edt_editProfile.setVisibility(View.VISIBLE);
            getDataBinder().idViewpager.setVisibility(View.VISIBLE);
            getDataBinder().tab.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        }

        getDataBinder().setPresenter(getPresenter());
        List<FragmentTabModel> list = new ArrayList<>();
        list.add(new FragmentTabModel(RepostedPostsFragment.newInstance(mUserData), "Post"));
//        list.add(new FragmentTabModel(PostFragment.newInstance(), "Shared"));
//        list.add(new FragmentTabModel(BlankFragment.newInstance(), "Media"));
        list.add(new FragmentTabModel(new FollowersFragment(false, mUserData), "Followers"));
        list.add(new FragmentTabModel(new FollowersFragment(true, mUserData), "Following"));

        // check if its my profile
        if ((mUserData != null && !mUserData.getUser_id().equals(new SharedPrefHelper(getActivity()).getUserId())) ? false : true) {
            list.add(new FragmentTabModel(new FollowRequestsFragment(true, mUserData), "Follow Requests"));
            getDataBinder().tab.setTabMode(TabLayout.GRAVITY_FILL);
        }

        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getChildFragmentManager(), list);
        getDataBinder().idViewpager.setAdapter(adapter);
        getDataBinder().tab.setupWithViewPager(getDataBinder().idViewpager);

        getDataBinder().tab.setTabTextColors(ContextCompat.getColor(getActivityG(), R.color.colorAccent), ContextCompat.getColor(getActivityG(), R.color.colorAccent));



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ChoosePostOptionActivity.start(getActivity());

            }
        });

        UtillsG.setHeightWidthWithRatio_LinearLayout(getActivity(), getDataBinder().frameProfileImage, 3.5f, 6f);
//        UtillsG.setHeightWidthWithRatio_FrameLayout(getActivity(), getDataBinder().imgvCoverPic, 1f, 2f);
    }

    @Override
    public void openProfileSettings() {
        ProfileSettingActivity.start(getActivityG());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void profileUpdated(ProfileUpdateBus data) {
        getPresenter().refreshUserData();
    }


}
