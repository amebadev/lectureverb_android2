package gilm.lecture.verb.Features.Home.RecentActivity;

import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Home.Notification.NotificationViewModel;
import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.UtilsG.DateHelper;
import gilm.lecture.verb.UtilsG.IsLoadingViewModel;

/**
 * Created by G-Expo on 17 Jul 2017.
 */

public class RecentActivitiesPresenter extends BasePresenter<RecentActivitiesView>
{

    public IsLoadingViewModel isLoadingViewModel;

    public IsLoadingViewModel getIsLoadingViewModel()
    {
        return isLoadingViewModel;
    }

    public RecentActivitiesPresenter()
    {
        isLoadingViewModel = new IsLoadingViewModel();
        isLoadingViewModel.setLoading(false);
    }

    private List<NotificationViewModel> list = new ArrayList<>();

    public void loadNotificationData()
    {
        for (int i = 0; i < 7; i++) {

            NotificationViewModel notificationData = new NotificationViewModel();
            notificationData.setMessage("Dummy activity goes here " + i);
            notificationData.setRead(false);
            notificationData.setTime(i + " minute ago");
            notificationData.setImageUrl("https://students.ucsd.edu/_images/student-life/involvement/getinvolved/csi-staff-contacts/Casey.jpg");

            list.add(notificationData);
        }

        isLoadingViewModel.setLoading(true);


        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                isLoadingViewModel.setLoading(false);

                if (getView() != null) {
                    getView().showData(list);
                }
            }
        }, 500);
    }
}
