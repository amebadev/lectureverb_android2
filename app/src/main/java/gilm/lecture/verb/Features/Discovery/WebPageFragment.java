/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Discovery;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import gilm.lecture.verb.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class WebPageFragment extends Fragment
{

    private WebView webView;

    static WebPageFragment newInstance()
    {
        return new WebPageFragment();
    }

    public WebPageFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View    view    = inflater.inflate(R.layout.fragment_web_page, container, false);
        webView = (WebView) view.findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("http://www.google.com");
        return view;
    }


    public String getCurrentPageUrl() {
        return webView.getUrl();
    }
}
