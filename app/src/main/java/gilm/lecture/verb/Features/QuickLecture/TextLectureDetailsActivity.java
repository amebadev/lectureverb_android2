package gilm.lecture.verb.Features.QuickLecture;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import gilm.lecture.verb.Features.Discovery.VideoRecording.WebViewRecorderActivity;
import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments.CommentsModel;
import gilm.lecture.verb.Features.Player.CommentsReplyDailog;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.Web;
import gilm.lecture.verb.Widget.TextViewWithImages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * created by PARAMBIR SINGH on 17/11/17.
 */

public class TextLectureDetailsActivity extends AppCompatActivity {
    ImageView imgvThumbnail, imgvUserImage, imgvOptions, imgvLike, imgv_repost, imgvRepostUserImage;
    TextView actxtvLectureTitle, txtvDuration;
    TextView txtvFavouriteCount, txtvCommentsCount, txtvRepostedCount, txtvTitleBig, txtvViewsCount, txtvTimeAgo;
    TextViewWithImages txtvUserName;
    LinearLayout layPlay, linearMain, layLike, llRepost, ll_comments, ll_connect, ll_repostDetails;
    FrameLayout frameLayout;
    LectureEventClicks mClicks = new LectureEventClicks();


    Lecture_And_Event_Model lecture_and_event_model;

    CommentsReplyDailog mCommentsReply;

    public static void start(Context context, Lecture_And_Event_Model lecture_and_event_model) {
        Intent starter = new Intent(context, TextLectureDetailsActivity.class);
        starter.putExtra(Constants.Extras.DATA, lecture_and_event_model);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_lecture_details);

        lecture_and_event_model = (Lecture_And_Event_Model) getIntent().getSerializableExtra(Constants.Extras.DATA);

        mCommentsReply = new CommentsReplyDailog(this);

        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        llRepost = (LinearLayout) findViewById(R.id.llRepost);
        layLike = (LinearLayout) findViewById(R.id.layLike);
        linearMain = (LinearLayout) findViewById(R.id.linearMain);
        layPlay = (LinearLayout) findViewById(R.id.layPlay);
        imgvLike = (ImageView) findViewById(R.id.imgvLike);
        imgvOptions = (ImageView) findViewById(R.id.imgvOptions);
        imgvUserImage = (ImageView) findViewById(R.id.imgvUserImage);
        imgvThumbnail = (ImageView) findViewById(R.id.imgvThumbnail);
        actxtvLectureTitle = (TextView) findViewById(R.id.actxtvLectureTitle);
        txtvFavouriteCount = (TextView) findViewById(R.id.txtvFavouriteCount);
        txtvCommentsCount = (TextView) findViewById(R.id.txtvCommentsCount);
        txtvRepostedCount = (TextView) findViewById(R.id.txtvRepostedCount);
        txtvDuration = (TextView) findViewById(R.id.txtvDuration);

        ll_repostDetails = (LinearLayout) findViewById(R.id.ll_repostDetails);

        txtvTitleBig = (TextView) findViewById(R.id.txtvTitleBig);
        txtvViewsCount = (TextView) findViewById(R.id.txtvViewsCount);
        imgv_repost = (ImageView) findViewById(R.id.imgv_repost);
        ll_comments = (LinearLayout) findViewById(R.id.ll_comments);

        txtvUserName = (TextViewWithImages) findViewById(R.id.txtvUserName);
        txtvTimeAgo = (TextView) findViewById(R.id.txtvTimeAgo);
        ll_connect = (LinearLayout) findViewById(R.id.ll_connect);
        imgvRepostUserImage = (ImageView) findViewById(R.id.imgvRepostUserImage);

        showData();


    }

    private void showData() {
        if (!UtillsG.getNotNullString(lecture_and_event_model.getThumbnail(), "").isEmpty()) {
            ImageLoader.setImageBig(imgvThumbnail, lecture_and_event_model.getThumbnail());
        } else {
            imgvThumbnail.setImageResource(R.drawable.white_square);
        }
        if (lecture_and_event_model.getOwnerUserdata() != null) {
            if (!UtillsG.getNotNullString(lecture_and_event_model.getOwnerUserdata().getProfile_pic(), "").isEmpty()) {
                ImageLoader.setImageBig(imgvUserImage, lecture_and_event_model.getOwnerUserdata().getProfile_pic());
            } else {
                ImageLoader.setImageBig(imgvUserImage, "https://images.designtrends.com/wp-content/uploads/2015/11/30162730/White-and-Grey-Plain-Background.jpg");
            }
        } else {
            ImageLoader.setImageBig(imgvUserImage, lecture_and_event_model.getUserdata().getProfile_pic());
        }

        txtvFavouriteCount.setText(lecture_and_event_model.getFavourites_count());
        txtvCommentsCount.setText(lecture_and_event_model.getComments_count());
        txtvRepostedCount.setText(lecture_and_event_model.getReposted_count());
        txtvViewsCount.setText(lecture_and_event_model.getLecture_view());

        ll_repostDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityOtherUserDetails.start(TextLectureDetailsActivity.this, lecture_and_event_model.getUserdata(), true);
            }
        });

        if (lecture_and_event_model.getIs_favourite().equalsIgnoreCase("1")) {
            imgvLike.setImageResource(R.drawable.ic_liked);
            imgvLike.setColorFilter(ContextCompat.getColor(TextLectureDetailsActivity.this, R.color.redTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
        } else {
            imgvLike.setImageResource(R.mipmap.ic_fav_white_border);
            imgvLike.setColorFilter(ContextCompat.getColor(TextLectureDetailsActivity.this, R.color.dialogBackgroundColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        }

        if (lecture_and_event_model.getIs_reposted()) {
            imgv_repost.setColorFilter(ContextCompat.getColor(TextLectureDetailsActivity.this, R.color.greenTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
        } else {
            imgv_repost.setColorFilter(ContextCompat.getColor(TextLectureDetailsActivity.this, R.color.dialogBackgroundColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        }

        layLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likeUnlikeLecture();
            }
        });

        imgvOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMoreOptions(v);
            }
        });

        ll_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCommentsReply.showPostDialog(lecture_and_event_model, false, new CallBackG<CommentsModel>() {
                    @Override
                    public void onCallBack(CommentsModel output) {
                        int commentCount = Integer.parseInt(lecture_and_event_model.getComments_count());
                        commentCount++;
                        txtvCommentsCount.setText("" + commentCount);
                        lecture_and_event_model.setComments_count(String.valueOf(commentCount));
                        showData();
                    }
                });
            }
        });


        ll_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityOtherUserDetails.start(TextLectureDetailsActivity.this, lecture_and_event_model.getUserdata(), true);
            }
        });


        llRepost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lecture_and_event_model.getUser_id().equals(new SharedPrefHelper(TextLectureDetailsActivity.this).getUserId())
                        ||
                        (lecture_and_event_model.getOwnerUserdata() != null &&
                                lecture_and_event_model.getOwnerUserdata().getUser_id().equals(new SharedPrefHelper(TextLectureDetailsActivity.this).getUserId()))) {
                    UtillsG.showToast("You are the creator so you cannot repost it.", TextLectureDetailsActivity.this, true);
                } else if (!lecture_and_event_model.getIs_reposted()) {

                    MediaPlayer mp = MediaPlayer.create(TextLectureDetailsActivity.this, R.raw.tone_like);
                    mp.start();

                    mClicks.RepostQuickLecture((Activity) TextLectureDetailsActivity.this, LVApplication.getRetrofit().create(HomeApis.class),
                            lecture_and_event_model.getQuick_lecture_id(), new CallBackG<Boolean>() {
                                @Override
                                public void onCallBack(Boolean output) {
                                    if (output) {

                                              /*  // hit to add the repost lecture under profile feed
                                                mClicks.ShareOnUserProfileFeed((Activity) TextLectureDetailsActivity.this, LVApplication.getRetrofit().create(LectureEventsApi.class),
                                                        lecture_and_event_model.getQuick_lecture_id(), "lecture",
                                                        new CallBackG<Boolean>() {
                                                            @Override
                                                            public void onCallBack(Boolean output) {

                                                            }
                                                        });*/

                                        int repostCount = Integer.parseInt(lecture_and_event_model.getReposted_count());
                                        repostCount++;
                                        txtvRepostedCount.setText("" + repostCount);
                                        lecture_and_event_model.setReposted_count(String.valueOf(repostCount));
                                        lecture_and_event_model.setIs_reposted(true);
                                        showData();
                                        EventBus.getDefault().post(new HomeFragmentUpdateBus(true));

                                    }
                                }
                            });
                } else if (lecture_and_event_model.getIs_reposted()) {

                    MediaPlayer mp = MediaPlayer.create(TextLectureDetailsActivity.this, R.raw.tone_dislike);
                    mp.start();

                    mClicks.unrepost_lecture((Activity) TextLectureDetailsActivity.this,
                            lecture_and_event_model.getQuick_lecture_id() != null ?
                                    lecture_and_event_model.getQuick_lecture_id()
                                    : ""
                            , new CallBackG<Boolean>() {
                                @Override
                                public void onCallBack(Boolean output) {
                                    if (output) {
                                        int repostCount = Integer.parseInt(lecture_and_event_model.getReposted_count());
                                        repostCount--;
                                        txtvRepostedCount.setText("" + repostCount);
                                        lecture_and_event_model.setReposted_count(String.valueOf(repostCount));
                                        lecture_and_event_model.setIs_reposted(false);
                                        EventBus.getDefault().post(new HomeFragmentUpdateBus(true));
                                    }
                                }
                            });
                } else {
                    UtillsG.showToast("Already reposted this lecture", TextLectureDetailsActivity.this, true);
                }
            }
        });

        if (lecture_and_event_model.getQuick_lecture_type().equalsIgnoreCase("text")
                || lecture_and_event_model.getQuick_lecture_type().equalsIgnoreCase("discovery_text")) {
            layPlay.setVisibility(View.GONE);
            actxtvLectureTitle.setMaxLines(2);
            actxtvLectureTitle.setVisibility(View.GONE);
            txtvTitleBig.setVisibility(View.VISIBLE);
            txtvTitleBig.setText(lecture_and_event_model.getQuick_lecture_text());
            actxtvLectureTitle.setText(lecture_and_event_model.getQuick_lecture_text());
//            UtillsG.setHeightWidtWRAP_LinearLayout((TextLectureDetailsActivity.this), frameLayout, TextLectureDetailsActivity.this);
            txtvDuration.setText("");
        } else {
            txtvDuration.setText(UtillsG.getNotNullString(lecture_and_event_model.getQuick_lecture_time_duration(), ""));
            layPlay.setVisibility(View.VISIBLE);
            actxtvLectureTitle.setMaxLines(2);
            actxtvLectureTitle.setVisibility(View.VISIBLE);
            txtvTitleBig.setVisibility(View.GONE);

            txtvTitleBig.setText(lecture_and_event_model.getQuick_lecture_text());
            actxtvLectureTitle.setText(lecture_and_event_model.getQuick_lecture_text());

            UtillsG.setHeightWidthWithRatio_LinearLayout((TextLectureDetailsActivity.this), frameLayout, 7f, 16f);
        }

        UtillsG.setTextSizeByPercentage(TextLectureDetailsActivity.this, actxtvLectureTitle, 4);
        UtillsG.setTextSizeByPercentage(TextLectureDetailsActivity.this, txtvTitleBig, 6);

        String timeAgo = UtillsG.get_time_ago(lecture_and_event_model.getDate_time());

        if (lecture_and_event_model.getIs_reposted() && lecture_and_event_model.getUserdata().getUser_id().equals(new SharedPrefHelper(TextLectureDetailsActivity.this).getUserId())) {
            ImageLoader.setImageSmall(imgvRepostUserImage, lecture_and_event_model.getOwnerUserdata().getProfile_pic());
            txtvTimeAgo.setText(timeAgo);
            txtvUserName.setText(lecture_and_event_model.getOwnerUserdata().getFull_name()
                    + " Posted a New " + UtillsG.QuickLectureType(lecture_and_event_model.getQuick_lecture_type(), "post") + ".");
        } else if ((lecture_and_event_model.getOwnerUserdata() != null
                && lecture_and_event_model.getUserdata().getUser_id().
                equals(lecture_and_event_model.getOwnerUserdata().getUser_id()))
                || lecture_and_event_model.getOwnerUserdata() == null
                ) {
            ImageLoader.setImageSmall(imgvRepostUserImage, lecture_and_event_model.getUserdata().getProfile_pic());
            txtvTimeAgo.setText(timeAgo);
            txtvUserName.setText(lecture_and_event_model.getUserdata().getFull_name()
                    + " Posted a New " + UtillsG.QuickLectureType(lecture_and_event_model.getQuick_lecture_type(), "post") + ".");

        } else {

            txtvUserName.setText(lecture_and_event_model.getUserdata().getFull_name()
                    + " [img src=ic_refresh_green/]  Re-Posted a " + UtillsG.QuickLectureType(lecture_and_event_model.getQuick_lecture_type(), "post"));

            ImageLoader.setImageSmall(imgvRepostUserImage, lecture_and_event_model.getUserdata().getProfile_pic());
            txtvTimeAgo.setText(timeAgo);
        }
    }


    public void showMoreOptions(View view) {
        DialogHelper.getInstance().showLectureMoreOptions(
                TextLectureDetailsActivity.this,
                view,
                lecture_and_event_model.getIs_reposted(),
                lecture_and_event_model.getUser_id().equalsIgnoreCase(new SharedPrefHelper(TextLectureDetailsActivity.this).getUserId()),
                new CallBackG<Integer>() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onCallBack(final Integer attendType) {

                        if (attendType == 1) {
                            layPlay.performClick();
                        } else if (attendType == 2) {
                            layLike.performClick();
                        } else if (attendType == 3) {
                            ll_connect.performClick();
                        } else if (attendType == 4) {

                        } else if (attendType == 5) {
                            llRepost.performClick();
                        } else if (attendType == 6) {
                            if (lecture_and_event_model.getDiscovery_url() != null && !lecture_and_event_model.getDiscovery_url().trim().isEmpty()) {
                                WebViewRecorderActivity.startForBrowsingOnly(TextLectureDetailsActivity.this, lecture_and_event_model.getDiscovery_url());
                            } else {
                                UtillsG.showToast("Url is empty", TextLectureDetailsActivity.this, true);
                            }
                        } else if (attendType == 7) {
                            externalShare();
                        } else if (attendType == 8) {
                            DialogHelper.getInstance().executeDeleteLecture(lecture_and_event_model, TextLectureDetailsActivity.this, new CallBackG<Boolean>() {
                                @Override
                                public void onCallBack(Boolean output) {
                                    if (output) {
                                        finish();
                                    }
                                }
                            });
                        }
                    }
                });
    }

    private void externalShare() {
        Call<BasicApiModel> shareExternally = LVApplication.getRetrofit().create(HomeApis.class).
                save_external_share("0",
                        lecture_and_event_model.getQuick_lecture_id(),
                        new SharedPrefHelper(TextLectureDetailsActivity.this).getUserId(),
                        lecture_and_event_model.getUser_id(),
                        lecture_and_event_model.getUser_id().equals(new SharedPrefHelper(TextLectureDetailsActivity.this).getUserId())
                                ? " You "
                                : new SharedPrefHelper(TextLectureDetailsActivity.this).getUserName(),
                        lecture_and_event_model.getQuick_lecture_text());
        shareExternally.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                Log.e("external shr success", "---------------------------------");
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("external shr failed", t.getMessage() + "---------------------------------");
            }
        });
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, Web.Path.BASE_URL + lecture_and_event_model.getFile_path());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }


    private void likeUnlikeLecture() {

        MediaPlayer mp = MediaPlayer.create(TextLectureDetailsActivity.this,
                lecture_and_event_model.getIs_favourite().equalsIgnoreCase("0")
                        ? R.raw.tone_like : R.raw.tone_dislike);
        mp.start();

        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance().add_remove_to_favourite_list(new SharedPrefHelper(TextLectureDetailsActivity.this).getUserId(), lecture_and_event_model.getQuick_lecture_id());
        basicApiModelCall.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body() != null && response.body().getStatus()) {
                    Log.e("RESPONSE", response.body().getStatus() + "");

                    if (response.body().getStatus()) {
                        int count = Integer.parseInt(lecture_and_event_model.getFavourites_count());
                        UtillsG.showToast(String.valueOf(response.body().getMessage()), TextLectureDetailsActivity.this, true);
                        if (lecture_and_event_model.getIs_favourite().equalsIgnoreCase("0")) {
                            lecture_and_event_model.setIs_favourite("1");
                            count++;
                            lecture_and_event_model.setFavourites_count("" + count);
                            imgvLike.setImageResource(R.drawable.ic_liked);
                            imgvLike.setColorFilter(ContextCompat.getColor(TextLectureDetailsActivity.this, R.color.redTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
                        } else {
                            lecture_and_event_model.setIs_favourite("0");
                            count--;
                            lecture_and_event_model.setFavourites_count("" + count);
                            imgvLike.setImageResource(R.mipmap.ic_fav_white_border);
                            imgvLike.setColorFilter(ContextCompat.getColor(TextLectureDetailsActivity.this, R.color.greyDark), android.graphics.PorterDuff.Mode.MULTIPLY);
                        }
                        lecture_and_event_model.setFavourites_count(String.valueOf(count));

                        showData();

                    }
                }
            }


            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("ERROR get_posted_le", t.getMessage().toString());
                UtillsG.showToast(String.valueOf(t.getMessage().toString()), TextLectureDetailsActivity.this, true);
            }
        });
    }

    protected HomeApis getRetrofitInstance() {
        return LVApplication.getRetrofit().create(HomeApis.class);
    }

}
