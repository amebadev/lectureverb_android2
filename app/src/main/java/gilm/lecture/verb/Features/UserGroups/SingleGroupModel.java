package gilm.lecture.verb.Features.UserGroups;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * created by PARAMBIR SINGH on 21/12/17.
 */

public class SingleGroupModel extends BasicApiModel
{
    GroupModel data = new GroupModel();

    public GroupModel getData() {
        return data;
    }

    public void setData(GroupModel data) {
        this.data = data;
    }
}
