package gilm.lecture.verb.Features.InterestsSelection;

import gilm.lecture.verb.Features.InterestsSelection.AddInterests.ListOfChildInterestsModel;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.Web;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * created by PARAMBIR SINGH on 28/9/17.
 */

public interface InterestSelectionApi
{
    @GET(Web.Path.get_categories)
    Call<ListOfInterestsModel> get_categories();

    @FormUrlEncoded
    @POST(Web.Path.save_users_interest)
    Call<BasicApiModel> save_users_interest(@Field(Web.Keys.category_id) String category_id, @Field(Web.Keys.USER_ID) String user_id);

    @FormUrlEncoded
    @POST(Web.Path.save_users_interest)
    Call<ListOfChildInterestsModel> save_users_interest_v2(@Field(Web.Keys.categories) String categories, @Field(Web.Keys.USER_ID) String user_id);

    @FormUrlEncoded
    @POST(Web.Path.add_new_categories)
    Call<ListOfChildInterestsModel> add_new_categories(@Field(Web.Keys.categories) String categories);

    @FormUrlEncoded
    @POST(Web.Path.remove_user_category)
    Call<BasicApiModel> remove_user_category(@Field(Web.Keys.category_id) String category_id, @Field(Web.Keys.USER_ID) String user_id);
}
