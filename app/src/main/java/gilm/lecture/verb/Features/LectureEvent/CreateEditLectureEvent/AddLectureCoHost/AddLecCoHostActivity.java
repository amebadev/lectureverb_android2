package gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Internal.Base.BaseActivityWithoutPresenter;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.ExisitngCoHost.AvaiableCoHostAdapter;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EventLectureApi;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.EndlessRecyclerOnScrollListener;
import gilm.lecture.verb.UtilsG.GeocodingNotifier;
import gilm.lecture.verb.UtilsG.GetCurrentLocation;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by harpreet on 9/6/17.
 */

public class AddLecCoHostActivity extends BaseActivityWithoutPresenter implements View.OnClickListener, ProgressRequestBody.UploadCallbacks
{
    RecyclerView recycle_liststatic, recycle_list_dynamic;
    int page = 1;
    int page1 = 1;
    Double mLatitude = 0.0, mLongitude = 0.0;
    public ArrayList<CoHostListModel.SingleCoHostData> coHostList = new ArrayList<>();
    AvaiableCoHostAdapter coHostListAdapter;
    public String coHostId = "", lecturerId = "";
    public int requestCode;
    private boolean isSearchingLecturer;
    String lattitude = "", longitude = "";
    boolean lecturer_only = false;

    EditText edSearch;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener, endlessRecyclerOnScrollListener_dynmic;

    public static void start(Context context, String lecturerId) {
        Intent starter = new Intent(context, AddLecCoHostActivity.class);
        starter.putExtra("lecturerId", lecturerId);
        starter.putExtra("requestCode", Constants.RequestCode.CoHostRequestCode);
        ((Activity) context).startActivityForResult(starter, Constants.RequestCode.CoHostRequestCode);
    }

    public static void startForLecturer(Context context, String coHostId) {
        Intent starter = new Intent(context, AddLecCoHostActivity.class);
        starter.putExtra("coHostId", coHostId);
        starter.putExtra("requestCode", Constants.RequestCode.LecturerSelectionRequestCode);
        ((Activity) context).startActivityForResult(starter, Constants.RequestCode.LecturerSelectionRequestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lecture_co_host);

        coHostId = UtillsG.getNotNullString(getIntent().getStringExtra("coHostId"), "");
        lecturerId = UtillsG.getNotNullString(getIntent().getStringExtra("lecturerId"), "");
        requestCode = getIntent().getIntExtra("requestCode", 0);

        isSearchingLecturer = requestCode == Constants.RequestCode.LecturerSelectionRequestCode;

        String screenTitle = "Connect " + (
                requestCode == Constants.RequestCode.LecturerSelectionRequestCode ? "Lecturer"
                        : "Recording Facilitator");

        lecturer_only = requestCode == Constants.RequestCode.LecturerSelectionRequestCode;

        setupToolbar(screenTitle);
        initViews();
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (viewById != null) {
            viewById.setText(title);
        }
        else {
            getSupportActionBar().setTitle(title);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void initViews() {
        edSearch = (EditText) findViewById(R.id.edSearch);
        recycle_liststatic = (RecyclerView) findViewById(R.id.recycle_liststatic);
        recycle_list_dynamic = (RecyclerView) findViewById(R.id.recycle_list_dynamic);
        LinearLayoutManager mLinearManager = new LinearLayoutManager(getActivityG());
        LinearLayoutManager mLinearManager1 = new LinearLayoutManager(getActivityG());
        recycle_liststatic.setLayoutManager(mLinearManager);
        recycle_list_dynamic.setLayoutManager(mLinearManager1);

        endlessRecyclerOnScrollListener_dynmic = new EndlessRecyclerOnScrollListener(mLinearManager1)
        {
            @Override
            public void onLoadMore(int current_page) {

                if (edSearch.getText().toString().trim().length() > 0) {
                    page1++;
                    getSearchedCoHosts(edSearch.getText().toString().trim());
                }

            }

            @Override
            public void countChange(int currentVisibleCount) {

            }
        };

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(mLinearManager)
        {
            @Override
            public void onLoadMore(int current_page) {


                page++;
                getCoHostLists();

            }

            @Override
            public void countChange(int currentVisibleCount) {

            }
        };

        recycle_liststatic.setOnScrollListener(endlessRecyclerOnScrollListener);

        coHostListAdapter = new AvaiableCoHostAdapter((Activity) getActivityG(), coHostList);
        recycle_liststatic.setAdapter(coHostListAdapter);

        if (lattitude.isEmpty()) {
            showLoading("Getting your location");
            GetCurrentLocation location = new GetCurrentLocation((Activity) getActivityG(), new GeocodingNotifier<Location>()
            {
                @Override
                public void GeocodingDetails(Location output) {
                    hideLoading();
                    lattitude = output.getLatitude() + "";
                    longitude = output.getLongitude() + "";

                    getCoHostLists();
                }
            }
            );
            if (location.mGoogleApiClient != null) {
                location.mGoogleApiClient.connect();
            }
        }
        else {
            getCoHostLists();
        }


        edSearch.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                coHostListAdapter.getFilter().filter(s);
                endlessRecyclerOnScrollListener.reset();
                if (edSearch.getText().toString().trim().length() < 1) {
                    page = 1;
                    page1 = 1;
                    getCoHostLists();
                    recycle_liststatic.setVisibility(View.VISIBLE);
                    recycle_list_dynamic.setVisibility(View.GONE);

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ((ImageView) findViewById(R.id.imgvGo)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                getSearchedCoHosts(edSearch.getText().toString().trim());
            }
        });


    }

    @Override
    public Context getActivityG() {
        return AddLecCoHostActivity.this;
    }

    @Override
    public void onClick(View view) {

    }


    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    public void getSearchedCoHosts(String searchedKeyword) {
        recycle_liststatic.setVisibility(View.GONE);
        recycle_list_dynamic.setVisibility(View.VISIBLE);

        Call<CoHostListModel> basicApiModelCall = getRetrofitInstance().search_user(
                getOtherParams("0")
                , getOtherParams(page1 + "")
                , getOtherParams(searchedKeyword));
        basicApiModelCall.enqueue(new Callback<CoHostListModel>()
        {
            @Override
            public void onResponse(Call<CoHostListModel> call, Response<CoHostListModel> response) {
                hideLoading();
                if (response.body() != null) {
                    if (response.body().getStatus()) {
                        if (page1 == 1) {
                            coHostList.clear();
                        }
                        coHostList.addAll(response.body().getmQuickLectureData());

                        coHostListAdapter = new AvaiableCoHostAdapter((Activity) getActivityG(), coHostList);

                        recycle_list_dynamic.setAdapter(coHostListAdapter);


                    }

                    else {
                        coHostListAdapter.setShouldLoadMore(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<CoHostListModel> call, Throwable t) {
                hideLoading();
                Log.e("Error occured", t.getMessage().toString());
            }
        });
    }

    public void getCoHostLists() {

        if (page == 1) {
            showLoading("Getting nearby cohosts");
        }

        Call<CoHostListModel> basicApiModelCall = getRetrofitInstance().get_nearby_co_hosts(
                getOtherParams(new SharedPrefHelper(getActivityG()).getUserId())
                , getOtherParams(lattitude)
                , getOtherParams(longitude)
                , getOtherParams("" + page)
                , getOtherParams("500")
                , getOtherParams(String.valueOf(lecturer_only))
        );
        basicApiModelCall.enqueue(new Callback<CoHostListModel>()
        {
            @Override
            public void onResponse(Call<CoHostListModel> call, Response<CoHostListModel> response) {
                hideLoading();
                if (response.body() != null) {
                    if (response.body().getStatus()) {
                        if (page == 1) {
                            coHostList.clear();
                            coHostList.addAll(response.body().getmQuickLectureData());

                            coHostListAdapter = new AvaiableCoHostAdapter((Activity) getActivityG(), coHostList);

                            recycle_liststatic.setAdapter(coHostListAdapter);


                        }
                        else {
                            coHostList.addAll(response.body().getmQuickLectureData());
                            coHostListAdapter.notifyDataSetChanged();
                        }
                    }
                    else {
                        coHostListAdapter.setShouldLoadMore(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<CoHostListModel> call, Throwable t) {
                hideLoading();
                Log.e("Error occured", t.getMessage().toString());
            }
        });


    }

    public ArrayList<CoHostListModel.SingleCoHostData> getcoHostList() {
        return coHostList;
    }

    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    protected EventLectureApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(EventLectureApi.class);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            /**
             * this intent is made in AvailableCoHostAdapter for selecting co host
             */
            if (requestCode == Constants.RequestCode.CoHostRequestCode
                    || requestCode == Constants.RequestCode.LecturerSelectionRequestCode) {
                String user_id = data.getStringExtra(Constants.Extras.user_id);
                String name = data.getStringExtra(Constants.Extras.name);

                Intent inn = new Intent();
                inn.putExtra(Constants.Extras.user_id, user_id);
                inn.putExtra(Constants.Extras.name, name);
                setResult(RESULT_OK, inn);
                finish();
            }
        }
    }
}
