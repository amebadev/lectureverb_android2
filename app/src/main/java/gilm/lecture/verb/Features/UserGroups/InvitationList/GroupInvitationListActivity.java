package gilm.lecture.verb.Features.UserGroups.InvitationList;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.Internal.Base.EmptyPresenter;
import gilm.lecture.verb.Features.Internal.Base.EmptyView;
import gilm.lecture.verb.Features.UserGroups.UserGroupApis;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.ActivityGroupInvitationListBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupInvitationListActivity extends BaseActivity<ActivityGroupInvitationListBinding, EmptyPresenter> implements EmptyView
{
    LinearLayoutManager linearLayoutManager;
    Group_InvitationAdapter group_invitationAdapter;

    public static void start(Context context) {
        Intent starter = new Intent(context, GroupInvitationListActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_group_invitation_list;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new EmptyPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        setupToolbar("Group Invitations");

        linearLayoutManager = new LinearLayoutManager(GroupInvitationListActivity.this, LinearLayoutManager.VERTICAL, false);
        getDataBinder().recyclerView.setLayoutManager(linearLayoutManager);
        showInvitationList();
    }

    @Override
    public Context getActivityG() {
        return GroupInvitationListActivity.this;
    }

    public void showInvitationList() {
        showLoading("Fetching Invitation List..");
        Call<InvitationsListModel> call = LVApplication.getRetrofit().create(UserGroupApis.class).get_pending_invitations(getLocalData().getUserId());

        call.enqueue(new Callback<InvitationsListModel>()
        {
            @Override
            public void onResponse(Call<InvitationsListModel> call, Response<InvitationsListModel> response) {
                hideLoading();
                if (response.body() != null && response.body().getStatus()) {
                    // UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                    group_invitationAdapter = new Group_InvitationAdapter(response.body().getData(), getActivityG());
                    getDataBinder().recyclerView.setAdapter(group_invitationAdapter);

                }
                else {
                    hideLoading();
                    DialogHelper.getInstance().showInformation(getActivityG(), response.body().getMessage(), new CallBackG<String>()
                    {
                        @Override
                        public void onCallBack(String output) {
                            finish();
                        }
                    });
                }


            }

            @Override
            public void onFailure(Call<InvitationsListModel> call, Throwable t) {
                hideLoading();
                UtillsG.showToast(t.getMessage(), GroupInvitationListActivity.this, true);

            }


        });

    }
}



