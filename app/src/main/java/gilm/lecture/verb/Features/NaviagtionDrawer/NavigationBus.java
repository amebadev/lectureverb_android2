package gilm.lecture.verb.Features.NaviagtionDrawer;

import android.support.v4.app.Fragment;

/**
 * Created by G-Expo on 06 Jul 2017.
 */

/**
 * used by EventBus on {@link NavigationActivity} to update screen/Fragments.
 */
public class NavigationBus
{
    @Title
    private String Title;

    private Fragment fragment;

    public Fragment getFragment()
    {
        return fragment;
    }

    public NavigationBus(String title, Fragment fragment)
    {
        Title = title;
        this.fragment = fragment;
    }

    public String getTitle()
    {
        return Title;
    }
}
