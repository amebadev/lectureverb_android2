package gilm.lecture.verb.Features.Home.Notification;

import java.io.Serializable;

import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.CoHostListModel;
import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;


/**
 * Created by harpreet on 10/3/17.
 */

public class NotificationSingleModel implements Serializable
{

    private String id;
    private String sender_id;
    private String receiver_id;
    private String notify_id;
    private String message;
    private String type;
    private String status;
    private String is_viewed;  // Y  (YES) and N (NO)
    private String date_time;
    private String notify_id_table; //lv_events or lv_quick_lecture
    UserDataModel.UserData Userdata;

    CoHostListModel.SingleCoHostData users;

    Lecture_And_Event_Model quick_lecture;

    public CoHostListModel.SingleCoHostData getUsers() {

        return users;
    }



    public void setUsers(CoHostListModel.SingleCoHostData users) {
        this.users = users;
    }

    public Lecture_And_Event_Model getQuick_lecture() {
        return quick_lecture;
    }

    public void setQuick_lecture(Lecture_And_Event_Model quick_lecture) {
        this.quick_lecture = quick_lecture;
    }

    public String getNotify_id_table() {
        return notify_id_table;
    }

    public void setNotify_id_table(String notify_id_table) {
        this.notify_id_table = notify_id_table;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id) {
        this.receiver_id = receiver_id;
    }

    public String getNotify_id() {
        return notify_id;
    }

    public void setNotify_id(String notify_id) {
        this.notify_id = notify_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIs_viewed() {
        return is_viewed;
    }

    public void setIs_viewed(String is_viewed) {
        this.is_viewed = is_viewed;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public UserDataModel.UserData getUserData() {
        return Userdata;
    }

    public void setUserData(UserDataModel.UserData Userdata) {
        this.Userdata = Userdata;
    }
}
