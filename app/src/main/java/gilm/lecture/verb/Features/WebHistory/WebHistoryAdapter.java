package gilm.lecture.verb.Features.WebHistory;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.InfiniteAdapter_WithoutBuinding;
import gilm.lecture.verb.UtilsG.LoadingViewHolder;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.UtilsG.WebHistoryModel;
import gilm.lecture.verb.WebServices.Web;

/**
 * created by PARAMBIR SINGH on 31/8/17.
 */

class WebHistoryAdapter extends InfiniteAdapter_WithoutBuinding<WebHistoryAdapter.MyViewHolderG>
{

    private LayoutInflater inflater;
    ArrayList<WebHistoryModel> data;
    Context context;

    public WebHistoryAdapter(ArrayList<WebHistoryModel> data, Context context) {
        this.data = data;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        return new LoadingViewHolder(inflater.inflate(R.layout.inflater_web_history, parent, false));
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public MyViewHolderG onCreateView(ViewGroup parent, int viewType) {
        return new MyViewHolderG(inflater.inflate(R.layout.inflater_web_history, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        try {
            if (!(holder instanceof LoadingViewHolder)) {
                final String iconUrl = Web.Path.BASE_URL + data.get(position).getIcon();
                Picasso.with(context).load(iconUrl).resize(100, 100).into(((MyViewHolderG) holder).imgvIcon);
                ((MyViewHolderG) holder).imgvIcon.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        UtillsG.showFullImage(iconUrl, context, false);
                    }
                });

                if (data.get(position).getIcon().isEmpty()) {
                    ((MyViewHolderG) holder).progressBar.setVisibility(View.GONE);
                }
                else {
                    ((MyViewHolderG) holder).progressBar.setVisibility(View.VISIBLE);
                }

                ((MyViewHolderG) holder).txtvTitle.setText("Title: " + data.get(position).getTitle());
                ((MyViewHolderG) holder).txtvUrl.setText(data.get(position).getUrl());

                ((MyViewHolderG) holder).layMain.setOnClickListener(new View.OnClickListener()
                {
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onClick(View v) {
                        DialogHelper.showInnerStackDialog(context, data.get(position).getInnerstack());

//                        WebViewRecorderActivity.startForBrowsingOnly(context, data.get(position).getUrl());
                    }
                });
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    class MyViewHolderG extends RecyclerView.ViewHolder
    {
        View view;
        TextView txtvTitle, txtvUrl;
        LinearLayout layMain;
        ImageView imgvIcon;
        ProgressBar progressBar;

        public MyViewHolderG(View row) {
            super(row);

            view = row;
            layMain = (LinearLayout) view.findViewById(R.id.layMain);
            txtvTitle = (TextView) view.findViewById(R.id.txtvTitle);
            txtvUrl = (TextView) view.findViewById(R.id.txtvUrl);
            imgvIcon = (ImageView) view.findViewById(R.id.imgvIcon);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        }
    }

}
