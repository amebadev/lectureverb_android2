package gilm.lecture.verb.Features.Settings.DataAndStorage;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.databinding.ActivityDataStorageSettingsBinding;

public class DataStorageActivity extends BaseActivity<ActivityDataStorageSettingsBinding, DataStoragePresenter> implements DataStorageView
{

    public static void start(Context context) {
        Intent starter = new Intent(context, DataStorageActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_data_storage_settings;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new DataStoragePresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        setupToolbar("Data and Storage usage");
        getDataBinder().txtvStorage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                DialogHelper.getInstance().showInformation(getActivityG(), "You can change your default storage location from device's Storage Settings. It would be possible only if your device allow to do so. Click Ok to open Settings.", new CallBackG<String>()
                {
                    @Override
                    public void onCallBack(String output) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_MEMORY_CARD_SETTINGS), 0);
                    }
                });
            }
        });
    }

    @Override
    public Context getActivityG() {
        return DataStorageActivity.this;
    }
}
