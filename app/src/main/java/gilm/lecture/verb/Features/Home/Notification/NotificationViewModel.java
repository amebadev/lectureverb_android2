package gilm.lecture.verb.Features.Home.Notification;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.view.View;

import gilm.lecture.verb.Features.Home.Notification.Details.NotificationDetailsActivity;
import gilm.lecture.verb.Features.LectureEvent.Details.LectureEventDetailsActivity;

/**
 * Created by G-Expo on 06 Jul 2017.
 */

public class NotificationViewModel extends BaseObservable
{
    private String  imageUrl;
    private String  time;
    private String  message;
    private boolean isRead;
    private boolean isFavorite=false;
    private boolean isReposted=false;
    private boolean isCommented=false;
    private String flag="";

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public boolean isReposted() {
        return isReposted;
    }

    public void setReposted(boolean reposted) {
        isReposted = reposted;
    }

    public boolean isCommented() {
        return isCommented;
    }

    public void setCommented(boolean commented) {
        isCommented = commented;
    }

    private NotificationSingleModel mNotificationDetails;

    public NotificationSingleModel getmNotificationDetails() {
        return mNotificationDetails;
    }

    public void setmNotificationDetails(NotificationSingleModel mNotificationDetails) {
        this.mNotificationDetails = mNotificationDetails;
    }

    @Bindable
    public String getImageUrl()
    {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
        notifyChange();
    }

    @Bindable
    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
        notifyChange();
    }

    @Bindable
    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
        notifyChange();
    }

    @Bindable
    public boolean isRead()
    {
        return isRead;
    }

    public void setRead(boolean read)
    {
        isRead = read;
        notifyChange();
//        notifyPropertyChanged(BR.read);
    }

    public void notificationClicked(View view, NotificationViewModel notificationViewModel)
    {
        if(notificationViewModel.getmNotificationDetails().getNotify_id_table().equals("lv_quick_lecture")) {
            NotificationDetailsActivity.start(view.getContext(), notificationViewModel.getmNotificationDetails());
        }
        else if(notificationViewModel.getmNotificationDetails().getNotify_id_table().equals("lv_events")) {
            LectureEventDetailsActivity.startActivityWithApiCall(view.getContext(),notificationViewModel.getmNotificationDetails().getNotify_id(),view);
        }
    }
}
