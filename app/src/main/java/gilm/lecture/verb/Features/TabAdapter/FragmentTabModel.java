package gilm.lecture.verb.Features.TabAdapter;

import android.support.v4.app.Fragment;

/**
 * Created by G-Expo on 06 Jul 2017.
 */

public class FragmentTabModel
{
    private Fragment fragment;
    private String Name;

    public FragmentTabModel(Fragment fragment, String name)
    {
        this.fragment = fragment;
        Name = name;
    }

    public Fragment getFragment()
    {
        return fragment;
    }

    public String getName()
    {
        return Name;
    }
}
