package gilm.lecture.verb.Features.LectureEvent.Details;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 11 Jul 2017.
 */

public interface LectureEventDetailsView extends Viewable<LectureEventDetailsPresenter>
{

    void closeActivity();
}
