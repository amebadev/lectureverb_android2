package gilm.lecture.verb.Features.LectureEvent;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.CreateLectureEventActivity;
import gilm.lecture.verb.Features.LectureEvent.Hosting.HostingFragment;
import gilm.lecture.verb.Features.LectureEvent.Invitation.InvitationFragment;
import gilm.lecture.verb.Features.LectureEvent.Upcoming.UpcomingFragment;
import gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence.UserType;
import gilm.lecture.verb.Features.TabAdapter.FragmentTabModel;
import gilm.lecture.verb.Features.TabAdapter.SectionsPagerAdapter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;

/**
 * Created by G-Expo on 07 Jul 2017.
 */

public class LectureEventTabFragment extends Fragment {

    private View view;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    public static LectureEventTabFragment newInstance() {
        return new LectureEventTabFragment();
    }

    public LectureEventTabFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_lecture_event, container, false);

        viewPager = (ViewPager) view.findViewById(R.id.container);

        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        //Creating our pager adapter

        List<FragmentTabModel> list = new ArrayList<>();
        list.add(new FragmentTabModel(UpcomingFragment.newInstance(), "UPCOMING"));
        list.add(new FragmentTabModel(InvitationFragment.newInstance(), "INVITATION"));
     //   list.add(new FragmentTabModel(BlankFragment.newInstance(), "DISCUSSING"));
        list.add(new FragmentTabModel(HostingFragment.newInstance(), "HOSTING"));

        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getChildFragmentManager(), list);
        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(4);
        setupTabIcons();


        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setVisibility((new SharedPrefHelper(getActivity()).getUserType().equalsIgnoreCase(UserType.LECTURER)) ? View.VISIBLE : View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (new SharedPrefHelper(getActivity()).getUserType().equalsIgnoreCase(UserType.LECTURER)) {
                    CreateLectureEventActivity.start(getActivity());
                } else {
                    DialogHelper.getInstance().showInformation(getActivity(), "Only a lecturer can schedule a lecture event.", null);
                }
            }
        });

        fab.setImageResource(R.drawable.ic_new);
        return view;
    }

    private void setupTabIcons() {
//        tabLayout.getTabAt(0).setIcon(R.mipmap.ic_home);
    }

}
