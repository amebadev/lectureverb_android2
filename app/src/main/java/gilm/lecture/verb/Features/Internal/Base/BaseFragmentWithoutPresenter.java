package gilm.lecture.verb.Features.Internal.Base;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gilm.lecture.verb.Features.Internal.Base.Contract.Presentable;
import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;
import gilm.lecture.verb.Features.Internal.Base.Contract.ViewableNoPresenter;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;

public abstract class BaseFragmentWithoutPresenter extends Fragment implements ViewableNoPresenter
{

    protected View view;
    protected SharedPrefHelper sharedPrefHelper;



    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getView() == null) {
            onCreateFragmentG();
        }

        setRetainInstance(true);
    }

    @Override
    public SharedPrefHelper getLocalData() {
        if (sharedPrefHelper == null) {
            sharedPrefHelper = new SharedPrefHelper(getActivityG());
        }
        return sharedPrefHelper;
    }

    @Override
    public Context getActivityG() {
        return getActivity();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void displayError(String message) {
        if (getParentView() != null) {
            Snackbar.make(getParentView(), message, Snackbar.LENGTH_LONG).show();
        }
    }

    public View getParentView() {
        return view;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void showLoading(String progressMessage) {
        // no-op by default
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void hideLoading() {
        // no-op by default
    }




    @LayoutRes
    protected abstract int getLayoutId();

    protected abstract void onCreateFragmentG();

}
