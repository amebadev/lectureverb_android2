package gilm.lecture.verb.Features.Discovery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.theartofdev.edmodo.cropper.CropImage;

import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.Navigator;

public class DiscoveryActivity extends AppCompatActivity
{
    private WebPageFragment webPageFragment;

    public static void start(Context context) {
        Intent starter = new Intent(context, DiscoveryActivity.class);
        ((Activity) context).startActivityForResult(starter, Constants.RequestCode.DISCOVERY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_empty_frame);

        setupToolbar("Discovery");
        webPageFragment = new WebPageFragment();
        new Navigator().replaceFragment(this, R.id.container, webPageFragment);
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (viewById != null) {
            viewById.setText(title);
        }
        else {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.menu_save, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        else {
            Intent inn = new Intent();
            inn.putExtra(Constants.Extras.DATA, webPageFragment.getCurrentPageUrl());
            setResult(RESULT_OK, inn);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                webPageFragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
