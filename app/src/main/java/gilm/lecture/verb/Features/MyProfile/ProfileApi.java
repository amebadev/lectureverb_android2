/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.MyProfile;

import gilm.lecture.verb.Features.Home.Home.ArrayListTagsModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureEventModel;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.Followers.ListOfUserModel;
import gilm.lecture.verb.Features.OtherProfile.FollowStatusModel;
import gilm.lecture.verb.Features.PaypalPayment.PriceModelRetrofit;
import gilm.lecture.verb.Features.Settings.ArrayListLectureModel;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.Web;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by G-Expo on 17 Aug 2017.
 */

public interface ProfileApi
{
    /**
     * @param imageUrl
     * @param pageNumber will start from "1"
     * @return
     */
    @FormUrlEncoded
    @POST(Web.Path.get_my_posted_lectures)
    Call<ArrayListLectureModel> get_my_posted_lectures(@Field(Web.Keys.USER_ID) String imageUrl, @Field(Web.Keys.page) String pageNumber, @Field("other_user_id") String other_user_id);

    @Multipart
    @POST(Web.Path.UPDATE_PROFILE_IMAGE)
    Call<UserDataModel> uploadProfileImage(@Part MultipartBody.Part filePart, @Part(Web.Keys.USER_ID) RequestBody userId, @Part("upload_type") RequestBody upload);


    @Multipart
    @POST(Web.Path.UPDATE_PROFILE_COVER_IMAGE)
    Call<UserDataModel> uploadCoverProfileImage(@Part MultipartBody.Part filePart, @Part(Web.Keys.USER_ID) RequestBody userId, @Part("upload_type") RequestBody upload);


    @FormUrlEncoded
    @POST(Web.Path.UPDATE_PROFILE_IMAGE)
    Call<UserDataModel> updateProfileImage(@Field("profile_image") String imageUrl, @Field(Web.Keys.USER_ID) String userId);
//    lectureverb=update_profile_image , user_id,profile_image,upload_type=upload

    //    URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
//    API NAme => update_profile
//    Method = > GET / POST
//    Description => Update user Profile
//    Parameaters = > lectureverb=update_profile , user_id,full_name,bio=,location=,lat=,lang=,website
//    Code ==> 200 for success 400 for error
    @FormUrlEncoded
    @POST(Web.Path.UPDATE_PROFILE)
    Observable<UserDataModel> updateProfile(@Field(Web.Keys.USER_ID) String userId,
                                            @Field(Web.Keys.FULL_NAME) String name,
                                            @Field(Web.Keys.BIO) String bio,
                                            @Field(Web.Keys.LOCATION) String location,
                                            @Field("lat") String lat,
                                            @Field("lang") String lang,
                                            @Field(Web.Keys.WEBSITE) String website);

    @FormUrlEncoded
    @POST(Web.Path.get_share_list)
    Call<LectureEventModel> get_share_list_lecture_event(@Field("user_id") String user_id, @Field("share_type") String share_type, @Field("page") String page);


    @FormUrlEncoded
    @POST(Web.Path.get_share_list)
    Call<ArrayListTagsModel> get_share_list_quick_lecture(@Field("user_id") String user_id, @Field("share_type") String share_type, @Field("page") String page);


    /*getNearByUsers
lat
lng
radius
user_id*/
    @GET(Web.Path.MID_URL + Web.Path.getNearByUsers)
    Call<ListOfUserModel> getNearByUsers(@Query(Web.Keys.USER_ID) String user_id
            , @Query(Web.Keys.lat) String lat
            , @Query(Web.Keys.lng) String lng
            , @Query(Web.Keys.radius) String radius);

    @GET(Web.Path.MID_URL + Web.Path.get_my_followings)
    Call<ListOfUserModel> get_my_followings(@Query(Web.Keys.USER_ID) String user_id);

    @GET(Web.Path.MID_URL + Web.Path.get_my_followers)
    Call<ListOfUserModel> get_my_followers(@Query(Web.Keys.USER_ID) String user_id);

    @GET(Web.Path.MID_URL + Web.Path.get_follow_requests_list)
    Call<ListOfUserModel> get_follow_requests_list(@Query(Web.Keys.USER_ID) String user_id);

    @GET(Web.Path.MID_URL + Web.Path.people_you_may_know)
    Call<ListOfUserModel> people_you_may_know(@Query(Web.Keys.USER_ID) String user_id);

    @GET(Web.Path.MID_URL + Web.Path.follow_user)
    Call<BasicApiModel> follow_user(@Query(Web.Keys.follower_user_id) String my_user_id, @Query(Web.Keys.followed_user_id) String target_user_id);

    @GET(Web.Path.MID_URL + Web.Path.approve_follow_request)
    Call<BasicApiModel> approve_reject_follow_request(@Query(Web.Keys.my_user_id) String my_user_id
            , @Query(Web.Keys.target_user_id) String target_user_id, @Query(Web.Keys.is_approved) String is_approved);

    @FormUrlEncoded
    @POST(Web.Path.unrepost_lecture)
    Call<BasicApiModel> unrepost_lecture(@Field("quick_lecture_id") String quick_lecture_id, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Web.Path.get_follow_status)
    Call<FollowStatusModel> get_follow_status(@Field("my_user_id") String my_user_id, @Field("target_user_id") String target_user_id);

    @FormUrlEncoded
    @POST(Web.Path.delete_notification)
    Call<BasicApiModel> delete_notification(@Field("notification_id") String notification_id);

      /*  URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => search_user
    Method = > GET / POST
    Description => Get user By name or email
    Parameaters = > user_id , page, keyword
    Code ==> 200 for success 400 for error*/

    @FormUrlEncoded
    @POST(Web.Path.search_user)
    Call<ListOfUserModel> search_user(
            @Field("user_id") String user_id
            , @Field("page") String page
            , @Field("keyword") String keyword
    );

    @GET(Web.Path.get_prices)
    Call<PriceModelRetrofit> get_subscription_prices();

    @FormUrlEncoded
    @POST(Web.Path.save_subscription)
    Call<BasicApiModel> save_subscription(
            @Field("user_id") String user_id
            , @Field("ending_date") String ending_date
    );


}
