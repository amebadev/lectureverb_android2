package gilm.lecture.verb.Features.LectureEvent.Hosting;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;
import gilm.lecture.verb.Features.LectureEvent.Invitation.InvitationPresenter;
import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;

/**
 * Created by G-Expo on 07 Jul 2017.
 */

public interface HostView extends Viewable<HostingPresenter>
{
    void showData(ArrayList<LectureEventViewModel> list,int pageNo);
}
