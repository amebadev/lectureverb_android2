package gilm.lecture.verb.Features.InterestsSelection.AddInterests;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * created by PARAMBIR SINGH on 28/9/17.
 */

public class ListOfChildInterestsModel extends BasicApiModel
{
    @SerializedName("data")
    List<ChildCategoryModel> categories;

    public List<ChildCategoryModel> getCategories() {
        return categories;
    }

    public void setCategories(List<ChildCategoryModel> categories) {
        this.categories = categories;
    }
}
