/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Connect.Contact;

import android.content.Context;

import java.util.List;

import gilm.lecture.verb.Features.Home.Notification.NotificationViewModel;
import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.R;
import gilm.lecture.verb.databinding.InflateContactUserBinding;

/**
 * Created by G-Expo on 28 Jul 2017.
 */

public class ContactListAdapter extends InfiniteAdapterG<InflateContactUserBinding>
{

    private List<NotificationViewModel> dataList;

    public ContactListAdapter(List<NotificationViewModel> dList)
    {
        this.dataList = dList;
    }

    @Override
    protected void bindData(int position, MyViewHolderG myViewHolderG)
    {
        myViewHolderG.binding.setData(dataList.get(position));
        myViewHolderG.binding.executePendingBindings();
    }

    @Override
    public int getCount()
    {
        return dataList.size();
    }

    @Override
    public int getViewType(int position)
    {
        return 1;
    }

    @Override
    public int getInflateLayout()
    {
        return R.layout.inflate_contact_user;
    }

}