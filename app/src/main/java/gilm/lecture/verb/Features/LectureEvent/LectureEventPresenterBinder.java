package gilm.lecture.verb.Features.LectureEvent;

import android.view.View;

/**
 * Created by G-Expo on 07 Jul 2017.
 */

public interface LectureEventPresenterBinder
{
    /**
     * mark an event as attendant.
     *
     * @param viewModel --data on clicked view.
     */
    void markAttendant(View view, LectureEventViewModel viewModel);

    /**
     * mark an event as interested.
     *
     * @param viewModel --data on clicked view.
     */
    void markInterested(View view, LectureEventViewModel viewModel);

    /**
     * share this event
     *
     * @param viewModel --data on clicked view.
     */
    void share(LectureEventViewModel viewModel);

    /**
     * show details of the event on new screen.
     * {@link gilm.lecture.verb.Features.LectureEvent.Details.LectureEventDetailsActivity}
     *
     * @param viewModel --data on clicked view.
     */
    void showDetails(View view, LectureEventViewModel viewModel);

}
