/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Search;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Discovery.VideoRecording.WebViewRecorderActivity;
import gilm.lecture.verb.Features.Favorites.FavouritesLecturesList.FavouriteLecturesListActivity;
import gilm.lecture.verb.Features.Home.Home.CategoriesInterestsAdapter;
import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.Home.Home.HomeFragment_NoBinding;
import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments.TextLectureCommentsActivity;
import gilm.lecture.verb.Features.Player.PlayerActivity;
import gilm.lecture.verb.Features.Player.PublicAudioPlayer;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.Web;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SuggestionPagerAdapter extends PagerAdapter
{

    Context context;
    LayoutInflater layoutInflater;
    ArrayList<Lecture_And_Event_Model> alData;

    LectureEventClicks mClicks = new LectureEventClicks();
    Fragment mRootFragment;

    public SuggestionPagerAdapter(Fragment mRootFragment, Context context, ArrayList<Lecture_And_Event_Model> alData) {
        this.context = context;
        this.alData = alData;
        this.mRootFragment = mRootFragment;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return alData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.inflator_home_two_no_binding, container, false);

        RecyclerView recyLectureInterests = (RecyclerView) itemView.findViewById(R.id.recyLectureInterests);
        ImageView imgvRepostUserImage = itemView.findViewById(R.id.imgvRepostUserImage);
        final ImageView imgvOptions = itemView.findViewById(R.id.imgvOptions);
        ImageView imgv_repost = itemView.findViewById(R.id.imgv_repost);
        ImageView imgvThumbnail = itemView.findViewById(R.id.imgvThumbnail);
        final ImageView imgvLike = itemView.findViewById(R.id.imgvLike);
        ImageView imgvUserImage = itemView.findViewById(R.id.imgvUserImage);
        TextView txtvFavouriteCount = itemView.findViewById(R.id.txtvFavouriteCount);
        TextView txtvCommentsCount = itemView.findViewById(R.id.txtvCommentsCount);
        final TextView txtvRepostedCount = itemView.findViewById(R.id.txtvRepostedCount);
        TextView txtvViewsCount = itemView.findViewById(R.id.txtvViewsCount);
        TextView actxtvLectureTitle = itemView.findViewById(R.id.actxtvLectureTitle);
        TextView txtvDuration = itemView.findViewById(R.id.txtvDuration);
        TextView txtvTitleBig = itemView.findViewById(R.id.txtvTitleBig);
        TextView txtvUserName = itemView.findViewById(R.id.txtvUserName);
        TextView txtvTimeAgo = itemView.findViewById(R.id.txtvTimeAgo);
        final LinearLayout layPlay = itemView.findViewById(R.id.layPlay);
        final LinearLayout linearMain = itemView.findViewById(R.id.linearMain);
        final LinearLayout ll_repostDetails = itemView.findViewById(R.id.ll_repostDetails);
        final LinearLayout layLike = itemView.findViewById(R.id.layLike);
        final LinearLayout ll_comments = itemView.findViewById(R.id.ll_comments);
        final LinearLayout ll_connect = itemView.findViewById(R.id.ll_connect);
        final LinearLayout llRepost = itemView.findViewById(R.id.llRepost);
        final FrameLayout frameLayout = itemView.findViewById(R.id.frameLayout);

        if (alData.get(position).getEvent_interest() != null && !alData.get(position).getEvent_interest().isEmpty()) {
            CategoriesInterestsAdapter adapter = new CategoriesInterestsAdapter(alData.get(position).getEvent_interest(), context, true);
            recyLectureInterests.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            recyLectureInterests.setAdapter(adapter);
            recyLectureInterests.setVisibility(View.VISIBLE);
            adapter.setShouldLoadMore(false);
        }
        else {
            recyLectureInterests.setVisibility(View.GONE);
        }

        if (!UtillsG.getNotNullString(alData.get(position).getThumbnail(), "").isEmpty()) {
            ImageLoader.setImageBig(imgvThumbnail, alData.get(position).getThumbnail());
        }
        else {
            imgvThumbnail.setImageResource(R.drawable.white_square);
        }
        if (alData.get(position).getOwnerUserdata() != null) {
            if (!UtillsG.getNotNullString(alData.get(position).getOwnerUserdata().getProfile_pic(), "").isEmpty()) {
                ImageLoader.setImageBig(imgvUserImage, alData.get(position).getOwnerUserdata().getProfile_pic());
            }
            else {
                ImageLoader.setImageBig(imgvUserImage, "https://images.designtrends.com/wp-content/uploads/2015/11/30162730/White-and-Grey-Plain-Background.jpg");
            }
        }
        else {
            ImageLoader.setImageBig(imgvUserImage, alData.get(position).getUserdata().getProfile_pic());
        }

        txtvFavouriteCount.setText(alData.get(position).getFavourites_count());
        txtvCommentsCount.setText(alData.get(position).getComments_count());
        txtvRepostedCount.setText(alData.get(position).getReposted_count());
        txtvViewsCount.setText(alData.get(position).getLecture_view());

        layPlay.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post("");
                if (UtillsG.isAudioVideoMediaPost(alData.get(position))) {
                    setLectureAsPlayed(position);
                    updateSuggestionCounter(position);

                    PublicAudioPlayer.setReleaseAudioPlayer();

                    PlayerActivity.startWithAds(context, alData.get(position), false);
                }
                else if (UtillsG.isTextPost(alData.get(position))) {
//                            UtillsG.showToast("It is a text Lecture.", context, true);
                }
            }
        });
        linearMain.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                layPlay.performClick();
            }
        });

        imgvUserImage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                if (alData.get(position).getOwnerUserdata() != null) {
                    if (!UtillsG.getNotNullString(alData.get(position).getOwnerUserdata().getProfile_pic(), "").isEmpty()) {
                        UtillsG.showFullImage(Web.Path.BASE_URL + alData.get(position).getOwnerUserdata().getProfile_pic(), context, false);

                    }
                    else {
                        UtillsG.showFullImage("https://images.designtrends.com/wp-content/uploads/2015/11/30162730/White-and-Grey-Plain-Background.jpg", context, false);
                    }
                }
                else {

                    UtillsG.showFullImage(Web.Path.BASE_URL + alData.get(position).getUserdata().getProfile_pic(), context, false);

                }


            }
        });
        ll_repostDetails.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                       /* if (alData.get(position).getIs_reposted()) {
                            UtillsG.showFullImage(Web.Path.BASE_URL + new SharedPrefHelper(context).getProfilePic(), context, false);
                        } else {
                            UtillsG.showFullImage(Web.Path.BASE_URL + alData.get(position).getUserdata().getProfile_pic(), context, false);
                        }*/
                ActivityOtherUserDetails.start(context, alData.get(position).getUserdata(), true);
            }
        });

        if (alData.get(position).getIs_favourite().equalsIgnoreCase("1")) {
            imgvLike.setImageResource(R.drawable.ic_liked);
            imgvLike.setColorFilter(ContextCompat.getColor(context, R.color.redTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
        }
        else {
            imgvLike.setImageResource(R.mipmap.ic_fav_white_border);
            imgvLike.setColorFilter(ContextCompat.getColor(context, R.color.dialogBackgroundColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        }

        if (alData.get(position).getIs_reposted()) {
            imgv_repost.setColorFilter(ContextCompat.getColor(context, R.color.greenTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
        }
        else {
            imgv_repost.setColorFilter(ContextCompat.getColor(context, R.color.dialogBackgroundColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        }

        layLike.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                likeUnlikeLecture(position, imgvLike);
            }
        });

        imgvOptions.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                DialogHelper.getInstance().showLectureMoreOptions(context, imgvOptions,
                        alData.get(position).getIs_reposted(), alData.get(position).getUser_id().equalsIgnoreCase(new SharedPrefHelper(context).getUserId()), new CallBackG<Integer>()
                        {
                            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                            @Override
                            public void onCallBack(final Integer attendType) {

                                if (attendType == 1) {
                                    layPlay.performClick();
                                }
                                else if (attendType == 2) {
                                    layLike.performClick();
                                }
                                else if (attendType == 3) {
                                    ll_connect.performClick();
                                }
                                else if (attendType == 4) {

                                }
                                else if (attendType == 5) {
                                    llRepost.performClick();
                                }
                                else if (attendType == 6) {
                                    if (alData.get(position).getDiscovery_url() != null && !alData.get(position).getDiscovery_url().trim().isEmpty()) {
                                        WebViewRecorderActivity.startForBrowsingOnly(context, alData.get(position).getDiscovery_url());
                                    }
                                    else {
                                        UtillsG.showToast("Url is empty", context, true);
                                    }
                                }
                                else if (attendType == 7) {
                                    Call<BasicApiModel> shareExternally = LVApplication.getRetrofit().create(HomeApis.class).
                                            save_external_share("0",
                                                    alData.get(position).getQuick_lecture_id(),
                                                    new SharedPrefHelper(context).getUserId(),
                                                    alData.get(position).getUser_id(),
                                                    alData.get(position).getUser_id().equals(new SharedPrefHelper(context).getUserId())
                                                            ? " You "
                                                            : new SharedPrefHelper(context).getUserName(),
                                                    alData.get(position).getQuick_lecture_text());
                                    shareExternally.enqueue(new Callback<BasicApiModel>()
                                    {
                                        @Override
                                        public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                                            Log.e("external shr success", "---------------------------------");
                                        }

                                        @Override
                                        public void onFailure(Call<BasicApiModel> call, Throwable t) {
                                            Log.e("external shr failed", t.getMessage() + "---------------------------------");
                                        }
                                    });
//                                    Intent sendIntent = new Intent();
//                                    sendIntent.setAction(Intent.ACTION_SEND);
//                                    sendIntent.putExtra(Intent.EXTRA_TEXT, Web.Path.BASE_URL + alData.get(position).getFile_path());
//                                    sendIntent.setType("text/plain");
//                                    context.startActivity(sendIntent);

                                    UtillsG.customShareDialog(context, Web.Path.EXTERNAL_SHARE_URL_QUICK_LECTURE + alData.get(position).getQuick_lecture_id(), alData.get(position).getQuick_lecture_id(), "lecture");

                                }
                                else if (attendType == 8) {
                                    DialogHelper.getInstance().executeDeleteLecture(alData.get(position), context, new CallBackG<Boolean>()
                                    {
                                        @Override
                                        public void onCallBack(Boolean output) {
                                            if (output) {
                                                alData.remove(position);
                                                notifyDataSetChanged();
                                            }
                                        }
                                    });
                                }


                            }
                        });
            }
        });

        ll_comments.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post("");
                if (UtillsG.isAudioVideoMediaPost(alData.get(position))) {
                    setLectureAsPlayed(position);
                    PlayerActivity.startWithAds(context, alData.get(position), true);
                }
                else if (UtillsG.isTextPost(alData.get(position))) {

                    TextLectureCommentsActivity.start(context, alData.get(position));
                }
            }
        });


        ll_connect.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                ActivityOtherUserDetails.start(context, alData.get(position).getUserdata(), true);
            }
        });


        llRepost.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                RepostLecture(position, txtvRepostedCount);
            }
        });

        if (alData.get(position).getQuick_lecture_type().equalsIgnoreCase("text")
                || alData.get(position).getQuick_lecture_type().equalsIgnoreCase("discovery_text")) {
            layPlay.setVisibility(View.GONE);
//                    ((LectureViewHolder) holder).viewTop.setVisibility(View.VISIBLE);
            actxtvLectureTitle.setMaxLines(2);
            actxtvLectureTitle.setVisibility(View.GONE);
            txtvDuration.setVisibility(View.GONE);
            txtvTitleBig.setVisibility(View.VISIBLE);
            txtvTitleBig.setText(alData.get(position).getQuick_lecture_text());
            actxtvLectureTitle.setText(alData.get(position).getQuick_lecture_text());
            UtillsG.setHeightWidtWRAP_LinearLayout(((Activity) context), frameLayout, context);
            txtvDuration.setText("");
        }
        else {
//                    ((LectureViewHolder) holder).viewTop.setVisibility(View.INVISIBLE);
            txtvDuration.setText(UtillsG.getNotNullString(alData.get(position).getQuick_lecture_time_duration(), ""));
            layPlay.setVisibility(View.VISIBLE);
            actxtvLectureTitle.setMaxLines(2);
            actxtvLectureTitle.setVisibility(View.VISIBLE);
            txtvDuration.setVisibility(View.VISIBLE);
            txtvTitleBig.setVisibility(View.GONE);

            txtvTitleBig.setText(alData.get(position).getQuick_lecture_text());
            actxtvLectureTitle.setText(alData.get(position).getQuick_lecture_text());

            UtillsG.setHeightWidthWithRatio_LinearLayout(((Activity) context), frameLayout, 7f, 16f);
        }

        UtillsG.setTextSizeByPercentage(context, actxtvLectureTitle, 4);
        UtillsG.setTextSizeByPercentage(context, txtvTitleBig, 6);


        String timeAgo = "";

        if (mRootFragment != null && mRootFragment instanceof HomeFragment_NoBinding &&
                alData.get(position).getIs_reposted()
                && alData.get(position).getUserdata().getUser_id().equals(new SharedPrefHelper(context).getUserId())) {
            ImageLoader.setImageSmall(imgvRepostUserImage, alData.get(position).getOwnerUserdata().getProfile_pic());
            timeAgo = UtillsG.get_time_ago(alData.get(position).getDate_time());
            txtvTimeAgo.setText(timeAgo);
            txtvUserName.setText(alData.get(position).getOwnerUserdata().getFull_name()
                    + " Posted a New " + UtillsG.QuickLectureType(alData.get(position).getQuick_lecture_type(), "post") + ".");
        }
        else if ((alData.get(position).getOwnerUserdata() != null
                && alData.get(position).getUserdata().getUser_id().
                equals(alData.get(position).getOwnerUserdata().getUser_id()))
                || alData.get(position).getOwnerUserdata() == null
                ) {
            ImageLoader.setImageSmall(imgvRepostUserImage, alData.get(position).getUserdata().getProfile_pic());
            timeAgo = UtillsG.get_time_ago(alData.get(position).getDate_time());
            txtvTimeAgo.setText(timeAgo);
            txtvUserName.setText(alData.get(position).getUserdata().getFull_name()
                    + " Posted a New " + UtillsG.QuickLectureType(alData.get(position).getQuick_lecture_type(), "post") + ".");

        }
        else {
            timeAgo = UtillsG.get_time_ago(alData.get(position).getReposted_date_time());
            txtvUserName.setText(alData.get(position).getUserdata().getFull_name()
                    + " [img src=ic_refresh_green/]  Re-Posted a " + UtillsG.QuickLectureType(alData.get(position).getQuick_lecture_type(), "post"));

            ImageLoader.setImageSmall(imgvRepostUserImage, alData.get(position).getUserdata().getProfile_pic());
            txtvTimeAgo.setText(timeAgo);
        }


        container.addView(itemView);
        return itemView;
    }

    private void updateSuggestionCounter(int position) {
        Call<BasicApiModel> call = LVApplication.getRetrofit().create(HomeApis.class).suggested_link_counter(alData.get(position).getQuick_lecture_id(), new SharedPrefHelper(context).getUserId(), "play");
        call.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                //       UtillsG.showToast(response.body().getMessage(),context,false);
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {

            }
        });

    }

    private void likeUnlikeLecture(final int position, final ImageView imgvLike) {
        MediaPlayer mp = MediaPlayer.create(context,
                alData.get(position).getIs_favourite().equalsIgnoreCase("0")
                        ? R.raw.tone_like : R.raw.tone_dislike);
        mp.start();

        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance().add_remove_to_favourite_list(new SharedPrefHelper(context).getUserId(), alData.get(position).getQuick_lecture_id());
        basicApiModelCall.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body() != null && response.body().getStatus()) {
                    Log.e("RESPONSE", response.body().getStatus() + "");

                    if (response.body().getStatus()) {
                        int count = Integer.parseInt(alData.get(position).getFavourites_count());
                        UtillsG.showToast(String.valueOf(response.body().getMessage()), context, true);
                        if (alData.get(position).getIs_favourite().equalsIgnoreCase("0")) {
                            alData.get(position).setIs_favourite("1");
                            count++;
                            alData.get(position).setFavourites_count("" + count);
                            imgvLike.setImageResource(R.drawable.ic_liked);
                            imgvLike.setColorFilter(ContextCompat.getColor(context, R.color.redTheme), android.graphics.PorterDuff.Mode.MULTIPLY);
                        }
                        else {
                            alData.get(position).setIs_favourite("0");
                            count--;
                            alData.get(position).setFavourites_count("" + count);
                            imgvLike.setImageResource(R.mipmap.ic_fav_white_border);
                            imgvLike.setColorFilter(ContextCompat.getColor(context, R.color.greyDark), android.graphics.PorterDuff.Mode.MULTIPLY);
                        }
                        alData.get(position).setFavourites_count(String.valueOf(count));
            /*
             * If user is in Favourite screen then it should be removed from list after Unliking
             */
                        if (alData.get(position).getIs_favourite().equalsIgnoreCase("0")) {
                            if (context instanceof FavouriteLecturesListActivity) {
                                alData.remove(position);
                                notifyDataSetChanged();
                            }
                            else {
                                notifyDataSetChanged();
                            }
                        }
                        else {
                            notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("ERROR get_posted_le", t.getMessage().toString());
                UtillsG.showToast(String.valueOf(t.getMessage().toString()), context, true);
            }
        });
    }

    private void RepostLecture(final int position, final TextView txtvRepostedCount) {
        if (alData.get(position).getUser_id().equals(new SharedPrefHelper(context).getUserId())
                ||
                (alData.get(position).getOwnerUserdata() != null &&
                        alData.get(position).getOwnerUserdata().getUser_id().equals(new SharedPrefHelper(context).getUserId()))) {
            UtillsG.showToast("You are the creator so you cannot repost it.", context, true);
        }
        else if (!alData.get(position).getIs_reposted()) {

            MediaPlayer mp = MediaPlayer.create(context, R.raw.tone_like);
            mp.start();

            mClicks.RepostQuickLecture((Activity) context, LVApplication.getRetrofit().create(HomeApis.class),
                    alData.get(position).getQuick_lecture_id(), new CallBackG<Boolean>()
                    {
                        @Override
                        public void onCallBack(Boolean output) {
                            if (output) {
                                int repostCount = Integer.parseInt(alData.get(position).getReposted_count());
                                repostCount++;
                                txtvRepostedCount.setText("" + repostCount);
                                alData.get(position).setReposted_count(String.valueOf(repostCount));
                                alData.get(position).setIs_reposted(true);
                                notifyDataSetChanged();
                                EventBus.getDefault().post(new HomeFragmentUpdateBus(true));

                            }
                        }
                    });
        }
        else if (alData.get(position).getIs_reposted()) {

            MediaPlayer mp = MediaPlayer.create(context, R.raw.tone_dislike);
            mp.start();

            mClicks.unrepost_lecture((Activity) context,
                    alData.get(position).getQuick_lecture_id() != null ?
                            alData.get(position).getQuick_lecture_id()
                            : ""
                    , new CallBackG<Boolean>()
                    {
                        @Override
                        public void onCallBack(Boolean output) {
                            if (output) {
                                int repostCount = Integer.parseInt(alData.get(position).getReposted_count());
                                repostCount--;
                                txtvRepostedCount.setText("" + repostCount);
                                alData.get(position).setReposted_count(String.valueOf(repostCount));
                                alData.get(position).setIs_reposted(false);
                                notifyDataSetChanged();
                                EventBus.getDefault().post(new HomeFragmentUpdateBus(true));
                            }
                        }
                    });
        }
        else {
            UtillsG.showToast("Already reposted this lecture", context, true);
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }


    private void setLectureAsPlayed(final int position) {
        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance().add_lecture_to_recent_played_list(new SharedPrefHelper(context).getUserId(), alData.get(position).getQuick_lecture_id());
        basicApiModelCall.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body() != null && response.body().getStatus()) {
                    Log.e("RESPONSE", response.body().getStatus() + "");

                    if (response.body().getStatus()) {
                        int count = Integer.parseInt(alData.get(position).getLecture_view());
                        count++;
                        alData.get(position).setLecture_view(String.valueOf(count));
                        notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("ERROR get_posted_le", t.getMessage().toString());
            }
        });
    }

    protected HomeApis getRetrofitInstance() {
        return LVApplication.getRetrofit().create(HomeApis.class);
    }
}