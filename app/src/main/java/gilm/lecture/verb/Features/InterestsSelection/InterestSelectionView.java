package gilm.lecture.verb.Features.InterestsSelection;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * created by PARAMBIR SINGH on 31/8/17.
 */

interface InterestSelectionView extends Viewable<InterestSelectionPresenter>
{
}
