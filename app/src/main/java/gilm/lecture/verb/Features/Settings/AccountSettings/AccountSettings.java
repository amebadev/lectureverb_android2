package gilm.lecture.verb.Features.Settings.AccountSettings;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CompoundButton;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.LoginRegisteration.LoginRegisterApi;
import gilm.lecture.verb.Features.Settings.SettingsApi;
import gilm.lecture.verb.Features.Splash.SplashActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.ActivityAccountSettingsBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountSettings extends BaseActivity<ActivityAccountSettingsBinding, AccountSettingsPresenter> implements AccountSettingsView
{

    public static void start(Context context)
    {
        Intent starter = new Intent(context, AccountSettings.class);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId()
    {
        return R.layout.activity_account_settings;
    }

    @Override
    protected void onCreateActivityG()
    {
        injectPresenter(new AccountSettingsPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews()
    {
        setupToolbar("Account Settings");
        getDataBinder().llPrivacySubOptions.setVisibility(View.GONE);

        getDataBinder().txtvPrivacy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                getDataBinder().txtvPrivacy.setTypeface(
                        getDataBinder().llPrivacySubOptions.getVisibility() == View.VISIBLE
                                ? Typeface.DEFAULT : Typeface.DEFAULT_BOLD);

                getDataBinder().llPrivacySubOptions.setVisibility(
                        getDataBinder().llPrivacySubOptions.getVisibility() == View.VISIBLE
                                ? View.GONE : View.VISIBLE);
            }
        });

        getPrivacyStatus();

        getDataBinder().switchPublicPrivate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                updatePrivacyOnServer(isChecked);
            }
        });
        getDataBinder().txtvDeleteAccount.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                DialogHelper.getInstance().deleteAccountDialog(getActivityG(), new CallBackG() {
                    @Override
                    public void onCallBack(Object output) {
                        UtillsG.showLoading("Please wait..", getActivityG());
                        Call<BasicApiModel> callApi = LVApplication.getRetrofit().create(LoginRegisterApi.class).delete_user_account(getLocalData().getUserId());
                        callApi.enqueue(new Callback<BasicApiModel>() {
                            @Override
                            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                                UtillsG.hideLoading();

                                if (response.body() != null && response.body().getStatus()) {
                                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                                    getLocalData().logOut();
                                    SplashActivity.start(getActivityG());
                                } else {
                                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                                }
                            }

                            @Override
                            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                                UtillsG.showToast("An error ha occured.", getActivityG(), true);
                                UtillsG.hideLoading();
                            }
                        });
                    }
                });

            }
        });
    }

    private void getPrivacyStatus()
    {
        getDataBinder().switchPublicPrivate.setChecked(getLocalData().getProfilePrivacy().equalsIgnoreCase("private"));
        Call<BasicApiModel> call = LVApplication.getRetrofit().create(SettingsApi.class).get_profile_privacy(getLocalData().getUserId());
        call.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
            {
                getDataBinder().switchPublicPrivate.setChecked(response.body().getMessage().equalsIgnoreCase("private"));
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t)
            {
                UtillsG.showToast(t.getMessage(), getActivityG(), true);
            }
        });
    }

    private void updatePrivacyOnServer(final boolean isChecked)
    {
        showLoading("Please wait..");
        Call<BasicApiModel> call = LVApplication.getRetrofit().create(SettingsApi.class).set_profile_privacy(
                getLocalData().getUserId()
                , isChecked ? "private" : "public"
        );
        call.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
            {
                hideLoading();
                UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                getLocalData().setProfilePrivacy(isChecked ? "private" : "public");
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t)
            {
                hideLoading();
                UtillsG.showToast(t.getMessage(), getActivityG(), true);
            }
        });
    }

    @Override
    public Context getActivityG()
    {
        return AccountSettings.this;
    }

    private void showPrivacyDialog(final CallBackG<String> callback)
    {
        String[] items = {"Anyone", "Private"};
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivityG());
        dialog.setTitle("Please select");
        dialog.setItems(items, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                switch (which)
                {
                    case 0:
                        callback.onCallBack("public");
                        break;
                    case 1:
                        callback.onCallBack("private");
                        break;
                }
            }
        });
        dialog.show();
    }
}
