/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Settings.Notifications;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.databinding.FragmentNotificationSettingsBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationSettingsFragment extends BaseFragment<FragmentNotificationSettingsBinding, NotificationSettingsPresenter> implements NotificationSettingsView
{
    public ListView listOfSounds;

    public static NotificationSettingsFragment newInstance() {
        return new NotificationSettingsFragment();
    }

    public NotificationSettingsFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_notification_settings;
    }

    @Override
    protected void onCreateFragmentG() {
        injectPresenter(new NotificationSettingsPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        getDataBinder().setBinder(getPresenter());

        /**
         *frameProfileImage is included using a layout  so it cannot be initialized using binder
         */
        UtillsG.setHeightWidthWithRatio_LinearLayout(getActivity(), (getView().findViewById(R.id.frameProfileImage)), 3f, 6f);

        getDataBinder().txtvVibrationOption.setText(new SharedPrefHelper(getActivityG()).getVibrationName());
        getDataBinder().txtvSoundName.setText(new SharedPrefHelper(getActivityG()).getSoundName());
        getDataBinder().txtvLightName.setText(new SharedPrefHelper(getActivityG()).getNotificationLightName());
        getDataBinder().acCheckBoxSound.setChecked(new SharedPrefHelper(getActivityG()).isNotificationSoundEnabled());
        getDataBinder().acCheckBoxSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                new SharedPrefHelper(getActivityG()).enableNotificationSound(b);
            }
        });

        getDataBinder().llSelectBlinkLight.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                openColorSelectionDialog();
            }
        });

        getDataBinder().llNotificationSound.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                openSoundSelectionDialog();
            }
        });
        getDataBinder().llVibration.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                showVibrationsOptions();
            }
        });
    }

    private void openColorSelectionDialog() {

        final String[] colorsList = {"White", "Green", "Orange", "Blue"};
        final String[] colorsCodeList = {"#FFFFFF", "#C900FF72", "#C9FFBF00", "#C9005EFF"};

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivityG());
        dialog.setTitle("Select Light Color");
        dialog.setItems(colorsList, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new SharedPrefHelper(getActivityG()).setNotificationLightAndColor(colorsList[i], colorsCodeList[i]);
                getDataBinder().txtvLightName.setText(new SharedPrefHelper(getActivityG()).getNotificationLightName());
            }
        });
        dialog.show();
    }

    public int selectSoundIndex = 0;

    private void openSoundSelectionDialog() {
        final int[] soundsList = {
                R.raw.notify_1,
                R.raw.notify_2,
                R.raw.notify_3,
                R.raw.notify_4,
                R.raw.notify_5,
                R.raw.notify_6,
                R.raw.notify_7,
                R.raw.notify_8,
                R.raw.notify_9,
                R.raw.notify_10,
                R.raw.notify_11,
                R.raw.notify_12,
                R.raw.notify_13,
                R.raw.notify_14,
        };

        final Dialog dialog = new Dialog(getActivityG());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_notification_sound_picker);

        listOfSounds = (ListView) dialog.findViewById(R.id.listOfSounds);
        Button btnSave = (Button) dialog.findViewById(R.id.btnSave);
        final List<String> arrayList = Arrays.asList(getResources().getStringArray(R.array.notificationSoundsList));

        List<Boolean> alIsChecked = new ArrayList<>();
        for (int i = 0; i < soundsList.length; i++) {
            alIsChecked.add(false);
        }

        listOfSounds.setAdapter(new SoundPickerAdapter(getActivityG(), arrayList, soundsList, alIsChecked, this));
        listOfSounds.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectSoundIndex = i;
                MediaPlayer.create(getActivityG(), soundsList[i]).start();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                new SharedPrefHelper(getActivityG()).setSoundId(soundsList[selectSoundIndex], arrayList.get(selectSoundIndex));
                getDataBinder().txtvSoundName.setText(arrayList.get(selectSoundIndex));
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void showVibrationsOptions() {
        final String[] vibrationNames = {"Short", "Medium", "Long"};
        final long[] vibrationPatterns = {100, 500, 1000};

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivityG());
        dialog.setTitle("Select Light Color");
        dialog.setItems(vibrationNames, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Vibrating as demo
                Vibrator v = (Vibrator) getActivityG().getSystemService(Context.VIBRATOR_SERVICE);
                long[] pattern = {vibrationPatterns[i]};
                v.vibrate(pattern, 0);

                //Saving to local storage
                new SharedPrefHelper(getActivityG()).setVibration(vibrationNames[i], vibrationPatterns[i]);
                getDataBinder().txtvVibrationOption.setText(new SharedPrefHelper(getActivityG()).getVibrationName());
            }
        });
        dialog.show();
    }

}
