package gilm.lecture.verb.Features.Favorites.FavouritesLecturesList;

import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.Settings.ArrayListLectureModel;
import gilm.lecture.verb.WebServices.LVApplication;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * created by PARAMBIR SINGH on 26/9/17.
 */

public class FavouriteLecturesListPresenter extends BasePresenter<FavouriteLexturesListView>
{
    public List<Lecture_And_Event_Model> alModel = new ArrayList<>();
    
    
    public void getMoreRecords(String user_id, final int pageNumber) {
        if (pageNumber == 1) {
            getView().getProgressBar().setVisibility(View.VISIBLE);
        }
        Call<ArrayListLectureModel> basicApiModelCall = getRetrofitInstance().get_favourite_lectures(user_id, String.valueOf(pageNumber));
        basicApiModelCall.enqueue(new Callback<ArrayListLectureModel>()
        {
            @Override
            public void onResponse(Call<ArrayListLectureModel> call, Response<ArrayListLectureModel> response) {
                getView().getProgressBar().setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getStatus()) {
                        Log.e("get_lectures : OUTPUT", String.valueOf(response.body().getListOfTagsModel()));
                        if (pageNumber == 1) {
                            alModel.clear();
                        }
                        alModel.addAll(response.body().getListOfTagsModel());
                        getView().getRecyclerView().getAdapter().notifyDataSetChanged();
                    }
                }
                if (alModel.isEmpty()) {
                    getView().getNoDataTextview().setVisibility(View.VISIBLE);
                    getView().getRecyclerView().setVisibility(View.GONE);
                }
                else {
                    getView().getNoDataTextview().setVisibility(View.GONE);
                    getView().getRecyclerView().setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ArrayListLectureModel> call, Throwable t) {
                getView().getProgressBar().setVisibility(View.GONE);
                Log.e("ERROR get_posted_le", t.getMessage().toString());
            }
        });
    }

    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    protected HomeApis getRetrofitInstance() {
        return LVApplication.getRetrofit().create(HomeApis.class);
    }
}
