package gilm.lecture.verb.Features.QuickLecture.Audio.RecordAudio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.shuyu.waveview.AudioWaveView;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.databinding.ActivityRecordingBinding;

public class RecordingActivity extends BaseActivity<ActivityRecordingBinding, RecordingPresenter> implements RecordingView {
    public int hours, minutes, seconds;
    String group_id = "";

    LectureDetailsModel lectureDetailsModel;

    public static void start(Context context, String title) {
        Intent starter = new Intent(context, RecordingActivity.class);
        starter.putExtra("title", title);
        context.startActivity(starter);
        ((Activity) context).finish();
    }

    public static void start(Context context, String title, String group_id) {
        Intent starter = new Intent(context, RecordingActivity.class);
        starter.putExtra("title", title);
        starter.putExtra("group_id", group_id);
        context.startActivity(starter);
        ((Activity) context).finish();
    }

    public static void startForEvent(Context context, String title, LectureDetailsModel lectureDetailsModel) {
        Intent starter = new Intent(context, RecordingActivity.class);
        starter.putExtra("title", title);
        starter.putExtra("lectureDetailsModel", lectureDetailsModel);
        context.startActivity(starter);
        ((Activity) context).finish();
    }

    @Override
    public String getGroupId() {
        return group_id;
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_recording;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new RecordingPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        getDataBinder().setBinder(getPresenter());
        getDataBinder().audioWave.setChangeColor(Color.BLACK, Color.BLACK, Color.BLACK);
        setupToolbar("Record");
        group_id = UtillsG.getNotNullString(getIntent().getStringExtra("group_id"), "0");

        lectureDetailsModel = (LectureDetailsModel) getIntent().getSerializableExtra("lectureDetailsModel");

        getPresenter().initRecorder();

    }

    @Override
    public LectureDetailsModel getEventModel() {
        return lectureDetailsModel;
    }

    @Override
    public TextView getTimerView() {
        return getDataBinder().txtvTimer;
    }

    @Override
    public void startTimer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //checking if recording is starting from start
                if (getDataBinder().txtvTimer.getText().toString().trim().equalsIgnoreCase("record")) {
                    seconds = 0;
                    minutes = 0;
                    hours = 0;
                }
                seconds++;
                if (seconds == 60) {
                    seconds = 0;
                    minutes++;
                    if (minutes == 60) {
                        minutes = 0;
                        hours++;
                    }
                }
                if (minutes == 0) {
                    getDataBinder().txtvTimer.setText("00:" + (seconds < 10 ? ("0" + seconds)
                            : seconds));
                } else if (hours == 0 && minutes > 0) {
                    getDataBinder().txtvTimer.setText((minutes < 10 ? ("0" + minutes)
                            : minutes) + ":" + (
                            seconds < 10 ? ("0" + seconds) : seconds));
                } else if (hours > 0) {
                    getDataBinder().txtvTimer.setText(hours + ":" + (minutes < 10 ? ("0" + minutes)
                            : minutes));
                }
                try {
                    if (getPresenter().isRecordingBoolean) {
                        if (minutes < 10) {
                            startTimer();
                        } else {
                            getPresenter().recordClicked(getDataBinder().btnRecord);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1000);

    }

    @Override
    public int getMinutesRecorded() {
        return minutes;
    }

    @Override
    public void setToolbartitle(String title) {
        setupToolbar(title);
        if (title.equalsIgnoreCase("record")) {
            getDataBinder().txtvTimer.setText(title);
        }
    }

    @Override
    public Context getActivityG() {
        return RecordingActivity.this;
    }

    @Override
    public String getAudioFileName() {
        return "gExpo" + System.currentTimeMillis() + ".m4a";
    }

    @Override
    public void recordingInProgress() {
        getDataBinder().btnRecord.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivityG(), R.color.redTheme)));

    }

    @Override
    public void recordingPaused() {
        getDataBinder().btnRecord.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivityG(), R.color.greenTheme)));

    }

    @Override
    public void recordingStopped() {
        getDataBinder().btnRecord.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivityG(), R.color.greenTheme)));
    }

    @Override
    public AudioWaveView getAudioWave() {
        return getDataBinder().audioWave;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResultAudioTrim(requestCode, resultCode, data);
    }

    @Override
    public String getLectureTitle() {
        return getIntent().getStringExtra("title");
    }
}
