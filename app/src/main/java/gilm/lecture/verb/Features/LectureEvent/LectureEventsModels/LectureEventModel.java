package gilm.lecture.verb.Features.LectureEvent.LectureEventsModels;

import java.io.Serializable;
import java.util.ArrayList;
import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * Created by harpreet on 9/12/17.
 */

public class LectureEventModel extends BasicApiModel {

    private ArrayList<LectureDetailsModel> data;

    public ArrayList<LectureDetailsModel> getmQuickLectureData() {
        return data;
    }

    public void setmQuickLectureData(ArrayList<LectureDetailsModel>
                                             data) {
        this.data = data;
    }



}
