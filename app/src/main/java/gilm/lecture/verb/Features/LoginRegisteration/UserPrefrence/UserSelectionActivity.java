package gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.view.View;
import android.widget.RadioGroup;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.LoginRegisteration.Login.LoginActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.databinding.ActivityUserSelectionBinding;

public class UserSelectionActivity extends BaseActivity<ActivityUserSelectionBinding, UserSelectionPresenter> implements UserSelectionView
{

    public static void start(Context context) {
        Intent starter = new Intent(context, UserSelectionActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_user_selection;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new UserSelectionPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
//        setupToolbar("");
        getDataBinder().setIsStudent(getPresenter().isStudent);

        getDataBinder().radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                getPresenter().onCheckChanged(i);

            }
        });

        getDataBinder().btnSkip.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                if (!getPresenter().isStudent.isStudent()) {
                    getPresenter().skipClicked();
                }
                else {
                    displayError("In Progress");
                }
            }
        });
    }

    @Override
    public View getViewButtonById(int i) {
        return findViewById(i);
    }

    @Override
    public void openLogin() {
        getLocalData().setUserType(getPresenter().selectedUser);
        LoginActivity.start(getActivityG());
    }

    @Override
    public Context getActivityG() {
        return UserSelectionActivity.this;
    }

}
