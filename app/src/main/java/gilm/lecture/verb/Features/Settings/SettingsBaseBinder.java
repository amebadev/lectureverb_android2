/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Settings;

import android.databinding.ObservableField;
import android.view.View;

/**
 * Created by G-Expo on 20 Jul 2017.
 */

public interface SettingsBaseBinder
{
    /**
     * change current fragment to {@link gilm.lecture.verb.Features.Settings.MainSettings.SettingsFragment}
     * in {@link gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity} screen.
     */
    void openMainSettings(View view);

    /**
     * @return profile image URL,of current logged in user.
     */
    ObservableField<String> profileImage();
    /**
     * @return profile image URL,of current logged in user.
     */
    ObservableField<String> coverProfileImage();

    /**
     * @return user name,of current logged in user.
     */
    ObservableField<String> name();

}
