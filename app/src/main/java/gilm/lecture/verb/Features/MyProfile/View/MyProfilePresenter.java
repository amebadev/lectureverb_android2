/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.MyProfile.View;

import android.app.Activity;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.view.View;

import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 21 Jul 2017.
 */

public class MyProfilePresenter extends BasePresenter<MyProfileView> {

    public ObservableField<String> profileImage = new ObservableField<>();
    public ObservableField<String> coverProfileImage = new ObservableField<>();

    public ObservableField<String> name = new ObservableField<>();

    public ObservableField<String> bio = new ObservableField<>();
    public String bioDefaultText = "No bio available ..!";
    public ObservableField<String> website = new ObservableField<>();
    public ObservableField<String> location = new ObservableField<>();
    UserDataModel.UserData mUserData = null;

    public MyProfilePresenter(UserDataModel.UserData mUserData) {
        this.mUserData = mUserData;
        name.set("");
        coverProfileImage.set("");
        profileImage.set("");
        bio.set(bioDefaultText);
        website.set("");
        location.set("");
    }

    public void editProfile(View view) {
        if (mUserData != null
                && !mUserData.getUser_id().equals(getView().getLocalData().getUserId())) {
            Call<BasicApiModel> follow = LVApplication.getRetrofit().create(ProfileApi.class)
                    .follow_user(getView().getLocalData().getUserId(), mUserData.getUser_id());
            follow.enqueue(new Callback<BasicApiModel>() {
                @Override
                public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                    if (response.body().getStatus()) {
                        mUserData.setIs_following(
                                (UtillsG.getNotNullString(mUserData.getIs_following(), "0").equalsIgnoreCase("1")
                                        || UtillsG.getNotNullString(mUserData.getIs_following(), "0").equalsIgnoreCase("true"))
                                        ? "0" : "1");
                        UtillsG.showToast(response.body().getMessage(), getView().getActivityG(), true);

                        //opening this activity again
                        ActivityOtherUserDetails.start(getView().getActivityG(), mUserData.getUser_id(), "User Profile");
                        ((Activity) getView().getActivityG()).finish();
                    } else {
                        UtillsG.showToast(response.body().getMessage(), getView().getActivityG(), true);
                    }
                }

                @Override
                public void onFailure(Call<BasicApiModel> call, Throwable t) {
                    UtillsG.showToast(t.getMessage(), getView().getActivityG(), true);
                }
            });
        } else {
            getView().openProfileSettings();
        }
    }

    public void initLocalData() {

        if (mUserData != null) {

            name.set(mUserData.getFull_name());
            profileImage.set(mUserData.getProfile_pic());
            coverProfileImage.set(mUserData.getCover_pic());
            bio.set(mUserData.getBio());
            website.set(mUserData.getWebsite());
            location.set(mUserData.getLocation());

        } else {

            name.set(getView().getLocalData().getUserName());
            profileImage.set(getView().getLocalData().getProfilePic());
            coverProfileImage.set(getView().getLocalData().getCoverProfilePic());
            bio.set(getView().getLocalData().getBio());
            website.set(getView().getLocalData().getWebsite());
            location.set(getView().getLocalData().getLocation());
        }

    }

    @Override
    public void attachView(@NonNull MyProfileView viewable) {
        super.attachView(viewable);
        initLocalData();
    }

    public void refreshUserData() {
        //refresh data from local storage here.
        initLocalData();
    }

}
