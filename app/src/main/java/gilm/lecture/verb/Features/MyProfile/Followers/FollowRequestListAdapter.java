package gilm.lecture.verb.Features.MyProfile.Followers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import gilm.lecture.verb.Features.Connect.Contact.ContactListFragment;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * created by PARAMBIR SINGH on 10/10/17.
 */

public class FollowRequestListAdapter extends RecyclerView.Adapter<FollowRequestListAdapter.MyViewHolderG> {


    private final LayoutInflater inflater;
    private List<UserDataModel.UserData> alData;
    private Context context;
    ContactListFragment connectsFragment;
    private boolean showButtons;

    public FollowRequestListAdapter(Context context, List<UserDataModel.UserData> alData, ContactListFragment connectsFragment, boolean showButtons) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.alData = alData;
        this.connectsFragment = connectsFragment;
        this.showButtons = showButtons;
    }

    @Override
    public MyViewHolderG onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolderG(inflater.inflate(R.layout.inflater_follow_requests, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolderG holder, final int position) {
        ImageLoader.setImageRoundSmall(holder.imgvUserImage, alData.get(position).getProfile_pic());
        holder.txtvUserName.setText(alData.get(position).getFull_name());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityOtherUserDetails.start(context, alData.get(position).getUser_id(), "User Profile");
            }
        });

        if (showButtons) {
            holder.txtvAccept.setVisibility(View.VISIBLE);
            holder.txtvReject.setVisibility(View.VISIBLE);
        } else {
            holder.txtvAccept.setVisibility(View.GONE);
            holder.txtvReject.setVisibility(View.GONE);
        }

        holder.txtvAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptRejectRequest(position, true);
            }
        });
        holder.txtvReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptRejectRequest(position, false);
            }
        });
    }

    private void acceptRejectRequest(final int position, boolean is_approved) {
        UtillsG.showLoading("Please wait..", context);
        Call<BasicApiModel> getFollowersList = getRetrofitInstance().approve_reject_follow_request(
                new SharedPrefHelper(context).getUserId(), alData.get(position).getUser_id(), String.valueOf(is_approved));
        getFollowersList.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                UtillsG.hideLoading();
                if (response.body().getStatus()) {
                    alData.remove(position);
                    notifyDataSetChanged();
                } else {
                    UtillsG.showToast(response.body().getMessage(), context, true);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                UtillsG.showToast(t.getMessage(), context, true);
                UtillsG.hideLoading();
            }
        });
    }

    @Override
    public int getItemCount() {
        return alData.size();
    }

    class MyViewHolderG extends RecyclerView.ViewHolder {
        View view;
        ImageView imgvUserImage;
        TextView txtvUserName, txtvAccept, txtvReject;

        public MyViewHolderG(View row) {
            super(row);

            view = row;
            imgvUserImage = (ImageView) view.findViewById(R.id.imgvUserImage);
            txtvUserName = (TextView) view.findViewById(R.id.txtvUserName);
            txtvAccept = (TextView) view.findViewById(R.id.txtvAccept);
            txtvReject = (TextView) view.findViewById(R.id.txtvReject);
        }
    }

    private ProfileApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(ProfileApi.class);
    }
}
