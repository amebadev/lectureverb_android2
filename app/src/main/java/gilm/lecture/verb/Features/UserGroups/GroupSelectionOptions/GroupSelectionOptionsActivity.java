package gilm.lecture.verb.Features.UserGroups.GroupSelectionOptions;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.LoginRegisteration.Login.LoginActivity;
import gilm.lecture.verb.Features.UserGroups.JoinGroup.AllGroupsListActivity;
import gilm.lecture.verb.Features.UserGroups.OptionsInfoActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.databinding.ActivityGroupSelectionOptionsBinding;

public class GroupSelectionOptionsActivity extends BaseActivity<ActivityGroupSelectionOptionsBinding, GroupSelectionPresenter> implements GroupSelectionView, View.OnClickListener
{

    public static void start(Context context) {
        Intent starter = new Intent(context, GroupSelectionOptionsActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_group_selection_options;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new GroupSelectionPresenter());
        getPresenter().attachView(this);

    }

    @Override
    public void initViews() {
        setupToolbar("");

        getDataBinder().txtvCreateGroup.setOnClickListener(this);
        getDataBinder().txtvLogin.setOnClickListener(this);
        getDataBinder().txtvJoinGroup.setOnClickListener(this);
        getDataBinder().imgvInfoJoinGroup.setOnClickListener(this);
        getDataBinder().imgvInfoCreateGroup.setOnClickListener(this);
    }


    @Override
    public Context getActivityG() {
        return GroupSelectionOptionsActivity.this;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtvLogin:
                LoginActivity.start(getActivityG());
                break;
            case R.id.txtvCreateGroup:
                getPresenter().createGroup();
                break;
            case R.id.txtvJoinGroup:
//                UtillsG.showToast("Functionality Under Progress.", getActivityG(), true);

                AllGroupsListActivity.start(getActivityG());

                break;
            case R.id.imgvInfoJoinGroup:
                OptionsInfoActivity.start(getActivityG(), getResources().getString(R.string.text_info_join_group));
                break;
            case R.id.imgvInfoCreateGroup:
                OptionsInfoActivity.start(getActivityG(), getResources().getString(R.string.text_info_create_group));
                break;
        }
    }
}
