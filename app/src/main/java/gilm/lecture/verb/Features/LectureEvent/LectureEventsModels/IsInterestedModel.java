package gilm.lecture.verb.Features.LectureEvent.LectureEventsModels;

import java.io.Serializable;

/**
 * Created by harpreet on 9/22/17.
 */

public class IsInterestedModel implements Serializable {

    private String  id;
    private String event_id;
    private String user_id;
    private String attend_type;
    private String is_interested;
    private String date_time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAttend_type() {
        return attend_type;
    }

    public void setAttend_type(String attend_type) {
        this.attend_type = attend_type;
    }

    public String getIs_interested() {
        return is_interested;
    }

    public void setIs_interested(String is_interested) {
        this.is_interested = is_interested;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }
}