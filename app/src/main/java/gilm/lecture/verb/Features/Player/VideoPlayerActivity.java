package gilm.lecture.verb.Features.Player;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.malmstein.fenster.play.FensterVideoFragment;

import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;

public class VideoPlayerActivity extends AppCompatActivity
{

    public static void start(Context context, String videoUrl) {
        Intent starter = new Intent(context, VideoPlayerActivity.class);
        starter.putExtra(Constants.Extras.DATA, videoUrl);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        setupToolbar("Video Player");

        getVideoFragment().playVideo(getIntent().getStringExtra(Constants.Extras.DATA));
    }


    private FensterVideoFragment getVideoFragment() {
        return (FensterVideoFragment) getFragmentManager().findFragmentById(R.id.play_demo_fragment);
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (viewById != null) {
            viewById.setText(title);
        }
        else {
            getSupportActionBar().setTitle(title);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }



}
