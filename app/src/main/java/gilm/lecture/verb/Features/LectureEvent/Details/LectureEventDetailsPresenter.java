package gilm.lecture.verb.Features.LectureEvent.Details;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.LectureEvent.AttendingLecture;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EditLectureEvent.EditLectureEventActivity;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsApi;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureEventModel;
import gilm.lecture.verb.Features.LectureEvent.RecordAudioVideoEvent.EventRecordingOptionsActivity;
import gilm.lecture.verb.Features.LectureEvent.RecordAudioVideoEvent.MergerModule.AudioRecorder.AudioRecorderActivity;
import gilm.lecture.verb.Features.LectureEvent.RecordAudioVideoEvent.MergerModule.VideoRecorder;
import gilm.lecture.verb.Features.LectureEvent.SendInvitationViaApp.SendInvitationActivity;
import gilm.lecture.verb.Features.VideoProduction.ProductionActivity;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DateHelper;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 11 Jul 2017.
 */

public class LectureEventDetailsPresenter extends BasePresenter<LectureEventDetailsView> implements LectureEventDetailsBinder {

    public LectureDetailsModel mLectureViewModel;

    LectureEventClicks mClicks = new LectureEventClicks();

    @Override
    public void gotoProduction(View view) {
        ProductionActivity.start(getView().getActivityG());
    }

    @Override
    public void gotoEdit(View view) {

        EditLectureEventActivity.start((Activity) getView().getActivityG(), mLectureViewModel);

    }

    @Override
    public void gotoPreview(View view) {
//        LectureMonitoringActivity.start(getView().getActivityG(),mLectureViewModel);
//        DialogHelper.getInstance().showEventRecordingOptions(getView().getActivityG());

        if (mLectureViewModel.getLecturer_co_hosts() != null
                && !mLectureViewModel.getLecturer_co_hosts().isEmpty()
                && mLectureViewModel.getLecturer_co_hosts().get(0).getUser_id() != null
                && mLectureViewModel.getLecturer_data() != null
                && mLectureViewModel.getLecturer_data().getUser_id() != null
                ) {
            if (mLectureViewModel.getLecturer_co_hosts().get(0).getUser_id().equalsIgnoreCase(getView().getLocalData().getUserId())) {
                VideoRecorder.start(getView().getActivityG(), mLectureViewModel);
            } else if (mLectureViewModel.getLecturer_data().getUser_id() != null && mLectureViewModel.getLecturer_data().getUser_id().equalsIgnoreCase(getView().getLocalData().getUserId())) {
                AudioRecorderActivity.start(getView().getActivityG(), mLectureViewModel);
            } else {
                EventRecordingOptionsActivity.start(getView().getActivityG(), mLectureViewModel);
            }
            getView().closeActivity();
        } else {
            EventRecordingOptionsActivity.start(getView().getActivityG(), mLectureViewModel);
        }
    }

    @Override
    public void markAttendant(View view, final LectureEventViewModel viewModel) {
        DialogHelper.getInstance().showAttendentMenu(getView().getActivityG(), view, new CallBackG<String>() {
            @Override
            public void onCallBack(final String attendType) {
                mClicks.setAttendingStatus((Activity) getView().getActivityG(), getRetrofitInstance(), mLectureViewModel.getEvent_id(),
                        attendType.equals(AttendingLecture.GOING_DIGITAL) ? "digital" : "attending", new CallBackG<Boolean>() {
                            @Override
                            public void onCallBack(Boolean output) {
                                if (output) {
                                    loadData(mLectureViewModel.getEvent_id());
                                    viewModel.setIgnored(false);
                                    viewModel.setAttendingLecture(attendType);
                                }
                            }
                        });
            }
        });
    }

    @Override
    public void markInterested(View view, final LectureEventViewModel viewModel) {
        DialogHelper.getInstance().showInterestedMenu(getView().getActivityG(), view, new CallBackG<String>() {
            @Override
            public void onCallBack(final String interestedValue) {
                mClicks.setInterestedStatus((Activity) getView().getActivityG(), getRetrofitInstance(), mLectureViewModel.getEvent_id(),
                        interestedValue.equals(AttendingLecture.IS_INTERESTED) ? "true" : "false", new CallBackG<Boolean>() {
                            @Override
                            public void onCallBack(Boolean output) {
                                if (output) {
                                    loadData(mLectureViewModel.getEvent_id());
                                    viewModel.setIgnored(false);
                                    viewModel.setIsInterested(interestedValue);
                                }
                            }
                        });
            }
        });
    }

    @Override
    public void share(final LectureEventViewModel viewModel) {
        DialogHelper.getInstance().shareEventDialog(viewModel, getView().getActivityG(), "Share event", new CallBackG<Boolean>() {
            @Override
            public void onCallBack(Boolean isShareOnProfile) {
                if (isShareOnProfile) {
                    mClicks.ShareOnUserProfileFeed((Activity) getView().getActivityG(), getRetrofitInstance(), viewModel.getmLectureEvent().getEvent_id(), "event",
                            new CallBackG<Boolean>() {
                                @Override
                                public void onCallBack(Boolean output) {

                                }
                            });
                } else {
                    SendInvitationActivity.start(getView().getActivityG(), viewModel.getLecture_event_id());
                }
            }
        });
    }

    public void loadData(final String lectureEventId) {

        Call<LectureEventModel> basicApiModelCall = getRetrofitInstance().get_single_lecture_event(
                lectureEventId, new SharedPrefHelper(getView().getActivityG()).getUserId());
        basicApiModelCall.enqueue(new Callback<LectureEventModel>() {
            @Override
            public void onResponse(Call<LectureEventModel> call, Response<LectureEventModel> response) {

                if (getView() != null) {
                    if (response.body() != null) {
                        if (response.body().getStatus()) {

                            if (response.body().getmQuickLectureData() != null && response.body().getmQuickLectureData().size() > 0) {


                          /*  LectureDetailsModel mLectureDetails=response.body().getmQuickLectureData().get(0);
                                LectureEventViewModel lectureEventViewModel = new LectureEventViewModel();
                                lectureEventViewModel.setmLectureEvent(response.body().getmQuickLectureData().get(0));
                                lectureEventViewModel.setLecture_event_id(mLectureDetails.getEvent_id());
                                lectureEventViewModel.setTitle(mLectureDetails.getLecture_title());
                                lectureEventViewModel.setStartTime(DateHelper.convertFromServerDateToRequiredDate(mLectureDetails.getLecture_start_date_time(),"dd MMM"));
                                lectureEventViewModel.setImageUrl(mLectureDetails.getLecture_poster());
                                lectureEventViewModel.setMessage(mLectureDetails.getDetails());
                                lectureEventViewModel.setLocation(mLectureDetails.getAddress());
                                lectureEventViewModel.setTimeRange(mLectureDetails.getLecture_created_date_time() +"- "+mLectureDetails.getLecture_end_date_time());
                                lectureEventViewModel.setUpcoming(true);
                                lectureEventViewModel.setCanInviteGuest(mLectureDetails.getCan_invite_friends().equals("true") ? true :false);

                                if(mLectureDetails.getmIsInterested()!=null) {

                                    if(mLectureDetails.getmIsInterested().getAttend_type().equals("digital")) {
                                        lectureEventViewModel.setAttendingLecture(AttendingLecture.GOING_DIGITAL);
                                    }
                                    else if(mLectureDetails.getmIsInterested().getAttend_type().equals("attending")) {
                                        lectureEventViewModel.setAttendingLecture(AttendingLecture.ATTENDING);
                                    }
                                    else {lectureEventViewModel.setAttendingLecture(AttendingLecture.NO_OUTPUT);
                                    }


                                    if(mLectureDetails.getmIsInterested().getIs_interested().equals("true")) {
                                        lectureEventViewModel.setIsInterested(AttendingLecture.IS_INTERESTED);
                                    }
                                    else if(mLectureDetails.getmIsInterested().getIs_interested().equals("false")) {
                                        lectureEventViewModel.setIsInterested(AttendingLecture.IS_IGNORED);
                                    }
                                    else{lectureEventViewModel.setIsInterested(AttendingLecture.NO_OUTPUT);}


                                    lectureEventViewModel.setIgnored(false);
                                }
                                else{
                                    lectureEventViewModel.setAttendingLecture(AttendingLecture.NO_OUTPUT);
                                    lectureEventViewModel.setIsInterested(AttendingLecture.NO_OUTPUT);
                                    lectureEventViewModel.setIgnored(false);
                                }*/

                                LectureDetailsModel mLectureDetails = response.body().getmQuickLectureData().get(0);
                                LectureEventViewModel lectureEventViewModel1 = new LectureEventViewModel();
                                lectureEventViewModel1.setmLectureEvent(response.body().getmQuickLectureData().get(0));
                                lectureEventViewModel1.setLecture_event_id(mLectureDetails.getEvent_id());
                                lectureEventViewModel1.setTitle(mLectureDetails.getLecture_title());
                                lectureEventViewModel1.setStartTime(DateHelper.convertFromServerDateToRequiredDate(mLectureDetails.getLecture_start_date_time(), "dd MMM"));
                                lectureEventViewModel1.setImageUrl(mLectureDetails.getLecture_poster());
                                lectureEventViewModel1.setMessage(mLectureDetails.getDetails());
                                lectureEventViewModel1.setLocation(mLectureDetails.getAddress());
                                lectureEventViewModel1.setTimeRange(mLectureDetails.getLecture_created_date_time() + "- " + mLectureDetails.getLecture_end_date_time());
                                if (response.body().getmQuickLectureData().get(0).getUser_id().equals(new SharedPrefHelper(getView().getActivityG()).getUserId())) {
                                    lectureEventViewModel1.setUpcoming(false);
                                } else {
                                    lectureEventViewModel1.setUpcoming(true);
                                }
                                lectureEventViewModel1.setCanInviteGuest(mLectureDetails.getCan_invite_friends().equals("true") ? true : false);

                                if (mLectureDetails.getmIsInterested() != null) {

                                    if (mLectureDetails.getmIsInterested().getAttend_type().equals("digital")) {
                                        lectureEventViewModel1.setAttendingLecture(AttendingLecture.GOING_DIGITAL);
                                    } else if (mLectureDetails.getmIsInterested().getAttend_type().equals("attending")) {
                                        lectureEventViewModel1.setAttendingLecture(AttendingLecture.ATTENDING);
                                    } else {
                                        lectureEventViewModel1.setAttendingLecture(AttendingLecture.NO_OUTPUT);
                                    }


                                    if (mLectureDetails.getmIsInterested().getIs_interested().equals("true")) {
                                        lectureEventViewModel1.setIsInterested(AttendingLecture.IS_INTERESTED);
                                    } else if (mLectureDetails.getmIsInterested().getIs_interested().equals("false")) {
                                        lectureEventViewModel1.setIsInterested(AttendingLecture.IS_IGNORED);
                                    } else {
                                        lectureEventViewModel1.setIsInterested(AttendingLecture.NO_OUTPUT);
                                    }


                                    lectureEventViewModel1.setIgnored(false);
                                } else {
                                    lectureEventViewModel1.setAttendingLecture(AttendingLecture.NO_OUTPUT);
                                    lectureEventViewModel1.setIsInterested(AttendingLecture.NO_OUTPUT);
                                    lectureEventViewModel1.setIgnored(false);
                                }


                                ((LectureEventDetailsActivity) getView().getActivityG()).updateViewModel(lectureEventViewModel1);


                            } else {
                                UtillsG.showToast("No lecture events yet", getView().getActivityG(), true);
                            }

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<LectureEventModel> call, Throwable t) {

                Log.e("Error occured", t.getMessage().toString());
            }
        });
    }


    public LectureDetailsModel getLectureModel() {
        return mLectureViewModel;
    }

    public void setLectureModel(LectureDetailsModel mLectureViewModel) {
        this.mLectureViewModel = mLectureViewModel;
    }


    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    protected LectureEventsApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(LectureEventsApi.class);
    }
}
