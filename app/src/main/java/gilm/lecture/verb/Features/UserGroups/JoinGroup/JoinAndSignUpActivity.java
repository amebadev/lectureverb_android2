package gilm.lecture.verb.Features.UserGroups.JoinGroup;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.LoginRegisteration.EmailVerification.EmailVerificationActivity;
import gilm.lecture.verb.Features.LoginRegisteration.LoginRegisterApi;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity;
import gilm.lecture.verb.Features.UserGroups.GroupModel;
import gilm.lecture.verb.Features.UserGroups.UserGroupApis;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.Web;
import gilm.lecture.verb.databinding.ActivityJoinAndSignupBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JoinAndSignUpActivity extends BaseActivity<ActivityJoinAndSignupBinding, JoinAndSignUpPresenter>
{
    GroupModel groupModel;

    public static void start(Context context, GroupModel groupModel) {
        Intent starter = new Intent(context, JoinAndSignUpActivity.class);
        starter.putExtra(Constants.Extras.DATA, groupModel);
        context.startActivity(starter);
    }

    @Override
    public void initViews() {
        setupToolbar("Join User Group Network");
        groupModel = (GroupModel) getIntent().getSerializableExtra(Constants.Extras.DATA);

        ImageLoader.setImageRoundSmall(getDataBinder().imgvGroupIcon, UtillsG.getNotNullString(groupModel.getGroup_image(), "null"));
        getDataBinder().txtvGroupName.setText(groupModel.getGroup_title());
        String groupTypeId = groupModel.getGroup_type();
        String groupType = groupTypeId.equals(Web.GroupType.Institute) ? "Institute"
                : groupTypeId.equals(Web.GroupType.PrivateTraining) ? "Private Training"
                        : "Public Group";
        getDataBinder().txtvGroupType.setText(groupType);

        if (groupModel.getData().getUser_id().equals(new SharedPrefHelper(getActivityG()).getUserId())) {
            getDataBinder().txtvAdmin.setVisibility(View.VISIBLE);
        }
        else {
            getDataBinder().txtvAdmin.setVisibility(View.GONE);
        }

        getDataBinder().btnRegister.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                if (isValidated()) {
                    registerAsLecturer();
                }
            }
        });

    }

    private void registerAsLecturer() {
        showLoading("Registering As Lecturer..");
        getPresenter().createApiRequest(LVApplication.getRetrofit().create(LoginRegisterApi.class).register(getDataBinder().edFullName.getText().toString().trim(), getDataBinder().edEmail.getText().toString().trim(), "android", getLocalData().getUserType(), getDataBinder().edPassword.getText().toString().trim()), new CallBackG<UserDataModel>()
        {
            @Override
            public void onCallBack(UserDataModel output) {
                hideLoading();
                if (output.getStatus()) {
                    getLocalData().setIsRegistered(true);
                }
                else {
                    if (output.getMessage().toLowerCase().contains("already exist")/* we are using this string to compare web message so that we can show an alert dialog to user */) {
                        DialogHelper.getInstance().showInformation(getActivityG(), "An account with the same email already exists.", new CallBackG<String>()
                        {
                            @Override
                            public void onCallBack(String output) {
                                UtillsG.hideKeyboard(getActivityG(), getDataBinder().edFullName);
                            }
                        });
                    }
                    else {
                        displayError(output.getMessage());
                    }
                }

                if (output.getData() != null) {
                    getLocalData().setUserData(output.getData());
                    EmailVerificationActivity.startForResult(getActivityG());
                }
            }
        });
    }

    private void joinGroupNow() {
        Call<BasicApiModel> call = LVApplication.getRetrofit().create(UserGroupApis.class).join_group(new SharedPrefHelper(getActivityG()).getUserId(), groupModel.getId());
        call.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body().getStatus()) {
                    DialogHelper.getInstance().showWithAction(getActivityG(), "You have been successfully registered as a Lecturer and joined group \"" + groupModel.getGroup_title() + "\".", new CallBackG<String>()
                    {
                        @Override
                        public void onCallBack(String output) {
                            NavigationActivity.start(getActivityG());
                        }
                    });
                }
                else {
                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("ERROR GET GROUP LIST", t.toString() + "");
            }
        });
    }

    @Override
    public Context getActivityG() {
        return JoinAndSignUpActivity.this;
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_join_and_signup;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new JoinAndSignUpPresenter());
        getPresenter().attachView(this);
    }

    private boolean isValidated() {
        if (getDataBinder().edFullName.getText().toString().trim().isEmpty()) {
            UtillsG.showToast("Please enter your Full Name", getActivityG(), true);
            return false;
        }
        else if (getDataBinder().edEmail.getText().toString().trim().isEmpty()) {
            UtillsG.showToast("Please enter your Email Id", getActivityG(), true);
            return false;
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(getDataBinder().edEmail.getText().toString()).matches()) {
            UtillsG.showToast("Please enter a valid Email Id", getActivityG(), true);
            return false;
        }
        else if (getDataBinder().edPassword.getText().toString().isEmpty()) {
            UtillsG.showToast("Please enter a password.", getActivityG(), true);
            return false;
        }
        else if (getDataBinder().edConfPassword.getText().toString().isEmpty()) {
            UtillsG.showToast("Please enter confirm password.", getActivityG(), true);
            return false;
        }
        else if (!getDataBinder().edConfPassword.getText().toString().equals(getDataBinder().edPassword.getText().toString())) {
            UtillsG.showToast("Password doesn't match.", getActivityG(), true);
            return false;
        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.RequestCode.EMAIL_VERIFICATION) {
                if (data.getBooleanExtra("isSuccess", false)) {
                    getDataBinder().edFullName.setEnabled(false);
                    getDataBinder().edEmail.setEnabled(false);
                    getDataBinder().edPassword.setEnabled(false);
                    getDataBinder().edConfPassword.setEnabled(false);

                    joinGroupNow();
                }
            }
        }
    }


}
