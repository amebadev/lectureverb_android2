package gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent;

import android.view.View;

/**
 * Created by G-Expo on 13 Jul 2017.
 */

public interface CreateEditLectureEventPresenterBinder
{
    /**
     * Set if it is live recording or not.
     *
     * @param isLive
     */
    void liveRecorder(boolean isLive);

    /**
     * show dialog to select image from camera and gallery.
     *
     * @param view
     */
    void selectImage(View view);

    /**
     * show dialog to select start date
     *
     * @param view
     */
    void selectStartDate(View view);

    /**
     * show dialog to select start time
     *
     * @param view
     */
    void selectStartTime(View view);

    /**
     * show dialog to select end date
     *
     * @param view
     */
    void selectEndDate(View view);

    /**
     * show dialog to select end time.
     *
     * @param view
     */
    void selectEndTime(View view);

    /**
     * show dialog to select lecture event location.
     *
     * @param view
     */
    void selectLocation(View view);
}
