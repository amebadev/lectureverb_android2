package gilm.lecture.verb.Features.LoginRegisteration.Login;

import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterSession;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.databinding.ActivityLoginBinding;

/**
 * Created by G-Expo on 05 Jul 2017.
 */

public interface LoginView extends Viewable<LoginPresenter>
{
    /**
     * open {@link gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity}
     * A.K.A <b>Home screen</b>
     */
    void openNavigationActivity(UserDataModel userDataModel);

    /**
     * google plus signin fail.
     */
    void googlePlusSignInFail();

    void twitterFailure(String message);

    void twitterLoggedInSuccessfully(Result<TwitterSession> result);

    ActivityLoginBinding getDataBinder();

    void openEmailVerification();
}
