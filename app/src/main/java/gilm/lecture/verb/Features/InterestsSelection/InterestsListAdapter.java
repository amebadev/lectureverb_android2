package gilm.lecture.verb.Features.InterestsSelection;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.InfiniteAdapter_WithoutBuinding;
import gilm.lecture.verb.UtilsG.LoadingViewHolder;
import gilm.lecture.verb.UtilsG.UtillsG;

/**
 * created by PARAMBIR SINGH on 31/8/17.
 */

class InterestsListAdapter extends InfiniteAdapter_WithoutBuinding<InterestsListAdapter.MyViewHolderG>
{

    private LayoutInflater inflater;
    List<ChildCategoryModel> data;
    Context context;

    public InterestsListAdapter(List<ChildCategoryModel> data, Context context) {
        this.data = data;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        return new LoadingViewHolder(inflater.inflate(R.layout.layout_progress, parent, false));
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public MyViewHolderG onCreateView(ViewGroup parent, int viewType) {
        return new MyViewHolderG(inflater.inflate(R.layout.inflater_interests, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        try {
            if (!(holder instanceof LoadingViewHolder)) {
                ((MyViewHolderG) holder).txtvTitle.setText(data.get(position).getCategory());
                UtillsG.setTextSizeByPercentage(context,((MyViewHolderG) holder).txtvTitle,3f);
                ((MyViewHolderG) holder).txtvParent.setText("(" + data.get(position).getParent_name() + ")");
                ((MyViewHolderG) holder).llInterest.setBackgroundResource(
                        data.get(position).isChecked()
                                ? R.drawable.btn_light_blue_unselected
                                : R.drawable.btn_light_blue_selected);
                ((MyViewHolderG) holder).llInterest.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        data.get(position).setChecked(!data.get(position).isChecked());
                        notifyDataSetChanged();
                    }
                });
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    class MyViewHolderG extends RecyclerView.ViewHolder
    {
        View view;
        TextView txtvTitle, txtvParent;
        LinearLayout llInterest;

        public MyViewHolderG(View row) {
            super(row);

            view = row;
            txtvParent = (TextView) view.findViewById(R.id.txtvParent);
            txtvTitle = (TextView) view.findViewById(R.id.txtvTitle);
            llInterest = (LinearLayout) view.findViewById(R.id.llInterest);
        }
    }

}
