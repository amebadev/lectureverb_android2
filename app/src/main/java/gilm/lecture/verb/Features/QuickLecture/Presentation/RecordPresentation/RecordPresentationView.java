package gilm.lecture.verb.Features.QuickLecture.Presentation.RecordPresentation;

import android.content.Intent;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * created by PARAMBIR SINGH on 19/8/17.
 */

public interface RecordPresentationView extends Viewable<RecordPresentationPresenter>
{

    MediaRecorder getRecorder();

    MediaProjection getMediaProjection();

    void setMediaProjectionNull();

    MediaProjection.Callback getMediaProjectionCallBack();

    void initMediaProjectionCallBack(int resultCode,Intent data);

    void shareScreen();

    void stopScreenSharing();

    void hideRecordingButton();

    void startTimer();

    void hideNotificationBar();

    void setVideoPath(String videoPath);

    void permissionDenied();
    String getTimeDuration();
}
