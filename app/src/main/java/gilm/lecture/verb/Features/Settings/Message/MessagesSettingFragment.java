/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Settings.Message;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CompoundButton;

import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.databinding.FragmentMessagesSettingBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesSettingFragment extends BaseFragment<FragmentMessagesSettingBinding, MessagesSettingPresenter> implements MessagesSettingView
{

    public static MessagesSettingFragment newInstance() {
        return new MessagesSettingFragment();
    }

    public MessagesSettingFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_messages_setting;
    }

    @Override
    protected void onCreateFragmentG() {
        injectPresenter(new MessagesSettingPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        getDataBinder().setBinder(getPresenter());

        /**
         *frameProfileImage is included using a layout  so it cannot be initialized using binder
         */
        UtillsG.setHeightWidthWithRatio_LinearLayout(getActivity(), (getView().findViewById(R.id.frameProfileImage)), 3f, 6f);

        getDataBinder().txtvFontSize.setText("Font Size : " + new SharedPrefHelper(getActivityG()).getFontName());
        getDataBinder().llFontOptions.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                showFontsOptions();
            }
        });

        getDataBinder().acCheckBoxSound.setChecked(new SharedPrefHelper(getActivityG()).isEnterIsSend());
        getDataBinder().acCheckBoxSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                new SharedPrefHelper(getActivityG()).enableEnterIsSend(b);
            }
        });

        getDataBinder().txtvChatHistory.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                ((NavigationActivity) getActivityG()).openHomeTabsFragment(3);
            }
        });
    }

    private void showFontsOptions() {
        final String[] fontNames = {"Small", "Medium", "Large", "Extra Large"};
        final int[] fontSize = {12, 14, 16, 18};

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivityG());
        dialog.setTitle("Select Light Color");
        dialog.setItems(fontNames, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Vibrating as demo
                Vibrator v = (Vibrator) getActivityG().getSystemService(Context.VIBRATOR_SERVICE);
                long[] pattern = {fontSize[i]};
                v.vibrate(pattern, 0);

                //Saving to local storage
                new SharedPrefHelper(getActivityG()).setChatFontSize(fontNames[i], fontSize[i]);
                getDataBinder().txtvFontSize.setText("Font Size : " + new SharedPrefHelper(getActivityG()).getFontName());
            }
        });
        dialog.show();
    }
}
