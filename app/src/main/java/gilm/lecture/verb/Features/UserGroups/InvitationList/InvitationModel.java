package gilm.lecture.verb.Features.UserGroups.InvitationList;

import java.io.Serializable;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.UserGroups.GroupModel;

class InvitationModel implements Serializable
{
    private UserDataModel.UserData UserData;
    private GroupModel groupdata;

    public UserDataModel.UserData getUserData() {
        return UserData;
    }

    public void setUserData(UserDataModel.UserData userData) {
        UserData = userData;
    }

    public GroupModel getGroupdata() {
        return groupdata;
    }

    public void setGroupdata(GroupModel groupdata) {
        this.groupdata = groupdata;
    }
}