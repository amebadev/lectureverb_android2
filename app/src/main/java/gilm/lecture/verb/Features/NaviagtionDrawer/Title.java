package gilm.lecture.verb.Features.NaviagtionDrawer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by G-Expo on 06 Jul 2017.
 */
// LV

@Retention(RetentionPolicy.SOURCE)
public @interface Title
{
    String HOME                  = "Home";
    String ALERT_NOTIFICATION    = "Alert Notification";
    String LECTURE_GROUP_NETWORK             = "Lecture Group Network";
    String FAVORITES             = "Favorites";
    String MY_PROFILE            = "My Profile";
    String DISCOVERY             = "Discovery";
    String CONNECT               = "Connect";
    String LECTURE_EVENTS        = "Lecture Event";
    String RECORD                = "Record";
    String SETTINGS              = "Settings";
    String HELP                  = "Help";
    String RECENT_ACTIVITIES     = "Recent Activity";
    String DIRECT_MESSAGE        = "Direct Message";
    String MESSAGE_SETTINGS      = "Message Settings";
    String NOTIFICATION_SETTINGS = "Notification Settings";
    String TAG_SETTINGS          = "Tag Settings";
    String SEARCH                = "Search";
}
