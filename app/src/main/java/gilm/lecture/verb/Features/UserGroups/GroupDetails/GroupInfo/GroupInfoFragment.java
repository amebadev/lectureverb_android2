package gilm.lecture.verb.Features.UserGroups.GroupDetails.GroupInfo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Home.Notification.Details.UserProfileModel;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.Features.UserGroups.GroupModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * created by PARAMBIR SINGH on 10/10/17.
 */

public class GroupInfoFragment extends Fragment
{
    private GroupModel groupModel;
    View rootView;
    TextView txtv_details, txtvUserName;
    ImageView imgvUserImage;
    RecyclerView recyclerView;
    GroupInfoAdapter groupInfoAdaTextViewpter;
    private View createrGroup;
    private TextView txtvTotalMembers, txtvLecturersCount;

    public GroupInfoFragment()
    {
        // Required empty public constructor
    }


    @SuppressLint("ValidFragment")
    public GroupInfoFragment(GroupModel groupModel)
    {
        this.groupModel = groupModel;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        rootView = getActivity().getLayoutInflater().inflate(R.layout.fragment_only_text, null);

        createrGroup = rootView.findViewById(R.id.createrGroup);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        txtvTotalMembers = (TextView) rootView.findViewById(R.id.txtvTotalMembers);
        txtvLecturersCount = (TextView) rootView.findViewById(R.id.txtvLecturersCount);
        txtv_details = (TextView) rootView.findViewById(R.id.txtv_details);
        txtvUserName = (TextView) rootView.findViewById(R.id.txtvUserName);
        imgvUserImage = (ImageView) rootView.findViewById(R.id.imgvUserImage);
        txtv_details.setText(UtillsG.getNotNullString(groupModel.getGroup_description(), "No Description found.."));
        getProfile(groupModel.getUser_id());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        int lecturersCount = 0;
        for (int i = 0; i < groupModel.getMembersList().size(); i++)
        {
            if (groupModel.getMembersList().get(i).getRole_id().equalsIgnoreCase("2"))
            {
                lecturersCount++;
            }
        }
        txtvLecturersCount.setText("Number lecturers : " + lecturersCount);
        txtvTotalMembers.setText("Total members : " + groupModel.getMembersList().size());

        return rootView;
    }

    private ProfileApi getRetrofitInstance()
    {
        return LVApplication.getRetrofit().create(ProfileApi.class);
    }

    private void getProfile(final String target_id)
    {
        Call<UserProfileModel> userProfileApi = LVApplication.getRetrofit().create(HomeApis.class).get_user_profile(new SharedPrefHelper(getActivity()).getUserId(), target_id);
        userProfileApi.enqueue(new Callback<UserProfileModel>()
        {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response)
            {
                UtillsG.hideLoading();
                if (response.body().getStatus())
                {
                    UserDataModel.UserData userProfileModel = response.body().getData().getUserdata();
                    txtvUserName.setText(userProfileModel.getFull_name());
                    ImageLoader.setImageRoundSmall(imgvUserImage, userProfileModel.getProfile_pic());
                    createrGroup.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            ActivityOtherUserDetails.start(getActivity(), target_id, "User Profile");
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t)
            {
                UtillsG.hideLoading();
            }
        });
    }

}
