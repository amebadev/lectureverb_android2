package gilm.lecture.verb.Features.QuickLecture;

import com.google.gson.annotations.SerializedName;

import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * created by PARAMBIR SINGH on 24/4/18.
 */

public class Single_Lecture_Event_Model extends BasicApiModel
{
    @SerializedName("data")
    Lecture_And_Event_Model quickLectureModels;

    public Lecture_And_Event_Model getQuickLectureModels() {
        return quickLectureModels;
    }

    public void setQuickLectureModels(Lecture_And_Event_Model quickLectureModels) {
        this.quickLectureModels = quickLectureModels;
    }
}
