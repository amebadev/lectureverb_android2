package gilm.lecture.verb.Features.Home.Notification;

import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 06 Jul 2017.
 */

public interface NotificationView extends Viewable<NotificationPresenter>
{
    void showData(List<NotificationViewModel> list,int pageNo);
}
