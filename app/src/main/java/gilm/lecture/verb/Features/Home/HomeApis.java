/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Home;

import gilm.lecture.verb.Features.Home.Notification.Details.UserProfileModel;
import gilm.lecture.verb.Features.Home.Notification.NotificationDataListModel;
import gilm.lecture.verb.Features.Home.ReactedUsers.ReacteddataModel;
import gilm.lecture.verb.Features.Settings.ArrayListLectureModel;
import gilm.lecture.verb.WebServices.AdsModel;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.Web;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by G-Expo on 17 Aug 2017.
 */

public interface HomeApis
{
    /**
     * @param imageUrl
     * @param pageNumber will start from "1"
     * @return
     */
    @FormUrlEncoded
    @POST(Web.Path.get_posted_lectures)
    Call<ArrayListLectureModel> get_posted_lectures(@Field(Web.Keys.USER_ID) String imageUrl, @Field(Web.Keys.page) String pageNumber);

    @FormUrlEncoded
    @POST(Web.Path.search_quick_lectures)
    Call<ArrayListLectureModel> search_quick_lecture(@Field(Web.Keys.USER_ID) String imageUrl, @Field(Web.Keys.keyword) String keyword, @Field(Web.Keys.page) String pageNumber);

    @FormUrlEncoded
    @POST(Web.Path.get_group_lectures)
    Call<ArrayListLectureModel> get_group_lectures(@Field(Web.Keys.USER_ID) String imageUrl, @Field(Web.Keys.group_id) String group_id, @Field(Web.Keys.page) String pageNumber);

    @FormUrlEncoded
    @POST(Web.Path.get_posted_lectures_by_category)
    Call<ArrayListLectureModel> get_posted_lectures_by_category(@Field(Web.Keys.USER_ID) String imageUrl, @Field(Web.Keys.category_id) String category_id, @Field(Web.Keys.page) String pageNumber);

    @GET(Web.Path.MID_URL + Web.Path.get_favourite_lectures)
    Call<ArrayListLectureModel> get_favourite_lectures(@Query(Web.Keys.USER_ID) String imageUrl, @Query(Web.Keys.page) String pageNumber);

    @GET(Web.Path.MID_URL + Web.Path.get_lectures_by_category)
    Call<ArrayListLectureModel> get_lectures_by_category(@Query(Web.Keys.USER_ID) String user_id, @Query(Web.Keys.page) String category_id, @Query(Web.Keys.page) String pageNumber);

    @GET(Web.Path.MID_URL + Web.Path.get_lecture_likes_comments_reposts_list)
    Call<ReacteddataModel> get_lecture_likes_comments_reposts_list(@Query(Web.Keys.USER_ID) String user_id, @Query(Web.Keys.quick_lecture_id) String quick_lecture_id);

    @GET(Web.Path.save_social_share)
    Call<BasicApiModel> save_social_share(
            @Query(Web.Keys.USER_ID) String user_id
            ,@Query(Web.Keys.share_type) String share_type
            ,@Query(Web.Keys.share_to) String share_to
            ,@Query(Web.Keys.share_url) String share_url
            , @Query(Web.Keys.lecture_event_id) String lecture_event_id
    );

    /*
    URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => like_unlike_lecture
    Method = > GET / POST
    Description => Like and unlike quick lecture
    Parameaters = > quick_lecture_id , user_id
    Code ==> 200 for success 400 for error */
    @GET(Web.Path.MID_URL + Web.Path.add_remove_to_favourite_list)
    Call<BasicApiModel> add_remove_to_favourite_list(@Query(Web.Keys.USER_ID) String user_id, @Query(Web.Keys.quick_lecture_id) String quick_lecture_id);

    /*
    URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => like_unlike_lecture
    Method = > GET / POST
    Description => Like and unlike quick lecture
    Parameaters = > quick_lecture_id , user_id
    Code ==> 200 for success 400 for error */
    @GET(Web.Path.MID_URL + Web.Path.like_unlike_lecture)
    Call<BasicApiModel> like_unlike_lecture(@Query(Web.Keys.USER_ID) String user_id, @Query(Web.Keys.quick_lecture_id) String quick_lecture_id);

    /*URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => add_lecture_to_recent_played_list
    Method = > GET / POST
    Description => Like and unlike quick lecture
    Parameaters = > quick_lecture_id , user_id
    Code ==> 200 for success 400 for error	*/
    @GET(Web.Path.MID_URL + Web.Path.add_lecture_to_recent_played_list)
    Call<BasicApiModel> add_lecture_to_recent_played_list(@Query(Web.Keys.USER_ID) String user_id, @Query(Web.Keys.quick_lecture_id) String quick_lecture_id);

    @GET(Web.Path.update_device_token)
    Call<BasicApiModel> update_device_token(@Query(Web.Keys.USER_ID) String user_id, @Query(Web.Keys.device_token) String device_token);

    /*  URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => update_device_details
    Method = > GET / POST
    Description => Update user profile phone
    Parameaters = > user_id, device_name, mac_address, signal_strength, latitude, longitude, microphone
    Code ==> 200 for success 400 for error*/
    @Multipart
    @POST(Web.Path.update_device_details)
    Call<BasicApiModel> update_device_details(
            @Part("user_id") RequestBody user_id
            , @Part("device_name") RequestBody device_name
            , @Part("mac_address") RequestBody mac_address
            , @Part("signal_strength") RequestBody signal_strength
            , @Part("latitude") RequestBody latitude
            , @Part("longitude") RequestBody longitude
            , @Part("microphone") RequestBody microphone
    );


    @FormUrlEncoded
    @POST(Web.Path.get_notifications)
    Call<NotificationDataListModel> get_notifications(@Field(Web.Keys.USER_ID) String user_id, @Field(Web.Keys.page) String page);

    @FormUrlEncoded
    @POST(Web.Path.repost_lecture)
    Call<BasicApiModel> repost_lecture(@Field(Web.Keys.USER_ID) String user_id, @Field("quick_lecture_id") String quick_lecture_id);

    @FormUrlEncoded
    @POST(Web.Path.get_user_profile)
    Call<UserProfileModel> get_user_profile(@Field("my_user_id") String my_user_id, @Field("target_user_id") String target_user_id);


    // http://103.43.152.211/lecture_verb_staging/app/lecture_verb.php?lectureverb=save_external_share
    // share_type = 0 // 0 for lecture 1 for event
    // notify_id
    // user_id
    // owner_id
    // user_name
    @FormUrlEncoded
    @POST(Web.Path.save_external_share)
    Call<BasicApiModel> save_external_share(
            @Field("share_type") String share_type
            , @Field("notify_id") String notify_id
            , @Field("user_id") String user_id
            , @Field("owner_id") String owner_id
            , @Field("user_name") String user_name
            , @Field("lecture_event_name") String lecture_or_event_name);

    @FormUrlEncoded
    @POST(Web.Path.mark_notification_read)
    Call<BasicApiModel> mark_notification_read(
            @Field("notification_id") String notification_id);

    @FormUrlEncoded
    @POST(Web.Path.delete_lecture)
    Call<BasicApiModel> delete_lecture(
            @Field("quick_lecture_id") String user_id);

    @FormUrlEncoded
    @POST(Web.Path.update_counter)
    Call<BasicApiModel> update_counter(
            @Field("user_id") String user_id,
            @Field("counter_type") String counter_type,//counter_type // payment(for credit_card / paypal)social_share(for facebook / google / twitter )
            @Field("meta_key") String meta_key);//credit_card / paypal / facebook / google / twitter


    /**
     * @param ad_screen = connect_screen_ads / lecture_ads / navigation_ads
     * @return
     */
    @FormUrlEncoded
    @POST(Web.Path.get_ads)
    Call<AdsModel> get_ads(
            @Field("ad_screen") String ad_screen);

    @FormUrlEncoded
    @POST(Web.Path.user_ads_counter)
    Call<BasicApiModel> user_ads_counter(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Web.Path.ads_counter)
    Call<BasicApiModel> ads_counter(
            @Field("ad_id") String ad_id);

    @FormUrlEncoded
    @POST(Web.Path.suggested_link_counter)
    Call<BasicApiModel> suggested_link_counter(
            @Field("lecture_id") String lecture_id,
            @Field("user_id") String user_id,
            @Field("action") String action);

}
