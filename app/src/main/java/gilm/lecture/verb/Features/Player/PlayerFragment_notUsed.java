/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Player;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.piterwilson.audio.MP3RadioStreamDelegate;
import com.piterwilson.audio.MP3RadioStreamPlayer;
import com.shuyu.waveview.AudioWaveView;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import gilm.lecture.verb.Features.Home.Home.LectureModel;
import gilm.lecture.verb.R;

/**
 * Created by G-Expo on 10 Aug 2017.
 */

@SuppressLint("ValidFragment")
public class PlayerFragment_notUsed extends Fragment implements MP3RadioStreamDelegate
{
    LectureModel lectureModel;
    SeekBar seekBar;
    AudioWaveView audioWave;
    MP3RadioStreamPlayer player;
    boolean playeEnd;
    ImageView playBtn;
    private boolean seekBarTouch;
    private Timer timer;
    String audioPath;

    @SuppressLint("ValidFragment")
    public PlayerFragment_notUsed(LectureModel lectureModel) {
        this.lectureModel = lectureModel;
    }

    public static PlayerFragment_notUsed newInstance(LectureModel lectureModel) {
        return new PlayerFragment_notUsed(lectureModel);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.music_player, container, false);

       // mediaPath = "http://dll.igeet.me/dataa/emmcu/Yaar_Khade_Ne-Dilpreet_Dhillon%5Bwww.Mp3MaD.Com%5D.mp3";//Web.Path.BASE_URL + lectureModel.getFile_path();

        initViews(view);


        return view;
    }

    private void initViews(View view) {
        audioWave = (AudioWaveView) view.findViewById(R.id.audioWave);
        audioWave.setChangeColor(Color.WHITE, Color.WHITE, Color.WHITE);

        seekBar = (SeekBar) view.findViewById(R.id.seekBAr);
        playBtn = (ImageView) view.findViewById(R.id.imgPlay);
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run() {
                play();
            }
        }, 1000);
        playBtn.setEnabled(false);
        playBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                if (playeEnd) {
                    stop();
                    playBtn.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_rew));
                    seekBar.setEnabled(true);
                    play();
                    return;
                }

                if (player.isPause()) {
                    playBtn.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
                    player.setPause(false);
                    seekBar.setEnabled(false);
                }
                else {
                    playBtn.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause));
                    player.setPause(true);
                    seekBar.setEnabled(true);
                }
            }
        });

        seekBar.setEnabled(false);


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBarTouch = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBarTouch = false;
                if (!playeEnd) {
                    player.seekTo(seekBar.getProgress());
                }
            }
        });

        timer = new Timer();
        timer.schedule(new TimerTask()
        {
            @Override
            public void run() {
                if (playeEnd || player == null || !seekBar.isEnabled()) {
                    return;
                }
                long position = player.getCurPosition();
                if (position > 0 && !seekBarTouch) {
                    seekBar.setProgress((int) position);
                }
            }
        }, 1000, 1000);
    }

    @Override
    public void onRadioPlayerPlaybackStarted(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
        getActivity().runOnUiThread(new Runnable()
        {

            @Override
            public void run() {
                playeEnd = false;
                playBtn.setEnabled(true);
                seekBar.setMax((int) player.getDuration());
                seekBar.setEnabled(true);
            }
        });
    }

    @Override
    public void onRadioPlayerStopped(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
        getActivity().runOnUiThread(new Runnable()
        {

            @Override
            public void run() {
                playeEnd = true;
                playBtn.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
                playBtn.setEnabled(true);
                seekBar.setEnabled(false);
            }
        });
    }

    @Override
    public void onRadioPlayerError(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
        getActivity().runOnUiThread(new Runnable()
        {

            @Override
            public void run() {
                playeEnd = false;
                playBtn.setEnabled(true);
                seekBar.setEnabled(false);
            }
        });
    }

    @Override
    public void onRadioPlayerBuffering(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
        getActivity().runOnUiThread(new Runnable()
        {

            @Override
            public void run() {
                playBtn.setEnabled(false);
                seekBar.setEnabled(false);
            }
        });
    }

    private void play() {
        if (player != null) {
            player.stop();
            player.release();
            player = null;
        }
        player = new MP3RadioStreamPlayer();
        player.setUrlString(getActivity(), true, audioPath);
        // player.setUrlString(getIntent().getStringExtra("uri"));
        player.setDelegate(this);



        //player.setStartWaveTime(5000);
        //audioWave.setDrawBase(false);
        audioWave.setBaseRecorder(player);
        audioWave.startView();
        try {
            player.play();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        int size = getScreenWidth(getActivity()) / dip2px(getActivity(), 1);//控件默认的间隔是1
        player.setDataList(audioWave.getRecList(), size);
    }

    // get width in pixels
    public static int getScreenWidth(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.widthPixels;
    }

    public static int dip2px(Context context, float dipValue) {
        float fontScale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * fontScale + 0.5f);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        audioWave.stopView();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        stop();
    }

    private void stop() {
        player.stop();
    }

}
