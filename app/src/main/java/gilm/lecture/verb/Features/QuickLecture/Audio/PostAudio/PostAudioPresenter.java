package gilm.lecture.verb.Features.QuickLecture.Audio.PostAudio;

import android.app.Activity;
import android.net.Uri;
import android.util.Log;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EventLectureApi;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsApi;
import gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence.UserType;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureApi;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureModel;
import gilm.lecture.verb.Features.QuickLecture.ThumbnailResponseModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 18 Jul 2017.
 */

public class PostAudioPresenter extends BasePresenter<PostAudioView> implements ProgressRequestBody.UploadCallbacks {

    Uri selectedArtWork;
    String discoveryUrl = "";
    String commaSeparatedIds = "";
    LectureEventClicks mClicks = new LectureEventClicks();


    public void setDiscoveryUrl(String discoveryUrl) {
        this.discoveryUrl = discoveryUrl;
    }

    public void cropMeImage(Uri uri) {
        CropImage.activity(uri)
                .setAspectRatio(16, 9)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start((Activity) getView().getActivityG());
    }

    public void setArtWorkImage(Uri output) {
        selectedArtWork = output;
    }


    public void upload() {
        if (getView().getLectureTitle().trim().isEmpty()) {
            getView().displayError("Please enter lecture title");
        } else if (selectedArtWork == null && getView().getEventModel() == null) {
            getView().displayError("Please select artwork thumbnail.");
        } else if (commaSeparatedIds.trim().isEmpty()) {
            getView().displayError("Please select quick lecture category");
        } else {
            getView().showLoading("Uploading Audio Lecture...");
            Call<QuickLectureModel> quickLectureApiModelCall = getRetrofitInstance().uploadAudioLecture(
                    getFilePart(getView().getAudioFile(), getView().getEventModel() != null ? "event_audio" : "audio"),
                    getView().getEventModel() != null ? getOtherParams("event_audio") : getOtherParams("audio"),
                    getOtherParams(new SharedPrefHelper(getView().getActivityG()).getUserId()),
                    getOtherParams(getView().getLectureTitle()),
                    getOtherParams(discoveryUrl),
                    getOtherParams(
                            getView().getRadioGroup().getCheckedRadioButtonId() == R.id.radioPublic
                                    ? "Public" : "Private")
                    , getOtherParams(getView().getTimeDuration())
                    , getOtherParams(new SharedPrefHelper(getView().getActivityG()).getUserName())
                    , getOtherParams("")
                    , getView().getEventModel() != null ? getOtherParams(getView().getEventModel().getLecture_poster()) : getOtherParams(""),getOtherParams(getView().get_Group_id())
                   // , getView().getEventModel() != null ? getOtherParams(getView().getEventModel().getGroup_id()) : getOtherParams(getView().get_Group_id())
            );
            quickLectureApiModelCall.enqueue(new Callback<QuickLectureModel>() {
                @Override
                public void onResponse(Call<QuickLectureModel> call, final Response<QuickLectureModel> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus()) {
                            if (getView().getEventModel() == null) {
                                Call<ThumbnailResponseModel> lectureThumbnailApiModelCall = getRetrofitInstance().uploadLectureThumbnail(
                                        getFilePart(new File(selectedArtWork.getPath()), "thumbnail"),
                                        getOtherParams(response.body().getmQuickLectureData().getQuick_lecture_id()),
                                        getUploadTypePart());
                                lectureThumbnailApiModelCall.enqueue(new Callback<ThumbnailResponseModel>() {
                                    @Override
                                    public void onResponse(Call<ThumbnailResponseModel> thumbnailApiCall, Response<ThumbnailResponseModel> thumbnailApiResponse) {
                                        getView().hideLoading();

                                        if (thumbnailApiResponse.body() != null) {

                                            if (thumbnailApiResponse.body().getStatus()) {
                                                updateLectureEventInterest(response.body().getmQuickLectureData().getQuick_lecture_id());

                                                UtillsG.deleteThisFile(getView().getAudioFile().getAbsolutePath());
                                                UtillsG.deleteThisFile(selectedArtWork.getPath());
                                            } else {
                                                getView().displayError("Error in creating quick lecture");
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ThumbnailResponseModel> thumbnailApiCall, Throwable t) {
                                        getView().hideLoading();
                                        Log.e("Error", t.toString());
                                        getView().displayError("Error in creating quick lecture");
                                    }
                                });
                            } else {
                                getView().hideLoading();

                                updateLectureEventInterest(response.body().getmQuickLectureData().getQuick_lecture_id());

                                UtillsG.deleteThisFile(getView().getAudioFile().getAbsolutePath());
                                if (selectedArtWork !=null) {
                                    UtillsG.deleteThisFile(selectedArtWork.getPath());
                                }
                            }
                        } else {
                            getView().hideLoading();
                            UtillsG.showToast(response.body().getMessage(), getView().getActivityG(), true);
                        }
                    }
                }

                @Override
                public void onFailure(Call<QuickLectureModel> call, Throwable t) {
                    Log.e("Error", t.toString());
                    getView().hideLoading();
                    getView().displayError("Error in creating quick lecture");
                }
            });
        }

    }

    private void markEventAsCompleted() {
        getView().showLoading("Please wait..");
        Call<BasicApiModel> markEventApi = LVApplication.getRetrofit().create(LectureEventsApi.class)
                .mark_event_completed(getView().getEventModel().getEvent_id());
        markEventApi.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                getView().hideLoading();
                if (response.body() != null && response.body().getStatus()) {
                    if (new SharedPrefHelper(getView().getActivityG()).getUserType().equalsIgnoreCase(UserType.LECTURER)) {
                        UtillsG.showInterstitialAds(getView().getActivityG(),getView().getActivityG().getResources().getString(R.string.userAppId),getView().getActivityG().getResources().getString(R.string.userAdUnitQuickPresent));
                    }
                    else {
                        UtillsG.showInterstitialAds(getView().getActivityG(),getView().getActivityG().getResources().getString(R.string.userAppId),getView().getActivityG().getResources().getString(R.string.nonStudentAdUnitDiscoveryupload));

                    }
                    UtillsG.showToast("Post uploaded successfully", getView().getActivityG(), true);
                    getView().uploadDone();
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                getView().hideLoading();
            }
        });
    }


    public void setSelectedLectureCategories(String Ids) {
        commaSeparatedIds = Ids;
    }

    public void updateLectureEventInterest(final String quickLectureId) {
        mClicks.setLectureInterests((Activity) getView().getActivityG(), LVApplication.getRetrofit().create(EventLectureApi.class), quickLectureId,
                commaSeparatedIds, "lecture", new CallBackG<Boolean>() {
                    @Override
                    public void onCallBack(Boolean output) {
                        if (getView() != null) getView().hideLoading();

                        if (getView().getEventModel() != null) {
                            markEventAsCompleted();
                        } else {
                            UtillsG.showToast("Post uploaded successfully", getView().getActivityG(), true);
                            getView().uploadDone();
                            if (new SharedPrefHelper(getView().getActivityG()).getUserType().equalsIgnoreCase(UserType.LECTURER)) {
                                UtillsG.showInterstitialAds(getView().getActivityG(),getView().getActivityG().getResources().getString(R.string.userAppId),getView().getActivityG().getResources().getString(R.string.userAdUnitQuickPresent));
                            }
                            else {
                                UtillsG.showInterstitialAds(getView().getActivityG(),getView().getActivityG().getResources().getString(R.string.userAppId),getView().getActivityG().getResources().getString(R.string.nonStudentAdUnitDiscoveryupload));

                            }

                        }
                    }
                });
    }

    private MultipartBody.Part getFilePart(final File file, String fileType) {
        ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(fileType, file.getName(), fileBody);
        return filePart;
    }

    private RequestBody getUploadTypePart() {
        return RequestBody.create(MediaType.parse("text/plain"), "upload");
    }


    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }


    private QuickLectureApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(QuickLectureApi.class);
    }

    @Override
    public void onProgressUpdate(int percentage) {
        getView().setprogress(percentage);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {
        getView().uploadDone();
    }
}
