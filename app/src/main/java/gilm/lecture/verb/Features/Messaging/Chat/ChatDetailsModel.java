package gilm.lecture.verb.Features.Messaging.Chat;

import java.io.Serializable;

/**
 * Created by harpreet on 11/2/17.
 */

public class ChatDetailsModel implements Serializable{


    String  is_deleted="";
    String  is_read ="";
    String  chat_id    ="";
    String  sender_id   ="";
    String  receiver_id    ="";
    String  message      ="";
    String  message_type   ="";
    String  group_id   ="";
    String  chat_media  ="";
    String  time     ="";
    String extension="";

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id) {
        this.receiver_id = receiver_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage_type() {
        return message_type;
    }

    public void setMessage_type(String message_type) {
        this.message_type = message_type;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getChat_media() {
        return chat_media;
    }

    public void setChat_media(String chat_media) {
        this.chat_media = chat_media;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
