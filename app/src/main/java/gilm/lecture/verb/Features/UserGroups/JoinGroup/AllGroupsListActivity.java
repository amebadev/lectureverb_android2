package gilm.lecture.verb.Features.UserGroups.JoinGroup;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.UserGroups.GroupList.GroupListAdapter;
import gilm.lecture.verb.Features.UserGroups.GroupListModel;
import gilm.lecture.verb.Features.UserGroups.GroupModel;
import gilm.lecture.verb.Features.UserGroups.UserGroupApis;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.EndlessRecyclerOnScrollListener;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.ActivityAllGroupsListBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllGroupsListActivity extends BaseActivity<ActivityAllGroupsListBinding, AllGroupsListPresenter> implements SwipeRefreshLayout.OnRefreshListener
{

    ArrayList<GroupModel> alData = new ArrayList<>();
    private LinearLayoutManager llm;

    int pageNumber = 1;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;

    public boolean isForResult;

    public static void start(Context context) {
        Intent starter = new Intent(context, AllGroupsListActivity.class);
        context.startActivity(starter);
    }

    public static void startForResult(Fragment fragment) {
        Intent starter = new Intent(fragment.getActivity(), AllGroupsListActivity.class);
        starter.putExtra("isForResult", true);
        ((Fragment) fragment).startActivityForResult(starter, Constants.RequestCode.JOIN_GROUP);
    }

    @Override
    protected void onResume() {
        super.onResume();
        endlessRecyclerOnScrollListener.reset();
        pageNumber = 1;
        getData(pageNumber);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_all_groups_list;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new AllGroupsListPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        setupToolbar("Join User Group Network");

        getDataBinder().swipeRefresh.setOnRefreshListener(this);
        isForResult = getIntent().getBooleanExtra("isForResult", false);

        llm = new LinearLayoutManager(getActivityG(), LinearLayoutManager.VERTICAL, false);
        getDataBinder().recyclerView.setLayoutManager(llm);
        getDataBinder().recyclerView.setAdapter(new GroupListAdapter(alData, getActivityG(),null));

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(llm)
        {
            @Override
            public void onLoadMore(int current_page) {
                pageNumber++;
                Log.e("PAGENUMBER ===== ", String.valueOf(pageNumber));
                getData(pageNumber);
            }

            @Override
            public void countChange(int currentVisibleCount) {

            }
        };

        getDataBinder().recyclerView.setOnScrollListener(endlessRecyclerOnScrollListener);
        getDataBinder().edSearch.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    endlessRecyclerOnScrollListener.reset();
                    pageNumber = 1;
                    searchGroup(pageNumber);
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public Context getActivityG() {
        return AllGroupsListActivity.this;
    }

    private void getAllGroups(final int pageNumber) {
        if (pageNumber == 1) {
            getDataBinder().progressBar.setVisibility(View.VISIBLE);
        }

        Call<GroupListModel> call = LVApplication.getRetrofit().create(UserGroupApis.class).get_all_user_groups(new SharedPrefHelper(getActivityG()).getUserId(), String.valueOf(pageNumber));
        call.enqueue(new Callback<GroupListModel>()
        {
            @Override
            public void onResponse(Call<GroupListModel> call, Response<GroupListModel> response) {
                getDataBinder().swipeRefresh.setRefreshing(false);
                getDataBinder().progressBar.setVisibility(View.GONE);

                if (response.body().getStatus()) {
                    Log.e("RESPONSE === ", response.body().getData().toString());
                    if (pageNumber == 1) {
                        alData.clear();
                    }
                    alData.addAll(response.body().getData());
                    getDataBinder().recyclerView.getAdapter().notifyDataSetChanged();
                }
                else {
                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                }

                showNoDataIfEmpty();
            }

            @Override
            public void onFailure(Call<GroupListModel> call, Throwable t) {
                getDataBinder().progressBar.setVisibility(View.GONE);
                Log.e("ERROR GET GROUP LIST", t.toString() + "");
            }
        });
    }

    private void showNoDataIfEmpty() {
        if (alData.isEmpty()) {
            getDataBinder().txtvNoData.setVisibility(View.VISIBLE);
            getDataBinder().progressBar.setVisibility(View.GONE);
            getDataBinder().recyclerView.setVisibility(View.GONE);
        }
        else {
            getDataBinder().txtvNoData.setVisibility(View.GONE);
            getDataBinder().progressBar.setVisibility(View.GONE);
            getDataBinder().recyclerView.setVisibility(View.VISIBLE);
        }

        UtillsG.hideKeyboard(getActivityG(), getDataBinder().edSearch);
    }

    private void searchGroup(final int pageNumber) {
        if (pageNumber == 1) {
            getDataBinder().progressBar.setVisibility(View.VISIBLE);
        }

        Call<GroupListModel> call = LVApplication.getRetrofit().create(UserGroupApis.class).search_all_user_groups(new SharedPrefHelper(getActivityG()).getUserId(), getDataBinder().edSearch.getText().toString().trim(), String.valueOf(pageNumber));
        call.enqueue(new Callback<GroupListModel>()
        {
            @Override
            public void onResponse(Call<GroupListModel> call, Response<GroupListModel> response) {
                getDataBinder().swipeRefresh.setRefreshing(false);
                getDataBinder().progressBar.setVisibility(View.GONE);

                if (response.body().getStatus()) {
                    Log.e("RESPONSE === ", response.body().getData().toString());
                    if (pageNumber == 1) {
                        alData.clear();
                    }
                    alData.addAll(response.body().getData());
                    getDataBinder().recyclerView.getAdapter().notifyDataSetChanged();
                }
                else {
                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                }

                showNoDataIfEmpty();
            }

            @Override
            public void onFailure(Call<GroupListModel> call, Throwable t) {
                getDataBinder().progressBar.setVisibility(View.GONE);
                Log.e("ERROR GET GROUP LIST", t.toString() + "");
            }
        });
    }

    @Override
    public void onRefresh() {
        UtillsG.hideKeyboard(getActivityG(), getDataBinder().edSearch);
        getDataBinder().edSearch.setText("");
        endlessRecyclerOnScrollListener.reset();
        pageNumber = 1;
        getData(pageNumber);
    }

    private void getData(int pageNumber) {
        if (!getDataBinder().edSearch.getText().toString().isEmpty()) {
            searchGroup(pageNumber);
        }
        else {
            getAllGroups(pageNumber);
        }
    }
}
