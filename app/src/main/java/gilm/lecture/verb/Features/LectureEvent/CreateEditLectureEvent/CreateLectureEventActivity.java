package gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;

import gilm.lecture.verb.Features.InterestsSelection.AddInterests.AddInterestCategoriesActivity;
import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.AddLecCoHostActivity;
import gilm.lecture.verb.Features.UserGroups.GroupList.GroupListAdapter;
import gilm.lecture.verb.Features.UserGroups.GroupListModel;
import gilm.lecture.verb.Features.UserGroups.GroupModel;
import gilm.lecture.verb.Features.UserGroups.UserGroupApis;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.Widget.PKTagsTextView;
import gilm.lecture.verb.databinding.ActivityCreateLectureEventBinding;
import mabbas007.tagsedittext.TagsEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateLectureEventActivity extends BaseActivity<ActivityCreateLectureEventBinding, CreateLectureEventPresenter> implements CreateLectureEventView, View.OnClickListener, PKTagsTextView.SuggestionsCallBack {

    TextView txtv_selectCohost, textv_lectureSelection, txtv_lectureCategory,txtvCanInvite;
    public String commaSeparatedIds = "", commaSeparatedNames = "";
    public String coHostId = "";
    private TextView txtvSelectLecturer;
    private String lecturerId = "";
    public String selectedGroupId = "0";

    public static void start(Context context) {
        Intent starter = new Intent(context, CreateLectureEventActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_create_lecture_event;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new CreateLectureEventPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public String getSelectedGroupId() {
        return selectedGroupId;
    }

    @Override
    public void initViews() {
        setupToolbar("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        txtvCanInvite = (TextView) findViewById(R.id.txtvCanInvite);
        (txtv_selectCohost = ((TextView) findViewById(R.id.txtv_selectCohost))).setOnClickListener(this);
        (txtvSelectLecturer = ((TextView) findViewById(R.id.txtvSelectLecturer))).setOnClickListener(this);
        (textv_lectureSelection = ((TextView) findViewById(R.id.textv_lectureSelection))).setOnClickListener(this);
        txtv_lectureCategory = (TextView) findViewById(R.id.txtv_lectureCategory);

        getDataBinder().setData(getPresenter().getDataViewModel());
        getDataBinder().setPresenter(getPresenter());

        getDataBinder().tagsEdittext.setTagsListener(new TagsEditText.TagsEditListener() {
            @Override
            public void onTagsChanged(Collection<String> collection) {
                getPresenter().getDataViewModel().setLectureTags(collection);
            }

            @Override
            public void onEditingFinished() {

            }
        });
        ((PKTagsTextView) findViewById(R.id.tagsEdittext)).startSuggestions(this, this);

        txtv_lectureCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                InterestsSelectionActivity.startfOResult(getActivityG(),"Select Lecture Category",txtv_lectureCategory.getText().toString());
                AddInterestCategoriesActivity.start(getActivityG(), true, commaSeparatedIds, commaSeparatedNames);
            }
        });

//        txtvCanInvite.setText((new SharedPrefHelper(getActivityG()).getUserType().equalsIgnoreCase(UserType.LECTURER)) ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            showExitMessage();
            return true;
        } else if (item.getItemId() == R.id.create) {
            getPresenter().createEvent();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 101) {
                getPresenter().locationPickerResult(data);
            } else if (requestCode == Constants.RequestCode.CoHostRequestCode) {
                String user_id = data.getStringExtra(Constants.Extras.user_id);
                String name = data.getStringExtra(Constants.Extras.name);

                coHostId = user_id;

                txtv_selectCohost.setText("Facilitator : " + name);
//                getPresenter().setSelectedCoHost((CoHostListModel.SingleCoHostData) data.getSerializableExtra("data"));
//                txtv_selectCohost.setText("Co Host Selected");
            } else if (requestCode == Constants.RequestCode.LecturerSelectionRequestCode) {
                String user_id = data.getStringExtra(Constants.Extras.user_id);
                String name = data.getStringExtra(Constants.Extras.name);

                lecturerId = user_id;

                txtvSelectLecturer.setText("Lecturer : " + name);
            } else if (requestCode == Constants.RequestCode.INTEREST_SELECTION) {

                commaSeparatedIds = "";
                commaSeparatedNames = "";
                ArrayList<ChildCategoryModel> alCategories = (ArrayList<ChildCategoryModel>) data.getSerializableExtra(Constants.Extras.DATA);
                for (int i = 0; i < alCategories.size(); i++) {
                    commaSeparatedIds = commaSeparatedIds + (commaSeparatedIds.isEmpty() ? ""
                            : ",") + alCategories.get(i).getId();
                    commaSeparatedNames = commaSeparatedNames + (
                            commaSeparatedNames.isEmpty() ? ""
                                    : ", ") + alCategories.get(i).getCategory();
                }
                getPresenter().setSelectedLectureCategories(alCategories, commaSeparatedIds);
                txtv_lectureCategory.setText(commaSeparatedNames);
            } else {
                getPresenter().onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public Context getActivityG() {
        return CreateLectureEventActivity.this;
    }

    @Override
    public void onBackPressed() {
        showExitMessage();
    }

    protected void showExitMessage() {
        DialogHelper.getInstance().showWith2Action(getActivityG(), "Leave Setup", "Keep Editing", "Are you sure ?", "Your lecture event won't be saved.", new CallBackG<String>() {
            @Override
            public void onCallBack(String output) {
                finish();
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.txtv_selectCohost:
                AddLecCoHostActivity.start(getActivityG(), lecturerId);
                break;
            case R.id.txtvSelectLecturer:
                AddLecCoHostActivity.startForLecturer(getActivityG(), coHostId);
                break;
            case R.id.textv_lectureSelection:
                selectLectureEventType();
                break;
        }

    }

    @Override
    public String getSelectedCoHostId() {
        return coHostId;
    }

    @Override
    public String getSelectedLecturerId() {
        return lecturerId;
    }

    void selectLectureEventType() {
        final Dialog dialog = new Dialog(getActivityG());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_select_privacy);

        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.TOP);
        dialog.show();


        final RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivityG(), LinearLayoutManager.VERTICAL, false));
        final LinearLayout ll_privateLectureEvent = (LinearLayout) dialog.findViewById(R.id.ll_privateLectureEvent);
        final LinearLayout ll_publicLectureEvent = (LinearLayout) dialog.findViewById(R.id.ll_publicLectureEvent);

        showLoading("Please wait..");
        Call<GroupListModel> call = LVApplication.getRetrofit().create(UserGroupApis.class).get_my_user_groups(getLocalData().getUserId(),"0");
        call.enqueue(new Callback<GroupListModel>() {
            @Override
            public void onResponse(Call<GroupListModel> call, Response<GroupListModel> response) {
                hideLoading();
                if (response.body().getStatus()) {
                    ArrayList<GroupModel> aldata;
                    aldata = response.body().getData();
//                    Collections.reverse(aldata);
                    recyclerView.setAdapter(new GroupListAdapter(aldata, getActivityG(), new CallBackG<GroupModel>() {
                        @Override
                        public void onCallBack(GroupModel output) {
//                            UtillsG.showToast(output.getGroup_title(), getActivityG(), true);

                            selectedGroupId = output.getId();

                            txtvCanInvite.setText("Students can invite guest");

                            textv_lectureSelection.setText("User Group : " + output.getGroup_title());

                            dialog.dismiss();
                        }
                    }));
                } else {
                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                }
            }

            @Override
            public void onFailure(Call<GroupListModel> call, Throwable t) {
                hideLoading();
                Log.e("ERROR GET GROUP LIST", t.toString() + "");
            }
        });

        ll_privateLectureEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textv_lectureSelection.setText("Private Lecture Event");
                getPresenter().getDataViewModel().setLectureEventIsPublic(false);
                dialog.dismiss();
            }
        });

        ll_publicLectureEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textv_lectureSelection.setText("Public Lecture Event");
                getPresenter().getDataViewModel().setLectureEventIsPublic(true);
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onTagInserted(PKTagsTextView.TagsModel tagsModel) {

    }
}
