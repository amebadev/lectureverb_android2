package gilm.lecture.verb.Features.Messaging.RecentChat;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * created by PARAMBIR SINGH on 2/11/17.
 */

public class ListOfRecentChatsModel extends BasicApiModel implements Serializable
{
    @SerializedName("chat")
    List<RecentChatsModel> chat;

    public List<RecentChatsModel> getChat() {
        return chat;
    }

    public void setChat(List<RecentChatsModel> chat) {
        this.chat = chat;
    }

    public class RecentChatsModel implements Serializable
    {
        String user_id = "";
        String chat_id = "";
        String sender_id = "";
        String receiver_id = "";
        String message = "";
        String message_type = "";
        String chat_media = "";
        String extension = "";
        String is_read = "";
        String is_deleted = "";
        String time = "";
        String unread_count = "";

        UserDataModel.UserData UserData;

        public String getUnread_count() {
            return unread_count;
        }

        public void setUnread_count(String unread_count) {
            this.unread_count = unread_count;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getChat_id() {
            return chat_id;
        }

        public void setChat_id(String chat_id) {
            this.chat_id = chat_id;
        }

        public String getSender_id() {
            return sender_id;
        }

        public void setSender_id(String sender_id) {
            this.sender_id = sender_id;
        }

        public String getReceiver_id() {
            return receiver_id;
        }

        public void setReceiver_id(String receiver_id) {
            this.receiver_id = receiver_id;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage_type() {
            return message_type;
        }

        public void setMessage_type(String message_type) {
            this.message_type = message_type;
        }

        public String getChat_media() {
            return chat_media;
        }

        public void setChat_media(String chat_media) {
            this.chat_media = chat_media;
        }

        public String getExtension() {
            return extension;
        }

        public void setExtension(String extension) {
            this.extension = extension;
        }

        public String getIs_read() {
            return is_read;
        }

        public void setIs_read(String is_read) {
            this.is_read = is_read;
        }

        public String getIs_deleted() {
            return is_deleted;
        }

        public void setIs_deleted(String is_deleted) {
            this.is_deleted = is_deleted;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public UserDataModel.UserData getUserData() {
            return UserData;
        }

        public void setUserData(UserDataModel.UserData userData) {
            UserData = userData;
        }
    }
}
