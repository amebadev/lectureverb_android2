package gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments;

import java.io.Serializable;

import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.CoHostListModel;

/**
 * Created by harpreet on 10/17/17.
 */

public class CommentsModel implements Serializable
{


    String comment_id = "comment_id";
    String user_id = "user_id";
    String quick_lecture_id = "quick_lecture_id";
    String comment = "comment";
    String comment_type = "comment_type";
    String status = "status";
    String date_time = "date_time";
    String reply_userid = "reply_userid";
    String reply_comment = "reply_comment";
    String reply_date_time = "reply_date_time";
    String reply_comments_count = "reply_comments_count";
    String comment_file = "comment_file";
    String file_extension = "file_extension";

    CoHostListModel.SingleCoHostData UserData;

    public String getFile_extension() {
        return file_extension;
    }

    public void setFile_extension(String file_extension) {
        this.file_extension = file_extension;
    }

    public String getComment_file() {
        return comment_file;
    }

    public void setComment_file(String comment_file) {
        this.comment_file = comment_file;
    }

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getQuick_lecture_id() {
        return quick_lecture_id;
    }

    public void setQuick_lecture_id(String quick_lecture_id) {
        this.quick_lecture_id = quick_lecture_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment_type() {
        return comment_type;
    }

    public void setComment_type(String comment_type) {
        this.comment_type = comment_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getReply_userid() {
        return reply_userid;
    }

    public void setReply_userid(String reply_userid) {
        this.reply_userid = reply_userid;
    }

    public String getReply_comment() {
        return reply_comment;
    }

    public void setReply_comment(String reply_comment) {
        this.reply_comment = reply_comment;
    }

    public String getReply_date_time() {
        return reply_date_time;
    }

    public void setReply_date_time(String reply_date_time) {
        this.reply_date_time = reply_date_time;
    }

    public String getReply_comments_count() {
        return reply_comments_count;
    }

    public void setReply_comments_count(String reply_comments_count) {
        this.reply_comments_count = reply_comments_count;
    }

    public CoHostListModel.SingleCoHostData getUserData() {
        return UserData;
    }

    public void setUserData(CoHostListModel.SingleCoHostData userData) {
        UserData = userData;
    }


}
