package gilm.lecture.verb.Features.LoginRegisteration.Login;

import android.databinding.ObservableField;
import android.view.View;

/**
 * Created by G-Expo on 12 Jul 2017.
 */

public interface LoginPresenterBinder
{
    /**
     * Login button is clicked,check for all fields.(validation)
     * and send data to server for authentication.
     */
    void LoginClicked(View view);

    /**
     * google plus login,after successful login send data to sever.
     */
    void GplusLoginClicked(View view);

    /**
     * FB login,after successful login send data to sever.
     */
    void FacebookLoginClicked(View view);

    /**
     * Twitter login,after successful login send data to sever.
     */
    void TwitterLoginClicked(View view);

    /**
     * bind with password edit text.
     */
    ObservableField<String> getPassword();

    /**
     * bind with user name edit text.
     */
    ObservableField<String> getUserName();
}
