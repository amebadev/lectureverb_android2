/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.LoginRegisteration;

import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.Web;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by G-Expo on 09 Aug 2017.
 */

/**
 * login , registration , email verification and forgot password related API's.
 */
public interface LoginRegisterApi {
    @FormUrlEncoded
    @POST(Web.Path.RESEND_VERIFICATION)
    Observable<BasicApiModel> resendVerification(@Field(Web.Keys.EMAIL) String email);

    @FormUrlEncoded
    @POST(Web.Path.FORGOT_PASSWORD)
    Observable<BasicApiModel> forgotPassword(@Field(Web.Keys.EMAIL) String email);

    @FormUrlEncoded
    @POST(Web.Path.MANUAL_LOGIN)
    Observable<UserDataModel> manualLogin(@Field(Web.Keys.EMAIL) String email, @Field(Web.Keys.PASSWORD) String password);

    @FormUrlEncoded
    @POST(Web.Path.SOCIAL_LOGIN)
    Observable<UserDataModel> socialLogin(@Field(Web.Keys.FULL_NAME) String name
            , @Field(Web.Keys.EMAIL) String email
            , @Field(Web.Keys.REGISTER_VIA) String register_via
            , @Field(Web.Keys.SOCIAL_ID) String socialId
            , @Field(Web.Keys.DEVICE_TYPE) String device_type
            , @Field(Web.Keys.ROLE_ID) String roleId);

    @FormUrlEncoded
    @POST(Web.Path.SIGNUP)
    Observable<UserDataModel> register(@Field(Web.Keys.FULL_NAME) String name
            , @Field(Web.Keys.EMAIL) String email
            , @Field(Web.Keys.DEVICE_TYPE) String device_type
            , @Field(Web.Keys.ROLE_ID) String roleId
            , @Field(Web.Keys.PASSWORD) String password);

    @FormUrlEncoded
    @POST(Web.Path.EMAIL_VERIFICATION)
    Observable<BasicApiModel> emailVerification(@Field(Web.Keys.USER_ID) String userId
            , @Field(Web.Keys.EMAIL) String email
            , @Field(Web.Keys.VERIFICATION_CODE) String code);


    @Multipart
    @POST(Web.Path.create_group)
    Call<BasicApiModel> create_group(
            @Part MultipartBody.Part group_image
            , @Part(Web.Keys.USER_ID) RequestBody user_id
            , @Part(Web.Keys.group_title) RequestBody group_title
            , @Part(Web.Keys.group_type) RequestBody group_type
            , @Part(Web.Keys.upload_type) RequestBody upload_type
            , @Part(Web.Keys.group_description) RequestBody group_description
    );
    @Multipart
    @POST(Web.Path.updateGroup)
    Call<BasicApiModel> update_group(
            @Part MultipartBody.Part group_image
            , @Part(Web.Keys.group_id) RequestBody group_id
            , @Part(Web.Keys.group_title) RequestBody group_title
            , @Part(Web.Keys.group_description) RequestBody group_description
            , @Part(Web.Keys.upload_type) RequestBody upload_type
    );

    @FormUrlEncoded
    @POST(Web.Path.logout)
    Call<BasicApiModel> logout(@Field(Web.Keys.USER_ID) String user_id);

    @FormUrlEncoded
    @POST(Web.Path.delete_user_account)
    Call<BasicApiModel> delete_user_account(@Field(Web.Keys.USER_ID) String user_id);

}
