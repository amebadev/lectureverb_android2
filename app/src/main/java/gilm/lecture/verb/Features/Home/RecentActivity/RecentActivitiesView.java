package gilm.lecture.verb.Features.Home.RecentActivity;

import java.util.List;

import gilm.lecture.verb.Features.Home.Notification.NotificationViewModel;
import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 17 Jul 2017.
 */

public interface RecentActivitiesView extends Viewable<RecentActivitiesPresenter>
{
    void showData(List<NotificationViewModel> list);
}
