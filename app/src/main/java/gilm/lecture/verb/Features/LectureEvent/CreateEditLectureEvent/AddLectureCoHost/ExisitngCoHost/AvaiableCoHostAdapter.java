package gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.ExisitngCoHost;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.AddLecCoHostActivity;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.CoHostListModel;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.CreateLectureEventActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.InfiniteAdapter_WithoutBuinding;
import gilm.lecture.verb.UtilsG.LoadingViewHolder;
import gilm.lecture.verb.UtilsG.UtillsG;

/**
 * Created by harpreet on 9/7/17.
 */

public class AvaiableCoHostAdapter extends InfiniteAdapter_WithoutBuinding<AvaiableCoHostAdapter.MyViewHolderG>
{
    private LayoutInflater inflater;

    Activity mActivity;
    ArrayList<CoHostListModel.SingleCoHostData> mCoHostList;
    ValueFilter valueFilter;

    public AvaiableCoHostAdapter(Activity mActivity, ArrayList<CoHostListModel.SingleCoHostData> mCoHostList) {
        this.mActivity = mActivity;
        inflater = LayoutInflater.from(mActivity);
        this.mCoHostList = mCoHostList;
    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        return new LoadingViewHolder(inflater.inflate(R.layout.layout_progress, parent, false));
    }

    @Override
    public int getCount() {
        return mCoHostList.size();
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }


    @Override
    public AvaiableCoHostAdapter.MyViewHolderG onCreateView(ViewGroup parent, int viewType) {
        return new AvaiableCoHostAdapter.MyViewHolderG(inflater.inflate(R.layout.inflater_co_host_row, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        try {
            if (!(holder instanceof LoadingViewHolder)) {
                final int requestCode = ((AddLecCoHostActivity) mActivity).requestCode;
                final String coHostId = ((AddLecCoHostActivity) mActivity).coHostId;
                final String lecturerId = ((AddLecCoHostActivity) mActivity).lecturerId;


                MyViewHolderG mHolder = ((MyViewHolderG) holder);
                final CoHostListModel.SingleCoHostData msingleCoHostData = mCoHostList.get(position);

                mHolder.txtv_userName.setText(msingleCoHostData.getFull_name());
                mHolder.txtv_lecVerbUserId.setText("(Lecture Verb user : " + msingleCoHostData.getUser_id() + ")");
                mHolder.txtv_deviceName.setText(msingleCoHostData.getDevice_name());
                mHolder.txtv_macName.setText(msingleCoHostData.getMac_address());
                mHolder.ratingBarVal.setRating(5);
                ImageLoader.setImageBig(mHolder.imgv_coHostPic, msingleCoHostData.getProfile_pic());


                ((MyViewHolderG) holder).view.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view) {
                        if (requestCode == Constants.RequestCode.LecturerSelectionRequestCode) {
                            if (!coHostId.equalsIgnoreCase(msingleCoHostData.getUser_id())) {
                                SingleCoHostDetails.start(mActivity, msingleCoHostData);
                            }
                            else {
                                UtillsG.showToast(msingleCoHostData.getFull_name() + " is already appointed.", mActivity, true);
                            }
                        }
                        else {
                            if (!lecturerId.equalsIgnoreCase(msingleCoHostData.getUser_id())) {
                                SingleCoHostDetails.start(mActivity, msingleCoHostData);
                            }
                            else {
                                UtillsG.showToast(msingleCoHostData.getFull_name() + " is already appointed.", mActivity, true);
                            }
                        }

                    }
                });

                ((MyViewHolderG) holder).view.setOnLongClickListener(new View.OnLongClickListener()
                {
                    @Override
                    public boolean onLongClick(View view) {
                        DialogHelper.getInstance().showWith2Action(mActivity, "Add As CoHost", "Cancel", "Are you sure ?", "Your lecture event won't be saved.", new CallBackG<String>()
                        {
                            @Override
                            public void onCallBack(String output) {
                                if (requestCode == Constants.RequestCode.LecturerSelectionRequestCode) {
                                    if (!coHostId.equalsIgnoreCase(msingleCoHostData.getUser_id())) {
                                        Intent in = new Intent(mActivity, CreateLectureEventActivity.class);
                                        in.putExtra("data", msingleCoHostData);
                                        mActivity.setResult(Activity.RESULT_OK, in);
                                        mActivity.finish();
                                    }
                                    else {
                                        UtillsG.showToast(msingleCoHostData.getFull_name() + " is already appointed.", mActivity, true);
                                    }
                                }
                                else {
                                    if (!lecturerId.equalsIgnoreCase(msingleCoHostData.getUser_id())) {
                                        Intent in = new Intent(mActivity, CreateLectureEventActivity.class);
                                        in.putExtra("data", msingleCoHostData);
                                        mActivity.setResult(Activity.RESULT_OK, in);
                                        mActivity.finish();
                                    }
                                    else {
                                        UtillsG.showToast(msingleCoHostData.getFull_name() + " is already appointed.", mActivity, true);
                                    }
                                }
                            }
                        });

                        return false;
                    }
                });
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    class MyViewHolderG extends RecyclerView.ViewHolder
    {
        View view;
        TextView txtv_userName, txtv_lecVerbUserId, txtv_deviceName, txtv_macName;
        RatingBar ratingBarVal;
        ImageView imgv_wifi, imgv_microphone, imgv_coHostPic;


        public MyViewHolderG(View row) {
            super(row);

            view = row;
            txtv_userName = (TextView) view.findViewById(R.id.txtv_userName);
            txtv_lecVerbUserId = (TextView) view.findViewById(R.id.txtv_lecVerbUserId);
            txtv_deviceName = (TextView) view.findViewById(R.id.txtv_deviceName);
            txtv_macName = (TextView) view.findViewById(R.id.txtv_macName);
            ratingBarVal = (RatingBar) view.findViewById(R.id.ratingBarVal);
            imgv_wifi = (ImageView) view.findViewById(R.id.imgv_wifi);
            imgv_microphone = (ImageView) view.findViewById(R.id.imgv_microphone);
            imgv_coHostPic = (ImageView) view.findViewById(R.id.imgv_coHostPic);

        }
    }


    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter
    {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<CoHostListModel.SingleCoHostData> filterList = new ArrayList();
                for (int i = 0; i < ((AddLecCoHostActivity) mActivity).getcoHostList().size(); i++) {
                    if ((((AddLecCoHostActivity) mActivity).getcoHostList().get(i).getFull_name().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(((AddLecCoHostActivity) mActivity).getcoHostList().get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            }
            else {
                results.count = ((AddLecCoHostActivity) mActivity).getcoHostList().size();
                results.values = ((AddLecCoHostActivity) mActivity).getcoHostList();
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            mCoHostList = (ArrayList<CoHostListModel.SingleCoHostData>) results.values;
            notifyDataSetChanged();
        }

    }


}

