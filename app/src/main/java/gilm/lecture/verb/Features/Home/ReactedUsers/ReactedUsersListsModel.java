package gilm.lecture.verb.Features.Home.ReactedUsers;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;

public class ReactedUsersListsModel implements Serializable {
    @SerializedName("comments")
    List<UserDataModel.UserData> comments;
    @SerializedName("likes")
    List<UserDataModel.UserData> likes;
    @SerializedName("reposts")
    List<UserDataModel.UserData> reposts;
    @SerializedName("viewedBy")
    List<UserDataModel.UserData> viewedBy;

    public List<UserDataModel.UserData> getComments() {
        return comments;
    }

    public void setComments(List<UserDataModel.UserData> comments) {
        this.comments = comments;
    }

    public List<UserDataModel.UserData> getLikes() {
        return likes;
    }

    public void setLikes(List<UserDataModel.UserData> likes) {
        this.likes = likes;
    }

    public List<UserDataModel.UserData> getReposts() {
        return reposts;
    }

    public void setReposts(List<UserDataModel.UserData> reposts) {
        this.reposts = reposts;
    }

    public List<UserDataModel.UserData> getViewedBy() {
        return viewedBy;
    }

    public void setViewedBy(List<UserDataModel.UserData> viewedBy) {
        this.viewedBy = viewedBy;
    }
}