/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Messaging.Chat;

import android.databinding.Observable;
import android.databinding.ObservableField;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.Messaging.ChatApis;
import gilm.lecture.verb.UtilsG.DateHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 08 Mar 2017.
 */

public class ChatPresenter extends BasePresenter<ChatView> implements ChatBinder
{
    private List<MsgDataModel> listData;

    public ObservableField<String> textMessage =
            new ObservableField<>();
    public ObservableField<String> imageUrl =
            new ObservableField<>();
    public ObservableField<String> name =
            new ObservableField<>();

    public List<ChatDetailsModel> alMessages = new ArrayList<>();

    public ChatPresenter() {
        listData = new ArrayList<>();
        textMessage.set("");
        name.set("Name");
        imageUrl.set("https://students.ucsd.edu/_images/student-life/involvement/getinvolved/csi-staff-contacts/Casey.jpg");


        textMessage.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback()
        {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                getView().flipButtons(textMessage.get().isEmpty());
            }
        });
    }

    /**
     * @return Messages list.
     */
    public List<MsgDataModel> getChatList() {
        return listData;
    }

    @Override
    public void sendMessage(View view) {
        if (textMessage.get().isEmpty()) {
            return;
        }
        //sending messages to server
        String id = "0";
        String sender_userid = "1";//static user id for now.
        String recipient_userid = "2";
        String message = textMessage.get();
        String created_at = DateHelper.getInstance().getCurrentDateForChat();

        MsgDataModel msgDataModel = new MsgDataModel();

        msgDataModel.setChat_id(id);
        msgDataModel.setSender_id(sender_userid);
        msgDataModel.setReceiver_id(recipient_userid);
        msgDataModel.setMessage(message);
        msgDataModel.setDate(created_at);

        listData.add(msgDataModel);
        getView().updateMyMessage(listData);

        textMessage.set("");
    }

    @Override
    public ObservableField<String> textMessage() {
        return textMessage;
    }

    @Override
    public ObservableField<String> imageUrl() {
        return imageUrl;
    }

    @Override
    public ObservableField<String> name() {
        return name;
    }

    public void getInitialChatList(int pageNumber) {


        Call<ChatsListModel> chatList = LVApplication.getRetrofit().create(ChatApis.class).get_chat_messages(new SharedPrefHelper(getView().getActivityG()).getUserId(), getView().getTargetUserId(), String.valueOf(pageNumber));
        chatList.enqueue(new Callback<ChatsListModel>()
        {
            @Override
            public void onResponse(Call<ChatsListModel> call, Response<ChatsListModel> response) {
                if(getView()!=null ) {
                    if (response.body() != null && response.body().getStatus() && response.body().getmChatList() != null) {
                        alMessages.clear();
                        alMessages.addAll(response.body().getmChatList());
                    }
                    getView().notifyDataUpdated();
                }
            }

            @Override
            public void onFailure(Call<ChatsListModel> call, Throwable t) {
                if(getView()!=null)  UtillsG.showToast(t.getMessage(), getView().getActivityG(), true);
            }
        });
    }

    public void loadMoreData(int page) {
        Call<ChatsListModel> chatList = LVApplication.getRetrofit().create(ChatApis.class).get_chat_messages(new SharedPrefHelper(getView().getActivityG()).getUserId(), getView().getTargetUserId(), String.valueOf(page));
        chatList.enqueue(new Callback<ChatsListModel>()
        {
            @Override
            public void onResponse(Call<ChatsListModel> call, Response<ChatsListModel> response) {
                if(getView()!=null ) {
                    if (response.body() != null && response.body().getStatus()  && response.body().getmChatList() != null) {
                        alMessages.addAll(response.body().getmChatList());
                    }
                    getView().notifyDataUpdated();
                }
            }

            @Override
            public void onFailure(Call<ChatsListModel> call, Throwable t) {
               if(getView()!=null) UtillsG.showToast(t.getMessage(), getView().getActivityG(), true);
            }
        });
    }
}
