package gilm.lecture.verb.Features.Home.Home;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.Widget.PKTagsTextView;

/**
 * created by PARAMBIR SINGH on 5/9/17.
 */

public class ArrayListTagsModel extends BasicApiModel
{
    @SerializedName("data")
    List<PKTagsTextView.TagsModel> listOfLectureModels;

    public List<PKTagsTextView.TagsModel> getListOfLectureModels() {
        return listOfLectureModels;
    }

    public void setListOfLectureModels(List<PKTagsTextView.TagsModel> listOfLectureModels) {
        this.listOfLectureModels = listOfLectureModels;
    }
}
