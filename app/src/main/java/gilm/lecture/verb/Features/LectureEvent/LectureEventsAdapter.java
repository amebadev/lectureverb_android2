package gilm.lecture.verb.Features.LectureEvent;

import android.content.Context;
import android.view.View;

import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.databinding.InflatorLectureEventBinding;

/**
 * Created by G-Expo on 07 Jul 2017.
 */

public class LectureEventsAdapter extends InfiniteAdapterG<InflatorLectureEventBinding>
{
    private List<LectureEventViewModel> dataList;

    private LectureEventPresenterBinder presenter;
    private Context context;

    public LectureEventsAdapter(List<LectureEventViewModel> dataList, LectureEventPresenterBinder presenter, Context context)
    {
        this.dataList = dataList;
        this.presenter = presenter;
        this.context = context;
    }

    @Override
    protected void bindData(int position, MyViewHolderG myViewHolderG)
    {
        myViewHolderG.binding.setData(dataList.get(position));
        myViewHolderG.binding.setPresenter(presenter);
        myViewHolderG.binding.executePendingBindings();

        boolean iAmOwner = dataList.get(position).getmLectureEvent().getUserdata().getUser_id().equals(new SharedPrefHelper(context).getUserId());
        boolean iAmLecturer = dataList.get(position).getmLectureEvent().getLecturer_data() != null
                && dataList.get(position).getmLectureEvent().getLecturer_data().getUser_id() != null
                && dataList.get(position).getmLectureEvent().getLecturer_data().getUser_id().equalsIgnoreCase(new SharedPrefHelper(context).getUserId());
        boolean iAmCoHost = dataList.get(position).getmLectureEvent().getLecturer_co_hosts() != null
                && dataList.get(position).getmLectureEvent().getLecturer_co_hosts().get(0).getUser_id() != null
                && dataList.get(position).getmLectureEvent().getLecturer_co_hosts().get(0).getUser_id().equals(new SharedPrefHelper(context).getUserId());

        myViewHolderG.binding.txtvGoingDigital.setVisibility(iAmOwner ? View.GONE : View.VISIBLE);
        myViewHolderG.binding.txtvInterested.setVisibility(iAmOwner ? View.GONE : View.VISIBLE);
        myViewHolderG.binding.vGoingDigital.setVisibility(iAmOwner ? View.GONE : View.VISIBLE);
        myViewHolderG.binding.vInterested.setVisibility(iAmOwner ? View.GONE : View.VISIBLE);

        myViewHolderG.binding.txtvShare.setVisibility(
                dataList.get(position).getmLectureEvent().getCan_invite_friends().equals("true")
                        ? View.VISIBLE
                        : View.GONE);
        myViewHolderG.binding.vGoingDigital.setVisibility((
                dataList.get(position).getmLectureEvent().getCan_invite_friends().equals("true") && !iAmOwner)
                ? View.VISIBLE
                : View.GONE);

        myViewHolderG.binding.txtvLocation.setVisibility(UtillsG.getNotNullString(dataList.get(position).getLocation(), "").isEmpty() ? View.GONE : View.VISIBLE);

        // IF THE EVENT IS COMPLETED THEN THE TEXT COLORS AND LINEARLAYOUT COLOR WILL BE GREY, SO THAT IT CAN BE INDICATED AS DISABLED..
        if (dataList.get(position).getmLectureEvent().getIs_completed() == 1)
        {
            myViewHolderG.binding.progressBar.setVisibility(View.GONE);
            myViewHolderG.binding.imgvEventImage.setAlpha(0.5f);
            myViewHolderG.binding.llMain.setBackgroundColor(context.getResources().getColor(R.color.color_ececec));
            myViewHolderG.binding.txtvStartTime.setBackgroundColor(context.getResources().getColor(R.color.greyLight));
            myViewHolderG.binding.txtvStartTime.setTextColor(context.getResources().getColor(R.color.color_dadada));
            myViewHolderG.binding.txtvTitle.setTextColor(context.getResources().getColor(R.color.greyLight));
            myViewHolderG.binding.txtvLocation.setTextColor(context.getResources().getColor(R.color.greyLight));

            myViewHolderG.binding.layoutBottomButtons.setVisibility(View.GONE);
        } else
        {
            myViewHolderG.binding.progressBar.setVisibility(View.VISIBLE);
            myViewHolderG.binding.imgvEventImage.setAlpha(1f);
            myViewHolderG.binding.llMain.setBackgroundColor(context.getResources().getColor(R.color.white));
            myViewHolderG.binding.txtvStartTime.setBackgroundColor(context.getResources().getColor(R.color.colorAccentTransparent));
            myViewHolderG.binding.txtvStartTime.setTextColor(context.getResources().getColor(R.color.white));
            myViewHolderG.binding.txtvTitle.setTextColor(context.getResources().getColor(R.color.black));
            myViewHolderG.binding.txtvLocation.setTextColor(context.getResources().getColor(R.color.colorAccent));

            myViewHolderG.binding.layoutBottomButtons.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getCount()
    {
        return dataList.size();
    }

    @Override
    public int getInflateLayout()
    {
        return R.layout.inflator_lecture_event;
    }
}
