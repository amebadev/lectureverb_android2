package gilm.lecture.verb.Features.QuickLecture.Presentation.SelectPPT;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;

public class PPTSelector extends AppCompatActivity
{
    File path;
    ListView list;
    static ArrayList<HashMap<String, String>> data = new ArrayList<>();
    static ArrayList<String> pdf_paths = new ArrayList<String>();
    static ArrayList<String> pdf_names = new ArrayList<String>();

    public static void start(Context context) {
        Intent starter = new Intent(context, PPTSelector.class);
        ((Activity) context).startActivityForResult(starter, Constants.RequestCode.PPT_SELECTOR);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ptt_to_image_converter);

        AlertDialog.Builder dialog = new AlertDialog.Builder(PPTSelector.this);
        dialog.setTitle("Information");
        dialog.setMessage("Please make sure your files are in phone's storage.");
        dialog.setPositiveButton("Ok", null);
        dialog.show();

        setupToolbar();

        list = (ListView) findViewById(R.id.listView1);
        pdf_paths.clear();
        pdf_names.clear();

        //Access External storage
//		String secStore = "/storage/extSdCarcd";
//		path = new File(secStore);
        path = new File(Environment.getExternalStorageDirectory() + "");
        data.clear();
        searchFolderRecursive1(path);

        list.setAdapter(new PdfAdapter());
        list.setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Intent inn = new Intent();
                inn.putExtra("path", pdf_paths.get(position));
                inn.putExtra("name", pdf_names.get(position));
                setResult(Activity.RESULT_OK, inn);
                finish();
            }
        });
    }

    private void setupToolbar() {
        ImageView imgvBack = (ImageView) findViewById(R.id.imgvBack);
        imgvBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void searchFolderRecursive1(File folder) {
        if (folder != null) {
            if (folder.listFiles() != null) {
                for (File file : folder.listFiles()) {
                    if (file.isFile()) {
                        if (file.getName().contains(".ppt")) {
                            Log.e("-----------------------", "path__=" + file.getName());
                            file.getPath();

                            HashMap<String, String> innerHAsh = new HashMap<>();
                            innerHAsh.put("name", file.getName());
                            innerHAsh.put("path", file.getPath());
                            data.add(innerHAsh);

//                            if (isUnderSize(file)) {
                            pdf_names.add(file.getName());
                            pdf_paths.add(file.getPath());
//                            }


                            Log.e("ppt_paths", "" + data);
                        }
                    }
                    else {
                        searchFolderRecursive1(file);
                    }
                }
            }
        }
    }

    private boolean isUnderSize(File file) {
        try {
//            File file = new File(filePath);
            long length = file.length();
            length = length / 1024;

            System.out.println("File Path : " + file.getPath() + ", File size : " + length + " KB");

            return length < 1024;
        }
        catch (Exception e) {
            System.out.println("File not found : " + e.getMessage() + e);
        }
        return false;
    }

    class PdfAdapter extends BaseAdapter
    {
        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View row, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) PPTSelector.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            row = inflater.inflate(R.layout.inflater_ppt_selector, viewGroup, false);

            TextView txtv_pdfname = (TextView) row.findViewById(R.id.txtv_pdfname);
            txtv_pdfname.setText(data.get(i).get("name"));

            return row;
        }
    }
}
