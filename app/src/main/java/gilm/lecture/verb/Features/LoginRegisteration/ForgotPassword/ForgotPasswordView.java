package gilm.lecture.verb.Features.LoginRegisteration.ForgotPassword;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 05 Jul 2017.
 */

public interface ForgotPasswordView extends Viewable<ForgotPasswordPresenter>
{
    void passwordSent();
}
