/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.MyProfile.Edit;

import android.app.Activity;
import android.databinding.ObservableField;
import android.net.Uri;
import android.view.View;

import com.google.android.gms.location.places.Place;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import gilm.lecture.verb.Features.InterestsSelection.AddInterests.AddInterestCategoriesActivity;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.NaviagtionDrawer.ProfileUpdateBus;
import gilm.lecture.verb.Features.Settings.SettingBasePresenter;
import gilm.lecture.verb.UtilsG.BitmapUtils.BitmapDecoderG;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;

/**
 * Created by G-Expo on 21 Jul 2017.
 */

public class ProfileSettingsPresenter extends SettingBasePresenter<ProfileSettingsView> implements ProfileSettingBinder
{
    boolean isCoverPic;
    public ObservableField<String> bio = new ObservableField<>();
    public ObservableField<String> website = new ObservableField<>();
    public ObservableField<String> location = new ObservableField<>();
    public ObservableField<String> interests = new ObservableField<>();

    public ProfileSettingsPresenter() {
        super();
        bio.set("");
        website.set("");
        location.set("");
        interests.set("");
    }

    @Override
    public void initLocalData() {
        super.initLocalData();

        bio.set(getView().getLocalData().getBio());
        website.set(getView().getLocalData().getWebsite());
        location.set(getView().getLocalData().getLocation());
        interests.set(getView().getLocalData().getMyInterests());
    }

    @Override
    public void openMainSettings(View view) {
        super.openMainSettings(view);
        getView().close();
    }

    @Override
    public void detachView() {
        UtillsG.hideKeyboard(getView().getActivityG(), getView().getViewToHideKeyBoard());
        super.detachView();
    }

    @Override
    public void changeProfileImage(View view) {
        isCoverPic = false;
        BitmapDecoderG.selectImage(getView().getActivityG(), null);
    }

    public void setSelectedProfileImageUri(Uri uri) {
        File file = null;
        try {
            file = new File(uri.getPath());
        }
        catch (Exception e) {
            file = null;
            e.printStackTrace();
        }


        if (file != null) {
            getView().showLoading("loading");

            if (isCoverPic) {
                uploadCoverProfileImage(file);

            }
            else {
                uploadProfileImage(file);
            }


        }
    }

    private void uploadCoverProfileImage(File file) {
        new ImageUploader()
        {
            @Override
            protected void onResponseG(UserDataModel userDataModel) {
                getView().getLocalData().setUserData(userDataModel.getData());
                coverProfileImage.set(getView().getLocalData().getCoverProfilePic());
                getView().hideLoading();
                EventBus.getDefault().post(new ProfileUpdateBus(true));
            }

            @Override
            protected void onErrorG(String error) {
                getView().hideLoading();
                getView().displayError(error);
            }

            @Override
            protected String getUserId() {
                return getView().getLocalData().getUserId();
            }
        }.uploadCoverProfileImage(file);
    }

    private void uploadProfileImage(File file) {
        new ImageUploader()
        {
            @Override
            protected void onResponseG(UserDataModel userDataModel) {
                if (getView() != null) {
                    getView().getLocalData().setUserData(userDataModel.getData());
                    profileImage.set(getView().getLocalData().getProfilePic());
                    getView().hideLoading();
                    EventBus.getDefault().post(new ProfileUpdateBus(true));

                    UtillsG.showToast("Profile Image Updated Successfully.", getView().getActivityG(), true);
                }
            }

            @Override
            protected void onErrorG(String error) {
                if (getView() != null) {
                    getView().hideLoading();
                    getView().displayError(error);
                }
            }

            @Override
            protected String getUserId() {
                return getView().getLocalData().getUserId();
            }
        }.uploadProfileImage(file);
    }

    @Override
    public ObservableField<String> bio() {
        return bio;
    }

    @Override
    public ObservableField<String> location() {
        return location;
    }

    @Override
    public ObservableField<String> website() {
        return website;
    }

    @Override
    public ObservableField<String> interests() {
        return interests;
    }

    @Override
    public void selectInterests(View view) {
//        InterestsSelectionActivity.startfOResult(getView().getActivityG(), "What's Your Interest's", new SharedPrefHelper(getView().getActivityG()).getMyInterests());
        AddInterestCategoriesActivity.start(getView().getActivityG(), false, "", "");
    }

    @Override
    public void selectLocation(View view) {
        getView().openPlacePicker(view);
    }

    public void updateProfile() {

        if (name.get().isEmpty()) {
            UtillsG.showToast("Please enter name", getView().getActivityG(), true);
        }
        else {
            getView().showProfileUpdateProgress();

            createApiRequest(getRetrofitInstance().updateProfile(getView().getLocalData().getUserId(),
                    name.get(),
                    bio.get(),
                    location.get(),
                    lat, lang,
                    website.get()),
                    new CallBackG<UserDataModel>()
                    {
                        @Override
                        public void onCallBack(UserDataModel output) {
                            getView().hideLoading();
                            if (output.getStatus()) {
                                getView().profileUpdated(output);
                            }
                            else {
                                getView().displayError(output.getMessage());
                            }
                        }
                    });
        }
    }


    @Override
    public void changeCoverProfileImage(View view) {
        isCoverPic = true;
        BitmapDecoderG.selectImage(getView().getActivityG(), null);
    }

    private ProfileApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(ProfileApi.class);
    }

    private String lat = "";
    private String lang = "";

    public void setSelectedLocation(Place place) {
        location.set(place.getName().toString());
        lat = String.valueOf(place.getLatLng().latitude);
        lang = String.valueOf(place.getLatLng().longitude);
    }

    public void cropMeImage(Uri uri) {

//        File imageFile = FileHelper.getInstance().createImageFile(System.currentTimeMillis() + ".png");

        if (isCoverPic) {
            CropImage.activity(uri)
                    .setAspectRatio(16, 9)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start((Activity) getView().getActivityG());
        }
        else {
            CropImage.activity(uri)
                    .setAspectRatio(2, 2)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start((Activity) getView().getActivityG());
        }
    }
}
