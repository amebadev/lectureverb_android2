/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Player.CommentsDiscussion;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.BlankFragment;
import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments.CommentsFragment;
import gilm.lecture.verb.Features.TabAdapter.FragmentTabModel;
import gilm.lecture.verb.Features.TabAdapter.SectionsPagerAdapter;
import gilm.lecture.verb.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommentDiscussionTabFragment extends Fragment
{

    public static Lecture_And_Event_Model lecDetails;

    public static CommentDiscussionTabFragment newInstance()
    {
        return new CommentDiscussionTabFragment();
    }


    public CommentDiscussionTabFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_comment_discussion_tab, container, false);


        List<FragmentTabModel> list = new ArrayList<>();
      //  list.add(new FragmentTabModel(CommentsFragment.newInstance(), "Comments"));
        list.add(new FragmentTabModel(BlankFragment.newInstance(), "Discussion"));

        ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tab);

        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getChildFragmentManager(), list);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

}
