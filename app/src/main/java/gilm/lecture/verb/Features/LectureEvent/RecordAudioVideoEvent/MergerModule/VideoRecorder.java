package gilm.lecture.verb.Features.LectureEvent.RecordAudioVideoEvent.MergerModule;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.flurgle.camerakit.CameraListener;
import com.flurgle.camerakit.CameraView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.io.File;

import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureApi;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VideoRecorder extends AppCompatActivity implements ProgressRequestBody.UploadCallbacks {

    Button btnRecord, btnStop;
    ToggleButton toggleFlashLight;
    boolean isFlashOn = false;
    private CameraView cameraView;

    Context context;
    LectureDetailsModel eventModel;
    private DatabaseReference hostReference;

    ImageView imgvHostStatus;
    TextView txtvLecturerStatus;

    boolean isLecturerReady = false;
    private PowerManager.WakeLock wl;

    boolean isRecording;

    File videoFile;

    /*THIS ACTIVITY IS COMMUNICATING WITH AUDIO RECORDER ACTIVITY IN THE SAME PACKGE - USING FIREBASE DATABASE*/
    public static void start(Context context, LectureDetailsModel eventModel) {
        Intent starter = new Intent(context, VideoRecorder.class);
        starter.putExtra(Constants.Extras.DATA, eventModel);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_rec_event_video);
        context = VideoRecorder.this;
        eventModel = (LectureDetailsModel) getIntent().getSerializableExtra(Constants.Extras.DATA);

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        wl.acquire();

        implementFirebase();

        txtvLecturerStatus = findViewById(R.id.txtvLecturerStatus);
        imgvHostStatus = findViewById(R.id.imgvHostStatus);
        cameraView = findViewById(R.id.camera);
        toggleFlashLight = findViewById(R.id.toggleFlashLight);

        btnRecord = findViewById(R.id.btnRecord);
        btnStop = findViewById(R.id.btnStop);

        btnRecord.setVisibility(View.VISIBLE);
        btnStop.setVisibility(View.GONE);


/*
        toggleFlashLight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isFlashOn = isChecked;
                cameraView.setFlash(isFlashOn ? CameraKit.Constants.FLASH_ON : CameraKit.Constants.FLASH_OFF);
            }
        });
*/

        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLecturerReady) {
                    hostReference.setValue("recording");
                    startVideoRecording();

                } else {
                    DialogHelper.getInstance().showInformation(context, "Please open event production screen on lecturer's device also in order to start recording in a sync.", new CallBackG<String>() {
                        @Override
                        public void onCallBack(String output) {

                        }
                    });
                }
//                toggleFlashLight.setVisibility(View.GONE);

//                sendRecordingNotification();
            }

            private void startVideoRecording() {
                cameraView.setCameraListener(new CameraListener() {
                    @Override
                    public void onVideoTaken(File video) {
                        Toast.makeText(VideoRecorder.this, "Video Recorded :\n" + video.getAbsolutePath(), Toast.LENGTH_SHORT).show();

                        videoFile = video;

                        super.onVideoTaken(video);
                    }
                });

                cameraView.startRecordingVideo();
                isRecording = true;
                btnStop.setVisibility(View.VISIBLE);
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hostReference.setValue("recordingStopped");

                cameraView.stopRecordingVideo();
                isRecording = false;

                btnRecord.setVisibility(View.VISIBLE);
                btnStop.setVisibility(View.GONE);

//                uploadVideoLecture();

//                toggleFlashLight.setVisibility(View.VISIBLE);
            }
        });
    }

    private void implementFirebase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        hostReference = database.getReference().child("event").child(eventModel.getEvent_id()).child("host");
        final DatabaseReference lecturerRef = database.getReference().child("event").child(eventModel.getEvent_id()).child("lecturer");

        // Read from the database
        database.getReference().child("event").child(eventModel.getEvent_id()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild("lecturer")) {
                    lecturerRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String state = (String) dataSnapshot.getValue();

                            if (state.equalsIgnoreCase("ready")) {
                                imgvHostStatus.setImageResource(R.drawable.ic_dot_greem);
                                txtvLecturerStatus.setText(Html.fromHtml("<b>Lecturer : </b>" + "Ready"));
                                isLecturerReady = true;
                            } else if (state.equalsIgnoreCase("recording")) {
                                imgvHostStatus.setImageResource(R.drawable.ic_dot_greem);
                                txtvLecturerStatus.setText(Html.fromHtml("<b>Lecturer : </b>" + "Recording"));
                                isLecturerReady = true;
                            } else if (state.equalsIgnoreCase("notReady")) {
                                imgvHostStatus.setImageResource(R.drawable.ic_dot_grey);
                                txtvLecturerStatus.setText(Html.fromHtml("<b>Lecturer : </b>" + "Not Ready"));
                                isLecturerReady = false;

                                if (isRecording) {
                                    cameraView.stopRecordingVideo();
                                    UtillsG.showToast("Recording cancelled by lecturer.", context, true);
                                    finish();
                                }
                            }else if (state.equalsIgnoreCase("uploading")) {
                                imgvHostStatus.setImageResource(R.drawable.ic_dot_greem);
                                txtvLecturerStatus.setText(Html.fromHtml("<b>Lecturer : </b>" + "Uploading"));
                                isLecturerReady = false;
                                UtillsG.showLoading("Please wait...",context);
                            }else if (state.equalsIgnoreCase("uploaded")) {
                                imgvHostStatus.setImageResource(R.drawable.ic_dot_greem);
                                txtvLecturerStatus.setText(Html.fromHtml("<b>Lecturer : </b>" + "Uploaded Successfully"));
                                isLecturerReady = false;
                                uploadVideoLecture();
                            }


                            Log.e("DATA CHANGED : " + state + ":", dataSnapshot.getValue() + "");
                        }

                        @Override
                        public void onCancelled(DatabaseError error) {
                            // Failed to read value
                            Log.e("FIREBASE DEMO", "Failed to read value.", error.toException());
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setMeReady() {
        hostReference.setValue("ready");
    }

    private void setMeNotReady() {
        hostReference.setValue("notReady");
    }

    private void sendRecordingNotification() {
        Gson gson = new Gson();
        UtillsG.sendDirectNotification(context, eventModel.getEvent_id(), "record_audio", gson.toJson(eventModel), new CallBackG<Boolean>() {
            @Override
            public void onCallBack(Boolean output) {
                Log.e("SEND NOTIFICATION :::", "----------------------" + output + "-------------------------");
            }
        });
    }


    @Override
    protected void onPause() {
        cameraView.stop();

//        setMeNotReady();
        hostReference.setValue("cancelled");
        hostReference.setValue("notReady");

        super.onPause();
        finish();
    }

    @Override
    public void onDestroy() {
//        if (EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().unregister(this);
//        }
        wl.release();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        cameraView.start();
        setMeReady();
//        if (!EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().register(this);
//        }
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void EventBusReceiver(Lecture_And_Event_Model model, String flag) {
//        Log.e("NOTIFICATION RECEIVED::" + getClass(), "----------------" + flag + "------------------");
//    }

    private void uploadVideoLecture() {
        UtillsG.showLoading("Uploading ...", context);
        Call<QuickLectureModel> quickLectureApiModelCall =
                LVApplication.getRetrofit().create(QuickLectureApi.class).uploadPreRecordedLecture(
                        getFilePart(videoFile, "video"),
                        getOtherParams("video"),
                        getOtherParams(eventModel.getUser_id()),
                        getOtherParams(eventModel.getLecture_title()),
                        getOtherParams(""),
                        getOtherParams("Public")
                        , getOtherParams("")
                        , getOtherParams(eventModel.getUserdata().getFull_name())
                        , getOtherParams("")
                        , getOtherParams(eventModel.getLecture_poster())
                        , getOtherParams("0")
                        , getOtherParams(eventModel.getEvent_id())
                );
        quickLectureApiModelCall.enqueue(new Callback<QuickLectureModel>() {
            @Override
            public void onResponse(Call<QuickLectureModel> call, final Response<QuickLectureModel> response) {


                if (response.body() != null) {

                    if (response.body().getStatus()) {

                        UtillsG.showToast("Uploaded successfully.", context, true);
                        finish();

                        /*Call<ThumbnailResponseModel> lectureThumbnailApiModelCall
                                = LVApplication.getRetrofit().create(QuickLectureApi.class).uploadLectureThumbnail(
                                getFilePart(asdf, "thumbnail"),
                                getOtherParams(response.body().getmQuickLectureData().getQuick_lecture_id()),
                                getUploadTypePart());
                        lectureThumbnailApiModelCall.enqueue(new Callback<ThumbnailResponseModel>() {
                            @Override
                            public void onResponse(Call<ThumbnailResponseModel> thumbnailApiCall, Response<ThumbnailResponseModel> thumbnailApiResponse) {
                                UtillsG.hideLoading();

                                if (thumbnailApiResponse.body() != null) {

                                    if (thumbnailApiResponse.body().getStatus()) {
                                        updateLectureEventInterest(response.body().getmQuickLectureData().getQuick_lecture_id());

                                    } else {
                                        UtillsG.showToast("Error in creating quick lecture", context, true);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ThumbnailResponseModel> thumbnailApiCall, Throwable t) {
                                UtillsG.hideLoading();
                                Log.e("Error", t.toString());
                            }
                        });*/
                    } else {
                        UtillsG.hideLoading();
                        UtillsG.showToast(response.body().getMessage(), context, true);
                    }
                } else {
                    UtillsG.hideLoading();
                    UtillsG.showToast("An error occured while uploading.", context, true);
                }
            }

            @Override
            public void onFailure(Call<QuickLectureModel> call, Throwable t) {
                Log.e("Error", t.toString());
                UtillsG.showToast("Error in creating quick lecture = " + t.toString(), context, true);
                UtillsG.hideLoading();
            }
        });
    }


    private MultipartBody.Part getFilePart(final File file, String fileType) {
        ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(fileType, file.getName(), fileBody);
        return filePart;
    }

    private RequestBody getUploadTypePart() {
        return RequestBody.create(MediaType.parse("text/plain"), "upload");
    }


    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
