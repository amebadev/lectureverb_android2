package gilm.lecture.verb.Features.UserGroups;

import java.util.ArrayList;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * created by PARAMBIR SINGH on 21/12/17.
 */

public class GroupListModel extends BasicApiModel
{
    ArrayList<GroupModel> data = new ArrayList<>();

    public ArrayList<GroupModel> getData() {
        return data;
    }

    public void setData(ArrayList<GroupModel> data) {
        this.data = data;
    }
}
