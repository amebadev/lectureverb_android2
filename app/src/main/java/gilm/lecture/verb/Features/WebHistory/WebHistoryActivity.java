package gilm.lecture.verb.Features.WebHistory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.WebHistoryModel;

public class WebHistoryActivity extends AppCompatActivity
{

    RecyclerView recyHistory;
    LinearLayoutManager linearLayoutManager;
    ArrayList<WebHistoryModel> alModels = new ArrayList<>();
    WebHistoryAdapter adapter;

    public static void start(Context context) {
        Intent starter = new Intent(context, WebHistoryActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
        setContentView(R.layout.activity_web_history);

        setupToolbar("Browsing History");

        recyHistory = (RecyclerView) findViewById(R.id.recyHistory);

        alModels = new SharedPrefHelper(this).getWebHistory(this);
        Collections.reverse(alModels);
        adapter = new WebHistoryAdapter(alModels, this);

        adapter.setShouldLoadMore(false);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyHistory.setLayoutManager(linearLayoutManager);
        recyHistory.setAdapter(adapter);
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.toolbar_title);

        if (viewById != null) {
            viewById.setText(title);
        }
        else {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
        }
        return super.onOptionsItemSelected(item);
    }

}
