package gilm.lecture.verb.Features.Home.ReactedUsers;

import com.google.gson.annotations.SerializedName;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * Created by param on 12/3/18.
 */

public class ReacteddataModel extends BasicApiModel {
    @SerializedName("data")
    ReactedUsersListsModel lists = new ReactedUsersListsModel();

    public ReactedUsersListsModel getLists() {
        return lists;
    }

    public void setLists(ReactedUsersListsModel lists) {
        this.lists = lists;
    }
}
