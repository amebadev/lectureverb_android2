package gilm.lecture.verb.Features.Favorites;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 17 Jul 2017.
 */

public interface FavoritesView extends Viewable<FavoritesPresenter>
{
    void showLectureEvents();
    void FavouriteLecturesClicked();
}
