package gilm.lecture.verb.Features.MyProfile.Followers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * created by PARAMBIR SINGH on 10/10/17.
 */

public class FollowRequestsFragment extends Fragment {
    View rootView;
    TextView txtvNoData;
    RecyclerView recyclerView;
    private boolean isFollowingsList;
    UserDataModel.UserData mUserData = null;
    String userId = "";
    FollowRequestListAdapter followingFollwerAdapter = null;
    List<UserDataModel.UserData> mUsersList = new ArrayList<>();

    public FollowRequestsFragment() {
        // Required empty public constructor
    }


    @SuppressLint("ValidFragment")
    public FollowRequestsFragment(boolean isFollowingsList, UserDataModel.UserData mUserData) {
        this.isFollowingsList = isFollowingsList;
        this.mUserData = mUserData;
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventBusCalled(String userId) {
        if (mUserData == null) {
            getFollowerAndFollowingList();
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = getActivity().getLayoutInflater().inflate(R.layout.fragment_following_followers, null);

        txtvNoData = (TextView) rootView.findViewById(R.id.txtvNoData);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);


        if (mUserData != null && !mUserData.getUser_id().equals(new SharedPrefHelper(getActivity()).getUserId())) {
            userId = mUserData.getUser_id();
        } else {
            userId = new SharedPrefHelper(getActivity()).getUserId();
        }


//        UtillsG.showLoading("Loading..", getActivity());

        txtvNoData.setText("Loading List");
        txtvNoData.setVisibility(View.VISIBLE);

        followingFollwerAdapter = new FollowRequestListAdapter(getActivity(), mUsersList, null, true);
        recyclerView.setAdapter(followingFollwerAdapter);

        getFollowerAndFollowingList();

        return rootView;
    }

    public void getFollowerAndFollowingList() {
        Call<ListOfUserModel> getFollowersList = getRetrofitInstance().get_follow_requests_list(userId);
        getFollowersList.enqueue(new Callback<ListOfUserModel>() {
            @Override
            public void onResponse(Call<ListOfUserModel> call, Response<ListOfUserModel> response) {
//                UtillsG.hideLoading();
                if (response.body().getStatus()) {
                    if (!(response.body().getUsersList()).isEmpty()) {
                        mUsersList.clear();
                        mUsersList.addAll(response.body().getUsersList());
                        followingFollwerAdapter.notifyDataSetChanged();
                        recyclerView.setVisibility(View.VISIBLE);
                        txtvNoData.setVisibility(View.GONE);
                    }
                } else {
                    recyclerView.setVisibility(View.GONE);
                    txtvNoData.setText("No requests.");
                    txtvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ListOfUserModel> call, Throwable t) {
                txtvNoData.setText("No requests.");
            }
        });
    }

    private ProfileApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(ProfileApi.class);
    }

}
