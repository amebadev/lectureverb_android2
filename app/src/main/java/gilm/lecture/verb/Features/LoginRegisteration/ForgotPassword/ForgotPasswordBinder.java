/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.LoginRegisteration.ForgotPassword;

import android.databinding.ObservableField;
import android.view.View;

/**
 * Created by G-Expo on 26 Jul 2017.
 */

public interface ForgotPasswordBinder
{
    /**
     * send forgot password request to server,using {@link ForgotPasswordBinder#getEmail()}
     */
    void submitEmail(View view);

    /**
     * bind with email edit text.
     */
    ObservableField<String> getEmail();
}
