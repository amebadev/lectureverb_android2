package gilm.lecture.verb.Features.QuickLecture;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * Created by harpreet on 9/2/17.
 */

public class QuickLectureModel extends BasicApiModel {

    private QuickLectureData data;

    public QuickLectureData getmQuickLectureData() {
        return data;
    }

    public void setmQuickLectureData(QuickLectureData data) {
        this.data = data;
    }

    public class QuickLectureData {
        private String file_path;            //: "/uploads/audio/7e2c5d90350bc5dcf07eb683a360c4c6.m4a",
        private String quick_lecture_type;
        private String user_id;
        private String quick_lecture_text;
        private String privacy;
        private String discovery_url;
        private String quick_lecture_id;


        public String getFile_path() {
            return file_path;
        }

        public void setFile_path(String file_path) {
            this.file_path = file_path;
        }

        public String getQuick_lecture_type() {
            return quick_lecture_type;
        }

        public void setQuick_lecture_type(String quick_lecture_type) {
            this.quick_lecture_type = quick_lecture_type;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getQuick_lecture_text() {
            return quick_lecture_text;
        }

        public void setQuick_lecture_text(String quick_lecture_text) {
            this.quick_lecture_text = quick_lecture_text;
        }

        public String getPrivacy() {
            return privacy;
        }

        public void setPrivacy(String privacy) {
            this.privacy = privacy;
        }

        public String getDiscovery_url() {
            return discovery_url;
        }

        public void setDiscovery_url(String discovery_url) {
            this.discovery_url = discovery_url;
        }

        public String getQuick_lecture_id() {
            return quick_lecture_id;
        }

        public void setQuick_lecture_id(String quick_lecture_id) {
            this.quick_lecture_id = quick_lecture_id;
        }
    }
}
