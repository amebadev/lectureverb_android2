package gilm.lecture.verb.Features.Home.Home;

/**
 * Created by harpreet on 10/6/17.
 */

public class HomeFragmentUpdateBus {
    private boolean mHomeFragment;

    public HomeFragmentUpdateBus(boolean mHomeFragment)
    {
        this.mHomeFragment = mHomeFragment;
    }

    public boolean isHomeFragmentUpdate()
    {
        return mHomeFragment;
    }

    public void setHomeFragmentUpdate(boolean mHomeFragment)
    {
        this.mHomeFragment = mHomeFragment;
    }
}
