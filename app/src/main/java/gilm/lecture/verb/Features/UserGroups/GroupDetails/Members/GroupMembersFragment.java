package gilm.lecture.verb.Features.UserGroups.GroupDetails.Members;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.UserGroups.GroupDetails.GroupDetailsFragment;
import gilm.lecture.verb.Features.UserGroups.GroupModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.WebServices.LVApplication;

/**
 * created by PARAMBIR SINGH on 10/10/17.
 */

    public class GroupMembersFragment extends Fragment
{
    View rootView;
    TextView txtvNoData;
    RecyclerView recyclerView;
    CardView cardview_total_members;
    private boolean isFollowingsList;
    TextView total_members;
    public GroupModel groupModel;
    public GroupDetailsFragment groupDetailsFragment;
    private List<UserDataModel.UserData> alUsers = new ArrayList<>();
    String userId = "";
    GroupMembersAdapter followingFollwerAdapter = null;

    public GroupMembersFragment() {
        // Required empty public constructor
    }


    @SuppressLint("ValidFragment")
    public GroupMembersFragment(boolean isFollowingsList, GroupModel groupModel, GroupDetailsFragment groupDetailsFragment, List<UserDataModel.UserData> alUsers) {
        this.isFollowingsList = isFollowingsList;
        this.groupModel = groupModel;
        this.groupDetailsFragment = groupDetailsFragment;
        this.alUsers = alUsers;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = getActivity().getLayoutInflater().inflate(R.layout.fragment_following_followers, null);

        txtvNoData = (TextView) rootView.findViewById(R.id.txtvNoData);
        total_members = (TextView) rootView.findViewById(R.id.total_members);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        cardview_total_members = (CardView) rootView.findViewById(R.id.cardview_total_members);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        followingFollwerAdapter = new GroupMembersAdapter(
                getActivity(),
                alUsers,
                isFollowingsList,
                false,
                GroupMembersFragment.this);
        recyclerView.setAdapter(followingFollwerAdapter);
if(alUsers!=null) {
    txtvNoData.setVisibility(View.GONE);
    cardview_total_members.setVisibility(View.VISIBLE);

    total_members.setText("Total Members : "+alUsers.size());
}


        return rootView;
    }

    private ProfileApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(ProfileApi.class);
    }

}
