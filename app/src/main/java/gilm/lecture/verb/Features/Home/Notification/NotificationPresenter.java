package gilm.lecture.verb.Features.Home.Notification;

import android.support.v4.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.UtilsG.IsLoadingViewModel;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.Web;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 06 Jul 2017.
 */

public class NotificationPresenter extends BasePresenter<NotificationView>
{

    public IsLoadingViewModel isLoadingViewModel;

    public IsLoadingViewModel getIsLoadingViewModel() {
        return isLoadingViewModel;
    }

    public NotificationPresenter() {
        isLoadingViewModel = new IsLoadingViewModel();
        isLoadingViewModel.setLoading(false);
    }

    private List<NotificationViewModel> list = new ArrayList<>();

    /**
     * loading data for notification list.
     */
   /* public void loadRecentChats() {
        for (int i = 0; i < 7; i++) {

            NotificationViewModel notificationData = new NotificationViewModel();
            notificationData.setMessage("Lecture is [img src=ic_fav/]  liked by Mr." + i);
            notificationData.setRead(false);
            notificationData.setTime(i + " minutes ago");
            notificationData.setImageUrl("https://students.ucsd.edu/_images/student-life/involvement/getinvolved/csi-staff-contacts/Casey.jpg");

            list.add(notificationData);
        }

        isLoadingViewModel.setLoading(true);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isLoadingViewModel.setLoading(false);

                if (getView() != null) {
                    getView().showData(list);
                }
            }
        }, 500);
    }*/
    public void loadData(final int pageNo, final SwipeRefreshLayout swipeRefreshLayout) {
        swipeRefreshLayout.setRefreshing(true);
        Call<NotificationDataListModel> apiCall = getRetrofitInstance().get_notifications(new SharedPrefHelper(getView().getActivityG()).getUserId(), "" + pageNo);

        apiCall.enqueue(new Callback<NotificationDataListModel>()
        {
            @Override
            public void onResponse(Call<NotificationDataListModel> call, Response<NotificationDataListModel> response) {
                swipeRefreshLayout.setRefreshing(false);
                if (getView() != null) {
                    if (response != null && response.body() != null) {

                        ArrayList<NotificationViewModel> mNotificationList = new ArrayList<>();
                        for (int i = 0; i < response.body().getListOfNotifications().size(); i++) {

                            NotificationSingleModel singleNotification = response.body().getListOfNotifications().get(i);
                            NotificationViewModel mNotifiView = new NotificationViewModel();
                            mNotifiView.setImageUrl(Web.Path.BASE_IMAGE_URL + singleNotification.getUserData().getProfile_pic());
                            mNotifiView.setTime(singleNotification.getDate_time());

                            if (singleNotification.getType().equals(Web.NotificationList_Type.favourite)) {
                                String name = "Your";
                                try {

                                    name = singleNotification.getQuick_lecture().getUser_id().trim().equals(new SharedPrefHelper(getView().getActivityG()).getUserId().trim())
                                            ? "your "
                                            : singleNotification.getQuick_lecture().getUserdata().getFull_name() + "'s ";
                                }
                                catch (Exception e) {
                                }

                                if (singleNotification.getNotify_id_table() != null && singleNotification.getNotify_id_table().equals("lv_quick_lecture")
                                        && singleNotification.getQuick_lecture().getQuick_lecture_text() != null) {
                                    mNotifiView.setMessage(singleNotification.getUserData().getFull_name() + " [img src=ic_favorite_red/]  liked " + name +
                                            UtillsG.QuickLectureType(singleNotification.getQuick_lecture().getQuick_lecture_type(), "post") + " '' "
                                            + singleNotification.getMessage().substring(singleNotification.getMessage().indexOf("(") + 1,
                                            singleNotification.getMessage().indexOf(")"))
                                            + " ''");
                                }
                                else {
                                    mNotifiView.setMessage(singleNotification.getUserData().getFull_name() + " [img src=ic_favorite_red/]  liked your post '' "
                                            + singleNotification.getMessage().substring(singleNotification.getMessage().indexOf("(") + 1,
                                            singleNotification.getMessage().indexOf(")"))
                                            + " ''");
                                }
                                mNotifiView.setFavorite(true);
                            }
                            else if (singleNotification.getType().equals(Web.NotificationList_Type.repost)) {
                                if (singleNotification.getNotify_id_table() != null && singleNotification.getNotify_id_table().equals("lv_quick_lecture")
                                        && singleNotification.getQuick_lecture().getQuick_lecture_text() != null) {

                                    mNotifiView.setMessage(singleNotification.getUserData().getFull_name() + " [img src=ic_refresh_green/]  reposted your " +
                                            UtillsG.QuickLectureType(singleNotification.getQuick_lecture().getQuick_lecture_type(), "post") + "'' "
                                            + singleNotification.getQuick_lecture().getQuick_lecture_text()
                                            + " ''");
                                }
                                else {
                                    mNotifiView.setMessage(singleNotification.getUserData().getFull_name() + " [img src=ic_refresh_green/]  re-posted your post '' "
                                            + singleNotification.getMessage().substring(singleNotification.getMessage().indexOf("(") + 1, singleNotification.getMessage().indexOf(")"))
                                            + " ''");
                                }
                                mNotifiView.setReposted(true);
                            }
                            else if (singleNotification.getType().equals(Web.NotificationList_Type.share)) {
                                if (singleNotification.getNotify_id_table() != null && singleNotification.getNotify_id_table().equals("lv_quick_lecture")
                                        && singleNotification.getQuick_lecture().getQuick_lecture_text() != null) {
                                    mNotifiView.setMessage(singleNotification.getUserData().getFull_name() + " [img src=ic_share_green/]  share your " +
                                            UtillsG.QuickLectureType(singleNotification.getQuick_lecture().getQuick_lecture_type(), "post") + "'' " +
                                            singleNotification.getMessage().substring(singleNotification.getMessage().indexOf("(") + 1, singleNotification.getMessage().indexOf(")"))
                                            + " ''");
                                }
                                else {
                                    mNotifiView.setMessage(singleNotification.getUserData().getFull_name() + " [img src=ic_share_green/]  share your posted event  '' " +
                                            singleNotification.getMessage().substring(singleNotification.getMessage().indexOf("(") + 1, singleNotification.getMessage().indexOf(")"))
                                            + " ''");
                                }

                            }
                            else if (singleNotification.getType().equals(Web.NotificationList_Type.comment)) {
                                if (singleNotification.getNotify_id_table() != null && singleNotification.getNotify_id_table().equals("lv_quick_lecture")
                                        && singleNotification.getQuick_lecture().getQuick_lecture_text() != null) {
                                    mNotifiView.setMessage(/*"Your "+*/UtillsG.QuickLectureType(singleNotification.getQuick_lecture().getQuick_lecture_type(), "post") + " ("
                                            + singleNotification.getMessage().substring(singleNotification.getMessage().indexOf("(") + 1, singleNotification.getMessage().indexOf(")")) + ")"
                                            + " is [img src=ic_message_grey/] commented by  '' " + singleNotification.getUserData().getFull_name());
                                }
                                else {
                                    mNotifiView.setMessage("Post ("
                                            + singleNotification.getMessage().substring(singleNotification.getMessage().indexOf("(") + 1, singleNotification.getMessage().indexOf(")")) + ")"
                                            + " is [img src=ic_message_grey/] commented by  '' " + singleNotification.getUserData().getFull_name());
                                }
                            }
                            else {
                                mNotifiView.setCommented(true);
                                mNotifiView.setMessage(singleNotification.getMessage());
                            }
                            singleNotification.setMessage(mNotifiView.getMessage());
                            singleNotification.setMessage(mNotifiView.getMessage());
                            mNotifiView.setmNotificationDetails(singleNotification);
                            mNotifiView.setFlag(singleNotification.getType());


                            if (singleNotification.getNotify_id_table() != null && singleNotification.getNotify_id_table().equals("lv_quick_lecture")
                                    && singleNotification.getQuick_lecture() != null && singleNotification.getQuick_lecture().getQuick_lecture_id() == null) {
                            }
                            else {
                                mNotificationList.add(mNotifiView);
                            }


                        }

                        getView().showData(mNotificationList, pageNo);


                    }
                    else {
                        getView().displayError("Json conversion error");
                    }
                }

            }

            @Override
            public void onFailure(Call<NotificationDataListModel> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                if (getView() != null) {
                    getView().displayError(t.getMessage());
                }
            }
        });

    }


    protected HomeApis getRetrofitInstance() {
        return LVApplication.getRetrofit().create(HomeApis.class);
    }

}
