package gilm.lecture.verb.Features.LectureEvent.Invitation;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsAdapter;
import gilm.lecture.verb.Features.LectureEvent.Upcoming.UpcomingFragment;
import gilm.lecture.verb.Features.LectureEvent.Upcoming.UpcomingPresenter;
import gilm.lecture.verb.Features.LectureEvent.Upcoming.UpcomingView;
import gilm.lecture.verb.R;
import gilm.lecture.verb.databinding.FragmentRecyclerlistBinding;

/**
 * Created by harpreet on 9/15/17.
 */

public class InvitationFragment extends BaseFragment<FragmentRecyclerlistBinding, InvitationPresenter> implements InvitationView, InfiniteAdapterG.OnLoadMoreListener
{
    int page = 1;
    public ArrayList<LectureEventViewModel> coHostList = new ArrayList<>();
    LectureEventsAdapter invitationAdapter;
    public static InvitationFragment newInstance()
    {
        return new InvitationFragment();
    }

    public InvitationFragment()
    {
// Required empty public constructor
    }

    @Override
    public void onPause() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        page=1;
        getPresenter().loadData(page);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void RefreshData(HomeFragmentUpdateBus data) {
        if (getView() != null) {
            page = 1;
            getPresenter().loadData(page);
        }
    }


    @Override
    protected int getLayoutId()
    {
        return R.layout.fragment_recyclerlist;
    }

    @Override
    protected void onCreateFragmentG()
    {
        injectPresenter(new InvitationPresenter());
        getPresenter().attachView(this);
    }


    @Override
    public void initViews()
    {
        getDataBinder().setIsLoading(getPresenter().getIsLoadingViewModel());

        invitationAdapter = new LectureEventsAdapter(coHostList,getPresenter(),getActivityG());
        invitationAdapter.setOnLoadMoreListener(this);
        getDataBinder().reviewsList.setAdapter(invitationAdapter);
        invitationAdapter.setShouldLoadMore(false);

        getPresenter().loadData(page);


    }

    @Override
    public void onLoadMore()
    {
        page++;
        getPresenter().loadData(page);
    }


    @Override
    public void showData(ArrayList<LectureEventViewModel> list,int pageNo) {
        if(pageNo==1){
            coHostList.clear();
        }
        coHostList.addAll(list);
        invitationAdapter.notifyDataSetChanged();
    }
}
