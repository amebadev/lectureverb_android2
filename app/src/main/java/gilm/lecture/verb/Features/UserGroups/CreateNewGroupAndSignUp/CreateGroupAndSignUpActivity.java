package gilm.lecture.verb.Features.UserGroups.CreateNewGroupAndSignUp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.LoginRegisteration.EmailVerification.EmailVerificationActivity;
import gilm.lecture.verb.Features.LoginRegisteration.LoginRegisterApi;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity;
import gilm.lecture.verb.Features.PaypalPayment.PaymentActivity;
import gilm.lecture.verb.Features.PaypalPayment.PriceModelRetrofit;
import gilm.lecture.verb.Features.UserGroups.GroupModel;
import gilm.lecture.verb.Features.UserGroups.UserGroupApis;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.BitmapDecoderG;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import gilm.lecture.verb.WebServices.Web;
import gilm.lecture.verb.databinding.ActivityCreateGroupBinding;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateGroupAndSignUpActivity extends BaseActivity<ActivityCreateGroupBinding, CreateGroupPresenter> implements CreateGroupView, View.OnClickListener, CompoundButton.OnCheckedChangeListener
{

    private Uri selectedImageUri;

    boolean isPaid;
    private boolean isAlreadyRegistered, isOnlyCreateGroup;
    GroupModel groupModel;

    public static void start(Context context)
    {
        Intent starter = new Intent(context, CreateGroupAndSignUpActivity.class);
        context.startActivity(starter);
    }

    public static void startForGroupOnly(Context context)
    {
        Intent starter = new Intent(context, CreateGroupAndSignUpActivity.class);
        starter.putExtra("isOnlyCreateGroup", true);
        context.startActivity(starter);
    }

    public static void startForEdit(Context context, GroupModel groupModel)
    {
        Intent starter = new Intent(context, CreateGroupAndSignUpActivity.class);
        starter.putExtra("isOnlyCreateGroup", true);
        starter.putExtra("groupModel", groupModel);
        context.startActivity(starter);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        isAlreadyRegistered = (getLocalData().isVerified() && getLocalData().isRegistered());
    }

    @Override
    protected int setLayoutId()
    {
        return R.layout.activity_create_group;
    }

    @Override
    protected void onCreateActivityG()
    {
        injectPresenter(new CreateGroupPresenter());
        getPresenter().attachView(this);
        setupToolbar("Update User Group");

    }

    @Override
    public void initViews()
    {
        isOnlyCreateGroup = getIntent().getBooleanExtra("isOnlyCreateGroup", false);
        groupModel = (GroupModel) getIntent().getSerializableExtra("groupModel");


        if (isOnlyCreateGroup)
        {
            getDataBinder().cardSignUp.setVisibility(View.GONE);
            getDataBinder().txtvAdmin.setVisibility(View.GONE);
        }

        getDataBinder().llPublicGroup.setOnClickListener(this);
        getDataBinder().llPublicGroup.setOnClickListener(this);
        getDataBinder().llPrivateTraining.setOnClickListener(this);
        getDataBinder().llInstitute.setOnClickListener(this);

        getDataBinder().imgPoster.setOnClickListener(this);
        getDataBinder().imgCamera.setOnClickListener(this);

        getDataBinder().rbPrivateTraining.setOnCheckedChangeListener(this);
        getDataBinder().rbPublicGroup.setOnCheckedChangeListener(this);
        getDataBinder().rbInstitute.setOnCheckedChangeListener(this);

        if (groupModel != null)
        {

            getDataBinder().edLectureTitle.setText(groupModel.getGroup_title());
            getDataBinder().edDescription.setText(groupModel.getGroup_description());
            getDataBinder().txtvAdmin.setVisibility(View.GONE);
            ImageLoader.setImageRoundSmall(getDataBinder().imgCamera, groupModel.getGroup_image());
            getDataBinder().rbInstitute.setChecked(groupModel.getGroup_type().equals(Web.GroupType.Institute) ? true : false);
            getDataBinder().rbPrivateTraining.setChecked(groupModel.getGroup_type().equals(Web.GroupType.PrivateTraining) ? true : false);
            getDataBinder().rbPublicGroup.setChecked(groupModel.getGroup_type().equals(Web.GroupType.PublicGroup) ? true : false);
            getDataBinder().rbInstitute.setEnabled(false);
            getDataBinder().rbPrivateTraining.setEnabled(false);
            getDataBinder().rbPublicGroup.setEnabled(false);
            getDataBinder().edLectureTitle.setEnabled(false);

        }
    }

    @Override
    public Context getActivityG()
    {
        return CreateGroupAndSignUpActivity.this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        if (groupModel != null)
        {

            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_update, menu);
            return true;
        } else
        {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_create, menu);
            return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            UtillsG.hideKeyboard(getActivityG(), getDataBinder().edLectureTitle);
            finish();
            return true;
        } else if (item.getItemId() == R.id.create)
        {
            createClicked();
            return true;
        } else if (item.getItemId() == R.id.update)
        {
            updateGroup();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateGroup()
    {
        {
            showLoading("Updating User Group..");

            Call<BasicApiModel> apiObject = LVApplication.getRetrofit().create(LoginRegisterApi.class).update_group(
                    selectedImageUri != null
                            ? getFilePart(new File(selectedImageUri.getPath()), Web.Keys.group_image)
                            : null,
                    getOtherParams(groupModel.getId()),
                    getOtherParams(getDataBinder().edLectureTitle.getText().toString().trim()),
                    getOtherParams(getDataBinder().edDescription.getText().toString().trim()),
                    getUploadTypePart()
            );
            apiObject.enqueue(new Callback<BasicApiModel>()
            {
                @Override
                public void onResponse(Call<BasicApiModel> call, final Response<BasicApiModel> response)
                {
                    hideLoading();
                    if (response.body() != null && response.body().getStatus())
                    {
                        DialogHelper.getInstance().showInformation(getActivityG(), "User Group has been updated successfully.", new CallBackG<String>()
                        {
                            @Override
                            public void onCallBack(String output)
                            {
                                finish();

                            }
                        });
                    } else
                    {
                        UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                    }
                }

                @Override
                public void onFailure(Call<BasicApiModel> call, Throwable t)
                {
                    hideLoading();
                    UtillsG.showToast("An error occurred " + t.toString(), getActivityG(), true);
                }
            });

        }

    }

    private void createClicked()
    {
        if (isValidated())
        {
            if (!isAlreadyRegistered)
            {
                showLoading("Please Wait..");
                getPresenter().createApiRequest(LVApplication.getRetrofit().create(LoginRegisterApi.class).register(getDataBinder().edFullName.getText().toString().trim(), getDataBinder().edEmail.getText().toString().trim(), "android", getLocalData().getUserType(), getDataBinder().edPassword.getText().toString().trim()), new CallBackG<UserDataModel>()
                {
                    @Override
                    public void onCallBack(UserDataModel output)
                    {
                        hideLoading();
                        if (output.getStatus())
                        {
                            getLocalData().setIsRegistered(true);
                        } else
                        {
                            if (output.getMessage().toLowerCase().contains("already exist")/* we are using this string to compare web message so that we can show an alert dialog to user */)
                            {
                                DialogHelper.getInstance().showInformation(getActivityG(), "An account with the same email already exists, Please login and then create a Group.", new CallBackG<String>()
                                {
                                    @Override
                                    public void onCallBack(String output)
                                    {
                                        UtillsG.hideKeyboard(getActivityG(), getDataBinder().edLectureTitle);
                                    }
                                });
                            } else
                            {
                                displayError(output.getMessage());
                            }
                        }

                        if (output.getData() != null)
                        {
                            getLocalData().setUserData(output.getData());
                            EmailVerificationActivity.startForResult(getActivityG());
                        }
                    }
                });
            } else
            {
                if (getDataBinder().rbPublicGroup.isChecked())
                {// Because for public groups , there will be no charges
                    createGroup();
                } else
                {
                    showLoading("Please wait..");
                    Call<PriceModelRetrofit> call = LVApplication.getRetrofit().create(ProfileApi.class).get_subscription_prices();
                    call.enqueue(new Callback<PriceModelRetrofit>()
                    {
                        @Override
                        public void onResponse(Call<PriceModelRetrofit> call, Response<PriceModelRetrofit> response)
                        {
                            hideLoading();
                            if (response.body().getStatus())
                            {
                                if (response.body().getData() != null && response.body().getData().size() >= 1)
                                {
                                    final String subscriptionPrice = response.body().getData().get(0).getAmount();// getting second item

                                    DialogHelper.getInstance().showWithAction(getActivityG(), "You need to pay $" + subscriptionPrice + " for creating a new \"Institute\" or \"Private Training\" group. Do you want to Continue ?", new CallBackG<String>()
                                    {
                                        @Override
                                        public void onCallBack(String output)
                                        {
                                            PaymentActivity.start(getActivityG(), subscriptionPrice, "Group Registration");
                                            // REST OF THE PROCESS IS IN onActivityResult Method
                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<PriceModelRetrofit> call, Throwable t)
                        {
                            hideLoading();
                            UtillsG.showToast("Unable to fetch price list.", getActivityG(), true);
                            finish();
                        }
                    });
                }
            }
        }
    }

    private boolean isValidated()
    {
        if (getDataBinder().edLectureTitle.getText().toString().trim().isEmpty())
        {
            UtillsG.showToast("Please enter a Group Title", getActivityG(), true);
            return false;
        } else if (selectedImageUri == null)
        {
            UtillsG.showToast("Please select a Group Image", getActivityG(), true);
            return false;
        }
        if (!isAlreadyRegistered)
        {
            if (getDataBinder().edFullName.getText().toString().trim().isEmpty())
            {
                UtillsG.showToast("Please enter your Full Name", getActivityG(), true);
                return false;
            } else if (getDataBinder().edEmail.getText().toString().trim().isEmpty())
            {
                UtillsG.showToast("Please enter your Email Id", getActivityG(), true);
                return false;
            } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(getDataBinder().edEmail.getText().toString()).matches())
            {
                UtillsG.showToast("Please enter a valid Email Id", getActivityG(), true);
                return false;
            } else if (getDataBinder().edPassword.getText().toString().isEmpty())
            {
                UtillsG.showToast("Please enter a password.", getActivityG(), true);
                return false;
            } else if (getDataBinder().edConfPassword.getText().toString().isEmpty())
            {
                UtillsG.showToast("Please enter confirm password.", getActivityG(), true);
                return false;
            } else if (!getDataBinder().edConfPassword.getText().toString().equals(getDataBinder().edPassword.getText().toString()))
            {
                UtillsG.showToast("Password doesn't match.", getActivityG(), true);
                return false;
            }
        } else
        {
            return true;
        }
        return true;
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {

            case R.id.img_poster:
                BitmapDecoderG.selectImage(getActivityG(), null);
                break;
            case R.id.img_camera:
                BitmapDecoderG.selectImage(getActivityG(), null);
                break;

            case R.id.llPublicGroup:
                if (groupModel == null)
                {
                    getDataBinder().rbPublicGroup.performClick();
                }
                break;
            case R.id.llInstitute:
                if (groupModel == null)
                {

                    getDataBinder().rbInstitute.performClick();
                }
                break;
            case R.id.llPrivateTraining:
                if (groupModel == null)
                {

                    getDataBinder().rbPrivateTraining.performClick();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
            {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK)
                {
                    Uri resultUri = result.getUri();

                    Bitmap bitmap = null;
                    try
                    {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    } catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                    File file = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "audioLectureThumbnail.png");
                    FileOutputStream out = null;
                    try
                    {
                        if (bitmap != null)
                        {
                            out = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                        }
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    } finally
                    {
                        try
                        {
                            if (out != null)
                            {
                                out.close();
                            }
                        } catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }

//                    txtvArtWork.setText(file.getAbsolutePath());
//                    getPresenter().setArtWorkImage(Uri.fromFile(file));
                    selectedImageUri = Uri.fromFile(file);
                    ImageLoader.setImage(getDataBinder().imgPoster, selectedImageUri);
                    getDataBinder().imgCamera.setVisibility(View.GONE);

                }
            } else if (requestCode == Constants.RequestCode.EMAIL_VERIFICATION)
            {
                if (data.getBooleanExtra("isSuccess", false))
                {
                    getDataBinder().edFullName.setEnabled(false);
                    getDataBinder().edEmail.setEnabled(false);
                    getDataBinder().edPassword.setEnabled(false);
                    getDataBinder().edConfPassword.setEnabled(false);

                    if (getDataBinder().rbPublicGroup.isChecked())
                    {// Because for public groups , there will be no charges
                        createGroup();
                    } else
                    {
                        showLoading("Please wait..");
                        Call<PriceModelRetrofit> call = LVApplication.getRetrofit().create(ProfileApi.class).get_subscription_prices();
                        call.enqueue(new Callback<PriceModelRetrofit>()
                        {
                            @Override
                            public void onResponse(Call<PriceModelRetrofit> call, Response<PriceModelRetrofit> response)
                            {
                                if (response.body().getStatus())
                                {
                                    if (response.body().getData() != null && response.body().getData().size() >= 1)
                                    {
                                        final String subscriptionPrice = response.body().getData().get(0).getAmount();// getting second item

                                        DialogHelper.getInstance().showWithAction(getActivityG(), "You have been successfully registered as a Lecturer.\nYou need to pay $" + subscriptionPrice + " for creating a new \"Institute\" or \"Private Training\" group. Do you want to Continue ?", new CallBackG<String>()
                                        {
                                            @Override
                                            public void onCallBack(String output)
                                            {
                                                PaymentActivity.start(getActivityG(), subscriptionPrice, "Group Registration");
                                                // REST OF THE PROCESS IS IN onActivityResult Method
                                            }
                                        });
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<PriceModelRetrofit> call, Throwable t)
                            {
                                hideLoading();
                                UtillsG.showToast("Unable to fetch price list.", getActivityG(), true);
                                finish();
                            }
                        });
                    }
                }
            } else if (requestCode == Constants.RequestCode.PAYPAL_PAYMENT)
            {
                if (data.getBooleanExtra("isSuccess", false))
                {
                    isPaid = true;
                    createGroup();
                }
            } else
            {
                selectedImageUri = BitmapDecoderG.onActivityResult(getActivityG(), requestCode, resultCode, data);
                cropMeImage(selectedImageUri);
//                ImageLoader.setImage(getDataBinder().imgPoster, selectedImageUri);
//                getDataBinder().imgCamera.setVisibility(View.GONE);
            }
        }
    }

    public void cropMeImage(Uri uri)
    {
        CropImage.activity(uri)
                .setAspectRatio(16, 9)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start((Activity) getActivityG());
    }


    private void createGroup()
    {
        showLoading("Creating User Group..");
        String groupType = getDataBinder().rbInstitute.isChecked() ? Web.GroupType.Institute
                : getDataBinder().rbPrivateTraining.isChecked() ? Web.GroupType.PrivateTraining
                : getDataBinder().rbPublicGroup.isChecked() ? Web.GroupType.PublicGroup
                : "0";

        Call<BasicApiModel> apiObject = LVApplication.getRetrofit().create(LoginRegisterApi.class).create_group(
                getFilePart(new File(selectedImageUri.getPath()), Web.Keys.group_image),
                getOtherParams(getLocalData().getUserId()),
                getOtherParams(getDataBinder().edLectureTitle.getText().toString().trim()),
                getOtherParams(groupType),
                getUploadTypePart(),
                getOtherParams(getDataBinder().edDescription.getText().toString().trim())
        );
        apiObject.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, final Response<BasicApiModel> response)
            {
                hideLoading();
                if (response.body() != null && response.body().getStatus())
                {
                    if (isOnlyCreateGroup)
                    {
                        savePaymentInfo(response, true);
                    } else
                    {
                        DialogHelper.getInstance().showInformation(getActivityG(), "User Group has been created successfully.", new CallBackG<String>()
                        {
                            @Override
                            public void onCallBack(String output)
                            {
                                savePaymentInfo(response, false);
                            }
                        });
                    }
                } else
                {
                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t)
            {
                hideLoading();
                UtillsG.showToast("An error occurred " + t.toString(), getActivityG(), true);
            }
        });

    }

    private void savePaymentInfo(Response<BasicApiModel> response, final boolean isOnlyCreateGroup)
    {
        if (!getDataBinder().rbPublicGroup.isChecked())
        {
            showLoading("Saving payment information..");
            Call<BasicApiModel> apiCall = LVApplication.getRetrofit().create(UserGroupApis.class).save_group_payments(
                    getLocalData().getUserId(),
                    response.body().getMessage(),
                    getResources().getString(R.string.group_payment_amount),
                    "paypal"
            );
            apiCall.enqueue(new Callback<BasicApiModel>()
            {
                @Override
                public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
                {
                    hideLoading();

                    UtillsG.hideKeyboard(getActivityG(), getDataBinder().edLectureTitle);
                    finish();

                    if (isOnlyCreateGroup)
                    {
                        UtillsG.showToast("User Group created successfully.", getActivityG(), true);
                    } else
                    {
                        NavigationActivity.start(getActivityG());
                    }
                }

                @Override
                public void onFailure(Call<BasicApiModel> call, Throwable t)
                {
                    hideLoading();
                }
            });
        } else
        {
            if (!isOnlyCreateGroup)
            {
                UtillsG.hideKeyboard(getActivityG(), getDataBinder().edLectureTitle);
                UtillsG.showToast("User Group created successfully.", getActivityG(), true);
                finish();
            } else
            {
                finish();
                UtillsG.hideKeyboard(getActivityG(), getDataBinder().edLectureTitle);
                NavigationActivity.start(getActivityG());
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b)
    {
        if (b)
        {
            switch (compoundButton.getId())
            {
                case R.id.rbInstitute:
                    getDataBinder().rbPrivateTraining.setChecked(false);
                    getDataBinder().rbPublicGroup.setChecked(false);
                    break;
                case R.id.rbPrivateTraining:
                    getDataBinder().rbInstitute.setChecked(false);
                    getDataBinder().rbPublicGroup.setChecked(false);
                    break;
                case R.id.rbPublicGroup:
                    getDataBinder().rbPrivateTraining.setChecked(false);
                    getDataBinder().rbInstitute.setChecked(false);
                    break;
            }
        }
    }


    private MultipartBody.Part getFilePart(final File file, String fileType)
    {
        RequestBody fileBody = new ProgressRequestBody(file, new ProgressRequestBody.UploadCallbacks()
        {
            @Override
            public void onProgressUpdate(int percentage)
            {

            }

            @Override
            public void onError()
            {

            }

            @Override
            public void onFinish()
            {

            }
        });
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(fileType, file.getName(), fileBody);
        return filePart;
    }

    private RequestBody getUploadTypePart()
    {
        return RequestBody.create(MediaType.parse("text/plain"), "upload");
    }


    private RequestBody getOtherParams(String value)
    {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

}
