/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Settings.Tags;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Collection;
import java.util.List;

import gilm.lecture.verb.Features.Home.Home.ArrayListTagsModel;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Home.Notification.Details.UserProfileModel;
import gilm.lecture.verb.Features.InterestsSelection.AddInterests.AddInterestCategoriesActivity;
import gilm.lecture.verb.Features.InterestsSelection.InterestSelectionApi;
import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.Settings.SettingsApi;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.Widget.PKTagsTextView;
import gilm.lecture.verb.databinding.FragmentTagSettingsBinding;
import mabbas007.tagsedittext.TagsEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TagSettingsFragment extends BaseFragment<FragmentTagSettingsBinding, TagSettingsPresenter> implements TagSettingsView, PKTagsTextView.SuggestionsCallBack
{

    LinearLayout ll_interestTags;
    TextView txtv_selectedTags;
    String commaSeparatedIds = "", commaSeparatedNames = "";

    public static TagSettingsFragment newInstance() {
        TagSettingsFragment fragment = new TagSettingsFragment();
        return fragment;
    }

    public TagSettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        getMyInterests();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_tag_settings;
    }

    @Override
    protected void onCreateFragmentG() {
        injectPresenter(new TagSettingsPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        getDataBinder().setBinder(getPresenter());

        /**
         *frameProfileImage is included using a layout  so it cannot be initialized using binder
         */
        UtillsG.setHeightWidthWithRatio_LinearLayout(getActivity(), (getView().findViewById(R.id.frameProfileImage)), 3f, 6f);

        ll_interestTags = (LinearLayout) getView().findViewById(R.id.ll_interestTags);
        txtv_selectedTags = (TextView) getView().findViewById(R.id.txtv_selectedTags);

        ll_interestTags.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
//                InterestsSelectionActivity.startfOResult(TagSettingsFragment.this, "What's Your Interest's", new SharedPrefHelper(getActivityG()).getMyInterests());
                AddInterestCategoriesActivity.startForFragment(TagSettingsFragment.this, false, "", "");
            }
        });

        getMyTags();

        getDataBinder().pktagsTxtv.setTagsTextColor(R.color.white);
        getDataBinder().pktagsTxtv.setTagsBackground(R.drawable.tags_drawable);
        getDataBinder().pktagsTxtv.setCloseDrawableRight(R.drawable.tag_close);
        getDataBinder().pktagsTxtv.setCloseDrawablePadding(R.dimen.padding_5);

        getDataBinder().pktagsTxtv.startSuggestions(getActivityG(), this);

        getDataBinder().imgvDone.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                saveEnteredTags();
            }
        });

        getDataBinder().pktagsTxtv.setTagsListener(new TagsEditText.TagsEditListener()
        {
            @Override
            public void onTagsChanged(Collection<String> collection) {

            }

            @Override
            public void onEditingFinished() {
                if (!getDataBinder().pktagsTxtv.getTags().isEmpty() || !getDataBinder().pktagsTxtv.getText().toString().trim().isEmpty()) {
                    getDataBinder().imgvDone.setVisibility(View.VISIBLE);
                }
                else {
                    getDataBinder().imgvDone.setVisibility(View.GONE);
                }
            }
        });

        getDataBinder().pktagsTxtv.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!getDataBinder().pktagsTxtv.getTags().isEmpty() || !getDataBinder().pktagsTxtv.getText().toString().trim().isEmpty()) {
                    getDataBinder().imgvDone.setVisibility(View.VISIBLE);
                }
                else {
                    getDataBinder().imgvDone.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        getMyInterests();
    }

    private void getMyInterests() {
        showLoading("Please wait..");
        Call<UserProfileModel> call = LVApplication.getRetrofit().create(HomeApis.class).get_user_profile(getLocalData().getUserId(), getLocalData().getUserId());
        call.enqueue(new Callback<UserProfileModel>()
        {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response) {
                hideLoading();
                if (response.body().getStatus()) {
                    List<UserDataModel.UserInterests> userInterests = response.body().getData().getUserdata().getUser_interest();
                    String commaSeparatedCategories = "";
                    for (int i = 0; i < userInterests.size(); i++) {
                        commaSeparatedCategories = commaSeparatedCategories.isEmpty()
                                ? userInterests.get(i).getCategory_name()
                                : commaSeparatedCategories + ", " + userInterests.get(i).getCategory_name();
                    }
                    getLocalData().setMyInterests(commaSeparatedCategories);

                    txtv_selectedTags.setText(
                            getLocalData().getMyInterests().trim().isEmpty()
                                    ? "No interests selected yet."
                                    : getLocalData().getMyInterests());

                }
            }

            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t) {
                hideLoading();
                UtillsG.showToast("ERROR: " + t.getMessage(), getActivityG(), true);
            }
        });
    }

    private void saveEnteredTags() {
        UtillsG.hideKeyboard(getActivityG(), getDataBinder().pktagsTxtv);

        List<String> alTags = getDataBinder().pktagsTxtv.getTags();
        String tags = "";
        for (int i = 0; i < alTags.size(); i++) {
            tags = tags + (tags.isEmpty()
                    ? alTags.get(i)
                    : "|" + alTags.get(i));
        }
        Call<BasicApiModel> ApiCall = LVApplication.getRetrofit().create(SettingsApi.class).add_user_ash_tags(new SharedPrefHelper(getActivityG()).getUserId(), tags);
        ApiCall.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body().getStatus()) {
                    UtillsG.showToast("Tags Saved Successfully.", getActivityG(), true);
                    getMyTags();
                }
                else {
                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {

            }
        });
    }

    private void getMyTags() {
        Call<ArrayListTagsModel> ApiCall = LVApplication.getRetrofit().create(SettingsApi.class).get_ash_tags(new SharedPrefHelper(getActivityG()).getUserId());
        ApiCall.enqueue(new Callback<ArrayListTagsModel>()
        {
            @Override
            public void onResponse(Call<ArrayListTagsModel> call, Response<ArrayListTagsModel> response) {
                if (response.body().getStatus()) {
                    List<PKTagsTextView.TagsModel> arrayListTagsModel = response.body().getListOfLectureModels();
                    Log.e("LIST SIZE = ", arrayListTagsModel.size() + "");

                    String[] myarray = new String[arrayListTagsModel.size()];

//                    String tags = "";
                    for (int i = 0; i < arrayListTagsModel.size(); i++) {
/*                        tags = tags + (tags.isEmpty()
                                ? arrayListTagsModel.get(i).getTag_name()
                                : ", " + arrayListTagsModel.get(i).getTag_name())*/
                        ;

                        myarray[i] = arrayListTagsModel.get(i).getTag_name();
                    }

                    getDataBinder().pktagsTxtv.setTags(myarray);
                    getDataBinder().imgvDone.setVisibility(View.GONE);
                }
                else {
                    getDataBinder().imgvDone.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ArrayListTagsModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == Constants.RequestCode.INTEREST_SELECTION) {
            if (resultCode == Activity.RESULT_OK) {
                /*commaSeparatedIds = "";
                commaSeparatedNames = "";
                ArrayList<ChildCategoryModel> alCategories = (ArrayList<ChildCategoryModel>) data.getSerializableExtra(Constants.Extras.DATA);
                for (int i = 0; i < alCategories.size(); i++) {
                    commaSeparatedIds = commaSeparatedIds + (commaSeparatedIds.isEmpty() ? ""
                            : ",") + alCategories.get(i).getId();
                    commaSeparatedNames = commaSeparatedNames + (
                            commaSeparatedNames.isEmpty() ? ""
                                    : ", ") + alCategories.get(i).getCategory();
                }
                txtv_selectedTags.setText(commaSeparatedNames);
                updateInterests();*/

                txtv_selectedTags.setText(getLocalData().getMyInterests());
            }
        }

    }

    public void updateInterests() {
        Call<BasicApiModel> saveMyInterests = getRetrofitInstance().save_users_interest(commaSeparatedIds, new SharedPrefHelper(getActivityG()).getUserId());
        saveMyInterests.enqueue(new Callback<BasicApiModel>()
        {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body().getStatus()) {
                    new SharedPrefHelper(getActivityG()).setInterestAlreadySet(getActivityG());
                    new SharedPrefHelper(getActivityG()).setMyInterests(commaSeparatedNames);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                UtillsG.showToast(t.getMessage(), getActivityG(), true);
            }
        });
    }

    private InterestSelectionApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(InterestSelectionApi.class);
    }


    @Override
    public void onTagInserted(PKTagsTextView.TagsModel tagsModel) {

    }
}
