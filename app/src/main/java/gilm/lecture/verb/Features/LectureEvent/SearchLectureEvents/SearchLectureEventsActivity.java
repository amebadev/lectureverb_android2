package gilm.lecture.verb.Features.LectureEvent.SearchLectureEvents;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsAdapter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.databinding.ActivitySearchLectureEventBinding;


/**
 * Created by harpreet on 9/26/17.
 */

public class SearchLectureEventsActivity extends BaseActivity<ActivitySearchLectureEventBinding, SearchLectureEventPresenter> implements SearchLectureEventView, InfiniteAdapterG.OnLoadMoreListener {
    int page = 1;
    public ArrayList<LectureEventViewModel> coHostList = new ArrayList<>();
    LectureEventsAdapter invitationAdapter;
    String searckKey = "";
    private EditText edSearch;
    ImageView imgvSearch;

    public static void startActivity(Activity mActivity, String searchKey) {
        Intent starter = new Intent(mActivity, SearchLectureEventsActivity.class);
        starter.putExtra("searchKey", searchKey);
        mActivity.startActivity(starter);
        mActivity.overridePendingTransition(0, R.anim.fade_out);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_search_lecture_event;
    }

    @Override
    protected void onCreateActivityG() {

        injectPresenter(new SearchLectureEventPresenter());
        getPresenter().attachView(this);
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (viewById != null) {
            viewById.setText(title);
        } else {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void initViews() {
        setupToolbar("Search Results");
        searckKey = getIntent().getStringExtra("searchKey");
        getDataBinder().setIsLoading(getPresenter().getIsLoadingViewModel());
        invitationAdapter = new LectureEventsAdapter(coHostList, getPresenter(), getActivityG());
        invitationAdapter.setOnLoadMoreListener(this);
        getDataBinder().reviewsList.setAdapter(invitationAdapter);
        invitationAdapter.setShouldLoadMore(false);


        getPresenter().loadData(page);

        imgvSearch = (ImageView) findViewById(R.id.imgvSearch);
        edSearch = (EditText) findViewById(R.id.edSearch);
        edSearch.setText(searckKey);
        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searckKey = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        imgvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtillsG.hideKeyboard(getActivityG(), edSearch);
                getPresenter().loadData(page, searckKey);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public Context getActivityG() {
        return SearchLectureEventsActivity.this;
    }

    @Override
    public void onLoadMore() {
        page++;

        if (searckKey.isEmpty()) {
            getPresenter().loadData(page);
        } else {
            getPresenter().loadData(page, searckKey);
        }
    }


    @Override
    public void showData(ArrayList<LectureEventViewModel> list, int pageNo) {
        if (pageNo == 1) {
            coHostList.clear();
        }
        coHostList.addAll(list);
        invitationAdapter.notifyDataSetChanged();
    }

    ValueFilter valueFilter;

    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<LectureEventViewModel> filterList = new ArrayList();
                for (int i = 0; i < coHostList.size(); i++) {
                    if ((coHostList.get(i).getTitle().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(coHostList.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = coHostList.size();
                results.values = coHostList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            coHostList = (ArrayList<LectureEventViewModel>) results.values;
            invitationAdapter.notifyDataSetChanged();
        }

    }


}

