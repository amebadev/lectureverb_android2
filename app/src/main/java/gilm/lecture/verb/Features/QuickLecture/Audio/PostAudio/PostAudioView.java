package gilm.lecture.verb.Features.QuickLecture.Audio.PostAudio;

import android.widget.RadioGroup;

import java.io.File;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;

/**
 * Created by G-Expo on 18 Jul 2017.
 */

public interface PostAudioView extends Viewable<PostAudioPresenter>
{
    RadioGroup getRadioGroup();

    String getLectureTitle();

    File getAudioFile();

    void setprogress(int percentage);

    void uploadDone();

    String getTimeDuration();

    LectureDetailsModel getEventModel();
    String get_Group_id();
}
