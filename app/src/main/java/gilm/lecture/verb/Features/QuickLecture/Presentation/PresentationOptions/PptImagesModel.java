package gilm.lecture.verb.Features.QuickLecture.Presentation.PresentationOptions;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * created by PARAMBIR SINGH on 5/9/17.
 */

public class PptImagesModel extends BasicApiModel
{
    /*{
    "status": "Success",
    "code": "200",
    "message": "Email verified successfully, Login to your account.",
    "data": [
        {
            "download_file": "/uploads/ppt/2017/Sep/05/d412bc0ed9161386ba62bd67703eef3e-0.png"
        },
        {
            "download_file": "/uploads/ppt/2017/Sep/05/d412bc0ed9161386ba62bd67703eef3e-1.png"
        },
        {
            "download_file": "/uploads/ppt/2017/Sep/05/d412bc0ed9161386ba62bd67703eef3e-2.png"
        },
        {
            "download_file": "/uploads/ppt/2017/Sep/05/d412bc0ed9161386ba62bd67703eef3e.zip"
        }
    ]
}*/
    @SerializedName("data")
    private List<SingleImageModel> download_files;

    public List<SingleImageModel> getDownload_file() {
        return download_files;
    }

    public void setDownload_file(List<SingleImageModel> download_file) {
        this.download_files = download_file;
    }
}
