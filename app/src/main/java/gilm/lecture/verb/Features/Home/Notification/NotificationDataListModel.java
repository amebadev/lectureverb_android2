package gilm.lecture.verb.Features.Home.Notification;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * Created by harpreet on 10/3/17.
 */

public class NotificationDataListModel extends BasicApiModel {


    @SerializedName("notif")
    List<NotificationSingleModel> listOfNotifications;

    public List<NotificationSingleModel> getListOfNotifications() {
        return listOfNotifications;
    }

    public void setListOfNotifications(List<NotificationSingleModel> listOfNotifications) {
        this.listOfNotifications = listOfNotifications;
    }

}
