/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Messaging.Chat;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import gilm.lecture.verb.BR;

/**
 * Model {@link Class} ,use to store chat message.
 */
public class MsgDataModel extends BaseObservable implements Parcelable
{
    private String chat_id, sender_id, receiver_id, message, date;

    public MsgDataModel()
    {
    }

    protected MsgDataModel(Parcel in)
    {
        chat_id = in.readString();
        sender_id = in.readString();
        receiver_id = in.readString();
        message = in.readString();
        date = in.readString();
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(chat_id);
        parcel.writeString(sender_id);
        parcel.writeString(receiver_id);
        parcel.writeString(message);
        parcel.writeString(date);
    }

    public static final Creator<MsgDataModel> CREATOR = new Creator<MsgDataModel>()
    {
        @Override
        public MsgDataModel createFromParcel(Parcel in)
        {
            return new MsgDataModel(in);
        }

        @Override
        public MsgDataModel[] newArray(int size)
        {
            return new MsgDataModel[size];
        }
    };

    @Bindable
    public String getChat_id()
    {
        return chat_id;
    }

    public void setChat_id(String chat_id)
    {
        this.chat_id = chat_id;
        notifyPropertyChanged(BR.chat_id);
    }

    @Bindable
    public String getSender_id()
    {
        return sender_id;
    }

    public void setSender_id(String sender_id)
    {
        this.sender_id = sender_id;
        notifyPropertyChanged(BR.sender_id);
    }

    @Bindable
    public String getReceiver_id()
    {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id)
    {
        this.receiver_id = receiver_id;
        notifyPropertyChanged(BR.receiver_id);
    }

    @Bindable
    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
        notifyPropertyChanged(BR.message);
    }

    @Bindable
    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
        notifyPropertyChanged(BR.date);
    }

}
