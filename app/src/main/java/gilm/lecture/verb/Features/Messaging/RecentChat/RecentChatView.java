/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Messaging.RecentChat;

import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 17 Jul 2017.
 */

public interface RecentChatView extends Viewable<RecentChatPresenter>
{
    /**
     * Show data on recycler view inside this method.
     */
    void showData(List<ListOfRecentChatsModel.RecentChatsModel> list);

    void moreDateLoaded(List<ListOfRecentChatsModel.RecentChatsModel> list);
}
