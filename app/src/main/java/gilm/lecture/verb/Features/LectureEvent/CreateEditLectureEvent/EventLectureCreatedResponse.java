package gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent;

import java.io.Serializable;
import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * Created by harpreet on 10/5/17.
 */

public class EventLectureCreatedResponse extends BasicApiModel {

    LectureEventId data;

    public LectureEventId getmQuickLectureData() {
        return data;
    }

    public void setmQuickLectureData(LectureEventId
                                             data) {
        this.data = data;
    }

    public class LectureEventId implements Serializable {

        private String event_id;

        public String getEvent_id() {
            return event_id;
        }

        public void setEvent_id(String event_id) {
            this.event_id = event_id;
        }
    }

}
