package gilm.lecture.verb.Features.Connect;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.appinvite.AppInviteInvitation;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Connect.ConnectNearBy.ContactsNearByFragment;
import gilm.lecture.verb.Features.Connect.Contact.ContactListFragment;
import gilm.lecture.verb.Features.TabAdapter.FragmentTabModel;
import gilm.lecture.verb.Features.TabAdapter.SectionsPagerAdapter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.UtillsG;

/**
 * Created by G-Expo on 11 Jul 2017.
 */

public class ConnectTabFragment extends Fragment
{
    private View view;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    ImageView imgvMoreOptions;

    ContactListFragment contactListFragment;

    public static ConnectTabFragment newInstance() {
        return new ConnectTabFragment();
    }

    public ConnectTabFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();

        if (viewPager.getCurrentItem() == 1) {
            displayLocationDialog();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_connect_tab, container, false);

        contactListFragment = ContactListFragment.newInstance();

        imgvMoreOptions = view.findViewById(R.id.imgvMoreOptions);
        viewPager = (ViewPager) view.findViewById(R.id.container);

        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        //Creating our pager adapter

        List<FragmentTabModel> list = new ArrayList<>();
        list.add(new FragmentTabModel(contactListFragment, ""));
        list.add(new FragmentTabModel(ContactsNearByFragment.newInstance(), ""));

        ImageView viewById = (ImageView) view.findViewById(R.id.fab);
        viewById.setVisibility(View.GONE);
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getChildFragmentManager(), list);
        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                UtillsG.hideKeyboard(getActivity(), contactListFragment.getDataBinder().edSearch);
                if (position == 1) {
                    displayLocationDialog();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        imgvMoreOptions.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                showMoreOptions();
            }
        });

        return view;
    }

    private void showMoreOptions() {
        final PopupMenu popup = new PopupMenu(getActivity(), imgvMoreOptions);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.menu_connect_more_options, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.addUser:
                        if (viewPager.getCurrentItem() != 0) {
                            viewPager.setCurrentItem(0);
                        }
                        UtillsG.shakeThisView(contactListFragment.getDataBinder().llSearchView);

                        break;
                    case R.id.inviteUsers:
                        Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.app_name))
                                .setMessage("Hi, I am inviting you to install and use Lecture Verb App.")
                                .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))
                                //.setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
                                .setCallToActionText("Check Now")
                                .build();
                        startActivityForResult(intent, Constants.RequestCode.FIREBASE_INVITE);
                        break;
                    case R.id.peopleYouMayKnow:
                        viewPager.setCurrentItem(0);
                        break;
                    case R.id.liveUserFinder:
                        viewPager.setCurrentItem(1);
                        break;
                }
                return true;
            }
        });

        popup.show(); //showing popup menu
    }

    private void displayLocationDialog() {
        if (!UtillsG.isGpsEnabled(getActivity())) {
            UtillsG.displayLocationSettingsRequest(ConnectTabFragment.this);
        }
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.mipmap.ic_add_person);
//        tabLayout.getTabAt(1).setIcon(R.mipmap.ic_refresh);
        tabLayout.getTabAt(1).setIcon(R.mipmap.ic_wifi);
//        tabLayout.getTabAt(3).setIcon(R.mipmap.ic_more);

    }

}