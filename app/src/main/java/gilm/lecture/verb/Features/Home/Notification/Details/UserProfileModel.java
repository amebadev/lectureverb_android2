package gilm.lecture.verb.Features.Home.Notification.Details;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * created by PARAMBIR SINGH on 31/10/17.
 */

public class UserProfileModel extends BasicApiModel
{

    public data data;

    public UserProfileModel.data getData() {
        return data;
    }

    public void setData(UserProfileModel.data data) {
        this.data = data;
    }

    public class data
    {
        private UserDataModel.UserData Userdata;

        public UserDataModel.UserData getUserdata() {
            return Userdata;
        }

        public void setUserdata(UserDataModel.UserData userdata) {
            Userdata = userdata;
        }
    }
}
