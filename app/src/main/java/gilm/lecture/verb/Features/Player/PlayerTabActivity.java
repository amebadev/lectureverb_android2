/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Player;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Home.Home.LectureModel;
import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Player.CommentsDiscussion.CommentDiscussionTabFragment;
import gilm.lecture.verb.Features.TabAdapter.FragmentTabModel;
import gilm.lecture.verb.Features.TabAdapter.SectionsPagerAdapter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;

public class PlayerTabActivity extends AppCompatActivity
{
    public static void start(Context context, Lecture_And_Event_Model lectureModel) {
       /* Intent starter = new Intent(context, PlayerTabActivity.class);
        starter.putExtra(Constants.Extras.DATA, lectureModel);
        context.startActivity(starter);*/

        PlayerActivity.start(context, lectureModel, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_tab);

        ViewPager viewPager = (ViewPager) findViewById(R.id.container);

        List<FragmentTabModel> list = new ArrayList<>();
        list.add(new FragmentTabModel(PlayerFragment_notUsed.newInstance((LectureModel) getIntent().getSerializableExtra(Constants.Extras.DATA)), ""));
        list.add(new FragmentTabModel(CommentDiscussionTabFragment.newInstance(), ""));


        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager(), list);
        viewPager.setAdapter(adapter);

    }
}
