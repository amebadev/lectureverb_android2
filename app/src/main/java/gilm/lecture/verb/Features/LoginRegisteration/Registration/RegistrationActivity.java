package gilm.lecture.verb.Features.LoginRegisteration.Registration;

import android.content.Context;
import android.content.Intent;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.LoginRegisteration.EmailVerification.EmailVerificationActivity;
import gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.databinding.ActivityRegisterationBinding;

public class RegistrationActivity extends BaseActivity<ActivityRegisterationBinding, RegistrationPresenter> implements RegistrationView
{

    public static void start(Context context)
    {
        Intent starter = new Intent(context, RegistrationActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId()
    {
        return R.layout.activity_registeration;
    }

    @Override
    protected void onCreateActivityG()
    {
        injectPresenter(new RegistrationPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews()
    {
        setProgressTitle("Creating new account");
        setupToolbar("");
        getDataBinder().setData(getPresenter());

    }

    @Override
    public Context getActivityG()
    {
        return RegistrationActivity.this;
    }

    @Override
    public void openNavigationActivity()
    {
        NavigationActivity.start(getActivityG());
    }

    @Override
    public void openEmailVerification()
    {
        EmailVerificationActivity.start(getActivityG());
    }
}
