package gilm.lecture.verb.Features.OtherProfile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Home.Notification.Details.UserProfileModel;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.MyProfile.View.MyProfileFragment;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Navigator;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harpreet on 10/24/17.
 */

public class ActivityOtherUserDetails extends AppCompatActivity implements View.OnClickListener {

    boolean is_following, Is_approved, isOtherUserConnected;

    TextView txtvAccept, txtvReject;
    Context context;

    UserDataModel.UserData userData;
    CardView cardFollowRequest;

    String notificationId = "";

    public static void start(Context context, UserDataModel.UserData mUserData, Boolean isOtherUserConnected) {
        Intent starter = new Intent(context, ActivityOtherUserDetails.class);
        starter.putExtra("userDetails", mUserData);
        starter.putExtra("isOtherUserConnected", isOtherUserConnected);
        context.startActivity(starter);
    }

    public static void start(Context context, String user_id, String screenTitle) {
        getProfile_StartActivity(context, user_id, screenTitle, "");
    }

    /**
     * We need to clear notification from notification list so we are execute an api to clear notification in case of follow unfollow
     *
     * @param context
     * @param user_id
     * @param screenTitle
     * @param notificationId
     */
    public static void startForFollow(Context context, String user_id, String screenTitle, String notificationId) {
        getProfile_StartActivity(context, user_id, screenTitle, notificationId);
    }

    /**
     * Method to get all profile details before performing intent
     *
     * @param context
     * @param user_id
     * @param screenTitle
     */
    private static void getProfile_StartActivity(final Context context, String user_id, final String screenTitle, final String notificationId) {
        UtillsG.showLoading("Loading...", context);
        Call<UserProfileModel> userProfileApi = LVApplication.getRetrofit().create(HomeApis.class).get_user_profile(new SharedPrefHelper(context).getUserId(), user_id);
        userProfileApi.enqueue(new Callback<UserProfileModel>() {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response) {
                UtillsG.hideLoading();
                if (response.body().getStatus()) {
                    UserDataModel.UserData userProfileModel = response.body().getData().getUserdata();

                    Intent starter = new Intent(context, ActivityOtherUserDetails.class);
                    starter.putExtra("userDetails", userProfileModel);
                    starter.putExtra("isOtherUserConnected", userProfileModel.is_following() && userProfileModel.isIs_approved());
                    starter.putExtra("is_following", userProfileModel.is_following());
                    starter.putExtra("Is_approved", userProfileModel.isIs_approved());
                    starter.putExtra("screenTitle", screenTitle);
                    starter.putExtra("notificationId", notificationId);
                    context.startActivity(starter);
                }
            }

            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t) {
                UtillsG.hideLoading();
            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_other_profile);
        context = this;

        userData = (UserDataModel.UserData) getIntent().getSerializableExtra("userDetails");
        notificationId = getIntent().getStringExtra("notificationId");
        showHideFollowRequest();

        cardFollowRequest = (CardView) findViewById(R.id.cardFollowRequest);
        txtvAccept = (TextView) findViewById(R.id.txtvAccept);
        txtvReject = (TextView) findViewById(R.id.txtvReject);
        txtvAccept.setOnClickListener(this);
        txtvReject.setOnClickListener(this);


        isOtherUserConnected = getIntent().getBooleanExtra("isOtherUserConnected", true);
        is_following = getIntent().getBooleanExtra("is_following", true);
        Is_approved = getIntent().getBooleanExtra("Is_approved", true);


        setupToolbar(UtillsG.getNotNullString(getIntent().getStringExtra("screenTitle"), "User Profile"));
        new Navigator().replaceFragment(this, R.id.container, new MyProfileFragment((UserDataModel.UserData) getIntent().getSerializableExtra("userDetails")
                , isOtherUserConnected, is_following, Is_approved));
    }

    private void showHideFollowRequest() {
        UtillsG.showLoading("Please wait..", context);
        Call<FollowStatusModel> call = LVApplication.getRetrofit().create(ProfileApi.class).get_follow_status(new SharedPrefHelper(context).getUserId(), userData.getUser_id());
        call.enqueue(new Callback<FollowStatusModel>() {
            @Override
            public void onResponse(Call<FollowStatusModel> call, Response<FollowStatusModel> response) {
                UtillsG.hideLoading();
                if (response.body() != null && response.body().getStatus()) {
                    if (response.body().getData().getIs_approved().equalsIgnoreCase("0")) {
                        cardFollowRequest.setVisibility(View.VISIBLE);
                    } else {
                        cardFollowRequest.setVisibility(View.GONE);
                    }
                } else {
                    cardFollowRequest.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<FollowStatusModel> call, Throwable t) {
                UtillsG.hideLoading();
                Log.e("Error occured", t.getMessage().toString());
                cardFollowRequest.setVisibility(View.GONE);
            }
        });
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.title);
        if (viewById != null) {
            viewById.setText(title);
            getSupportActionBar().setTitle("");
        } else {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtvAccept:
                acceptRejectRequest(true);
                break;
            case R.id.txtvReject:
                acceptRejectRequest(false);
                break;
        }
    }

    private void acceptRejectRequest(final boolean isAccepted) {
        UtillsG.showLoading("Please wait..", context);
        Call<BasicApiModel> getFollowersList = LVApplication.getRetrofit().create(ProfileApi.class).approve_reject_follow_request(new SharedPrefHelper(context).getUserId(), userData.getUser_id(), String.valueOf(isAccepted));
        getFollowersList.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                UtillsG.hideLoading();
                if (response.body().getStatus()) {
                    UtillsG.showToast(isAccepted ? "Request accepted." : "Request rejected.", context, true);
                    cardFollowRequest.setVisibility(View.GONE);
                    if (notificationId != null && !notificationId.isEmpty()) {
                        executeDeleteNotification();
                    }
                } else {
                    UtillsG.showToast(response.body().getMessage(), context, true);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                UtillsG.showToast(t.getMessage(), context, true);
                UtillsG.hideLoading();
            }
        });
    }

    private void executeDeleteNotification() {
        Call<BasicApiModel> deleteNotification = LVApplication.getRetrofit().create(ProfileApi.class).delete_notification(notificationId);
        deleteNotification.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                Log.e("DELETE NOTIFICATION :", response.body().toString()+"");
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
            }
        });
    }


}

