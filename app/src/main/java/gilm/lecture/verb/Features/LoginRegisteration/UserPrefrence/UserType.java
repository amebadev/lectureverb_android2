package gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence;

import android.support.annotation.StringDef;

/**
 * Created by G-Expo on 05 Jul 2017.
 */
// LV


@StringDef({UserType.STUDENT, UserType.NON_STUDENT, UserType.LECTURER, UserType.GUEST})
public @interface UserType
{
    String LECTURER    = "2";
    String STUDENT     = "3";
    String GUEST       = "4";
    String NON_STUDENT = "5";
}
