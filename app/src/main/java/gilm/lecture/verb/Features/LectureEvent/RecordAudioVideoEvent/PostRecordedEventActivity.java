package gilm.lecture.verb.Features.LectureEvent.RecordAudioVideoEvent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import gilm.lecture.verb.Features.Discovery.DiscoveryActivity;
import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.InterestsSelection.AddInterests.AddInterestCategoriesActivity;
import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.Internal.Base.EmptyPresenter;
import gilm.lecture.verb.Features.Internal.Base.EmptyView;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EventLectureApi;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsApi;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence.UserType;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureApi;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureModel;
import gilm.lecture.verb.Features.QuickLecture.ThumbnailResponseModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.BitmapDecoderG;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import gilm.lecture.verb.WebServices.Web;
import gilm.lecture.verb.databinding.ActivityPostRecordedEventBinding;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostRecordedEventActivity extends BaseActivity<ActivityPostRecordedEventBinding, EmptyPresenter> implements EmptyView, View.OnClickListener, ProgressRequestBody.UploadCallbacks {

    String videoPath = "";
    private int currentPosition;
    LectureDetailsModel lectureDetailsModel;
    private String commaSeparatedIds = "";
    private String commaSeparatedNames = "";
    private File mArtWorkUrl;
    File mArtWorkUrl_updated;

    public static void start(Context context, String videoPath, LectureDetailsModel lectureDetailsModel) {
        Intent starter = new Intent(context, PostRecordedEventActivity.class);
        starter.putExtra("videoPath", videoPath);
        starter.putExtra(Constants.Extras.DATA, lectureDetailsModel);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_post_recorded_event;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new EmptyPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        setupToolbar("Post Event");

        videoPath = getIntent().getStringExtra("videoPath");
        lectureDetailsModel = (LectureDetailsModel) getIntent().getSerializableExtra(Constants.Extras.DATA);

        getDataBinder().imageButton.setOnClickListener(this);
        getDataBinder().txtvArtWork.setOnClickListener(this);
        getDataBinder().imgvFullScreen.setOnClickListener(this);
        getDataBinder().txtvAddPresentationDiscoveryUrl.setOnClickListener(this);
        getDataBinder().fabUpload.setOnClickListener(this);
        getDataBinder().edTitle.setText(lectureDetailsModel.getDetails());

/*
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(getDataBinder().frameVideo.getHeight(), getDataBinder().frameVideo.getHeight());
        getDataBinder().videoView.set*/

        setCategoryNames();

        Uri uri = Uri.parse(videoPath);
        if (uri != null) {
            getDataBinder().videoView.setVideoURI(uri);

            getDataBinder().videoView.start();
            getDataBinder().videoView.pause();

            getDataBinder().videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    getDataBinder().imageButton.setImageResource(R.mipmap.ic_play);
                }
            });

            Bitmap bitmap2 = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MICRO_KIND);
            getDataBinder().imgvThumbnail.setImageBitmap(bitmap2);

            mArtWorkUrl = new File(Environment.getExternalStorageDirectory().toString() + File.separator + Web.LocalFolders.LectureVerbThumbs + System.currentTimeMillis() + ".png");

            FileOutputStream out = null;
            try {
                out = new FileOutputStream(mArtWorkUrl);
                bitmap2.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setCategoryNames() {
        ArrayList<ChildCategoryModel> alCategories = lectureDetailsModel.getEvent_interest();
        if (alCategories != null) {
            for (int i = 0; i < alCategories.size(); i++) {

                commaSeparatedIds = commaSeparatedIds + (commaSeparatedIds.isEmpty() ? ""
                        : ",") + alCategories.get(i).getCategory_id();
                commaSeparatedNames = commaSeparatedNames + (
                        commaSeparatedNames.isEmpty() ? ""
                                : ", ") + alCategories.get(i).getCategory_name();
            }
            getDataBinder().txtvLectureCategory.setText(commaSeparatedNames);
        }
        getDataBinder().txtvLectureCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddInterestCategoriesActivity.start(getActivityG(), true, commaSeparatedIds, commaSeparatedNames);
            }
        });

    }

    @Override
    public Context getActivityG() {
        return PostRecordedEventActivity.this;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageButton:
                if (getDataBinder().videoView.isPlaying()) {
                    currentPosition = getDataBinder().videoView.getCurrentPosition();
                    getDataBinder().videoView.pause();
                    getDataBinder().imageButton.setImageResource(R.mipmap.ic_play);
                } else {
                    getDataBinder().videoView.start();
                    getDataBinder().videoView.seekTo(currentPosition);
                    getDataBinder().imageButton.setImageResource(R.drawable.ic_pause);
                }
                break;
            case R.id.imgvFullScreen:
                UtillsG.showFullSizeVideo(videoPath, getActivityG());
                break;
            case R.id.txtvAddPresentationDiscoveryUrl:
                DiscoveryActivity.start(this);
                break;
                case R.id.txtvArtWork:
                BitmapDecoderG.selectImage(getActivityG(), null);

                break;
            case R.id.fabUpload:
                if (getDataBinder().edTitle.getText().toString().trim().isEmpty()) {
                    UtillsG.showToast("Please enter title.", getActivityG(), true);
                }
                else if (mArtWorkUrl_updated == null) {
                   displayError("Please select artwork thumbnail.");
                }
                    else if (getDataBinder().txtvLectureCategory.getText().toString().trim().isEmpty()) {
                    UtillsG.showToast("Please select category.", getActivityG(), true);
                } else {
                    DialogHelper.getInstance().showWith2Action(
                            getActivityG(),
                            "Start Uploading",
                            "Cancel",
                            getResources().getString(R.string.app_name),
                            "Please confirm to start uploading in background. Do not kill the app while uploading otherwise your recorded data will be lost. You will be notified when the uploading is completed.",
                            new CallBackG<String>() {
                                @Override
                                public void onCallBack(String output) {
                                    LVApplication app = (LVApplication) getApplication();
                                    app.uploadEventInBackground(
                                            videoPath,
                                            getDataBinder().edTitle.getText().toString(),
                                            getDataBinder().txtvAddPresentationDiscoveryUrl.getText().toString(),
                                            commaSeparatedIds,
                                            mArtWorkUrl_updated,
                                            lectureDetailsModel
                                    );
                                    markEventAsCompleted();
                                }
                            });

//                    uploadToServer();
                }
                break;
        }
    }

    private void markEventAsCompleted() {
        showLoading("Please wait..");
        Call<BasicApiModel> markEventApi = LVApplication.getRetrofit().create(LectureEventsApi.class)
                .mark_event_completed(lectureDetailsModel.getEvent_id());
        markEventApi.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                hideLoading();
                if (response.body() != null && response.body().getStatus()) {
                    finish();
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                hideLoading();
            }
        });
    }

    private void uploadToServer() {
        showLoading("Uploading ...");
        Call<QuickLectureModel> quickLectureApiModelCall = getRetrofitInstance().uploadAudioLecture(
                getFilePart(new File(videoPath), "video"),
                getOtherParams("video"),
                getOtherParams(new SharedPrefHelper(getActivityG()).getUserId()),
                getOtherParams(getDataBinder().edTitle.getText().toString()),
                getOtherParams(getDataBinder().txtvAddPresentationDiscoveryUrl.getText().toString()),
                getOtherParams("Public")
                , getOtherParams("")
                , getOtherParams(new SharedPrefHelper(getActivityG()).getUserName())
                , getOtherParams("")
                , getOtherParams(lectureDetailsModel.getLecture_poster())
                , getOtherParams("0"));
        quickLectureApiModelCall.enqueue(new Callback<QuickLectureModel>() {
            @Override
            public void onResponse(Call<QuickLectureModel> call, final Response<QuickLectureModel> response) {


                if (response.body() != null) {

                    if (response.body().getStatus()) {

                        Call<ThumbnailResponseModel> lectureThumbnailApiModelCall = getRetrofitInstance().uploadLectureThumbnail(
                                getFilePart(mArtWorkUrl, "thumbnail"),
                                getOtherParams(response.body().getmQuickLectureData().getQuick_lecture_id()),
                                getUploadTypePart());
                        lectureThumbnailApiModelCall.enqueue(new Callback<ThumbnailResponseModel>() {
                            @Override
                            public void onResponse(Call<ThumbnailResponseModel> thumbnailApiCall, Response<ThumbnailResponseModel> thumbnailApiResponse) {
                                hideLoading();

                                if (thumbnailApiResponse.body() != null) {

                                    if (thumbnailApiResponse.body().getStatus()) {
//                                                UtillsG.deleteThisFile(getIntent().getStringExtra("artWorkImage"));
//                                                UtillsG.deleteThisFile(getIntent().getStringExtra(Constants.Extras.DATA));
//                                                UtillsG.deleteThisFile(mArtWorkUrl.getAbsolutePath());

                                        updateLectureEventInterest(response.body().getmQuickLectureData().getQuick_lecture_id());

                                    } else {
                                        displayError("Error in creating quick lecture");
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ThumbnailResponseModel> thumbnailApiCall, Throwable t) {
                                hideLoading();
                                Log.e("Error", t.toString());
                            }
                        });
                    } else {
                        hideLoading();
                    }
                } else {
                    hideLoading();
                }
            }

            @Override
            public void onFailure(Call<QuickLectureModel> call, Throwable t) {
                Log.e("Error", t.toString());
                UtillsG.showToast("Error in creating quick lecture = " + t.toString(), getActivityG(), true);
                hideLoading();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
           if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    File file = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "audioLectureThumbnail.png");
                    FileOutputStream out = null;
                    try {
                        if (bitmap != null) {
                            out = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (out != null) {
                                out.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    getDataBinder().txtvArtWork.setText(file.getAbsolutePath());
                    mArtWorkUrl_updated=file;
                }
            }
            else if (requestCode == Constants.RequestCode.INTEREST_SELECTION) {
                commaSeparatedIds = "";
                commaSeparatedNames = "";
                ArrayList<ChildCategoryModel> alCategories = (ArrayList<ChildCategoryModel>) data.getSerializableExtra(Constants.Extras.DATA);
                for (int i = 0; i < alCategories.size(); i++) {
                    commaSeparatedIds = commaSeparatedIds + (commaSeparatedIds.isEmpty() ? ""
                            : ",") + alCategories.get(i).getId();
                    commaSeparatedNames = commaSeparatedNames + (
                            commaSeparatedNames.isEmpty() ? ""
                                    : ", ") + alCategories.get(i).getCategory();
                }
                getDataBinder().txtvLectureCategory.setText(commaSeparatedNames);
            } else if (requestCode == Constants.RequestCode.DISCOVERY) {
                getDataBinder().txtvAddPresentationDiscoveryUrl.setText(data.getStringExtra(Constants.Extras.DATA));
            }
            else {
               cropMeImage(BitmapDecoderG.onActivityResult(getActivityG(), requestCode, resultCode, data));

           }
        }
    }

    private MultipartBody.Part getFilePart(final File file, String fileType) {
        ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(fileType, file.getName(), fileBody);
        return filePart;
    }

    private RequestBody getUploadTypePart() {
        return RequestBody.create(MediaType.parse("text/plain"), "upload");
    }


    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }


    private QuickLectureApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(QuickLectureApi.class);
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    LectureEventClicks mClicks = new LectureEventClicks();

    public void updateLectureEventInterest(final String quickLectureId) {
        mClicks.setLectureInterests((Activity) getActivityG(), LVApplication.getRetrofit().create(EventLectureApi.class), quickLectureId,
                getIntent().getStringExtra("lectureCategory"), "lecture", new CallBackG<Boolean>() {
                    @Override
                    public void onCallBack(Boolean output) {
                        hideLoading();

                        UtillsG.showToast("Quick lecture created successfully", getActivityG(), true);
                        EventBus.getDefault().post(new HomeFragmentUpdateBus(true));
                        if (new SharedPrefHelper(getActivityG()).getUserType().equalsIgnoreCase(UserType.LECTURER)) {
                            UtillsG.showInterstitialAds(getActivityG(),getResources().getString(R.string.userAppId),getResources().getString(R.string.userAdUnitQuickPresent));
                        }
                        else {
                            UtillsG.showInterstitialAds(getActivityG(),getResources().getString(R.string.userAppId),getResources().getString(R.string.nonStudentAdUnitDiscoveryupload));

                        }   finish();

                    }
                });
    }
    public void cropMeImage(Uri uri) {
        CropImage.activity(uri)
                .setAspectRatio(16, 9)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start((Activity)getActivityG());
    }
}
