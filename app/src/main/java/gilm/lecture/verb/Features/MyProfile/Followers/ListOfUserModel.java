package gilm.lecture.verb.Features.MyProfile.Followers;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * created by PARAMBIR SINGH on 10/10/17.
 */

public class ListOfUserModel extends BasicApiModel
{
    @SerializedName("data")
    List<UserDataModel.UserData> usersList = new ArrayList<>();

    public List<UserDataModel.UserData> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<UserDataModel.UserData> usersList) {
        this.usersList = usersList;
    }
}
