package gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.net.Uri;

import java.util.ArrayList;
import java.util.Collection;

import gilm.lecture.verb.BR;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.CoHostListModel;
import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;

/**
 * Created by G-Expo on 07 Jul 2017.
 */

public class CreateEditLectureEventViewModel extends BaseObservable
{
    private String Title, detail;
    private Uri     selectedImage;
    private boolean isLive,isGuestInvited,lectureEventIsPublic;
    private String    startDate, endDate,startTime,endTime;
    private String locationG;
    private Collection<String> lectureTags;
    private CoHostListModel.SingleCoHostData selectedCoHost;
    ArrayList<ChildCategoryModel> alCategories=new ArrayList<>();

    public ArrayList<ChildCategoryModel> getAlCategories() {
        return alCategories;
    }

    public void setAlCategories(ArrayList<ChildCategoryModel> alCategories) {
        this.alCategories = alCategories;
    }

    public CreateEditLectureEventViewModel()
    {
    }

    @Bindable
    public Collection<String> getLectureTags()
    {
        return lectureTags;
    }

    public void setLectureTags(Collection<String> lectureTags)
    {
        this.lectureTags = lectureTags;
        notifyPropertyChanged(BR.lectureTags);
    }

    @Bindable
    public boolean getLectureEventIsPublic()
    {
        return lectureEventIsPublic;
    }

    public void setLectureEventIsPublic(boolean lectureEventIsPublic)
    {
        this.lectureEventIsPublic = lectureEventIsPublic;
        notifyPropertyChanged(BR.lectureEventIsPublic);
    }

    @Bindable
    public Uri getSelectedImage()
    {
        return selectedImage;
    }

    public void setSelectedImage(Uri selectedImage)
    {
        this.selectedImage = selectedImage;
        notifyPropertyChanged(BR.selectedImage);
    }

    @Bindable
    public String getTitle()
    {
        return Title;
    }

    public void setTitle(String title)
    {
        Title = title;
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public boolean getIsLive()
    {
        return isLive;
    }

    public void setIsLive(boolean isLive)
    {
        this.isLive = isLive;
        notifyPropertyChanged(BR.isLive);
    }

    @Bindable
    public String getDetail()
    {
        return detail;
    }

    public void setDetail(String detail)
    {
        this.detail = detail;
        notifyPropertyChanged(BR.detail);
    }

    @Bindable
    public String getStartDate()
    {
        return startDate;
    }

    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
        notifyPropertyChanged(BR.startDate);
    }

    @Bindable
    public String getEndDate()
    {
        return endDate;
    }

    public void setEndDate(String endDate)
    {
        this.endDate = endDate;
        notifyPropertyChanged(BR.endDate);
    }

    @Bindable
    public String getStartTime()
    {
        return startTime;
    }

    public void setStartTime(String startTime)
    {
        this.startTime = startTime;
        notifyPropertyChanged(BR.startTime);
    }

    @Bindable
    public String getEndTime()
    {
        return endTime;
    }

    public void setEndTime(String endTime)
    {
        this.endTime = endTime;
        notifyPropertyChanged(BR.endTime);
    }

    @Bindable
    public String getLocationG()
    {
        return locationG;
    }

    public void setLocationG(String locationG)
    {
        this.locationG = locationG;
        notifyPropertyChanged(BR.locationG);
    }


    @Bindable
    public boolean getIsGuestInvited()
    {
        return isGuestInvited;
    }

    public void setIsGuestInvited(boolean isGuestInvited)
    {
        this.isGuestInvited = isGuestInvited;
        notifyPropertyChanged(BR.isGuestInvited);
    }

    @Bindable
    public CoHostListModel.SingleCoHostData getSelectedCoHost()
    {
        return selectedCoHost;
    }

    public void setSelectedCoHost(CoHostListModel.SingleCoHostData selectedCoHost)
    {
        this.selectedCoHost = selectedCoHost;
        notifyPropertyChanged(BR.selectedCoHost);
    }

}
