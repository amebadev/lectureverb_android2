/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.UserGroups.GroupList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence.UserType;
import gilm.lecture.verb.Features.UserGroups.CreateNewGroupAndSignUp.CreateGroupAndSignUpActivity;
import gilm.lecture.verb.Features.UserGroups.GroupListModel;
import gilm.lecture.verb.Features.UserGroups.GroupModel;
import gilm.lecture.verb.Features.UserGroups.JoinGroup.AllGroupsListActivity;
import gilm.lecture.verb.Features.UserGroups.UserGroupApis;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.EndlessRecyclerOnScrollListener;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupListFragment extends Fragment implements View.OnClickListener {
    RecyclerView recyclerView;
    private View view;
    ProgressBar progressBar;
    FloatingActionButton fabAddMore;
    FrameLayout frameAddGroup;
    ImageView imgvCreateGroup, imgvJoinGroup;
    private TextView txtvCreate;

    ArrayList<GroupModel> aldata = new ArrayList<>();

    int page = 1;

    EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    private LinearLayoutManager linearLayMgr;
    private GroupListAdapter adapter;

    public GroupListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        page=1;
        endlessRecyclerOnScrollListener.reset();
        refreshData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_group_list, container, false);

        imgvJoinGroup = view.findViewById(R.id.imgvJoinGroup);
        imgvCreateGroup = view.findViewById(R.id.imgvCreateGroup);
        txtvCreate = view.findViewById(R.id.txtvCreate);
        fabAddMore = view.findViewById(R.id.fabAddMore);
        fabAddMore.setOnClickListener(this);
        imgvCreateGroup.setOnClickListener(this);
        txtvCreate.setOnClickListener(this);
        imgvJoinGroup.setOnClickListener(this);
        progressBar = view.findViewById(R.id.progressBar);

        linearLayMgr = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayMgr);

        frameAddGroup = view.findViewById(R.id.frameAddGroup);

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayMgr) {
            @Override
            public void onLoadMore(int current_page) {
                page++;
                refreshData();
            }

            @Override
            public void countChange(int currentVisibleCount) {

            }
        };
        recyclerView.setOnScrollListener(endlessRecyclerOnScrollListener);

        adapter = new GroupListAdapter(aldata, getActivity(), null);
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public static Fragment newInstance() {
        return new GroupListFragment();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabAddMore:

                if (frameAddGroup.getVisibility() == View.GONE) {
                    RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
                    anim.setInterpolator(new LinearInterpolator());
                    anim.setRepeatCount(0);
                    anim.setDuration(500);
                    fabAddMore.setAnimation(anim);
                    fabAddMore.startAnimation(anim);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            frameAddGroup.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            fabAddMore.setRotation(45);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                } else {
                    RotateAnimation anim = new RotateAnimation(360.0f, 0.0f, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
                    anim.setInterpolator(new LinearInterpolator());
                    anim.setRepeatCount(0);
                    anim.setDuration(300);
                    fabAddMore.setAnimation(anim);
                    fabAddMore.startAnimation(anim);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            frameAddGroup.setVisibility(View.GONE);
                            fabAddMore.setRotation(0);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                }

                break;

            case R.id.imgvCreateGroup:
                if (new SharedPrefHelper(getActivity()).getUserType().equals(UserType.LECTURER)) {
                    CreateGroupAndSignUpActivity.startForGroupOnly(getActivity());
                    fabAddMore.performClick();
                } else {
                    UtillsG.showToast("Only a lecturer can create a new user group.", getActivity(), true);
                }
                break;
            case R.id.imgvJoinGroup:
                AllGroupsListActivity.startForResult(GroupListFragment.this);
                fabAddMore.performClick();

                break;
        }
    }

    private void refreshData() {
        if (page == 1) {
            progressBar.setVisibility(View.VISIBLE);
        }

        Call<GroupListModel> call = LVApplication.getRetrofit().create(UserGroupApis.class).get_my_user_groups(new SharedPrefHelper(getActivity()).getUserId(), String.valueOf(page));
        call.enqueue(new Callback<GroupListModel>() {
            @Override
            public void onResponse(Call<GroupListModel> call, Response<GroupListModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body().getStatus()) {
                    if (page == 1) {
                        aldata.clear();
                    }
                    aldata.addAll(response.body().getData());
                    adapter.notifyDataSetChanged();
                } else {
                    UtillsG.showToast(response.body().getMessage(), getActivity(), true);
                }
            }

            @Override
            public void onFailure(Call<GroupListModel> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.e("ERROR GET GROUP LIST", t.toString() + "");
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.RequestCode.JOIN_GROUP) {
                final GroupModel groupModel = (GroupModel) data.getSerializableExtra(Constants.Extras.DATA);
                joinGroupNow(groupModel);
            }
        }
    }

    private void joinGroupNow(final GroupModel groupModel) {
        Call<BasicApiModel> call = LVApplication.getRetrofit().create(UserGroupApis.class).join_group(new SharedPrefHelper(getActivity()).getUserId(), groupModel.getId());
        call.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body().getStatus()) {
                    DialogHelper.getInstance().showInformation(getActivity(), "You have been successfully joined group \"" + groupModel.getGroup_title() + "\".", new CallBackG<String>() {
                        @Override
                        public void onCallBack(String output) {
                            refreshData();
                        }
                    });
                } else {
                    UtillsG.showToast(response.body().getMessage(), getActivity(), true);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("ERROR GET GROUP LIST", t.toString() + "");
            }
        });
    }
}
