package gilm.lecture.verb.Features.MyProfile.View.Post;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.Features.LectureEvent.Hosting.HostView;
import gilm.lecture.verb.Features.LectureEvent.Hosting.HostingFragment;
import gilm.lecture.verb.Features.LectureEvent.Hosting.HostingPresenter;
import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsAdapter;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.databinding.FragmentRecyclerlistBinding;

/**
 * Created by harpreet on 9/27/17.
 */

public class PostFragment extends BaseFragment<FragmentRecyclerlistBinding, PostPresenter> implements PostView, InfiniteAdapterG.OnLoadMoreListener
{
    int page = 1;
    public ArrayList<LectureEventViewModel> coHostList = new ArrayList<>();
    LectureEventsAdapter invitationAdapter;
    public static PostFragment newInstance()
    {
        return new PostFragment();
    }

    public PostFragment()
    {
// Required empty public constructor
    }

    @Override
    protected int getLayoutId()
    {
        return R.layout.fragment_recyclerlist;
    }

    @Override
    protected void onCreateFragmentG()
    {
        injectPresenter(new PostPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews()
    {
        getDataBinder().setIsLoading(getPresenter().getIsLoadingViewModel());

        invitationAdapter = new LectureEventsAdapter(coHostList,getPresenter(),getActivityG());
        invitationAdapter.setOnLoadMoreListener(this);
        getDataBinder().reviewsList.setAdapter(invitationAdapter);
        invitationAdapter.setShouldLoadMore(false);

        getPresenter().loadData(page);


    }



    @Override
    public void onLoadMore()
    {
        page++;
        getPresenter().loadData(page);
    }


    @Override
    public void showData(ArrayList<LectureEventViewModel> list,int pageNo) {
        if(pageNo==1){
            coHostList.clear();
        }
        coHostList.addAll(list);
        invitationAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void RefreshData(HomeFragmentUpdateBus data)
    {
        if(getView()!=null) {
            page = 1;
            getPresenter().loadData(page);
        }
    }

}
