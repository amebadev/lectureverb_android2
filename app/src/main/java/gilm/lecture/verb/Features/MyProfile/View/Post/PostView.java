package gilm.lecture.verb.Features.MyProfile.View.Post;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;
import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;

/**
 * Created by harpreet on 9/27/17.
 */

public interface PostView extends Viewable<PostPresenter>
{
        void showData(ArrayList<LectureEventViewModel> list, int pageNo);
}
