package gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.UtillsG;

public class TextLectureCommentsActivity extends AppCompatActivity
{

    private static Lecture_And_Event_Model model = new Lecture_And_Event_Model();

    public static void start(Context context, Lecture_And_Event_Model model) {
        TextLectureCommentsActivity.model = model;
        Intent starter = new Intent(context, TextLectureCommentsActivity.class);
        starter.putExtra(Constants.Extras.DATA, model);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_lecture_comments);

        model = (Lecture_And_Event_Model) getIntent().getSerializableExtra(Constants.Extras.DATA);

        setupToolbar(UtillsG.getNotNullString(model.getQuick_lecture_text(), "Comments"));

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frameLayout, CommentsFragment.newInstance(model));
        transaction.commit();
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.toolbar_title);

        if (viewById != null) {
            viewById.setText(title);
        }
        else {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
