package gilm.lecture.verb.Features.LectureEvent.LectureEventsModels;

import java.io.Serializable;
import java.util.ArrayList;

import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost.CoHostListModel;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;

/**
 * Created by harpreet on 9/22/17.
 */

public class LectureDetailsModel implements Serializable {

    private String group_id;
    private int is_completed;
    private String event_id;
    private UserDataModel.UserData lecturer_data;
    private String user_id;
    private String lecturer_id;
    private String lecture_title;
    private String lecture_poster;
    private String recording_type;
    private String address;
    private String latitude;
    private String longitude;
    private String details;
    private String can_invite_friends;
    private ArrayList<CoHostListModel.SingleCoHostData> lecturer_co_hosts;
    private String lecture_subject_tags;
    private String isPublic;
    private String lecture_start_date_time;
    private String lecture_end_date_time;
    private String lecture_created_date_time;
    private CoHostListModel.SingleCoHostData Userdata;
    private IsInterestedModel Isinterested = null;
    private ArrayList<ChildCategoryModel> event_interest;

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public int getIs_completed() {
        return is_completed;
    }

    public void setIs_completed(int is_completed) {
        this.is_completed = is_completed;
    }

    public String getLecturer_id() {
        return lecturer_id;
    }

    public void setLecturer_id(String lecturer_id) {
        this.lecturer_id = lecturer_id;
    }

    public ArrayList<ChildCategoryModel> getEvent_interest() {
        return event_interest;
    }

    public void setEvent_interest(ArrayList<ChildCategoryModel> event_interest) {
        this.event_interest = event_interest;
    }

    public UserDataModel.UserData getLecturer_data() {
        return lecturer_data;
    }

    public void setLecturer_data(UserDataModel.UserData lecturer_data) {
        this.lecturer_data = lecturer_data;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }


    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getLecture_title() {
        return lecture_title;
    }

    public void setLecture_title(String lecture_title) {
        this.lecture_title = lecture_title;
    }

    public String getLecture_poster() {
        return lecture_poster;
    }

    public void setLecture_poster(String lecture_poster) {
        this.lecture_poster = lecture_poster;
    }

    public String getRecording_type() {
        return recording_type;
    }

    public void setRecording_type(String recording_type) {
        this.recording_type = recording_type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getCan_invite_friends() {
        return can_invite_friends;
    }

    public void setCan_invite_friends(String can_invite_friends) {
        this.can_invite_friends = can_invite_friends;
    }

    public ArrayList<CoHostListModel.SingleCoHostData> getLecturer_co_hosts() {
        return lecturer_co_hosts;
    }

    public void setLecturer_co_hosts(ArrayList<CoHostListModel.SingleCoHostData> lecturer_co_hosts) {
        this.lecturer_co_hosts = lecturer_co_hosts;
    }

    public String getLecture_subject_tags() {
        return lecture_subject_tags;
    }

    public void setLecture_subject_tags(String lecture_subject_tags) {
        this.lecture_subject_tags = lecture_subject_tags;
    }

    public String getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(String isPublic) {
        this.isPublic = isPublic;
    }

    public String getLecture_start_date_time() {
        return lecture_start_date_time;
    }

    public void setLecture_start_date_time(String lecture_start_date_time) {
        this.lecture_start_date_time = lecture_start_date_time;
    }

    public String getLecture_end_date_time() {
        return lecture_end_date_time;
    }

    public void setLecture_end_date_time(String lecture_end_date_time) {
        this.lecture_end_date_time = lecture_end_date_time;
    }

    public String getLecture_created_date_time() {
        return lecture_created_date_time;
    }

    public void setLecture_created_date_time(String lecture_created_date_time) {
        this.lecture_created_date_time = lecture_created_date_time;
    }

    public CoHostListModel.SingleCoHostData getUserdata() {
        return Userdata;
    }

    public void setUserdata(CoHostListModel.SingleCoHostData userdata) {
        Userdata = userdata;
    }

    public IsInterestedModel getmIsInterested() {
        return Isinterested;
    }

    public void setmIsInterested(IsInterestedModel Isinterested) {
        this.Isinterested = Isinterested;
    }
}