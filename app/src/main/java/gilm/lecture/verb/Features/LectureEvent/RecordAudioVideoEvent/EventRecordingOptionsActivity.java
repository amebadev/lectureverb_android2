package gilm.lecture.verb.Features.LectureEvent.RecordAudioVideoEvent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.view.View;

import com.afollestad.materialcamera.MaterialCamera;

import java.io.File;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.Internal.Base.EmptyPresenter;
import gilm.lecture.verb.Features.Internal.Base.EmptyView;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.Features.QuickLecture.Audio.RecordAudio.RecordingActivity;
import gilm.lecture.verb.Features.VideoTrimmer.VideoTrimmerActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.WebServices.Web;
import gilm.lecture.verb.databinding.ActivityEventRecordingOptionsBinding;
import life.knowledge4.videotrimmer.utils.FileUtils;

public class EventRecordingOptionsActivity extends BaseActivity<ActivityEventRecordingOptionsBinding, EmptyPresenter> implements EmptyView
{

    LectureDetailsModel mLectureViewModel;

    public static void start(Context context, LectureDetailsModel mLectureViewModel)
    {
        Intent starter = new Intent(context, EventRecordingOptionsActivity.class);
        starter.putExtra(Constants.Extras.DATA, mLectureViewModel);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId()
    {
        return R.layout.activity_event_recording_options;
    }

    @Override
    protected void onCreateActivityG()
    {
        injectPresenter(new EmptyPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews()
    {
        mLectureViewModel = (LectureDetailsModel) getIntent().getSerializableExtra(Constants.Extras.DATA);

        setupToolbar("Record Event");

        getDataBinder().layoutRecordPPT.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO: 02-Jun-18 Just uncomment whatever the option you want to use. No need to change onActivityResult
//                recordVideoUsingLibrary(EventRecordingOptionsActivity.this);
                recordVideoUsingDefaultCamera();
            }
        });
        getDataBinder().layoutRecordAudio.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//                UtillsG.showToast("Audio Recording is coming soon.", getActivityG(), true);
                RecordingActivity.startForEvent(getActivityG(), "record", mLectureViewModel);
            }
        });
    }

    @Override
    public Context getActivityG()
    {
        return EventRecordingOptionsActivity.this;
    }

    private void recordVideoUsingLibrary(Activity activity)
    {
        File saveFolder = new File(Environment.getExternalStorageDirectory() + File.separator + Web.LocalFolders.LectureVerbVideos);
        if (!saveFolder.exists())
        {
            if (!saveFolder.mkdirs())
            {
                throw new RuntimeException("Unable to create save directory, make sure WRITE_EXTERNAL_STORAGE permission is granted.");
            }
        }

        new MaterialCamera(activity)
                .saveDir(saveFolder)
                .qualityProfile(MaterialCamera.QUALITY_LOW)
                .showPortraitWarning(false)
                .audioDisabled(false)
                .start(Constants.RequestCode.VIDEO_CAMERA);
    }

    private void recordVideoUsingDefaultCamera()
    {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        File saveFolder = new File(Environment.getExternalStorageDirectory() + File.separator + Web.LocalFolders.LectureVerbVideos + "/event_video_" + System.currentTimeMillis() + ".mp4");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(saveFolder));
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        startActivityForResult(intent, Constants.RequestCode.VIDEO_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == Activity.RESULT_OK)
        {
            switch (requestCode)
            {
                case Constants.RequestCode.VIDEO_CAMERA:

                    final Uri selectedUri1 = data.getData();
                    if (selectedUri1 != null)
                    {
                        startTrimActivity(selectedUri1);
                    }
                    break;
                case Constants.RequestCode.VIDEO_TRIMMER:

                    String videoPath = data.getStringExtra("data");

                    PostRecordedEventActivity.start(getActivityG(), videoPath, mLectureViewModel);
                    finish();

                    break;
            }
        }
    }

    private void startTrimActivity(@NonNull Uri uri)
    {
        Intent intent = new Intent(getActivityG(), VideoTrimmerActivity.class);
        intent.putExtra(VideoTrimmerActivity.EXTRA_VIDEO_PATH, FileUtils.getPath(getActivityG(), uri));
        startActivityForResult(intent, Constants.RequestCode.VIDEO_TRIMMER);
    }
}