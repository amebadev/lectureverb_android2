package gilm.lecture.verb.Features.Player;

import com.piterwilson.audio.MP3RadioStreamPlayer;

/**
 * created by PARAMBIR SINGH on 3/10/17.
 */

public class PublicAudioPlayer
{
    public static MP3RadioStreamPlayer player;
    public static PublicAudioPlayer publicAudioPlayer;

    public PublicAudioPlayer() {
    }

    public static PublicAudioPlayer getInstance() {
        if (publicAudioPlayer == null) {
            publicAudioPlayer = new PublicAudioPlayer();
        }

        return publicAudioPlayer;
    }

    public static MP3RadioStreamPlayer getAudioPlayer() {
        return player;
    }

    public void initPlayer() {
        if (player == null) {
            player = new MP3RadioStreamPlayer();
        }
    }


    public static void setReleaseAudioPlayer() {
        if (player != null) {
            player.stop();
            player.release();
            player = null;
        }
    }
}
