package gilm.lecture.verb.Features.Discovery.DiscoveryRecorder_ComingFromPlayer;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Discovery.DiscoveryTabFragment;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.WebInnerStackModel;

/**
 * This activity is also using @{@link DiscoveryTabFragment} just like Navigation activity.
 */
public class DiscoveryActivity_HistoryStack extends AppCompatActivity
{

    private Fragment selectedFragment;

    public static void start(Context context, String startingUrl) {
        Intent starter = new Intent(context, DiscoveryActivity_HistoryStack.class);
        starter.putExtra(Constants.Extras.startingUrl, startingUrl);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discovery__history_stack);
        setupToolbar("Discovery");
        String startingUrl = getIntent().getStringExtra("startingUrl");

        selectedFragment = DiscoveryTabFragment.newInstance(startingUrl);

        FragmentManager fragmentManager = getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.layoutContainer, selectedFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.RequestCode.WEBVIEW_RECORDING) {
                String filePath = data.getStringExtra(Constants.Extras.DATA);
                String currentUrl = data.getStringExtra(Constants.Extras.currentUrl);
                String TITLE = data.getStringExtra(Constants.Extras.TITLE);
                ArrayList<WebInnerStackModel> alStack = (ArrayList<WebInnerStackModel>) data.getSerializableExtra(Constants.Extras.WEB_STACK);
                ((DiscoveryTabFragment) selectedFragment).onActivityResultWebViewRecorder(filePath, currentUrl, TITLE, alStack, data.getStringExtra("time"));
            }
            else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                if (selectedFragment instanceof DiscoveryTabFragment) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    Uri resultUri = result.getUri();
                    ((DiscoveryTabFragment) selectedFragment).onActivityResultImageCropper(resultUri);
                }
            }
        }
    }
}
