/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.LoginRegisteration.EmailVerification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity;
import gilm.lecture.verb.Features.Splash.SplashActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.databinding.ActivityEmailVerificationBinding;

public class EmailVerificationActivity extends BaseActivity<ActivityEmailVerificationBinding, EmailVerificationPresenter> implements EmailVerificationView
{

    boolean isForResult;

    public static void start(Context context) {
        Intent starter = new Intent(context, EmailVerificationActivity.class);
        context.startActivity(starter);
    }

    public static void startForResult(Context context) {
        Intent starter = new Intent(context, EmailVerificationActivity.class);
        starter.putExtra("isForResult", true);
        ((Activity) context).startActivityForResult(starter, Constants.RequestCode.EMAIL_VERIFICATION);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_email_verification;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new EmailVerificationPresenter());
        getPresenter().attachView(this);

        isForResult = getIntent().getBooleanExtra("isForResult", false);
    }

    @Override
    public void initViews() {
        setProgressTitle("Verifying email");

        getDataBinder().setData(getPresenter());

        getDataBinder().tvEmail.setText(String.format("For -%s", getLocalData().getEmail()));
        getDataBinder().otherAccount.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                if (isForResult) {
                    finish();
                }
                else {
                    getLocalData().logOut();
                    SplashActivity.start(getActivityG());
                }
            }
        });
    }

    @Override
    public Context getActivityG() {
        return EmailVerificationActivity.this;
    }

    @Override
    public void gotoHome() {
        if (!isForResult) {
            getLocalData().setIsVerified(true);
            getLocalData().setIsRegistered(true);
            NavigationActivity.start(getActivityG());
        }
        else {
            getLocalData().setIsVerified(true);
            getLocalData().setIsRegistered(true);

            Intent inn = new Intent();
            inn.putExtra("isSuccess", true);
            setResult(RESULT_OK, inn);
            finish();
        }
    }
}
