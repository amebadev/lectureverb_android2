/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Messaging.Chat;

import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.R;
import gilm.lecture.verb.databinding.InflatorChatBubbleBinding;

public class ChatAdapter extends InfiniteAdapterG<InflatorChatBubbleBinding>
{
    private List<MsgDataModel> dataList;

    public ChatAdapter(List<MsgDataModel> dList)
    {
        this.dataList = dList;
    }

    private final int MY_MSG    = 1;
    private final int OTHER_MSG = 2;

    @Override
    public int getCount()
    {
        return dataList.size();
    }

    @Override
    public int getViewType(int position)
    {
        if (dataList.get(position).getSender_id().equals("1")) {
            return MY_MSG;
        }
        else {
            return OTHER_MSG;
        }
    }

    @Override
    public int getInflateLayout()
    {
        return R.layout.inflator_chat_bubble;
    }

    @Override
    protected void bindData(int position, MyViewHolderG myViewHolderG)
    {
        myViewHolderG.binding.setMyMessage(getViewType(position) == MY_MSG);
        myViewHolderG.binding.setData(dataList.get(position));
        myViewHolderG.binding.executePendingBindings();
    }

}