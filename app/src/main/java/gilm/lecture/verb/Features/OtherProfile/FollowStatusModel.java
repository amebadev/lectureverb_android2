package gilm.lecture.verb.Features.OtherProfile;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * Created by param on 13/3/18.
 */

public class FollowStatusModel extends BasicApiModel {

    private data data;

    public FollowStatusModel.data getData() {
        return data;
    }

    public void setData(FollowStatusModel.data data) {
        this.data = data;
    }

    public class data {
        private String is_approved = "";
        private String id = "";
        private String follower_user_id = "";
        private String followed_user_id = "";
        private String date_time = "";

        public String getIs_approved() {
            return is_approved;
        }

        public void setIs_approved(String is_approved) {
            this.is_approved = is_approved;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFollower_user_id() {
            return follower_user_id;
        }

        public void setFollower_user_id(String follower_user_id) {
            this.follower_user_id = follower_user_id;
        }

        public String getFollowed_user_id() {
            return followed_user_id;
        }

        public void setFollowed_user_id(String followed_user_id) {
            this.followed_user_id = followed_user_id;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }
    }
}
