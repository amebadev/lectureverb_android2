package gilm.lecture.verb.Features.Home.Home;

import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.R;
import gilm.lecture.verb.databinding.InflatorHomeTwoBinding;

/**
 * Created by G-Expo on 06 Jul 2017.
 */

public class HomeAdapter extends InfiniteAdapterG<InflatorHomeTwoBinding>
{
    private HomePresenterBinder presenter;

    private List<HomeViewModel> dataList;

    public HomeAdapter(List<HomeViewModel> dList, HomePresenterBinder presenter)
    {
        this.dataList = dList;
        this.presenter = presenter;
    }

    @Override
    public int getCount()
    {
        return dataList.size();
    }

    @Override
    protected void bindData(int position, MyViewHolderG myViewHolderG)
    {
//        myViewHolderG.binding.viewPager.reset();


        myViewHolderG.binding.setData(dataList.get(position));
        myViewHolderG.binding.setPresenter(presenter);

//        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, presenter.getLayoutHeight());
//        myViewHolderG.binding.linearLayout.setLayoutParams(params);
//        myViewHolderG.binding.linearLayout.invalidate();

        myViewHolderG.binding.executePendingBindings();
    }

    @Override
    public int getInflateLayout()
    {
        return R.layout.inflator_home_two;
    }
}