package gilm.lecture.verb.Features.WebHistory;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Discovery.DiscoveryRecorder_ComingFromPlayer.DiscoveryActivity_HistoryStack;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.InfiniteAdapter_WithoutBuinding;
import gilm.lecture.verb.UtilsG.LoadingViewHolder;
import gilm.lecture.verb.UtilsG.WebInnerStackModel;

/**
 * created by PARAMBIR SINGH on 31/8/17.
 */

public class WebHistoryInnerAdapter extends InfiniteAdapter_WithoutBuinding<WebHistoryInnerAdapter.MyViewHolderG>
{

    private LayoutInflater inflater;
    ArrayList<WebInnerStackModel> data;
    Context context;

    public WebHistoryInnerAdapter(ArrayList<WebInnerStackModel> data, Context context) {
        this.data = data;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        return new LoadingViewHolder(inflater.inflate(R.layout.inflater_web_history_stack, parent, false));
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public MyViewHolderG onCreateView(ViewGroup parent, int viewType) {
        return new MyViewHolderG(inflater.inflate(R.layout.inflater_web_history_stack, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        try {
            if (!(holder instanceof LoadingViewHolder)) {
                if (!data.get(position).getTitle().isEmpty()) {
                    ((MyViewHolderG) holder).txtvTitle.setText("Title: " + data.get(position).getTitle());
                }
                else {
                    if (data.get(position).getUrl().length() > 22) {
                        ((MyViewHolderG) holder).txtvTitle.setText("Title: " + data.get(position).getUrl().substring(0, 22) + "...");
                    }
                    else {
                        ((MyViewHolderG) holder).txtvTitle.setText("Title: " + data.get(position).getUrl() + "...");
                    }
                }

                ((MyViewHolderG) holder).txtvUrl.setText(data.get(position).getUrl());

                ((MyViewHolderG) holder).layMain.setOnClickListener(new View.OnClickListener()
                {
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onClick(View v) {
//                        WebViewRecorderActivity.startForBrowsingOnly(context, data.get(position).getUrl());
                        DiscoveryActivity_HistoryStack.start(context, data.get(position).getUrl());
                    }
                });
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    class MyViewHolderG extends RecyclerView.ViewHolder
    {
        View view;
        TextView txtvTitle, txtvUrl;
        LinearLayout layMain;

        public MyViewHolderG(View row) {
            super(row);

            view = row;
            layMain = (LinearLayout) view.findViewById(R.id.layMain);
            txtvTitle = (TextView) view.findViewById(R.id.txtvTitle);
            txtvUrl = (TextView) view.findViewById(R.id.txtvUrl);
        }
    }

}
