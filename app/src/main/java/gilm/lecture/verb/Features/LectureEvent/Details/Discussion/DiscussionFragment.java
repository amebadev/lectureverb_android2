package gilm.lecture.verb.Features.LectureEvent.Details.Discussion;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gilm.lecture.verb.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiscussionFragment extends Fragment
{

    public static DiscussionFragment newInstance()
    {
        return new DiscussionFragment();
    }

    public DiscussionFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_discussion, container, false);
    }

}
