package gilm.lecture.verb.Features.Home.Notification;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.EndlessRecyclerOnScrollListener;
import gilm.lecture.verb.databinding.FragmentRecyclerlistBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends BaseFragment<FragmentRecyclerlistBinding, NotificationPresenter> implements NotificationView, InfiniteAdapterG.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    ArrayList<NotificationViewModel> mNotificationList = new ArrayList<>();
    NotificationAdapter notificationAdapter;
    int page = 1;

    public static NotificationFragment newInstance() {
        return new NotificationFragment();
    }

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_recyclerlist;
    }

    @Override
    protected void onCreateFragmentG() {
        injectPresenter(new NotificationPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        page = 1;
        getPresenter().loadData(page, getDataBinder().swipeRefresh);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventBusCalled(String userId) {
        page = 1;
        getPresenter().loadData(page, getDataBinder().swipeRefresh);
    }


    @Override
    public void initViews() {
        getDataBinder().swipeRefresh.setOnRefreshListener(this);
        getDataBinder().setIsLoading(getPresenter().getIsLoadingViewModel());

        LinearLayoutManager mLinearManager = new LinearLayoutManager(getActivityG());
        getDataBinder().reviewsList.setLayoutManager(mLinearManager);

        EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(mLinearManager) {
            @Override
            public void onLoadMore(int current_page) {
                page++;
                getPresenter().loadData(page, getDataBinder().swipeRefresh);
            }

            @Override
            public void countChange(int currentVisibleCount) {

            }
        };
        getDataBinder().reviewsList.setOnScrollListener(endlessRecyclerOnScrollListener);

        notificationAdapter = new NotificationAdapter(getActivity(), mNotificationList);
        notificationAdapter.setShouldLoadMore(false);
        getDataBinder().reviewsList.setAdapter(notificationAdapter);

        getPresenter().loadData(page, getDataBinder().swipeRefresh);
    }

    @Override
    public void showData(List<NotificationViewModel> list, int pageNo) {
        if (list.size() > 0) {
            if (pageNo == 1) {
                mNotificationList.clear();
            }
            mNotificationList.addAll(list);
            notificationAdapter.notifyDataSetChanged();
        } else {
            notificationAdapter.setShouldLoadMore(false);
        }
/*        notificationAdapter.setShouldLoadMore(false);*/
    }

    @Override
    public void onLoadMore() {
        page++;
        getPresenter().loadData(page, getDataBinder().swipeRefresh);
    }

    @Override
    public void onRefresh() {
        page = 1;
        getPresenter().loadData(page, getDataBinder().swipeRefresh);
    }
}
