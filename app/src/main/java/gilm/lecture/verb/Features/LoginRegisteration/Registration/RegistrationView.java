package gilm.lecture.verb.Features.LoginRegisteration.Registration;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 05 Jul 2017.
 */

public interface RegistrationView extends Viewable<RegistrationPresenter>
{
    /**
     * open {@link gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity}
     * A.K.A <b>Home Screen</b>
     */
    void openNavigationActivity();

    void openEmailVerification();
}
