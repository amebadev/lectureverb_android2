/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Messaging.Chat;

import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 08 Mar 2017.
 */

public interface ChatView extends Viewable<ChatPresenter>
{
    void notifyDataUpdated();


    /**
     * Update my message in recycler view. (sent message)
     *
     * @param listData -- it contain sent message., contain a single entry in most the cases.
     */
    void updateMyMessage(List<MsgDataModel> listData);

    /**
     * @return page number
     */
    int getPageNumber();

    /**
     * Remove data of adapter on particular position.
     *
     * @param i --data to be removed from i position.
     */
    void removeAdapterPosition(int i);



    void flipButtons(boolean showCameraButton);

    String getTargetUserId();
}
