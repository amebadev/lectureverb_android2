package gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.AddLectureCoHost;

import java.io.Serializable;
import java.util.ArrayList;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * Created by harpreet on 9/12/17.
 */

public class CoHostListModel extends BasicApiModel {

    private ArrayList<SingleCoHostData> data;

    public ArrayList<SingleCoHostData> getmQuickLectureData() {
        return data;
    }

    public void setmQuickLectureData(ArrayList<SingleCoHostData>
                                             data) {
        this.data = data;
    }

    public class SingleCoHostData implements Serializable{

        private String user_id;
        private String role_id;
        private String full_name;
        private String email;
        private String password;
        private String device_type;
        private String device_token;
        private String device_name;
        private String mac_address;
        private String signal_strength;
        private String microphone;
        private String register_via;
        private String social_id;
        private String profile_pic;
        private String cover_pic;
        private String website;
        private String location;
        private String bio;
        private String lat;
        private String lang;
        private String login_status;
        private String status;
        private String is_deleted;
        private String create_on;
        private String updated_on;
        private String deleted_on;
        private String verification_code;
        private String allow_notification;
        private String distance;

        private boolean isReadyToRecord;

        public boolean isReadyToRecord() {
            return isReadyToRecord;
        }

        public void setReadyToRecord(boolean readyToRecord) {
            isReadyToRecord = readyToRecord;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public String getDevice_name() {
            return device_name;
        }

        public void setDevice_name(String device_name) {
            this.device_name = device_name;
        }

        public String getMac_address() {
            return mac_address;
        }

        public void setMac_address(String mac_address) {
            this.mac_address = mac_address;
        }

        public String getSignal_strength() {
            return signal_strength;
        }

        public void setSignal_strength(String signal_strength) {
            this.signal_strength = signal_strength;
        }

        public String getMicrophone() {
            return microphone;
        }

        public void setMicrophone(String microphone) {
            this.microphone = microphone;
        }

        public String getRegister_via() {
            return register_via;
        }

        public void setRegister_via(String register_via) {
            this.register_via = register_via;
        }

        public String getSocial_id() {
            return social_id;
        }

        public void setSocial_id(String social_id) {
            this.social_id = social_id;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }

        public String getCover_pic() {
            return cover_pic;
        }

        public void setCover_pic(String cover_pic) {
            this.cover_pic = cover_pic;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLang() {
            return lang;
        }

        public void setLang(String lang) {
            this.lang = lang;
        }

        public String getLogin_status() {
            return login_status;
        }

        public void setLogin_status(String login_status) {
            this.login_status = login_status;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getIs_deleted() {
            return is_deleted;
        }

        public void setIs_deleted(String is_deleted) {
            this.is_deleted = is_deleted;
        }

        public String getCreate_on() {
            return create_on;
        }

        public void setCreate_on(String create_on) {
            this.create_on = create_on;
        }

        public String getUpdated_on() {
            return updated_on;
        }

        public void setUpdated_on(String updated_on) {
            this.updated_on = updated_on;
        }

        public String getDeleted_on() {
            return deleted_on;
        }

        public void setDeleted_on(String deleted_on) {
            this.deleted_on = deleted_on;
        }

        public String getVerification_code() {
            return verification_code;
        }

        public void setVerification_code(String verification_code) {
            this.verification_code = verification_code;
        }

        public String getAllow_notification() {
            return allow_notification;
        }

        public void setAllow_notification(String allow_notification) {
            this.allow_notification = allow_notification;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }
    }

}
