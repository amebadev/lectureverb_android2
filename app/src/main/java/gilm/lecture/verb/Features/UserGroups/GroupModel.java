package gilm.lecture.verb.Features.UserGroups;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;

/**
 * created by PARAMBIR SINGH on 21/12/17.
 */

public class GroupModel implements Serializable {
    UserDataModel.UserData data;

    @SerializedName("member_data")
    List<UserDataModel.UserData> membersList;
    String date_created = "";
    String group_description = "";
    String payment_date = "";
    String group_status = "";
    String payment_status = "";
    String amount = "";
    String group_type = "";
    String group_image = "";
    String id = "";
    String user_id = "";
    String group_title = "";

    public List<UserDataModel.UserData> getMembersList() {
        return membersList;
    }

    public String getGroup_description() {
        return group_description;
    }

    public void setGroup_description(String group_description) {
        this.group_description = group_description;
    }

    public void setMembersList(List<UserDataModel.UserData> membersList) {
        this.membersList = membersList;
    }

    public UserDataModel.UserData getData() {
        return data;
    }

    public void setData(UserDataModel.UserData data) {
        this.data = data;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getPayment_date() {
        return payment_date;
    }

    public void setPayment_date(String payment_date) {
        this.payment_date = payment_date;
    }

    public String getGroup_status() {
        return group_status;
    }

    public void setGroup_status(String group_status) {
        this.group_status = group_status;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getGroup_type() {
        return group_type;
    }

    public void setGroup_type(String group_type) {
        this.group_type = group_type;
    }

    public String getGroup_image() {
        return group_image;
    }

    public void setGroup_image(String group_image) {
        this.group_image = group_image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGroup_title() {
        return group_title;
    }

    public void setGroup_title(String group_title) {
        this.group_title = group_title;
    }
}
