/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.QuickLecture;

import gilm.lecture.verb.Features.QuickLecture.Presentation.PresentationOptions.PptImagesModel;
import gilm.lecture.verb.WebServices.Web;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by G-Expo on 17 Aug 2017.
 */

public interface QuickLectureApi {

  /*  URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => add_quick_lecture
    Method = > GET / POST
    Description => Get all User tags
    Parameaters = > quick_lecture_type  (audio , video) , user_id ,  file naeme should be (audio ya video) , quick_lecture_text , privacy
    Code ==> 200 for success 400 for error*/

    // for video as well

    @Multipart
    @POST(Web.Path.ADD_QUICK_LECTURE)
    Call<QuickLectureModel> uploadAudioLecture(
            @Part MultipartBody.Part filePart,
            @Part(Web.Keys.QUICK_LECTURE_TYPE) RequestBody quickLectureType
            , @Part(Web.Keys.USER_ID) RequestBody userId
            , @Part(Web.Keys.QUICK_LECTURE_TEXT) RequestBody quickLecText
            , @Part(Web.Keys.DISCOVERY_URL) RequestBody discovery_url
            , @Part(Web.Keys.PRIVACY) RequestBody privacy
            , @Part("quick_lecture_time_duration") RequestBody quick_lecture_time_duration
            , @Part("user_name") RequestBody user_name
            , @Part("history_stack") RequestBody history_stack
            , @Part("thumbnail_url") RequestBody thumbnail_url
            , @Part("group_id") RequestBody group_id
    );

    @Multipart
    @POST(Web.Path.ADD_QUICK_LECTURE)
    Call<QuickLectureModel> uploadPreRecordedLecture(
            @Part MultipartBody.Part filePart,
            @Part(Web.Keys.QUICK_LECTURE_TYPE) RequestBody quickLectureType
            , @Part(Web.Keys.USER_ID) RequestBody userId
            , @Part(Web.Keys.QUICK_LECTURE_TEXT) RequestBody quickLecText
            , @Part(Web.Keys.DISCOVERY_URL) RequestBody discovery_url
            , @Part(Web.Keys.PRIVACY) RequestBody privacy
            , @Part("quick_lecture_time_duration") RequestBody quick_lecture_time_duration
            , @Part("user_name") RequestBody user_name
            , @Part("history_stack") RequestBody history_stack
            , @Part("thumbnail_url") RequestBody thumbnail_url
            , @Part("group_id") RequestBody group_id
            , @Part("lecture_token") RequestBody lecture_token
    );


    @Multipart
    @POST(Web.Path.ADD_QUICK_LECTURE)
    Call<QuickLectureModel> textTypeQuickLecture(
            @Part(Web.Keys.QUICK_LECTURE_TYPE) RequestBody quickLectureType
            , @Part(Web.Keys.USER_ID) RequestBody userId
            , @Part(Web.Keys.QUICK_LECTURE_TEXT) RequestBody quickLecText
            , @Part("user_name") RequestBody user_name
            , @Part("history_stack") RequestBody history_stack);

    @Multipart
    @POST(Web.Path.upload_zamzar_file)
    Call<PptImagesModel> uploadVideoLecture(@Part MultipartBody.Part filePart
            , @Part(Web.Keys.USER_ID) RequestBody userId);


/*    URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => update_quick_lecture_thumbnail
    Method = > GET / POST
    Description => upload quick lecture thumbnail upload_type = upload if using multipart
    Parameaters = > quick_lecture_id , upload_type=upload
    Code ==> 200 for success 400 for error*/

    @Multipart
    @POST(Web.Path.update_quick_lecture_thumbnail)
    Call<ThumbnailResponseModel> uploadLectureThumbnail(@Part MultipartBody.Part filePart, @Part(Web.Keys.quick_lecture_id) RequestBody lectureId, @Part("upload_type") RequestBody upload);


   @FormUrlEncoded
    @POST(Web.Path.get_single_lecture_event)
    Call<Single_Lecture_Event_Model> get_single_lecture_event(
            @Field("user_id") String user_id,
            @Field("event_id") String event_id);

   @FormUrlEncoded
    @POST(Web.Path.
            get_single_posted_lectures)
    Call<Single_Lecture_Event_Model> get_single_posted_lectures(
            @Field("user_id") String user_id,
            @Field("quick_lecture_id") String quick_lecture_id);






}
