/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.UserGroups.GroupDetails;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Connect.ConnectNearBy.UsersList.UsersListAdapter;
import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence.UserType;
import gilm.lecture.verb.Features.MyProfile.Followers.ListOfUserModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.QuickLecture.ChoosePostOptionActivity;
import gilm.lecture.verb.Features.TabAdapter.FragmentTabModel;
import gilm.lecture.verb.Features.TabAdapter.SectionsPagerAdapter;
import gilm.lecture.verb.Features.UserGroups.GroupDetails.GroupInfo.GroupInfoFragment;
import gilm.lecture.verb.Features.UserGroups.GroupDetails.Members.GroupMembersFragment;
import gilm.lecture.verb.Features.UserGroups.GroupLectures.LecturesFragment;
import gilm.lecture.verb.Features.UserGroups.GroupModel;
import gilm.lecture.verb.Features.UserGroups.SingleGroupModel;
import gilm.lecture.verb.Features.UserGroups.UserGroupApis;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.Web;
import gilm.lecture.verb.databinding.FragmentGroupDetailsBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class GroupDetailsFragment extends BaseFragment<FragmentGroupDetailsBinding, GroupDetailsPresenter> implements GroupDetailsView
{

    public GroupModel groupModel;

    @SuppressLint("ValidFragment")
    public GroupDetailsFragment(GroupModel groupModel)
    {
        this.groupModel = groupModel;
    }

    @Override
    protected int getLayoutId()
    {
        return R.layout.fragment_group_details;
    }

    @Override
    protected void onCreateFragmentG()
    {
        injectPresenter(new GroupDetailsPresenter(groupModel));
        getPresenter().attachView(this);

    }

    @Override
    public void initViews()
    {

        getDataBinder().btnInvite.setText(
                ((GroupDetailsActivity) getActivity()).isMeAdmin ? "Add Member" : "Invite Member");

        ImageLoader.setImageBig_FitCenter(getDataBinder().imgvGroupImage, groupModel.getGroup_image());
        getDataBinder().txtvGroupName.setText(groupModel.getGroup_title());
        UtillsG.setHeightWidthWithRatio_LinearLayout(getActivity(), getDataBinder().frameProfileImage, 3.5f, 6f);

        getDataBinder().idViewpager.setVisibility(View.VISIBLE);
        getDataBinder().tab.setVisibility(View.VISIBLE);

        List<FragmentTabModel> list = new ArrayList<>();
        list.add(new FragmentTabModel(LecturesFragment.newInstance(groupModel), "All Lectures"));
        list.add(new FragmentTabModel(new GroupMembersFragment(false, groupModel,
                this,
                (groupModel.getMembersList() != null && !groupModel.getMembersList().isEmpty())
                        ? groupModel.getMembersList()
                        : new ArrayList<UserDataModel.UserData>()), "Members"));
        list.add(new FragmentTabModel(new GroupInfoFragment(groupModel), "Info"));
//        list.add(new FragmentTabModel(BlankFragment.newInstance(), "Media"));

        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getChildFragmentManager(), list);
        getDataBinder().idViewpager.setAdapter(adapter);
        getDataBinder().tab.setupWithViewPager(getDataBinder().idViewpager);
        getDataBinder().fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (new SharedPrefHelper(getActivity()).getUserType().equalsIgnoreCase(UserType.LECTURER))
                {
                    ChoosePostOptionActivity.start(getActivity(), groupModel.getId());
                } else
                {
                    DialogHelper.getInstance().showInformation(getActivity(), "Only a lecturer can post in a user group.", null);
                }
            }
        });
        getDataBinder().tab.setTabTextColors(ContextCompat.getColor(getActivityG(), R.color.colorAccent), ContextCompat.getColor(getActivityG(), R.color.colorAccent));
        getDataBinder().tab.setTabMode(TabLayout.MODE_SCROLLABLE);

        getDataBinder().btnInvite.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showUserSelectionDialog();
            }
        });
        getDataBinder().imgvGroupImage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                UtillsG.showFullImage(Web.Path.BASE_IMAGE_URL + UtillsG.getNotNullString(groupModel.getGroup_image(), "null"), getActivity(), false);
            }
        });

        UtillsG.setHeightWidthWithRatio_LinearLayout(getActivity(), getDataBinder().imgvGroupImage, 9, 16);

    }


    void showUserSelectionDialog()
    {
        final Dialog dialog = new Dialog(getActivityG());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_invite_users);

        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.TOP);
        dialog.show();


        final RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivityG(), 2, LinearLayoutManager.VERTICAL, false));

        showLoading("Please wait..");
        Call<ListOfUserModel> callFollowings = LVApplication.getRetrofit().create(ProfileApi.class).get_my_followings(getLocalData().getUserId());
        callFollowings.enqueue(new Callback<ListOfUserModel>()
        {
            @Override
            public void onResponse(Call<ListOfUserModel> call, Response<ListOfUserModel> response)
            {
                hideLoading();
                if (response.body().getStatus())
                {
                    List<UserDataModel.UserData> aldata = new ArrayList<>();

                    // Checking if the user already exists in the member list then not to show.
                    for (int i = 0; i < response.body().getUsersList().size(); i++)
                    {
                        boolean isFound = false;
                        for (int j = 0; j < groupModel.getMembersList().size(); j++)
                        {
                            if (response.body().getUsersList().get(i).getUser_id().equalsIgnoreCase(groupModel.getMembersList().get(j).getUser_id()))
                            {
                                isFound = true;
                            }
                        }
                        if (!isFound)
                        {
                            aldata.add(response.body().getUsersList().get(i));
                        }
                    }

                    if (aldata.isEmpty())
                    {
                        dialog.dismiss();
                        DialogHelper.getInstance().showInformation(getActivityG(), "All of your connected users are already in this user group.", new CallBackG<String>()
                        {
                            @Override
                            public void onCallBack(String output)
                            {

                            }
                        });
                    }

                    UsersListAdapter usersListAdapter = new UsersListAdapter(getActivityG(), aldata, new CallBackG<UserDataModel.UserData>()
                    {
                        @Override
                        public void onCallBack(final UserDataModel.UserData output1)
                        {
                            DialogHelper.getInstance().showWith2Action(getActivityG(), "Yes", "No", getResources().getString(R.string.app_name), "Do yo want to invite " + output1.getFull_name() + " to this group?", new CallBackG<String>()
                            {
                                @Override
                                public void onCallBack(String output)
                                {
                                    showLoading("Sending Invite..");
                                    Call<BasicApiModel> call = LVApplication.getRetrofit().create(UserGroupApis.class).invite_user(
                                            groupModel.getId(),
                                            getLocalData().getUserId(),
                                            ((GroupDetailsActivity) getActivity()).isMeAdmin ? "1"
                                                    : "0",
                                            groupModel.getUser_id(),
                                            output1.getUser_id(),
                                            output1.getFull_name(),
                                            groupModel.getGroup_title()
                                    );
                                    call.enqueue(new Callback<BasicApiModel>()
                                    {
                                        @Override
                                        public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
                                        {
                                            hideLoading();
                                            if (response.body() != null && response.body().getStatus())
                                            {
                                                UtillsG.showToast(response.body().getMessage(), getActivityG(), true);

                                                showFragmentAgain();
                                            } else
                                            {
                                                UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                                            }
                                            dialog.dismiss();
                                        }

                                        @Override
                                        public void onFailure(Call<BasicApiModel> call, Throwable t)
                                        {
                                            hideLoading();
                                            UtillsG.showToast(t.getMessage() + "", getActivityG(), true);
                                        }
                                    });
                                }
                            });
                        }
                    });
                    usersListAdapter.setShouldLoadMore(false);
                    recyclerView.setAdapter(usersListAdapter);
                } else
                {
                    UtillsG.showToast(response.body().getMessage(), getActivityG(), true);
                }
            }

            @Override
            public void onFailure(Call<ListOfUserModel> call, Throwable t)
            {
                hideLoading();
                UtillsG.showToast(t.getMessage() + "", getActivityG(), true);
            }
        });
    }

    public void showFragmentAgain()
    {
        showLoading("Loading..");
        Call<SingleGroupModel> call = LVApplication.getRetrofit().create(UserGroupApis.class).get_single_group(groupModel.getId());
        call.enqueue(new Callback<SingleGroupModel>()
        {
            @Override
            public void onResponse(Call<SingleGroupModel> call, Response<SingleGroupModel> response)
            {
                hideLoading();

                if (response.body() != null && response.body().getStatus())
                {
                    ((GroupDetailsActivity) getActivity()).showGroupDetailsFragment(response.body().getData());
                } else
                {
                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<SingleGroupModel> call, Throwable t)
            {
                hideLoading();
            }
        });
    }


}
