/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.piterwilson.audio.MP3RadioStreamPlayer;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Player.VideoPlayerActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.DateHelper;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.Web;
import gilm.lecture.verb.Widget.SquareImageViewByWidth;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;


public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public MP3RadioStreamPlayer audioPlayer;

    private LayoutInflater inflater;

    public ImageView publicImgvPlayAudio;
    SeekBar publicSeekbarAudio;

    private List<CommentsModel> commentsList = new ArrayList<>();
    Context context;
    public boolean playeEnd;

    public CommentAdapter(List<CommentsModel> commentsList, Context context, CommentsFragment fragment) {
        this.commentsList = commentsList;
        this.context = context;
        inflater = LayoutInflater.from(context);

        audioPlayer = new MP3RadioStreamPlayer();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CommentsViewHolder(inflater.inflate(R.layout.inflater_comments_row, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        try {
            if (holder instanceof CommentsViewHolder) {

                final CommentsViewHolder mViewHolder = (CommentsViewHolder) holder;
                final CommentsModel mCommentData = commentsList.get(position);

                if (mCommentData.getUser_id().equals(new SharedPrefHelper(context).getUserId())) {

                    // it means if its my comment then the view will be green colored otherwise the view will be of grey color
                    if (mCommentData.getUser_id().equals(new SharedPrefHelper(context).getUserId())) {
                        mViewHolder.llChatBubbleRight.setBackgroundResource(R.drawable.chat_bubble_background);
                        mViewHolder.imgvPooshGreen.setVisibility(View.VISIBLE);
                        mViewHolder.imgvPooshGrey.setVisibility(View.GONE);
                        mViewHolder.txtv_ownMessage.setTextColor(context.getResources().getColor(R.color.white));
                        mViewHolder.txtv_ownMessageTime.setTextColor(context.getResources().getColor(R.color.white));
                    } else {
                        mViewHolder.llChatBubbleRight.setBackgroundResource(R.drawable.chat_bubble_background_grey);
                        mViewHolder.imgvPooshGreen.setVisibility(View.GONE);
                        mViewHolder.imgvPooshGrey.setVisibility(View.VISIBLE);
                        mViewHolder.txtv_ownMessage.setTextColor(context.getResources().getColor(R.color.black));
                        mViewHolder.txtv_ownMessageTime.setTextColor(context.getResources().getColor(R.color.black));
                    }

                    mViewHolder.ll_userMessage.setVisibility(View.VISIBLE);
                    mViewHolder.ll_otherUserMessage.setVisibility(View.GONE);

                    mViewHolder.txtv_ownMessage.setText(UtillsG.decodeEmoji(mCommentData.getComment()));

                    if (UtillsG.getNotNullString(mCommentData.getComment(), "").isEmpty()) {
                        mViewHolder.txtv_ownMessage.setVisibility(View.GONE);
                    } else {
                        mViewHolder.txtv_ownMessage.setVisibility(View.VISIBLE);
                    }

                    mViewHolder.txtv_ownMessageTime.setText(DateHelper.convertFromServerDateToRequiredDate(mCommentData.getDate_time(), "dd-MMM, hh:mm"));
                    ImageLoader.setImageRoundSmall(mViewHolder.imgUserOwnIcon, mCommentData.getUserData().getProfile_pic());

                    if (mCommentData.getFile_extension() != null) {
                        if (mCommentData.getFile_extension().equals(Web.File_Options.IMAGE)) {
                            setOwnLayerViewsVisibility(0, mViewHolder);
                            ImageLoader.setImageBig(mViewHolder.imgUserOwnImageAdded, mCommentData.getComment_file());
                            mViewHolder.frame_ownUserProfileForImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    UtillsG.showFullImage(Web.Path.BASE_URL + mCommentData.getComment_file(), context, false);
                                }
                            });
                        } else if (mCommentData.getFile_extension().equals(Web.File_Options.VIDEO)) {
                            setOwnLayerViewsVisibility(1, mViewHolder);

                            ImageLoader.setImageBig(mViewHolder.imgOwnUserVideoThumbnail, mCommentData.getComment_file());
                            mViewHolder.frame_ownUserProfileForVideo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    VideoPlayerActivity.start(context, Web.Path.BASE_URL + mCommentData.getComment_file());
                                }
                            });
                        } else if (mCommentData.getFile_extension().equals(Web.File_Options.PDF)) {
                            setOwnLayerViewsVisibility(2, mViewHolder);
                            mViewHolder.ll_ownUserProfileForPdf.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    openFile(context, mCommentData);
                                }
                            });
                        } else if (mCommentData.getFile_extension().equals(Web.File_Options.DOC)) {
                            setOwnLayerViewsVisibility(3, mViewHolder);
                            mViewHolder.ll_ownUserProfileForDoc.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    openFile(context, mCommentData);
                                }
                            });
                        } else if (mCommentData.getFile_extension().equals(Web.File_Options.TEXT)) {
                            setOwnLayerViewsVisibility(4, mViewHolder);
                            mViewHolder.ll_ownUserProfileForText.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    openFile(context, mCommentData);
                                }
                            });
                        } else if (mCommentData.getFile_extension().equals(Web.File_Options.AUDIO)) {
                            final String thisAudioUrl = Web.Path.BASE_URL + mCommentData.getComment_file();

                            setOwnLayerViewsVisibility(5, mViewHolder);

                            if (!playeEnd && thisAudioUrl.equals(audioPlayer.getUrlString())) {
                                mViewHolder.imgvPlayAudio.setEnabled(true);
                                mViewHolder.seekbarAudio.setMax((int) audioPlayer.getDuration());
                                mViewHolder.imgvPlayAudio.setImageDrawable(context.getResources().getDrawable(android.R.drawable.ic_media_pause));
                                mViewHolder.seekbarAudio.setProgress((int) audioPlayer.getCurPosition());
                            } else {
                                mViewHolder.imgvPlayAudio.setEnabled(true);
                                mViewHolder.seekbarAudio.setMax((int) audioPlayer.getDuration());
                                mViewHolder.imgvPlayAudio.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_play));
                                mViewHolder.seekbarAudio.setProgress((int) audioPlayer.getCurPosition());
                            }

                            // Play audio file using url
                            mViewHolder.imgvPlayAudio.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    DialogHelper.getInstance().playAudio(((Activity) context), Web.Path.BASE_URL + mCommentData.getComment_file());
                                }
                            });
                        }
                    } else {
                        mViewHolder.frame_ownUserProfileForImage.setVisibility(View.GONE);
                        mViewHolder.ll_ownUserProfileForPdf.setVisibility(View.GONE);
                        mViewHolder.ll_ownUserProfileForDoc.setVisibility(View.GONE);
                        mViewHolder.ll_ownUserProfileForText.setVisibility(View.GONE);
                        mViewHolder.frame_ownUserProfileForVideo.setVisibility(View.GONE);
                        mViewHolder.llAudioPlayer.setVisibility(View.GONE);
                    }

                } else {

                    mViewHolder.txtv_otherMessage.setTextSize((float) new SharedPrefHelper(context).getFontSize());

                    mViewHolder.ll_userMessage.setVisibility(View.GONE);

                    mViewHolder.imgUserOtherIcon.setVisibility(View.VISIBLE);
                    ImageLoader.setImageRoundSmall(mViewHolder.imgUserOtherIcon, mCommentData.getUserData().getProfile_pic());

                    mViewHolder.txtv_otherMessage.setText(UtillsG.decodeEmoji(mCommentData.getComment()));
                    mViewHolder.txtv_otherMessageTime.setText(UtillsG.get_time_ago(mCommentData.getDate_time()));

                    if (mCommentData.getFile_extension() != null && !mCommentData.getFile_extension().trim().isEmpty()) {
                        if (mCommentData.getFile_extension().equals(Web.File_Options.IMAGE)) {
                            setOtherLayerViewsVisibility(0, mViewHolder);
                            ImageLoader.setImageBig(mViewHolder.imgUserOtherImageAdded, mCommentData.getComment_file());
                            mViewHolder.frame_otherUserProfileForImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    UtillsG.showFullImage(Web.Path.BASE_URL + mCommentData.getComment_file(), context, false);
                                }
                            });
                        } else if (mCommentData.getFile_extension().equals(Web.File_Options.VIDEO)) {
                            setOtherLayerViewsVisibility(1, mViewHolder);

                            ImageLoader.setImageBig(mViewHolder.imgUserOtherVideoThumbnail, mCommentData.getComment_file());
                            mViewHolder.frame_otherUserProfileForVideo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    VideoPlayerActivity.start(context, Web.Path.BASE_URL + mCommentData.getComment_file());
                                }
                            });
                        } else if (mCommentData.getFile_extension().equals(Web.File_Options.PDF)) {
                            setOtherLayerViewsVisibility(2, mViewHolder);
                            mViewHolder.ll_otherUserProfileForPdf.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    openFile(context, mCommentData);
                                }
                            });
                        } else if (mCommentData.getFile_extension().equals(Web.File_Options.DOC)) {
                            setOtherLayerViewsVisibility(3, mViewHolder);
                            mViewHolder.ll_otherUserProfileForDoc.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    openFile(context, mCommentData);
                                }
                            });
                        } else if (mCommentData.getFile_extension().equals(Web.File_Options.TEXT)) {
                            setOtherLayerViewsVisibility(4, mViewHolder);
                            mViewHolder.ll_otherUserProfileForText.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    openFile(context, mCommentData);
                                }
                            });
                        } else if (mCommentData.getFile_extension().equals(Web.File_Options.AUDIO)) {
                            final String thisAudioUrl = Web.Path.BASE_URL + mCommentData.getComment_file();

                            setOtherLayerViewsVisibility(5, mViewHolder);

                            if (!playeEnd && thisAudioUrl.equals(audioPlayer.getUrlString())) {
                                mViewHolder.imgvOtherPlayAudio.setEnabled(true);
                                mViewHolder.seekbarOtherAudio.setMax((int) audioPlayer.getDuration());
                                mViewHolder.imgvOtherPlayAudio.setImageDrawable(context.getResources().getDrawable(android.R.drawable.ic_media_pause));
                                mViewHolder.seekbarOtherAudio.setProgress((int) audioPlayer.getCurPosition());
                            } else {
                                mViewHolder.imgvOtherPlayAudio.setEnabled(true);
                                mViewHolder.seekbarOtherAudio.setMax((int) audioPlayer.getDuration());
                                mViewHolder.imgvOtherPlayAudio.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_play));
                                mViewHolder.seekbarOtherAudio.setProgress((int) audioPlayer.getCurPosition());
                            }

                            // Play audio file using url
                            mViewHolder.imgvOtherPlayAudio.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    DialogHelper.getInstance().playAudio((Activity) context, Web.Path.BASE_URL + mCommentData.getComment_file());
                                }
                            });
                        }

                    } else {
                        mViewHolder.llOtherAudioPlayer.setVisibility(View.GONE);
                        mViewHolder.frame_otherUserProfileForImage.setVisibility(View.GONE);
                        mViewHolder.ll_otherUserProfileForPdf.setVisibility(View.GONE);
                        mViewHolder.ll_otherUserProfileForDoc.setVisibility(View.GONE);
                        mViewHolder.ll_otherUserProfileForText.setVisibility(View.GONE);
                        mViewHolder.frame_otherUserProfileForVideo.setVisibility(View.GONE);
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return commentsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    public void setOwnLayerViewsVisibility(int type, CommentsViewHolder mViewHolder) {
        switch (type) {
            case 0:
                mViewHolder.frame_ownUserProfileForImage.setVisibility(View.VISIBLE);
                mViewHolder.llAudioPlayer.setVisibility(View.GONE);
                mViewHolder.frame_ownUserProfileForVideo.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForPdf.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForDoc.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForText.setVisibility(View.GONE);

                break;
            case 1:
                mViewHolder.llAudioPlayer.setVisibility(View.GONE);
                mViewHolder.frame_ownUserProfileForImage.setVisibility(View.GONE);
                mViewHolder.frame_ownUserProfileForVideo.setVisibility(View.VISIBLE);
                mViewHolder.ll_ownUserProfileForPdf.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForDoc.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForText.setVisibility(View.GONE);
                break;
            case 2:
                mViewHolder.llAudioPlayer.setVisibility(View.GONE);
                mViewHolder.frame_ownUserProfileForImage.setVisibility(View.GONE);
                mViewHolder.frame_ownUserProfileForVideo.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForPdf.setVisibility(View.VISIBLE);
                mViewHolder.ll_ownUserProfileForDoc.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForText.setVisibility(View.GONE);
                break;
            case 3:
                mViewHolder.llAudioPlayer.setVisibility(View.GONE);
                mViewHolder.frame_ownUserProfileForImage.setVisibility(View.GONE);
                mViewHolder.frame_ownUserProfileForVideo.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForPdf.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForDoc.setVisibility(View.VISIBLE);
                mViewHolder.ll_ownUserProfileForText.setVisibility(View.GONE);
                break;
            case 4:
                mViewHolder.llAudioPlayer.setVisibility(View.GONE);
                mViewHolder.frame_ownUserProfileForImage.setVisibility(View.GONE);
                mViewHolder.frame_ownUserProfileForVideo.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForPdf.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForDoc.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForText.setVisibility(View.VISIBLE);
                break;
            case 5:
                mViewHolder.llAudioPlayer.setVisibility(View.VISIBLE);
                mViewHolder.frame_ownUserProfileForImage.setVisibility(View.GONE);
                mViewHolder.frame_ownUserProfileForVideo.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForPdf.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForDoc.setVisibility(View.GONE);
                mViewHolder.ll_ownUserProfileForText.setVisibility(View.GONE);
                break;
        }
    }

    public void setOtherLayerViewsVisibility(int type, CommentsViewHolder mViewHolder) {
        switch (type) {
            case 0:
                mViewHolder.llOtherAudioPlayer.setVisibility(View.GONE);
                mViewHolder.frame_otherUserProfileForImage.setVisibility(View.VISIBLE);
                mViewHolder.frame_otherUserProfileForVideo.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForPdf.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForDoc.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForText.setVisibility(View.GONE);

                break;
            case 1:
                mViewHolder.llOtherAudioPlayer.setVisibility(View.GONE);
                mViewHolder.frame_otherUserProfileForImage.setVisibility(View.GONE);
                mViewHolder.frame_otherUserProfileForVideo.setVisibility(View.VISIBLE);
                mViewHolder.ll_otherUserProfileForPdf.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForDoc.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForText.setVisibility(View.GONE);
                break;
            case 2:
                mViewHolder.llOtherAudioPlayer.setVisibility(View.GONE);
                mViewHolder.frame_otherUserProfileForImage.setVisibility(View.GONE);
                mViewHolder.frame_otherUserProfileForVideo.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForPdf.setVisibility(View.VISIBLE);
                mViewHolder.ll_otherUserProfileForDoc.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForText.setVisibility(View.GONE);
                break;
            case 3:
                mViewHolder.llOtherAudioPlayer.setVisibility(View.GONE);
                mViewHolder.frame_otherUserProfileForImage.setVisibility(View.GONE);
                mViewHolder.frame_otherUserProfileForVideo.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForPdf.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForDoc.setVisibility(View.VISIBLE);
                mViewHolder.ll_otherUserProfileForText.setVisibility(View.GONE);
                break;
            case 4:
                mViewHolder.llOtherAudioPlayer.setVisibility(View.GONE);
                mViewHolder.frame_otherUserProfileForImage.setVisibility(View.GONE);
                mViewHolder.frame_otherUserProfileForVideo.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForPdf.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForDoc.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForText.setVisibility(View.VISIBLE);
                break;
            case 5:
                mViewHolder.llOtherAudioPlayer.setVisibility(View.VISIBLE);
                mViewHolder.frame_otherUserProfileForImage.setVisibility(View.GONE);
                mViewHolder.frame_otherUserProfileForVideo.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForPdf.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForDoc.setVisibility(View.GONE);
                mViewHolder.ll_otherUserProfileForText.setVisibility(View.GONE);
                break;

        }
    }


    class CommentsViewHolder extends RecyclerView.ViewHolder {
        View view;
        SquareImageViewByWidth imgUserOwnImageAdded, imgOwnUserVideoThumbnail;
        ImageView imgUserOtherIcon, imgUserOwnIcon, imgUserOtherImageAdded, imgUserOtherVideoThumbnail, imgvPlayAudio, imgvPooshGrey, imgvPooshGreen, imgvOtherPlayAudio;
        TextView txtv_ownMessageTime, txtv_otherMessageTime;
        LinearLayout ll_userMessage, ll_otherUserMessage, llChatBubbleRight, llOtherAudioPlayer;
        FrameLayout frame_ownUserProfileForImage, frame_otherUserProfileForImage, frame_otherUserProfileForVideo, frame_ownUserProfileForVideo;
        SeekBar seekbarAudio, seekbarOtherAudio;
        EmojiconTextView txtv_ownMessage, txtv_otherMessage;


        LinearLayout ll_ownUserProfileForPdf, ll_ownUserProfileForDoc, ll_ownUserProfileForText, ll_otherUserProfileForPdf, ll_otherUserProfileForDoc, ll_otherUserProfileForText, llAudioPlayer;


        public CommentsViewHolder(View row) {
            super(row);

            view = row;
            seekbarAudio = (SeekBar) view.findViewById(R.id.seekbarAudio);
            seekbarOtherAudio = (SeekBar) view.findViewById(R.id.seekbarOtherAudio);
            ll_userMessage = (LinearLayout) view.findViewById(R.id.ll_userMessage);
            llAudioPlayer = (LinearLayout) view.findViewById(R.id.llAudioPlayer);
            llChatBubbleRight = (LinearLayout) view.findViewById(R.id.llChatBubbleRight);
            imgvOtherPlayAudio = (ImageView) view.findViewById(R.id.imgvOtherPlayAudio);
            llOtherAudioPlayer = (LinearLayout) view.findViewById(R.id.llOtherAudioPlayer);

            imgvPooshGreen = (ImageView) view.findViewById(R.id.imgvPooshGreen);
            imgvPooshGrey = (ImageView) view.findViewById(R.id.imgvPooshGrey);
            imgvPlayAudio = (ImageView) view.findViewById(R.id.imgvPlayAudio);
            imgUserOwnIcon = (ImageView) view.findViewById(R.id.imgUserOwnIcon);
            txtv_ownMessage = (EmojiconTextView) view.findViewById(R.id.txtv_ownMessage);
            txtv_ownMessageTime = (TextView) view.findViewById(R.id.txtv_ownMessageTime);
            imgUserOwnImageAdded = (SquareImageViewByWidth) view.findViewById(R.id.imgUserOwnImageAdded);
            frame_ownUserProfileForImage = (FrameLayout) view.findViewById(R.id.frame_ownUserProfileForImage);
            ll_ownUserProfileForPdf = (LinearLayout) view.findViewById(R.id.ll_ownUserProfileForPdf);
            ll_ownUserProfileForDoc = (LinearLayout) view.findViewById(R.id.ll_ownUserProfileForDoc);
            ll_ownUserProfileForText = (LinearLayout) view.findViewById(R.id.ll_ownUserProfileForText);
            imgOwnUserVideoThumbnail = (SquareImageViewByWidth) view.findViewById(R.id.imgOwnUserVideoThumbnail);
            frame_ownUserProfileForVideo = (FrameLayout) view.findViewById(R.id.frame_ownUserProfileForVideo);


            frame_otherUserProfileForVideo = (FrameLayout) view.findViewById(R.id.frame_otherUserProfileForVideo);
            imgUserOtherVideoThumbnail = (ImageView) view.findViewById(R.id.imgUserOtherVideoThumbnail);
            ll_otherUserMessage = (LinearLayout) view.findViewById(R.id.ll_otherUserMessage);
            ll_otherUserProfileForPdf = (LinearLayout) view.findViewById(R.id.ll_otherUserProfileForPdf);
            ll_otherUserProfileForDoc = (LinearLayout) view.findViewById(R.id.ll_otherUserProfileForDoc);
            ll_otherUserProfileForText = (LinearLayout) view.findViewById(R.id.ll_otherUserProfileForText);
            frame_otherUserProfileForImage = (FrameLayout) view.findViewById(R.id.frame_otherUserProfileForImage);
            imgUserOtherImageAdded = (ImageView) view.findViewById(R.id.imgUserOtherImageAdded);
            txtv_otherMessage = (EmojiconTextView) view.findViewById(R.id.txtv_otherMessage);
            txtv_otherMessageTime = (TextView) view.findViewById(R.id.txtv_otherMessageTime);
            imgUserOtherIcon = (ImageView) view.findViewById(R.id.imgUserOtherIcon);


        }
    }


    public void openFile(Context context, CommentsModel mComment) {
        try {
            String url = Web.Path.BASE_URL + mComment.getComment_file();
            Uri uri = Uri.parse(Web.Path.BASE_URL + mComment.getComment_file());

            Intent intent = new Intent(Intent.ACTION_VIEW);
            // Check what kind of file you are trying to open, by comparing the url with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) type,
            // so Android knew what application to use to open the file
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                //if you want you can also define the intent type for any other file

                //additionally use else clause below, to manage other unknown extensions
                //in this case, Android will show all applications installed on the device
                //so you can choose which application to use
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (Exception ex) {
            UtillsG.showToast("Application not found", context, true);
        }
    }

    public void releaseAudioPlayer() {
        if (audioPlayer != null) {
            audioPlayer.stop();
            audioPlayer.release();
            audioPlayer = null;
        }
    }


}
