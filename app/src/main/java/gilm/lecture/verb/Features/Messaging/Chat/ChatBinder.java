/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Messaging.Chat;

import android.databinding.ObservableField;
import android.view.View;

/**
 * Created by G-Expo on 25 Jul 2017.
 */

public interface ChatBinder
{
    /**
     * @return {@link String} message ,which is bind with the enter message edit text.
     */
    ObservableField<String> textMessage();

    /**
     * @return {@link String} users Image URL .
     */
    ObservableField<String> imageUrl();

    /**
     * @return {@link String} users name.
     */
    ObservableField<String> name();

    /**
     * send String message ,what ever is in {@link #textMessage()}
     */
    void sendMessage(View view);

}
