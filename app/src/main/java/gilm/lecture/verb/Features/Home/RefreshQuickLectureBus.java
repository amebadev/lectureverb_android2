package gilm.lecture.verb.Features.Home;

/**
 * Created by harpreet on 11/10/17.
 */

public class RefreshQuickLectureBus {

    private String quickLectureId;
    private Boolean isForLike;
    private Boolean is_actionTrue;

    public RefreshQuickLectureBus(String quickLectureId,Boolean isForLike,Boolean is_actionTrue)
    {
        this.quickLectureId = quickLectureId;
        this.is_actionTrue=is_actionTrue;
        this.isForLike=isForLike;
    }

    public String getQuickLectureId() {
        return quickLectureId;
    }

    public void setQuickLectureId(String quickLectureId) {
        this.quickLectureId = quickLectureId;
    }

    public Boolean getForLike() {
        return isForLike;
    }

    public void setForLike(Boolean forLike) {
        isForLike = forLike;
    }

    public Boolean getIs_actionTrue() {
        return is_actionTrue;
    }

    public void setIs_actionTrue(Boolean is_actionTrue) {
        this.is_actionTrue = is_actionTrue;
    }
}
