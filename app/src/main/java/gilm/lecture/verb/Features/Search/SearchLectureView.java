/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Search;

import java.util.List;

import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 24 Jul 2017.
 */

public interface SearchLectureView extends Viewable<SearchLecturePresenter>
{
    void showHotLecture(List<Lecture_And_Event_Model> data);

    void showTopLecture(List<Lecture_And_Event_Model> data);
}
