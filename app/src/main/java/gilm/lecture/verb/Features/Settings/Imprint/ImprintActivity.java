package gilm.lecture.verb.Features.Settings.Imprint;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.CompoundButton;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.databinding.ActivityImprintBinding;

public class ImprintActivity extends BaseActivity<ActivityImprintBinding, ImprintPresenter> implements ImprintView
{

    public static void start(Context context) {
        Intent starter = new Intent(context, ImprintActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_imprint;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new ImprintPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        setupToolbar("Imprint");

        getDataBinder().llNotificationCategories.setVisibility(View.GONE);
        getDataBinder().llPushNotifications.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                getDataBinder().switchNotifications.performClick();
            }
        });

        getDataBinder().switchNotifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                getDataBinder().llNotificationCategories.setVisibility(
                        b ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public Context getActivityG() {
        return ImprintActivity.this;
    }
}
