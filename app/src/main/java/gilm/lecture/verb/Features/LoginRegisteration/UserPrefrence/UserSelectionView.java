package gilm.lecture.verb.Features.LoginRegisteration.UserPrefrence;

import android.support.annotation.IdRes;
import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 05 Jul 2017.
 */

public interface UserSelectionView extends Viewable<UserSelectionPresenter>
{
    void openLogin();

    View getViewButtonById(@IdRes int i);
}
