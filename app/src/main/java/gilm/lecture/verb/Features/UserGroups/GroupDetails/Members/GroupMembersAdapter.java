package gilm.lecture.verb.Features.UserGroups.GroupDetails.Members;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.Features.UserGroups.GroupDetails.GroupDetailsActivity;
import gilm.lecture.verb.Features.UserGroups.UserGroupApis;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * created by PARAMBIR SINGH on 10/10/17.
 */

public class GroupMembersAdapter extends RecyclerView.Adapter<GroupMembersAdapter.MyViewHolderG>
{


    private final LayoutInflater inflater;
    private List<UserDataModel.UserData> alData = new ArrayList<>();
    private boolean isFollowing, isMyProfile;
    private Context context;
    GroupMembersFragment groupMembersFragment;

    public GroupMembersAdapter(Context context, List<UserDataModel.UserData> alData, boolean isFollowing, boolean isMyProfile, GroupMembersFragment groupMembersFragment)
    {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.alData = alData;
        this.isFollowing = isFollowing;
        this.isMyProfile = isMyProfile;
        this.groupMembersFragment = groupMembersFragment;
    }

    @Override
    public MyViewHolderG onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new MyViewHolderG(inflater.inflate(R.layout.inflater_followers_followings, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolderG holder, final int position)
    {
        ImageLoader.setImageRoundSmall(holder.imgvUserImage, alData.get(position).getProfile_pic());
        holder.txtvUserName.setText(alData.get(position).getFull_name() + (alData.get(position).getRole_id().equals("2") ? "(Lecturer)" : ""));
        holder.txtvFollowUnFollow.setText(
                (UtillsG.getNotNullString(alData.get(position).getIs_following(), "").equalsIgnoreCase("1") || UtillsG.getNotNullString(alData.get(position).getIs_following(), "").equalsIgnoreCase("true"))
                        ? "Un Follow"
                        : "Follow");
        holder.txtvFollowUnFollow.setVisibility(
                alData.get(position).getUser_id().equalsIgnoreCase(new SharedPrefHelper(context).getUserId())
                        ? View.GONE : View.VISIBLE);


        // show the button if its my profile only
        if (isMyProfile)
        {
            holder.txtvFollowUnFollow.setVisibility(View.VISIBLE);
        } else
        {
            holder.txtvFollowUnFollow.setVisibility(View.GONE);
        }
        if (((GroupDetailsActivity) groupMembersFragment.groupDetailsFragment.getActivity()).isMeAdmin)
        {
            holder.imgvDelete.setVisibility(View.VISIBLE);
        } else
        {
            holder.imgvDelete.setVisibility(View.GONE);
        }


        holder.view.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                ActivityOtherUserDetails.start(context, alData.get(position).getUser_id(), "User Profile");
            }
        });
        holder.imgvDelete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                DialogHelper.getInstance().showWith2Action(context, "Yes", "No", context.getResources().getString(R.string.app_name), "Are you sure you want to remove this user from the member list?", new CallBackG<String>()
                {
                    @Override
                    public void onCallBack(String output)
                    {
                        Call<BasicApiModel> call = LVApplication.getRetrofit().create(UserGroupApis.class).leave_group(
                                alData.get(position).getUser_id(), groupMembersFragment.groupModel.getId());
                        call.enqueue(new Callback<BasicApiModel>()
                        {
                            @Override
                            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
                            {
                                if (response.body() != null && response.body().getStatus())
                                {
                                    groupMembersFragment.groupDetailsFragment.showFragmentAgain();
                                } else
                                {
                                    UtillsG.showToast(response.body().getMessage(), context, true);
                                }
                            }

                            @Override
                            public void onFailure(Call<BasicApiModel> call, Throwable t)
                            {

                            }
                        });
                    }
                });
            }
        });


        holder.txtvFollowUnFollow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Call<BasicApiModel> getFollowersList = getRetrofitInstance().follow_user(new SharedPrefHelper(context).getUserId(), alData.get(position).getUser_id());
                getFollowersList.enqueue(new Callback<BasicApiModel>()
                {
                    @Override
                    public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response)
                    {
                        if (response.body().getStatus())
                        {
                            alData.get(position).setIs_following(
                                    (UtillsG.getNotNullString(alData.get(position).getIs_following(), "").equalsIgnoreCase("1") || UtillsG.getNotNullString(alData.get(position).getIs_following(), "").equalsIgnoreCase("true"))
                                            ? "0" : "1");
                            UtillsG.showToast(response.body().getMessage(), context, true);

                            if (UtillsG.getNotNullString(alData.get(position).getIs_following(), "").equalsIgnoreCase("0") || UtillsG.getNotNullString(alData.get(position).getIs_following(), "").equalsIgnoreCase("false"))
                            {
                                alData.remove(position);
                            }

                            notifyDataSetChanged();
                        } else
                        {
                            UtillsG.showToast(response.body().getMessage(), context, true);
                        }
                    }

                    @Override
                    public void onFailure(Call<BasicApiModel> call, Throwable t)
                    {
                        UtillsG.showToast(t.getMessage(), context, true);
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return alData.size();
    }

    class MyViewHolderG extends RecyclerView.ViewHolder
    {
        View view;
        ImageView imgvUserImage, imgvDelete;
        TextView txtvUserName, txtvFollowUnFollow;

        public MyViewHolderG(View row)
        {
            super(row);

            view = row;
            imgvDelete = (ImageView) view.findViewById(R.id.imgvDelete);
            imgvUserImage = (ImageView) view.findViewById(R.id.imgvUserImage);
            txtvUserName = (TextView) view.findViewById(R.id.txtvUserName);
            txtvFollowUnFollow = (TextView) view.findViewById(R.id.txtvFollowUnFollow);
        }
    }

    private ProfileApi getRetrofitInstance()
    {
        return LVApplication.getRetrofit().create(ProfileApi.class);
    }
}
