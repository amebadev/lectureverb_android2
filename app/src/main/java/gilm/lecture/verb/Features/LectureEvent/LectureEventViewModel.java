package gilm.lecture.verb.Features.LectureEvent;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;
import gilm.lecture.verb.BR;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureEventModel;

/**
 * Created by G-Expo on 07 Jul 2017.
 */

public class LectureEventViewModel extends BaseObservable implements Parcelable
{
    private String title, timeRange, startTime, location, message, imageUrl, attendingLecture,detail,isInterested,lecture_event_id;
    private boolean  isIgnored, isUpcoming,canInviteGuest;
    private LectureDetailsModel mLectureEvent;



    public LectureEventViewModel()
    {
    }

    protected LectureEventViewModel(Parcel in)
    {
        title = in.readString();
        timeRange = in.readString();
        startTime = in.readString();
        location = in.readString();
        message = in.readString();
        imageUrl = in.readString();
        attendingLecture = in.readString();
        detail = in.readString();
        isInterested =  in.readString();
        isIgnored = in.readByte() != 0;
        isUpcoming = in.readByte() != 0;
        canInviteGuest=in.readByte() != 0;
    }

    public static final Creator<LectureEventViewModel> CREATOR = new Creator<LectureEventViewModel>()
    {
        @Override
        public LectureEventViewModel createFromParcel(Parcel in)
        {
            return new LectureEventViewModel(in);
        }

        @Override
        public LectureEventViewModel[] newArray(int size)
        {
            return new LectureEventViewModel[size];
        }
    };

    public LectureDetailsModel getmLectureEvent() {
        return mLectureEvent;
    }

    public void setmLectureEvent(LectureDetailsModel mLectureEvent) {
        this.mLectureEvent = mLectureEvent;
    }

    public String getLecture_event_id() {
        return lecture_event_id;
    }

    public void setLecture_event_id(String lecture_event_id) {
        this.lecture_event_id = lecture_event_id;
    }

    @Bindable
    public String getDetail()
    {
        return detail;
    }

    public void setDetail(String detail)
    {
        this.detail = detail;
        notifyPropertyChanged(BR.detail);
    }

    @Bindable
    public boolean getCanInviteGuest()
    {
        return canInviteGuest;
    }

    public void setCanInviteGuest(boolean canInviteGuest)
    {
        this.canInviteGuest = canInviteGuest;
        notifyPropertyChanged(BR.canInviteGuest);
    }

    @Bindable
    public boolean isUpcoming()
    {
        return isUpcoming;
    }

    public void setUpcoming(boolean upcoming)
    {
        isUpcoming = upcoming;
        notifyPropertyChanged(BR.upcoming);

    }

    @Bindable
    public String getAttendingLecture()
    {
        return attendingLecture;
    }

    public void setAttendingLecture(String attendingLecture)
    {
        this.attendingLecture = attendingLecture;
        notifyPropertyChanged(BR.attendingLecture);
    }


    @Bindable
    public String getIsInterested() {
        return isInterested;
    }

    public void setIsInterested(String isInterested) {
        this.isInterested = isInterested;
        notifyPropertyChanged(BR.isInterested);
    }

    @Bindable
    public boolean isIgnored()
    {
        return isIgnored;
    }

    public void setIgnored(boolean ignored)
    {
        isIgnored = ignored;
        notifyPropertyChanged(BR.ignored);
    }

    @Bindable
    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
        notifyPropertyChanged(BR.title);

    }

    @Bindable
    public String getTimeRange()
    {
        return timeRange;
    }

    public void setTimeRange(String timeRange)
    {
        this.timeRange = timeRange;
        notifyPropertyChanged(BR.timeRange);

    }

    @Bindable
    public String getStartTime()
    {
        return startTime;
    }

    public void setStartTime(String startTime)
    {
        this.startTime = startTime;
        notifyPropertyChanged(BR.startTime);

    }

    @Bindable
    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
        notifyPropertyChanged(BR.location);

    }

    @Bindable
    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
        notifyPropertyChanged(BR.message);
    }

    @Bindable
    public String getImageUrl()
    {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
        notifyPropertyChanged(BR.imageUrl);

    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(title);
        parcel.writeString(timeRange);
        parcel.writeString(startTime);
        parcel.writeString(location);
        parcel.writeString(message);
        parcel.writeString(imageUrl);
        parcel.writeString(attendingLecture);
        parcel.writeString(detail);
        parcel.writeString(isInterested);
        parcel.writeByte((byte) (isIgnored ? 1 : 0));
        parcel.writeByte((byte) (isUpcoming ? 1 : 0));
        parcel.writeByte((byte) (canInviteGuest ? 1 : 0));
    }
}
