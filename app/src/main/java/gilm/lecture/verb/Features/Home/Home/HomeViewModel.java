package gilm.lecture.verb.Features.Home.Home;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

/**
 * Created by G-Expo on 06 Jul 2017.
 */

public class HomeViewModel extends BaseObservable
{
    private String imageUrl;
    private String backImageUrl;
    private String time;
    private String title;

    @Bindable
    public String getImageUrl()
    {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
        notifyPropertyChanged(BR.imageUrl);
    }

    @Bindable
    public String getBackImageUrl()
    {
        return backImageUrl;
    }

    public void setBackImageUrl(String backImageUrl)
    {
        this.backImageUrl = backImageUrl;
        notifyPropertyChanged(BR.backImageUrl);
    }

    @Bindable
    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
        notifyPropertyChanged(BR.time);
    }

    @Bindable
    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }
}
