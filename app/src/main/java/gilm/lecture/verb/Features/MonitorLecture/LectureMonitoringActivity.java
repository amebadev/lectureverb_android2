/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.MonitorLecture;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.czt.mp3recorder.MP3Recorder;
import com.malmstein.fenster.play.FensterVideoFragment;
import com.shuyu.waveview.AudioPlayer;
import com.shuyu.waveview.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import gilm.lecture.verb.Features.Connect.ConnectNearBy.UsersList.UsersListActivity;
import gilm.lecture.verb.Features.InterestsSelection.AddInterests.AddInterestCategoriesActivity;
import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EventLectureApi;
import gilm.lecture.verb.Features.LectureEvent.LectureEventClicks;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.Followers.ListOfUserModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.CustomDialog;
import gilm.lecture.verb.UtilsG.DateHelper;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.FileHelper;
import gilm.lecture.verb.UtilsG.GeocodingNotifier;
import gilm.lecture.verb.UtilsG.GetCurrentLocation;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.databinding.ActivityLectureMonitoringBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class LectureMonitoringActivity extends BaseActivity<ActivityLectureMonitoringBinding, LectureMonitorPresenter> implements LectureMonitorView {
    private boolean mIsPlay;
    private boolean mIsRecord;
    public MP3Recorder mRecorder;
    public boolean isRecordingBoolean;
    int hours, minutes, seconds;
    private String filePath = "";
    private AudioPlayer audioPlayer;

    private String latitude = "", longitude = "";
    private ArrayList<UserDataModel.UserData> alData = new ArrayList<>();

    LectureDetailsModel mLectureViewModel;
    private String commaSeparatedNames = "";
    private String commaSeparatedIds = "";

    boolean isVideoRecordingCompleted;
    public boolean isRecordingVideo;

    public static void start(Context context, LectureDetailsModel mLectureViewModel) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent starter = new Intent(context, LectureMonitoringActivity.class);
            starter.putExtra("mLectureViewModel", mLectureViewModel);
            context.startActivity(starter);
        } else {
            UtillsG.showToast("This device does not support this functionality.", context, true);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
//        getDataBinder().cameraView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        getDataBinder().cameraView.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        getDataBinder().cameraView.destroy();
    }

    @Override
    public void fileUploadingDone() {
        DialogHelper.getInstance().showInformation(getActivityG(), "Lecture Event files uploaded to server. Please check web console to review and edit files.", new CallBackG<String>() {
            @Override
            public void onCallBack(String output) {
//                getDataBinder().cameraView.stop();
//                getDataBinder().cameraView.destroy();
                finish();
            }
        });
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_lecture_monitoring;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new LectureMonitorPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void videoRecordingStopped(String absolutePath) {
        isVideoRecordingCompleted = true;
        getDataBinder().fabRecordVideo.setVisibility(View.GONE);
        getVideoFragment().playVideo(absolutePath);
        getDataBinder().flvideoPlayer.setVisibility(View.VISIBLE);
        supportInvalidateOptionsMenu();
    }

    public FensterVideoFragment getVideoFragment() {
        return (FensterVideoFragment) getFragmentManager().findFragmentById(R.id.play_demo_fragment);
    }


    @Override
    public void initViews() {
        mLectureViewModel = (LectureDetailsModel) getIntent().getSerializableExtra("mLectureViewModel");
        setupToolbar("Lecture Monitoring Panel");

        getDataBinder().txtvLectureCategory.setVisibility(
                mLectureViewModel.getUser_id().equals(getLocalData().getUserId())
                        ? View.VISIBLE
                        : View.GONE);
        getDataBinder().txtvLectureCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddInterestCategoriesActivity.start(getActivityG(), true, commaSeparatedIds, commaSeparatedNames);
            }
        });

        setExistingCategoriesOnTextView();

        ImageLoader.setImageBig(getDataBinder().imgvPoster, mLectureViewModel.getLecture_poster());
        getDataBinder().txtvTitle.setText(mLectureViewModel.getLecture_title());
        getDataBinder().txtvStartDate.setText(DateHelper.getInstance().getCompleteDateForEvent(mLectureViewModel.getLecture_start_date_time()));

//        getPresenter().setCameraView(getDataBinder().cameraView);
        getPresenter().setCameraView(getFragmentManager());
        getPresenter().setCameraListener();
        getDataBinder().imgbVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDataBinder().llMainLayout.setVisibility(View.GONE);
                getDataBinder().imgbMonitor.setBackground(ContextCompat.getDrawable(getActivityG(), R.drawable.red_theme_square));
                getDataBinder().imgbVideo.setBackground(ContextCompat.getDrawable(getActivityG(), R.drawable.red_theme_dark_square));

                if (isVideoRecordingCompleted) {
                    getDataBinder().flvideoPlayer.setVisibility(View.VISIBLE);
                }
            }
        });
        getDataBinder().imgbMonitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDataBinder().llMainLayout.setVisibility(View.VISIBLE);
                getDataBinder().imgbMonitor.setBackground(ContextCompat.getDrawable(getActivityG(), R.drawable.red_theme_dark_square));
                getDataBinder().imgbVideo.setBackground(ContextCompat.getDrawable(getActivityG(), R.drawable.red_theme_square));

                getDataBinder().flvideoPlayer.setVisibility(View.GONE);
            }
        });
        getDataBinder().imgbMonitor.performClick();

        getDataBinder().audioWave.setChangeColor(Color.WHITE, Color.WHITE, Color.WHITE);
//        startRefreshingViewers();
        getNearByUsers();

        getDataBinder().fabRecordVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startStopBothRecordings();
            }
        });
        getDataBinder().imgvChangeCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isRecordingVideo) {
                    getPresenter().toggleCamera();
                }
            }
        });
    }

    private void setExistingCategoriesOnTextView() {
        if (mLectureViewModel.getEvent_interest() != null && mLectureViewModel.getEvent_interest().size() > 0) {
            ArrayList<ChildCategoryModel> alCategories = mLectureViewModel.getEvent_interest();
            if (alCategories != null) {
                for (int i = 0; i < alCategories.size(); i++) {

                    commaSeparatedIds = commaSeparatedIds + (commaSeparatedIds.isEmpty() ? ""
                            : ",") + alCategories.get(i).getCategory_id();
                    commaSeparatedNames = commaSeparatedNames + (
                            commaSeparatedNames.isEmpty() ? ""
                                    : ", ") + alCategories.get(i).getCategory_name();
                }
                getDataBinder().txtvLectureCategory.setText(commaSeparatedNames);
            }
        } else {
            getDataBinder().txtvLectureCategory.setText("No lecture category selected yet.");
        }
    }

    private void startStopVideoRecording() {
      /*  if (!getDataBinder().cameraView.isCapturingVideo()) {
            getDataBinder().fabRecordVideo.setImageResource(R.drawable.red_circle);
            getPresenter().startVideoRecording();
        } else {
            getDataBinder().fabRecordVideo.setImageResource(R.drawable.ic_green_dot);
            getPresenter().stopVideoRecording();

        }*/

        String videoFilePath = new String(UtillsG.getNotNullString(getPresenter().getVideoRecorderFragment().mNextVideoAbsolutePath, ""));
        isRecordingVideo = getPresenter().getVideoRecorderFragment().startStopVideoRecording();
        if (isRecordingVideo) {
            getDataBinder().fabRecordVideo.setImageResource(R.drawable.red_circle);
        } else {
            getDataBinder().fabRecordVideo.setImageResource(R.drawable.ic_green_dot);
            Log.e("VIDEO FILE PATH", videoFilePath);
            getPresenter().setVideoFilePath(videoFilePath);
            videoRecordingStopped(videoFilePath);
        }
    }

    private void startRefreshingViewers() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivityG() != null) {
                    getNearByUsers();
                    startRefreshingViewers();
                }
            }
        }, 30000);
    }

    @Override
    public void onBackPressed() {
        showExitMessage();
    }

    AlertDialog dialog;

    protected void showExitMessage() {

/*
        if (getDataBinder().cameraView.isCapturingVideo()) {
            startStopVideoRecording();
        }
*/

        if (mIsRecord) {
            startStopAudioRecording();
        }
        finish();
    }


    @Override
    public Context getActivityG() {
        return LectureMonitoringActivity.this;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isVideoRecordingCompleted) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_save, menu);
        } else {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_audio_icon, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            showExitMessage();
        } else if (item.getItemId() == R.id.startStopRecording) {
            startStopBothRecordings();
        } else if (item.getItemId() == R.id.save) {
            getPresenter().uploadRecordedMedia();
        }
        return super.onOptionsItemSelected(item);
    }

    private void startStopBothRecordings() {
        startStopAudioRecording();
        startStopVideoRecording();
    }

    private void startStopAudioRecording() {
        if (mIsRecord) {
            getDataBinder().lecturerAudioIndicator.setVisibility(View.GONE);
            getDataBinder().txtvLecturerAudio.setText("Stopped");
            pauseRecording();

            getDataBinder().imgvPlayAudio.setVisibility(View.VISIBLE);
            getDataBinder().audioWave.setVisibility(View.GONE);
            getDataBinder().imgvPlayAudio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogHelper.getInstance().playAudio(LectureMonitoringActivity.this, filePath);
                }
            });
        } else {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    getDataBinder().lecturerAudioIndicator.setVisibility(View.VISIBLE);
                    getDataBinder().txtvLecturerAudio.setText("Recording");
                    startRecording();
                    startTimer();
                }
            });
        }
    }


    private void getNearByUsers() {
        getDataBinder().llVenueViewers.setOnClickListener(null);

        GetCurrentLocation location = new GetCurrentLocation((Activity) getActivityG(), new GeocodingNotifier<Location>() {
            @Override
            public void GeocodingDetails(Location geocodeLatLng) {
                Log.e("Latitude", geocodeLatLng.getLatitude() + "");
                Log.e("Longitude", geocodeLatLng.getLongitude() + "");

                latitude = geocodeLatLng.getLatitude() + "";
                longitude = geocodeLatLng.getLongitude() + "";

                getLocalData().setMyLatitude(latitude);
                getLocalData().setMyLongitude(longitude);

                float range = getLocalData().getMaxKmRange() / 1000f;
                Call<ListOfUserModel> apiCall = getRetrofitInstance().getNearByUsers(getLocalData().getUserId(), getLocalData().getMyLatitude(), getLocalData().getMyLongitude(), String.valueOf(range));
                apiCall.enqueue(new Callback<ListOfUserModel>() {
                    @Override
                    public void onResponse(Call<ListOfUserModel> call, Response<ListOfUserModel> response) {
                        if (response.body().getStatus() && response.body().getUsersList() != null && !response.body().getUsersList().isEmpty()) {
                            Log.e("--- NEAR BY DATA SIZE", response.body().getUsersList().size() + "-------------------");
                            alData.clear();
                            alData.addAll(response.body().getUsersList());
                            getDataBinder().txtvVenueUsersCount.setText(String.valueOf(alData.size()));
                            // preparing button to show data in a listview
                            getDataBinder().llVenueViewers.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    UsersListActivity.start(getActivityG(), alData);
                                }
                            });
                        } else {
                            Log.e("--- NEAR BY DATA SIZE", "---------  NO DATA ----------");
                        }
                    }

                    @Override
                    public void onFailure(Call<ListOfUserModel> call, Throwable t) {
                        Log.e("--- NEAR BY --- ERROR", t.getMessage());
                    }
                });
            }
        });
        if (location.mGoogleApiClient != null) {
            location.mGoogleApiClient.connect();
        }
    }

    private ProfileApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(ProfileApi.class);
    }

    public void showAudioHost(View view) {
        CustomDialog.getInstance().showAudioHosts(LectureMonitoringActivity.this);
    }

    public void startRecording() {
        startRecord();
    }

    public void pauseRecording() {
        if (mRecorder.isRecording()) {
            mIsRecord = false;
            mRecorder.setPause(true);
            isRecordingBoolean = false;
//            getView().recordingPaused();
        } else {
            startRecord();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.RequestCode.INTEREST_SELECTION) {
                commaSeparatedIds = "";
                commaSeparatedNames = "";
                ArrayList<ChildCategoryModel> alCategories = (ArrayList<ChildCategoryModel>) data.getSerializableExtra(Constants.Extras.DATA);
                for (int i = 0; i < alCategories.size(); i++) {
                    commaSeparatedIds = commaSeparatedIds + (commaSeparatedIds.isEmpty() ? ""
                            : ",") + alCategories.get(i).getId();
                    commaSeparatedNames = commaSeparatedNames + (
                            commaSeparatedNames.isEmpty() ? ""
                                    : ", ") + alCategories.get(i).getCategory();
                }
                getDataBinder().txtvLectureCategory.setText(commaSeparatedNames);

                new LectureEventClicks().setLectureInterests((Activity) getActivityG(), LVApplication.getRetrofit().create(EventLectureApi.class), mLectureViewModel.getEvent_id(),
                        commaSeparatedIds, "event", new CallBackG<Boolean>() {
                            @Override
                            public void onCallBack(Boolean output) {
                                hideLoading();
                                if (output) {
                                    UtillsG.showToast("Updated successfully", getActivityG(), true);
                                }
                            }
                        });

            }
        }
    }

    public void startTimer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //checking if recording is starting from start
                if (((TextView) findViewById(R.id.txtvTimer)).getText().toString().trim().equalsIgnoreCase("record")) {
                    seconds = 0;
                    minutes = 0;
                    hours = 0;
                }
                seconds++;
                if (seconds == 60) {
                    seconds = 0;
                    minutes++;
                    if (minutes == 60) {
                        minutes = 0;
                        hours++;
                    }
                }
                if (minutes == 0) {
                    ((TextView) findViewById(R.id.txtvTimer)).setText("00:" + (seconds < 10
                            ? ("0" + seconds)
                            : seconds));
                    ((TextView) findViewById(R.id.txtvTimerVideo)).setText("00:" + (seconds < 10
                            ? ("0" + seconds)
                            : seconds));
                } else if (hours == 0 && minutes > 0) {
                    ((TextView) findViewById(R.id.txtvTimer)).setText((minutes < 10
                            ? ("0" + minutes)
                            : minutes) + ":" + (
                            seconds < 10 ? ("0" + seconds) : seconds));
                    ((TextView) findViewById(R.id.txtvTimerVideo)).setText((minutes < 10
                            ? ("0" + minutes)
                            : minutes) + ":" + (
                            seconds < 10 ? ("0" + seconds) : seconds));
                } else if (hours > 0) {
                    ((TextView) findViewById(R.id.txtvTimer)).setText(hours + ":" + (minutes < 10
                            ? ("0" + minutes)
                            : minutes));
                    ((TextView) findViewById(R.id.txtvTimerVideo)).setText(hours + ":" + (minutes < 10
                            ? ("0" + minutes)
                            : minutes));
                }
                try {
                    if (isRecordingBoolean) {
                        startTimer();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1000);

    }


    @Override
    public LectureDetailsModel getEventModel() {
        return mLectureViewModel;
    }

    private void startRecord() {
        if (mIsPlay) {
            mIsPlay = false;
            pausePlayer();
        }

        if (filePath.isEmpty()) {
            filePath = createAudioFile("code_cops" + System.currentTimeMillis() + ".m4a").getAbsolutePath();
            getPresenter().setAudioFilePath(filePath);
        }
        if (mRecorder == null) {
            mRecorder = new MP3Recorder(new File(filePath));
            mRecorder.stop();
        } else {
            if (mRecorder.isPause()) {
                mIsRecord = true;
                mRecorder.setPause(false);
//                getView().recordingInProgress();
                isRecordingBoolean = true;
                getDataBinder().audioWave.setBaseRecorder(mRecorder);
                getDataBinder().audioWave.startView();

                return;
            }
        }

        int size = getScreenWidth(LectureMonitoringActivity.this) / dip2px(LectureMonitoringActivity.this, 1);
        mRecorder.setDataList(getDataBinder().audioWave.getRecList(), size);

        mRecorder.setErrorHandler(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == MP3Recorder.ERROR_TYPE) {
                    resolveErrorDelete();
                    filePath = "";
                    mRecorder = null;
                    return;
                }
            }
        });

        //audioWave.setBaseRecorder(mRecorder);

        try {
            mRecorder.start();
            mIsRecord = true;

//            getView().recordingInProgress();
//            isRecording.set(true);
            isRecordingBoolean = true;
//            isRecorded.set(true);
            getDataBinder().audioWave.setBaseRecorder(mRecorder);
            getDataBinder().audioWave.startView();
        } catch (IOException e) {
            e.printStackTrace();
            resolveErrorDelete();
        }
    }

    private void pausePlayer() {
        if (audioPlayer != null) {
            audioPlayer.pause();
        }
    }

    public File createAudioFile(String fileName) {
        File newFile = new File(new FileHelper().initFolderAudio(), fileName);
        return newFile;
    }

    private void resolveErrorDelete() {
        stopAndResetUI();
        mRecorder = null;
        FileUtils.deleteFile(filePath);
        filePath = "";
    }

    protected void stopAndResetUI() {
        if (mIsRecord) {
            StopRecord();
//            getView().recordingPaused();
        }
        if (mRecorder != null) {
            mRecorder.stop();
        }
        isRecordingBoolean = true;
        getDataBinder().audioWave.stopView();
        if (mIsPlay) {
            mIsPlay = false;
            audioPlayer.pause();
        }
    }

    private void StopRecord() {
        if (mRecorder != null && mRecorder.isRecording()) {
            mRecorder.setPause(false);
            mRecorder.stop();
            getDataBinder().audioWave.stopView();
        }
        mIsRecord = false;
    }


    public static int getScreenWidth(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.widthPixels;
    }

    public static int dip2px(Context context, float dipValue) {
        float fontScale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * fontScale + 0.5f);
    }

}
