/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.LoginRegisteration;

import java.io.Serializable;
import java.util.List;

import gilm.lecture.verb.WebServices.BasicApiModel;

/**
 * Created by G-Expo on 09 Aug 2017.
 */

public class UserDataModel extends BasicApiModel {
    private UserData data;

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }

    public class UserData implements Serializable {
        private List<UserInterests> user_interest;
        private String is_admin;
        private String role_id = "";
        private String password;
        private String device_type;
        private String device_token;
        private String device_name;
        private String mac_address;
        private String signal_strength;
        private String microphone;
        private String login_status;
        private String status;
        private String is_deleted;
        private String create_on;
        private String updated_on;
        private String deleted_on;
        private String verification_code;
        private String allow_notification;
        private String distance;
        private boolean isReadyToRecord;

        private String user_id,
                location,
                lat,
                lang,
                website,
                bio;
        private String full_name;
        private String email;
        private String profile_pic, cover_pic;
        private String social_id;
        private String register_via;
        private String is_following = "";

        private String Followings_count;
        private String Followers_count;
        private boolean Is_following;
        private boolean Is_approved;

        private String is_blocked;

        public boolean isReadyToRecord() {
            return isReadyToRecord;
        }

        public void setReadyToRecord(boolean readyToRecord) {
            isReadyToRecord = readyToRecord;
        }

        public boolean isIs_approved() {
            return Is_approved;
        }

        public boolean getIs_admin() {
            return is_admin.equals("1");
        }

        public void setIs_admin(String is_admin) {
            this.is_admin = is_admin;
        }

        public void setIs_approved(boolean is_approved) {
            Is_approved = is_approved;
        }

        public String getFollowings_count() {
            return Followings_count;
        }

        public void setFollowings_count(String followings_count) {
            Followings_count = followings_count;
        }

        public String getFollowers_count() {
            return Followers_count;
        }

        public void setFollowers_count(String followers_count) {
            Followers_count = followers_count;
        }

        public boolean is_following() {
            return Is_following;
        }

        public void setIs_following(boolean is_following) {
            Is_following = is_following;
        }

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public String getDevice_name() {
            return device_name;
        }

        public void setDevice_name(String device_name) {
            this.device_name = device_name;
        }

        public String getMac_address() {
            return mac_address;
        }

        public void setMac_address(String mac_address) {
            this.mac_address = mac_address;
        }

        public String getSignal_strength() {
            return signal_strength;
        }

        public void setSignal_strength(String signal_strength) {
            this.signal_strength = signal_strength;
        }

        public String getMicrophone() {
            return microphone;
        }

        public void setMicrophone(String microphone) {
            this.microphone = microphone;
        }

        public String getLogin_status() {
            return login_status;
        }

        public void setLogin_status(String login_status) {
            this.login_status = login_status;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getIs_deleted() {
            return is_deleted;
        }

        public void setIs_deleted(String is_deleted) {
            this.is_deleted = is_deleted;
        }

        public String getCreate_on() {
            return create_on;
        }

        public void setCreate_on(String create_on) {
            this.create_on = create_on;
        }

        public String getUpdated_on() {
            return updated_on;
        }

        public void setUpdated_on(String updated_on) {
            this.updated_on = updated_on;
        }

        public String getDeleted_on() {
            return deleted_on;
        }

        public void setDeleted_on(String deleted_on) {
            this.deleted_on = deleted_on;
        }

        public String getVerification_code() {
            return verification_code;
        }

        public void setVerification_code(String verification_code) {
            this.verification_code = verification_code;
        }

        public String getAllow_notification() {
            return allow_notification;
        }

        public void setAllow_notification(String allow_notification) {
            this.allow_notification = allow_notification;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getIs_following() {
            return is_following;
        }

        public void setIs_following(String is_following) {
            this.is_following = is_following;
        }

        public String getCover_pic() {
            return cover_pic;
        }

        public void setCover_pic(String cover_pic) {
            this.cover_pic = cover_pic;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLang() {
            return lang;
        }

        public void setLang(String lang) {
            this.lang = lang;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }

        public String getSocial_id() {
            return social_id;
        }

        public void setSocial_id(String social_id) {
            this.social_id = social_id;
        }

        public String getRegister_via() {
            return register_via;
        }

        public void setRegister_via(String register_via) {
            this.register_via = register_via;
        }

        public String getIs_blocked() {
            return is_blocked;
        }

        public void setIs_blocked(String is_blocked) {
            this.is_blocked = is_blocked;
        }

        public List<UserInterests> getUser_interest() {
            return user_interest;
        }

        public void setUser_interest(List<UserInterests> user_interest) {
            this.user_interest = user_interest;
        }
    }

    public class UserInterests implements Serializable {
        String id = "";
        String user_id = "";
        String category_id = "";
        String category_name = "";

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }
    }

}
