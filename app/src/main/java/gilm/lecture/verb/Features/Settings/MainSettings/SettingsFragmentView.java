/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Settings.MainSettings;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 20 Jul 2017.
 */

public interface SettingsFragmentView extends Viewable<SettingsFragmentPresenter>
{
}
