/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Player.CommentsDiscussion.Comments;

import android.databinding.ObservableField;
import android.view.View;

/**
 * Created by G-Expo on 16 Aug 2017.
 */

public interface CommentBinder
{
    /**
     * @return {@link String} message ,which is bind with the enter message edit text.
     */
    ObservableField<String> textMessage();

    /**
     * send String comment,what ever is in {@link #textMessage()}
     */
    void sendMessage(View view);

    /**
     * send audio comment
     */
    void sendAudio(View view);

}
