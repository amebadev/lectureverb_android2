package gilm.lecture.verb.Features.LoginRegisteration.Registration;

import android.databinding.ObservableField;
import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.Features.LoginRegisteration.LoginRegisterApi;

/**
 * Created by G-Expo on 05 Jul 2017.
 */

public class RegistrationPresenter extends BasePresenter<RegistrationView> implements RegistrationPresenterBinder
{
    public ObservableField<String> userName         =
            new ObservableField<>();
    public ObservableField<String> email            =
            new ObservableField<>();
    public ObservableField<String> password         =
            new ObservableField<>();
    public ObservableField<String> confirm_password =
            new ObservableField<>();

    public RegistrationPresenter()
    {
        userName.set("");
        email.set("");
        password.set("");
        confirm_password.set("");
    }

    /**
     * register user on server with filled details.
     */
    public void registerClicked(View view)
    {
        if (userName.get().isEmpty()) {
            getView().displayError("Please enter name");
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.get()).matches()) {
            getView().displayError("Please enter a valid email");
        }
        else if (password.get().isEmpty()) {

            getView().displayError("Please enter password");
        }
        else if (confirm_password.get().isEmpty()) {
            getView().displayError("Please enter confirm password");

        }
        else if (!confirm_password.get().equals(password.get())) {
            getView().displayError("Password not matched");

        }
        else {

            getView().showLoading("Please Wait..");
            createApiRequest(getRetrofitInstance().register(userName.get(), email.get(), "android", getView().getLocalData().getUserType(), password.get()), new CallBackG<UserDataModel>()
            {
                @Override
                public void onCallBack(UserDataModel output)
                {
                    getView().hideLoading();
                    if (output.getStatus()) {
//                        all done ,just verify on server.
                        getView().getLocalData().setIsRegistered(true);

                    }
                    else {
                        getView().displayError(output.getMessage());
                    }


                    if (output.getData() != null) {
                        getView().getLocalData().setUserData(output.getData());
                        getView().openEmailVerification();
                    }

                }
            });

        }
    }

    private LoginRegisterApi getRetrofitInstance()
    {
        return LVApplication.getRetrofit().create(LoginRegisterApi.class);
    }

    @Override
    public void skipClicked(View view)
    {
        getView().getLocalData().setIsRegistered(false);
        getView().openNavigationActivity();
    }

    @Override
    public ObservableField<String> getPassword()
    {
        return password;
    }

    @Override
    public ObservableField<String> getUserName()
    {
        return userName;
    }

    @Override
    public ObservableField<String> getEmail()
    {
        return email;
    }

    @Override
    public ObservableField<String> getConfirmPassword()
    {
        return confirm_password;
    }
}
