package gilm.lecture.verb.Features.Home.RecentActivity;

import android.content.Context;

import java.util.List;

import gilm.lecture.verb.Features.Home.Notification.NotificationViewModel;
import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.R;
import gilm.lecture.verb.databinding.InflatorRecentActivitiesBinding;

/**
 * Created by G-Expo on 17 Jul 2017.
 */

public class RecentActivitiesAdapter extends InfiniteAdapterG<InflatorRecentActivitiesBinding>
{
//    private LayoutInflater inflater;

    private Context con;

    private List<NotificationViewModel> dataList;

    public RecentActivitiesAdapter(Context context, List<NotificationViewModel> dList)
    {
        this.dataList = dList;
        con = context;
//        inflater = LayoutInflater.from(context);
    }

    @Override
    protected void bindData(int position, MyViewHolderG myViewHolderG)
    {
        myViewHolderG.binding.setData(dataList.get(position));
        myViewHolderG.binding.executePendingBindings();
    }

    @Override
    public int getCount()
    {
        return dataList.size();
    }

    @Override
    public int getViewType(int position)
    {
        return 1;
    }

    @Override
    public int getInflateLayout()
    {
        return R.layout.inflator_recent_activities;
    }

}