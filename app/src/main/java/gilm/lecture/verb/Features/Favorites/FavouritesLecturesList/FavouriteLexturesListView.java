package gilm.lecture.verb.Features.Favorites.FavouritesLecturesList;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * created by PARAMBIR SINGH on 26/9/17.
 */

public interface FavouriteLexturesListView extends Viewable<FavouriteLecturesListPresenter>
{

    View getProgressBar();

    RecyclerView getRecyclerView();

    TextView getNoDataTextview();
}
