package gilm.lecture.verb.Features.Connect.ConnectNearBy.UsersList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;

import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.Constants;

public class UsersListActivity extends AppCompatActivity {

    RecyclerView recyUsersList;

    public static void start(Context context, ArrayList<UserDataModel.UserData> alData) {
        Intent starter = new Intent(context, UsersListActivity.class);
        starter.putExtra(Constants.Extras.DATA, alData);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reacted_users);
        setupToolbar("Near By Users");

        recyUsersList = (RecyclerView) findViewById(R.id.recyUsersList);
        recyUsersList.setLayoutManager(new GridLayoutManager(this, 2));

        UsersListAdapter adapter = new UsersListAdapter(this, (ArrayList<UserDataModel.UserData>) getIntent().getSerializableExtra(Constants.Extras.DATA), null);
        recyUsersList.setAdapter(adapter);
        adapter.setShouldLoadMore(false);
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.title);
        if (viewById == null) {
            viewById = (TextView) toolbar.findViewById(R.id.title);
        }

        if (viewById != null) {
            viewById.setText(title);
            getSupportActionBar().setTitle("");
        } else {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
