package gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EditLectureEvent;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;

/**
 * Created by G-Expo on 07 Jul 2017.
 */

public interface EditLectureEventView extends Viewable<EditLectureEventPresenter>
{
    String getCoHostId();

    String getLecturerId();

    String getSelectedGroupId();
}
