package gilm.lecture.verb.Features.PaypalPayment;

import java.io.Serializable;

/**
 * Created by Parambir Singh on 08-Jul-18.
 */

public class PaymentPriceModel implements Serializable
{
    private String id = "";
    private String payment_type = "";
    private String amount = "";
    private String expire_date = "";

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getPayment_type()
    {
        return payment_type;
    }

    public void setPayment_type(String payment_type)
    {
        this.payment_type = payment_type;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }

    public String getExpire_date()
    {
        return expire_date;
    }

    public void setExpire_date(String expire_date)
    {
        this.expire_date = expire_date;
    }
}
