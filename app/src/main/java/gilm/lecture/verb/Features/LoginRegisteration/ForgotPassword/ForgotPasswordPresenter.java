package gilm.lecture.verb.Features.LoginRegisteration.ForgotPassword;

import android.databinding.ObservableField;
import android.view.View;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.Features.LoginRegisteration.LoginRegisterApi;

/**
 * Created by G-Expo on 05 Jul 2017.
 */

public class ForgotPasswordPresenter extends BasePresenter<ForgotPasswordView> implements ForgotPasswordBinder, CallBackG<BasicApiModel>
{
    public final ObservableField<String> email =
            new ObservableField<>();

    public ForgotPasswordPresenter()
    {
        email.set("");
    }

    @Override
    public void submitEmail(View view)
    {
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.get()).matches()) {
            getView().displayError("Please enter a valid email");
        }
        else {
            getView().showLoading("Please Wait");
            createApiRequest(getRetrofitInstance().forgotPassword(email.get()), this);
        }
    }

    protected LoginRegisterApi getRetrofitInstance()
    {
        return LVApplication.getRetrofit().create(LoginRegisterApi.class);
    }

    @Override
    public ObservableField<String> getEmail()
    {
        return email;
    }

    @Override
    public void onCallBack(BasicApiModel output)
    {
        getView().hideLoading();
        if (output.getStatus()) {
            getView().passwordSent();
        }
        UtillsG.showToast(output.getMessage(), getView().getActivityG(), true);
    }
}
