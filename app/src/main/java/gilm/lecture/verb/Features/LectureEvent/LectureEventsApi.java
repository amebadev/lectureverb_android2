package gilm.lecture.verb.Features.LectureEvent;

import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureEventModel;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.Web;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by harpreet on 9/15/17.
 */

public interface LectureEventsApi
{


    /*    URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
 API NAme => get_lecture_event
 Method = > GET / POST
 Description => Get all user lecture
 Parameaters = > page , user_id
 Code ==> 200 for success 400 for error
*/
    @Multipart
    @POST(Web.Path.get_lecture_event)
    Call<LectureEventModel> get_lecture_event(
            @Part("user_id") RequestBody user_id,
            @Part("action") RequestBody action
            , @Part("page") RequestBody page
    );




 /*   URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => get_event_invitation
    Method = > GET / POST
    Description => Get all invitations
    Parameaters = > user_id , page
    Code ==> 200 for success 400 for error*/

    @Multipart
    @POST(Web.Path.get_event_invitation)
    Call<LectureEventModel> get_event_invitation(
            @Part("user_id") RequestBody user_id
            , @Part("page") RequestBody page
    );



/*    URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => get_my_hosting_list
    Method = > GET / POST
    Description => Get my hosting list
    Parameaters = > user_id , page
    Code ==> 200 for success 400 for error*/

    @Multipart
    @POST(Web.Path.get_my_hosting_list)
    Call<LectureEventModel> get_my_hosting_list(
            @Part("user_id") RequestBody user_id
            , @Part("page") RequestBody page
    );

/*
    URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => event_attend_and_interest
    Method = > GET / POST
    Description => Updat event detail for user
    Parameaters = > user_id , event_id , attend_type=digital OR attending , action=interst OR attend , is_interested=true/false
    Code ==> 200 for success 400 for error*/

    @Multipart
    @POST(Web.Path.event_attend_and_interest)
    Call<BasicApiModel> event_attend_and_interest(
            @Part("user_id") RequestBody user_id
            , @Part("event_id") RequestBody event_id
            , @Part("attend_type") RequestBody attend_type
            , @Part("action") RequestBody action
            , @Part("is_interested") RequestBody is_interested
    );


 /*   URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => send_event_invitation
    Method = > GET / POST
    Description => Send event invitations
    Parameaters = > sender_id, receiver_id , event_id
    Code ==> 200 for success 400 for error*/

    @Multipart
    @POST(Web.Path.send_event_invitation)
    Call<BasicApiModel> send_event_invitation(
            @Part("sender_id") RequestBody sender_id
            , @Part("receiver_id") RequestBody receiver_id
            , @Part("event_id") RequestBody event_id
    );

    /*  URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
     API NAme => get_single_lecture_event
     Method = > GET / POST
     Description => Get single lecture
     Parameaters = > lecture_id
     Code ==> 200 for success 400 for error
 */
    @FormUrlEncoded
    @POST(Web.Path.get_single_lecture_event)
    Call<LectureEventModel> get_single_lecture_event(
            @Field("event_id") String event_id, @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST(Web.Path.mark_event_completed)
    Call<BasicApiModel> mark_event_completed(
            @Field("event_id") String event_id
    );




 /*   URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
    API NAme => search_lecture_event
    Method = > GET / POST
    Description => Search events
    Parameaters = > keyword , page
    Code ==> 200 for success 400 for error*/

    @FormUrlEncoded
    @POST(Web.Path.search_lecture_event)
    Call<LectureEventModel> search_lecture_event(
            @Field("keyword") String keyword,
            @Field("page") String page,
            @Field("user_id") String user_id
    );

     /*   URL => http://112.196.34.43/lecture_verb/app/lecture_verb.php
   shared_id:24
    user_id:18
    share_type:event for lecture event , lecture for quick lecture
    lectureverb:share*/

    @FormUrlEncoded
    @POST(Web.Path.share)
    Call<BasicApiModel> share(
            @Field("user_id") String sender_id
            , @Field("shared_id") String receiver_id
            , @Field("share_type") String event_id
    );

    @FormUrlEncoded
    @POST(Web.Path.send_direct_notification)
    Call<BasicApiModel> send_direct_notification(
            @Field("receiver_id") String receiver_id
            , @Field("flag") String flag
            , @Field("extra_data") String extra_data
    );


}
