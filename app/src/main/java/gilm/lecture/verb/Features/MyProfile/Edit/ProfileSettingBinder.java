/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.MyProfile.Edit;

import android.databinding.ObservableField;
import android.view.View;

import gilm.lecture.verb.Features.Settings.SettingsBaseBinder;

/**
 * Created by G-Expo on 21 Jul 2017.
 */

public interface ProfileSettingBinder extends SettingsBaseBinder
{
    void changeProfileImage(View view);

    void changeCoverProfileImage(View view);

    /**
     * select location for profile ,using place picker.
     *
     * @param view
     */
    void selectLocation(View view);

    /**
     * @return profile image URL,of current logged in user.
     */
    ObservableField<String> bio();

    /**
     * @return profile image URL,of current logged in user.
     */
    ObservableField<String> location();

    /**
     * @return profile image URL,of current logged in user.
     */
    ObservableField<String> website();
    ObservableField<String> interests();
    void selectInterests(View view);
}
