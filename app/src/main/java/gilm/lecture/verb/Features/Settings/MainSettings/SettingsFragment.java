/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Settings.MainSettings;

import android.support.v4.app.Fragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.NaviagtionDrawer.ProfileUpdateBus;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.databinding.FragmentSettingsV2Binding;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends BaseFragment<FragmentSettingsV2Binding, SettingsFragmentPresenter> implements SettingsFragmentView
{

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_settings_v2;
    }

    @Override
    protected void onCreateFragmentG() {
        injectPresenter(new SettingsFragmentPresenter());
        getPresenter().attachView(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
    public void initViews() {
        getDataBinder().setBinder(getPresenter());

//        UtillsG.setTextSizeByPercentage(getActivityG(), getDataBinder().txtvProfileSettings, 5f);
//        UtillsG.setTextSizeByPercentage(getActivityG(), getDataBinder().txtvHelp, 5f);
//        UtillsG.setTextSizeByPercentage(getActivityG(), getDataBinder().txtvLogout, 5f);
//        UtillsG.setTextSizeByPercentage(getActivityG(), getDataBinder().txtvMessageSettings, 5f);
//        UtillsG.setTextSizeByPercentage(getActivityG(), getDataBinder().txtvNotificationsSettings, 5f);
//        UtillsG.setTextSizeByPercentage(getActivityG(), getDataBinder().txtvTagsSettings, 5f);
        UtillsG.setHeightWidthWithRatio_LinearLayout(getActivity(), getDataBinder().frameProfileImage, 3f, 6f);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void profileUpdated(ProfileUpdateBus data) {
        getPresenter().refreshUserData();
    }
}
