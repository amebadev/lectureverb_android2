package gilm.lecture.verb.Features.Help;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.InfiniteAdapter_WithoutBuinding;
import gilm.lecture.verb.UtilsG.LoadingViewHolder;

/**
 * created by PARAMBIR SINGH on 31/8/17.
 */

class QuestionAnswerAdapter extends InfiniteAdapter_WithoutBuinding<QuestionAnswerAdapter.MyViewHolderG>
{
    private LayoutInflater inflater;
    List<QuestionAnswerModel> data;
    Fragment fragment;

    public QuestionAnswerAdapter(List<QuestionAnswerModel> data, Fragment fragment) {
        this.data = data;
        this.fragment = fragment;
        inflater = LayoutInflater.from(fragment.getActivity());
    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        return new LoadingViewHolder(inflater.inflate(R.layout.layout_progress, parent, false));
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public MyViewHolderG onCreateView(ViewGroup parent, int viewType) {
        return new MyViewHolderG(inflater.inflate(R.layout.inflater_question_answers, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        try {
            if (!(holder instanceof LoadingViewHolder)) {
                if (fragment instanceof HelpFragment) {
                    ((MyViewHolderG) holder).txtvQuestion.setText(Html.fromHtml(data.get(position).getQuestion().replaceAll(((HelpFragment) fragment).searchString.trim(), "<font color='yellow'>" + ((HelpFragment) fragment).searchString.trim() + "</font>")));
                    ((MyViewHolderG) holder).txtvAnswer.setText(Html.fromHtml(data.get(position).getAnswer().replaceAll(((HelpFragment) fragment).searchString.trim(), "<font color='yellow'>" + ((HelpFragment) fragment).searchString.trim() + "</font>")));
                }
                else {
                    ((MyViewHolderG) holder).txtvQuestion.setText(data.get(position).getQuestion());
                    ((MyViewHolderG) holder).txtvAnswer.setText(data.get(position).getAnswer());
                }

                ((MyViewHolderG) holder).txtvAnswer.setVisibility(View.GONE);
                ((MyViewHolderG) holder).view.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view) {
                        DialogHelper.getInstance().showInformation(fragment.getActivity(), data.get(position).getAnswer(), new CallBackG<String>()
                        {
                            @Override
                            public void onCallBack(String output) {

                            }
                        });
                    }
                });
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    class MyViewHolderG extends RecyclerView.ViewHolder
    {
        View view;
        TextView txtvQuestion, txtvAnswer;

        public MyViewHolderG(View row) {
            super(row);

            view = row;
            txtvQuestion = view.findViewById(R.id.txtvQuestion);
            txtvAnswer = (TextView) view.findViewById(R.id.txtvAnswer);
        }
    }

}
