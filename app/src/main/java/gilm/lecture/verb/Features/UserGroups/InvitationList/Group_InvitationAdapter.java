package gilm.lecture.verb.Features.UserGroups.InvitationList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import gilm.lecture.verb.Features.Favorites.FavouritesLecturesList.FavouriteLecturesListActivity;
import gilm.lecture.verb.Features.Home.Home.HomeFragment_NoBinding;
import gilm.lecture.verb.Features.OtherProfile.ActivityOtherUserDetails;
import gilm.lecture.verb.Features.UserGroups.GroupModel;
import gilm.lecture.verb.Features.UserGroups.UserGroupApis;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.BitmapUtils.ImageLoader;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ParamBir Singh on 06 Jul 2017.
 *
 * @see Group_InvitationAdapter is used in 2 classes one is {@link HomeFragment_NoBinding} and another one is {@link FavouriteLecturesListActivity}
 */

public class Group_InvitationAdapter extends RecyclerView.Adapter<Group_InvitationAdapter.GroupViewHolder>
{
    private final List<InvitationModel> invitationsListModel;
    private LayoutInflater inflater;
    private CallBackG<GroupModel> selectionCallBack;

    Context context;


    public Group_InvitationAdapter(List<InvitationModel> invitationsListModel, Context context) {
        this.invitationsListModel = invitationsListModel;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new GroupViewHolder(inflater.inflate(R.layout.invitationinflator, parent, false));
    }

    @Override
    public void onBindViewHolder(final GroupViewHolder holder, final int position) {

        ImageLoader.setImageSmallCentreCrop(holder.imgvUserImage, UtillsG.getNotNullString(invitationsListModel.get(position).getUserData().getProfile_pic(), "null"));

        // holder.txtvGroupName.setText(userData.get(position).getGroup_title());
        holder.textFullName.setText(invitationsListModel.get(position).getUserData().getFull_name());
        holder.txtvGroupName.setText(Html.fromHtml("Invited in group: <b>" + invitationsListModel.get(position).getGroupdata().getGroup_title() + "</b>"));
       holder.view.setOnClickListener(new View.OnClickListener()
       {
           @Override
           public void onClick(View view) {
               ActivityOtherUserDetails.start(context,invitationsListModel.get(position).getUserData().getUser_id(),"User Details");
           }
       });
        holder.txtvAccept.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
               // UtillsG.showLoading("Please wait..", context);
                Call<BasicApiModel> call = LVApplication.getRetrofit().create(UserGroupApis.class).approve_invite(
                        invitationsListModel.get(position).getUserData().getUser_id(),
                        invitationsListModel.get(position).getGroupdata().getId(),
                        new SharedPrefHelper(context).getUserId(),
                        invitationsListModel.get(position).getGroupdata().getGroup_title()

                );
                call.enqueue(new Callback<BasicApiModel>()
                {
                    @Override
                    public void onResponse(Call<BasicApiModel> call, final Response<BasicApiModel> response) {
                      //  UtillsG.hideLoading();
//                        UtillsG.showToast(response.body().getMessage(), context, true);

                        DialogHelper.getInstance().showInformation(context, response.body().getMessage(), new CallBackG<String>()
                        {
                            @Override
                            public void onCallBack(String output) {
                                if (response.body().getStatus()) {
                                    ((GroupInvitationListActivity) context).showInvitationList();
                                }

                            }
                        });

                    }

                    @Override
                    public void onFailure(Call<BasicApiModel> call, Throwable t) {
                      //  UtillsG.hideLoading();
                        UtillsG.showToast(t.getMessage() + "", context, true);
                    }
                });
            }
        });
    }


    @Override
    public int getItemCount() {
        return invitationsListModel.size();
    }

    class GroupViewHolder extends RecyclerView.ViewHolder
    {
        View view;
        ImageView imgvUserImage;
        TextView txtvGroupName, textFullName, txtvAccept;

        public GroupViewHolder(View row) {
            super(row);

            view = row;
            imgvUserImage = view.findViewById(R.id.imgvUserImage);
            txtvGroupName = view.findViewById(R.id.textGroupname);
            textFullName = view.findViewById(R.id.textFullName);
            txtvAccept = view.findViewById(R.id.txtvAccept);
        }
    }
}