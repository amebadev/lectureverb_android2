package gilm.lecture.verb.Features.MonitorLecture;

import android.content.Context;
import android.content.Intent;

import gilm.lecture.verb.Features.Internal.Base.BaseActivity;
import gilm.lecture.verb.R;
import gilm.lecture.verb.databinding.ActivityTestBinding;

public class TestActivity extends BaseActivity<ActivityTestBinding,TestPresenter>implements TestView{
    public static void start(Context context) {
        Intent starter = new Intent(context, TestActivity.class);
        context.startActivity(starter);
    }
    @Override
    public void initViews() {
    }

    @Override
    public Context getActivityG() {
        return TestActivity.this;
    }

    @Override
    protected int setLayoutId() {
    return R.layout.activity_test;
    }

    @Override
    protected void onCreateActivityG() {
        injectPresenter(new TestPresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void finiss() {
        finish();
    }


}