/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Settings.Notifications;

import gilm.lecture.verb.Features.Settings.SettingBasePresenter;

/**
 * Created by G-Expo on 20 Jul 2017.
 */

public class NotificationSettingsPresenter extends SettingBasePresenter<NotificationSettingsView>
{
    public NotificationSettingsPresenter()
    {
        super();
    }
}
