/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.QuickLecture.Presentation.PresentationOptions;

import android.app.Activity;
import android.net.Uri;
import android.util.Log;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureApi;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import gilm.lecture.verb.WebServices.ProgressRequestBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 20 Jul 2017.
 */

public class PresentationOptionsPresenter extends BasePresenter<PresentationOptionsView> implements ProgressRequestBody.UploadCallbacks
{

    String discoveryUrl = "", pptPath = "",commaSeparatedIds="";
    public String artWorkImage = "";

    public void setDiscoveryUrl(String discoveryUrl) {
        this.discoveryUrl = discoveryUrl;
    }

    public void setPPTPath(String pptPath) {
        this.pptPath = pptPath;
    }

    public void setLectureTitle(String lectureTitle) {
    }


    public void setSelectedLectureCategories( String Ids){
        commaSeparatedIds=Ids;
    }

    public void uploadPptToServer() {
        getView().showLoading("Please wait till PPT gets prepared..");

        File pptFile = new File(pptPath);
        Call<PptImagesModel> basicApiModelCall = getRetrofitInstance().uploadVideoLecture(getFilePart(pptFile),
                getOtherParams(new SharedPrefHelper(getView().getActivityG()).getUserId()));
        basicApiModelCall.enqueue(new Callback<PptImagesModel>()
        {
            @Override
            public void onResponse(Call<PptImagesModel> call, Response<PptImagesModel> response) {
                if(getView()!=null) {
                    getView().hideLoading();
                    if (response.body().getStatus()) {
                        getView().displayError("PPt successfully converted to Images.");
                        getView().PPT_into_Images_Done(response.body().getDownload_file());
                    } else {
                        getView().displayError("Error in Jason response, Pls try later.");
                    }
                }
            }

            @Override
            public void onFailure(Call<PptImagesModel> call, Throwable t) {
                if(getView()!=null)  getView().hideLoading();
                Log.e("Error is", t.toString());
                getView().displayError("Error in converting PPT into Images. Please try later");
            }
        });


    }


    private MultipartBody.Part getFilePart(File file) {
        ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("uploadfile", file.getName(), fileBody);
        return filePart;
    }

    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }


    private QuickLectureApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(QuickLectureApi.class);
    }

    @Override
    public void onProgressUpdate(int percentage) {
//        UtillsG.showToast(percentage + "%", getView().getActivityG(), true);
    }

    @Override
    public void onError() {
        UtillsG.showToast("Error in Process", getView().getActivityG(), true);
    }

    @Override
    public void onFinish() {
        UtillsG.showToast("Process Finished", getView().getActivityG(), true);
    }

    public void cropMeImage(Uri uri) {
        CropImage.activity(uri)
                .setAspectRatio(16, 9)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start((Activity) getView().getActivityG());
    }

    public void setArtWorkImage(String artWorkImage) {
        this.artWorkImage = artWorkImage;
    }
}
