package gilm.lecture.verb.Features.MyProfile.View.RepostedPosts;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Home.Home.HomeAdapter_NoBinding;
import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.Features.Home.RefreshQuickLectureBus;
import gilm.lecture.verb.Features.LoginRegisteration.UserDataModel;
import gilm.lecture.verb.Features.MyProfile.ProfileApi;
import gilm.lecture.verb.Features.Settings.ArrayListLectureModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.EndlessRecyclerOnScrollListener;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.WebServices.LVApplication;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harpreet on 10/13/17.
 */

public class RepostedPostsFragment extends Fragment
{

    RecyclerView recyclerView;
    int pageNumber = 1;
    List<Lecture_And_Event_Model> alModel = new ArrayList<>();
    private HomeAdapter_NoBinding homeAdapter;
    ProgressBar progressBar;
    LinearLayoutManager linearLayoutManager;
    Activity mActivity;
    UserDataModel.UserData mUserData = null;
    String userId = "";
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;


    public static RepostedPostsFragment newInstance(UserDataModel.UserData mUserData) {
        return new RepostedPostsFragment(mUserData);
    }

    public RepostedPostsFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public RepostedPostsFragment(UserDataModel.UserData mUserData) {
        // Required empty public constructor
        this.mUserData = mUserData;
    }


    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.VISIBLE);


        endlessRecyclerOnScrollListener.reset();
        pageNumber = 1;
        loadHomeData(userId, pageNumber);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recyclerlist_no_binding, container, false);
        mActivity = getActivity();

        if (mUserData != null) {
            userId = mUserData.getUser_id();
        }
        else {
            userId = new SharedPrefHelper(mActivity).getUserId();
        }
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        homeAdapter = new HomeAdapter_NoBinding(null,alModel, mActivity);
        recyclerView.setAdapter(homeAdapter);


        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager)
        {
            @Override
            public void onLoadMore(int current_page) {
                pageNumber++;
                loadHomeData(userId, pageNumber);
            }

            @Override
            public void countChange(int currentVisibleCount) {

            }
        };
        recyclerView.setOnScrollListener(endlessRecyclerOnScrollListener);


        return view;
    }


    public void loadHomeData(String user_id, final int pageNumber) {
        Call<ArrayListLectureModel> basicApiModelCall = LVApplication.getRetrofit().create(ProfileApi.class).get_my_posted_lectures(user_id, String.valueOf(pageNumber)
                , new SharedPrefHelper(getActivity()).getUserId());
        basicApiModelCall.enqueue(new Callback<ArrayListLectureModel>()
        {
            @Override
            public void onResponse(Call<ArrayListLectureModel> call, Response<ArrayListLectureModel> response) {
                progressBar.setVisibility(View.GONE);

                if (response.body() != null) {
                    if (response.body().getStatus()) {
                        Log.e("get_lectures : OUTPUT", String.valueOf(response.body().getListOfTagsModel()));
                        if (pageNumber == 1) {
                            alModel.clear();
                        }
                        alModel.addAll(response.body().getListOfTagsModel());
                        recyclerView.getAdapter().notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayListLectureModel> call, Throwable t) {
                Log.e("ERROR get_posted_le", t.getMessage().toString());
            }
        });

    }


    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    protected HomeApis getRetrofitInstance() {
        return LVApplication.getRetrofit().create(HomeApis.class);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void RefreshData(HomeFragmentUpdateBus data) {
        if (getView() != null) {
            pageNumber = 1;
            loadHomeData(userId, pageNumber);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void RefeshParticularQuickLecture(RefreshQuickLectureBus data) {
        if (getView() != null && alModel != null && alModel.size() > 0) {

            for (int i = 0; i < alModel.size(); i++) {
                if (alModel.get(i).getQuick_lecture_id() != null && alModel.get(i).getQuick_lecture_id().equals(data.getQuickLectureId())) {

                    if (data.getForLike()) {
                        int previousCount = Integer.parseInt(alModel.get(i).getFavourites_count());

                        if (data.getIs_actionTrue()) {
                            previousCount++;
                            alModel.get(i).setFavourites_count("" + previousCount);
                        }
                        else {
                            previousCount--;
                            alModel.get(i).setFavourites_count("" + previousCount);
                        }
                        recyclerView.getAdapter().notifyDataSetChanged();
                    }
                    else {
                        int previousCount = Integer.parseInt(alModel.get(i).getReposted_count());

                        if (data.getIs_actionTrue()) {
                            previousCount++;
                            alModel.get(i).setReposted_count("" + previousCount);
                        }
                        else {
                            previousCount--;
                            alModel.get(i).setReposted_count("" + previousCount);
                        }
                        recyclerView.getAdapter().notifyDataSetChanged();
                    }
                    return;
                }


            }

        }
    }

}
