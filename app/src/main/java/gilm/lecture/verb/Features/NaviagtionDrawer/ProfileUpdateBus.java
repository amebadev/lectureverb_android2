/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.NaviagtionDrawer;

/**
 * Created by G-Expo on 17 Aug 2017.
 */

public class ProfileUpdateBus
{
    private boolean profileUpdated;

    public ProfileUpdateBus(boolean profileUpdated)
    {
        this.profileUpdated = profileUpdated;
    }

    public boolean isProfileUpdated()
    {
        return profileUpdated;
    }

    public void setProfileUpdated(boolean profileUpdated)
    {
        this.profileUpdated = profileUpdated;
    }
}
