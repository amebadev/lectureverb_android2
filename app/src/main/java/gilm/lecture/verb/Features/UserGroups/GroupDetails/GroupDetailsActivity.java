package gilm.lecture.verb.Features.UserGroups.GroupDetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import gilm.lecture.verb.Features.UserGroups.CreateNewGroupAndSignUp.CreateGroupAndSignUpActivity;
import gilm.lecture.verb.Features.UserGroups.GroupModel;
import gilm.lecture.verb.Features.UserGroups.UserGroupApis;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupDetailsActivity extends AppCompatActivity
{
    GroupModel groupModel;

    public boolean isMeAdmin;

    public static void start(Context context, GroupModel groupModel) {
        Intent starter = new Intent(context, GroupDetailsActivity.class);
        starter.putExtra(Constants.Extras.DATA, groupModel);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_details);

        setupToolbar("Group Network");

        groupModel = (GroupModel) getIntent().getSerializableExtra(Constants.Extras.DATA);

        showGroupDetailsFragment(groupModel);
    }

    public void showGroupDetailsFragment(GroupModel groupModel_new) {
        groupModel = groupModel_new;

        isMeAdmin = groupModel_new.getUser_id().equals(new SharedPrefHelper(GroupDetailsActivity.this).getUserId());

        for (int i = 0; i < groupModel_new.getMembersList().size(); i++) {
            if (new SharedPrefHelper(GroupDetailsActivity.this).getUserId().equals(groupModel_new.getMembersList().get(i).getUser_id()) && groupModel_new.getMembersList().get(i).getIs_admin()) {
                isMeAdmin = true;
            }
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, new GroupDetailsFragment(groupModel_new));
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView viewById = (TextView) toolbar.findViewById(R.id.title);
        if (viewById != null) {
            viewById.setText(title);
            getSupportActionBar().setTitle("");
        }
        else {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        else if (item.getItemId() == R.id.leaveGroup) {
            leaveGroupNow();
        }
        else if (item.getItemId() == R.id.deleteGroup) {
            deleteGroupNow();
        }
        else if (item.getItemId() == R.id.editGroup) {
            CreateGroupAndSignUpActivity.startForEdit(GroupDetailsActivity.this, groupModel);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isMeAdmin) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_delete_group, menu);
        }
        else {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_leave_group, menu);
        }
        return true;
    }

    private void leaveGroupNow() {
        DialogHelper.getInstance().showWithAction(GroupDetailsActivity.this, "Are you sure you want to left group \"" + groupModel.getGroup_title() + "\"? Click Ok to confirm.", new CallBackG<String>()
        {
            @Override
            public void onCallBack(String output) {
                Call<BasicApiModel> call = LVApplication.getRetrofit().create(UserGroupApis.class).leave_group(new SharedPrefHelper(GroupDetailsActivity.this).getUserId(), groupModel.getId());
                call.enqueue(new Callback<BasicApiModel>()
                {
                    @Override
                    public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                        if (response.body().getStatus()) {
                            DialogHelper.getInstance().showInformation(GroupDetailsActivity.this, "You have successfully left group \"" + groupModel.getGroup_title() + "\".", new CallBackG<String>()
                            {
                                @Override
                                public void onCallBack(String output) {
                                    finish();
                                }
                            });
                        }
                        else {
                            UtillsG.showToast(response.body().getMessage(), GroupDetailsActivity.this, true);
                        }
                    }

                    @Override
                    public void onFailure(Call<BasicApiModel> call, Throwable t) {
                        Log.e("ERROR GET GROUP LIST", t.toString() + "");
                    }
                });
            }
        });


    }

    private void deleteGroupNow() {
        DialogHelper.getInstance().showWithAction(GroupDetailsActivity.this, "Are you sure you want to Delete this group \"" + groupModel.getGroup_title() + "\"? All the group members and content will be lost. Click Ok to confirm.", new CallBackG<String>()
        {
            @Override
            public void onCallBack(String output) {
                Call<BasicApiModel> call = LVApplication.getRetrofit().create(UserGroupApis.class).delete_group(groupModel.getId());
                call.enqueue(new Callback<BasicApiModel>()
                {
                    @Override
                    public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                        if (response.body().getStatus()) {
                            DialogHelper.getInstance().showInformation(GroupDetailsActivity.this, "You have successfully deleted group \"" + groupModel.getGroup_title() + "\".", new CallBackG<String>()
                            {
                                @Override
                                public void onCallBack(String output) {
                                    finish();
                                }
                            });
                        }
                        else {
                            UtillsG.showToast(response.body().getMessage(), GroupDetailsActivity.this, true);
                        }
                    }

                    @Override
                    public void onFailure(Call<BasicApiModel> call, Throwable t) {
                        Log.e("ERROR GET GROUP LIST", t.toString() + "");
                    }
                });
            }
        });


    }

}
