/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.MonitorLecture;

import gilm.lecture.verb.Features.QuickLecture.Presentation.PresentationOptions.PptImagesModel;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureModel;
import gilm.lecture.verb.Features.QuickLecture.ThumbnailResponseModel;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.Web;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by G-Expo on 17 Aug 2017.
 */

public interface LectureMonitorApi {
    @Multipart
    @POST(Web.Path.upload_recorded_event)
    Call<BasicApiModel> upload_recorded_event(
            @Part MultipartBody.Part audio_file,
            @Part MultipartBody.Part video_file,
            @Part(Web.Keys.lectureverb) RequestBody lectureverb,
            @Part(Web.Keys.user_id) RequestBody user_id,
            @Part(Web.Keys.is_co_host) RequestBody is_co_host,
            @Part(Web.Keys.event_id) RequestBody event_id
    );
}
