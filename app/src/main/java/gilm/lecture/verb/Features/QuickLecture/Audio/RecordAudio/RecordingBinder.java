/*
 * Copyright (c) 2017. Code by G-Expo , Everyone is open to use code in this project. Happy coding
 */

package gilm.lecture.verb.Features.QuickLecture.Audio.RecordAudio;

import android.databinding.ObservableField;
import android.view.View;

import gilm.lecture.verb.Features.QuickLecture.Audio.PostAudio.PostAudioActivity;

/**
 * Created by G-Expo on 18 Jul 2017.
 */

public interface RecordingBinder
{
    /**
     * Record or Pause if already recording
     * use {@link RecordingBinder#isRecording()} to detect recording.
     *
     * @param view-clicked view
     */
    void recordClicked(View view);

    /**
     * Play recorded file.
     *
     * @param view-clicked view
     */
    void playRecording(View view);

    /**
     * trim recorded file.{@link param.app.audiotrimmer2.RingdroidEditActivity} is used for trim.
     *
     * @param view-clicked view
     */
    void trimRecording(View view);

    /**
     * delete recorded file.
     *
     * @param view-clicked view
     */
    void deleteRecording(View view);

    /**
     * Continue with recorded audio to next screen.i.e {@link PostAudioActivity}
     *
     * @param view-clicked view
     */
    void next(View view);

    /**
     * delete,trim,play,next buttons will be visible according to this boolean.
     *
     * @return binded {@link Boolean} value
     */
    ObservableField<Boolean> isRecording();

    ObservableField<Boolean> isRecorded();

    /**
     * @return message to be displayed at bottom of record button.
     */
    ObservableField<String> recordingMessage();
}
