package gilm.lecture.verb.Features.LectureEvent.Invitation;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Internal.Base.Contract.Viewable;
import gilm.lecture.verb.Features.LectureEvent.LectureEventViewModel;
import gilm.lecture.verb.Features.LectureEvent.Upcoming.UpcomingPresenter;

/**
 * Created by G-Expo on 07 Jul 2017.
 */

public interface InvitationView extends Viewable<InvitationPresenter>
{
    void showData(ArrayList<LectureEventViewModel> list,int pageNo);
}
