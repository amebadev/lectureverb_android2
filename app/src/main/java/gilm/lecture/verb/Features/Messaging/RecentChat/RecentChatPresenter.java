/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Features.Messaging.RecentChat;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.BasePresenter;
import gilm.lecture.verb.Features.Messaging.ChatApis;
import gilm.lecture.verb.UtilsG.IsLoadingViewModel;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by G-Expo on 17 Jul 2017.
 */

public class RecentChatPresenter extends BasePresenter<RecentChatView> implements RecentChatBinder
{

    public IsLoadingViewModel isLoadingViewModel;

    public IsLoadingViewModel getIsLoadingViewModel() {
        return isLoadingViewModel;
    }

    public RecentChatPresenter() {
        isLoadingViewModel = new IsLoadingViewModel();
        isLoadingViewModel.setLoading(false);
    }

    private List<ListOfRecentChatsModel.RecentChatsModel> list = new ArrayList<>();

    public void loadRecentChats() {
        isLoadingViewModel.setLoading(true);

        Call<ListOfRecentChatsModel> recentChatsApi = LVApplication.getRetrofit().create(ChatApis.class).get_recent_chats(new SharedPrefHelper(getView().getActivityG()).getUserId(), "1");
        recentChatsApi.enqueue(new Callback<ListOfRecentChatsModel>()
        {
            @Override
            public void onResponse(Call<ListOfRecentChatsModel> call, Response<ListOfRecentChatsModel> response) {
                isLoadingViewModel.setLoading(false);
                if(getView()!=null && response.body()!=null) {
                    if (response.body().getStatus()) {
                        if (getView() != null) {
                            list.clear();
                            list.addAll(response.body().getChat());
                            getView().showData(list);
                        }
                    } else {
                        // UtillsG.showToast("No Recent Chats Found.", getView().getActivityG(), true);
                    }
                }
            }

            @Override
            public void onFailure(Call<ListOfRecentChatsModel> call, Throwable t) {
                if(getView()!=null) UtillsG.showToast(t.getMessage(), getView().getActivityG(), true);
            }
        });


    }

    @Override
    public void openChat(View view, RecentChatViewModel recentChatViewModel) {
//        ChatActivity.start(getView().getActivityG(), ((ViewGroup) view).getChildAt(0));
    }

    public void loadMoreData(int current_page) {
        Call<ListOfRecentChatsModel> recentChatsApi = LVApplication.getRetrofit().create(ChatApis.class).
                get_recent_chats(new SharedPrefHelper(getView().getActivityG()).getUserId(), String.valueOf(current_page));
        recentChatsApi.enqueue(new Callback<ListOfRecentChatsModel>()
        {
            @Override
            public void onResponse(Call<ListOfRecentChatsModel> call, Response<ListOfRecentChatsModel> response) {
                isLoadingViewModel.setLoading(false);
                if (response.body().getStatus()) {
                    if (getView() != null) {
                        list.addAll(response.body().getChat());
                        getView().moreDateLoaded(list);
                    }
                }
                else {
                    UtillsG.showToast(response.body().getMessage(), getView().getActivityG(), true);
                }
            }

            @Override
            public void onFailure(Call<ListOfRecentChatsModel> call, Throwable t) {
                UtillsG.showToast(t.getMessage(), getView().getActivityG(), true);
            }
        });
    }
}
