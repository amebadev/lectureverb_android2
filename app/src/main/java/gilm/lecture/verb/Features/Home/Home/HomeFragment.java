package gilm.lecture.verb.Features.Home.Home;

import android.view.View;

import java.util.List;

import gilm.lecture.verb.Features.Internal.Base.BaseFragment;
import gilm.lecture.verb.Features.Internal.Base.InfiniteAdapterG;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.DialogHelper;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.databinding.FragmentRecyclerlistBinding;

/**
 * Created by G-Expo on 06 Jul 2017.
 */

public class HomeFragment extends BaseFragment<FragmentRecyclerlistBinding, HomePresenter> implements HomeView, InfiniteAdapterG.OnLoadMoreListener
{

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_recyclerlist;
    }

    @Override
    protected void onCreateFragmentG() {
        injectPresenter(new HomePresenter());
        getPresenter().attachView(this);
    }

    @Override
    public void initViews() {
        getDataBinder().setIsLoading(getPresenter().getIsLoadingViewModel());
        getPresenter().loadHomeData(new SharedPrefHelper(getActivityG()).getUserId(), 1);
    }

    @Override
    public void showData(List<HomeViewModel> list) {
        HomeAdapter homeAdapter = new HomeAdapter(list, getPresenter());
        homeAdapter.setOnLoadMoreListener(this);
        getDataBinder().reviewsList.setAdapter(homeAdapter);
        homeAdapter.setShouldLoadMore(false);
    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void showOptionsOnAdapterItem(View view) {
        DialogHelper.getInstance().showHomeAdapterMenu(getActivityG(), view, new CallBackG<Integer>()
        {
            @Override
            public void onCallBack(Integer output) {
                getPresenter().adapterOptionClicked(output);
            }
        });
    }
}
