package gilm.lecture.verb.WebServices;

import java.util.List;

/**
 * Created by param on 23/4/18.
 */

public class AdsModel extends BasicApiModel {


    private List<AdsModel> list;

    public List<AdsModel> getList() {
        return list;
    }

    public void setList(List<AdsModel> list) {
        this.list = list;
    }

    int id;
    String title = "";
    String image_path = "";
    String data = "";
    String expire_date = "";
    String ad_type = ""; // 1= subscription, 2= event, 3 = quick_lecture, 4 = custom link
    String ad_screen = "";
    int counter;
    String created = "";
    String updated = "";

    String ad_height = "";
    String ad_width = "";

    public String getAd_height() {
        return ad_height;
    }

    public void setAd_height(String ad_height) {
        this.ad_height = ad_height;
    }

    public String getAd_width() {
        return ad_width;
    }

    public void setAd_width(String ad_width) {
        this.ad_width = ad_width;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public String getAd_type() {
        return ad_type;
    }

    public void setAd_type(String ad_type) {
        this.ad_type = ad_type;
    }

    public String getAd_screen() {
        return ad_screen;
    }

    public void setAd_screen(String ad_screen) {
        this.ad_screen = ad_screen;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }
}
