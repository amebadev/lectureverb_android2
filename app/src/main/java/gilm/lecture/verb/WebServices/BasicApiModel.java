/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.WebServices;

import com.google.gson.annotations.SerializedName;

/**
 * Created by G-Expo on 09 Aug 2017.
 */

public class BasicApiModel
{
    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    public boolean getStatus()
    {
        return status.equals(Web.Keys.SUCCESS);
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }


}
