/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.WebServices;

import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.piterwilson.audio.MP3RadioStreamDelegate;
import com.piterwilson.audio.MP3RadioStreamPlayer;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

import io.fabric.sdk.android.Fabric;
import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.concurrent.TimeUnit;

import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.Home.Home.Lecture_And_Event_Model;
import gilm.lecture.verb.Features.LectureEvent.CreateEditLectureEvent.EventLectureApi;
import gilm.lecture.verb.Features.LectureEvent.LectureEventsModels.LectureDetailsModel;
import gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity;
import gilm.lecture.verb.Features.Player.PlayerActivity;
import gilm.lecture.verb.Features.Player.PublicAudioPlayer;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureApi;
import gilm.lecture.verb.Features.QuickLecture.QuickLectureModel;
import gilm.lecture.verb.Features.QuickLecture.ThumbnailResponseModel;
import gilm.lecture.verb.R;
import gilm.lecture.verb.UtilsG.CallBackG;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class LVApplication extends Application implements ProgressRequestBody.UploadCallbacks {

    private static Retrofit retrofit;
    private final static float CLICK_DRAG_TOLERANCE = 10; // Often, there will be a slight, unintentional, drag when the user taps the FAB, so we need to account for this.

    private float downRawX, downRawY;
    private float dX, dY;
    private LinearLayout llMinimizedPlayer;

    final int progressNotificationId = 002;

    /**
     * Change this to {@code false} when you want to use the downloadable Emoji font.
     */

    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            initRetrofitModule();
        }
        return retrofit;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        MultiDex.install(getApplicationContext());


        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(getResources().getString(R.string.com_twitter_sdk_android_CONSUMER_KEY), getResources().getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET)))
                .debug(true)
                .build();
        Twitter.initialize(config);


        //for camera
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }

        initRetrofitModule();
    }

    private static void initRetrofitModule() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(Web.Path.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okClient())
                .build();

    }

    private static OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10, TimeUnit.MINUTES)
                .build();
    }

    LinearLayout minimizxedPlayerLayout;
    private CardView cardMinimizedPlayer;
    private ImageView imgvPlayPause;
    private Lecture_And_Event_Model lecture_and_event_model;

    /**
     * For marshmallow and above devices, there is a special permission needed for this .
     * check {@link gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity}'s onResume to check that permission .
     * I have implemented it there.
     */
    public void showMinimizedPlayer(Lecture_And_Event_Model lecture_and_event_model) {

        this.lecture_and_event_model = lecture_and_event_model;

        minimizxedPlayerLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.inflater_minimized_audio_player, null);
        llMinimizedPlayer = (LinearLayout) minimizxedPlayerLayout.findViewById(R.id.llMinimizedPlayer);

        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.BOTTOM;

        WindowManager mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.addView(minimizxedPlayerLayout, params);

        ImageView imgvClosePlayer = minimizxedPlayerLayout.findViewById(R.id.imgvClosePlayer);
        TextView txtvOwnerName = minimizxedPlayerLayout.findViewById(R.id.txtvOwnerName);
        TextView txtvLectureTitle = minimizxedPlayerLayout.findViewById(R.id.txtvLectureTitle);
        cardMinimizedPlayer = minimizxedPlayerLayout.findViewById(R.id.cardMinimizedPlayer);
        imgvPlayPause = minimizxedPlayerLayout.findViewById(R.id.imgvPlayPause);

        String name = lecture_and_event_model.getUserdata().getFull_name();
        txtvOwnerName.setText(name.trim());
        txtvLectureTitle.setText(lecture_and_event_model.getQuick_lecture_text());
        txtvLectureTitle.setSelected(true);
        txtvOwnerName.setSelected(true);

        if (PublicAudioPlayer.getInstance().getAudioPlayer().isPause()) {
            imgvPlayPause.setImageResource(android.R.drawable.ic_media_play);
        } else {
            imgvPlayPause.setImageResource(android.R.drawable.ic_media_pause);
        }


        PublicAudioPlayer.getInstance().getAudioPlayer().setDelegate(new MP3RadioStreamDelegate() {
            @Override
            public void onRadioPlayerPlaybackStarted(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
            }

            @Override
            public void onRadioPlayerStopped(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
                try {
                    Handler handler = new Handler(Looper.getMainLooper()) {
                        @Override
                        public void handleMessage(Message msg) {
                            // Any UI task, example
                            hideMusicPlayer();
                        }
                    };
                    handler.sendEmptyMessage(1);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                PublicAudioPlayer.setReleaseAudioPlayer();
            }

            @Override
            public void onRadioPlayerError(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
            }

            @Override
            public void onRadioPlayerBuffering(MP3RadioStreamPlayer mp3RadioStreamPlayer) {

            }
        });

        imgvPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PublicAudioPlayer.getInstance().getAudioPlayer().isPause()) {
                    PublicAudioPlayer.getInstance().getAudioPlayer().setPause(false);
                    imgvPlayPause.setImageResource(android.R.drawable.ic_media_pause);
                } else {
                    PublicAudioPlayer.getInstance().getAudioPlayer().setPause(true);
                    imgvPlayPause.setImageResource(android.R.drawable.ic_media_play);
                }
            }
        });
        imgvClosePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PublicAudioPlayer.setReleaseAudioPlayer();
            }
        });

        cardMinimizedPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayerActivity.startAndResume(LVApplication.this, LVApplication.this.lecture_and_event_model, false);

                hideMusicPlayer();
            }
        });


        llMinimizedPlayer.setVisibility(View.VISIBLE);
    }

    private void hideMusicPlayer() {
        minimizxedPlayerLayout.setVisibility(View.GONE);
    }


    private void showProgressNotification(String title, String message) {
        try {
            NotificationCompat.Builder mBuilder;
            mBuilder = new NotificationCompat.Builder(this, "default")
                    .setSmallIcon(R.mipmap.ic_upload)
                    .setContentTitle(title)
                    .setLights(Color.parseColor(new SharedPrefHelper(this).getNotificationLightColorCode()), 20, 80)
                    .setAutoCancel(true)
                    .setOngoing(true)
                    .setContentText(message);

            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            new Intent(),
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            mBuilder.setContentIntent(resultPendingIntent);

            final NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            mNotifyMgr.notify(progressNotificationId, mBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearProgressNotification() {
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.cancel(progressNotificationId);
    }

    public void uploadEventInBackground(final String videoPath, String title, String discoveryUrl, final String categoryIds, final File mArtWorkUrl, LectureDetailsModel lectureDetailsModel) {

        showProgressNotification("Uploading", "Uploading recorded event \'" + title + "\'.");

        // getting duration of video file *********************
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        //use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(this, Uri.fromFile(new File(videoPath)));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time);

        // conerting milis to minutes***************
        long minutes = (timeInMillisec / 1000) / 60;
        long seconds = (timeInMillisec / 1000) % 60;
        String secondsStr = Long.toString(seconds);
        String secs;
        if (secondsStr.length() >= 2) {
            secs = secondsStr.substring(0, 2);
        } else {
            secs = "0" + secondsStr;
        }

        String duration = minutes + ":" + secs;
// conerting milis to minutes**********
        retriever.release();
        // getting duration of video file *********************

        Call<QuickLectureModel> quickLectureApiModelCall = getRetrofitInstance().uploadAudioLecture(
                getFilePart(new File(videoPath), "event_video"),
                getOtherParams("event_video"),
                getOtherParams(new SharedPrefHelper(this).getUserId()),
                getOtherParams(title),
                getOtherParams(discoveryUrl),
                getOtherParams("Public")
                , getOtherParams(duration)
                , getOtherParams(new SharedPrefHelper(this).getUserName())
                , getOtherParams("")
                , getOtherParams(lectureDetailsModel.getLecture_poster())
                , getOtherParams(lectureDetailsModel.getGroup_id()));
        quickLectureApiModelCall.enqueue(new Callback<QuickLectureModel>() {
            @Override
            public void onResponse(Call<QuickLectureModel> call, final Response<QuickLectureModel> response) {


                if (response.body() != null) {

                    if (response.body().getStatus()) {

                        Call<ThumbnailResponseModel> lectureThumbnailApiModelCall = getRetrofitInstance().uploadLectureThumbnail(
                                getFilePart(mArtWorkUrl, "thumbnail"),
                                getOtherParams(response.body().getmQuickLectureData().getQuick_lecture_id()),
                                getUploadTypePart());
                        lectureThumbnailApiModelCall.enqueue(new Callback<ThumbnailResponseModel>() {
                            @Override
                            public void onResponse(Call<ThumbnailResponseModel> thumbnailApiCall, Response<ThumbnailResponseModel> thumbnailApiResponse) {

                                if (thumbnailApiResponse.body() != null) {

                                    if (thumbnailApiResponse.body().getStatus()) {
                                        UtillsG.deleteThisFile(mArtWorkUrl.getAbsolutePath());
                                        UtillsG.deleteThisFile(videoPath);
//                                                UtillsG.deleteThisFile(mArtWorkUrl.getAbsolutePath());

                                        updateLectureEventInterest(categoryIds, response.body().getmQuickLectureData().getQuick_lecture_id());

                                    } else {
//                                        displayError("Error in creating quick lecture");
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ThumbnailResponseModel> thumbnailApiCall, Throwable t) {
                                Log.e("Error", t.toString());
                                clearProgressNotification();
                            }
                        });
                    } else {
                        clearProgressNotification();
                        UtillsG.showNotification(LVApplication.this, "Uploading Failed", "Lecture event uploading failed.", new Intent());
                    }
                }
            }

            @Override
            public void onFailure(Call<QuickLectureModel> call, Throwable t) {
                Log.e("Error", t.toString());

                clearProgressNotification();
                UtillsG.showNotification(LVApplication.this, "Uploading Failed", "Lecture event uploading failed.", new Intent());
//                UtillsG.showToast("Error in creating quick lecture = " + t.toString(), getActivityG(), true);
            }
        });
    }

    public void updateLectureEventInterest(String categoryIds, final String quickLectureId) {
        setLectureInterests(this, LVApplication.getRetrofit().create(EventLectureApi.class), quickLectureId,
                categoryIds, "lecture", new CallBackG<Boolean>() {
                    @Override
                    public void onCallBack(Boolean output) {
                        clearProgressNotification();

                        Intent intent = new Intent(LVApplication.this, NavigationActivity.class);
                        intent.putExtra(Constants.Extras.tabNumber, 1);

                        UtillsG.showNotification(LVApplication.this, "Uploading completed", "Lecture event successfully uploaded as a post.", intent);
//                        UtillsG.showToast("Quick lecture created successfully", LVApplication.this, true);
                        EventBus.getDefault().post(new HomeFragmentUpdateBus(true));
                    }
                });
    }

    public void setLectureInterests(final Context mActivity, EventLectureApi getRetrofitInstance, String eventId, String interestedIds, String interestType, final CallBackG<Boolean> values) {
        Call<BasicApiModel> basicApiModelCall = getRetrofitInstance.save_lecture_or_event_interest(
                new SharedPrefHelper(mActivity).getUserId()
                , eventId
                , interestedIds
                , interestType
        );
        basicApiModelCall.enqueue(new Callback<BasicApiModel>() {
            @Override
            public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                if (response.body() != null) {

//                    UtillsG.showToast(response.body().getMessage(), mActivity, true);

                    if (response.body().getStatus()) {
                        values.onCallBack(true);
                    } else {
                        values.onCallBack(false);
                    }
                } else {
                    values.onCallBack(false);
                }
            }

            @Override
            public void onFailure(Call<BasicApiModel> call, Throwable t) {
                Log.e("Error occured", t.getMessage().toString());
                values.onCallBack(false);
            }
        });
    }


    private MultipartBody.Part getFilePart(final File file, String fileType) {
        ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(fileType, file.getName(), fileBody);
        return filePart;
    }

    private RequestBody getUploadTypePart() {
        return RequestBody.create(MediaType.parse("text/plain"), "upload");
    }


    private RequestBody getOtherParams(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }


    private QuickLectureApi getRetrofitInstance() {
        return LVApplication.getRetrofit().create(QuickLectureApi.class);
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }


}
