/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.WebServices;

import java.io.File;

/**
 * Created by G-Expo on 09 Aug 2017.
 */

public @interface Web
{

    @interface Keys
    {

        String FAILURE = "Failure";
        String SUCCESS = "Success";
        String EMAIL = "email";
        String PASSWORD = "password";
        String FULL_NAME = "full_name";
        String DEVICE_TYPE = "device_type";
        String DEVICE_TOKEN = "device_token";
        String SOCIAL_ID = "social_id";
        String PROFILE_PIC = "profile_pic";

        String USER_ID = "user_id";
        String VERIFICATION_CODE = "verification_code";
        String ROLE_ID = "role_id";
        String REGISTER_VIA = "register_via";
        String BIO = "bio";
        String WEBSITE = "website";
        String LOCATION = "location";

        String QUICK_LECTURE_TYPE = "quick_lecture_type"; // (audio , video)
        String FILE = "file"; // (audio ya video)
        String QUICK_LECTURE_TEXT = "quick_lecture_text";
        String DISCOVERY_URL = "discovery_url";
        String PRIVACY = "privacy";

        String quick_lecture_id = "quick_lecture_id";
        String page = "page";


        String category_id = "category_id";
        String followed_user_id = "followed_user_id";
        String follower_user_id = "follower_user_id";
        String device_token = "device_token";
        String tag_name = "tag_name";
        String query = "query";
        String message = "message";
        String group_title = "group_title";
        String group_image = "group_image";
        String group_type = "group_type";
        String upload_type = "upload_type";
        String group_id = "group_id";
        String keyword = "keyword";
        String categories = "categories";
        String radius = "radius";
        String lng = "lng";
        String lat = "lat";
        String is_co_host = "is_co_host";
        String event_id = "event_id";
        String user_id = "user_id";
        String lectureverb = "lectureverb";
        String my_user_id = "my_user_id";
        String target_user_id = "target_user_id";
        String is_approved = "is_approved";
        String privacy = "privacy";
        String amount = "amount";
        String payment_via = "payment_via";
        String group_description = "group_description";
        String sender_id = "sender_id";
        String sender_is_admin = "sender_is_admin";
        String group_creater_id = "group_creater_id";
        String target_user_name = "target_user_name";
        String group_name = "group_name";
        String sender_user_id = "sender_user_id";
        String lecture_event_id = "lecture_event_id";
        String share_type = "share_type";
        String share_to = "share_to";
        String share_url = "share_url";
    }


    @interface File_Options
    {
        String IMAGE = "IMAGE";
        String AUDIO = "AUDIO";
        String VIDEO = "VIDEO";
        String DOC = "DOC";
        String PDF = "PDF";
        String TEXT = "TEXT";

    }

    @interface NotificationList_Type
    {
        String favourite = "favourite";
        String repost = "reposted";
        String share = "share";
        String comment = "comment";
        String external_share = "external_share";
    }

    @interface BlockUserStates
    {
        String BlockByMe = "1";
        String BlockByOther = "2";
        String Noblock = "0";
    }

    @interface GroupType
    {
        String Institute = "1";
        String PrivateTraining = "2";
        String PublicGroup = "3";
    }

    @interface LocalFolders
    {
        String LectureVerbVideos = "LectureVerb/LectureVerbVideos" + File.separator;
        String LectureVerbAudio = "LectureVerb/LectureVerbAudio" + File.separator;
        String LectureVerbThumbs = "LectureVerb/LectureVerbThumbs" + File.separator;
        String LectureVerbImages = "LectureVerb/LectureVerbImages" + File.separator;
    }


    @interface Path
    {
        //        String EXTERNAL_SHARE_URL_QUICK_LECTURE = "http://103.43.152.211/lecture_verb_web/share/lecture?id=";
        String EXTERNAL_SHARE_URL_QUICK_LECTURE = "https://lectureverb-app.lectureverb.com/share/lecture?id=";
        //        String EXTERNAL_SHARE_URL_EVENT = "http://103.43.152.211/lecture_verb_web/share/event?id=";
        String EXTERNAL_SHARE_URL_EVENT = "https://lectureverb-app.lectureverb.com/share/event?id=";

        String BASE_URL = "https://lectureverb-app.lectureverb.com/app/";
//        String BASE_URL = "http://103.43.152.211/lecture_verb_staging/app/";
//        String BASE_URL = "http://192.168.88.18/lecture_verb_staging/app/";

        String BASE_IMAGE_URL = "https://lectureverb-app.lectureverb.com/app";
        //        String BASE_IMAGE_URL = "http://192.168.88.18/lecture_verb_staging/app";
        String MID_URL = "lecture_verb.php?lectureverb=";
        String MANUAL_LOGIN = MID_URL + "login";
        String SIGNUP = MID_URL + "signup";
        String RESEND_VERIFICATION = MID_URL + "resendverificationcode";
        String FORGOT_PASSWORD = MID_URL + "forgot_password";
        String EMAIL_VERIFICATION = MID_URL + "emailverification";
        String SOCIAL_LOGIN = MID_URL + "social_login";
        String UPDATE_PROFILE_IMAGE = MID_URL + "update_profile_image";
        String UPDATE_PROFILE = MID_URL + "update_profile";
        String UPDATE_PROFILE_COVER_IMAGE = MID_URL + "update_profile_cover_image";
        String ADD_QUICK_LECTURE = MID_URL + "add_quick_lecture";
        String update_quick_lecture_thumbnail = MID_URL + "update_quick_lecture_thumbnail";
        String get_posted_lectures = MID_URL + "get_posted_lectures";
        String upload_zamzar_file = MID_URL + "upload_zamzar_file";
        String create_lecture_event = MID_URL + "create_lecture_event";
        String get_single_lecture_event = MID_URL + "get_single_lecture_event";
        String update_device_details = MID_URL + "update_device_details";
        String search_user = MID_URL + "search_user";
        String get_single_user = MID_URL + "get_single_user";
        String get_nearby_co_hosts = MID_URL + "get_nearby_co_hosts";
        String get_lecture_event = MID_URL + "get_lecture_event";
        String get_event_invitation = MID_URL + "get_event_invitation";
        String get_my_hosting_list = MID_URL + "get_my_hosting_list";
        String event_attend_and_interest = MID_URL + "event_attend_and_interest";
        String send_event_invitation = MID_URL + "send_event_invitation";
        String like_unlike_lecture = "like_unlike_lecture";
        String add_remove_to_favourite_list = "add_remove_to_favourite_list";
        String add_lecture_to_recent_played_list = "add_lecture_to_recent_played_list";
        String get_favourite_lectures = "get_favourite_lectures";
        String update_quick_lecture = MID_URL + "update_quick_lecture";
        String update_lecture_event = MID_URL + "update_lecture_event";
        String search_lecture_event = MID_URL + "search_lecture_event";
        String share = MID_URL + "share";
        String send_direct_notification = MID_URL + "send_direct_notification";
        String get_share_list = MID_URL + "get_share_list";
        String get_notifications = MID_URL + "get_notifications";
        String save_lecture_or_event_interest = MID_URL + "save_lecture_or_event_interest";
        String save_users_interest = MID_URL + "save_users_interest";
        String get_categories = MID_URL + "get_categories";
        String get_suggested_lectures = MID_URL + "get_suggested_lectures";
        String repost_lecture = MID_URL + "repost_lecture";
        String get_my_followers = "get_my_followers";
        String get_follow_requests_list = "get_follow_requests_list";
        String get_my_followings = "get_my_followings";
        String people_you_may_know = "people_you_may_know";
        String follow_user = "follow_user";
        String approve_follow_request = "approve_follow_request";
        String comments_list = MID_URL + "comments_list";
        String reply_comments_list = MID_URL + "reply_comments_list";
        String write_comment_reply = MID_URL + "write_comment_reply";
        String lecture_write_comment = MID_URL + "lecture_write_comment";
        String get_my_posted_lectures = MID_URL + "get_my_posted_lectures";
        String unrepost_lecture = MID_URL + "unrepost_lecture";
        String get_user_profile = MID_URL + "get_user_profile";
        String get_chat_messages = MID_URL + "get_chat_messages";
        String chat = MID_URL + "chat";
        String delete_chat_message = MID_URL + "delete_chat_message";
        String get_recent_chats = MID_URL + "get_recent_chats";
        String block_unblock_user = MID_URL + "block_unblock_user";
        String update_device_token = MID_URL + "update_device_token";
        String save_external_share = MID_URL + "save_external_share";
        String mark_notification_read = MID_URL + "mark_notification_read";
        String update_counter = MID_URL + "update_counter";
        String get_ash_tags = MID_URL + "get_ash_tags";
        String add_user_ash_tags = MID_URL + "add_user_ash_tags";
        String find_help_answers = "find_help_answers";
        String get_general_answers = "get_general_answers";
        String send_report_email = MID_URL + "send_report_email";
        String create_group = MID_URL + "create_group";
        String get_my_user_groups = MID_URL + "get_my_user_groups";
        String get_all_user_groups = MID_URL + "get_all_user_groups";
        String join_group = MID_URL + "join_group";
        String search_all_user_groups = MID_URL + "search_all_user_groups";
        String leave_group = MID_URL + "leave_group";
        String delete_group = MID_URL + "delete_group";
        String add_new_categories = MID_URL + "add_new_categories";
        String remove_user_category = MID_URL + "remove_user_category";
        String getNearByUsers = "getNearByUsers";
        String get_lectures_by_category = "get_lectures_by_category";
        String upload_recorded_event = "lecture_verb.php";
        String get_lecture_likes_comments_reposts_list = "get_lecture_likes_comments_reposts_list";
        String get_follow_status = MID_URL + "get_follow_status";
        String delete_notification = MID_URL + "delete_notification";
        String set_profile_privacy = MID_URL + "set_profile_privacy";
        String get_profile_privacy = MID_URL + "get_profile_privacy";
        String delete_lecture = MID_URL + "delete_quick_lecture";
        String get_posted_lectures_by_category = MID_URL + "get_posted_lectures_by_category";
        String mark_event_completed = MID_URL + "mark_event_completed";
        String logout = MID_URL + "logout";
        String delete_user_account = MID_URL + "delete_user_account";
        String save_group_payments = MID_URL + "save_group_payments";
        String get_group_lectures = MID_URL + "get_group_lectures";
        String invite_user = MID_URL + "invite_user";
        String get_single_group = MID_URL + "get_single_group";
        String updateGroup = MID_URL + "edit_group";
        String get_pending_invitations = MID_URL + "get_pending_invitations";
        String approve_invite = MID_URL + "approve_invite";
        String get_ads = MID_URL + "get_ads";
        String get_single_posted_lectures = MID_URL + "get_single_posted_lectures";
        String user_ads_counter = MID_URL + "user_ads_counter";
        String ads_counter = MID_URL + "ads_counter";
        String suggested_link_counter = MID_URL + "suggested_link_counter";
        String search_quick_lectures = MID_URL + "search_quick_lectures";
        String get_prices = MID_URL + "get_prices";
        String save_subscription = MID_URL + "save_subscription";
        String save_social_share = MID_URL + "save_social_share";
    }
}
