package gilm.lecture.verb.WebServices;

import java.util.ArrayList;

import gilm.lecture.verb.Features.Home.Home.LectureModel;

/**
 * created by PARAMBIR SINGH on 5/9/17.
 */

public class PostedLecturesModel
{
    private String status;
    private String message;
    private ArrayList<LectureModel> data;

    public ArrayList<LectureModel> getData() {
        return data;
    }

    public void setData(ArrayList<LectureModel> data) {
        this.data = data;
    }

    public boolean getStatus()
    {
        return status.equals(Web.Keys.SUCCESS);
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
