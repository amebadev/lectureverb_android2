package gilm.lecture.verb;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import gilm.lecture.verb.Features.Home.Home.HomeFragmentUpdateBus;
import gilm.lecture.verb.Features.Home.NewNotificationTypeBus;
import gilm.lecture.verb.Features.Home.RefreshQuickLectureBus;
import gilm.lecture.verb.Features.Messaging.Chat.ISBlockedUnBlockedBus;
import gilm.lecture.verb.Features.NaviagtionDrawer.NavigationActivity;
import gilm.lecture.verb.UtilsG.Constants;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by mohit on 19/8/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    Context context;
    SharedPrefHelper mSharedPrefsHelper;
    int notification_icon;

    /*{user_id=5, body={"msg":null,"content_available":true,"flag":"22","user_id":"5"}, flag=22, sound=default, title=LectureVerb, content_available=true}*/

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("NOTIFICATION_RECEIVED", "--------------------------------------NOTIFICATIONS " + remoteMessage.getData() + " -------------------------------------------------");
        context = this;
        mSharedPrefsHelper = new SharedPrefHelper(context);

        notification_icon = R.mipmap.ic_notification;

        try {
            final String notifMessage;
            if (remoteMessage.getData().get("body") != null) {
                notifMessage = remoteMessage.getData().get("body");
            } else {
                notifMessage = remoteMessage.getNotification().getBody();
            }

            JSONObject mainObj = new JSONObject(notifMessage);

            String flag = mainObj.optString("flag");

            Intent intent = getRelevantIntent(flag);

            EventBus.getDefault().post(UtillsG.getNotNullString(mainObj.optString("sender_id"), ""));

            if (flag.equals(Constants.NotificationFlags.add_to_favourite_list)) {
                notification_icon = R.mipmap.ic_favorite;
                EventBus.getDefault().post(new RefreshQuickLectureBus(mainObj.optString("quick_lecture_id"), true, true));
            } else if (flag.equals(Constants.NotificationFlags.remove_from_favorite)) {
                EventBus.getDefault().post(new RefreshQuickLectureBus(mainObj.optString("quick_lecture_id"), true, false));
            } else if (flag.equals(Constants.NotificationFlags.reposted)) {
                notification_icon = R.mipmap.ic_repost;
                EventBus.getDefault().post(new RefreshQuickLectureBus(mainObj.optString("quick_lecture_id"), false, true));
            } else if (flag.equals(Constants.NotificationFlags.unreposted)) {
                EventBus.getDefault().post(new RefreshQuickLectureBus(mainObj.optString("quick_lecture_id"), false, false));
            } else if (flag.equals(Constants.NotificationFlags.new_quick_lecture)
                    ||
                    flag.equals(Constants.NotificationFlags.new_lecture_event)
                    ||
                    flag.equals(Constants.NotificationFlags.update_lecture_event)
                    ||
                    flag.equals(Constants.NotificationFlags.lecture_write_comment)) {
                notification_icon = R.mipmap.new_post;
                EventBus.getDefault().post(new HomeFragmentUpdateBus(true));
            } else if (flag.equals(Constants.NotificationFlags.block_unblock_user)) {
                EventBus.getDefault().post(new ISBlockedUnBlockedBus(
                        mainObj.optString("blocked_status").equals("blocked"), mainObj.optString("user_id"), false));
            } else if (flag.equals(Constants.NotificationFlags.chat)) {
                notification_icon = R.mipmap.ic_message_notification;
                EventBus.getDefault().post(new ISBlockedUnBlockedBus(false, mainObj.optString("sender_id"), true));
            } else if (flag.equals(Constants.NotificationFlags.following) || flag.equals(Constants.NotificationFlags.unfollowing) || flag.equals(Constants.NotificationFlags.follow_approved)) {
                notification_icon = R.mipmap.ic_following;
            } else if (flag.equals(Constants.NotificationFlags.send_event_invitation)) {
                notification_icon = R.mipmap.ic_notification_invite;
            } else if (flag.equals(Constants.NotificationFlags.share)) {
                notification_icon = R.mipmap.ic_notification_share;
            }

            if (!flag.equals(Constants.NotificationFlags.block_unblock_user)
                    && !flag.equals(Constants.NotificationFlags.unfollowing)
                    ) {
                generateNotificationNew(
                        UtillsG.getNotNullString(mainObj.optString("sender_name"), getResources().getString(R.string.app_name)),
                        UtillsG.getNotNullString(UtillsG.decodeEmoji(mainObj.optString("msg")), "You have new notification.")
                        , intent);
            }


            if (flag.equals(Constants.NotificationFlags.chat)) {
                mSharedPrefsHelper.setIsNewMessage(true);
                EventBus.getDefault().post(new NewNotificationTypeBus(true, false));

            }


            else {
                mSharedPrefsHelper.setIsNewNotification(true);
                EventBus.getDefault().post(new NewNotificationTypeBus(false, true));
            }


            // setting us badge on app launcher icon
            int unreadCount = mSharedPrefsHelper.getUnreadCount();
            if (flag.equals(Constants.NotificationFlags.remove_from_favorite)) {
                unreadCount = (unreadCount == 0) ? unreadCount : (unreadCount - 1);
            } else {
                if (!flag.equals(Constants.NotificationFlags.block_unblock_user)
                        && !flag.equals(Constants.NotificationFlags.unfollowing)
                        )
                {
                    unreadCount++;
                }
            }
            mSharedPrefsHelper.setUnreadCount(unreadCount);
            ShortcutBadger.applyCount(this, unreadCount);

        } catch (Exception e) {
            Log.e("FCM ERROR", e.toString());
            generateNotificationNew(getResources().getString(R.string.app_name), "You have new notification.", new Intent());
        }
    }

    private void generateNotificationNew(String title, String message, Intent notificationIntent) {
        try {
//            Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Uri defaultSound = resourceToUri(this, new SharedPrefHelper(this).getSoundId());

            boolean isSoundEnabled = new SharedPrefHelper(this).isNotificationSoundEnabled();


            if (defaultSound == null) {
                defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                if (defaultSound == null) {
                    defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                }
            }

            SharedPrefHelper spHelper = new SharedPrefHelper(this);

            NotificationCompat.Builder mBuilder;
            if (isSoundEnabled) {
                mBuilder = new NotificationCompat.Builder(this, "default")
                        .setSmallIcon(notification_icon)
                        .setContentTitle(title)
                        .setSound(defaultSound)
                        .setVibrate(new long[]{spHelper.getVibrationPattern(), spHelper.getVibrationPattern()})
                        .setLights(Color.parseColor(new SharedPrefHelper(this).getNotificationLightColorCode()), 20, 80)
                        .setAutoCancel(true)
                        .setContentText(message);
            } else {
                mBuilder = new NotificationCompat.Builder(this, "default")
                        .setSmallIcon(notification_icon)
                        .setContentTitle(title)
                        .setVibrate(new long[]{spHelper.getVibrationPattern(), spHelper.getVibrationPattern()})
                        .setLights(Color.parseColor(new SharedPrefHelper(this).getNotificationLightColorCode()), 20, 80)
                        .setAutoCancel(true)
                        .setContentText(message);

            }
            Intent resultIntent = notificationIntent;
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            mBuilder.setContentIntent(resultPendingIntent);

            int mNotificationId = 001;
            // Gets an instance of the NotificationManager service
            NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // Builds the notification and issues it.
            mNotifyMgr.notify(mNotificationId, mBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Uri resourceToUri(Context context, int resID) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                context.getResources().getResourcePackageName(resID) + '/' +
                context.getResources().getResourceTypeName(resID) + '/' +
                context.getResources().getResourceEntryName(resID));
    }


    public Intent getRelevantIntent(String flag) {
        Intent intent = new Intent();
        if (!UtillsG.getNotNullString(mSharedPrefsHelper.getUserId(), "").isEmpty()) { // If user logged in
            switch (flag) {
                case Constants.NotificationFlags.add_to_favourite_list: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 2);//Notification List Screen under HometabFragment
                    return intent;
                }
                case Constants.NotificationFlags.remove_from_favorite: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 1);//Notification List Screen under HometabFragment
                    return intent;
                }
                case Constants.NotificationFlags.lecture_write_comment: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 2);//Notification List Screen under HometabFragment
                    return intent;
                }
                case Constants.NotificationFlags.block_unblock_user: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 2);//Notification List Screen under HometabFragment
                    return intent;
                }
                case Constants.NotificationFlags.notification_event_updated: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 2);//Notification List Screen under HometabFragment
                    return intent;
                }
                case Constants.NotificationFlags.event_attend_and_interest: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 2);//Notification List Screen under HometabFragment
                    return intent;
                }
                case Constants.NotificationFlags.send_event_invitation: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 2); //Notification List Screen under HometabFragment
                    return intent;
                }
                case Constants.NotificationFlags.share: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 0);
                    return intent;
                }
                case Constants.NotificationFlags.chat: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 3); //Recent Chat Screen under HometabFragment
                    return intent;
                }
                case Constants.NotificationFlags.following: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 2); //notification fragment
                    return intent;
                }
                case Constants.NotificationFlags.unfollowing: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 1); //My Profile Tab
                    return intent;
                }
                case Constants.NotificationFlags.follow_approved: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 2); //notification fragment
                    return intent;
                }
                case Constants.NotificationFlags.reposted: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 1); //My Profile Tab
                    return intent;
                }
                case Constants.NotificationFlags.unreposted: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 1); //My Profile Tab
                    return intent;
                }
                case Constants.NotificationFlags.new_quick_lecture: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 0); //My Profile Tab
                    return intent;
                }
                case Constants.NotificationFlags.new_lecture_event: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 0); //My Profile Tab
                    return intent;
                }
                case Constants.NotificationFlags.update_lecture_event: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 0); //My Profile Tab
                    return intent;
                }
                case Constants.NotificationFlags.added_to_group: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 2);  //notification fragment
                    return intent;
                } case Constants.NotificationFlags.group_invitation: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 2);  //notification fragment
                    return intent;
                }case Constants.NotificationFlags.add_group_admin: {
                    intent = new Intent(this, NavigationActivity.class);
                    intent.putExtra(Constants.Extras.tabNumber, 2);  //notification fragment
                    return intent;
                }
                default:
                    return intent;
            }
        } else {
            return intent;
        }
    }

}
