package gilm.lecture.verb;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import gilm.lecture.verb.Features.Home.HomeApis;
import gilm.lecture.verb.UtilsG.SharedPrefHelper;
import gilm.lecture.verb.UtilsG.UtillsG;
import gilm.lecture.verb.WebServices.BasicApiModel;
import gilm.lecture.verb.WebServices.LVApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mohit on 19/8/17.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService
{
    private static final String TAG = "---------------------- ";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Token Refreshed --------------------- :::: " + refreshedToken + " ::::----------");

        sendRegistrationToServer(refreshedToken);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void sendRegistrationToServer(String refreshedToken) {

        SharedPreferences sharedPreferences = getSharedPreferences("LV_DeviceId_Prfs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("LV_DeviceId",refreshedToken).apply();

        if (!UtillsG.getNotNullString(new SharedPrefHelper(this).getUserId(), "").isEmpty()) {
            Call<BasicApiModel> updateDeviceId = LVApplication.getRetrofit().create(HomeApis.class).update_device_token(new SharedPrefHelper(this).getUserId(), refreshedToken);
            updateDeviceId.enqueue(new Callback<BasicApiModel>()
            {
                @Override
                public void onResponse(Call<BasicApiModel> call, Response<BasicApiModel> response) {
                }

                @Override
                public void onFailure(Call<BasicApiModel> call, Throwable t) {

                }
            });
        }
    }
}
