package gilm.lecture.verb.Widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.Features.InterestsSelection.ChildCategoryModel;
import gilm.lecture.verb.WebServices.Web;


@SuppressLint("AppCompatCustomView")
public class CategoriesEdittext extends AutoCompleteTextView implements AdapterView.OnItemClickListener
{
    GetTagsHelperPK GET_CITY_G;
    SuggestionsCallBack suggestionsCallBack;

    public CategoriesEdittext(Context context) {
        super(context);
    }

    public CategoriesEdittext(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CategoriesEdittext(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CategoriesEdittext(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public <T extends ListAdapter & Filterable> void setAdapter(T adapter) {
        super.setAdapter(adapter);
    }

    public void startSuggestions(Context con, SuggestionsCallBack suggestionsCallBack) {
        this.suggestionsCallBack = suggestionsCallBack;
        GET_CITY_G = new GetTagsHelperPK(con, android.R.layout.simple_list_item_1);
        this.setAdapter(GET_CITY_G);
        this.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        ChildCategoryModel resultList = GET_CITY_G.set_LAT_Lng_value(position);
        suggestionsCallBack.onTagInserted(resultList);
    }

    // *************************************************************  INTERFACE FOR SENDING THE SELECTED LOCATION DATA BACK TO THE CALLING CLASS  ******************************************************************


    public interface SuggestionsCallBack
    {
        public void onTagInserted(ChildCategoryModel tagsModel);
    }

    // *********************************************************  ADAPTER  **********************************************************************

    public class GetTagsHelperPK extends ArrayAdapter<String> implements Filterable
    {
        public List<ChildCategoryModel> resultList = new ArrayList<>();
        private Context con;

        public GetTagsHelperPK(Context context, int textViewResourceId) {

            super(context, textViewResourceId);
            con = context;
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            try {
                return resultList.get(index).getCategory();
            }
            catch (Exception e) {

            }
            return "";
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter()
            {
                @Override
                protected FilterResults performFiltering(final CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        if (!fetchingAddress) {
                            resultList = GetAddressByString(con, constraint.toString());
                        }

                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    try {
                        if (results != null && results.count > 0) {
                            notifyDataSetChanged();
                        }
                        else {
                            notifyDataSetInvalidated();
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            return filter;
        }


        boolean fetchingAddress = false;

        private List<ChildCategoryModel> GetAddressByString(Context con, final String addressssss) {
            fetchingAddress = true;
            String addressReturn = "";

            List<ChildCategoryModel> dataList = new ArrayList<>();


            if (!addressssss.isEmpty()) {
                dataList = getLocationFromString(addressssss);
            }
            fetchingAddress = false;


            return dataList;

        }

        public List<ChildCategoryModel> getLocationFromString(String address) {

            List<ChildCategoryModel> Ldata = new ArrayList<>();

            try {
                String URL = Web.Path.BASE_URL + Web.Path.MID_URL + "search_categories&keyword=" + URLEncoder.encode(address, "UTF-8");
                JSONObject jsonObject = new JSONObject(new WebServiceHandler().performGetCall(URL));
                JSONArray results = jsonObject.getJSONArray("data");
                ChildCategoryModel l;
                for (int j = 0; j < results.length(); j++) {
                    JSONObject innerObj = results.optJSONObject(j);
                    l = new ChildCategoryModel();
                    l.setCategory(innerObj.optString("category"));
                    l.setId(innerObj.optString("id"));
                    Ldata.add(l);
                }
            }
            catch (Exception e) {
                return Ldata;
            }
            catch (Error e) {
                return Ldata;
            }

            return Ldata;
        }

        public ChildCategoryModel set_LAT_Lng_value(int position) {
            return resultList.get(position);
        }


    }

    public class WebServiceHandler
    {
        public String performGetCall(String url) throws Exception {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            Log.e("== get Call response====".toUpperCase(), response.toString());
            return response.toString(); //this is your response
        }
    }
}
