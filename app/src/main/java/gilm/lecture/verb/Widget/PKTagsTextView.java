package gilm.lecture.verb.Widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import gilm.lecture.verb.WebServices.Web;
import mabbas007.tagsedittext.TagsEditText;


public class PKTagsTextView extends TagsEditText implements AdapterView.OnItemClickListener
{
    GetTagsHelperPK GET_CITY_G;
    SuggestionsCallBack suggestionsCallBack;

    public PKTagsTextView(Context context) {
        super(context);
    }

    public PKTagsTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PKTagsTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PKTagsTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public <T extends ListAdapter & Filterable> void setAdapter(T adapter) {
        super.setAdapter(adapter);
    }

    public void startSuggestions(Context con, SuggestionsCallBack suggestionsCallBack) {
        this.suggestionsCallBack = suggestionsCallBack;
        GET_CITY_G = new GetTagsHelperPK(con, android.R.layout.simple_list_item_1);
        this.setAdapter(GET_CITY_G);
        this.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        TagsModel resultList = GET_CITY_G.set_LAT_Lng_value(position);
        suggestionsCallBack.onTagInserted(resultList);
    }

    // *************************************************************  INTERFACE FOR SENDING THE SELECTED LOCATION DATA BACK TO THE CALLING CLASS  ******************************************************************


    public interface SuggestionsCallBack
    {
        public void onTagInserted(TagsModel tagsModel);
    }

    // *********************************************************  ADAPTER  **********************************************************************

    public class GetTagsHelperPK extends ArrayAdapter<String> implements Filterable
    {
        public List<TagsModel> resultList = new ArrayList<>();
        private Context con;

        public GetTagsHelperPK(Context context, int textViewResourceId) {

            super(context, textViewResourceId);
            con = context;
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            try {
                return resultList.get(index).getTag_name();
            }
            catch (Exception e) {

            }
            return "";
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter()
            {
                @Override
                protected FilterResults performFiltering(final CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        if (!fetchingAddress) {
                            resultList = GetAddressByString(con, constraint.toString());
                        }

                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    try {
                        if (results != null && results.count > 0) {
                            notifyDataSetChanged();
                        }
                        else {
                            notifyDataSetInvalidated();
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            return filter;
        }


        boolean fetchingAddress = false;

        private List<TagsModel> GetAddressByString(Context con, final String addressssss) {
            fetchingAddress = true;
            String addressReturn = "";

            List<TagsModel> dataList = new ArrayList<>();


            if (!addressssss.isEmpty()) {
                dataList = getLocationFromString(addressssss);
            }
            fetchingAddress = false;


            return dataList;

        }

        public List<TagsModel> getLocationFromString(String address) {

            List<TagsModel> Ldata = new ArrayList<TagsModel>();

            try {
                String URL = Web.Path.BASE_URL + Web.Path.MID_URL + "get_all_hash_tags&tag_name=" + URLEncoder.encode(address, "UTF-8");
                JSONObject jsonObject = new JSONObject(new WebServiceHandler().performGetCall(URL));
                JSONArray results = jsonObject.getJSONArray("data");
                TagsModel l;
                for (int j = 0; j < results.length(); j++) {
                    JSONObject innerObj = results.optJSONObject(j);
                    l = new TagsModel();
                    l.setTag_name(innerObj.optString("tag_name"));
                    l.setTag_id("tag_id");
                    Ldata.add(l);
                }
            }
            catch (Exception e) {
                return Ldata;
            }
            catch (Error e) {
                return Ldata;
            }

            return Ldata;
        }

        public TagsModel set_LAT_Lng_value(int position) {
            return resultList.get(position);
        }


    }

    // *************************************************************  LATITUDE LONGITUDE MODEL  ******************************************************************
    public class TagsModel
    {
        String tag_name, tag_id;

        public String getTag_name() {
            return tag_name;
        }

        public void setTag_name(String tag_name) {
            this.tag_name = tag_name;
        }

        public String getTag_id() {
            return tag_id;
        }

        public void setTag_id(String tag_id) {
            this.tag_id = tag_id;
        }
    }

    // *****************************************************************  WEBSERVICE CLASS FOR GETTINGRESPONSE FROM GOOGLE  **************************************************************
    public class WebServiceHandler
    {
        public String performGetCall(String url) throws Exception {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            Log.e("== get Call response====".toUpperCase(), response.toString());
            return response.toString(); //this is your response
        }
    }
}
