/*
 * Copyright (c) 2017. Code by G-Expo . Happy coding
 */

package gilm.lecture.verb.Widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by G-Expo on 31 Jul 2017.
 */

public class HomeLinearLayout extends LinearLayout
{
    public HomeLinearLayout(Context context)
    {
        super(context);
    }

    public HomeLinearLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public HomeLinearLayout(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int i = Math.round(widthMeasureSpec/2);
        super.onMeasure(widthMeasureSpec, i );
    }
}
