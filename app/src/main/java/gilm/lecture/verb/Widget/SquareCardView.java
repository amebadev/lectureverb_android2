/*
 * Copyright (c) 2017. Code by G-Expo , Everyone is open to use code in this project. Happy coding
 */

package gilm.lecture.verb.Widget;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

/**
 * Created by G-Expo on 18 Jul 2017.
 */

/**
 * View with height==width i.e Square
 */
public class SquareCardView extends CardView
{

    public SquareCardView(Context context)
    {
        super(context);
    }

    public SquareCardView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public SquareCardView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

}
